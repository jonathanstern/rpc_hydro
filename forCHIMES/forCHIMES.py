import time, os, sys, multiprocessing, traceback, h5py
homedir = os.environ['HOME']
if homedir=='/home/jonathan': homedir+='/Dropbox'
chimes_dir = homedir+'/other_repositories/chimes/'
chimes_cross_sections_dir = chimes_dir+ 'chimes-tools/generate_cross_sections/'

rpc_hydro_dir = homedir+'/other_repositories/rpc_hydro/'
project_dir = rpc_hydro_dir+'forCHIMES/'
cross_sections_dir = project_dir + 'cross_sections/'

sys.path.append(chimes_cross_sections_dir )
V96_data_file = chimes_cross_sections_dir +'data/cross_section_fits_verner96.dat'
import generate_cross_sections
import numpy as np, pylab as pl
from numpy import log as ln, log10 as log
from scipy import interpolate
from astropy import units as un, constants as cons

import pylab as pl, matplotlib
from matplotlib import ticker

sys.path.append(rpc_hydro_dir+'pysrc_new/' )
from parameters import *
from plot_utils import *
from Table import PLUTO_Table




def find_temperature_equilibrium(netCoolingTable,U0,NH,NHI):
    Trange = 10.**np.arange(2,8,0.01)
    LAMBDAs = netCoolingTable(U0,Trange,NH,NHI)
    ind_Teqs = (LAMBDAs[:-1]*LAMBDAs[1:]<0).nonzero()[0]
    if(len(ind_Teqs )==1):
        return Trange[ind_Teqs[0]]
    if len((LAMBDAs<0).nonzero()[0])==0: #all cooling
        return None
    assert(False)
        
def equilibrium_solution(U_f, opacityTables, netCoolingTable, fHI_Table,return_extras=False):
    dustOptOpacity, dustIonOpacity, gasIonOpacity = opacityTables
    def opacityTable(U0,T,NH,NHI): 
        return ((1-ion_fraction)*dustOptOpacity(U0,T,NH,NHI)*dust2gasRatio
                + ion_fraction*(dustIonOpacity(U0,T,NH,NHI)*dust2gasRatio 
                                + gasIonOpacity(U0,T,NH,NHI))) / X * cons.m_p.to('g').value
    c = cons.c.to('cm/s').value; kB = cons.k_B.to('erg/K').value
    N_ZONES = 30000
    dr = min(0.01*un.pc.to('cm'), 3e18 / (10**lng / U_f))
    rs   = (np.ones(N_ZONES)*dr).cumsum()
    U0s  = np.zeros(N_ZONES)
    Ts   = np.zeros(N_ZONES)
    NHs  = np.zeros(N_ZONES)
    NHIs = np.zeros(N_ZONES)
    mus = np.zeros(N_ZONES)
    taus = np.zeros(N_ZONES)
    sigmas = np.zeros(N_ZONES)
    fHIs = np.zeros(N_ZONES)
    
    U0s[0] = U_f 
    taus[0] = 0.
    for i in range(N_ZONES-1): #update Ts[i], U0s[i+1], NHs[i+1], NHIs[i+1]
        if i%1000==0: print('zone #%d/%d'%(i,N_ZONES),end=',')
        Ts[i] = find_temperature_equilibrium(netCoolingTable, U0s[i], NHs[i],NHIs[i])
        if Ts[i]==None: # no equilibrium_solution, gas always cools
            Ts = Ts[:i]
            rs = rs[:i]            
            U0s = U0s[:i]
            NHs = NHs[:i]
            NHIs = NHIs[:i]
            taus = taus[:i]
            break
        fHIs[i] = fHI_Table(U0s[i],Ts[i],NHs[i],NHIs[i])
        mus[i] = 1./((1.-fHIs[i])*(X+Y/2.+Z/2.) + X/AH + Y/AHe + Z/AZ)
        if i>0:
            U0s[i] *= Ts[i]*(mus[i]*X)**-1/(Ts[i-1]*(mus[i-1]*X)**-1)
        nH  = 10**lng / U0s[i]
        sigmas[i] = opacityTable(U0s[i],Ts[i],NHs[i],NHIs[i])        
        dtau = sigmas[i]*nH*dr
        dPgas_to_nH = U0s[i] * mean_hnu / ion_fraction * np.e**(-taus[i]) * (1-np.e**-dtau) #U0 * mean_hnu / ion_fr = Frad_0 / (nH * c).                 
        NHs[i+1]  = NHs[i]  + dr * nH
        NHIs[i+1] = NHIs[i] + dr * nH * fHIs[i]
        taus[i+1] = taus[i] + dtau        
        U0s[i+1]  = U0s[i] / (1 + dPgas_to_nH/((mus[i]*X)**-1 * kB * Ts[i]))
    if not return_extras:
        return U0s, Ts, NHIs, NHs, rs, taus
    return U0s, Ts, NHIs, NHs, rs, taus, sigmas, fHIs

def plot_equilibrium_solutions(U_fs,fn_static_solution,elec_table):
    pl.figure(figsize=(10,7))
    pl.subplots_adjust(wspace=0.5,hspace=0.3)
    cmap = pl.get_cmap('viridis')
    for iU,U_f in enumerate(U_fs):
        f = np.load(fn_static_solution[:-4]+'%d.npz'%log(U_f))
        NHIs = f['NHIs']; NHs = f['NHs']; Ts = f['Ts']; U0s = f['U0s']; taus=f['taus']
        f.close()
        nHs = 10.**lng/U0s
        nes = elec_table(NHIs,NHs,Ts,nHs) * nHs
        fHIs = np.concatenate([NHIs[:1]/NHs[:1], (NHIs[1:]-NHIs[:-1]) / (NHs[1:]-NHs[:-1])])
        mus = 1./((1.-fHIs)*(X+Y/2.+Z/2.) + X/AH + Y/AHe + Z/AZ)
        Prad_to_k = (10.**lng*un.cm**-3 * mean_hnu*un.erg / cons.k_B).to('cm**-3*K')
        Pgas_to_Prad = (mus*X)**-1*10**lng/U0s*Ts / Prad_to_k
        
        for iPanel in range(6):
            ax = pl.subplot(2,3,iPanel+1)        
            pl.semilogx(taus, (U0s,nHs, Ts,(mus*X)**-1,Pgas_to_Prad,Ts/U0s)[iPanel],
                        c=cmap(iU/6),label='%.0f'%log(U_f))    
            if iPanel==4:
                pl.plot(taus,Pgas_to_Prad[0].value + 1-np.e**(-taus),c='k',ls='--',zorder=100)
    for iPanel in range(6):
        ax = pl.subplot(2,3,iPanel+1)
        pl.xlim(0.01,10)
        if iPanel==0:
            pl.ylabel(r'$U_0$')
            ax.set_yscale('log')
            Ueq = 2.3*cons.k_B.to('erg/K')*1e4*un.K / mean_hnu
            pl.axhline(Ueq.value,c='k',ls=':')            
            pl.ylim(0.01,1)
        if iPanel==1:
            pl.ylabel(r'$n_H$')
            ax.set_yscale('log')
        if iPanel==2:
            pl.ylabel(r'$T$')
            ax.set_yscale('log')
            pl.legend(title=r'$\log\ U_i$',handlelength=1)
        if iPanel==3: 
            pl.ylabel(r'$n/n_{\rm H}$')
            pl.ylim(1,2.5)        
            pl.axhline(1,c='k',ls=':')            
        if iPanel==4:             
            pl.ylabel(r'$P_{\rm gas}/P_{\rm rad}$')
            
            pl.axhline(1,c='k',ls=':')
            ax.set_yscale('log')
        if iPanel==5:             
            #pl.ylabel(r'$\tau_{\rm Ha}$')
            ax.set_yscale('log')
    
        
    
    
def baseSpectrum():    
    # slopes in lambda_min, lambda max, alpha (defined as L_nu ~ nu^alpha)
    slopes = [(Ryd_to_Ang(10.**min_logE), 60e4, 2.5), 
              (60e4,20e4, -0.5), 
              (20e4,3e4,-1.001),
              (3e4,1e4,-1.5),
              (1e4,1100,-0.5),  #LD93
              (1100,6.2,aEUV), 
              (6.2,12.4/200,-1.001), 
              (12.4/200, Ryd_to_Ang(10.**max_logE),-2.) ]  # a_x from BAT (Tueller+08), a_gamma from Hazy
    
    freqslist = [log(Ang_to_Ryd(slope[0])) for slope in slopes] + [log(Ang_to_Ryd(slopes[-1][1]))]
    fluxslist = np.array([0] + [log(slope[0]/slope[1])*slope[2] for slope in slopes])
    fluxslist = fluxslist.cumsum()
    
    total_lum = np.sum([Ang_to_Ryd(slope[0])* Rydbergnu *
                        10.**fluxslist[i] / (slope[2]+1)
                         * ((slope[0]/slope[1])**(slope[2]+1)-1)
                        for i,slope in enumerate(slopes)]) 
    print(fluxslist,total_lum)
    fluxslist += log(LAGN / (4*np.pi*rAGN**2) / (4*np.pi) / total_lum)
    
    
    return freqslist, fluxslist

    
def writeOut(filename, freqslist, fluxslist):
    f = open(filename,'w')
    f.write("""# This file defines the shape of the 
# spectrum used to calculate the
# average cross sections, photoheating
# rates and shielding factors.
# This example is a power-law AGN SED (Laor & Draine 1993)
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Column      Variable      Units          Description
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#   1	      log10(E)       Ryd           log10 of the photon
#   	   	       	                   energy in Ryd
#   2        log10(J_nu) erg/s/sr/cm^2/Hz  log10 of the specific intensity
#   	     		 		   in erg/s/sr/cm^2/Hz 
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
""")
    f.writelines(['%f %f\n'%(freqslist[i],fluxslist[i]) for i in range(len(freqslist))])
    f.close()

def absorbed_spectrum(baseSpectrum,grain_cross_sections_fn):
    # read dust opacities    
    grain_cross_sections = grain_cross_sections_func(logEs)

    # read base spectrum
    baseSpectrum_logJs = interpolate.interp1d(baseSpectrum[0],baseSpectrum[1],
                                              bounds_error=False,fill_value=MIN_log_flux)(logEs)
    
    
    # read HI absorption spectrum
    H_section_pars = generate_cross_sections.read_in_V96_cross_sections(V96_data_file)[0][0]
    H_thresh_energy = H_section_pars[0]
    HI_cross_sections = generate_cross_sections.sigma(10.**logEs*H_thresh_energy, 
                                                               H_section_pars[2:9], 
                                                               0)    
    HI_cross_sections *= (logEs>-dlogE)
    # calculate attentuations
    attentuatedSpectrums_logJs = np.zeros((NHIs.shape[0],NHs.shape[0],logEs.shape[0]))
    for i in range(NHIs.shape[0]):
        for j in range(NHs.shape[0]):
            taus = NHIs[i]*HI_cross_sections + NHs[j]*grain_cross_sections 
            attentuatedSpectrums_logJs[i,j,:] = baseSpectrum_logJs - taus/ln(10)
            
    attentuatedSpectrums_logJs = ((attentuatedSpectrums_logJs<=MIN_log_flux)*MIN_log_flux +
                                  (attentuatedSpectrums_logJs>MIN_log_flux)*attentuatedSpectrums_logJs)
    print('HI cross section at 1 ryd: %.3g'%HI_cross_sections[logEs>0][0])
    print('dust cross section at 1 ryd: %.3g '%grain_cross_sections[logEs>0][0])
    return attentuatedSpectrums_logJs

def ionizing_photons_fraction(baseSpectrum,spectrum):
    Qs   = spectrum / 10.**logEs 
    Qs0  = baseSpectrum / 10.**logEs
    dnus = np.pad(10.**logEs[2:] - 10.**logEs[:-2],1,'edge')
    return (Qs*dnus)[logEs>0].cumsum() / (Qs0*dnus)[logEs>0].cumsum()  
    
    
def cross_sections_by_attentuation_loop(absorbed_spectrum_filename,multipleProcs=4):    
    pool = multiprocessing.Pool(processes=multipleProcs,maxtasksperchild=1)
    for i,NHI in enumerate(NHIs):
        for j,NH in enumerate(NHs):
            pool.apply_async(cross_sections_by_attentuation, (absorbed_spectrum_filename,
                                                              NHI,NH,i,j))
    pool.close()
 
def cross_sections_by_attentuation(absorbed_spectrum_filename,
                                   NHI,NH,iNHI,iNH):
    try:
        print('====process id: %d (%s), generating cross sections for NHI=%.1f NH=%.1f======'%(
            os.getpid(),time.ctime().split()[3],log(NHI),log(NH)),flush=True)
            
        parameters = {
            "chimes_main_data_path" : chimes_dir + "chimes-data/chimes_main_data.hdf5", 
            "cross_sections_data_path" : chimes_dir + "chimes-tools/generate_cross_sections/data",
            "output_file_template" : cross_sections_dir+"cross_sections_PL_AGN_NHI%.1f_NH%.1f.hdf5",        
        }
        parameters["output_file"] = parameters["output_file_template"]%(log(NHI),log(NH))
        if os.path.exists(parameters["output_file"]): return
        f = np.load(absorbed_spectrum_filename)
        parameters['spectrum_E_log'] = f['spectrum_E_log']
        parameters['spectrum_J_log'] = f['spectrum_J_log'][iNHI,iNH]        
        generate_cross_sections.main(parameters,log_N=np.array([15.])) #log_N here is for calculation of internal shielding, assumed to be 0
    except:
        traceback.print_exc()
        raise
                
def add_Cloudy_terms(CHIMES_table,Cloudy_arr,Cloudy_lUs,lnHs,lUs,lTs,lNHs,lNHIs):
    new_table = np.zeros(CHIMES_table.shape)
    for inH,lU in enumerate(lUs):
        for iT,lT in enumerate(lTs):        
            for iNH,lNH in enumerate(lNHs):
                for iNHI,lNHI in enumerate(lNHIs):
                    index = iNHI,iNH,iT,inH
                    if lNH<log(sigma_dust_one_rydberg**-1) and lNHI<log(sigma_HI_one_rydberg**-1):
                        dust_term = np.interp(lU,Cloudy_lUs,Cloudy_arr[iT]) + 2*lnHs[inH]
                        new_table[index] = log(10**CHIMES_table[index]+10**dust_term)
                    else:
                        new_table[index] = CHIMES_table[index]
    return new_table
            
                        
                        

def create_CHIMES_based_Table(cross_section_filename_template,
                              absorbed_spectrum_filename,
                              grain_cross_sections_fn,
                              chimes_abundances_filename,
                              chimes_cooling_rates_filename,
                              main_chimes_data_filename,
                              output_filename, nVals = 7,
                              Cloudy_filename = None):     

    # load reactants indices
    f_main = h5py.File(main_chimes_data_filename,'r')
    photoion_euv_reactants = np.array(f_main["photoion_euv/reactants"])
    f_main.close()
    
    
    # load abundances, cooling and heating from CHIMES
    f_abundances = h5py.File(chimes_abundances_filename)
    f_cooling = h5py.File(chimes_cooling_rates_filename)
    abundances = f_abundances['Abundances'][:,:,:,:,0,:]
    fHIs = abundances[:,:,:,:,1]
    
    lnHs = f_abundances['TableBins']['Densities'][:]
    lUs = lng-lnHs    
    lTs = f_abundances['TableBins']['Temperatures'][:]
    lNHs  = f_abundances['TableBins']['AVs'][:]
    lNHIs = f_abundances['TableBins']['NHIs'][:]
    
    _cooling = f_cooling['log_cooling_rate'][:,:,:,:,0]
    _heating = f_cooling['log_heating_rate'][:,:,:,:,0]
    if Cloudy_filename!=None: 
        f = np.load(Cloudy_filename)
        heating = add_Cloudy_terms(_cooling,f['log_coolings'],f['lUs'],lnHs,lUs,lTs,lNHs,lNHIs)
        cooling = add_Cloudy_terms(_heating,f['log_heatings'],f['lUs'],lnHs,lUs,lTs,lNHs,lNHIs)
    else:
        heating = _heating
        cooling = _cooling

    f_abundances.close()
    f_cooling.close()

    # read dust opacities
    grain_cross_sections = grain_cross_sections_func(logEs)
    
    
    # load absorbed spectrum
    f = np.load(absorbed_spectrum_filename)
    attentuatedSpectrums_logJs = f['spectrum_J_log']
    dnus = np.pad((10.**logEs[2:] - 10.**logEs[:-2])*Rydbergnu/2.,1,mode='edge')
    ind_nu0 = (logEs>=0).nonzero()[0][0]
    f.close()
    
    
    # create table
    f_output = open(output_filename,'w')
    f_output.write('%20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s\n'%(
        'U0', 'T [K]','NH [cm^-2]','NHI [cm^-2]',
        'Dust Optical Opacity [cm^2 g^-1]','Dust Ion Opacity [cm^2 g^-1]','Gas Ion Opacity [cm^2 g^-1]',
        'f_HI',
        'Heating [erg cm^3 s^-1]','Cooling [erg cm^3 s^-1]','netCooling [erg cm^3 s^-1]'))
    
    start_time = time.time()
    for inH,lU in enumerate(lUs):
        print('log U: %.1f. time since start: %.0f'%(lU,time.time()-start_time))
        for iT,lT in enumerate(lTs):        
            for iNH,lNH in enumerate(lNHs):
                for iNHI,lNHI in enumerate(lNHIs):
                    index = iNHI,iNH,iT,inH
                    f_output.write('%20.4g %20.4g %20.4g %20.4g '%(10**lU,10**lT,10**lNH,10**lNHI))            
                    #energetics = NamekataEnergetics(10**lU,10**T,10**newlNH,10**newlNHI,grainOpacityTuple,
                                                       #Lnu_spectral_slope=ionizing_spectral_slope)
                                                       
                    for ivalType in range(nVals):
                        if ivalType<2: #dust opacity (opt, ion)
                            nu_index = ((0,ind_nu0),(ind_nu0,None))[ivalType]
                            attentuatedSpectrum = 10.**attentuatedSpectrums_logJs[iNHI,iNH][nu_index[0]:nu_index[1]]
                            dust_absorbed_spectrum = attentuatedSpectrum * grain_cross_sections[nu_index[0]:nu_index[1]] 
                            val = ((dust_absorbed_spectrum * dnus[nu_index[0]:nu_index[1]]).sum() / 
                                   (attentuatedSpectrum * dnus[nu_index[0]:nu_index[1]]).sum())
                            val /= (cons.m_p.to('g').value/X) #opacities are per gram, not per H
                            val /= dust2gasRatio #dust opacities are per gram dust                        
                        if ivalType==2: #gas_ion_opacity_per_H
                            filename = cross_section_filename_template%(lNHI,lNH)                            
                            f = h5py.File(filename,'r')
                            opacities = f['photoion_euv']['sigmaPhot'][:]                            
                            f.close()
                            relevant_abundances = abundances[index][photoion_euv_reactants]
                            val = (relevant_abundances*opacities)[0] #only HI absorption
                            val /= (cons.m_p.to('g').value/X) #opacities are per gram, not per H
                        if ivalType==3: #fHI
                            val = fHIs[index]
                        if ivalType==4: #heating
                            val = 10.**heating[index] / (10.**lnHs[inH])**2
                        if ivalType==5: #cooling
                            val = 10.**cooling[index] / (10.**lnHs[inH])**2
                        if ivalType==6: #net_cooling
                            val = (10.**cooling[index] - 10.**heating[index]) / (10.**lnHs[inH])**2
                        f_output.write('%20.4g '%val)
                        if ivalType==nVals-1: f_output.write('\n')
    f_output.close()
def cooling_plot(table,baseU0):
    Ts = 10.**np.arange(1.5,8.5,.01)
    U0s  = 10.**np.array([-8,-5,-2,0.,2,5])
    
    fig = pl.figure(figsize=(15,5))
    pl.subplots_adjust(wspace=0.3)
    for irow in range(2):
        for icol in range(2):            
            pl.subplot(2,2,irow*2+icol+1)
            if irow==0:
                for iU0,U0 in enumerate(U0s):
                    c='kbcmry'[iU0]
                    coolings = table(np.array([U0]*len(Ts)),Ts,
                                                 np.array([0.]*len(Ts)),np.array([0.]*len(Ts))) 
                    pl.plot(Ts,coolings,label=r'$U_0=%s$'%(arilogformat(U0,dollarsign=False)),c=c)
                    pl.plot(Ts,-coolings,ls='--',c=c)
                pl.text(0.95,0.05,r'$A_{\rm V}=0,\ N_{\rm HI}=0$',transform=pl.gca().transAxes,ha='right')
            if irow==1:
                for iN,N in enumerate(NHs):   
                    c = pl.get_cmap('viridis')(iN/len(NHs))
                    if icol==0:
                        NHI = NHIs[iN]
                        NH = NHs[0]
                    if icol==1:
                        NH = NHs[iN]
                        NHI = NHIs[0]                
                    coolings = table(baseU0*np.ones(Ts.shape),Ts,NH*np.ones(Ts.shape),NHI*np.ones(Ts.shape))
                    pl.plot(Ts,coolings,label=('NH%s = %.1f'%('I '[icol],log(N)),'_')[iN%4!=0],c=c)
                    pl.plot(Ts,-coolings,ls='--',c=c)
                pl.text(0.95,0.05,r'$U_0=%s$'%nSignificantDigits(baseU0,1,True),transform=pl.gca().transAxes,ha='right')
            pl.loglog()
            pl.gca().xaxis.set_major_formatter(arilogformatter)
            pl.xlabel(r'$T$ [K]')
            pl.ylabel(r'$\Lambda$ [erg s$^{-1}$ cm$^3$]')
            pl.xlim(0.3,1e8)    
            pl.ylim(1e-25,0.3e-20)
            pl.legend(loc='upper left',fontsize=8,frameon=False)
def Teq_plot(table):
    cmap = pl.get_cmap('viridis')
    U0s  = 10.**np.arange(-8,5,.1)
    fig = pl.figure(figsize=(10,3.5))
    pl.subplots_adjust(wspace=0.3)    
    for icol in range(2):
        ax = pl.subplot(1,2,icol+1)
        for iN,N in enumerate(10.**np.arange(15.,24.1)):
            if icol==0: NH,NHI=N,1
            if icol==1: NH,NHI=1,N
            Teqs = []; goods = []
            for iU,U0 in enumerate(U0s):
                try:
                    Teq = find_temperature_equilibrium(table, U0, NH, NHI)
                    if Teq!=None:
                        Teqs.append(Teq)
                        goods.append(iU)
                    else:
                        if iU%10==0:
                            print('all cooling U0=%.1f NH=%.0f NHI=%.0f'%(log(U0),log(NH),log(NHI)))
                except:
                    if iU%10==0:
                        print('all heating U0=%.1f NH=%.0f NHI=%.0f'%(log(U0),log(NH),log(NHI)))
            if len(goods):
                Teqs = np.array(Teqs); goods = np.array(goods)
                pl.plot(U0s[goods],Teqs,label=r'$\log\ N_{\rm H%s}=%.0f$'%(('','I')[icol],log(N)),c=cmap(iN/9))
        pl.loglog()
        ax.xaxis.set_major_formatter(arilogformatter)
        ax.yaxis.set_major_formatter(arilogformatter)
        pl.ylabel(r'$T_{\rm eq}$ [K]')
        pl.xlabel(r'$U_0$')
        pl.ylim(1000,1e6)    
        pl.xlim(1e-7,100)
        pl.legend(loc='upper left',ncol=2,frameon=False)    
    
def split_table(table_fn,new_tables_prefix):
    table_names = {4: ('DustOpticalOpacity_table.txt','Dust Optical Opacity [cm^2 g^-1]'),
                   5: ('DustIonOpacity_table.txt','Dust Ion Opacity [cm^2 g^-1]'),
                   6: ('GasIonOpacity_table.txt','Gas Ion Opacity [cm^2 g^-1]'),
                   7: ('fHI_table.txt','f_HI'),
                   10: ('netCooling_table.txt','netCooling [erg cm^3 s^-1]')}    
    
    
    f = open(table_fn)   
    ls = f.readlines()
    f.close()
    for i in table_names.keys():
        f_out = open(new_tables_prefix+table_names[i][0],'w')     
        f_out.write('%20s %20s %20s %20s %20s\n'%(
                'U0', 'T [K]','NH [cm^-2]','NHI [cm^-2]',table_names[i][1]))        
        for l in ls[1:]:
            s = [float(x) for x in l.split()]
            f_out.write('%20.4g %20.4g %20.4g %20.4g %20.4g\n'%(s[0],s[1],s[2],s[3],s[i]))
        f_out.close()
         
def check_HI_opacity(gasIonOpacityTable,fHITable):
    column_tuples = (1e15,1e15),(1e18,1e18),(1e21,1e15),(1e22,1e22)
    U0s  = 10.**np.arange(-5.5,3,.1)
    T=1e4
    ones = np.ones(U0s.shape)    
    for iN,(NH,NHI) in enumerate(column_tuples):         
        ls = ('-','--',':','-.')[iN]
        fHIs = fHITable(U0s,T*ones,NH*ones,NHI*ones)
        sigmaHI = gasIonOpacityTable(U0s,T*ones,NH*ones,NHI*ones)
        pl.plot(U0s,fHIs,c='b',ls=ls)
        pl.plot(U0s,sigmaHI,c='r',ls=ls)
        pl.plot(U0s,sigmaHI/fHIs,c='k',ls=ls)
    pl.loglog()
        
        
def opacity_and_cooling_plot(iN_tups,attentuatedSpectrums_logJs,netCoolingTable,tables,figname,T = 1e4):
    """
    U -> n_H, U on top
    NH, NHI -> tau_HI, tau_dust
    """
    dustOptOpacityTable, dustIonOpacityTable, gasIonOpacityTable,fHITable = tables
    
    fig = pl.figure(figsize=(fig_width/2.2,7))
    pl.subplots_adjust(hspace=0.4)
    
    L = 1e46*un.erg/un.s
    r = 100*un.pc    
    
    ax = pl.subplot(311)
    ngamma = (L/(mean_hnu*un.erg) * ion_fraction / (4*np.pi*r**2*cons.c)).to('cm**-3')
    U0s  = 10.**np.arange(-5.5,3,.1)
    nHs = ngamma/U0s
    
    ones = np.ones(U0s.shape)
    for itup,(iNHI,iNH,c,lw,label) in enumerate(iN_tups[:]):                 
        NH,NHI = NHs[iNH],NHIs[iNHI]
        fHIs = fHITable(U0s,T*ones,NH*ones,NHI*ones)
        tau_HI = NHI * sigma_HI_one_rydberg
        tau_d = NH * sigma_dust_one_rydberg
        pl.plot(nHs,fHIs,c=c,lw=lw)
    pl.loglog()
    ax.xaxis.set_major_formatter(arilogformatter)
    ax.xaxis.set_major_locator(ticker.LogLocator(numdecs=15,numticks=15))
    ax.yaxis.set_major_locator(ticker.LogLocator(numdecs=15,numticks=15))
    pl.xlabel(r'$n_{\rm H}\ [{\rm cm}^{-3}]$')
    xls = np.array([100,0.3e7])
    pl.xlim(*xls)    
    pl.ylabel(r'$f_{\rm HI}$') 
    ax.yaxis.set_major_formatter(arilogformatter)                
    pl.ylim(1e-6,3)                
    pl.text(0.95,0.18,r'$r=100$ pc',transform=ax.transAxes,ha='right')
    pl.text(0.95,0.06,r'$L=10^{46}$ erg s$^{-1}$',transform=ax.transAxes,ha='right')
    ax2 = pl.twiny()
    ax2.set_xscale('log')
    ax2.xaxis.set_major_formatter(arilogformatter)
    ax2.xaxis.set_major_locator(ticker.LogLocator(numdecs=15,numticks=15))
    pl.xlim(*(ngamma.value/xls))
    pl.xlabel(r'$U_0$')
    #pl.plot(U0s,10**-5.5/U0s,c='k',ls=':')
    
    

    ax = pl.subplot(312)
    dnus = np.pad((10.**logEs[2:] - 10.**logEs[:-2])*Rydbergnu/2.,1,mode='edge')
    ind_nu0 = (logEs>=0).nonzero()[0][0]
    base_flux = (L/(4*np.pi*r**2)).to('erg * s**-1 *cm**-2')
    for iN,(iNHI,iNH,c,lw,label) in enumerate(iN_tups):         
        NH,NHI = NHs[iNH],NHIs[iNHI]
        attentuatedFlux  = 10.**attentuatedSpectrums_logJs[iNHI,iNH]*dnus 
        attentuatedFlux /= (10.**attentuatedSpectrums_logJs[0,0]*dnus).sum()
        absorbed_flux_fraction = 0.            
        for iband in range(2): #opt, ion                 
            opacity = (dustOptOpacityTable(U0s,T*ones,NH*ones,NHI*ones)*dust2gasRatio,
                       dustIonOpacityTable(U0s,T*ones,NH*ones,NHI*ones)*dust2gasRatio +
                       gasIonOpacityTable(U0s,T*ones,NH*ones,NHI*ones))[iband] 
            #opacity = (0,gasIonOpacityTable(U0s,T*ones,NH*ones,NHI*ones))[iband]                         
            nu_index = ((0,ind_nu0),(ind_nu0,None))[iband]                
            absorbed_flux_fraction += attentuatedFlux[nu_index[0]:nu_index[1]].sum() * opacity                                            
        accel = (absorbed_flux_fraction * base_flux * un.cm**2/un.g / cons.c).to('cm*s**-2')
        pl.plot(nHs,accel,lw=lw,c=c,label=label)
    pl.loglog()
    ax.xaxis.set_major_formatter(arilogformatter)
    ax.xaxis.set_major_locator(ticker.LogLocator(numdecs=15,numticks=15))
    ax.yaxis.set_major_locator(ticker.LogLocator(numdecs=15,numticks=15))
    ax.yaxis.set_major_formatter(arilogformatter)
    pl.xlabel(r'$n_{\rm H}\ [{\rm cm}^{-3}]$')
    pl.xlim(*xls)
    pl.ylabel(r'$a_{\rm rad}$ [cm s$^{-2}$]')
    pl.ylim(0.7e-5,0.003)
    #dP_to_rho = (2.3*X*cons.k_B*1e4*un.K / cons.m_p).to('cm * s**-2')
    #pl.plot(nHs,dP_to_rho ,ls=':',c='k')
    pl.legend(loc='upper left',handlelength=1.5,frameon=False)
            
    ax = pl.subplot(313)
    Ts = 10.**np.arange(1.5,8.5,.01)
    U0s  = 10.**np.array([-3,-1,1])    
    for iN,(iNHI,iNH,c,lw,label) in enumerate(iN_tups[0::3]):         
        for iU0,U0 in enumerate(U0s):
            ls = ('-','--',':')[iU0]
            NH,NHI = NHs[iNH],NHIs[iNHI]
            coolings = netCoolingTable(np.array([U0]*len(Ts)),Ts,
                             np.array([NH]*len(Ts)),np.array([NHI]*len(Ts))) 
            pl.plot(Ts,coolings,label=(r'_','$U_0=%s$'%(arilogformat(U0,dollarsign=False)))[iN==0],c=c,lw=lw,ls=ls)
            pl.plot(Ts,-coolings,ls=ls,lw=lw,c=c)
    pl.loglog()
    ax.xaxis.set_major_formatter(arilogformatter)
    ax.xaxis.set_major_locator(ticker.LogLocator(numdecs=15,numticks=15))
    ax.yaxis.set_major_locator(ticker.LogLocator(numdecs=15,numticks=15))
    
    pl.xlabel(r'$T$ [K]')
    pl.ylabel(r'$\Lambda$ [erg s$^{-1}$ cm$^3$]')
    pl.xlim(1000,1e7)    
    pl.ylim(3e-25,1e-21)
    pl.legend(loc='lower right',fontsize=8,frameon=False)
    pl.savefig(figname,bbox_inches='tight')    


def opacity_plot(iN_tups,attentuatedSpectrums_logJs,tables,figname,T = 1e4):
    """
    U -> n_H, U on top
    NH, NHI -> tau_HI, tau_dust
    """
    dustOptOpacityTable, dustIonOpacityTable, gasIonOpacityTable,fHITable = tables
    
    fig = pl.figure(figsize=(fig_width/2.2,4))
    pl.subplots_adjust(hspace=0.01)
    
    L = 1e46*un.erg/un.s
    r = 100*un.pc    
    
    ax = pl.subplot(211)
    ngamma = (L/(mean_hnu*un.erg) * ion_fraction / (4*np.pi*r**2*cons.c)).to('cm**-3')
    U0s  = 10.**np.arange(-5.5,3,.1)
    nHs = ngamma/U0s
    
    ones = np.ones(U0s.shape)
    for itup,(iNHI,iNH,c,lw,label) in enumerate(iN_tups[:]):                 
        NH,NHI = NHs[iNH],NHIs[iNHI]
        fHIs = fHITable(U0s,T*ones,NH*ones,NHI*ones)
        tau_HI = NHI * sigma_HI_one_rydberg
        tau_d = NH * sigma_dust_one_rydberg
        pl.plot(nHs,fHIs,c=c,lw=lw)
    pl.loglog()
    ax.xaxis.set_major_formatter(ticker.NullFormatter())
    ax.xaxis.set_major_locator(ticker.LogLocator(numdecs=15,numticks=15))
    ax.yaxis.set_major_locator(ticker.LogLocator(numdecs=15,numticks=15))
    xls = np.array([100,0.3e7])
    pl.xlim(*xls)    
    pl.ylabel(r'$f_{\rm HI}$') 
    ax.yaxis.set_major_formatter(arilogformatter)                
    pl.ylim(1e-6,3)                
    pl.text(0.95,0.30,r'$T=10^4$ K',transform=ax.transAxes,ha='right')
    pl.text(0.95,0.18,r'$r=100$ pc',transform=ax.transAxes,ha='right')
    pl.text(0.95,0.06,r'$L=10^{46}$ erg s$^{-1}$',transform=ax.transAxes,ha='right')
    ax2 = pl.twiny()
    ax2.set_xscale('log')
    ax2.xaxis.set_major_formatter(arilogformatter)
    ax2.xaxis.set_major_locator(ticker.LogLocator(numdecs=15,numticks=15))
    pl.xlim(*(ngamma.value/xls))
    pl.xlabel(r'$U_0$')
    #pl.plot(U0s,10**-5.5/U0s,c='k',ls=':')
    
    

    ax = pl.subplot(212)
    dnus = np.pad((10.**logEs[2:] - 10.**logEs[:-2])*Rydbergnu/2.,1,mode='edge')
    ind_nu0 = (logEs>=0).nonzero()[0][0]
    base_flux = (L/(4*np.pi*r**2)).to('erg * s**-1 *cm**-2')
    for iN,(iNHI,iNH,c,lw,label) in enumerate(iN_tups):         
        NH,NHI = NHs[iNH],NHIs[iNHI]
        attentuatedFlux  = 10.**attentuatedSpectrums_logJs[iNHI,iNH]*dnus 
        attentuatedFlux /= (10.**attentuatedSpectrums_logJs[0,0]*dnus).sum()
        absorbed_flux_fraction = 0.            
        for iband in range(2): #opt, ion                 
            opacity = (dustOptOpacityTable(U0s,T*ones,NH*ones,NHI*ones)*dust2gasRatio,
                       dustIonOpacityTable(U0s,T*ones,NH*ones,NHI*ones)*dust2gasRatio +
                       gasIonOpacityTable(U0s,T*ones,NH*ones,NHI*ones))[iband] 
            #opacity = (0,gasIonOpacityTable(U0s,T*ones,NH*ones,NHI*ones))[iband]                         
            nu_index = ((0,ind_nu0),(ind_nu0,None))[iband]                
            absorbed_flux_fraction += attentuatedFlux[nu_index[0]:nu_index[1]].sum() * opacity                                            
        accel = (absorbed_flux_fraction * base_flux * un.cm**2/un.g / cons.c).to('cm*s**-2')
        pl.plot(nHs,accel,lw=lw,c=c,label=label)
    pl.loglog()
    ax.xaxis.set_major_formatter(arilogformatter)
    ax.xaxis.set_major_locator(ticker.LogLocator(numdecs=15,numticks=15))
    ax.yaxis.set_major_locator(ticker.LogLocator(numdecs=15,numticks=15))
    ax.yaxis.set_major_formatter(arilogformatter)
    pl.xlabel(r'$n_{\rm H}\ [{\rm cm}^{-3}]$')
    pl.xlim(*xls)
    pl.ylabel(r'$a_{\rm rad}$ [cm s$^{-2}$]')
    pl.ylim(0.7e-5,0.003)
    #dP_to_rho = (2.3*X*cons.k_B*1e4*un.K / cons.m_p).to('cm * s**-2')
    #pl.plot(nHs,dP_to_rho ,ls=':',c='k')
    pl.legend(loc='upper left',handlelength=1.5,frameon=False)
    pl.savefig(figname,bbox_inches='tight')    
            
def cooling_plot(iN_tups,attentuatedSpectrums_logJs,netCoolingTable,figname,T = 1e4):
    """
    U -> n_H, U on top
    NH, NHI -> tau_HI, tau_dust
    """
    
    fig = pl.figure(figsize=(fig_width/2,3.5))
    L = 1e46*un.erg/un.s
    r = 100*un.pc    
    
    ax = pl.subplot(111)
    Ts = 10.**np.arange(1.5,8.5,.01)  
    for iN,(iNHI,iNH,c,lw,label,U0s) in enumerate(iN_tups):         
        for iU0,U0 in enumerate(U0s):
            ls = ('-','--','-.',':')[iU0]
            NH,NHI = NHs[iNH],NHIs[iNHI]
            coolings = netCoolingTable(np.array([U0]*len(Ts)),Ts,
                             np.array([NH]*len(Ts)),np.array([NHI]*len(Ts))) 
            pl.plot(Ts,coolings,label=(r'_','$U=%s$'%(arilogformat(U0,dollarsign=False)))[iN==0],c=c,lw=lw,ls=ls)
            pl.plot(Ts,-coolings,ls=ls,lw=lw,c=c)
    pl.loglog()
    ax.xaxis.set_major_formatter(arilogformatter)
    ax.xaxis.set_major_locator(ticker.LogLocator(numdecs=15,numticks=15))
    ax.yaxis.set_major_locator(ticker.LogLocator(numdecs=15,numticks=15))
    
    pl.xlabel(r'$T$ [K]')
    pl.ylabel(r'$\left|\Lambda\right|$ [erg s$^{-1}$ cm$^3$]')
    pl.text(0.96,0.96,r'unabsorbed spectrum',ha='right',va='top',transform=ax.transAxes)
    pl.xlim(1000,3e6)    
    pl.ylim(1e-25,1e-21)
    pl.legend(loc='lower right',fontsize=8,frameon=False,handlelength=1.8)
    pl.savefig(figname,bbox_inches='tight')    

    
def spectrum(baseSpectrum,spectrum_J_log,iN_tups,figname,cloudy_solution=None):
    fig = pl.figure(figsize=(4,4))
    ax = pl.subplot(111)
    norm = 5e-16 * (LAGN/1e46)**1 * (rAGN/3e21)**-2
    for (iNHI,iNH,c,lw,label) in iN_tups:
        if iNHI==0 and iNH==0:
            if cloudy_solution!=None:
                nus, nuLnus = cloudy_solution['nus'],cloudy_solution['incident_spectrum']/5000
                pl.plot(3e18/nus,nuLnus,label='Cloudy')
            else:
                pl.plot(912*10.**-np.array(baseSpectrum[0]),
                      10**(baseSpectrum[0]+baseSpectrum[1])/norm,
                      ls='-',c=c,lw=lw,zorder=100,label=label)
        else:
            pl.plot(912.*10.**-logEs,
                    10.**(logEs+spectrum_J_log[iNHI,iNH])/norm,
                    c=c,lw=lw,label=label)
    pl.loglog()
    xls = np.array([3e6,0.03])
    pl.xlim(*xls)
    pl.ylim(0.03,1)
    ax.xaxis.set_major_locator(ticker.LogLocator(numticks=15,numdecs=15))
    ax.xaxis.set_major_formatter(arilogformatter)
    ax.yaxis.set_major_formatter(arilogformatter)
    ax.yaxis.set_minor_formatter(ticker.FuncFormatter(lambda x,pos: (nSignificantDigits(x,1,True),'')[np.abs(x-np.array([0.06,0.2,0.3,0.4,0.6])).min()>1e-4]))
    pl.xlabel(r'wavelength $[{\rm \AA}]$')
    pl.ylabel(r'$\nu F_\nu\ [{\rm arbitrary\ units}]$')
    # pl.grid()
    pl.legend(loc='upper right',handlelength=1.5)
    ax2 = pl.twiny()
    nus = cons.c / (xls*un.angstrom)
    ax2.set_xscale('log')
    pl.xlim(*(cons.h*nus).to('eV').value)
    ax2.xaxis.set_major_locator(ticker.LogLocator(numticks=15,numdecs=15))
    ax2.xaxis.set_major_formatter(arilogformatter)
    pl.xlabel(r'energy $[{\rm eV}]$')
    pl.savefig(figname,bbox_inches='tight')    
def opacity_spectrum(figname):
    fig = pl.figure(figsize=(4,3))
    ax = pl.subplot(111)
    pl.plot(Ryd_to_Ang(10.**grain_logEs),10.**grain_logOpacities,label='grains',c='r')
    H_section_pars = generate_cross_sections.read_in_V96_cross_sections(V96_data_file)[0][0]
    H_thresh_energy = H_section_pars[0]
    HI_cross_sections = generate_cross_sections.sigma(10.**grain_logEs*H_thresh_energy, 
                                                               H_section_pars[2:9], 
                                                               0)    
    HI_cross_sections *= (grain_logEs>-dlogE)
    for ifhi,(fhi,fhilabel) in enumerate(((1,r'$1$'),(1e-5,r'$10^{-5}$'))):
        pl.plot(Ryd_to_Ang(10.**grain_logEs),fhi*HI_cross_sections,label=r'HI$\ (f_{\rm HI}=$'+fhilabel+r'$)$',c='k',ls=('-','--')[ifhi])
    
    pl.loglog()
    xls = np.array([1e6,1])
    pl.xlim(*xls)
    pl.ylim(1e-24,2e-17)
    ax.xaxis.set_major_locator(ticker.LogLocator(numticks=15,numdecs=15))
    ax.yaxis.set_major_locator(ticker.LogLocator(numticks=15,numdecs=15))
    ax.xaxis.set_major_formatter(arilogformatter)
    #ax.yaxis.set_major_formatter(arilogformatter)
    #ax.yaxis.set_minor_formatter(ticker.FuncFormatter(lambda x,pos: (nSignificantDigits(x,1,True),'')[np.abs(x-np.array([0.06,0.2,0.3,0.4,0.6])).min()>1e-4]))
    pl.xlabel(r'wavelength $[{\rm \AA}]$')
    pl.ylabel(r'$\sigma_\nu\ [{\rm cm}^2\ {\rm H}^{-1}]$')
    # pl.grid()
    pl.legend(loc='upper left',handlelength=1.5)
    ax2 = pl.twiny()
    nus = cons.c / (xls*un.angstrom)
    ax2.set_xscale('log')
    pl.xlim(*(cons.h*nus).to('eV').value)
    ax2.xaxis.set_major_locator(ticker.LogLocator(numticks=15,numdecs=15))
    ax2.xaxis.set_major_formatter(arilogformatter)
    pl.xlabel(r'energy $[{\rm eV}]$')
    pl.savefig(figname,bbox_inches='tight')    
def weightedOpacity_plot(tables,X=0.73825,dust2gas_ratio = 0.01):
    dustOptOpacityTable, dustIonOpacityTable, gasIonOpacityTable,fHITable = tables
    U0s  = 10.**np.arange(-8,4,.1)    
    ones = np.ones(U0s.shape)
    fig = pl.figure(figsize=(10,5))
    pl.subplots_adjust(wspace=0.3,hspace=0.3)
    
    for irow in range(2):
        for icol in range(2):
            ax = pl.subplot(2,2,irow*2+icol+1)
            for iN,N in enumerate(NHs):
                c = pl.get_cmap('viridis')(iN/len(NHs))
                if icol==0:
                    NHI = NHIs[iN]
                    NH = NHs[0]
                if icol==1:
                    NH = NHs[iN]
                    NHI = NHIs[0]                
                T = 10**4. #change to T_eq
                if irow==0:
                    fHIs = fHITable(U0s,T*ones,NH*ones,NHI*ones)
                    pl.plot(U0s,fHIs,c=c,label=('NH%s = %.1f'%('I '[icol],log(N)),'_')[iN%4!=0])
                if irow==1:
                    opacities = (0. * dustOptOpacityTable(U0s,T*ones,NH*ones,NHI*ones)*dust2gas_ratio +
                                 0. * dustIonOpacityTable(U0s,T*ones,NH*ones,NHI*ones)*dust2gas_ratio +
                                 1. * gasIonOpacityTable(U0s,T*ones,NH*ones,NHI*ones))
                    pl.plot(U0s,opacities*cons.m_p.to('g').value / X,c=c,label='NH%s = %.1f'%('I '[icol],log(N)))
                    if iN==0:
                        pl.plot(U0s,10**-17.6*fHITable(U0s,T*ones,NH*ones,NHI*ones),c='k',ls=':')
            pl.loglog()
            ax.xaxis.set_major_formatter(arilogformatter)
            pl.xlabel(r'$U_0$')
            pl.xlim(0.5e-8,2e5)
            
            if irow==0:
                pl.legend(loc='lower left',fontsize=7,ncol=1)
                pl.ylabel(r'$f_{\rm HI}$') 
                ax.yaxis.set_major_formatter(arilogformatter)                
                pl.plot(U0s,10**-5.5/U0s,c='k',ls=':')
                pl.ylim(0.5e-8,2)                
                #ax.set_yscale('linear')
                #pl.ylim(0.5e-8,1.2)                
                #pl.axhline(1.,c='k',ls=':')
                #pl.xlim(1e-4,1)
            if irow==1:
                pl.ylabel(r'$\tilde{\sigma}_{\rm r.p.}$ [cm$^2$ per H]')
                pl.ylim(1e-27,1e-17)
                
