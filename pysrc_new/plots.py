import numpy as np
from astropy import units as un, constants as cons


import pylab as pl, matplotlib
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
import work_paths as wp;runsDir,rpc_hydro_dir=wp.work_paths()

import multiprocessing, itertools

figdir = rpc_hydro_dir+'figures/'
videoDir = rpc_hydro_dir + 'framesForVideo/'

import BPT
from plot_utils import *
from parameters import *
import emission
from Table import PLUTO_Table
x_cmap = pl.get_cmap('Oranges')
time_cmap    = pl.get_cmap('plasma_r')
luminosity_cmap    = pl.get_cmap('magma_r')
density_cmap = pl.get_cmap('viridis')
pressure_cmap = pl.get_cmap('bone_r')
temperature_cmap = pl.get_cmap('inferno')
intensity_cmap = pl.get_cmap('gray_r')
pratio_cmap = pl.get_cmap('inferno_r')
slantlineprops = slantlinepropsgray = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'0.5'}
slantlinepropsblue  = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'b'}
slantlinepropsgreen  = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'g'}
slantlinepropsmagenta  = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'m'}
slantlinepropsred   = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'r'}
slantlinepropsblack = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'k'}
slantlinepropswhite = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'w'}




def variable_map(snapshot,var,logScale=False,minZrange=1.01,ax=None,cmap=density_cmap,**kwargs):
    vals = snapshot[var].cgs()        
    
    if not logScale:
        zmin = kwargs.get('vmin',np.min(vals))
        zmax = max(minZrange*zmin,kwargs.get('vmax',np.max(vals)))
        pl.pcolormesh(snapshot.x.cgs(),snapshot.r.cgs(),vals,vmin=zmin,vmax=zmax,cmap=cmap)
    else:
        zmin = kwargs.get('vmin',np.min(vals.flatten().take(vals.flatten().nonzero()[0])))
        zmax = max(minZrange*zmin,kwargs.get('vmax',np.max(vals)))
        #print var, zmin, zmax
        pl.pcolormesh(snapshot.x.cgs(),snapshot.r.cgs(),vals,
                   norm=matplotlib.colors.LogNorm(vmin=zmin, vmax=zmax),cmap=cmap)
    pl.ylabel(snapshot.r)
    pl.xlabel(snapshot.x)
    if 'title' in kwargs: tit = kwargs['title']
    else: tit = str(snapshot[var])
    pl.title(tit)        
    pl.ylim(np.min(snapshot.r.cgs()),np.max(snapshot.r.cgs()))
    pl.xlim(np.min(snapshot.x.cgs()),np.max(snapshot.x.cgs()))

    
def variable_map_norm(snapshot,var,logScale=False,minZrange=1.01,ax=None,cmap=density_cmap,**kwargs):
    vals = snapshot[var].cgs()
    vals/=vals.max()
    
    if not logScale:
        zmin = kwargs.get('vmin',np.min(vals))
        zmax = max(minZrange*zmin,kwargs.get('vmax',np.max(vals)))
        pl.pcolormesh(snapshot.x.cgs(),snapshot.r.cgs(),vals,vmin=zmin,vmax=zmax,cmap=cmap)
    else:
        zmin = kwargs.get('vmin',np.min(vals.flatten().take(vals.flatten().nonzero()[0])))
        zmax = max(minZrange*zmin,kwargs.get('vmax',np.max(vals)))
        #print var, zmin, zmax
        pl.pcolormesh(snapshot.x.cgs(),snapshot.r.cgs(),vals,
                   norm=matplotlib.colors.LogNorm(vmin=zmin, vmax=zmax),cmap=cmap)
    pl.ylabel(snapshot.r)
    pl.xlabel(snapshot.x)
    if 'title' in kwargs: tit = kwargs['title']
    else: tit = str(snapshot[var])
    pl.title(tit)        
    pl.ylim(np.min(snapshot.r.cgs()),np.max(snapshot.r.cgs()))
    pl.xlim(np.min(snapshot.x.cgs()),np.max(snapshot.x.cgs()))

def sim_evolution(runs,iSnapshots,var,rng,cmap,layout=0,**kwargs):
    fig = pl.figure(figsize=(fig_width*(1.1**-1,0.5)[layout],5.75*len(runs)/2))
    gss = gridspec.GridSpec(len(runs),len(iSnapshots))
    gss.update(left=0.1,right=0.98,bottom=0.07,top=0.88,hspace=0.2)
    
    for irun,run in enumerate(runs):        
        r0 = run.r0/(un.pc/un.cm).to('')        
        logScale = not ( (var[0]=='L') or var in ('absorbedFrac','F2F0','v_r'))
        axs = []
        for iiSnapshot,iSnapshot in enumerate(iSnapshots):
            ax = pl.subplot(gss[irun,iiSnapshot])
            axs.append(ax)
            vmin,vmax = rng
            
            variable_map(run[iSnapshot], var,logScale=logScale, title='',vmin=vmin,vmax=vmax,cmap=cmap,**kwargs)            
            ax.yaxis.set_major_locator(ticker.MultipleLocator(1))
            ax.xaxis.set_major_locator(ticker.MultipleLocator(0.5))
            
            if ax.is_first_col():                  
                pl.ylabel(r'$r\ [{\rm pc}]$')
            else:
                ax.yaxis.set_major_formatter(matplotlib.ticker.NullFormatter())                    
                pl.ylabel(r'')                
            if ax.is_first_row():
                pl.title(run[iSnapshot].t.fullStr(withvar=True))
            if not ax.is_last_row():                        
                pl.xlabel('')
                ax.xaxis.set_major_formatter(matplotlib.ticker.NullFormatter())
            pl.ylim(100,103)
            pl.xlim(0,1.49)
            if ax.is_first_col():
                pl.text(0.025,0.935,
                        r'$\log\ L=%.0f$'%log(run.LAGN),
                        transform=ax.transAxes,color='w') 
                pl.text(0.025,0.85,
                        r'$p_{\rm rad} / p^0_{\rm gas} = %s$'%nSignificantDigits(run.Prad_to_Pgas(),1,True),
                        transform=ax.transAxes,color='w') 
            
        cb = pl.colorbar(ax=axs,orientation='vertical',fraction=0.1,pad=0.02,
                         ticks=ticker.LogLocator(numdecs=6, numticks=6),format=arilogformatter)
        cb.set_label(str(run[iSnapshot][var])) #fontsize=14
        
        


def skewers(runs,xaxisvar, variables,rngs, iTs, ix=0,logx=False,logy=True,xls=None,**kwargs):
    pl.figure(figsize=(fig_width/1.1,3))
    pl.subplots_adjust(hspace=0.1,wspace=0.15)
    for ivar,var in enumerate(variables): 
        for irun,run in enumerate(runs):
            ax = pl.subplot(len(variables),len(runs),len(runs)*ivar+irun+1)
            for iiT,iT in enumerate(iTs):                
                xs = run[iT][xaxisvar].cgs()
                if run[iT][xaxisvar].dimension==[1,2]: xs = xs[:,ix]
                c = time_cmap(iiT*1./(len(iTs)-1))
                pl.plot(xs, run[iT][var].cgs()[:,ix],label=run[iT].t.fullStr(withvar=False),c=c)
                front = (run[iT].fHII.cgs()[:,ix]<0.5).nonzero()[0]
                if iT>0 and len(front):
                    x = xs[front[0]]
                    pl.axvline(x,c=c,ls=':')
            pl.ylabel(run[iTs[0]][var])
            pl.ylim(rngs[ivar])
            if xls!=None:
                pl.xlim(*xls)
                print(xls)
            if logx:
                ax.set_xscale('log')
                ax.xaxis.set_major_formatter(arilogformatter)
            if logy:
                ax.set_yscale('log')
                ax.yaxis.set_major_formatter(arilogformatter)
            if ax.is_last_row():
                pl.xlabel(run[iTs[0]][xaxisvar])            
            else:
                ax.xaxis.set_major_formatter(matplotlib.ticker.NullFormatter())
            if ax.is_first_row():
                pl.title(r'$\log\ L=%.0f,\ \ p_{\rm rad} / p^0_{\rm gas} = %s$'%(
                    log(run.LAGN),nSignificantDigits(run.Prad_to_Pgas(),1,True)),
                         fontsize=14)            
            if ax.is_first_col() and ax.is_first_row():
                pl.legend(loc='upper right',handlelength=0.5,columnspacing=0.7,handletextpad=0.35,ncol=1)            
                pl.text(0.05,0.925,r'$x=%.1f$ pc'%(run[iT].x.cgs()[ix]),transform=ax.transAxes)
            if ax.is_last_col():
                ax.yaxis.set_label_position('right')
                ax.yaxis.set_ticks_position('right')
                pl.annotate("ionization front",(x+0.15,1e5),(x+0.3,1e5),arrowprops=slantlinepropsblack,va='center')
            ax.yaxis.set_ticks_position('both')				                
def comparison_to_static_solution_by_time(run,iTs,ix,static_solution_fn,**kwargs):
    pl.figure(figsize=(fig_width,4))    
    r0 = run.r0/(un.pc/un.cm).to('')        
    tau_rng = 0.01,100
    pl.subplots_adjust(wspace=0.55,left=0.05,right=0.98)
    
    
    for ivar,var in enumerate(('nH','nHT')):        
        ax = pl.subplot(1,2,ivar+1)
        rng= np.array(((10,1.5e5),(3e5,1.2e9))[ivar])
        for iiT,iT in enumerate(iTs):
            c = time_cmap(iiT*1./(len(iTs)-1))
            label=run[iT].t.fullStr(withvar=False)
            y_property = getattr(run[iT],var)
            if iT!=0:
                taus = run[iT].tau_HI.cgs()[:,ix] + run[iT].tau_dust.cgs()[:,ix]
                sr,er = np.searchsorted(taus, tau_rng)
                if er==run[0].r.shape()[0]: er-=1   
                taus = taus[sr:er]                
                ys = y_property.cgs()[sr:er,ix]        
                pl.plot(taus,ys,c=c,label=label)
            else:
                pl.axhline(y_property.cgs()[:,ix].max(),c=c,label=label)
        else: #columns not defined yet
            sr,er = 0,run[0].r.shape()[0]//2
            taus = 10.**np.linspace(log(tau_rng[0]),log(tau_rng[1]),er)
                
    
        pl.loglog()
        pl.xlim(*tau_rng)
        pl.xlabel(r'optical depth at 1 Ryd')
        pl.ylim(*rng)          
        ax.xaxis.set_major_formatter(arilogformatter)
        ax.xaxis.set_major_locator(ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
        ax.xaxis.set_minor_locator(ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
        pl.ylabel(y_property)
        ax.yaxis.set_major_formatter(arilogformatter)        
        
        if static_solution_fn!=None:
            static_solution = np.load(static_solution_fn)
            if var=='nH': y_static = run.n_gamma().value/static_solution['U0s']
            if var=='nHT': 
                y_static = run.n_gamma().value/static_solution['U0s']*static_solution['Ts']             
                nHT_rad = (run.Prad()/(2.3*cons.k_B)).to('cm**-3*K')                
            pl.plot(static_solution['NHs']*sigma_dust_one_rydberg+static_solution['NHIs']*sigma_HI_one_rydberg,
                    y_static,c='.75',lw=6,zorder=-10,alpha=0.5)
            
        if var=='nH':
            pl.text(30,7e4,r'RPC',ha='right')
            pl.plot([30,50],[6e4,3.5e4],lw=0.5,c='.5')
            pl.text(0.05,0.94,r'$\log L=%.0f$'%(log(run.LAGN)),transform=ax.transAxes)
            pl.text(0.05,0.88,r'$x=%.1f$ pc'%(run[iT].x.cgs()[ix]),transform=ax.transAxes)            
            pl.legend(ncol=1,fontsize=11,frameon=False,loc='lower right',handlelength=1,columnspacing=1,handletextpad=0.5)
            ax2=pl.twinx()
            ax2.set_yscale('log')    
            ax2.yaxis.set_major_formatter(arilogformatter)
            ax2.set_ylim(*((run.n_gamma().value/rng)))
            pl.ylabel(r'ionization parameter $U$')            
        if var=='nHT':
            #pl.text(0.8,3e8,r'RPC',ha='right')
            #pl.text(0.8,3e8,r'$\frac{L}{4\pi r^2 c}\left(1-e^{-\tilde\tau}\right)$',ha='right')
            #pl.plot([0.4,1.1],[3e8,1.85e8],lw=0.5,c='.5')
            ax3=pl.twinx()
            ax3.set_yscale('log')    
            ax3.yaxis.set_major_formatter(arilogformatter)
            ax3.set_ylim(*((np.array(rng)/nHT_rad.value)))
            pl.ylabel(r'$P_{\rm gas} / \frac{L}{4\pi r_0^2 c}$')            
                                 


def comparison_to_static_solution_by_time_vs_NH(run,iTs,var,rng,ix,static_solution_fn,**kwargs):
    pl.figure(figsize=(fig_width/2.1,4))    
    r0 = run.r0/(un.pc/un.cm).to('')        
    
    
    ax = pl.subplot(111)
    static_solution = np.load(static_solution_fn)
    pl.plot(static_solution['NHs'],
            100*10**lng/static_solution['U0s'],c='.75',lw=6,zorder=-10,alpha=0.5,
            label=r'$p_{\rm gas} = p_{\rm rad}\left(1-e^{-\tilde\tau}\right)$')

    for iiT,iT in enumerate(iTs):
        NHs = run[iT].NH.cgs()[:,ix]
        ys = run[iT].nH.cgs()[:,ix]        
        label=run[iT].t.fullStr(withvar=False)
        c = time_cmap(iiT*1./(len(iTs)-1))
        pl.plot(NHs,ys,c=c,label=label)

    pl.loglog()
    pl.xlim(1e17,1e23)
    pl.xlabel(r'hydrogen column from domain edge $N_{\rm H}\ [{\rm cm}^{-2}]$')
    yls = np.array([1,3e6])
    pl.ylim(*yls)          
    ax.xaxis.set_major_formatter(arilogformatter)
    ax.xaxis.set_major_locator(ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
    ax.xaxis.set_minor_locator(ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
    pl.ylabel(r'$n_{\rm H}\ [{\rm cm}^{-3}]$')        
    ax.yaxis.set_major_formatter(arilogformatter)        
    

    pl.text(0.5,0.08,r'$\log L=%.0f$'%(log(run.LAGN)),transform=ax.transAxes)
    pl.text(0.5,0.02,r'$x=%.1f$ pc'%(run[iT].x.cgs()[ix]),transform=ax.transAxes)
    pl.legend(ncol=1,fontsize=11,frameon=False,loc='upper left',handlelength=1,columnspacing=1,handletextpad=0.5)
    ax2=pl.twinx()
    ax2.set_yscale('log')    
    ax2.yaxis.set_major_formatter(arilogformatter)
    ax2.set_ylim(*((100*10**lng/yls)))
    pl.ylabel(r'ionization parameter $U$')

def comparison_to_static_solution_by_radius(runs1,runs2,iTfunc,var,rng,ix,static_solution_fn,**kwargs):
    fig=pl.figure(figsize=(fig_width/1.1,7))    
    pl.subplots_adjust(wspace=0.2,top=0.92)
    tau_rng = 0.01,10
    axs = [None,None]
    for iPanel in range(2):
        ax = axs[iPanel] = pl.subplot(1,2,iPanel+1)
        runs = (runs1,runs2)[iPanel]
        if len(runs)==0: continue
        for irun,run in enumerate(runs):
            iT = iTfunc(iPanel,irun)
            if len(run)<iT: continue
            taus = run[iT].tau_HI.cgs()[:,ix] + run[iT].tau_dust.cgs()[:,ix]
            sr,er = np.searchsorted(taus, tau_rng)
            if er==run[iT].r.shape()[0]: er-=1             
            run_lng = log(run.n_gamma().value)
            ys = 10.**run_lng / run[iT].nH.cgs()[sr:er+1,ix]
            if iPanel==0:
                label=r'$\log\ L=%.1f$'%(log(run.LAGN))
            if iPanel==1:
                label=r'$r_0 = %s\ {\rm kpc}$'%nSignificantDigits(run.r0/3e21,1,True)
            logXi = log(run.Prad_to_Pgas())
            logXi_normed = (logXi-(-2.5))/(1.8-(-2.5))
            c = pratio_cmap(logXi_normed)
            print('%d %.1f %.1f %s'%(iPanel,logXi,logXi_normed, run.name))
            pl.plot(taus[sr:er+1],ys,c=c,label=label)
    
        pl.loglog()
        pl.xlabel(r'optical depth at 1 Ryd')
        yls = np.array([100,2e-3])
        pl.ylim(*yls)          
        ax.xaxis.set_major_formatter(arilogformatter)
        ax.xaxis.set_major_locator(ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
        ax.xaxis.set_minor_locator(ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
        if iPanel==1:
            ax.yaxis.set_label_position('right')
            ax.yaxis.set_ticks_position('right')
            ax.yaxis.set_ticks_position('both')
        pl.ylabel(r'ionization parameter $U$')
        ax.yaxis.set_major_formatter(arilogformatter)        
        
        static_solution = np.load(static_solution_fn)
        pl.plot(static_solution['NHs']*sigma_dust_one_rydberg+static_solution['NHIs']*sigma_HI_one_rydberg,
                static_solution['U0s'],c='.75',lw=6,zorder=-10,alpha=0.5,
                label=r'RPC')
        pl.legend(ncol=1,fontsize=11,frameon=False,loc='lower right',handlelength=1,columnspacing=1,handletextpad=0.5)
        pl.xlim(*tau_rng)
    
    pl.text(0.5,0.95,r'$x=%.1f$ pc, $t=$ %s'%(run[iT].x.cgs()[ix],run[iT].t.fullStr(withvar=False)),
            transform=fig.transFigure,ha='center')    
    cbar_label = r'$p_{\rm rad} / p^0_{\rm gas}$'
    sm = matplotlib.cm.ScalarMappable(norm=matplotlib.colors.LogNorm(vmin=1e-2, vmax=200),cmap=pratio_cmap)
    sm.set_array(10.**np.arange(-3,2.5))
    cb = pl.colorbar(sm,ax=axs,label=cbar_label, 
                     orientation='horizontal',pad=0.1,shrink=0.5,fraction=0.25)#,format=arilogformatter)

    
def comparison_to_static_solution_by_L(runs,iTfunc,var,rng,ix,static_solution_fn,**kwargs):
    fig=pl.figure(figsize=(fig_width/2.2,4.5))    
    pl.subplots_adjust(wspace=0.2,top=0.92)
    tau_rng = 0.01,100
    ax = pl.subplot(111)
    for irun,run in enumerate(runs):
        iT = iTfunc(irun)
        taus = run[iT].tau_HI.cgs()[:,ix] + run[iT].tau_dust.cgs()[:,ix]
        sr,er = np.searchsorted(taus, tau_rng)
        if er==run[iT].r.shape()[0]: er-=1             
        ys = run.n_gamma()/run[iT].nH.cgs()[sr:er+1,ix]
        logXi = log(run.Prad_to_Pgas())
        logXi_normed = (logXi-(-1.8))/(1.8-(-1.8))
        c = pratio_cmap(logXi_normed)
        print('%.1f %.1f %s'%(logXi,logXi_normed, run.name))        
        label=r'$%.1f,\ %s$'%(log(run.LAGN),nSignificantDigits(10.**logXi,1,True))
        pl.plot(taus[sr:er+1],ys,c=c,label=label)
    pl.text(0.92,0.95,r'$\log L\ \ \frac{P_{\rm rad}}{P_{\rm gas}^0}$',transform=ax.transAxes,
            ha='right',va='top',fontsize=14)
    pl.loglog()
    pl.xlabel(r'optical depth at 1 Ryd')
    yls = np.array([1e-3,100])
    pl.ylim(*yls)          
    ax.xaxis.set_major_formatter(arilogformatter)
    ax.xaxis.set_major_locator(ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
    ax.xaxis.set_minor_locator(ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
    pl.ylabel(r'ionization parameter $U$')
    ax.yaxis.set_major_formatter(arilogformatter)        

    static_solution = np.load(static_solution_fn)
    pl.plot(static_solution['NHs']*sigma_dust_one_rydberg+static_solution['NHIs']*sigma_HI_one_rydberg,
            static_solution['U0s'],c='.75',lw=6,zorder=-10,alpha=0.5,
            label=r'RPC')
    pl.legend(ncol=1,fontsize=12,frameon=False,loc=(0.6,0.45),handlelength=1,columnspacing=1,handletextpad=0.5)
    pl.xlim(*tau_rng)
    pl.text(0.05,0.05,r'$x=%.1f$ pc'%(run[iT].x.cgs()[ix]),transform=ax.transAxes)    
    pl.text(0.05,0.1,r'$t=$ %s'%(run[iT].t.fullStr(withvar=False)),transform=ax.transAxes)
    pl.text(0.05,0.15,r'$r=100$ pc'%(run[iT].x.cgs()[ix]),transform=ax.transAxes)    

def comparison_to_static_solution_by_res(runs,iTfunc,var,rng,ix_base,static_solution_fn,**kwargs):
    fig=pl.figure(figsize=(fig_width/2.2,3.5))    
    pl.subplots_adjust(wspace=0.2,top=0.92)
    tau_rng = 0.01,100
    ax = pl.subplot(111)
    for irun,run in enumerate(runs):        
        res = int(run.name.split('_')[1][1:])
        ix = ix_base * (res//1024)
        iT = iTfunc(irun)
        taus = run[iT].tau_HI.cgs()[:,ix] + run[iT].tau_dust.cgs()[:,ix]
        sr,er = np.searchsorted(taus, tau_rng)
        if er==run[iT].r.shape()[0]: er-=1             
        ys = run.n_gamma()/run[iT].nH.cgs()[sr:er+1,ix]
        c = pratio_cmap(irun/(len(runs)-1.))
        label=r'$\delta r=%s\ {\rm pc}$'%nSignificantDigits(3./res,1,False)
        pl.plot(taus[sr:er+1],ys,c=c,label=label)
    pl.loglog()
    pl.xlabel(r'optical depth at 1 Ryd')
    yls = np.array([1e-2,100])
    pl.ylim(*yls)          
    ax.xaxis.set_major_formatter(arilogformatter)
    ax.xaxis.set_major_locator(ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
    ax.xaxis.set_minor_locator(ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
    pl.ylabel(r'ionization parameter $U$')
    ax.yaxis.set_major_formatter(arilogformatter)        

    static_solution = np.load(static_solution_fn)
    pl.plot(static_solution['NHs']*sigma_dust_one_rydberg+static_solution['NHIs']*sigma_HI_one_rydberg,
            static_solution['U0s'],c='.75',lw=6,zorder=-10,alpha=0.5,
            label=r'RPC')
    pl.legend(ncol=1,fontsize=12,frameon=False,loc='upper right',handlelength=1,columnspacing=1,handletextpad=0.5)
    pl.xlim(*tau_rng)
    pl.text(0.05,0.05,r'$x=%.1f$ pc'%(run[iT].x.cgs()[ix]),transform=ax.transAxes)    
    pl.text(0.05,0.1,r'$t=$ %s'%(run[iT].t.fullStr(withvar=False)),transform=ax.transAxes)
    pl.text(0.05,0.15,r'$r=100$ pc'%(run[iT].x.cgs()[ix]),transform=ax.transAxes)    
    pl.text(0.05,0.2,r'$L=10^{46}$ erg s$^{-1}$'%(run[iT].x.cgs()[ix]),transform=ax.transAxes)    

    
    
def comparison_to_static_solution_by_Xi(runs,iTfunc,var,rng,ix,static_solution_fn,**kwargs):
    fig=pl.figure(figsize=(fig_width/2,5))    
    tau_rng = 0.01,10
    ax = pl.subplot(111)
    for irun,run in enumerate(runs):
        iT = iTfunc(irun)
        taus = run[iT].tau_HI.cgs()[:,ix] + run[iT].tau_dust.cgs()[:,ix]
        sr,er = np.searchsorted(taus, tau_rng)
        if er==run[iT].r.shape()[0]: er-=1             
        ys = run.n_gamma().value / run[iT].nH.cgs()[sr:er+1,ix]
        n0 = run[0].nH.cgs().max()
        logXi = log(run.Prad_to_Pgas())
        logXi_normed = (logXi-(-2.5))/(1.8-(-2.5))
        c = pratio_cmap(logXi_normed)
        print('%.1f %.1f %s'%(logXi,logXi_normed, run.name))
        label = r'%s'%nSignificantDigits(10.**logXi,1,True)
        #r'$p_{\rm rad} / p^0_{\rm gas}$'

        pl.plot(taus[sr:er+1],ys,c=c,label=label)

    pl.loglog()
    pl.xlabel(r'optical depth at 1 Ryd')
    yls = np.array([100,2e-3])
    pl.ylim(*yls)          
    ax.xaxis.set_major_formatter(arilogformatter)
    ax.xaxis.set_major_locator(ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
    ax.xaxis.set_minor_locator(ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
    pl.ylabel(r'ionization parameter $U$')
    ax.yaxis.set_major_formatter(arilogformatter)        

    static_solution = np.load(static_solution_fn)
    pl.plot(static_solution['NHs']*sigma_dust_one_rydberg+static_solution['NHIs']*sigma_HI_one_rydberg,
            static_solution['U0s'],c='.75',lw=6,zorder=-10,alpha=0.5,
            label=r'RPC')
    pl.legend(ncol=1,fontsize=11,frameon=False,loc='lower right',handlelength=1,columnspacing=1,handletextpad=0.5)
    pl.xlim(*tau_rng)
    
    pl.text(0.5,0.95,r'$x=%.1f$ pc, $t=$ %s'%(run[iT].x.cgs()[ix],run[iT].t.fullStr(withvar=False)),
            transform=fig.transFigure,ha='center')    
    
def comparison_to_static_solution_by_rho(runs,iTfunc,var,rng,ix,static_solution_fn,**kwargs):
    fig=pl.figure(figsize=(fig_width/1.1,7))    
    pl.subplots_adjust(wspace=0.2,top=0.92)
    tau_rng = 0.01,10
    ax = pl.subplot(111)
    for irun,run in enumerate(runs):
        iT = iTfunc(irun)
        taus = run[iT].tau_HI.cgs()[:,ix] + run[iT].tau_dust.cgs()[:,ix]
        sr,er = np.searchsorted(taus, tau_rng)
        if er==run[iT].r.shape()[0]: er-=1             
        run_lng = log(run.n_gamma().value)
        ys = 10.**run_lng / run[iT].nH.cgs()[sr:er+1,ix]
        label=r'$n_0 = %s\ {\rm cm}^{-3}$'%nSignificantDigits(run[0].nH.cgs().max(),1,True)
        logXi = log(run.Prad_to_Pgas())
        logXi_normed = (logXi-(-2.5))/(1.8-(-2.5))
        c = pratio_cmap(logXi_normed)
        print('%.1f %.1f %s'%(logXi,logXi_normed, run.name))
        pl.plot(taus[sr:er+1],ys,c=c,label=label)
    
    pl.loglog()
    pl.xlim(*tau_rng)
    pl.xlabel(r'optical depth at 1 Ryd')
    yls = np.array([300,3e-4])
    pl.ylim(*yls)          
    ax.xaxis.set_major_formatter(arilogformatter)
    ax.xaxis.set_major_locator(ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
    ax.xaxis.set_minor_locator(ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
    pl.ylabel(r'ionization parameter $U$')
    ax.yaxis.set_major_formatter(arilogformatter)        
        
    static_solution = np.load(static_solution_fn)
    pl.plot(static_solution['NHs']*sigma_dust_one_rydberg+static_solution['NHIs']*sigma_HI_one_rydberg,
            static_solution['U0s'],c='.75',lw=6,zorder=-10,alpha=0.5,
            label=r'$p_{\rm gas} = \frac{L}{4\pi r^2 c}\left(1-e^{-\tilde\tau}\right)$')
    pl.legend(ncol=1,fontsize=11,frameon=False,loc='lower right',handlelength=1,columnspacing=1,handletextpad=0.5)
    
    pl.text(0.5,0.95,r'$x=%.1f$ pc, $t=$ %s'%(run[iT].x.cgs()[ix],run[iT].t.fullStr(withvar=False)),transform=fig.transFigure,ha='center')    
    cbar_label = r'$p_{\rm rad} / p^0_{\rm gas}$'
    sm = matplotlib.cm.ScalarMappable(norm=matplotlib.colors.LogNorm(vmin=1e-3, vmax=200),cmap=pratio_cmap)
    sm.set_array(10.**np.arange(-3,2.5))

    cb = pl.colorbar(sm,ax=ax,label=cbar_label,orientation='horizontal', 
                     pad=0.1,shrink=0.5,fraction=0.25)#,format=arilogformatter)

    
def comparison_to_static_solution_by_x(run,iT,var,rng,ixs,static_solution_fn,dx=25,**kwargs):
    fig = pl.figure(figsize=(fig_width/1.1,4))    
    pl.subplots_adjust(wspace=0.55,hspace=0.3)
    r0 = run.r0/(un.pc/un.cm).to('')        
    tau_rng = 0.01,100
    
    gss1 = gridspec.GridSpec(1,2)
    gss2 = gridspec.GridSpec(1,4)
    gss1.update(left=0.1,right=0.98,bottom=0.07,top=0.88,wspace=0.75)
    gss2.update(left=0.05,right=0.98,bottom=0.07,top=0.88,wspace=0.3)
    
    ax1 = ax = fig.add_subplot(gss1[0])
    
    for iixs,ix in enumerate(ixs):              
        taus = run[iT].tau_HI.cgs()[:,ix] + run[iT].tau_dust.cgs()[:,ix]
        sr,er = np.searchsorted(taus, tau_rng)
        if er==run[0].r.shape()[0]: er-=1             
        rrng = run[iT].r.cgs()[np.array([sr,er])]    
        ys = run[iT].nH.cgs()[sr:er+1,ix]
        
        label=r'$%.1f$ pc'%run[iT].x.cgs()[ix]
        if iixs==0: label = r'$x=$'+label
            
        c = x_cmap(1 - ix*1./run[iT].x.shape()[0])
        pl.plot(taus[sr:er+1],ys,c=c,label=label)

    static_solution = np.load(static_solution_fn)
    pl.plot(static_solution['NHs']*sigma_dust_one_rydberg+static_solution['NHIs']*sigma_HI_one_rydberg,
            100*10**lng/static_solution['U0s'],c='.75',lw=6,zorder=-10,alpha=0.5,
            label=r'RPC')
#            label=r'$p_{\rm gas} = \frac{L}{4\pi r^2 c}\left(1-e^{-\tilde\tau}\right)$')

    
    pl.loglog()
    pl.xlim(*tau_rng)
    pl.xlabel(r'optical depth at 1 Ryd')
    yls = np.array([10,1.5e5])
    
    ax.xaxis.set_major_formatter(arilogformatter)
    ax.xaxis.set_major_locator(matplotlib.ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
    ax.xaxis.set_minor_locator(matplotlib.ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
    ax.yaxis.set_ticks_position('both')

    #pl.ylabel('hydrogen density [cm$^{-3}$]')        
    pl.ylabel(r'$n_{\rm H}\ [{\rm cm}^{-3}]$')        
    ax.yaxis.set_major_formatter(arilogformatter)            
    
    
    pl.text(0.5,0.95,r'$\log L=%.0f,\ \ $ %s'%(log(run.LAGN),run[iT].t.fullStr(withvar=True)),transform=fig.transFigure,ha='center')
    pl.legend(ncol=1,fontsize=11,frameon=False,loc='upper left',handlelength=1,columnspacing=1,handletextpad=0.5)
    
    pl.ylim(*yls) 
    print(yls)
    print(pl.ylim())
    #ax2=pl.twinx()
    #ax2.set_yscale('log')    
    #ax2.yaxis.set_major_formatter(arilogformatter)
    #ax2.set_ylim(*((run.n_gamma()/(yls*un.cm**-3)).to('').value))
    #pl.ylabel('ionization parameter')

    
    ax = fig.add_subplot(gss2[2])
    variable_map(run[iT], 'nH',logScale=True, title='',vmin=10,vmax=1e5,cmap=density_cmap)
    cb = pl.colorbar(ax=ax,orientation='horizontal',fraction=0.04,pad=0.2,
                     ticks=ticker.LogLocator(numdecs=6, numticks=6),
                     format=arilogformatter)
    #cb.set_label('hydrogen density [cm$^{-3}$]')
    cb.set_label(r'$n_{\rm H}\ [{\rm cm}^{-3}]$')        
    
    qd = 32
    pl.quiver(run[iT].x.cgs()[qd//2::qd],
              run[iT].r.cgs()[qd//2::qd*2],
              run[iT].v_theta.cgs()[qd//2::qd*2,qd//2::qd],
              run[iT].v_r.cgs()[qd//2::qd*2,qd//2::qd],color='k')
    
    for iixs,ix in enumerate(ixs):              
        sx,mx,ex = np.array([ix-dx,ix,ix+dx])
        xrng = run[iT].x.cgs()[np.array([sx,ex])]        
        taus = run[iT].tau.cgs()[:,ix]
        sr,er = np.searchsorted(taus, tau_rng)
        if er==run[0].r.shape()[0]: er-=1             
        rrng = run[iT].r.cgs()[np.array([sr,er])]
        c = x_cmap(1 - ix*1./run[iT].x.shape()[0])
        #ax.add_patch(matplotlib.patches.Rectangle((xrng[0],rrng[0]), (xrng[1]-xrng[0]), (rrng[1]-rrng[0]),fc='none',ec=c,lw=1))
        #pl.arrow(run[iT].x.cgs()[mx],103.2,0,-0.1,head_width=0.05,
                 #clip_on=False,color=c)
        pl.text(run[iT].x.cgs()[mx],103.05,'+',clip_on=False,color=c,fontsize=8)
    ax.yaxis.set_major_locator(ticker.MultipleLocator(1))
    ax.xaxis.set_major_locator(ticker.MultipleLocator(0.5))
    ax.yaxis.set_ticks_position('both')
    
    pl.ylim(100,103)
    pl.xlim(0,1.5)
    
    ax = fig.add_subplot(gss2[3])
    variable_map_norm(run[iT], 'LHa_noExtinct',logScale=True, title='',cmap=intensity_cmap,vmin=3e-4,vmax=1)    
    cb = pl.colorbar(ax=ax,orientation='horizontal',fraction=0.04,pad=0.2,
                     ticks=ticker.LogLocator(numdecs=6, numticks=6),format=arilogformatter)
    cb.set_label(r'normalized H$\alpha$'+'\nluminosity')
    
    
    for iixs,ix in enumerate(ixs):              
        sx,mx,ex = np.array([ix-dx,ix,ix+dx])
        xrng = run[iT].x.cgs()[np.array([sx,ex])]        
        taus = run[iT].tau.cgs()[:,ix]
        sr,er = np.searchsorted(taus, tau_rng)
        if er==run[0].r.shape()[0]: er-=1             
        rrng = run[iT].r.cgs()[np.array([sr,er])]
        c = x_cmap(1 - ix*1./run[iT].x.shape()[0])
        pl.text(run[iT].x.cgs()[mx],103.05,'+',clip_on=False,color=c)
        
    pl.ylabel('')
    ax.yaxis.set_ticks_position('right')
    ax.yaxis.set_ticks_position('both')
    ax.yaxis.set_major_locator(ticker.MultipleLocator(1))
    ax.xaxis.set_major_locator(ticker.MultipleLocator(0.5))
    ax.yaxis.set_major_formatter(ticker.NullFormatter())
    
    pl.ylim(100,103)
    pl.xlim(0,1.5)
    
    ax1.set_ylim(*yls)
    
    #LHa_skewers = run[iT]['LHa'].cgs().sum(axis=0)
    #pl.figure()
    #pl.plot(run[iT].x.cgs(),LHa_skewers.cumsum())
    
def comparison_to_static_temperature_solution(run,iT,ix,static_solution_fn,**kwargs):
    pl.figure(figsize=(fig_width/2.1,3))    
    tau_rng = 0.01,1e6
        
    ax = pl.subplot(111)
    taus = run[iT].tau.cgs()[:,ix]
    sr,er = np.searchsorted(taus, tau_rng)
    if er==run[0].r.shape()[0]: er-=1             
    ys = run[iT].T.cgs()[sr:er,ix]
        
    label='sim %s'%run[iT].t.fullStr(withvar=True)
    pl.plot(taus[sr:er],ys,c='k',label=label)

    pl.loglog()
    pl.xlim(*tau_rng)
    pl.xlabel(r'spectrum-averaged optical depth')
    yls = np.array([3000,1e6])
    pl.ylim(*yls)          
    ax.xaxis.set_major_formatter(arilogformatter)
    ax.xaxis.set_major_locator(ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
    ax.xaxis.set_minor_locator(ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
    pl.ylabel('temperature [K]')        
    ax.yaxis.set_major_formatter(arilogformatter)        
    pl.text(0.0125,1e5,r'$x = %.1f$ pc'%run[iT].x.cgs()[ix],fontsize=8)
    static_solution = np.load(static_solution_fn)
    pl.plot(static_solution['taus'],static_solution['Ts'],c='.75',lw=6,zorder=-10,alpha=0.5,label='static')        
    pl.legend(fontsize=10,frameon=False)

def L_dependence_figs(runs,res,T=1e4*un.K,dlogn=0.1,iT0=100,d=1,save=True,norm_by_Lbol=False):
    bins=np.arange(-10,15,dlogn)
    pl.figure(figsize=(fig_width/2,4))
    pl.subplots_adjust(hspace=0.4)
    ax = pl.subplot(111)    
    iruns = np.argsort(np.array([run.LAGN for run in runs]))
    for irun in iruns:
        run = runs[irun]
        c = time_cmap((log(run.LAGN)-42)/5)
        lng = log(run.n_gamma().value)
        xs = lng - bins[:-1]+dlogn/2.
        if norm_by_Lbol: norm = run.LAGN
        else:            norm = res[irun][0].sum()
        ys = res[irun][0] / norm
        label = r'$L=10^{%.0f}$'%log(run.LAGN)+'\n'+r'$\Xi=%s$'%nSignificantDigits(run.LAGN/1e46*20,1,True)
        pl.step(10.**xs,ys,color=c,label=label)
        if irun<3:
            pl.text(10.**xs[ys.argmax()], ys.max()+0.01, label,fontsize=8,ha='center')
    pl.text(0.04,0.35,r'$L=10^{45}-10^{46.5}$'+'\n'+r'$\Xi=2-60$',fontsize=8,ha='center')
    pl.text(0.00125,0.25,r'$10^{44.5},$'+'\n'+r'$0.6$',fontsize=8,ha='center')
    pl.annotate('',(0.005,0.2375),(0.0023,0.26),arrowprops=slantlinepropsblack)
        
    pl.xlabel('ionization parameter')
    pl.ylabel(r'emission measure distribution')
    pl.semilogx()
    pl.xlim(10.**-5,10.**0.5)
    ax.xaxis.set_major_formatter(arilogformatter)
    if not norm_by_Lbol: pl.ylim(0,0.5)
    U_RPC = (2.3*cons.k_B.to('erg/K')*T*ion_fraction/(mean_hnu*un.erg)).to('')
    pl.axvline(U_RPC,c='.7',ls=':')
    #pl.legend(loc='upper left',fontsize=8,ncol=2)
    pl.annotate(r'$2.3kT/\langle h\nu\rangle$', (U_RPC*10**0.05,0.45),(U_RPC*10**0.25,0.45),arrowprops=slantlinepropsblack,va='center')
    pl.title(r'$t=%.0f\ {\rm kyr}$'%iT0) 


def BPTplot_byL(runs):
    axlist = BPT.figBPT_overKewley()
    iT = 30
    sqs = sorted(runs, key=lambda r: r.LAGN)
    ys =  np.array([q[iT].LOIII.cgs().sum() / q[iT].LHb.cgs().sum() for q in sqs])
    xs1 = np.array([q[iT].LNII.cgs().sum()  / q[iT].LHa.cgs().sum() for q in sqs])
    xs2 = np.array([q[iT].LSII.cgs().sum()  / q[iT].LHa.cgs().sum() for q in sqs])
    xs3 = np.array([q[iT].LOI.cgs().sum()   / q[iT].LHa.cgs().sum() for q in sqs])
    for iax,ax in enumerate(axlist):
        pl.sca(ax)
        xs = (xs1,xs2,xs3)[iax]
        pl.plot(log(xs),log(ys),c='k',lw=0.7)
        pl.scatter(log(xs),log(ys),marker='s',facecolors='w',edgecolors='k',s=10.*np.arange(1,len(sqs)+1),zorder=100)
    # pl.savefig(plots.figDir+'BPT.png',bbox_inches='tight',dpi=600)


def static_emission_lines(static,q):
    nHs_static = static['nHs']
    delta_rs_static = static['rs'] #* (q.n_gamma().value/10.**lng)**-1 #eq. 16 in Stern+14
    fHIs_static = (static['NHIs'][1:]-static['NHIs'][:-1])/(static['NHs'][1:]-static['NHs'][:-1])
    fHIs_static = np.concatenate([[static['NHIs'][0]/static['NHs'][0]], fHIs_static])
    nes = q.elec_table(static['NHIs'],static['NHs'],static['Ts'],nHs_static) * nHs_static
    nNeVs = q.NeV_table(static['NHIs'],static['NHs'],static['Ts'],nHs_static) * nHs_static
    nNeIIIs = q.NeIII_table(static['NHIs'],static['NHs'],static['Ts'],nHs_static) * nHs_static
    nOIIIs = q.OIII_table(static['NHIs'],static['NHs'],static['Ts'],nHs_static) * nHs_static
    LNeVas = emission.LNeVa(static['Ts']/1e4,nNeVs,nes) * emission.extinction(static['NHs'],emission.E_NeVa)
    LNeIIIbs = emission.LNeIIIb(static['Ts']/1e4,nNeIIIs,nes) * emission.extinction(static['NHs'],emission.E_NeIIIb)
    LOIIIs = emission.LOIIIa(static['Ts']/1e4,nOIIIs,nes) * emission.extinction(static['NHs'],emission.E_OIIIa)
    return nHs_static, delta_rs_static,fHIs_static,nNeVs,nNeIIIs,nOIIIs,LNeVas,LNeIIIbs,LOIIIs

def ionFraction_and_emissivity_static(static,q,cloudy_static=None):
    nHs_static, delta_rs_static,fHIs_static,nNeVs,nNeIIIs,nOIIIs,LNeVas,LNeIIIbs,LOIIIs = static_emission_lines(static,q)
    inds = fHIs_static<0.5
    pl.semilogx(static['taus'][inds],(nNeVs/nHs_static)[inds],c='b',label='NeV')
    pl.semilogx(static['taus'][inds],(nNeIIIs/nHs_static)[inds],c='r',label='NeIII')
    pl.semilogx(static['taus'][inds],(nOIIIs/nHs_static)[inds],c='y',label='OIII')
    pl.ylabel('ion fraction (solid)')
    pl.legend()
    pl.xlabel('optical depth')
    pl.twinx()
    pl.semilogx(static['taus'][inds],(LNeVas*np.gradient(delta_rs_static)).cumsum()[inds],c='b',ls='--')
    pl.semilogx(static['taus'][inds],(LNeIIIbs*np.gradient(delta_rs_static)).cumsum()[inds],c='r',ls='--')
    pl.semilogx(static['taus'][inds],(LOIIIs*np.gradient(delta_rs_static)).cumsum()[inds],c='y',ls='--')
    if cloudy_static!=None:
        inds_cloudy = cloudy_static['fHIs']<0.5
        pl.semilogx(cloudy_static['taus'][inds_cloudy],cloudy_static['LNeVa'][inds_cloudy],c='b',ls=':')
        pl.semilogx(cloudy_static['taus'][inds_cloudy],cloudy_static['LNeIIIb'][inds_cloudy],c='r',ls=':')
        pl.semilogx(cloudy_static['taus'][inds_cloudy],cloudy_static['LOIII'][inds_cloudy],c='y',ls=':')
    pl.ylabel('emissivity (dashed)')
    pl.xlim(0.01,3)
    pl.title('static solution',fontsize=16)

def ionFraction_and_emissivity(ion,static,q,iT,ix):
    nHs_static, delta_rs_static,fHIs_static,nNeVs,nNeIIIs,nOIIIs,LNeVas,LNeIIIbs,LOIIIs = static_emission_lines(static,q)
    inds = fHIs_static<0.5
    if ion=='NeV':   nIon,LIon,n_attr,L_attr = nNeVs,  LNeVas,  'nNeV2nH',  'LNeVa'
    if ion=='NeIII': nIon,LIon,n_attr,L_attr = nNeIIIs,LNeIIIbs,'nNeIII2nH','LNeIIIb'
    pl.semilogx(static['taus'],(nIon/nHs_static),c='b',label='static')
    pl.semilogx(q[iT].tau.cgs()[:,ix],getattr(q[iT],n_attr).cgs()[:,ix],c='k',label='sim')
    pl.ylabel('ion fraction (solid)')
    pl.legend()
    pl.xlabel('optical depth')
    pl.twinx()
    pl.semilogx(static['taus'],(LIon*np.gradient(delta_rs_static)).cumsum(),c='b',ls='--')
    pl.semilogx(q[iT].tau.cgs()[:,ix],((getattr(q[iT],L_attr).cgs()/q[iT].V.cgs())[:,ix]*q[iT].r.d()).cumsum()*un.pc.to('cm'),c='k',ls='--')
    pl.ylabel('emissivity (dashed)')
    pl.xlim(0.01,10)
    pl.title(ion,fontsize=16)

def density_and_temperature_vs_static(static,q,static_cloudy,iT,ix,yls0=(0,2e5)):
    nHs_static, delta_rs_static,fHIs_static,nNeVs,nNeIIIs,nOIIIs,LNeVas,LNeIIIbs,LOIIIs = static_emission_lines(static,q)
    inds = fHIs_static<0.5
    pl.semilogx(static['taus'][inds],nHs_static[inds]*(q.n_gamma().value/10.**lng),c='r',label='static')
    pl.semilogx(q[iT].tau.cgs()[:,ix],q[iT].nH.cgs()[:,ix],c='k',label='sim')
    if static_cloudy!=None:
        inds_cloudy = static_cloudy['fHIs']<0.5
        pl.semilogx(static_cloudy['taus'][inds_cloudy],static_cloudy['nHs'][inds_cloudy]*(q.n_gamma().value/10.**lng),c='.5',label='cloudy')
    pl.ylabel('density (solid)')
    pl.legend()
    pl.xlabel('optical depth')
    pl.ylim(*yls0)
    pl.twinx()
    pl.semilogx(q[iT].tau.cgs()[:,ix],q[iT].T.cgs()[:,ix],c='k',ls='--')
    pl.semilogx(static['taus'][inds],static['Ts'][inds],c='r',ls='--')
    if static_cloudy!=None:
        pl.semilogx(static_cloudy['taus'][inds_cloudy],static_cloudy['Ts'][inds_cloudy],c='.5',ls='--')

    pl.ylabel('temperature (dashed)')
    pl.xlim(0.01,10)
    pl.ylim(0,1.2e5)

def framesForRPCMovieMultiProcessor(Nprocs,run,min_iT,max_iT,var,rng,ix,xls=None,yls=None,suffix='png'):
    pool = multiprocessing.Pool(processes=Nprocs,maxtasksperchild=1)
    for iT in range(min_iT,max_iT):
        pool.apply_async(RPC_movie, (run,iT,var,rng,ix,xls,yls,suffix))
    return pool
def EAS_presentation_map(run,iT,var,rng,ix=205,dx=25,xls=None,yls=None,suffix='png',**kwargs):
    labelsize(True)
    pl.figure(figsize=(fig_width/1.65,fig_width*554/678))
    ax = pl.subplot(111)
    r0 = run.r0/un.pc.to('cm')
    logScale = not ( (var[0]=='L') or var in ('absorbedFrac','F2F0'))
    variable_map(run[iT], var,logScale=logScale, title='',vmin=rng[0],vmax=rng[1],**kwargs)                  
    
    qd = 64
    pl.quiver(run[iT].x.cgs()[qd//2::qd],
              run[iT].r.cgs()[qd//2::qd*2],
              run[iT].v_theta.cgs()[qd//2::qd*2,qd//2::qd],
              run[iT].v_r.cgs()[qd//2::qd*2,qd//2::qd],color='.75')
    
    
    ax.yaxis.set_major_locator(matplotlib.ticker.MultipleLocator())
    pl.ylabel(r'$r\ [{\rm pc}]$')
        
    pl.xlim(0,1.5)
    pl.ylim(100,103)
    cb = pl.colorbar(orientation='vertical')
    cb.set_label(str(run[iT][var])) #fontsize=14

    pl.savefig(figdir+run.name+'__'+var+'_%03d.%s'%(iT,suffix),bbox_inches='tight',dpi=600)
    #mencoder "mf://B2D_N2048_R100_100__nH_???.png" -mf fps=20 -o RPC.avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=800

def EAS_profile(run,iT,ix,static_solution_fn,showU,showRPC):
    labelsize(True)
    fig=pl.figure(figsize=(fig_width/1.65*0.7,fig_width*554/678*0.7))
    pl.subplots_adjust(left=0.15)
    ax = pl.subplot(111)
    
    tau_rng = 0.01,100    
    nu_in_ryd = mean_hnu / (13.6*un.eV.to('erg'))
    taus = run[iT].NHI.cgs()[:,ix]*sigma_HI_one_rydberg*nu_in_ryd**-3 + run[iT].NH.cgs()[:,ix]*grain_cross_sections_func(log(nu_in_ryd))
    sr,er = np.searchsorted(taus, tau_rng)
    if er==run[0].r.shape()[0]: er-=1             
    ys = run[iT].nH.cgs()[sr:er+1,ix]
    
    pl.plot(taus[sr:er+1],ys,c='k',lw=3,ls='--',label='simulation',dashes=[3,2])

    if showRPC:        
        static_solution = np.load(static_solution_fn)
        pl.plot(static_solution['NHs']*grain_cross_sections_func(log(nu_in_ryd)) +
                static_solution['NHIs']*sigma_HI_one_rydberg*nu_in_ryd**-3,
                100*10**lng/static_solution['U0s'],c='.75',lw=5,zorder=-10,alpha=0.75,label="RPC slab")
        pl.axhline(2e9/2.3/1e4,c='k',lw=0.5)
        
    
    pl.loglog()
    pl.xlim(*tau_rng)
    #pl.xlabel(r'optical depth at 1 Ryd')
    #pl.xlabel(r'optical depth at mean $\nu\approx40\,\frac{\rm eV}{\rm h}$')
    
    yls = np.array([40,1.5e5])
    pl.ylim(*yls) 
    ax.xaxis.set_major_formatter(arilogformatter)
    ax.xaxis.set_major_locator(matplotlib.ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
    ax.xaxis.set_minor_locator(matplotlib.ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
    ax.yaxis.set_ticks_position('both')

    pl.ylabel(r'$n_{\rm H}\ [{\rm cm}^{-3}]$')        
    ax.yaxis.set_major_formatter(arilogformatter)            
    if showRPC:
        pl.legend(loc='lower right',handlelength=1.5,fontsize=15,frameon=False)


    ax3=pl.twiny()
    #taus_to_show = np.array([0.02,1,100])
    inds = np.searchsorted(taus, tau_rng)
    ax3.set_xscale('log')
    ax3.xaxis.set_major_locator(ticker.FixedLocator(tau_rng))
    ax3.xaxis.set_major_formatter(ticker.FixedFormatter(['%.1f'%(run[iT].r.cgs()[i]) for i in (sr,er)]))
    ax3.xaxis.set_minor_locator(ticker.NullLocator())
    pl.xlabel(r'$r\ [{\rm pc}]$')
    pl.xlim(*tau_rng)
    pl.savefig(figdir+run.name+'profile_ix%d_%03d.pdf'%(ix,iT),bbox_inches='tight',dpi=600)
    
def EAS_profile_pressure(run,iT,ixs,static_solution_fn):
    labelsize(True)
    fig=pl.figure(figsize=(fig_width/1.65*0.7,fig_width*554/678*0.7))
    pl.subplots_adjust(left=0.15)
    ax = pl.subplot(111)
    
    tau_rng = 0.01,100    
    nu_in_ryd = mean_hnu / (13.6*un.eV.to('erg'))
    Prad2k = (run[iT].F0.cgs()[:]*un.erg/un.cm**2/un.s/cons.c/cons.k_B).to('cm**-3 K')
    print(Prad2k)
    for i,ix in enumerate(ixs):        
        taus = run[iT].NHI.cgs()[:,ix]*sigma_HI_one_rydberg*nu_in_ryd**-3 + run[iT].NH.cgs()[:,ix]*grain_cross_sections_func(log(nu_in_ryd))
        sr,er = np.searchsorted(taus, tau_rng)
        if er==run[0].r.shape()[0]: er-=1             
        ys = run[iT].P2k.cgs()[sr:er+1,ix] / Prad2k[sr:er+1]       
        pl.plot(taus[sr:er+1],ys,c='k',lw=3,ls='--',label='simulation',dashes=[3,2])

    pl.axhline(1.,c='.5',lw=0.5)
    pl.loglog()
    pl.xlim(*tau_rng)
    #pl.xlabel(r'optical depth at 1 Ryd')
    #pl.xlabel(r'optical depth at mean $\nu\approx40\,\frac{\rm eV}{\rm h}$')
    #pl.text(0.02,2e9,r'$P_{\rm rad}\equiv L/4\pi r^2c$')
    yls = np.array([0.01,3])
    pl.ylim(*yls) 
    ax.xaxis.set_major_formatter(arilogformatter)
    ax.xaxis.set_major_locator(matplotlib.ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
    ax.xaxis.set_minor_locator(matplotlib.ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
    ax.yaxis.set_ticks_position('both')

    pl.ylabel(r'$P_{\rm gas} / P_{\rm rad}$')        
    ax.yaxis.set_major_formatter(arilogformatter)

    static_solution = np.load(static_solution_fn)
    pl.plot(static_solution['NHs']*grain_cross_sections_func(log(nu_in_ryd)) +
            static_solution['NHIs']*sigma_HI_one_rydberg*nu_in_ryd**-3,
            100*10**lng/static_solution['U0s']*static_solution['Ts']*2.3/1.87e9,c='.75',lw=5,zorder=-10,alpha=0.75,label="RPC slab")


    pl.savefig(figdir+run.name+'profile_ix%d_%03d_pressure.pdf'%(ix,iT),bbox_inches='tight',dpi=600)
    
def plotIonizationStates(m,element):	
    fig=pl.figure(figsize=(fig_width*1.2,3))
    labelsize(True,18)
    pl.subplots_adjust(bottom=0.2,right=0.97)
    ax = pl.subplot(111)
    cs = itertools.cycle('kbcrgy')	

    NHI = m.Nion('HI')
    NH = m.Nion('HI')+m.Nion('HII')
    taus = NH*sigma_dust_one_rydberg + NHI*sigma_HI_one_rydberg
    attr = m.elementStates[element]
    for i in range(9):
        c=cs.next()
        l = pl.plot(taus, 10**attr[i],c=c,lw=2)
        s = element+roman[i+1]
        if i in (0,1,2,3, 5,6,7):            
            pl.text([80,40,5,4,2.8,1.6,0.4,0.1,0.02][i], 
                [0.75,0.75,0.9,0.55,0.45,0.45,0.8,0.6,0.65][i],
                s,ha='center',fontsize=16,color=c)
    pl.semilogx()
    pl.xlim(0.01,100)
    pl.ylabel('ion fraction')	
    ax.yaxis.set_major_locator(matplotlib.ticker.MultipleLocator(0.5))
    ax.xaxis.set_major_formatter(ticker.FixedFormatter(['0.01','0.1','1','10','100']))
    ax.xaxis.set_major_locator(ticker.FixedLocator([0.01,0.1,1,10,100]))
    pl.xlabel(r'optical depth at 1 Ryd')

def IonizationTables(f_abundances,element,Nstates):    
    tables_dic = {}
    for i in range(Nstates+1):
        s = element+roman[i+1]
        tables_dic[s] = PLUTO_Table(('NHIs','AVs','Temperatures','Densities'),
                          chimes_output = f_abundances,
                          calculatedValueColumn = speciesIndex(s))
    return tables_dic

def plotIonizationStatesSim(run, iT, ix,tables_oxygen, tables_iron, tau_rng=(0.01,100)):    
    
    lng_factor = run.LAGN / LAGN * (run.r0 / rAGN)**-2
    nHs = run[iT].nH.cgs()[:,ix] / lng_factor
    Ts,NHs,NHIs = run[iT].T.cgs()[:,ix], run[iT].NH.cgs()[:,ix], run[iT].NHI.cgs()[:,ix]
    taus = run[iT].tau_HI.cgs()[:,ix] + run[iT].tau_dust.cgs()[:,ix]    
    pl.figure(figsize=(fig_width/2.1,5))    
    for ielement,element in enumerate(('O','Fe')): 
        ax = pl.subplot(2,1,ielement+1)
        Nstates=(8,26)[ielement]
        for i in range(Nstates+1)[::-1]:
            s = element+roman[i+1]
            table = (tables_oxygen,tables_iron)[ielement][s]
            c = pratio_cmap(i/(Nstates+1.))
            if element=='O':  tup = ((40,0.05),(50,0.4),(15,0.37),None,(3,0.47),(0.4,0.32),(0.6,0.7),(0.015,0.2),None)[i]
            if element=='Fe': tup = (None,(35,0.5),None,None,None,None,            
                                     (1.5,0.55),None,None,(0.055,0.4),None,None,None,None,None,None,None,                                                        (0.015,0.75),None,None,None,None,None,None,None,None,
                                     None,None,None,None,None,None,None,None)[i]
            fions = table(NHIs,NHs,Ts,nHs)/DC16_abundances[element]
            pl.plot(taus, fions,label=s,c=c)            
            if fions[taus>0.01].max()>0.5: print(s)
            if tup!=None:                        
                pl.text(tup[0],tup[1],s,color=c)
            
        pl.semilogx()
        pl.xlim(*tau_rng)                 
        #pl.legend(ncol=2,fontsize=9,handlelength=1,columnspacing=1,handletextpad=0.4,frameon=False)
        
        pl.ylim(0,1.05)
        ax.xaxis.set_major_formatter(arilogformatter)  
        pl.ylabel('ion fraction')	
        if ielement==1:
            pl.xlabel(r'optical depth at 1 Ryd')
        else:
            ax.xaxis.set_major_formatter(ticker.NullFormatter())  
            pl.text(0.75,0.9,r'$\log L=%.0f$'%(log(run.LAGN)),transform=ax.transAxes)
            pl.text(0.75,0.82,r'%s'%(run[iT].t.fullStr()),transform=ax.transAxes)
            pl.text(0.75,0.74,r'$x=%.1f$ pc'%(run[iT].x.cgs()[ix]),transform=ax.transAxes)
        if ielement==0:
            ax3=pl.twiny()
            taus_to_show = np.array([0.01,0.1,1,10,100])
            inds = np.searchsorted(taus, taus_to_show)
            ax3.set_xscale('log')
            ax3.xaxis.set_major_locator(ticker.FixedLocator(taus_to_show))
            nDigits = (1,2,2,3,3)
            ax3.xaxis.set_major_formatter(ticker.FixedFormatter([('%.'+'%d'%nDigits[ii]+'f')%(run[iT].r.cgs()[i]) for ii,i in enumerate(inds)]))
            ax3.xaxis.set_minor_locator(ticker.NullLocator())
            pl.xlabel(r'$r\ [{\rm pc}]$')
            pl.xlim(*tau_rng)
            
    
    
    pl.savefig(figdir+run.name+'Ostates_ix%d_%03d.pdf'%(ix,iT),bbox_inches='tight',dpi=600)
def plotIonizationStatesSimPresentation(run, iT, ix,tables_oxygen, tables_iron, tau_rng=(0.01,30)):    
    cmap = pl.get_cmap('copper')
    lng_factor = run.LAGN / LAGN * (run.r0 / rAGN)**-2
    nHs = run[iT].nH.cgs()[:,ix] / lng_factor
    Ts,NHs,NHIs = run[iT].T.cgs()[:,ix], run[iT].NH.cgs()[:,ix], run[iT].NHI.cgs()[:,ix]
    nu_in_ryd = mean_hnu / (13.6*un.eV.to('erg'))
    taus = run[iT].NHI.cgs()[:,ix]*sigma_HI_one_rydberg*nu_in_ryd**-3 + run[iT].NH.cgs()[:,ix]*sigma_dust_one_rydberg*nu_in_ryd**-1
    pl.figure(figsize=(fig_width,3.5))
    pl.subplots_adjust(top=0.9)
    ielement=0; element='O'
    ax = pl.subplot(111)
    Nstates=(8,26)[ielement]
    for i in range(Nstates+1)[::-1]:
        s = element+roman[i+1]
        table = (tables_oxygen,tables_iron)[ielement][s]
        c = cmap(i/(Nstates+1))
        pl.plot(taus, table(NHIs,NHs,Ts,nHs)/(DC16_abundances[element]),label=s,c=c)            
    pl.semilogx()
    pl.xlim(*tau_rng)                 
    pl.ylim(0,1.05)
    ax.xaxis.set_major_formatter(arilogformatter)  
    #pl.ylabel('ion fraction')	
    #pl.xlabel(r'optical depth at mean $\nu\approx40\,\frac{\rm eV}{\rm h}$')
    #ax3=pl.twiny()
    #taus_to_show = np.array([0.01,0.1,1,10,100])
    #inds = np.searchsorted(taus, taus_to_show)
    #ax3.set_xscale('log')
    #ax3.xaxis.set_major_locator(ticker.FixedLocator(taus_to_show))
    #nDigits = (1,2,2,3,3)
    #ax3.xaxis.set_major_formatter(ticker.FixedFormatter([('%.'+'%d'%nDigits[ii]+'f')%(run[iT].r.cgs()[i]) for ii,i in enumerate(inds)]))
    #ax3.xaxis.set_minor_locator(ticker.NullLocator())
    #pl.xlabel('radius [pc]')
    pl.xlim(*tau_rng)    
    pl.savefig(figdir+run.name+'Ostates_ix%d_%03d_presentation.pdf'%(ix,iT),bbox_inches='tight',dpi=600)
def RPC_movie(run,iT,var,rng,ix=205,dx=25,xls=None,yls=None,suffix='png',static_solution_fn=None,**kwargs):
    labelsize(True)
    pl.figure(figsize=(fig_width,5.85))
    gss = (gridspec.GridSpec(2,1,height_ratios=[4,1.5]),
           gridspec.GridSpec(2,1,height_ratios=[4,1.5]))
    gss[0].update(left=0.1,right=0.39,bottom=0.07,top=0.97)
    gss[1].update(left=0.58,right=0.87,bottom=0.07,top=0.97)        
    
    r0 = run.r0/un.pc.to('cm')
    ax = pl.subplot(gss[0][0,0])
    logScale = not ( (var[0]=='L') or var in ('absorbedFrac','F2F0'))
    variable_map(run[iT], var,logScale=logScale, title='',vmin=rng[0],vmax=rng[1],**kwargs)                  
    
    ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x,pos,r0=r0: '%d'%(x-r0)))
    ax.yaxis.set_major_locator(matplotlib.ticker.FixedLocator(r0+np.array([0.,1.,2.,3.])))
    pl.ylabel(r'$\delta r\ [{\rm pc}]$')
        
    if xls!=None: pl.xlim(*xls)
    if yls!=None: pl.ylim(*yls)
    cb = pl.colorbar(ax=pl.subplot(gss[0][1,0],visible=False),orientation='horizontal',pad=0.,shrink=1.,fraction=0.5)
    cb.set_label(str(run[iT][var])) #fontsize=14

    sx,mx,ex = np.array([ix-dx,ix,ix+dx])
    xrng = run[iT].x.cgs()[np.array([sx,ex])]
    tau_rng = np.array([0.001,100])
    
    if iT!=0:
        taus = run[iT].tau_HI.cgs()[:,mx] + run[iT].tau_dust.cgs()[:,mx]
        sr,er = np.searchsorted(taus, tau_rng)
        rrng = run[iT].r.cgs()[np.array([sr,er])]
        ax.add_patch(matplotlib.patches.Rectangle((xrng[0],rrng[0]), (xrng[1]-xrng[0]), (rrng[1]-rrng[0]),fc='none',ec='w',lw=1))
        pl.annotate('',(1,100.8),(xrng[1]+0.02,np.mean(rrng)), arrowprops={'width':0.3,'headwidth':1.5,'frac':0.15,'color':'.3'},fontsize=8,ha='center')

    ax = pl.subplot(gss[1][0])
    pl.sca(ax)
    
    if iT!=0: 
        ys = run[iT].nH.cgs()[sr:er,mx]*un.cm**-3
        pl.plot(taus[sr:er],ys,c='k')
    else:
        pl.axhline(run[iT].nH.cgs().max(),c='k')

        
    pl.loglog()
    xls = np.array([0.01,10])
    yls = np.array([6,1.5e5])
    pl.xlim(*xls)
    pl.ylabel('hydrogen density [cm$^{-3}$]')
    pl.xlabel(r'optical depth (1 ryd)')
    pl.ylim(*yls)
    ax.xaxis.set_major_formatter(arilogformatter)
    pl.gca().xaxis.set_major_locator(matplotlib.ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
    pl.gca().xaxis.set_minor_locator(matplotlib.ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
    ax.yaxis.set_major_formatter(arilogformatter)
    pl.annotate('static\nsolution',(0.15,200),(1,35),arrowprops=slantlinepropsblack,ha='center')
    ax2=pl.twinx()
    ax2.set_yscale('log')
    ax2.set_ylim(*(10**lng*100/(yls/1.3))) #1.3 is ratio of electrons to hydrogen atoms    
    static_solution = np.load(static_solution_fn)
    pl.plot(static_solution['NHs']*sigma_dust_one_rydberg+static_solution['NHIs']*sigma_HI_one_rydberg,
            static_solution['U0s'],c='.75',lw=6,zorder=-10,alpha=0.5)            
    ax2.yaxis.set_major_formatter(arilogformatter)
    pl.ylabel('ionization parameter')

    timestr='%.1f'%run[iT].t.cgs()
    ax.text(0.5,0.025,'t = %s kyr'%timestr,ha='center',transform=pl.gcf().transFigure,clip_on=False)
    pl.savefig(videoDir+run.name+'__'+var+'_withProfile_%03d.%s'%(iT,suffix),dpi=300)
    pl.close(fig)
    #ffmpeg -framerate 20 -i ./B2D_N1024_R100_430D_video__nH_withProfile_%03d.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p -vf "pad=ceil ( iw/2 ) *2:ceil ( ih/2 ) *2" RPC2.mp4



def RPC_movie2(run,iT,suffix='png'):
    labelsize(True)
    pl.figure(figsize=(fig_width,7.85))
    r0 = run.r0/un.pc.to('cm')
    
    ax = pl.subplot(121)
    variable_map(run[iT], 'nH',logScale=True, title='',vmin=10,vmax=1e5,cmap='viridis')                      
    ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x,pos,r0=r0: '%d'%x))
    ax.yaxis.set_major_locator(matplotlib.ticker.MultipleLocator())
    pl.ylabel(r'$r\ [{\rm pc}]$')        
    pl.xlim(0,1.5)
    pl.ylim(100,103)
    cb = pl.colorbar(orientation='horizontal',pad=0.15,shrink=1.,fraction=0.5)
    cb.set_label(str(run[iT]['nH'])) #fontsize=14

    ax = pl.subplot(122)
    variable_map(run[iT], 'T',logScale=True, title='',vmin=100,vmax=1e6,cmap='inferno')                      
    ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x,pos,r0=r0: '%d'%x))
    ax.yaxis.set_major_locator(matplotlib.ticker.MultipleLocator())
    pl.ylabel(r'$r\ [{\rm pc}]$')        
    pl.xlim(0,1.5)
    pl.ylim(100,103)
    ax.yaxis.set_label_position('right')
    ax.yaxis.set_ticks_position('right')
    ax.yaxis.set_ticks_position('both')				
    
    cb = pl.colorbar(orientation='horizontal',pad=0.15,shrink=1.,fraction=0.5)
    cb.set_label(str(run[iT]['T'])) #fontsize=14

    pl.savefig(videoDir+run.name+'__'+'nH_and_T'+'_%03d.%s'%(iT,suffix))
    #mencoder "mf://B2D_N2048_R100_100__nH_???.png" -mf fps=20 -o RPC.avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=800


def RPC_movie3(run,iT,suffix='png'):
    labelsize(True)
    fig = pl.figure(figsize=(fig_width/2,fig_width*554/678))
    pl.subplots_adjust(top=0.9,left=0.12,right=0.92)
    r0 = run.r0/un.pc.to('cm')
    
    ax = pl.subplot(111)
    variable_map(run[iT], 'nH',logScale=True, title='',vmin=10,vmax=1e5,cmap='viridis')                      
    ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x,pos,r0=r0: '%d'%x))
    ax.yaxis.set_major_locator(matplotlib.ticker.MultipleLocator())
    pl.ylabel(r'$r\ [{\rm pc}]$')        
    pl.xlim(0,1.5)
    pl.ylim(100,103)
    cb = pl.colorbar(orientation='vertical',format=arilogformatter)
    cb.set_label(str(run[iT]['nH'])) #fontsize=14

    timestr='%.1f'%run[iT].t.cgs()
    ax.text(0.5,0.95,'t = %s kyr'%timestr,ha='center',transform=pl.gcf().transFigure,clip_on=False)

    pl.savefig(videoDir+run.name+'__'+'nH'+'_%03d.%s'%(iT,suffix),dpi=600)
    pl.close(fig)
    #ffmpeg -framerate 20 -i ./B2D_N1024_R100_430D_video__nH_%03d.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p -vf "pad=ceil ( iw/2 ) *2:ceil ( ih/2 ) *2" RPC.mp4

def RPC_movie4(run,iT,suffix='png'):
    labelsize(True)
    fig = pl.figure(figsize=(fig_width,fig_width))
    r0 = run.r0/un.pc.to('cm')
    
    ax = pl.subplot(121)
    variable_map(run[iT], 'nH',logScale=True, title='',vmin=10,vmax=1e5,cmap='viridis')                      
    ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x,pos,r0=r0: '%d'%x))
    ax.yaxis.set_major_locator(matplotlib.ticker.MultipleLocator())
    pl.ylabel(r'$r\ [{\rm pc}]$')        
    pl.xlim(0,1.5)
    pl.ylim(100,103)
    cb = pl.colorbar(orientation='horizontal')
    cb.set_label(str(run[iT]['nH'])) #fontsize=14

    ax = pl.subplot(122)
    variable_map(run[iT], 'T',logScale=True, title='',vmin=100,vmax=1e6,cmap='inferno')                      
    ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x,pos,r0=r0: '%d'%x))
    ax.yaxis.set_major_locator(matplotlib.ticker.MultipleLocator())
    pl.ylabel(r'')        
    pl.xlim(0,1.5)
    pl.ylim(100,103)
    cb = pl.colorbar(orientation='horizontal')
    cb.set_label(str(run[iT]['T'])) #fontsize=14

    timestr='%.1f'%run[iT].t.cgs()
    ax.text(0.5,0.975,'t = %s kyr'%timestr,ha='center',transform=pl.gcf().transFigure,clip_on=False)

    pl.savefig(videoDir+run.name+'__'+'nHT'+'_%03d.%s'%(iT,suffix))
    #mencoder "mf://B2D_N2048_R100_100__nH_???.png" -mf fps=20 -o RPC.avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=800
