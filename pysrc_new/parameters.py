import numpy as np
from numpy import log10 as log
from astropy import units as un, constants as cons
from scipy import interpolate
import work_paths as wp;runsDir,rpc_hydro_dir=wp.work_paths()

V96_data_file = chimes_cross_sections_dir +'data/cross_section_fits_verner96.dat'

rydberg_angstrom = 1e8/1.0968e5 
Rydbergnu = 3e18/rydberg_angstrom
Ryd_to_Ang = lambda ryd: rydberg_angstrom / ryd
Ang_to_Ryd = lambda ang: rydberg_angstrom / ang

min_logE = -9.
max_logE = 10.
dlogE = 0.01
logEs = np.arange(min_logE,max_logE+dlogE/2.,dlogE)

#parameters for generating cross-sections. parameters for CHIMES tables later defined in CHIMES param file
NHIs = 10.0 ** np.arange(15.0, 24.05, 0.5)
NHs  = 10.0 ** np.arange(19.0, 24.05, 0.5)
#for debugging
#NHIs = 10.0 ** np.arange(15.0, 15.55, 0.5)
#NHs  = 10.0 ** np.arange(19.0, 19.55, 0.5)
LAGN = 1e46
rAGN = 1000*un.pc.to('cm')
aEUV = -1.6
mean_hnu = 41.09*un.electronvolt.to('erg') #verify the same as calculated in 'verify_chimes_output.ipynb'
ion_fraction = 0.31                        #verify the same as calculated in 'verify_chimes_output.ipynb'
lng = log(ion_fraction*LAGN/(4*np.pi*rAGN**2*mean_hnu*cons.c.to('cm/s').value))
MIN_log_flux = -70 #to avoid zero division errors in CHIMES


X,Y,Z = 0.7,0.28,0.02
AH,AHe,AZ = 1.,4.,15.500
dust2gasRatio = 0.01 

grain_opacities_filename = rpc_hydro_dir + 'forCHIMES/grainOpacities.npz'
f = np.load(grain_opacities_filename)
grain_logEs, grain_logOpacities = f['logEs'],f['logOpacities']
f.close()
grain_cross_sections_func = lambda logE: 10.**interpolate.interp1d(grain_logEs,
                                                                grain_logOpacities,
                                                                fill_value=-100,bounds_error=False)(logE) * (dust2gasRatio/0.01)


    
sigma_HI_one_rydberg = 6.23e-18
sigma_dust_one_rydberg = 1.91e-21 * (dust2gasRatio/0.01)


### CHIMES species

species = np.array(['elec', 'HI', 'HII', 'Hm', 'HeI', 'HeII', 'HeIII', 'CI', 'CII', 'CIII', 'CIV', 'CV', 'CVI', 'CVII', 'Cm',
 'NI', 'NII', 'NIII', 'NIV', 'NV', 'NVI', 'NVII', 'NVIII', 'OI', 'OII', 'OIII', 'OIV', 'OV', 'OVI', 'OVII', 'OVIII',
 'OIX', 'Om', 'NeI', 'NeII', 'NeIII', 'NeIV', 'NeV','NeVI', 'NeVII', 'NeVIII', 'NeIX', 'NeX', 'NeXI', 'MgI', 'MgII',
 'MgIII', 'MgIV', 'MgV', 'MgVI', 'MgVII', 'MgVIII','MgIX','MgX', 'MgXI', 'MgXII', 'MgXIII', 'SiI', 'SiII', 'SiIII',
 'SiIV', 'SiV', 'SiVI', 'SiVII', 'SiVIII', 'SiIX', 'SiX', 'SiXI', 'SiXII', 'SiXIII', 'SiXIV', 'SiXV', 'SI', 'SII',
 'SIII', 'SIV', 'SV', 'SVI','SVII', 'SVIII', 'SIX', 'SX', 'SXI', 'SXII','SXIII', 'SXIV', 'SXV', 'SXVI', 'SXVII',
 'CaI', 'CaII', 'CaIII','CaIV', 'CaV', 'CaVI','CaVII', 'CaVIII', 'CaIX', 'CaX', 'CaXI', 'CaXII', 'CaXIII', 'CaXIV',
 'CaXV', 'CaXVI', 'CaXVII', 'CaXVIII', 'CaXIX','CaXX', 'CaXXI', 'FeI', 'FeII','FeIII', 'FeIV', 'FeV', 'FeVI', 'FeVII',
 'FeVIII', 'FeIX', 'FeX', 'FeXI', 'FeXII', 'FeXIII', 'FeXIV', 'FeXV', 'FeXVI', 'FeXVII', 'FeXVIII', 'FeXIX', 'FeXX',
 'FeXXI', 'FeXXII', 'FeXXIII', 'FeXXIV', 'FeXXV', 'FeXXVI', 'FeXXVII', 'H2', 'H2p', 'H3p', 'OH', 'H2O', 'C2', 'O2',
 'HCOp', 'CH', 'CH2', 'CH3p', 'CO', 'CHp', 'CH2p', 'OHp', 'H2Op', 'H3Op', 'COp', 'HOCp', 'O2p'])

speciesIndex = lambda s: (species==s).nonzero()[0][0]

solar_abundances = {'H':1, #Table 1 in Wiersma+09
              'He':0.1,
              'C':2.46e-4,
              'N':8.51e-5,
              'O':4.9e-4,
              'Ne':1e-4,
              'Mg':3.47e-5,
              'Si':3.47e-5,
              'S':1.86e-5,
              'Ca':2.29e-6,
              'Fe':2.82e-6}


DC16_abundances = {'H':1, 
                   'He':0.1,
                   'C':1.50e-4,
                   'N':6.58e-5,
                   'O':2.59e-4,
                   'Ne':9.98e-5,
                   'Mg':3.03e-6,
                   'Si':2.78e-6,
                   'S':5.55e-6,
                   'Ca':2.28e-6,
                   'Fe':2.02e-7}
