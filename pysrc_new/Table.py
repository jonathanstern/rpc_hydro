import numpy as np
from numpy import log as ln, log10 as log

def searchsortedclosest(arr, val):
    assert(arr[0]!=arr[1])
    if arr[0]<arr[1]:
        ind = np.searchsorted(arr,val)
        ind = minarray(ind, len(arr)-1)
        return maxarray(ind - (val - arr[maxarray(ind-1,0)] < arr[ind] - val),0)        
    else:
        ind = np.searchsorted(-arr,-val)
        ind = minarray(ind, len(arr)-1)
        return maxarray(ind - (-val + arr[maxarray(ind-1,0)] < -arr[ind] + val),0)        
def maxarray(arr, v):
    return arr + (arr<v)*(v-arr)
def minarray(arr, v):
    return arr + (arr>v)*(v-arr)


def searchAndInterpolateArr(arr_xs,vals,arr_ys,weights=None): # beyond edges extrapolates from last two points
    inds = np.searchsorted(arr_xs,vals)
    inds = minarray( maxarray(inds,1),len(arr_xs)-1)
    x1s = arr_xs.take(inds-1)
    y1s = arr_ys.take(inds-1)

    x2s = arr_xs.take(inds)
    y2s = arr_ys.take(inds)

    if weights!=None:
        w1s = weights.take(inds-1)
        w2s = weights.take(inds)
    else:
        w1s = w2s = 1.

    return ( y2s*w2s*(vals-x1s) + y1s*w1s*(x2s-vals) ) / (w1s*(vals-x1s) + w2s*(x2s-vals))

def searchAndInterpolate2DArr(arr_xs,valx,arr_ys,valy,matrix_zs):
    i = minarray( maxarray(np.searchsorted(arr_xs, valx), 1), len(arr_xs)-1)
    j = minarray( maxarray(np.searchsorted(arr_ys, valy), 1), len(arr_ys)-1)

    dx = arr_xs[i] - arr_xs[i-1]
    xn = 1.* (valx - arr_xs[i-1]) / dx
    dy = arr_ys[j] - arr_ys[j-1]
    yn = 1.* (valy - arr_ys[j-1]) / dy

    return (matrix_zs[i-1,j-1]*(1-xn)*(1-yn) + matrix_zs[i-1,j]*(1-xn)*yn + 
                matrix_zs[i,j-1]*xn*(1-yn) + matrix_zs[i,j]*xn*yn)



def searchAndInterpolate3DArr(arr_xs,valx,arr_ys,valy,arr_zs, valz, matrix_ws):
    i = minarray( maxarray(np.searchsorted(arr_xs, valx), 1), len(arr_xs)-1)
    j = minarray( maxarray(np.searchsorted(arr_ys, valy), 1), len(arr_ys)-1)
    k = minarray( maxarray(np.searchsorted(arr_zs, valz), 1), len(arr_zs)-1)

    dx = arr_xs[i] - arr_xs[i-1]
    xn = 1.* (valx - arr_xs[i-1]) / dx
    dy = arr_ys[j] - arr_ys[j-1]
    yn = 1.* (valy - arr_ys[j-1]) / dy
    dz = arr_zs[k] - arr_zs[k-1]
    zn = 1.* (valz - arr_zs[k-1]) / dz

    return ( (matrix_ws[i-1,j-1,k-1]*(1-xn)*(1-yn) + matrix_ws[i-1,j,k-1]*(1-xn)*yn + matrix_ws[i,j-1,k-1]*xn*(1-yn) + matrix_ws[i,j,k-1]*xn*yn)*(1-zn) +
                 (matrix_ws[i-1,j-1,k]  *(1-xn)*(1-yn) + matrix_ws[i-1,j,k]  *(1-xn)*yn + matrix_ws[i,j-1,k]  *xn*(1-yn) + matrix_ws[i,j,k]  *xn*yn)*zn )

def searchAndInterpolate4DArr(arr_xs,valx,arr_ys,valy,arr_zs, valz, arr_ws,valw, matrix_vs):
    i = minarray( maxarray(np.searchsorted(arr_xs, valx), 1), len(arr_xs)-1)
    j = minarray( maxarray(np.searchsorted(arr_ys, valy), 1), len(arr_ys)-1)
    k = minarray( maxarray(np.searchsorted(arr_zs, valz), 1), len(arr_zs)-1)
    l = minarray( maxarray(np.searchsorted(arr_ws, valw), 1), len(arr_ws)-1)


    dx = arr_xs[i] - arr_xs[i-1]
    xn = 1.* (valx - arr_xs[i-1]) / dx
    dy = arr_ys[j] - arr_ys[j-1]
    yn = 1.* (valy - arr_ys[j-1]) / dy
    dz = arr_zs[k] - arr_zs[k-1]
    zn = 1.* (valz - arr_zs[k-1]) / dz
    dw = arr_ws[l] - arr_ws[l-1]
    wn = 1.* (valw - arr_ws[l-1]) / dw

    return ( ( (matrix_vs[i-1,j-1,k-1,l-1]*(1-xn)*(1-yn) + matrix_vs[i-1,j,k-1,l-1]*(1-xn)*yn + matrix_vs[i,j-1,k-1,l-1]*xn*(1-yn) + matrix_vs[i,j,k-1,l-1]*xn*yn)*(1-zn) +
                   (matrix_vs[i-1,j-1,k,l-1]  *(1-xn)*(1-yn) + matrix_vs[i-1,j,k,l-1]  *(1-xn)*yn + matrix_vs[i,j-1,k,l-1]  *xn*(1-yn) + matrix_vs[i,j,k,l-1]  *xn*yn)*zn )*(1-wn) + 
                 ( (matrix_vs[i-1,j-1,k-1,l]*(1-xn)*(1-yn) + matrix_vs[i-1,j,k-1,l]*(1-xn)*yn + matrix_vs[i,j-1,k-1,l]*xn*(1-yn) + matrix_vs[i,j,k-1,l]*xn*yn)*(1-zn) +
                   (matrix_vs[i-1,j-1,k,l]  *(1-xn)*(1-yn) + matrix_vs[i-1,j,k,l]  *(1-xn)*yn + matrix_vs[i,j-1,k,l]  *xn*(1-yn) + matrix_vs[i,j,k,l]  *xn*yn)*zn )*wn )

def searchAndInterpolate5DArr(arr_xs,valx,arr_ys,valy,arr_zs, valz, arr_ws,valw, arr_vs, valv, matrix_qs):
    i = minarray( maxarray(np.searchsorted(arr_xs, valx), 1), len(arr_xs)-1)
    j = minarray( maxarray(np.searchsorted(arr_ys, valy), 1), len(arr_ys)-1)
    k = minarray( maxarray(np.searchsorted(arr_zs, valz), 1), len(arr_zs)-1)
    l = minarray( maxarray(np.searchsorted(arr_ws, valw), 1), len(arr_ws)-1)
    m = minarray( maxarray(np.searchsorted(arr_vs, valv), 1), len(arr_vs)-1)


    dx = arr_xs[i] - arr_xs[i-1]
    xn = 1.* (valx - arr_xs[i-1]) / dx
    dy = arr_ys[j] - arr_ys[j-1]
    yn = 1.* (valy - arr_ys[j-1]) / dy
    dz = arr_zs[k] - arr_zs[k-1]
    zn = 1.* (valz - arr_zs[k-1]) / dz
    dw = arr_ws[l] - arr_ws[l-1]
    wn = 1.* (valw - arr_ws[l-1]) / dw
    dv = arr_vs[m] - arr_vs[m-1]
    vn = 1.* (valv - arr_vs[m-1]) / dv


    return ( ( ( (matrix_qs[i-1,j-1,k-1,l-1,m-1]*(1-xn)*(1-yn) + matrix_qs[i-1,j,k-1,l-1,m-1]*(1-xn)*yn + matrix_qs[i,j-1,k-1,l-1,m-1]*xn*(1-yn) + matrix_qs[i,j,k-1,l-1,m-1]*xn*yn)*(1-zn) +
                     (matrix_qs[i-1,j-1,k,l-1,m-1]  *(1-xn)*(1-yn) + matrix_qs[i-1,j,k,l-1,m-1]  *(1-xn)*yn + matrix_qs[i,j-1,k,l-1,m-1]  *xn*(1-yn) + matrix_qs[i,j,k,l-1,m-1]  *xn*yn)*zn )*(1-wn) + 
                   ( (matrix_qs[i-1,j-1,k-1,l,m-1]*(1-xn)*(1-yn) + matrix_qs[i-1,j,k-1,l,m-1]*(1-xn)*yn + matrix_qs[i,j-1,k-1,l,m-1]*xn*(1-yn) + matrix_qs[i,j,k-1,l,m-1]*xn*yn)*(1-zn) +
                     (matrix_qs[i-1,j-1,k,l,m-1]  *(1-xn)*(1-yn) + matrix_qs[i-1,j,k,l,m-1]  *(1-xn)*yn + matrix_qs[i,j-1,k,l,m-1]  *xn*(1-yn) + matrix_qs[i,j,k,l,m-1]  *xn*yn)*zn )*wn )*(1-vn) + 
                 ( ( (matrix_qs[i-1,j-1,k-1,l-1,m]*(1-xn)*(1-yn) + matrix_qs[i-1,j,k-1,l-1,m]*(1-xn)*yn + matrix_qs[i,j-1,k-1,l-1,m]*xn*(1-yn) + matrix_qs[i,j,k-1,l-1,m]*xn*yn)*(1-zn) +
                     (matrix_qs[i-1,j-1,k,l-1,m]  *(1-xn)*(1-yn) + matrix_qs[i-1,j,k,l-1,m]  *(1-xn)*yn + matrix_qs[i,j-1,k,l-1,m]  *xn*(1-yn) + matrix_qs[i,j,k,l-1,m]  *xn*yn)*zn )*(1-wn) + 
                   ( (matrix_qs[i-1,j-1,k-1,l,m]*(1-xn)*(1-yn) + matrix_qs[i-1,j,k-1,l,m]*(1-xn)*yn + matrix_qs[i,j-1,k-1,l,m]*xn*(1-yn) + matrix_qs[i,j,k-1,l,m]*xn*yn)*(1-zn) +
                     (matrix_qs[i-1,j-1,k,l,m]  *(1-xn)*(1-yn) + matrix_qs[i-1,j,k,l,m]  *(1-xn)*yn + matrix_qs[i,j-1,k,l,m]  *xn*(1-yn) + matrix_qs[i,j,k,l,m]  *xn*yn)*zn )*wn )*vn )








class PLUTO_Table:
    def __init__(self, properties, filename=None, firstLine=None,calculatedValueColumn=0,chimes_output=None):
        self.properties = properties
        
        self.propertyValues = []; self.minVals = []; self.maxVals = []
        if filename!=None:
            self.filename = filename        
            f = open(self.filename)
            ls = f.readlines()[firstLine:]
            f.close()        
            data = np.array([[float(x) for x in l.split()] for l in ls])

            
            for iProp in range(len(self.properties)):
                self.propertyValues.append( np.array(sorted(np.unique([log(d[iProp]) for d in data])) ) )
                self.minVals.append(10.**self.propertyValues[-1].min())
                self.maxVals.append(10.**self.propertyValues[-1].max())
            
            self.calculatedValueMatrix = np.zeros([len(x) for x in self.propertyValues])
            for d in data:
                inds = tuple([searchsortedclosest(self.propertyValues[iProp], log(d[iProp])) for iProp in range(len(self.properties))])
                self.calculatedValueMatrix[inds] = d[len(self.properties)+calculatedValueColumn]
        elif chimes_output is not None: 
            for iProp,prop in enumerate(self.properties):
                self.propertyValues.append( chimes_output['TableBins'][prop][:] )
                self.minVals.append(10.**self.propertyValues[-1].min())
                self.maxVals.append(10.**self.propertyValues[-1].max())
            self.calculatedValueMatrix = chimes_output['Abundances'][:,:,:,:,0,calculatedValueColumn]
        else:
            assert(False)
                
    def __call__(self,*args):
        if type(args[0])==np.ndarray:
            flattenFunc = lambda x: x.flatten()
            deflattenFunc = lambda x,shape=args[0].shape: x.reshape(shape)
        else:
            flattenFunc = lambda x: np.array([x])
            deflattenFunc = lambda x: x[0]
        
        newVals = self.interpolateFunc([log(
            maxarray(
                minarray(flattenFunc(arg),self.maxVals[iarg]),
                self.minVals[iarg])) for iarg,arg in enumerate(args)])                                       
        
        return deflattenFunc(newVals)
    def interpolateFunc(self,vals):
        if len(self.properties)==1: 
            return searchAndInterpolateArr(self.propertyValues[0], vals[0],self.calculatedValueMatrix)
        if len(self.properties)==2: 
            return searchAndInterpolate2DArr(self.propertyValues[0], vals[0],
                                               self.propertyValues[1], vals[1],
                                               self.calculatedValueMatrix)
        if len(self.properties)==3: 
            return searchAndInterpolate3DArr(self.propertyValues[0], vals[0],
                                             self.propertyValues[1], vals[1],
                                             self.propertyValues[2], vals[2],
                                             self.calculatedValueMatrix)
        if len(self.properties)==4: 
            return searchAndInterpolate4DArr(self.propertyValues[0], vals[0],
                                             self.propertyValues[1], vals[1],
                                             self.propertyValues[2], vals[2],
                                             self.propertyValues[3], vals[3],
                                             self.calculatedValueMatrix)
        if len(self.properties)==5: 
            return searchAndInterpolate5DArr(self.propertyValues[0], vals[0],
                                             self.propertyValues[1], vals[1],
                                             self.propertyValues[2], vals[2],
                                             self.propertyValues[3], vals[3],
                                             self.propertyValues[4], vals[4], 
                                             self.calculatedValueMatrix)

