import numpy as np
from numpy import log10 as log, log as ln
import matplotlib
import pylab as pl

#!/usr/bin/python
# -*- coding: utf-8 -*-
pl.rc('font', family = 'serif')
NiceGraphics = 1

if NiceGraphics:
    fig_width_pt = 504.0  # Get this from LaTeX using \showthe\columnwidth
    # 240 -- one column
    # 504 -- full page
    inches_per_pt = 1.0/72.27               # Convert pt to inch
    golden_mean = (np.sqrt(5)-1.0)/2.0         # Aesthetic ratio
    fig_width = fig_width_pt*inches_per_pt  # width in inches
    fig_height = fig_width*golden_mean      # height in inches
    fig_size =  [fig_width,fig_height]
    mpparams = {#'backend': 'ps',
            #'ps.usedistiller':'xpdf',
            'axes.labelsize': 10,
            'font.size': 10,
            'font.size':10,
            'legend.fontsize': 10,
            'legend.labelspacing' : 0.3,
            'legend.numpoints' : 1,
            'xtick.labelsize': 10,
            'ytick.labelsize': 10,
            'text.usetex': True,
            'figure.figsize': fig_size,
            'axes.linewidth': 0.5,
            'axes.titlesize': 9,
            'patch.linewidth':0.5,
            'figure.subplot.bottom': 0.12,
            'figure.subplot.left': 0.1,
            'figure.subplot.right': 0.98,
            'figure.subplot.top': 0.95,
            'figure.subplot.wspace': 0.07,
            'figure.subplot.hspace': 0.07}

    pl.rcParams.update(mpparams)
    #pl.rcParams['text.latex.preamble']=[r'\usepackage{times}',r'\usepackage{mathptmx}',r'\usepackage{amsmath}',
    #    r'\usepackage{amssymb}', r'\usepackage{nicefrac}']
    pl.rcParams['mathtext.fontset'] = 'cm'
    pl.rc('font', family='serif', size=10)
    pl.rcParams['xtick.direction'] = 'in'
    pl.rcParams['ytick.direction'] = 'in'
    pl.rcParams['xtick.top'] = True
    pl.rcParams['ytick.right'] = True

bySizeParams={ (13,9):    {'bottom':0.06,'left':0.06,'top':0.97,'right':0.985, 'wspace':0.12},
               (6.5,4.7): {'left':.12, 'bottom':.09, 'top':0.995,'right':0.985},
               (9,5):     {'left':.08, 'bottom':.12, 'top':0.995,'right':0.985, 'wspace':0.1},
               (9,9):     {'left':.08, 'bottom':.06, 'top':0.97,'right':0.985, 'wspace':0.,'hspace':1e-5},
               (9,13):    {'left':.08, 'bottom':.08, 'top':0.985,'right':0.985, 'hspace':1e-5},
               (8,6):     {'left':.09, 'bottom':.1, 'top':0.97,'right':0.91, 'wspace':0.09},
               (10,10):    {'left':.09, 'bottom':.09, 'top':0.985, 'hspace':0.34},
               (10,12):    {'left':.07, 'bottom':.06, 'top':0.985, 'right':0.985},
               }

slantlineprops = slantlinepropsgray = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'0.5'}
slantlinepropsblue  = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'b'}
slantlinepropsgreen  = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'g'}
slantlinepropsmagenta  = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'m'}
slantlinepropsred   = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'r'}
slantlinepropsblack = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'k'}
slantlinepropswhite = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'w'}


roman = [None,'I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI','XVII','XVIII','XIV','XX','XXI','XXII','XXIII','XXIV','XXV','XXVI','XXVII','XXVIII','XXIX','XXX']


def myScientificFormat(x,pos=None,base=None,halfs=False,afterDotDigits=1):
    if x==0: return r'$0$'
    if base==None: 
        base = 10**int(log(x))
        if x/base>=9.5: base*=10
    if not halfs and round(x/base)!=x/base: return ''
    if afterDotDigits>0:
        digitstr = (r'%.'+str(afterDotDigits)+'f')%(x/base)
    else:
        digitstr = r'%d'%iround(x/base)
    expstr = r'10^{%d}'%(log(float(base)))		
    if x==base or digitstr=='1': return r'$%s$'%expstr
    return r'$%s\cdot%s$'%(digitstr,expstr)

def iround(f,modulo=1):
    if isinstance(f,np.ndarray):
        if modulo >= 1: return np.round(f/modulo).astype(int)*modulo
        else: return np.round(f/modulo).astype(int) / (1/modulo)  #patch for floating point bug
    if modulo >= 1: return int(round(f/modulo))*modulo
    else: return int(round(f/modulo)) / (1/modulo)  #patch for floating point bug
ndigits = lambda f,maxdigits: ([(f*10**x)%1==0 for x in range(maxdigits)] + [True]).index(True)
intandfloatFunc = lambda x,pos=0: ('%.'+str(ndigits(x,2))+'f')%x
def arilogformat(x,pos=None,dollarsign=True,logbase=1,with1000=True,showHalfs=False):
    if not (showHalfs and str(x).strip('0.')[0]=='3'):
        if x==0 or iround(log(abs(x)))%logbase!=0: return ''
    if True in [abs(abs(x)-a)/a<1e-6 for a in [1,1.,3.,10,30.,100]+([],[1000])[with1000]]:
        s = '$%d$'%x
    elif True in [abs(abs(x)-a)/a<1e-6 for a in (0.03,0.01,0.3,0.1)]:
        s = '$%s$'%intandfloatFunc(x)
    else:
        s = matplotlib.ticker.LogFormatterMathtext()(x)
    if dollarsign: 	return s
    else: return s[1:-1]
arilogformatter = matplotlib.ticker.FuncFormatter(arilogformat)
def labelsize(increase,large=16,small=10):
    for param in 'font.size', 'xtick.labelsize','ytick.labelsize','legend.fontsize':
        pl.rcParams[param]=(small,large)[increase]
    pl.rcParams['axes.labelsize'] = (small,large)[increase] + 4

def nSignificantDigits(N,sigdig,retstr=False):
    if N==0.: 
        if retstr: return '0'
        return 0.
    sn = abs(N)/N
    N = abs(N)
    maxpower = int(log(N))
    if log(N)<0 and maxpower!=log(N): maxpower-=1
    afterpoint = maxpower+1-sigdig
    val = sn * iround(N, 10**afterpoint)
    if retstr: 
        if afterpoint>=0: return '%d'%val
        else: 
            s = ('%.'+'%d'%(-afterpoint)+'f')%val
            if s[-2:] == '.0': s = s[:-2]  #patch for nSigDig(0.997,1,True)
            return s
    return val
