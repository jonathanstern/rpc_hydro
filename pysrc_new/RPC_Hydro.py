import time, os, sys
import numpy as np
from numpy import log as ln, log10 as log
from scipy import interpolate
from astropy import units as un, constants as cons
import work_paths as wp;runsDir,rpc_hydro_dir=wp.work_paths()



sys.path.append(rpc_hydro_dir+'pysrc_new/' )
from parameters import *
from pylab import histogram

import pyPLUTO, emission
from parameters import *
from plot_utils import *
from Table import PLUTO_Table 


class Property():
    def __init__(self,snapshot,symbol,description,unitsStr,varNameCodeUnits=None,codeUnitsToCGS=None,otherProperty=None,function=None,dimension=None,scientificFormat=True):
        self.snapshot = snapshot
        # hide original variable
        self.symbol = symbol
        self.description = description
        self.unitsStr = unitsStr
        self.scientificFormat = scientificFormat
        if function == None:       
            if hasattr(self.snapshot, varNameCodeUnits): #verify this property was indeed read from outputs
                newVarName = '_' + varNameCodeUnits
                setattr(self.snapshot, newVarName, getattr(self.snapshot,varNameCodeUnits) )
                delattr(self.snapshot, varNameCodeUnits)
                self.isFunction = False
                self.varNameCodeUnits = newVarName
                self.codeUnitsToCGS = codeUnitsToCGS
                self.read = True
            else:
                self.read = False
        else:
            self.read = True
            self.isFunction = True
            self.functionOfOtherProperty = function 
            self.otherProperty = otherProperty
        if dimension!=None: self.dimension = dimension
        elif varNameCodeUnits!=None and varNameCodeUnits[0]=='x': self.dimension = int(varNameCodeUnits[1])
        else: self.dimension = range(1,(2,3)[len(self.snapshot.dx3)>1]+1)
    def nDims(self):
        return len(self.shape())
    def codeUnits(self):
        if not self.read: 
            print(str(self) + 'not read')
            raise
        assert(not self.isFunction)
        return getattr(self.snapshot, self.varNameCodeUnits)
    def cgs(self,sliceObj=None):
        if not self.read: 
            print(str(self) + 'not read')
            raise            
        if self.isFunction:
            if type(self.otherProperty)==type(()):
                val = self.functionOfOtherProperty(*self.otherProperty)
            else:
                val = self.functionOfOtherProperty(self.otherProperty)
        else:
            val = self.codeUnits() * self.codeUnitsToCGS
        return np.array(val)
    def tiled_cgs(self,tiledByProperty,tile_d=False):
        if not tile_d: prop = self.cgs()
        else: prop = self.d()
        if tiledByProperty.nDims()==2:
            if self.dimension==1:
                return np.tile(prop,tiledByProperty.shape()[1]).reshape(tiledByProperty.shape(),order='F')
            if self.dimension==2:
                return np.tile(prop,tiledByProperty.shape()[0]).reshape(tiledByProperty.shape(),order='C')
        
        
    def shape(self):
        return self.cgs().shape
    def d(self,dimension=None):
        if self.nDims()==1:
            padded_array = np.pad(self.cgs(),[(1,1)],mode='reflect',reflect_type='odd')
            return (padded_array[2:] - padded_array[:-2])/2        
        if self.nDims()==2:
            if dimension==1:
                padded_array = np.pad(self.cgs(),[(1,1), (0,0)],mode='reflect',reflect_type='odd')
                return (padded_array[2:,:] - padded_array[:-2,:])/2
            if dimension==2:
                padded_array = np.pad(self.cgs(),[(0,0),(1,1)],mode='reflect',reflect_type='odd')
                return (padded_array[:,2:] - padded_array[:,:-2])/2
        if self.nDims()==3:
            if dimension==1:
                padded_array = np.pad(self.cgs(),[(1,1), (0,0),(0,0)],mode='reflect',reflect_type='odd')
                return (padded_array[2:,:,:] - padded_array[:-2,:,:])/2
            if dimension==2:
                padded_array = np.pad(self.cgs(),[(0,0),(1,1),(0,0)],mode='reflect',reflect_type='odd')
                return (padded_array[:,2:,:] - padded_array[:,:-2,:])/2
            if dimension==3:
                padded_array = np.pad(self.cgs(),[(0,0),(0,0),(1,1)],mode='reflect',reflect_type='odd')
                return (padded_array[:,:,2:] - padded_array[:,:,:-2])/2        
            
    def __str__(self):
        if self.unitsStr!='':
            return r'$%s~[%s]$'%(self.symbol,self.unitsStr)
        else:
            return r'$%s$'%(self.symbol)
    def __repr__(self):
        return self.__str__()
    def formatStr(self,x):
        if self.scientificFormat:
            return '%s'%myScientificFormat(x,halfs=True,afterDotDigits=0)
        return '%.0f'%x
    def fullStr(self,withvar=True):
        if withvar:
            return r'$%s=$ %s $\,%s$'%(self.symbol,self.formatStr(self.cgs()),self.unitsStr)
        else:
            return r'%s$\,%s$'%(self.formatStr(self.cgs()),self.unitsStr)
#class TableProperty(Property):
    #def cgs(self,slicesTuple=None):
        #if type(self.otherProperty)==type(()):
            #return self.functionOfOtherProperty(*tuple([x.cgs()[slicesTuple] for x in self.otherProperty]))
        #else:
            #return self.functionOfOtherProperty(self.otherProperty.cgs()[slicesTuple])

#class LambdaProperty(TableProperty):
    #def cgs(self,slicesTuple=None):
        #normedLambda = self.functionOfOtherProperty(*tuple([x.cgs()[slicesTuple] for x in self.otherProperty[1:]]))
        #return normedLambda * self.otherProperty[0].cgs()[slicesTuple]**2
    
    
class Snapshot(pyPLUTO.pload):
    def varsToRead(self):
        if self._varsToRead==None: return self.vars
        else: return self._varsToRead
    def __init__(self,run,*args,**kwargs):  
        self._varsToRead = run.varsToRead
        pyPLUTO.pload.__init__(self,*args,**kwargs)
        self.rho   = Property(self,r'\rho',       'mass volume density',  r'{\rm gr}~{\rm cm}^{-3}',varNameCodeUnits='rho',codeUnitsToCGS= run.rho0)
        self.nH    = Property(self,r'n_{\rm H}','Hydrogen volume density',r'{\rm cm}^{-3}',otherProperty=self.rho,
                                       function=lambda rho: rho.cgs() * X / cons.m_p.to('g').value)
        
        
        self.P     = Property(self,r'P_{\rm gas}','gas thermal pressure', r'{\rm dyn~cm}^{-2}',       varNameCodeUnits='prs',codeUnitsToCGS= run.rho0 * run.v0**2)
        self.v_r     = Property(self,r'v_{\rm r}','radial velocity', r'{\rm km} {\rm s}^{-1}',        varNameCodeUnits='vx1',codeUnitsToCGS= run.v0/1e5)
        self.v_theta = Property(self,r'v_{\theta}','perpendicular velocity', r'{\rm km} {\rm s}^{-1}',varNameCodeUnits='vx2',codeUnitsToCGS= run.v0/1e5)
        self.Fion  = Property(self,r'F_{\rm ion}','ionizing flux', r'{\rm erg} {\rm s}^{-1} {\rm cm}^{-2}',            varNameCodeUnits='FluxIon',codeUnitsToCGS= 1e13)
        self.Fopt  = Property(self,r'F_{\rm ion}','optical flux',  r'{\rm erg} {\rm s}^{-1} {\rm cm}^{-2}',            varNameCodeUnits='FluxOpt',codeUnitsToCGS= 1e13)
        self.t     = Property(self,r't',          'time',                 r'{\rm kyr}',              varNameCodeUnits='SimTime', codeUnitsToCGS=0.001,dimension=-1,scientificFormat=False)
        self.r     = Property(self,r'r',          'distance',             r'{\rm pc}',              varNameCodeUnits='x1', codeUnitsToCGS=1.)
        self.theta = Property(self,r'\theta',     'polar coordinate',     r'',                      varNameCodeUnits='x2', codeUnitsToCGS=1.)
        self.fHII  = Property(self,r'f_{\rm HII}',r'HII fraction'        ,r'',                      varNameCodeUnits='ionx',codeUnitsToCGS=1.) 
        self.fHI   = Property(self,r'f_{\rm HI}', r'HI fraction'         ,r'',otherProperty=self.fHII,
                              function = lambda fHII:1-fHII.cgs())
        self.mass_flux = Property(self,r'rho v_r', r'radial mass flux'   ,r'g {\rm cm}^{-2} {\rm s}^{-1}',otherProperty=(self.rho,self.v_r),
                              function = lambda rho,v_r:rho.cgs() * v_r.cgs() * 1e5)
        self.mu    = Property(self,r'\mu','mean molecular weight','',otherProperty=self.fHI,
                              function = lambda fHI,X=X,Y=Y,Z=Z,AH=AH,AHe=AHe,AZ=AZ: 1./((1.-fHI.cgs())*(X+Y/2.+Z/2.) + X/AH + Y/AHe + Z/AZ))
        self.T     = Property(self,r'T','gas temperature',r'{\rm K}',otherProperty=(self.P,self.rho,self.mu),
                              function = lambda P,rho,mu: self.mu.cgs() * cons.m_p.to('g').value * P.cgs()/rho.cgs() / cons.k_B.to('erg/K').value)
        self.Toutput = Property(self,r'T','gas temperature',r'{\rm K}',varNameCodeUnits='Tgas',codeUnitsToCGS= 10.**10.5)
        
        self.A     = Property(self,r'A','cell area',r'{\rm cm}^2',otherProperty=(self.r,self.theta,self.rho),
                                  function=lambda r,theta,rho: (r.tiled_cgs(rho) * theta.tiled_cgs(rho,tile_d=True) *
                                                                    2. * np.pi * theta.tiled_cgs(rho) * r.tiled_cgs(rho) *
                                                                   un.pc.to('cm')**2))
        self.V     = Property(self,r'V','cell volume',r'{\rm cm}^3',otherProperty=(self.r,self.A,self.rho),
                             function=lambda r,A,rho: r.tiled_cgs(rho,tile_d=True) * un.pc.to('cm') * A.cgs())
        self.Omega = Property(self,r'\Omega','cell covering factor',r'',otherProperty=self.A,
                             function=lambda A,r=run.r0: A.cgs() / (4*np.pi*r**2) )
        self.m     = Property(self,r'm','cell mass',r'g',otherProperty=(self.V,self.rho),
                              function=lambda V,rho: V.cgs() * rho.cgs() )
                             
        
        self.NH     = Property(self,r'N_{\rm H}',  'H column',             r'{\rm cm}^{-2}',         varNameCodeUnits='NH', codeUnitsToCGS=1.)
        self.NHI    = Property(self,r'N_{\rm HI}', 'HI column',            r'{\rm cm}^{-2}',         varNameCodeUnits='NHI',codeUnitsToCGS=1.)
        self.tau_HI = Property(self,r'\tau_{\rm HI} (1ryd)', 'HI optical depth at 1 rydberg',  r'',otherProperty=self.NHI, function=lambda NHI,sigma=sigma_HI_one_rydberg: NHI.cgs()*sigma)
        self.tau_dust = Property(self,r'\tau_{\rm dust} (1ryd)', 'dust optical depth at 1 rydberg',  r'',otherProperty=self.NH, function=lambda NH,sigma=sigma_dust_one_rydberg: NH.cgs()*sigma)
        self.F0     = Property(self,r'F_0','Flux without absorption',r'{\rm erg}~{\rm s}^{-1}~{\rm cm}^{-2}',otherProperty=self.r,          
                              function=lambda r,L=run.LAGN: L / (4*np.pi*(r.cgs()*un.pc.to('cm'))**2),dimension=self.r.dimension)
        self.Ftotal = Property(self,r'F_{\rm total}','total Flux',r'{\rm erg}~{\rm s}^{-1}~{\rm cm}^{-2}',otherProperty=(self.Fion,self.Fopt),
                              function=lambda Fion,Fopt: Fion.cgs()+Fopt.cgs())
        self.F2F0   = Property(self,r'F/F_0','flux fraction',r'',otherProperty=(self.F0,self.Ftotal),
                                   function=lambda F0,Ftotal: Ftotal.cgs()/F0.tiled_cgs(tiledByProperty=Ftotal) )
        self.Fion2Fion0 = Property(self,r'F^{\rm ion}/F^{\rm ion}_0','ionizing flux fraction',r'',otherProperty=(self.F0,self.Fion),
                                   function=lambda F0,Fion,optFrac=run.optFrac: Fion.cgs()/((1-optFrac)*F0.tiled_cgs(tiledByProperty=Ftotal)) )
        self.Prad1  = Property(self,r'P_{\rm rad}','absorbed radiation pressure',r'{\rm dyn}',otherProperty=(self.F0,self.Ftotal),
                              function=lambda F0,Ftotal: (F0.tiled_cgs(tiledByProperty=Ftotal)-Ftotal.cgs())/cons.c.to('cm/s').value)
        self.Prad12k= Property(self,r'P_{\rm rad}/k','absorbed radiation pressure (nT)', r'{\rm cm}^{-3}{\rm K}',otherProperty=self.Prad1,            
                                          function=lambda Prad1: Prad1.cgs()/cons.k_B.to('erg/K').value)
        self.Xi    = Property(self,r'\Xi','rad pressure to gas pressure', r'',otherProperty=(self.F0,self.P),
                                  function=lambda F0,P: F0.tiled_cgs(tiledByProperty=P)/np.array(cons.c.to('cm/s').value)/P.cgs() )
        self.U0     = Property(self,r'U_0','ionization parameter without absorption',r'',otherProperty=(self.F0,self.nH),
                              function=lambda F0,nH,ionfrac=ion_fraction,hnu=mean_hnu: (
                                  F0.tiled_cgs(tiledByProperty=nH) *ionfrac / hnu / 
                                  np.array(cons.c.to('cm/s').value)/nH.cgs()))
   
        self.P2k           = Property(self,r'P_{\rm gas}/k','gas thermal pressure (nT)', r'{\rm cm}^{-3}{\rm K}',otherProperty=self.P,            
                                      function=lambda P: P.cgs()/cons.k_B.to('erg/K').value)
        self.nHT           = Property(self,r'n_{\rm H}T','gas thermal pressure (nT)', r'{\rm cm}^{-3}{\rm K}',otherProperty=(self.nH,self.T),
                                      function=lambda nH,T: nH.cgs()*T.cgs())

        self.x             = Property(self,r'x', 'perpendicular cartesian coordinate',r'{\rm pc}',otherProperty=(self.theta,self.r),
                                      function=lambda theta,r:r.cgs().mean()*theta.cgs(),dimension=2)
        
        self.z             = Property(self,r'z', 'parallel cartesian coordinate',r'{\rm pc}',otherProperty=self.r,             
                                      function=lambda r:r.cgs()-r.cgs().mean(),dimension=1)
        
        self.propertyDict = {}
        properties = ['rho','P','t','r','theta','nH','P2k','nHT','T','x','z','V','A','Omega','Xi','Fion','mass_flux',
                      'Fopt','Ftotal','F2F0','Fion2Fion0','Prad1','Prad12k','mu','fHI','v_r','v_theta']
        properties += ['NH','NHI','F0','U0','tau_HI']
        for k in properties:
            self.propertyDict[k] = getattr(self,k)
                
        ## table based properties
        self.Lambda = Property(self,r'\Lambda',r'cooling function',r'{\rm erg}~{\rm s}^{-1}~{\rm cm}^{3}',
                                    otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.absCoolingTable.properties]),
                                    function=lambda *props: PLUTO_Table.__call__(run.absCoolingTable,*tuple([x.cgs() for x in props])))
        
        self.coolingPerUnitVolume = Property(self,r'n_{\rm H}^2\Lambda',r'cooling per unit Volume',r'{\rm erg}~{\rm s}^{-1}~{\rm cm}^{-3}',
                                     otherProperty=(self.nH,self.Lambda),
                                     function=lambda nH,Lambda: nH.cgs()**2*Lambda.cgs())
        self.Lgas        = Property(self,r'L_{\rm gas}','cell gas luminosity',r'{\rm erg~s}^{-1}',otherProperty=(self.coolingPerUnitVolume,self.V),
                              function=lambda coolingPerUnitVolume,V: coolingPerUnitVolume.cgs()*V.cgs())
        self.Lgas2Omega  = Property(self,r'L_{\rm gas}/\Omega','cell gas luminosity per unit covering factor',r'{\rm erg~s}^{-1}',otherProperty=(self.Lgas,self.Omega),
                                  function=lambda L,Omega: L.cgs()/Omega.cgs())
        
        for k in 'coolingPerUnitVolume','Lambda','Lgas','Lgas2Omega':
            self.propertyDict[k] = getattr(self,k)
        
        self.gasIonOpacity  = Property(self,r'\sigma_{\rm gas,ion}',r'ion gas opacity',r'{\rm cm^2~per~H-atom}',
                                           otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.gasIonOpacityTable.properties]),
                                           function=lambda *props: PLUTO_Table.__call__(run.gasIonOpacityTable,*tuple([x.cgs() for x in props]))*cons.m_p.to('g').value/X)
        self.dustIonOpacity = Property(self,r'\sigma_{\rm dust,ion}',r'ion dust opacity',r'{\rm cm^2~per~H-atom}',
                                            otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.dustIonOpacityTable.properties]),
                                            function=lambda *props: PLUTO_Table.__call__(run.dustIonOpacityTable,*tuple([x.cgs() for x in props]))*cons.m_p.to('g').value/X*dust2gasRatio)
        self.dustOptOpacity = Property(self,r'\sigma_{\rm dust,opt}',r'opt dust opacity',r'{\rm cm^2~per~H-atom}',
                                            otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.dustOptOpacityTable.properties]),
                                            function=lambda *props: PLUTO_Table.__call__(run.dustOptOpacityTable,*tuple([x.cgs() for x in props]))*cons.m_p.to('g').value/X*dust2gasRatio)           
        self.fHI_Table      = Property(self,r'f_{\rm HI}',r'HI fraction',r'',
                                           otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.fHITable.properties]),
                                                function=lambda *props: PLUTO_Table.__call__(run.fHITable,*tuple([x.cgs() for x in props])))           
    
        
        self.Ldust          = Property(self,r'L_{\rm dust}','cell dust luminosity',r'{\rm erg~s}^{-1}',otherProperty=(self.Fion,self.Fopt,self.A,self.dustIonOpacity,self.dustOptOpacity,self.NH),
                                        function=lambda Fion,Fopt,A,sigma_ion,sigma_opt,NH: (Fion.cgs()*sigma_ion.cgs() + Fopt.cgs()*sigma_opt.cgs()) * A.cgs()*NH.d(dimension=1))
        self.Ldust2Omega = Property(self,r'L_{\rm dust}/\Omega','cell dust per unit covering factor',r'{\rm erg~s}^{-1}',otherProperty=(self.Ldust,self.Omega),
                                        function=lambda L,Omega: L.cgs()/Omega.cgs())
        self.Liondust       = Property(self,r'L_{\rm dust}^{\rm ion}','cell dust luminosity due to ionizing photons',r'{\rm erg~s}^{-1}',otherProperty=(self.Fion,self.A,self.dustIonOpacity,self.NH),
                                           function=lambda Fion,A,sigma_ion,NH: Fion.cgs()*sigma_ion.cgs() * A.cgs()*NH.d(dimension=1))
        self.Liondust2Omega = Property(self,r'L_{\rm dust}^{\rm ion}/\Omega','cell dust per unit covering factor',r'{\rm erg~s}^{-1}',otherProperty=(self.Liondust,self.Omega),
                                        function=lambda L,Omega: L.cgs()/Omega.cgs())
        
        #self.Lattenuated     = Property(self,r'L e^{-\tau_{\rm d}}','attentuated cell luminosity',r'{\rm erg~s}^{-1}',
                                        #otherProperty=(self.Lgas,self.tau_d),
                              #function=lambda L,tau_d,norm=3.: L.cgs()*e**(-tau_d.cgs()/norm))
        
        self.particlesPerH  = Property(self,r'n/n_{\rm H}',r'particles per hydrogen',r'',otherProperty=self.mu,function=lambda mu,X=X: (1./X/mu))
        
        self.nHI            = Property(self,r'n_{\rm HI}','neutral hydrogen density',r'{\rm cm}^{-3}',otherProperty=(self.nH,self.fHI),
                                       function=lambda nH,fHI: nH.cgs()*fHI.cgs() )   
        self.nHII           = Property(self,r'n_{\rm HII}','ionized hydrogen density',r'{\rm cm}^{-3}',otherProperty=(self.nH,self.fHI),
                                           function=lambda nH,fHI: nH.cgs()*(1-fHI.cgs()) )   
        
        self.sigma = Property(self,r'\sigma','spectrum averaged opacity',r'{\rm cm^2~per~H-atom}',
                              otherProperty=(self.gasIonOpacity,self.dustIonOpacity,self.dustOptOpacity),
                              function=lambda gasIon,dustIon,dustOpt: (
                                  (1-ion_fraction)*dustOpt.cgs()
                                  + ion_fraction*(dustIon.cgs()+ gasIon.cgs())))
        self.tau = Property(self,r'\tau','spectrum averaged optical depth',r'', #int(sigma nH dr)
                            otherProperty=(self.sigma,self.nH,self.r),
                            function=lambda sigma,nH,r: (sigma.cgs()*nH.cgs()*r.tiled_cgs(nH,tile_d=True)*un.pc.to('cm')).cumsum(axis=0))
        
        for k in ('gasIonOpacity','dustIonOpacity','dustOptOpacity','nHI','nHII','Ldust','Ldust2Omega',
                  'Liondust','Liondust2Omega','fHI_Table','sigma','tau'):   
            self.propertyDict[k] = getattr(self,k)
        

        ###### BPT stuff
        self.propertyDict['NHIs'] = self.NHI
        self.propertyDict['AVs'] = self.NH
        self.propertyDict['Temperatures'] = self.T
        self.propertyDict['Densities'] = self.nH
        
        self.ne2nH          = Property(self,r'ne / nH',r'electron abundance',r'',
                                    otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.elec_table.properties]),
                                    function=lambda *props: PLUTO_Table.__call__(run.elec_table,*tuple([x.cgs() for x in props])))        
        self.nOIII2nH       = Property(self,r'nOIII / nH',r'OIII abundance',r'',
                                    otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.OIII_table.properties]),
                                    function=lambda *props: PLUTO_Table.__call__(run.OIII_table,*tuple([x.cgs() for x in props])))        
        self.nNII2nH        = Property(self,r'nNII / nH',r'NII abundance',r'',
                                    otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.NII_table.properties]),
                                    function=lambda *props: PLUTO_Table.__call__(run.NII_table,*tuple([x.cgs() for x in props])))        
        self.nSII2nH        = Property(self,r'nSII / nH',r'SII abundance',r'',
                                    otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.SII_table.properties]),
                                    function=lambda *props: PLUTO_Table.__call__(run.SII_table,*tuple([x.cgs() for x in props])))        
        self.nOI2nH         = Property(self,r'nOI / nH',r'OI abundance',r'',
                                    otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.OI_table.properties]),
                                    function=lambda *props: PLUTO_Table.__call__(run.OI_table,*tuple([x.cgs() for x in props])))        
        self.nOII2nH         = Property(self,r'nOII / nH',r'OII abundance',r'',
                                    otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.OII_table.properties]),
                                    function=lambda *props: PLUTO_Table.__call__(run.OII_table,*tuple([x.cgs() for x in props])))        
        self.nNeIII2nH      = Property(self,r'nNeIII / nH',r'NeIII abundance',r'',
                                    otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.NeIII_table.properties]),
                                    function=lambda *props: PLUTO_Table.__call__(run.NeIII_table,*tuple([x.cgs() for x in props])))        
        self.nNeV2nH        = Property(self,r'nNeV / nH',r'NeV abundance',r'',
                                    otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.NeV_table.properties]),
                                    function=lambda *props: PLUTO_Table.__call__(run.NeV_table,*tuple([x.cgs() for x in props])))        
        self.LHa            = Property(self,r'L_{H\alpha}', 'Ha luminosity', r'{\rm erg}\ {\rm s}^{-1}',otherProperty=(self.T,self.nHII,self.ne2nH,self.nH,self.NH,self.V),
                                       function=lambda T,nHII,ne2nH,nH,NH,V:  emission.L_Ha(T.cgs()/1e4,nHII.cgs(),ne2nH.cgs()*nH.cgs())*emission.extinction(NH.cgs(),emission.E_Ha)*V.cgs())
        self.LHa_noExtinct  = Property(self,r'L_{H\alpha}', 'Ha luminosity', r'{\rm erg}\ {\rm s}^{-1}',otherProperty=(self.T,self.nHII,self.ne2nH,self.nH,self.V),
                                       function=lambda T,nHII,ne2nH,nH,V:  emission.L_Ha(T.cgs()/1e4,nHII.cgs(),ne2nH.cgs()*nH.cgs())*V.cgs())
        
        self.LHb            = Property(self,r'L_{H\beta}', 'Hb luminosity', r'{\rm erg}\ {\rm s}^{-1}',otherProperty=(self.T,self.nHII,self.ne2nH,self.nH,self.NH,self.V),
                                       function=lambda T,nHII,ne2nH,nH,NH,V:  emission.L_Hb(T.cgs()/1e4,nHII.cgs(),ne2nH.cgs()*nH.cgs())*emission.extinction(NH.cgs(),emission.E_Hb)*V.cgs())
        #self.LOIII          = Property(self,r'L_{5007}', '[OIII] luminosity', r'{\rm erg}\ {\rm s}^{-1}',otherProperty=(self.T,self.nH,self.ne2nH,self.nOIII2nH,self.nHII,self.NH,self.V),
                                       #function=lambda T,nH,ne2nH,nOIII2nH,nHII,NH,V: emission.LOIIIs(nOIII2nH.cgs()*nH.cgs(),nHII.cgs(),T.cgs()/1e4,ne2nH.cgs()*nH.cgs())*emission.extinction(NH.cgs(),emission.E_OIIIa)*V.cgs())
        self.LNII           = Property(self,r'L_{6584}', '[NII] luminosity', r'{\rm erg}\ {\rm s}^{-1}', otherProperty=(self.T,self.nH,self.ne2nH,self.nNII2nH,self.nHII,self.NH,self.V),
                                       function=lambda T,nH,ne2nH,nNII2nH,nHII,NH,V:  emission.LNIIs(nNII2nH.cgs()*nH.cgs(),nHII.cgs(),T.cgs()/1e4,ne2nH.cgs()*nH.cgs())*emission.extinction(NH.cgs(),emission.E_NII)*V.cgs())
        self.LSII            = Property(self,r'L_{6716+6731}', '[SII] luminosity', r'{\rm erg}\ {\rm s}^{-1}',otherProperty=(self.T,self.nH,self.ne2nH,self.nSII2nH,self.NH,self.V),
                                       function=lambda T,nH,ne2nH,nSII2nH,NH,V: (emission.LSII_6716(T.cgs()/1e4,nSII2nH.cgs()*nH.cgs(),ne2nH.cgs()*nH.cgs())*emission.extinction(NH.cgs(),emission.E_SII6716) +
                                                                                 emission.LSII_6731(T.cgs()/1e4,nSII2nH.cgs()*nH.cgs(),ne2nH.cgs()*nH.cgs())*emission.extinction(NH.cgs(),emission.E_SII6731))*V.cgs())
        self.LOI            = Property(self,r'L_{6300}', '[OI] luminosity', r'{\rm erg}\ {\rm s}^{-1}',otherProperty=(self.T,self.nH,self.ne2nH,self.nOI2nH,self.NH,self.V),
                                       function=lambda T,nH,ne2nH,nOI2nH,NH,V:  emission.LOI(T.cgs()/1e4,nOI2nH.cgs()*nH.cgs(),ne2nH.cgs()*nH.cgs())*emission.extinction(NH.cgs(),emission.E_OI)*V.cgs())
        self.LOIIIa        = Property(self,r'L_{5007}', '[OIII] luminosity', r'{\rm erg}\ {\rm s}^{-1}',otherProperty=(self.T,self.nH,self.ne2nH,self.nOIII2nH,self.NH,self.V),
                                       function=lambda T,nH,ne2nH,nOIII2nH,NH,V:  emission.LOIIIa(T.cgs()/1e4,nOIII2nH.cgs()*nH.cgs(),ne2nH.cgs()*nH.cgs())*emission.extinction(NH.cgs(),emission.E_OIIIa)*V.cgs())
        self.LOIIIb        = Property(self,r'L_{4961}', '[OIII] luminosity', r'{\rm erg}\ {\rm s}^{-1}',otherProperty=(self.T,self.nH,self.ne2nH,self.nOIII2nH,self.NH,self.V),
                                       function=lambda T,nH,ne2nH,nOIII2nH,NH,V:  emission.LOIIIb(T.cgs()/1e4,nOIII2nH.cgs()*nH.cgs(),ne2nH.cgs()*nH.cgs())*emission.extinction(NH.cgs(),emission.E_OIIIa)*V.cgs())
        self.LNeIIIa        = Property(self,r'L_{3968}', '[NeIII] luminosity', r'{\rm erg}\ {\rm s}^{-1}',otherProperty=(self.T,self.nH,self.ne2nH,self.nNeIII2nH,self.NH,self.V),
                                       function=lambda T,nH,ne2nH,nNeIII2nH,NH,V:  emission.LNeIIIa(T.cgs()/1e4,nNeIII2nH.cgs()*nH.cgs(),ne2nH.cgs()*nH.cgs())*emission.extinction(NH.cgs(),emission.E_NeIIIa)*V.cgs())
        self.LNeIIIb        = Property(self,r'L_{3869}', '[NeIII] luminosity', r'{\rm erg}\ {\rm s}^{-1}',otherProperty=(self.T,self.nH,self.ne2nH,self.nNeIII2nH,self.NH,self.V),
                                       function=lambda T,nH,ne2nH,nNeIII2nH,NH,V:  emission.LNeIIIb(T.cgs()/1e4,nNeIII2nH.cgs()*nH.cgs(),ne2nH.cgs()*nH.cgs())*emission.extinction(NH.cgs(),emission.E_NeIIIa)*V.cgs())
        self.LNeVa          = Property(self,r'L_{3426}', '[NeV] luminosity', r'{\rm erg}\ {\rm s}^{-1}',otherProperty=(self.T,self.nH,self.ne2nH,self.nNeV2nH,self.NH,self.V),
                                       function=lambda T,nH,ne2nH,nNeV2nH,NH,V:  emission.LNeVa(T.cgs()/1e4,nNeV2nH.cgs()*nH.cgs(),ne2nH.cgs()*nH.cgs())*emission.extinction(NH.cgs(),emission.E_NeVa)*V.cgs())
        self.LNeVb          = Property(self,r'L_{3346}', '[NeV] luminosity', r'{\rm erg}\ {\rm s}^{-1}',otherProperty=(self.T,self.nH,self.ne2nH,self.nNeV2nH,self.NH,self.V),
                                       function=lambda T,nH,ne2nH,nNeV2nH,NH,V:  emission.LNeVb(T.cgs()/1e4,nNeV2nH.cgs()*nH.cgs(),ne2nH.cgs()*nH.cgs())*emission.extinction(NH.cgs(),emission.E_NeVa)*V.cgs())
        self.LOIIa          = Property(self,r'L_{3729}', '[OII] luminosity', r'{\rm erg}\ {\rm s}^{-1}',otherProperty=(self.T,self.nH,self.ne2nH,self.nOII2nH,self.NH,self.V),
                                       function=lambda T,nH,ne2nH,nOII2nH,NH,V:  emission.LOIIa(T.cgs()/1e4,nOII2nH.cgs()*nH.cgs(),ne2nH.cgs()*nH.cgs())*emission.extinction(NH.cgs(),emission.E_OIIa)*V.cgs())
        self.LOIIb          = Property(self,r'L_{3727}', '[OII] luminosity', r'{\rm erg}\ {\rm s}^{-1}',otherProperty=(self.T,self.nH,self.ne2nH,self.nOII2nH,self.NH,self.V),
                                       function=lambda T,nH,ne2nH,nOII2nH,NH,V:  emission.LOIIb(T.cgs()/1e4,nOII2nH.cgs()*nH.cgs(),ne2nH.cgs()*nH.cgs())*emission.extinction(NH.cgs(),emission.E_OIIb)*V.cgs())
        for k in ('ne2nH','nOIII2nH','nNII2nH','nSII2nH','nOI2nH',
                  'LHa','LHa_noExtinct','LHb','LOIIIa','LOIIIb','LNII','LSII','LOI',
                  'LNeIIIa','LNeIIIa','LNeVa','LNeVb','LOIIb','LOIIb',):   
            self.propertyDict[k] = getattr(self,k)
                
            
    def __getitem__(self,k):
        if type(k)==type(''):
            return self.propertyDict[k]
        elif type(k)==type((None,)):
            if type(k[0])==type(lambda x: None):
                return self.funcProperty(k[0],k[1],*k[2:])
            elif len(k)==2:
                return self.derivative(k[0], k[1])
    def derivative(self,property1, property2):
        if type(property1) in (type(''),type((None,))): property1 = self[property1]
        if type(property2) in (type(''),type((None,))): property2 = self[property2]                
        return Property(self, r'{\rm d}%s / {\rm d}%s'%(property1.symbol,property2.symbol),'d(%s)/d(%s)'%(property1.description,property2.description), 
                        unitsStr = '%s / %s'%(property1.unitsStr, property2.unitsStr),
                        otherProperty=(property1,property2),
                        function = lambda property1, property2: _derivativeFunc(property1, property2))        
        
    def integrate(self, integrand, integrateOver,factor=1.,name=None,desc=None,unitsStr=None):
        
        if type(integrand)     in (type(''),type((None,))): integrand     = self[integrand]
        if type(integrateOver) in (type(''),type((None,))): integrateOver = self[integrateOver]       
        if name==None: name = r'int %s {\rm d}%s'%(integrand.symbol,integrateOver.symbol)
        if desc==None: desc = r'int (%s) d(%s)'%(integrand.description,integrateOver.description), 
        if unitsStr==None: unitsStr = '%s %s'%(property1.unitsStr, property2.unitsStr),
        return Property(self, name,desc,unitsStr,
                        otherProperty=(integrand,integrateOver),
                        function = lambda integrand, integrateOver,factor=factor: _integrateFunc(integrand, integrateOver)*factor)        
        
    def funcProperty(self,func,s,*otherProperties):
        props = [None]*len(otherProperties)
        for i in rl(otherProperties):
            if type(otherProperties[i])  in (type(''),type((None,))): props[i] = self[otherProperties[i]]
            else: props[i] = otherProperties[i]
        return Property(self, s,'','',otherProperty=tuple(props),function=func)

        


class Run:
    runsDir,rpc_hydro_dir=wp.work_paths()
    #runsDir = runsDir
    def __init__(self,name,nTimeSteps,r,v0,L0,rho0,L,optFrac,f_abundances=None, varsToRead=None):
        self.name = name
        self.dirName = runsDir + self.name + '/'
        self.dataDirName = self.dirName 
        self.dataDirName += 'data/'
        self.tableFilename = runsDir + 'output_table.txt'
        self.netCoolingTable        = PLUTO_Table(('U0','T','NH','NHI'),self.tableFilename,firstLine=1,calculatedValueColumn=6)
        self.absCoolingTable        = PLUTO_Table(('U0','T','NH','NHI'),self.tableFilename,firstLine=1,calculatedValueColumn=5)
        self.gasIonOpacityTable     = PLUTO_Table(('U0','T','NH','NHI'),self.tableFilename,firstLine=1,calculatedValueColumn=2)
        self.dustIonOpacityTable    = PLUTO_Table(('U0','T','NH','NHI'),self.tableFilename,firstLine=1,calculatedValueColumn=1)
        self.dustOptOpacityTable    = PLUTO_Table(('U0','T','NH','NHI'),self.tableFilename,firstLine=1,calculatedValueColumn=0)
        self.fHITable               = PLUTO_Table(('U0','T','NH','NHI'),self.tableFilename,firstLine=1,calculatedValueColumn=3)
        
        self.r0 = r*un.pc.to('cm')
        self.v0,self.L0, self.rho0 = v0, L0, rho0        
        self.LAGN = L
        self.optFrac = optFrac
        self.varsToRead = varsToRead
        
        self._data = [None for iT in range(nTimeSteps)]
        
        if f_abundances!=None:
            self.elec_table, self.OIII_table, self.NII_table, self.SII_table, self.OI_table, self.NeIII_table, self.NeV_table, self.OII_table = [
                PLUTO_Table(('NHIs','AVs','Temperatures','Densities'),
                            chimes_output = f_abundances,calculatedValueColumn = speciesIndex(s))
                for s in ('elec','OIII','NII','SII','OI','NeIII','NeV','OII')]        
    def Prad(self):
        return self.LAGN*un.erg/un.s / (4*np.pi*(self.r0*un.cm)**2*cons.c)
    def Pgas0(self):
        return (2.3*cons.k_B*1e4*un.K*self[0].nH.cgs().max()*un.cm**-3)
    def Prad_to_Pgas(self):
        return  (self.Prad()/self.Pgas0()).to('') 
    def n_gamma(self):
        return (self.Prad() * ion_fraction  / (mean_hnu * un.erg)).to('cm**-3')
    def __getitem__(self,k):
        if self._data[k]==None: 
            print('loading snapshot #%d'%k)
            self._data[k] = Snapshot(self,k,self.dataDirName) 
        return self._data.__getitem__(k)
    def delSnapshot(self,k):
        if self._data[k]!=None: 
            del self._data[k] 
    def __len__(self):
        return self._data.__len__()


class Static_solution:
    lines = [('NeV','a',3426.0),
         ('NeIII','b',3869.0),
         ('OIII','a',5006.84),
         ('Hb','',4861.33)]
    def __init__(self,fn_static_solution,tables_dic):
        self._static = np.load(fn_static_solution)
        self.tables_dic = tables_dic
    def __getitem__(self,k):        
        return self._static[k]    
    def static_line_emission(self,line):
        nes = self.tables_dic['elec'](self['NHIs'],self['NHs'],self['Ts'],self['nHs']) * self['nHs']
        nIon = self.tables_dic[line[0]](self['NHIs'],self['NHs'],self['Ts'],self['nHs']) * self['nHs']
        LIon = getattr(emission,'L'+line[0]+line[1])(self['Ts']/1e4,nIon,nes) * emission.extinction(self['NHs'],getattr(emission,'E_'+line[0]+line[1]))
        return nIon,LIon
    def static_total_line_emission(self,line):
        fHIs_static = (self['NHIs'][1:]-self['NHIs'][:-1])/(self['NHs'][1:]-self['NHs'][:-1])
        fHIs_static = np.concatenate([[self['NHIs'][0]/self['NHs'][0]], fHIs_static])
        delta_rs_static = self['rs'] #* (q.n_gamma().value/10.**lng)**-1 #eq. 16 in Stern+14
        inds = fHIs_static<0.5
        nIon,LIon = self.static_line_emission(line)
        return (LIon*np.gradient(delta_rs_static)).cumsum()[inds][-1]
    
    
def r_dependence_calc(runs,iTs=[100,200,300,400,500],dlogn=0.1,dlogT=0.025):
    res_nH = [None]*len(runs)
    res_T = [None]*len(runs)
    bins_nH=10.**np.arange(-10,15,dlogn)
    bins_T =10.**np.arange(0,8,dlogT)
    for irun,run in enumerate(runs):
        vals_nH = np.array([])
        vals_Ts = np.array([])
        weights = np.array([])
        for iT in iTs:
            vals_nH = np.concatenate([vals_nH,run[iT].nH.cgs().flatten()])
            vals_Ts = np.concatenate([vals_Ts,run[iT].T.cgs().flatten()])
            #snapshot_weights = run[iT].LHa.cgs().flatten()
            snapshot_weights = (run[iT].nHI.cgs()*run[iT].nH.cgs() * run[iT].ne2nH.cgs() * emission.extinction(run[iT].NH.cgs(),emission.E_Ha) ).flatten()
            weights = np.concatenate([weights, snapshot_weights])
        res_nH[irun] = histogram(vals_nH,bins=bins_nH,weights=weights/len(iTs))
        res_T[irun]  = histogram(vals_Ts,bins=bins_T,weights=weights/len(iTs))
    return res_nH, res_T
