# eqs. 14.8,14.9,18.12, 18.13 in Draine's book
import sys
import numpy as np
from numpy import log as ln, log10 as log
from astropy import units as un, constants as cons

from scipy import interpolate
import work_paths as wp;runsDir,rpc_hydro_dir=wp.work_paths()


sys.path.append(rpc_hydro_dir+'pysrc_new/')
from parameters import *

wls = np.array([912., 6564.6,4862.7,4341.8,4103.0,3971.3,5008.2,4960.3, 6584, 6731, 6716,6300,3729.8,3727.1,3968.6,3869.9,3426.8,3346.8])
E_Rydberg, E_Ha,E_Hb,E_Hg,E_Hd, E_He,E_OIIIa,E_OIIIb,E_NII, E_SII6731, E_SII6716, E_OI, E_OIIa, E_OIIb, E_NeIIIa, E_NeIIIb,E_NeVa,E_NeVb = cons.h.to('erg s').value * 3e18 / wls 
 
alpha_Ha  = lambda T4: 1.17e-13*T4**(-0.942-0.031*ln(T4)) #eq. 14.8 in Draine
alpha_Hb  = lambda T4: 3.03e-14*T4**(-0.874-0.058*ln(T4)) #eq. 14.9 in Draine
Lrecomb   = lambda alpha, nHII, ne, E: alpha*nHII*ne*E
L_Ha      = lambda T4, nHII, ne: Lrecomb(alpha_Ha(T4),nHII,ne,E_Ha)
L_Hb      = lambda T4, nHII, ne: Lrecomb(alpha_Hb(T4),nHII,ne,E_Hb)
L_Hg      = lambda T4, nHII, ne: 0.469*L_Hb(T4,nHII,ne) # table 14.2 in Draine, for T=10000. Other T differ by <~2%
L_Hd      = lambda T4, nHII, ne: 0.259*L_Hb(T4,nHII,ne) # table 14.2 in Draine, for T=10000. Other T differ by <~2%
L_He      = lambda T4, nHII, ne: 0.159*L_Hb(T4,nHII,ne) # table 14.2 in Draine, for T=10000. Other T differ by <~2%
OIII2Hb   = lambda T4, nOIII2nHII: 214  * nOIII2nHII*T4**(0.494+0.089*ln(T4))*np.e**(-2.917/T4)/(0.8*5.37e-4) #eq. 18.12 in Draine
NII2Ha    = lambda T4, nNII2nHII : 12.4 * nNII2nHII *T4**(0.495+0.040*ln(T4))*np.e**(-2.204/T4)/(7.41e-5)     #eq. 18.13 in Draine 

LOIIIs   = lambda nOIII,nHII,T4,ne: L_Hb(alpha_Hb(T4),nHII,ne) * OIII2Hb(T4, nOIII/nHII )
LNIIs    = lambda nNII, nHII,T4,ne: L_Ha(alpha_Ha(T4),nHII,ne) * NII2Ha( T4, nNII/nHII )
# up to ~30% addition from collisions into levels 3-4 
#general formula is n_e n_l E_ul * 8.629e-8 *T_4^-0.5 Omega_lu/g_l e^(-E_ul/kT) 
LSII_6716 = lambda T4, nSII,   ne : 8.629e-8/4      * 3.83    * nSII   * ne * T4**(-0.570-0.022*ln(T4))    *np.e**(-2.1416/T4) * E_SII6716 
LSII_6731 = lambda T4, nSII,   ne : 8.629e-8/4      * 2.56    * nSII   * ne * T4**(-0.571-0.023*ln(T4))    *np.e**(-2.1370/T4) * E_SII6731 
LOI       = lambda T4, nOI,    ne : 0.75 * 8.629e-8 * 0.0312  * nOI    * ne * T4**(+0.445-0.001*ln(T4))    *np.e**(-1.5868/T4) * E_OI
LOIIa     = lambda T4, nOII,   ne : 8.629e-8        * 0.803/4 * nOII   * ne * T4**(-0.5+0.023-0.008*ln(T4))*np.e**(-E_OIIa  /(1e4*T4*cons.k_B.to('erg/K').value)) * E_OIIa
LOIIb     = lambda T4, nOII,   ne : 8.629e-8        * 0.550/4 * nOII   * ne * T4**(-0.5+0.054-0.004*ln(T4))*np.e**(-E_OIIb  /(1e4*T4*cons.k_B.to('erg/K').value)) * E_OIIb
LOIIIa    = lambda T4, nOIII,  ne : 0.75 * 8.629e-8 * 0.243   * nOIII  * ne * T4**(-0.5+0.120+0.031*ln(T4))*np.e**(-E_OIIIa /(1e4*T4*cons.k_B.to('erg/K').value)) * E_OIIIa
LOIIIb    = lambda T4, nOIII,  ne : 0.25 * 8.629e-8 * 0.243   * nOIII  * ne * T4**(-0.5+0.120+0.031*ln(T4))*np.e**(-E_OIIIb /(1e4*T4*cons.k_B.to('erg/K').value)) * E_OIIIb
LNeIIIa   = lambda T4, nNeIII, ne : 0.25 * 8.629e-8 * 0.754/5 * nNeIII * ne * T4**(-0.5-0.011+0.004*ln(T4))*np.e**(-E_NeIIIa/(1e4*T4*cons.k_B.to('erg/K').value)) * E_NeIIIa
LNeIIIb   = lambda T4, nNeIII, ne : 0.75 * 8.629e-8 * 0.452/3 * nNeIII * ne * T4**(-0.5-0.010+0.004*ln(T4))*np.e**(-E_NeIIIb/(1e4*T4*cons.k_B.to('erg/K').value)) * E_NeIIIb
LNeVa     = lambda T4, nNeV,   ne : 0.75 * 8.629e-8 * 0.232   * nNeV   * ne * T4**(-0.5+0.016+0.019*ln(T4))*np.e**(-E_NeVa  /(1e4*T4*cons.k_B.to('erg/K').value)) * E_NeVa
LNeVb     = lambda T4, nNeV,   ne : 0.25 * 8.629e-8 * 0.232   * nNeV   * ne * T4**(-0.5+0.016+0.019*ln(T4))*np.e**(-E_NeVb  /(1e4*T4*cons.k_B.to('erg/K').value)) * E_NeVb
#OVI, NV, CIV, HeIII, CIII, NIV
extinction = lambda NH, E: np.e**-(NH * grain_cross_sections_func(log(E/E_Rydberg)))
