#Metafile to define working paths

# Adding to files
# import work_paths as wp;runsDir,rpc_hydro_dir=wp.work_paths()

import os
homedir = os.environ['HOME']
if homedir in ('/home/jonathan','/Users/jonathanstern'): homedir+='/Dropbox/other_repositories'


def work_paths():
    #runsDir = homedir+'/belt/'
    #rpc_hydro_dir = homedir+'/other_repositories/rpc_hydro/'
    #runsDir = '/home/jose/Dropbox/work_shared_folders/belt/'
    #rpc_hydro_dir = '/home/jose/data/analysis/rpc_hydro/'
    #####on Jose's computer
    runsDir = homedir+'/simulations/'
    rpc_hydro_dir = homedir+'/rpc_hydro/'
    return runsDir,rpc_hydro_dir

