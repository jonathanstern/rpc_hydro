import numpy as np, pickle
from numpy import array, log10 as log
from pylab import *

class Enum:
    def __init__(self, vals):
        assert(len(np.unique(vals))==len(vals))
        self.vals = vals
        for v in vals:
            setattr(self, v, v)
    def __iter__(self):
        return self.vals.__iter__()
    def isValid(self,val):
        return val in self
    def index(self,v):
        return self.vals.index(v)


Verdicts =  Enum(['Good', 'UpperLimit', 'LowerLimit', 'NoLimit', 'LargeError', 'NotObserved'])
Suspicions = Enum(['Good', 'FailedEye','OiiiUnder','OiiiCheck','LargenHaFWHMandSteepHbBase'])
Slopes = Enum(['Positive','Negative','Vertical'])
PointVsLine = Enum(['Above', 'Below', 'Ambiguous'])
class DividingLine:
    def __init__(self, func, slope, additionalCond=None,plotxrng=None,plotyrng=None):
        assert(Slopes.isValid(slope))
        self.func = func
        if additionalCond != None: self.additionalCond = additionalCond
        else: self.additionalCond = lambda x: True
        self.slope = slope
        self.plotxrng = array(plotxrng)
        self.plotyrng = array(plotyrng)
    def __call__(self,*args,**kwargs):
        return self.func(*args,**kwargs)
    def pointPosition(self, x,y,ignoreAmbiguous=False,xVerd= Verdicts.Good,yVerd=Verdicts.Good):
        if ignoreAmbiguous:
            xVerd, yVerd = Verdicts.Good, Verdicts.Good
        if self.slope==Slopes.Vertical:
            if x < self.func():
                if xVerd == Verdicts.UpperLimit or xVerd == Verdicts.Good:
                    return PointVsLine.Below
                else:
                    return PointVsLine.Ambiguous
            else:
                if xVerd == Verdicts.LowerLimit or xVerd == Verdicts.Good:
                    return PointVsLine.Above
                else:
                    return PointVsLine.Ambiguous		

        isBelow = self.additionalCond(x) and (y < self.func(x))
        if isBelow:
            if yVerd == Verdicts.UpperLimit or yVerd == Verdicts.Good:
                if self.slope==Slopes.Negative:
                    if xVerd == Verdicts.UpperLimit or xVerd == Verdicts.Good:
                        return PointVsLine.Below
                    else:
                        return PointVsLine.Ambiguous
                else:
                    if xVerd == Verdicts.LowerLimit or xVerd == Verdicts.Good:
                        return PointVsLine.Below
                    else:
                        return PointVsLine.Ambiguous
            else:
                return PointVsLine.Ambiguous
        else:
            if yVerd == Verdicts.LowerLimit or yVerd == Verdicts.Good:
                if self.slope==Slopes.Negative:
                    if xVerd == Verdicts.LowerLimit or xVerd == Verdicts.Good:
                        return PointVsLine.Above
                    else:
                        return PointVsLine.Ambiguous
                else:
                    if xVerd == Verdicts.UpperLimit or xVerd == Verdicts.Good:
                        return PointVsLine.Above
                    else:
                        return PointVsLine.Ambiguous
                return PointVsLine.Ambiguous

class SpectrumVals:
    Ke01 = [DividingLine(lambda x: 0.61 / ( x - 0.47 ) + 1.19, Slopes.Negative, lambda x: x<0.47),
            DividingLine(lambda x: 0.72 / ( x - 0.32 ) + 1.30, Slopes.Negative, lambda x: x<0.32),
            DividingLine(lambda x: 0.73 / ( x + 0.59 ) + 1.33, Slopes.Negative, lambda x: x<-0.59)]
    Ka03 = [DividingLine(lambda x: 0.61 / ( x - 0.05 ) + 1.3, Slopes.Negative,lambda x: x<0.05),None,None]
    Ke06 = [None,DividingLine(lambda x: 1.89*x + 0.76,Slopes.Positive), DividingLine(lambda x: 1.18*x+ 1.3,Slopes.Positive)]    
    #SF:     <fN2b[0]                                                 equal to their cln='SF'   assumin S\N>3
    #Comp:   >fN2b[0], <fns[0]                                        equal to their cln='comp' assumin S\N>3
    #S2:     >fns[0], >fns[1], >fns[2], >S2L2[1], >S2L2[2]            part of their cln='AGN non Liner' assumin S\N>3
    #LINERS: >fns[0], >fns[1], >fns[2], <S2L2[1], <S2L2[2]            part of their cln='AGN non Liner' assumin S\N>3
    #AGN Ambig1: >fns[0], >fns[1], >fns[2], >S2L2[1] xor <S2L2[2]     part of their cln='AGN non Liner' assumin S\N>3
    #AGN Ambig2: >fns[0], <fns[1] or <fns[2]                          part of their cln='AGN non Liner' assumin S\N>3
    #Not clear what their low S\N Liner and low S\N SF but probably relevant to S\N, together with unclassifiable
    Ho97 = [None,None,DividingLine(lambda: log(0.17),Slopes.Vertical,plotxrng=(log(0.17),log(0.17)),plotyrng=(-0.2,0.4))]
    Wa09 = [None,DividingLine(lambda x: -1.67*x+ 0.18,Slopes.Negative,plotxrng=(-0.16,0.17)), DividingLine(lambda x: -2.09*x - 1.14,Slopes.Negative,plotxrng=(-0.78,-0.45))]
    SL12 = [DividingLine(lambda x: 3.33 * x + 2.8,Slopes.Positive), DividingLine(lambda x: 2. * x + 2.2,Slopes.Positive),None]


def figBPT(rows=1,cols=3):
    fig = figure(figsize=(1+cols*2,1.4*(rows+1)))
    subplots_adjust(wspace=0,left=(0.1,0.12,0.1)[cols-1],bottom=0.13,right=0.99,top=0.99,hspace=1e-4)
    axlist = [subplot(rows,cols,i+1) for i in range(cols*rows)]
    rcParams['font.size']=7
    return fig,axlist
#xr_axes = Kewley06.extents
xr_axes=[(-1.9,0.8,-1.1,1.45),(-1.15,0.4,-1.1,1.45),(-2.15,-0.1,-1.1,1.45)]
xr_axes_zoom = [(-0.9,0.3,0.,1.1), (-1,0.2,0.,1.1), (-1.6,-0.4,0.,1.1)]

def plotBPTbg(axlist,plotLines=True,zoom=False,plotOnlyLiner=False,plotWa09=False,axlistinds=[0,1,2],plotHo97=False,Ke06ls='--'):
    for i in axlistinds:
        axslimit = (xr_axes,xr_axes_zoom)[zoom][i]
        axes(axlist[axlistinds.index(i)])
        gca().xaxis.set_major_locator( matplotlib.ticker.MultipleLocator((0.5,0.3)[zoom]))
        gca().xaxis.set_minor_locator( matplotlib.ticker.MultipleLocator(0.1))
        gca().yaxis.set_major_locator( matplotlib.ticker.MultipleLocator((0.5,0.3)[zoom]))
        gca().yaxis.set_minor_locator( matplotlib.ticker.MultipleLocator(0.1))			
        if plotLines:
            xs = arange(axslimit[0],axslimit[1],0.01)
            ys = SpectrumVals.Ke01[i](xs)
            mx = (ys<axslimit[2]).tolist().index(True) + 1
            if not plotOnlyLiner: 
                plot(xs[:mx], ys[:mx],'k',lw=0.5)
            if i==0:
                ys2=SpectrumVals.Ka03[i](xs)
                mx2= (ys2<axslimit[2]).tolist().index(True) + 1
                plot(xs[:mx2],ys2[:mx2],'k--',lw=2)
            if i!=0:		
                xs2 = arange([0,-0.3,-1.1][i],[0,0.3,-0.2][i],.01)
                ys2=SpectrumVals.Ke06[i](xs2)
                plot(xs2,ys2,'k'+Ke06ls,lw=1)
                if plotWa09:
                    xs3 = SpectrumVals.Wa09[i].plotxrng
                    ys3 = SpectrumVals.Wa09[i](xs3)
                    plot(xs3,ys3,'k:',lw=2)
            if i==2:
                if plotHo97:
                    xs3 = SpectrumVals.Ho97[i].plotxrng
                    ys3 = SpectrumVals.Ho97[i].plotyrng
                    plot(xs3,ys3,c='k',ls='-.',lw=1)

            if False and i in (0,1):
                ys3 = SpectrumVals.SL12[i](xs)
                plot(xs,ys3,'k:',lw=2)
        axis(axslimit)
        xlabel(r'$\log\ {\rm [%s] / H}\alpha$'%(('N II','S II', 'O I')[i]))
        if axlistinds.index(i)==0: ylabel(r'$\log\ {\rm [O III] / H}\beta$')
        else: gca().yaxis.set_major_formatter(matplotlib.ticker.NullFormatter())

figDir = 'figures/'; figureExt2 = '.png'         
class BGBPT:
    def __init__(self,fn_prefix,extents,axlist=None,grids=None,Us=None,aoxs=None,Zs=None):
        self.imgs = [imread(figDir+fn_prefix+'_%d.png'%(iBPT+1)) for iBPT in range(3) if extents[iBPT]!=None]
        self.extents = extents
        self.grids = grids # should be {aox} x {U} x {BPT ratios}
        self.Us = Us
        self.Zs = Zs	
        self.aoxs = aoxs
        if axlist!=None: self.plotOnAx(axlist)
    def plotOnAx(self,axlist,applyExtent=False):
        [axlist[i].imshow((self.imgs[i][::-1]+0.3)/1.3,aspect='auto',origin='lower',extent=self.extents[i]) for i in range(len(axlist)) if self.extents[i]!=None]
        if applyExtent:
            [axlist[i].axis(self.extents[i]) for i in range(len(axlist)) if self.extents[i]!=None]
Kewley06   = BGBPT('Kewley06',   [(-1.9,0.8,-1.1,1.45),(-1.15,0.7,-1.1,1.45),(-2.15,-0.1,-1.1,1.45)])	

def figBPT_overKewley(shownew=True,groupbymorphology=False,contour=False,showpercentage=True,save=True):
    fig,axlist = figBPT()
    plotBPTbg(axlist,True,plotOnlyLiner=True,plotHo97=False)
    Kewley06.plotOnAx(axlist,applyExtent=False)
    xdic = [{'SF':-1.4,'Co':-0.3,'AG':-1.5}, {'Se':-1.05,'LI':0.,'SF':-1.1},{'Se':-1.8,'LI':-0.7,'SF':-2.1}]
    ydic = [{'SF':-0.5,'Co':-1,'AG':1.2},{'Se':1.2,'LI':-0.3,'SF':-0.5},  {'Se':1.2,'LI':-0.5,'SF':0.45}]

    for i in range(3):
        for cln in (('AGN','Seyfert')[i>0],('Composite','LINER')[i>0],'SF'):
            sca(axlist[i])
            text(xdic[i][cln[:2]],ydic[i][cln[:2]],'%s'%cln,zorder=100)
    
    #mysavefig('BPT_overKewley' + (('_plain','')[shownew],'_byMorphology')[groupbymorphology],save=save)
    return axlist