def searchAndInterpolate4D(arr_xs,valx,arr_ys,valy,arr_zs, valz, arr_ws,valw, matrix_vs):
    # matrix_vs is a 4D-array representing the table
    # valx, valy, valz, valw are the four values for which you want to calculate the result
    # arr_xs, arr_ys, arr_zs, arr_ws should be replaced with the linear coefficient of the entries in table, i.e.
    # switch the following four lines with a more efficient way of finding the four indices
    # e.g. if the lowest value of T in the Table is T_0 and there is a table entry every half-decade in T
    # then the line should read i = (log(T) - log(T_0))/0.5
    i = min( max(searchsorted(arr_xs, valx), 1), len(arr_xs)-1)
    j = min( max(searchsorted(arr_ys, valy), 1), len(arr_ys)-1)
    k = min( max(searchsorted(arr_zs, valz), 1), len(arr_zs)-1)
    l = min( max(searchsorted(arr_ws, valw), 1), len(arr_ws)-1)


    dx = arr_xs[i] - arr_xs[i-1]
    xn = 1.* (valx - arr_xs[i-1]) / dx
    dy = arr_ys[j] - arr_ys[j-1]
    yn = 1.* (valy - arr_ys[j-1]) / dy
    dz = arr_zs[k] - arr_zs[k-1]
    zn = 1.* (valz - arr_zs[k-1]) / dz
    dw = arr_ws[l] - arr_ws[l-1]
    wn = 1.* (valw - arr_ws[l-1]) / dw

    return ( ( (matrix_vs[i-1,j-1,k-1,l-1]*(1-xn)*(1-yn) + matrix_vs[i-1,j,k-1,l-1]*(1-xn)*yn + matrix_vs[i,j-1,k-1,l-1]*xn*(1-yn) + matrix_vs[i,j,k-1,l-1]*xn*yn)*(1-zn) +
               (matrix_vs[i-1,j-1,k,l-1]  *(1-xn)*(1-yn) + matrix_vs[i-1,j,k,l-1]  *(1-xn)*yn + matrix_vs[i,j-1,k,l-1]  *xn*(1-yn) + matrix_vs[i,j,k,l-1]  *xn*yn)*zn )*(1-wn) + 
             ( (matrix_vs[i-1,j-1,k-1,l]*(1-xn)*(1-yn) + matrix_vs[i-1,j,k-1,l]*(1-xn)*yn + matrix_vs[i,j-1,k-1,l]*xn*(1-yn) + matrix_vs[i,j,k-1,l]*xn*yn)*(1-zn) +
               (matrix_vs[i-1,j-1,k,l]  *(1-xn)*(1-yn) + matrix_vs[i-1,j,k,l]  *(1-xn)*yn + matrix_vs[i,j-1,k,l]  *xn*(1-yn) + matrix_vs[i,j,k,l]  *xn*yn)*zn )*wn )
