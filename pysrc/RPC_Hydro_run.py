#######obselete file, use python notebook!!!!!!!


def run():
    execfile('rpc_hydro/pysrc/RPC_Hydro.py')

    ##create cooling/heating tables
    Tgrid = Cloudy.Grid('TemperatureMap', emissionLines=Cloudy.emissionLines[:1],colDenss=[24.],savelevel=1,useIR=False,emittedRadiationPressure=False,
                        ns=2.5-arange(-5,5.1,.5),Us=arange(-5,5.1,.5),notCartesian='nU',
                        saveTmap=True,dusties=[True],aEUVs=[-1.6],Zs=[1.])
    #all warnings are dust sublimation issues, but don't load because model didn't finish running!!!
    pp.Table.Tmaps(Tgrid)

    ## calculate values for run
    DummyGrid = Cloudy.Grid('Dummy', emissionLines=Cloudy.emissionLines[:1],colDenss=[24.],savelevel=1,useIR=False,emittedRadiationPressure=False,
                            ns=[7.5],Us=[-5],
                            saveTmap=False,dusties=[True],aEUVs=[-1.6],Zs=[1.]) # for SED and grainOpacity calculating
    DummyGrid.loadAll()
    EnergyDensityToIonizingPhotonDensity(-1.6, DummyGrid[()])
    MeanOpacity(DummyGrid[()])
    

    ## opacity calculations
    OpacityGrid = Cloudy.Grid('Opacities', emissionLines=Cloudy.emissionLines[:1],colDenss=[22.5],savelevel=2,useIR=False,emittedRadiationPressure=False,
                        ns=2.5-arange(-5,5.1,.5),Us=arange(-5,5.1,.5),notCartesian='nU',forCompatibility='nUN',dusties=[True],aEUVs=[-1.6],Zs=[1.])    
    OpacityGrid_const_n = Cloudy.Grid('Opacities_const_n', emissionLines=Cloudy.emissionLines[:1],colDenss=[22.5],savelevel=2,useIR=False,emittedRadiationPressure=False,
                            ns=2.5-arange(-5,5.1),Us=arange(-5,5.1),notCartesian='nU',forCompatibility='nUN',dusties=[True],aEUVs=[-1.6],Zs=[1.],ps=[False])
    AbsorbedSpectra = Cloudy.Grid('AbsorbedSpectra', 
                                  emissionLines=Cloudy.emissionLines[:1],colDenss=[17,18,19,20,21,22,23],savelevel=0,useIR=False,
                                  emittedRadiationPressure=False,
                                  ns=2.5-arange(-5,5.1),Us=arange(-5,5.1),
                                  notCartesian='nU',forCompatibility='nUN',dusties=[True],aEUVs=[-1.6],Zs=[1.],ps=[False])
    u.send(AbsorbedSpectra(lambda m: (m.localColumnDensity()[-1], m.Nion('HI')[-1])),'rpc_hydro/data/AbsorbedSpectra_NH_NHI.pckl')

    FromAbsorbedSpectra  = Cloudy.Grid('FromAbsorbedSpectra_wTmap', 
                                       emissionLines=Cloudy.emissionLines[:1],colDenss=[16],
                                       savelevel=0,useIR=False,emittedRadiationPressure=False,
                                       ns=2.5-arange(-5,5.1),Us=[None],dusties=[True],aEUVs=[None],
                                       Zs=[1.],ps=[False],nuFnus=[1.2175],nuFnu_freq=10000.,saveTmap=True,
                                       SEDs=['AbsorbedSpectra/'+string.split(x,'/')[-1] for x in glob.glob(Cloudy.Model.basedir+'AbsorbedSpectra/*')],
                                       strfunc = lambda dirname: '_spec_%s'%(dirname[-10:]))
    FromAbsorbedSpectra2  = Cloudy.Grid('FromAbsorbedSpectra_noTmap', 
                                        emissionLines=Cloudy.emissionLines[:1],
                                        colDenss=[12.],
                                        savelevel=0,useIR=False,emittedRadiationPressure=False,
                                        ns=2.5-arange(-5,5.1),Us=[None],dusties=[True],
                                        aEUVs=[None],Zs=[1.],ps=[False],nuFnus=[1.2175],nuFnu_freq=10000.,
                                        SEDs=['AbsorbedSpectra/'+string.split(x,'/')[-1] for x in glob.glob(Cloudy.Model.basedir+'AbsorbedSpectra/*')],
                                        strfunc = lambda dirname: '_spec_%s'%(dirname[-10:]))
    FromAbsorbedSpectra2.loadAll()
    figure(); FromAbsorbedSpectra2(lambda m: plot(m.nus(False)/ ld.Rydbergnu, m.transmittedSpectrum)); loglog(); ylim(1e-4,1e3); xlim(1e-4,1e5)
    
    AbsorbedSpectra_NH_NHI = u.pickle.load(file('rpc_hydro/data/AbsorbedSpectra_NH_NHI.pckl'))
    lng = 2.5
    #pp.Table.ThreeD_opacityTable(FromAbsorbedSpectra2,AbsorbedSpectra_NH_NHI,lng=lng)
    pp.Table.DustAndGasOpacityTables(FromAbsorbedSpectra2,AbsorbedSpectra_NH_NHI,lng=lng)
    pp.Table.fHI_table(FromAbsorbedSpectra2,AbsorbedSpectra_NH_NHI,lng=lng)
    
    pp.Table.FourD_Tmaps(FromAbsorbedSpectra,AbsorbedSpectra_NH_NHI,lng=lng,showFigs=True)
    
    
    
    OpacityGrid_const_n.loadAll()
    OpacityGrid_const_n(lambda m: m.loadSpectra(onlyIonizing=False))
    OpacityGrid_const_n(lambda m: m.loadOpacities(onlyIonizing=False))
    #OpacityGrid(lambda m: m.meanSigmaDust(photonmean=False))
    #pp.Table.OpacityTable(OpacityGrid)
    pp.Table.testApproximations(OpacityGrid_const_n)

    ## load run
    netCoolingTable = pp.Table.Table(('U','T'),'rpc_hydro/data/OldCoolingTable.txtCoolingTable.txt')
    q = pp.Run('B2D_N1024_R100_000',273,dr=3*3e18/1024.,v0=ld.pc/ld.yr,L0=ld.pc,rho0=1e-20,netCoolingTable = netCoolingTable,low2highOpacity=1e-3,thresholdRho=1e-25/0.01,opacity=448.16)  #units in file 'Belt/run-folder/user_defined_parameters.h'
    
    q.plotter().multiplot2D('F',   iTs=(0,1,3,10,30,100,200,250,267),vmin=10, vmax=1e5, logScale=True,save=True)
    q.plotter().multiplot2D('tau', iTs=(0,1,3,10,30,100,200,250,267),vmin=0, vmax=10, logScale=False,save=True)
    q.plotter().multiplot2D('T',   iTs=(0,1,3,10,30,100,200,250,267),vmin=30, vmax=1e5, logScale=True,save=True)
    q.plotter().multiplot2D('P2k', iTs=(0,1,3,10,30,100,200,250,267),vmin=1e4,vmax=1e10,logScale=True,save=True)
    q.plotter().multiplot2D('nH',   iTs=(0,1,3,10,30,100,200,250,267),vmin=10, vmax=1e5, logScale=True,save=True)
    q.plotter().multiplot2D('U',   iTs=(0,1,3,10,30,100,200,250,267),vmin=1e-8, vmax=1e5, logScale=True,save=True)
    q.plotter().multiplot2D('Lambda',   iTs=(0,1,3,10,30,100,200,250,267),vmin=1e-17, vmax=1e-10, logScale=True,save=True)

    q.plotter().multiplotMidPlane('ng','nH',(1,3,10,30,100,200,250,267),logScale=True,var1range=(1e-9,1e6), var2range=(1e-9,1e6))

    q2.plotter().multiplot2D('T',iTs=(0,1,3,10,20,30,50,70,105),vmin=10,vmax=1e5,logScale=True,save=True)
    q2.plotter().multiplotMidPlane('ng','nH',(1,3,10,20,30,50,70,85,105),logScale=True,var1range=(1e-9,1e6), var2range=(1e-9,1e6))
    
    
    opacityTuple = (lambda dtaudr,nH: dtaudr.cgs()/nH.cgs()/3e18,r'opacity [{\rm cm}^2]',('tau','r'),'nH')
    q.plotter().multiplotMidPlane('tau','nH', (1,3,10,30,100,200,250,267),logScale=True,var1range=(1e-6,10),var2range=(1,1e6))
    q.plotter().multiplotMidPlane(opacityTuple,'U', (1,3,10,30,100,200,250,267),logScale=True,var1range=(1e-22,1e-18), var2range=(1e-6,1000))
    q.plotter().multiplotMidPlane(opacityTuple,'nH',(1,3,10,30,100,200,250,267),logScale=True,var1range=(1e-22,1e-18), var2range=(1,1e6))
    q.plotter().multiplotMidPlane('T','U', (1,3,10,30,100,200,250,267),logScale=True,var1range=(10,1e6), var2range=(1e-6,1000))
    q.plotter().multiplotMidPlane('T','NH', (1,3,10,30,100,200,250,267),logScale=True,var1range=(10,1e6), var2range=(1e20,1e23))
    q6.plotter().multiplotMidPlane('T','U', (1,3,10,30,80),logScale=True,var1range=(10,1e6), var2range=(1e-6,1000))

    ## calculate EMD
    figure(); [tplot(sorted(q[it].EMD().items()),'-',c='kbcry'[iit%5]) for iit,it in enumerate(range(1,200,20))]
    
    
    qshort = pp.Run('test0_bigcloud_1yr',82,dr=6*3e18/128.,v0=ld.pc/ld.yr,L0=ld.pc,rho0=1e-20,netCoolingTable = netCoolingTable,low2highOpacity=1e-3,thresholdRho=1e-25/0.01,opacity=448.16)  #units in file 'Belt/run-folder/user_defined_parameters.h'    
    q = pp.Run('test0_bigcloud_1E5yr',955,dr=6*3e18/128.,v0=ld.pc/ld.yr,L0=ld.pc,rho0=1e-20,netCoolingTable = netCoolingTable,low2highOpacity=1e-3,thresholdRho=1e-25/0.01,opacity=448.16)  #units in file 'Belt/run-folder/user_defined_parameters.h'    
    q.plotter().multiplot2D('nH',   iTs=(0,3,10,30,50,100,150,200,250),vmin=10, vmax=1e5, logScale=True,save=True)
    q.plotter().multiplotMidPlane('ng','nH',(0,3,10,30,50,100,150,200,250),logScale=True,var1range=(1e-9,1e6), var2range=(1e-9,1e6))
    
    
    ## new type runs    
    pp.Table.testTables2(FromAbsorbedSpectra,AbsorbedSpectra_NH_NHI,2.5)
    q = pp.Run('B2D_N2048_R100_100',134,dr=3*3e18/2048.,v0=ld.pc/ld.yr,L0=ld.pc,rho0=1e-20,low2highOpacity=1e-3,thresholdRho=1e-25/0.01,L=2.5e+12*ld.Lsun,optFrac=0.514,opacity=448.16,newType=True)
    q.plotter().multiplot2D_multiVars({'nH':(1,1e4),'T':(30,3e4),'U0':(1e-5,1e5),'tau_d':(0.01,100),'tau_HI':(0.01,100)}, iT=5, logScale=True,save=True)
    q.plotter().midPlane_multiVars({'nH':(0,1,1e6),'T':(1,30,3e6),'U0':(3,1e-3,1e5),'NHI':(4,1e15,1e23),'NH':(5,1e15,1e23), 'P2k':(2,1e4,1e10), 'Lambda':(7,1e-18,1e-12),
                                    'gasIonOpacity':(8,1e-24,1e-20),'dustIonOpacity':(9,1e-24,1e-20),'dustOptOpacity':(10,1e-24,1e-20),'fHI':(6,1e-5,3)}, 
                                   iTs=(0,1,45,90,133), logScale=True,save=True,xls=(100.,103.))
    
    pp.Table.testTables(q)
    q.plotter().midPlane_multiVars({'T':(0,30,1e6)},iTs=range(6), logScale=True,save=True,xls=(100.4,101.),sqrtnPanels=1)
    [axhline(10**logT,ls=':',c='.5',lw=0.5) for logT in q.netCoolingTable.propertyValues[0]]

    

def Applications2015():
    execfile('rpc_hydro/pysrc/RPC_Hydro.py')
    netCoolingTable = pp.Table.Table(('U','T'),'rpc_hydro/data/OldCoolingTable.txt')
    q = pp.Run('B2D_N1024_R100_000',273,dr=3*3e18/1024.,v0=ld.pc/ld.yr,L0=ld.pc,rho0=1e-20,netCoolingTable = netCoolingTable,low2highOpacity=1e-3,thresholdRho=1e-25/0.01,opacity=448.16)  #units in file 'Belt/run-folder/user_defined_parameters.h'
    q.plotter().forApplications2015(125)
def jobtour2016():
    execfile('rpc_hydro/pysrc/RPC_Hydro.py')
    netCoolingTable = pp.Table.Table(('U','T'),'rpc_hydro/data/OldCoolingTable.txt')
    q = pp.Run('B2D_N1024_R100_000',273,dr=3*3e18/1024.,v0=ld.pc/ld.yr,L0=ld.pc,rho0=1e-20,netCoolingTable = netCoolingTable,low2highOpacity=1e-3,thresholdRho=1e-25/0.01,opacity=448.16)  #units in file 'Belt/run-folder/user_defined_parameters.h'    
    q.plotter().forApplications2015(125,showLegend=False,showInset=True)
    q.plotter().forApplications2015(125,showLegend=False,showInset=False)
def newRun():
    execfile('rpc_hydro/pysrc/RPC_Hydro.py')
    #pp.Table.testTables2(FromAbsorbedSpectra,AbsorbedSpectra_NH_NHI,2.5)
    #pp.Table.testTables(q)

    q100 = pp.Run('B2D_N2048_R100_200',143,dr=3*3e18/2048.,v0=ld.pc/ld.yr,L0=ld.pc,rho0=1e-20,L=2.5e+12*ld.Lsun,optFrac=0.514,opacity=448.16)
    
    q100.plotter().midPlane_multiVars({'nH':(0,1,1e6),'T':(1,30,3e6),'U0':(3,1e-3,1e5),'NHI':(4,1e15,1e23),'NH':(5,1e15,1e23), 'P2k':(2,1e4,1e10), 'Lambda':(7,1e-18,1e-12),
                                       'gasIonOpacity':(8,1e-24,1e-20),'dustIonOpacity':(9,1e-24,1e-20),'dustOptOpacity':(10,1e-24,1e-20),'fHI':(6,1e-5,3)}, 
                                      iTs=(1,10,20,30,40,50,60,70,80,90,100,110), logScale=True,xls=(100.,103.),sqrtnPanels=4)
    
    
    q1000 = pp.Run('B2D_N2048_R1000_200',4001,dr=3*3e18/2048.,v0=ld.pc/ld.yr,L0=ld.pc,rho0=1e-20,L=2.5e+12*ld.Lsun,optFrac=0.514,opacity=448.16)
    q1000.plotter().dLdlogn([1,1000,2000,3000,4000])
    q1000.plotter().midPlane_multiVars({'nH':(0,1,1e8),'T':(1,30,3e6),'U0':(3,1e-3,1e5),'NHI':(4,1e15,1e23),'NH':(5,1e15,1e23), 'P2k':(2,1e4,1e10), 
                                        'gasIonOpacity':(8,1e-24,1e-20),'dustIonOpacity':(9,1e-24,1e-20),'dustOptOpacity':(10,1e-24,1e-20),'fHI':(6,1e-5,3),'Lattenuated':(7,1e-5,0.1)}, 
                                       iTs=(1,1000,2000,3000,4000), logScale=True,xls=(1000.,1003.),sqrtnPanels=4)
    [q1000.plotter().multiplot2D_multiVars({'T':(1e2,1e6),'nH':(1,1e8),'P2k':(1e5,3e8),'Lattenuated':(1e-5,0.1)}, iT=i, logScale=True,save=False,sqrtnPanels=2) for i in 1,1000,2000,3000,4000]
    
    q10000 = pp.Run('B2D_N2048_R10000_200',4001,dr=3*3e18/2048.,v0=ld.pc/ld.yr,L0=ld.pc,rho0=1e-20,L=2.5e+12*ld.Lsun,optFrac=0.514,opacity=448.16)
    q10000.plotter().dLdlogn([1,1000,2000,3000,4000])





    AbsorbedSpectra_NH_NHI = u.pickle.load(file('rpc_hydro/data/AbsorbedSpectra_NH_NHI.pckl'))
    lng = 2.5

    FromAbsorbedSpectra3  = Cloudy.Grid('FromAbsorbedSpectra_BPTlines', 
                                        emissionLines=Cloudy.emissionLines[:7],
                                        colDenss=[12.],
                                        savelevel=0,useIR=False,emittedRadiationPressure=False,
                                        ns=2.5-arange(-5,5.1),Us=[None],dusties=[True],
                                        aEUVs=[None],Zs=[1.],ps=[False],
                                        nuFnus=[1.2175,-1.2175,-3.2175],nuFnu_freq=10000.,
                                        SEDs=['AbsorbedSpectra/'+string.split(x,'/')[-1] for x in glob.glob(Cloudy.Model.basedir+'AbsorbedSpectra/*')],
                                        strfunc = lambda dirname: '_spec_%s'%(dirname[-10:]))
    pp.Table.lineEmissionTable(Cloudy.emissionLines[:7],
                               FromAbsorbedSpectra3,AbsorbedSpectra_NH_NHI,spectrum_lng=lng,
                               lngDic = {1.2175:lng, -1.2175: lng-2, -3.2175:lng-4})
    pp.Table.plotCoolingTime(q.netCoolingTable,2.5)
    
    
    
    
    
    
