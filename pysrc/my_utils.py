from IPython.core.display import clear_output
import sys
import os
import string
import glob
from numpy import *
from pylab import *
import scipy
import scipy.stats
import pdb
import subprocess
import pickle
import time
import itertools
#import brewer2mpl
#import pyfits
#import celgal
import ListsAndDics as ld
import math
#from scipy.stats import chisqprob
from scipy.special import wofz
#from distances import calcDists
import numpy.random
#from mpfit import *

ln = log
log = log10
concat = concatenate
MAX_ANDERSON_SCORE = 996
Fwhm2Sigma = 2.35482
roman = [None,'I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI','XVII','XVIII','XIV','XX','XXI','XXII','XXIII','XXIV','XXV','XXVI','XXVII','XXVIII','XXIX','XXX']
fig_width = 6.973848

ndigits = lambda f,maxdigits: ([(f*10**x)%1==0 for x in range(maxdigits)] + [True]).index(True)
intandfloatFunc = lambda x,pos=0: ('%.'+str(ndigits(x,2))+'f')%x
intandfloatFormatter = matplotlib.ticker.FuncFormatter(intandfloatFunc)
singleGroup = noFilter = lambda k: True
dummyFunc = lambda x: x
def sizeformat(x,pos=None,units=1.):
        if x*units<=3e20: return r'$%s$ pc'%(nSignificantDigits(x*units/3e18, sigdig=2,retstr=True))
        if 3e20<x*units<=3e23: return r'$%s$ kpc'%(nSignificantDigits(x*units/3e21, sigdig=2,retstr=True))
        if 3e23<x*units<=3e26: return r'$%s$ Mpc'%(nSignificantDigits(x*units/3e24, sigdig=2,retstr=True))
        if 3e26<x*units: return r'$%s$ Gpc'%(nSignificantDigits(x*units/3e27, sigdig=2,retstr=True))
def arilogformat(x,pos=None,dollarsign=True,logbase=1,with1000=True,showHalfs=False):
        if not (showHalfs and str(x).strip('0.')[0]=='3'):
                if x==0 or iround(log(abs(x)))%logbase!=0: return ''
        if True in [abs(abs(x)-a)/a<1e-6 for a in [1,1.,3.,10,30.,100]+([],[1000])[with1000]]:
                s = '$%d$'%x
        elif True in [abs(abs(x)-a)/a<1e-6 for a in (0.03,0.01,0.3,0.1)]:
                s = '$%s$'%intandfloatFunc(x)
        else:
                s = matplotlib.ticker.LogFormatterMathtext()(x)
        if dollarsign: 	return s
        else: return s[1:-1]

def arilogformat2(x,pos=None,dollarsign=True,nDigs=1,maxNormalFont=1000,minNormalFont=0.01):
        if 1<=abs(x)<=maxNormalFont:
                s = '$%s$'%nSignificantDigits(x,max(nDigs+1,1),retstr=True)
        elif minNormalFont<=abs(x)<=1:
                s = '$%s$'%intandfloatFunc(iround(x,1e-4))
        else:
                base = 10**int(log(x))
                sbase = matplotlib.ticker.LogFormatterMathtext()(base)
                if nDigs>=0: scoeff = ('%.'+str(nDigs)+'f')%(x*1./base) +r' \cdot '
                else: scoeff = ''
                s = sbase[0] + scoeff +sbase[1:]
        if dollarsign: 	return s
        else: return s[1:-1]

def myScientificFormat(x,pos=None,base=None,halfs=False,afterDotDigits=1):
        if x==0: return r'$0$'
        if base==None: 
                base = 10**int(log(x))
                if x/base>=9.5: base*=10
        if not halfs and round(x/base)!=x/base: return ''
        if afterDotDigits>0:
                digitstr = (r'%.'+str(afterDotDigits)+'f')%(x/base)
        else:
                digitstr = r'%d'%iround(x/base)
        expstr = r'10^{%d}'%(log(float(base)))		
        if x==base or digitstr=='1': return r'$%s$'%expstr
        return r'$%s\cdot%s$'%(digitstr,expstr)


def myFuncFormatter(formatter,**kwargs):
        return matplotlib.ticker.FuncFormatter(lambda x,pos: formatter(x,pos,**kwargs))

arilogformatter = matplotlib.ticker.FuncFormatter(arilogformat)
arilogformatter2 = matplotlib.ticker.FuncFormatter(arilogformat2)
sizeformatter = matplotlib.ticker.FuncFormatter(sizeformat)
mylogformatter = matplotlib.ticker.FuncFormatter(lambda x,pos: '%d'%log(x))
mypowerformatter = matplotlib.ticker.FuncFormatter(lambda x,pos: arilogformat(10**x,pos))
def mylogformat2(x,pos,toshow='2357'):
        s = nSignificantDigits(x,1,True)
        if x<1: firstDigit=s[-1]
        if x>1: firstDigit=s[0]
        return ('',s)[firstDigit in toshow]
mylogformatter2 = matplotlib.ticker.FuncFormatter(mylogformat2)
boolstr = 'False', 'True'
def wavelengthformat(x,pos):
        # x in angstrom
        if x<1e4:
                return r'$%s$\AA'%nSignificantDigits(x,1,True) 
        else:
                return r'$%s\mu$m'%nSignificantDigits(x/1e4,1,True) 
def lens(arr):
        return array([len(x) for x in arr])
def dlens(dic):
        return dict([(k,len(dic[k])) for k in dic])
def dfunc(dic,func,func_on_key=False):
        if not func_on_key:
                return dict([(k,func(dic[k])) for k in dic])
        else:
                return dict([(k,func(k)) for k in dic])


def mifkad(arr):
        dic={}
        for x in arr:
                if dic.has_key(x):
                        dic[x] += 1
                else:
                        dic[x] = 1
        return dic

def mifkaddists(dists,rngs):	
        mifs = []
        for i in rl(rngs):
                mif = concat([ones(1),zeros(len(dists))])
                mnb,mxb = rngs[i]
                for j in rl(dists):
                        d = dists[j]
                        strt = searchsorted(d[0], mnb)
                        end = searchsorted(d[0],mxb)
                        binchance = (d[1][strt:end]).sum()
                        mif = concat([zeros(1),mif[:-1]*binchance]) + mif*(1.-binchance)
                mifs.append(mif)
        return mifs



def strsci(flt,ndigits=2):
        h = 10**ndigits
        newN = round(flt*h)
        return (newN/h).__str__()

def str(x):
        return x.__str__()


def sumlst(lst):
        return reduce(lambda x,y: x+y,lst)
def sumdiclst(diclst):
        totalDic = {}
        for dic in diclst:
                for k in dic:
                        if k not in totalDic:
                                totalDic[k] = 0
                        totalDic[k] += dic[k]
        return totalDic

def plotmif(arr,syn='',toShow=True):
# non-negative integers only
        m = max(arr)
        res = [0]*(m+1)
        for i in arr:
                res[i] = res[i]+1
        plot(res,syn)
        if toShow: show()

def plotdens(arr,dx,nsamp=None,syn='',lab='',retXY=False):
        mn = min(arr)-dx/2.
        mx = max(arr)+dx/2.
        xs = [mn + 1.0*i*(mx-mn)/nsamp for i in range(nsamp+1)]
        halfdxInd = int( dx/2.*nsamp / (mx-mn) )
        ys = [0]*(nsamp+1)
        for a in arr:
                centInd = int(nsamp*(a-mn)*1. / (mx-mn) )
                for d in range(centInd - halfdxInd, centInd+halfdxInd+2):
                        try:
                                ys[d]+=1
                        except IndexError,e:
                                print e, "\nprobably dx larger than mx-mn"



        plot(xs,ys,syn,label=lab)
        if retXY:
                return xs,ys



def chiscore(data,expected,sigmas=None,pr=True, nModelParams=1):
        if sigmas==None:
                chisq = ((data-expected)**2/expected).sum()
        else:
                chisq = ((data-expected)**2/sigmas**2).sum()
        degrees = len(data) - nModelParams
        if pr:
                prob = chisqprob(chisq,degrees)
                print "Score:\t", chisq
                print "Degrees:", degrees
                print "Prob:\t", prob
                print "Lprob:\t", log2(prob)
        return chisq, degrees

def line_chisq(data,expected,sigmaut):
        sigs = (data-expected)/sigmaut
        dx = len(data)
        sigMif = mifkad([round(x/dx) for x in sigs])
        sigdat = [None]*(8/dx+1)
        for i in range(-4,4+dx,dx):
                if sigMif.has_key(i):
                        sigDat[i/dx] = mifkad(i)
                else:
                        sigDat[i/dx] = 0

def sigs2bits(sigs):
        f = 1-normcdf(sigs,0,1)
        if f==0:
                f=10**(-300)
        return -log2(f)

normcdf = scipy.stats.distributions.norm.cdf
normpdf = scipy.stats.distributions.norm.pdf

def anderson(data,std=None,mean=None):
# run Anderson test on data
        n = len(data)
        sdata = sort(data)
        if mean==None:
                mean = average(sdata)
        if std==None:
                std = sqrt(var(sdata))
        sigs = (sdata - mean)/std
        if (sigs[-1] > 8.29) or (sigs[0] < -37.67):
                return mean,std,MAX_ANDERSON_SCORE
        Ps = [None]+[normcdf(x) for x in sigs]
        Asquare = -sumlst([(2*i-1)*(log(Ps[i]) + log(1-Ps[n+1-i])) \
                           for i in range(1,n+1)])/n - n
        Asquare2 = Asquare*(1+0.75/n + 2.25/n**2)
        return mean,std,Asquare2

def smooth(arr,box_size,disregardErrors=False,errorSign=None):
        newarr = [None]*len(arr)
        for i in range(len(arr)):
                rounder = box_size%2
                a = arr[max(0,i-box_size/2):min(len(arr),i+box_size/2+rounder)]
                if disregardErrors:
                        b = [x for x in a if x!=errorSign]
                        if len(b)>0:
                                newarr[i] = average(b)
                        else:
                                newarr[i] = errorSign
                else:
                        newarr[i] = average(a)

        return newarr
def subsample(arrVal,binlimitPos,arrPos=None,funcVal=lambda x,y: mean(x), funcPos=mean):
        #arrPos should be sorted
        if arrPos==None: arrPos = rl(arrVal)
        if type(binlimitPos[0])!=type([]): binlimitPos = [binlimitPos]  #only one list means no gaps
        binnedVal = []; binnedPos = []
        for limitlist in binlimitPos:
                inds = searchsorted(arrPos,limitlist).tolist()
                binnedVal += [arrVal[inds[i-1]:inds[i]] for i in rl(inds,1)]
                binnedPos += [arrPos[inds[i-1]:inds[i]] for i in rl(inds,1)]
        pos = map(funcPos, binnedPos)
        val = map(funcVal, binnedVal, binnedPos)
        return pos,val


def supersample(arrVal,arrPos,newarrPos,default=0.):
        newarrVal = array([default] * len(newarrPos))
        inds = searchsorted(arrPos,newarrPos)
        for iInd, ind in enumerate(inds):
                if ind not in (0,len(arrPos)):
                        newarrVal[iInd] = lineFunc(arrPos[ind-1],arrVal[ind-1],arrPos[ind],arrVal[ind])(newarrPos[iInd])
        return newarrVal




def sections(arr,func):
        inSect = False
        sections = []

        for i in range(len(arr)):
                if func(arr[i]):
                        if not inSect:
                                sections.append([i,None])
                                inSect = True
                else:
                        if inSect:
                                sections[-1][1] = i
                                inSect = False
        if inSect:
                sections[-1][1] = len(arr)
        return sections


def weighted_regression(Xs,Ys,sigs,verbose=0):
# run regression where each point sigma error can be different
        if verbose==1: print 'wr'
        a = transpose(matrix([1/sigs,Xs/sigs]))
        if verbose: print a
        b = Ys/sigs
        if verbose: print b
        x,resids,rank,s = LinearAlgebra.linear_least_squares(a,b)
        return x





def iround(f,modulo=1):
        if isinstance(f,np.ndarray):
                if modulo >= 1: return np.round(f/modulo).astype(int)*modulo
                else: return np.round(f/modulo).astype(int) / (1/modulo)  #patch for floating point bug
        if modulo >= 1: return int(round(f/modulo))*modulo
        else: return int(round(f/modulo)) / (1/modulo)  #patch for floating point bug


def fltrange(start,end,step, l=None):
        if l == None:
                l = (end-start)/step
                return [start + i*step for i in range(rupint(l))]
        else:
                return [start + i*step for i in range(l)]


def rupint(f):
        i = int(f)
        if f == i:
                return i
        else:
                return i+1
def listfind(lst,func):
        if type(func) != type(lambda x: x):
                f = lambda x: x==func
        else:
                f = func

        for i in range(len(lst)):
                if f(lst[i]): 
                        return i
        return -1

def listsearch(lst,func):
        if type(func) != type(lambda x: x):
                f = lambda x: x==func
        else:
                f = func

        res = []
        for i in range(len(lst)):
                if f(lst[i]): 
                        res.append(i)
        return res

def col(mat,coln):
        return array([x[coln] for x in mat])


def rl(arr,lim=0):
        if lim==0:
                return range(len(arr))
        if lim<0:
                return range(len(arr)+lim)
        if lim>0:
                return range(lim,len(arr))

def irl(arr):
        return range(len(arr)-1,-1,-1)


def multisplit(str, seps):
        lst = [str]
        for s in seps:
                newlst = []
                for l in lst:
                        newlst += string.split(l, s)
                lst = newlst
        return lst
def smap(func, lst):
        return [x[1] for x in sorted([(func(y), y) for y in lst])]

def moment(dat, n):
        d = array(dat)
        mu = average(d)
        va = var(d)
        if n==1: return mu
        if n==2: return va
        return sum( (d-mu)**n / va**(n/2.) ) / len(d)

def distributionMoment(xs,ys,dx,N):
        return (xs**N*dx*ys).sum()


def out(arr,fmt=None):
        for a in arr:
                if fmt==None:
                        print a
                else:
                        print fmt%a

def scient(flt,n=3):
        a = abs(flt)
        b = int(a)
        c = a%1
        return ['','-'][flt<0] + str(b) +'.'+ str(c)[2:2+n]

def densityFunc(xs,ys,dx,dy,n,retZvals=False):
        # n should be even
        zvalsDic=[None]*len(xs)
        rngs = [min(xs)-dx*2, max(xs)+dx*2, min(ys)-dy*2,max(ys)+dy*2]
        xaxis = arange(rngs[0],rngs[1],dx*1./n)
        yaxis = arange(rngs[2],rngs[3],dy*1./n)
        print len(xaxis), len(yaxis)
        newzs = zeros((len(yaxis), len(xaxis)))
        for i in rl(xs):
                x = xs[i]
                y = ys[i]
                ix = searchsorted(xaxis, x)
                iy = searchsorted(yaxis, y)
                zvalsDic[i] = (iy,ix)
                for tx in range(ix-n/2,ix+n/2):
                        for ty in range(iy-n/2,iy+n/2):
                                if 0 < tx < len(xaxis) and 0 < ty < len(yaxis):
                                        if (ty-iy)**2 + (tx-ix)**2 < (n/2.)**2:
                                                newzs[ty,tx] += 1

        if not retZvals: return xaxis,yaxis,newzs
        else: return xaxis,yaxis,newzs, zvalsDic

def densityLinesPlot(xaxis, yaxis, newzs, vs):
        contour(xaxis,yaxis,newzs,vs)
        legend()

def density3DPlot(xaxis, yaxis, newzs, ifig):
        ax = matplotlib.axes3d.Axes3D(figure(ifig))
        ax.plot_wireframe(outer(ones(size(yaxis)), xaxis), outer(yaxis, ones(size(xaxis))),newzs)

def mag(x):
        return sqrt(dot(x,x))


def group(func, arr):
        dic = {}
        for a in arr:
                if dic.has_key(func(a)):
                        dic[func(a)].append(a)
                else:
                        dic[func(a)] = [a]
        return dic

def points2line(x0,y0,x1,y1):
        a = (y1-y0)*1./(x1-x0)
        b = y0 - a*x0
        return a,b
def unzip(tupslist):
        l = min(lens(tupslist))
        return [[tups[i] for tups in tupslist] for i in range(l)]
def tplot(tups, args='.',allowzerolen=False,**kwargs):
        if allowzerolen and len(tups)==0: return
        lsts = unzip(tups)
        return plot(lsts[0], lsts[1], *args,**kwargs)

def tscatter(tups,keys=[], **kwargs):
        lsts = unzip(tups)
        assert(len(keys)==len(lsts)-2)
        d = dict([(keys[i], lsts[i+2]) for i in rl(keys)])
        d.update(kwargs)
        return scatter(lsts[0], lsts[1], **d)

def terrorbar(tups, args='.',fix=False,allowzerolen=False,**kwargs):
        if allowzerolen and len(tups)==0: return
        lsts = unzip(tups)
        if len(lsts)==3 and not fix:  return errorbar(lsts[0], lsts[1], lsts[2], None, *args,**kwargs)
        if len(lsts)==3 and fix:      return errorbar(lsts[0], lsts[1], array(lsts[2]).T, None, *args,**kwargs)
        else:             return errorbar(lsts[0], lsts[1], array(lsts[2]).T, array(lsts[3]).T, *args,**kwargs)

def fltList2str(lst, sz=3):
        return string.join([('%.'+str(sz)+'G')%x for x in lst])

def ftest(xs, ws, ys, f1, args1, f2, args2):
        """
        f1,f2 should be functions with first argument x and args1, args2 correspond to the rest of the arguments.
        """
        assert(len(args1) < len(args2))
        rss = lambda f, xs, ws, args: (((f(xs,*args) - ys) / ws )**2.).sum()
        d1 = len(xs) - len(args1)*1.
        d2 = len(xs) - len(args2)*1.
        rss1 = rss(f1, xs, ws, args1)
        rss2 = rss(f2, xs, ws, args2)
        return ftestshort(rss1,d1,rss2,d2)

def ftestshort(rss1,d1,rss2,d2):
        assert(d1>d2)
        if rss1<rss2:
                return 0.
        arg = ( ( rss1 - rss2 ) / (d1-d2) ) / ( rss2 / d2)
        return scipy.stats.distributions.f.cdf(arg, d1-d2, d2)


def solveQuadratic(points):
        """
        points should be 3 tuples: (xi, yi)
        """
        m  = matrix( [ [p[0]**2, p[0], 1] for p in points])
        v = matrix( [[p[1]] for p in points] )
        return [x[0] for x in (m.I*v).tolist()]

def send(o, fn='pyobjs/tmp/t',smart=False,useCpickle=False):
        if smart:
                try: 
                        if useCpickle: oldO = cPickle.load(file(fn))
                        else: oldO = pickle.load(file(fn))
                        if oldO == o:
                                return
                except IOError:
                        pass			
        f = file(fn,'w')
        if useCpickle: cPickle.dump(o,f)
        else: pickle.dump(o,f)
        f.close()
def retrieve(fn):
        f = file(fn)
        o = pickle.load(f)
        f.close()
        return o

def lineFunc(x1,y1,x2,y2,pr=False):
        a = (y2-y1) * 1. / (x2-x1)
        b = y1
        if pr: print "y=%.2f x + %.2f"%(a, b-a*x1)
        return lambda x: a*(x-x1)+b
def squareFunc(a1,a2,b1,b2,y11,y12,y21,y22): #ys are yab
        f1 = lineFunc(a1,y11,a2,y21)
        f2 = lineFunc(a1,y12,a2,y22)
        return lambda a,b: lineFunc(b1,f1(a),b2,f2(a))(b)
def parabola(ps):
        m=matrix([[p[0]**2,p[0],1] for p in ps])
        fv = array([p[1] for p in ps])
        return dot(m.I,fv).tolist()[0]
def getFileSVNRevision(fn):
        p = subprocess.Popen("svn info " + fn,shell=True,stdout=subprocess.PIPE)
        ls = p.stdout.readlines()
        line = [x for x in ls if  'Last Changed Rev:' in x][0]
        return int(string.split(line)[-1])
def subdic(dic,func,funconkey=True):
        nd = {}
        for k in dic:
                if funconkey and func(k) or not funconkey and func(dic[k]):
                        nd[k] = dic[k]
        return nd
def recursiveDir(inst, objlist = None, prefix='.'):
        class tmpClass:
                pass
        tmpInst = tmpClass()
        arrtypes = [ type(numpy.array(range(3))) , type( Numeric.array(range(3))), type(()) ]
        res = []
        if objlist==None: objlist = [inst]
        for a in dir(inst):
                at = getattr(inst, a)
                if type(at) in arrtypes:
                        res.append( (type(at), a) )
                elif at not in objlist:
                        res.append( (type(at), a) )
                        objlist.append(at)
                        if type(at) == type(tmpInst):
                                ret = recursiveDir(at, objlist, prefix + a + '.')
                                res += ret[0]
                                objlist += ret[1]
        if objlist != None:
                return res, objlist
        else:
                return res
def dblhist(sample, valfunc, boolfunc):
        figure()
        if type(valfunc)==str:
                valfunc = lambda x: getattr(x,valfunc)
        if type(boolfunc)==str:
                boolfunc = lambda x: getattr(x,boolfunc)

        hist([[valfunc(x) for x in sample if boolfunc(x)],[valfunc(x) for x in sample if not boolfunc(x)]], normed=True)
def SDSSname2radec(s,isFirst=False):
        rhh,rmm,rss,rpp = map(int, [s[1:3], s[3:5], s[5:7], s[8:10+isFirst]])
        ds = s[10+isFirst]
        ddd, dmm, dss, dp = map(int, [s[11+isFirst:13+isFirst], s[13+isFirst:15+isFirst], s[15+isFirst:17+isFirst], s[18+isFirst]])
        ra = rhh * 15 + rmm / 4. + rss / 240. + rpp / 24000. / 10**isFirst
        dec = [-1,1][ds=='+'] * ddd + dmm / 60. + dss / 3600. + dp / 36000.
        return ra,dec

def SDSSname(r, tmp_d, fix=1e-6):
        d = abs(tmp_d)
        fr = r/360.
        vals = [fr*24, (fr*24*60)%60, (fr*24*60*60)%60, (fr*24*60*60*100)%100, 
                d, (d*60)%60, (d*60*60)%60, (d*60*60*10)%10]
        svals = ['%02d'%int(x+fix) for x in vals]
        return 'J'+string.join(svals[:3],'')+'.'+svals[3]+ ['+','-'][tmp_d<0]+string.join(svals[4:7],'')+'.'+svals[7][1]
def allPossibleSDSSnames(r,d):
        nm = SDSSname(r,d)
        a,s,b = float(nm[1:10]), nm[10], float(nm[11:19])
        return ['J%.2f%c%.1f'%(a+x,s,b+y) for x in (-0.01,0,0.01) for y in (0.1,0,-0.1)]

def distPointFromLine(x0,y0,a,b,isabs=True):
        d = (y0 - (a*x0+b)) / (a**2+1)**0.5
        if isabs: return abs(d)
        else: return d
def dxFromIntersection(x0,y0,a,b):
        d = distPointFromLine(x0,y0,a,b,False)
        return a*d/(a**2+1)**0.5
def linefit2(tups, doLogs=[True,True],plotFit=True,pr=1,color='k',linewidth=1,linestyle='-', retScatter=False,label='_nolegend_',
             init=[None,None],fixed=[False,False], retLine=False,retError=False,errortype='Y_by_X',scaleerrors=True,scattertype='Y_by_X'):	

        if len(tups[0])==2: xfunc, yfunc = [[lambda x: x, lambda x: log(x)][i] for i in doLogs]
        else: xfunc, yfunc, efunc = [[lambda x: x, lambda x: log(x)][i] for i in doLogs]
        xs = array([xfunc(x[0]) for x in tups])
        ys = array([yfunc(x[1]) for x in tups])
        if len(tups[0])==3: errs = array([efunc(x[2]) for x in tups])
        else: errs = array([1. for x in tups])*std(ys)

        if   errortype=='Y_by_X': fitfunc = lambda ps,fjac,x,y,e: [0,(ps[0]*x+ps[1]-y)/e]
        elif errortype=='X_by_Y': fitfunc = lambda ps,fjac,x,y,e: [0,((y-ps[1])/ps[0] - x)/e]
        elif errortype=='ortho':  fitfunc = lambda ps,fjac,x,y,e: [0,distPointFromLine(x,y,ps[0],ps[1])/e]
        m = mpfit(fitfunc, 
                  functkw={'x':xs,'y':ys,'e':errs}, parinfo=[{'value':(1.,init[0])[init[0]!=None], 'fixed':fixed[0]},
                                                             {'value':(0.,init[1])[init[1]!=None], 'fixed':fixed[1]}],nprint=pr)
        p = m.params
        if retError:
                if scaleerrors:
                        dof = len(xs) - len(p) # deg of freedom
                        # scaled uncertainties
                        pcerror = m.perror * (m.fnorm / dof)**0.5
                else:
                        pcerror = m.perror
                if fixed==[False,False]:
                        correl = m.covar[1,0] / (m.covar[0,0]*m.covar[1,1])**0.5
                else:
                        correl = None


        if plotFit:
                yfunc = [lambda x: x, lambda x: 10**x][ doLogs[1]]
                l = plot(xlim(),yfunc(xfunc(array(xlim()))*p[0]+p[1]),ls=linestyle,c=color,linewidth=linewidth,label=label)[0]
        retvals = [p]
        if retLine: retvals.append(l)
        if retError: retvals += [pcerror, correl]
        if retScatter:
                if   scattertype=='Y_by_X': scat = (p[0]*xs+p[1]-ys).var()**0.5
                elif scattertype=='X_by_Y': scat = ((ys-p[1])/p[0] - xs).var()**0.5
                elif scattertype=='ortho':  scat = array([distPointFromLine(xs[i],ys[i],p[0],p[1]) for i in rl(xs)]).var()**0.5
                retvals.append(scat)
        return retvals


def linefit3(tups, doLogs=[True,True],plotFit=True,errortype='Y_by_X',pr=1,xrng=None,**kwargs):
        if len(tups[0])==2: xfunc, yfunc = [[lambda x: x, lambda x: log(x)][i] for i in doLogs]
        else:        xfunc, yfunc, efunc = [[lambda x: x, lambda x: log(x)][i] for i in doLogs]
        xs = array([xfunc(x[0]) for x in tups])
        ys = array([yfunc(x[1]) for x in tups])
        if len(tups[0])==3: errs = array([efunc(x[2]) for x in tups])
        else:               errs = array([1. for x in tups])*std(ys)

        if   errortype=='Y_by_X': fitfunc = lambda ps,x,y,e: (((ps[0]*x+ps[1]-y)/e)**2).sum()
        elif errortype=='X_by_Y': fitfunc = lambda ps,x,y,e: ((((y-ps[1])/ps[0] - x)/e)**2).sum()
        elif errortype=='ortho':  fitfunc = lambda ps,x,y,e: ((distPointFromLine(x,y,ps[0],ps[1])/e)**2).sum()
        res = scipy.optimize.minimize(fitfunc, x0 = (1.,0.),args=(xs,ys,errs),
                                      method='Nelder-Mead',options={'maxiter':10000,'disp':pr})
        a,b = res.x
        if plotFit:
                if xrng==None: xrng=xlim()
                yfunc = [lambda x: x, lambda x: 10**x][ doLogs[1]]
                l = plot(xrng,yfunc(xfunc(array(xrng))*a+b),**kwargs)[0]
                print yfunc(xfunc(array(xlim()))*a+b)
        return a,b
def linefit4(xs,ys,errs=None,ULs=None,pr=True):
        if errs==None: errs = ones(len(xs))
        if ULs ==None: ULs = zeros(len(xs))
        def fitfunc(ps,x,y,err,UL): 
                vals = ps[0] + ps[1]*x
                return (((vals-y)/err)**2 * ((UL==0) + (vals>y))).sum()
        res = scipy.optimize.minimize(fitfunc, x0 = (1.,1.),args=(xs,ys,errs,ULs),
                                      method='Nelder-Mead',options={'maxiter':10000,'disp':pr})
        return res
def expfit(xs,ys,errs=None,ULs=None,pr=True):
        if errs==None: errs = ones(len(xs))
        if ULs ==None: ULs = zeros(len(xs))
        def fitfunc(ps,x,y,err,UL): 
                vals = ps[0]*e**(-x/ps[1])
                return (((vals-y)/err)**2 * ((UL==0) + (vals>y))).sum()
        res = scipy.optimize.minimize(fitfunc, x0 = (1.,1.),args=(xs,ys,errs,ULs),
                                      method='Nelder-Mead',options={'maxiter':10000,'disp':pr})
        return res


def polyfit2(tups, n,doLogs=[True,True],plotFit=True,pr=1,color='k',linewidth=1,linestyle='-',
             retchis=False,retError=False,scaleerrors=True):
        if len(tups[0])==2: xfunc, yfunc = [[lambda x: x, lambda x: log(x)][i] for i in doLogs]
        else: xfunc, yfunc, efunc = [[lambda x: x, lambda x: log(x)][i] for i in doLogs]
        if len(tups[0])==3: errs = array([efunc(x[2]) for x in tups])
        else: errs = array([1. for x in tups])
        mpf = mpfit(lambda ps,fjac,x,y,e: [0,(reduce(lambda x,y: x+y, [ps[len(ps)-1-i]*x**i for i in rl(ps)]) -y)/e], 
                    functkw={'x':array([xfunc(x[0]) for x in tups]), 
                           'y':array([yfunc(x[1]) for x in tups]),
                           'e':errs},parinfo=[{'value':0.}]*n,nprint=pr)
        p = mpf.params
        if plotFit:
                yfunc = [lambda x: x, lambda x: 10**x][ doLogs[1]]
                xs = [linspace(xlim()[0], xlim()[1], 100),logspace(log(xlim()[0]), log(xlim()[1]), 100)][doLogs[0]]
                plot(xs,yfunc( reduce(lambda x,y: x+y,[p[len(p)-1-i]*xfunc(xs)**i for i in rl(p)])),ls=linestyle,c=color,linewidth=linewidth)
        if retchis:
                return p, mpf.fnorm
        elif retError:
                if scaleerrors:
                        dof = len(tups) - len(p) # deg of freedom
                        # scaled uncertainties
                        pcerror = mpf.perror * (mpf.fnorm / dof)**0.5
                else:
                        pcerror = mpf.perror
                if n==3: 
                        correl = [mpf.covar[a,b] / (mpf.covar[a,a]*mpf.covar[b,b])**0.5
                                  for a,b in [(0,1), (1,2), (0,2)]]			          
                        return p, pcerror, correl
                else:
                        return p, pcerror		
        else: 
                return p	


def brokenlinefit2(tups, doLogs=[True,True],plotFit=True,pr=1,color='',linewidth=1,linestyle='-', retScatter=False):
        xfunc, yfunc = [[lambda x: x, lambda x: log(x)][i] for i in doLogs]
        fitfunc = lambda x, ps: array([ps[0],ps[1]]).take(x>ps[2])*(x-ps[2])+ps[3]
        xs = array([xfunc(x[0]) for x in tups])
        ys = array([yfunc(x[1]) for x in tups])
        p = mpfit(lambda ps,fjac,x,y: [0,fitfunc(x,ps)-y], 
                  functkw={'x':xs,'y':ys}, parinfo=[{'value':1.},{'value':0.},{'value':700.},{'value':0}],nprint=pr).params
        if plotFit:
                yfunc = [lambda x: x, lambda x: 10**x][ doLogs[1]]
                mid = [p[2], 10**p[2]][doLogs[0]]
                plot([xlim()[0],mid,xlim()[1]],yfunc(fitfunc(xfunc(array([xlim()[0],mid,xlim()[1]])),p)),linestyle+color,linewidth=linewidth)
        if retScatter:
                return p, array([ys[i] - fitfunc(xs[i],p) for i in rl(xs)]).var()**0.5
        return p

def plotLine(a=1,b=0,doLogs=[False,False],throughPoint=None,**kwargs):
        xfunc = [lambda x: x, lambda x: log(x)][doLogs[0]]
        yfunc = [lambda x: x, lambda x: 10**x][doLogs[1]]
        if throughPoint!=None:
                x,y = throughPoint
                b = y-a*x
        return plot(xlim(), yfunc(xfunc(array(xlim()))*a + b),**kwargs)

def binVals(arr,binarr, func=None,expand=False):
        #returns value before and after values in arr
        if func==None: func = lambda x: x	
        expanded = concat([array([-inf]),binarr,array([inf])])
        grp = group(lambda x: tuple(expanded[searchsorted(binarr, func(x)):searchsorted(binarr, func(x))+2]), arr)
        if expand: return grp
        else:
                grp.pop(tuple(expanded[:2]),0)
                grp.pop(tuple(expanded[-2:]),0)
                return grp
def PoissonDist(lamb, k=None):
        if k==None: k=lamb
        return 1./factorial(k) * lamb**k * exp(-lamb)

def unpack(*args):
        return args

def mreplace(st, froms,tos):
        s = st
        for i in rl(froms):
                s = s.replace(froms[i],tos[i])
        return s

import matplotlib.transforms
from matplotlib.transforms import blended_transform_factory, IdentityTransform
def offset(ax, x, y):
        trans = blended_transform_factory(ax.transData, ax.transData)
        trans.set_offset((x,y), IdentityTransform())
        return trans

def transform2(trans,xy):
        """A debug for some transform types"""
        return tuple(trans.transform(array([xy])).tolist()[0])

import matplotlib
import matplotlib.figure
class RotatableTextFigure(matplotlib.figure.Figure):
        class RotatableText:
                def __init__(self, text, twopoints, alignByAngle, dangle=0):
                        self.text = text
                        self.x1, self.y1, self.x2, self.y2 = twopoints
                        self.alignByAngle = alignByAngle
                        self.dangle=dangle
                def rotationAng(self):
                        x1,y1 = transform2(self.text.get_axes().transData, (self.x1, self.y1))
                        x2,y2 = transform2(self.text.get_axes().transData, (self.x2, self.y2))
                        return arctan((y2-y1)/(x2-x1))*180/pi + self.dangle
                def updateAng(self):
                        ang = self.rotationAng()
                        if self.alignByAngle: self.text.set_verticalalignment(('bottom','top')[ang<0])
                        self.text.set_rotation(self.rotationAng())
        class SmallAx:
                def __init__(self):
                        self.xtext = None
                        self.xfmt  = None
                        self.ytext = None
                        self.yfmt  = None
                def update(self,axis,txt,fmt):
                        if axis=='x':
                                self.xtext = txt
                                self.xfmt  = fmt
                        else:
                                self.ytext = txt
                                self.yfmt  = fmt
        def __init__(self,tickright=False,ticktop=False,showAllYaxislabels=False, showAllXaxislabels=False, showLabelRight=None,showLabelTop=None,**kwargs):
                matplotlib.figure.Figure.__init__(self,**kwargs)
                self.rottexts = []
                self.firstTime = True
                self.formattedAndLabeled = {}
                if not showAllXaxislabels: self.subplots_adjust(hspace=1e-4)
                if not showAllYaxislabels: self.subplots_adjust(wspace=1e-4)
                self.tickright = tickright
                self.ticktop = ticktop
                self.showLabelRight = showLabelRight
                self.showLabelTop = showLabelTop
                self.xkwargs = {}
                self.ykwargs = {}
                self.showAllXaxislabels = showAllXaxislabels
                self.showAllYaxislabels = showAllYaxislabels

        def addRotatableText(self, text, twopoints, alignByAngle,dangle=0):
                if self.firstTime:
                        self.canvas.mpl_connect('draw_event', self._onRedraw)
                        self.recursive = False
                        self.firstTime = False
                self.rottexts.append(self.RotatableText(text, twopoints, alignByAngle, dangle))
        def _onRedraw(self, event):
                if not self.recursive:
                        [rt.updateAng() for rt in self.rottexts]
                        self.recursive = True
                        self.canvas.draw()
                else:
                        self.recursive = False
        def gcsp(self):
                return [ax for ax in self.axes if ax==gca()][0]
        def firstrow(self, col):
                firstrow = inf
                for ax in self.axes:
                        if hasattr(ax, 'colNum') and ax.colNum == col:
                                firstrow = min(firstrow, ax.rowNum)
                return firstrow
        def lastrow(self, col):
                lastrow = -1
                for ax in self.axes:
                        if hasattr(ax, 'colNum') and ax.colNum == col:
                                lastrow = max(lastrow, ax.rowNum)
                return lastrow
        def firstcol(self, row):
                firstcol = inf
                for ax in self.axes:
                        if hasattr(ax, 'rowNum') and ax.rowNum == row:
                                firstcol = min(firstcol, ax.colNum)
                return firstcol	
        def lastcol(self, row):
                lastcol = -1
                for ax in self.axes:
                        if hasattr(ax, 'rowNum') and ax.rowNum == row:
                                lastcol = max(lastcol, ax.colNum)
                return lastcol		
        def updateFormatAndLabel(self):
                for ax,sax in self.formattedAndLabeled.items():
                        self.yformat(sax.yfmt,ax)
                        self.ylabel(sax.ytext,ax,**self.ykwargs)
                        self.xformat(sax.xfmt,ax)
                        self.xlabel(sax.xtext,ax,**self.xkwargs)
                        ax.fmt_xdata = lambda x,pos=None: '%.3g'%x
                        ax.fmt_ydata = lambda x,pos=None: '%.3g'%x
        def addAxAndUpdateFormatAndLabel(self,axis,txt=None,fmt=None):
                ax = self.gcsp()
                if ax not in self.formattedAndLabeled: self.formattedAndLabeled[ax] = self.SmallAx()
                self.formattedAndLabeled[ax].update(axis,txt,fmt)
                self.updateFormatAndLabel()
        def ylabel(self, txt=None, ax=None, **kwargs):
                self.ykwargs = kwargs
                if ax==None: self.addAxAndUpdateFormatAndLabel('y',txt=txt)
                else:
                        if ax.colNum == self.firstcol(ax.rowNum) or self.showAllYaxislabels:
                                if txt!=None: ax.set_ylabel(txt,**kwargs)
                        elif self.tickright and ax.colNum == self.lastcol(ax.rowNum):		
                                ax.yaxis.set_label_position('right')
                                ax.yaxis.set_ticks_position('right')
                                ax.yaxis.set_ticks_position('both')				
                                if txt!=None:
                                        if self.showLabelRight!=False: ax.set_ylabel(txt,**kwargs)
                                        else: ax.set_ylabel('')
                        else: 
                                ax.set_ylabel('')
        def yformat(self, formatter=None, ax=None):
                if ax==None: self.addAxAndUpdateFormatAndLabel('y',fmt=formatter)
                else:
                        if ax.colNum == self.firstcol(ax.rowNum) or self.showAllYaxislabels: 
                                if formatter!=None: ax.yaxis.set_major_formatter( formatter )
                        elif self.tickright and ax.colNum == self.lastcol(ax.rowNum):
                                ax.yaxis.set_label_position('right')
                                if formatter!=None: ax.yaxis.set_major_formatter( formatter )
                        else: 
                                ax.yaxis.set_major_formatter( matplotlib.ticker.NullFormatter() )

        def xlabel(self, txt=None,ax=None, **kwargs):
                self.xkwargs = kwargs
                if ax==None: self.addAxAndUpdateFormatAndLabel('x',txt=txt)
                else:
                        if ax.rowNum == self.lastrow(ax.colNum) or self.showAllXaxislabels: 
                                if txt!=None: ax.set_xlabel(txt,**kwargs)
                        elif self.ticktop and ax.rowNum == self.firstrow(ax.colNum):				
                                ax.xaxis.set_label_position('top')
                                ax.xaxis.set_ticks_position('top')
                                ax.xaxis.set_ticks_position('both')				
                                if txt!=None:
                                        if self.showLabelTop!=False: ax.set_xlabel(txt,**kwargs)
                                        else: ax.set_xlabel('')
                        else: 
                                ax.set_xlabel('')
        def xformat(self, formatter=None,ax=None):
                if ax==None: self.addAxAndUpdateFormatAndLabel('x',fmt=formatter)
                else:
                        if ax.rowNum == self.lastrow(ax.colNum) or self.showAllXaxislabels: 
                                if formatter!=None: ax.xaxis.set_major_formatter( formatter )
                        elif self.ticktop and ax.rowNum == self.firstrow(ax.colNum):
                                ax.xaxis.set_label_position('top')
                                if formatter!=None: ax.xaxis.set_major_formatter( formatter )
                        else: 
                                ax.xaxis.set_major_formatter( matplotlib.ticker.NullFormatter() )

        def xloc(self,locator,major=True):
                if major: self.gcsp().xaxis.set_major_locator(locator)
                else: self.gcsp().xaxis.set_minor_locator(locator)
        def yloc(self,locator,major=True):
                if major: self.gcsp().yaxis.set_major_locator(locator)
                else: self.gcsp().yaxis.set_minor_locator(locator)

def rtfig(num=None, figsize=None, dpi=None, facecolor=None, edgecolor=None,frameon=True, **kwargs):
        return figure(num, figsize, dpi, facecolor, edgecolor, frameon, FigureClass=RotatableTextFigure, **kwargs)

def restoreFigCoords(res=3):
        for ax in gcf().axes:
                ax.fmt_xdata = lambda x,pos=None: ('%.' +str(res)+'g')%x
                ax.fmt_ydata = lambda x,pos=None: ('%.' +str(res)+'g')%x

def annotateLine(line,s,x=None,y=None,xytext=(0,0),xycoords='data', textcoords='offset points', linkdic={},
                 rotate=True, alignByAngle=True, dangle=0, backgroundcolor=None, **kwargs):
        """Either x xor y should not be None. The axis that is not None should have data points in sorted (ascending/descending) order."""
        assert ( x!=None ) ^ ( y!=None )
        ax = line.get_axes()
        xs = line.get_xdata(); ys = line.get_ydata()
        if xycoords=='axes fraction': 
                tr = ax.transAxes + ax.transData.inverted()
                if x!=None: x = transform2(tr, (x,0))[0]
                else: y = transform2(tr, (0,y))[1]
        if x!=None: 
                if xs[0]<xs[-1]: i = searchsorted(xs, x)
                else: i = len(xs) - searchsorted(xs[::-1], x)
        else: 
                if ys[0]<ys[-1]: i = searchsorted(ys, y)
                else: i = len(ys) - searchsorted(ys[::-1], y)
        if i<=0: i=1
        if i>=len(xs): i=len(xs)-1
        x1,y1 = transform2(ax.transData, (xs[i-1], ys[i-1]))
        x2,y2 = transform2(ax.transData, (xs[i],ys[i]))
        if x!=None:
                xmid = transform2(ax.transData, (x,0))[0]
                ymid = lineFunc(x1,y1,x2,y2)(xmid)
        else:
                ymid = transform2(ax.transData, (0,y))[1]
                xmid = lineFunc(y1,x1,y2,x2)(ymid)

        x,y = transform2(ax.transData.inverted(), (xmid,ymid))
        if not kwargs.has_key('color'): kwargs['color'] = line.get_color()
        if linkdic!={} and textcoords=='data': 
                plot([x,xytext[0]],[y,xytext[1]], **linkdic)
                backgroundcolor='w'
        if backgroundcolor!=None: t = ax.annotate(s, (x,y), xytext, 'data', textcoords=textcoords, backgroundcolor=backgroundcolor,**kwargs)
        else:t = ax.annotate(s, (x,y), xytext, 'data', textcoords=textcoords, **kwargs)
        if rotate: ax.get_figure().addRotatableText(t, (xs[i-1], ys[i-1], xs[i], ys[i]), alignByAngle, dangle)
        ax.get_figure().canvas.draw()
        return t


def shapeParams(xs,ys,maxFraction=0.5, retPoints=False):
        maxInd = ys.argmax()
        m = ys.max()
        rightWL = maxInd; leftWL = maxInd
        for i in range(len(ys)-1,maxInd-1,-1):
                if ys[i] > maxFraction*m:
                        if i<len(ys)-1: rightWL = (xs[i]*(maxFraction*m - ys[i+1]) + xs[i+1]*(ys[i] - maxFraction*m)) / (ys[i]-ys[i+1])
                        else: 		rightWL = xs[i]
                        break
        for i in range(0, maxInd+1):
                if ys[i] > maxFraction*m:
                        if i>0: leftWL = (xs[i]*(maxFraction*m - ys[i-1]) + xs[i-1]*(ys[i] - maxFraction*m)) / (ys[i]-ys[i-1])
                        else:   leftWL = xs[i]
                        break
        val = rightWL - leftWL
        if retPoints: return val, leftWL, rightWL, maxFraction*m
        return val

def exctinctionCurve(Eb_v, wls, Rv = 3.1,origin='Card89',wl0=4400., ext0=1.):
        if origin=='Card89':  #cardelli 89
                Av = Eb_v * Rv
                a = lambda y: 1+0.17699*y - 0.50447*y**2 - 0.02427*y**3 + 0.72085*y**4 \
                        +0.01979*y**5 - 0.77530*y**6 + 0.32999*y**7
                b = lambda y: 1.41338*y + 2.28305*y**2 + 1.07233*y**3 - 5.38434*y**4 - \
                        0.62251*y**5 + 5.30260*y**6 - 2.09002*y**7
                y = 1e4/wls - 1.82
                return (a(y) + b(y) / Rv) * Av
        #Gaskell 2007
        if origin=='Gaskell':  # good for 1216<wl<6564, In Gaskell04 Rv = 5.15, but by fitting to MW at \lambda>4000, R=3.1 is the relevant one
                Rv=3.1
                a = lambda x: 0.000843*x**5 - 0.02496*x**4 + 0.2919*x**3 - 1.815*x**2 + 6.83*x - 7.92		
                return (a(1e4/wls) + Rv)* Eb_v 

        #pei 92
        if origin=='MW':
                a = 165., 14., 0.045,0.002,0.002,0.012
                lamb = 0.047, 0.08, 0.22,9.7,18,25
                b = 90., 4., -1.95,-1.95,-1.8,0.
                n = 2.,6.5,2.,2.,2.,2.
        if origin=='LMC':
                a = 175., 19., 0.023
                lamb = 0.046, 0.08, 0.22
                b = 90., 5.5, -1.95
                n = 2.,4.5,2.
        if origin=='SMC':
                a = 185., 27., 0.005
                lamb = 0.042, 0.08, 0.22
                b = 90., 5.5, -1.95
                n = 2.,4.,2.
        xsi = lambda wl: reduce(lambda x,y: x+y, [ a[i] / ( (wl/lamb[i])**n[i] + (wl/lamb[i])**(-n[i]) + b[i] ) for i in rl(a) ])
        assert(Eb_v!=0)
        if Eb_v==0: A_B = ext0 / xsi(wl0/1e4)
        else: A_B = Eb_v / ( xsi(0.44) - xsi(0.55) )
        return xsi(wls/1e4) * A_B

def exctinctionFilter(Eb_v, transmission, origin, slope):
        slopelambLlamb = slope + 1.
        num = 0.; denom = 0.
        for iwl in range(1, len(transmission)-1):
                wl, sens = transmission[iwl]
                wl_b = (wl + transmission[iwl-1][0])/2.
                wl_a = (wl + transmission[iwl+1][0])/2.
                num   += (1./wl)**slopelambLlamb * sens * (wl_a - wl_b)
                denom += (1./wl)**slopelambLlamb * sens * (wl_a - wl_b) * 10**(-0.4*exctinctionCurve(Eb_v, wl, origin=origin))
        return log( num / denom ) / 0.4

def filterResponse(transmission, slope, effectiveWL=False):
        if not effectiveWL: slopelambLlamb = slope + 1.
        else: slopelambLlamb=1
        num = 0.; denom = 0.
        for iwl in range(1, len(transmission)-1):
                wl, sens = transmission[iwl]
                wl_b = (wl + transmission[iwl-1][0])/2.
                wl_a = (wl + transmission[iwl+1][0])/2.
                num   += (1./wl)**slopelambLlamb * sens * (wl_a - wl_b)
                denom += sens * (wl_a - wl_b) 
        return num / denom 


PARSEC2CM = 3.086e18
standardSphere = 4*pi*(10*PARSEC2CM)**2
def ABmag2flux(mag, wl=None,nln=False):
        if wl==None:
                return 10**(-0.4*(mag + 48.6)) #ergs / s / Hz / cm^2
        else:
                if not nln:
                        return 10**(-0.4*(mag + 48.6))  * (3e18/wl) / wl
                else:
                        return 10**(-0.4*(mag + 48.6))  * (3e18/wl)
def ABmag2lum(mag,obswl,nln=True):
        return ABmag2flux(mag, obswl, nln) * standardSphere
def nln2mag(nln, wl):
        return log(nln / standardSphere / (3e18/wl) )/-0.4 - 48.6

def flux2ABmag(flux, wl=None,nln=False):
        if wl==None:
                return log(flux)/-0.4 - 48.6
        else:
                if not nln:
                        return log(flux/ (3e18/wl) * wl)/-0.4 - 48.6  
                else:
                        return log(flux/(3e18/wl)) /-0.4 - 48.6

def linehist(vals,newfig=False, plotkeys={}, mynorm=False,onlyPositives=False,xlog=False,**keys):
        q = hist(vals, visible=False, **keys)
        if newfig: figure()
        if not xlog: xs = ( q[1][1:] + q[1][:-1] ) / 2.
        else: xs = 10**( ( log(q[1][1:]) + log(q[1][:-1]) ) / 2. )
        ys = q[0]*1.
        if onlyPositives:
                inds = array([i for i in rl(xs) if ys[i]>0])
                xs = xs.take(inds)
                ys = ys.take(inds)
        if mynorm: ys /= len(vals)
        ls = plot(xs,ys, **plotkeys)
        return q, ls
def edge2Mean(arr):
        return (arr[1:]+arr[:-1])/2.
def mean2Edge(arr):
        d = arr[1]-arr[0]
        return concat([array([arr[0]-d/2.]),arr+d/2.])

def coeffandpower(flt):
        pwr =int(log(flt))
        coeff = iround(flt / 10**pwr)
        return coeff, pwr
def fraction(func,lst,prcnt=True):
        return len(filter(func,lst))*(1.,100.)[prcnt] / len(lst)
def threadplay():
        """
        doesn't work probably because of interpreter. see documentation
        """
        import threading

        def f():
                prefix = threading.currentThread().name
                f = file(prefix,'w')
                c=0
                while c!=1e8:
                        if c%1e6==0: 
                                f.write(prefix + ': ' + str(c)+'\n')
                                f.flush()
                        c+=1

        t1 = threading.Thread(target=f, name='thread1')
        t1.start()
        t1.run()
        t2 = threading.Thread(target=f, name='thread2')
        t2.run()
def nonuniqdict(tups,filtfunc=None,sortfunc=None):
        if filtfunc==None: filtfunc = lambda k: True	
        d = {}
        for tup in tups:
                if filtfunc(tup[1]):
                        if tup[0] in d:
                                d[tup[0]].append(tup[1])
                        else:
                                d[tup[0]] = [tup[1]]
        if sortfunc!=None:
                for k in d:
                        d[k] = sorted(d[k], key=sortfunc)
        return d
def buildMarker(angles):
        """
        angle 0 is uparrow
        """
        verts = []
        if 0 in angles:
                verts += [(0,1), (-0.4,0.6), (0,1), (0,0), (0,1), (0.4,0.6), (0,1), (0,0)]
        if 90 in angles:
                verts += [(1,0), (0.6,-0.4), (1,0), (0,0), (1,0), (0.6,0.4), (1,0), (0,0)]
        if 180 in angles:
                verts += [(0,-1), (-0.4,-0.6), (0,-1), (0,0), (0,-1), (0.4,-0.6), (0,-1), (0,0)]
        if 270 in angles:
                verts += [(-1,0), (-0.6,-0.4), (-1,0), (0,0), (-1,0), (-0.6,0.4), (-1,0), (0,0)]
        if 315 in angles:
                verts += [(-1,1), (-1,0.5), (-1,1), (0,0), (-1,1), (-0.5,1), (-1,1), (0,0)]
        if len(verts)==0: 
                return [(0,1),(0,0.3),(0.3,0),(0,-0.3),(-0.3,0),(0,0.3)]
        else: 
                return verts
def findNotClosedBrackets(fn):
        ls = file(fn).readlines()
        locarr = []
        for il in rl(ls):
                l = ls[il]
                for c in l:
                        if c=='{': 
                                locarr.append(il)
                        if c=='}':
                                if len(locarr)>0:
                                        locarr = locarr[:-1]
                                else:
                                        print 'closed before opened at line %d'%il
                                        return 
        return locarr
def mathover(n,k):
        return factorial(n) / factorial(k) / factorial(n-k)
def mifratio(mif):
        return mif.get(True,0)*1./(mif.get(True,0.)+mif.get(False,0.))

def findpairs(list1, list2, dist,outfile=None):
        """
        list1, list2: lists of ra,dec pairs (in degrees)
        dist: maximum separation (in degrees)

        output: a list of triplets i,j,sep where the angular distance between list1[i] and list2[j] is sep, which is less than dist
        """
        res = []
        sdecinds = sorted(rl(list2),key=lambda j: list2[j][1])
        sdecs = array([list2[j][1] for j in sdecinds])

        for i in Progress(rl(list1)):
                r1, d1 = list1[i]
                ind = searchsorted(sdecs,d1)
                jlist=[]
                k = ind-1
                while d1 - list2[sdecinds[k]][1] < dist: 
                        if k<0: break
                        jlist.append(sdecinds[k])
                        k-=1
                k=ind
                while k<len(sdecinds) and list2[sdecinds[k]][1] - d1 < dist: 
                        if k==len(sdecinds): break
                        jlist.append(sdecinds[k])
                        k+=1
                for j in jlist:
                        r2, d2 = list2[j]
                        try:
                                sep = myangulardist((r1,d1),(r2,d2))
                        except:
                                print "BAD: ",i,j,r1
                                continue
                        if -dist<sep<dist:
                                res.append((i,j,sep))
                                if outfile!=None:
                                        outfile.write('%d %d %.6f\n'%(i,j,sep))
                                        outfile.flush()

        return res

import math
def myangulardist(a, b):
        """Angular separation in degrees between two sky coordinates"""

        ra1, dec1 = a
        ra2, dec2 = b
        ra1 = ra1*pi/180.
        dec1 = dec1*pi/180.
        ra2 = ra2*pi/180.
        dec2 = dec2*pi/180.
        mu = (cos(dec1)*cos(ra1)*cos(dec2)*cos(ra2)
              + cos(dec1)*sin(ra1)*cos(dec2)*sin(ra2) + sin(dec1)*sin(dec2))
        return math.acos(mu)*180./pi

class Struct:
        def __init__(self,name,initype='vals',valarray=None,keylist=None,descriptionList=None,**keys):
                self.name = name
                if initype=='vals':			
                        self.atrlist = keys.keys()
                        for k in keys:
                                setattr(self,k,keys[k])
                if initype=='array':
                        #values of 'keys' dictionary should be indices
                        self.vals = valarray
                        self.atrlist = keylist
                        self.indexdic = dict([(self.atrlist[i],i) for i in rl(self.atrlist)])
                        for ik in rl(keylist):
                                setattr(self,keylist[ik],valarray[ik])
                                if descriptionList!=None:
                                        setattr(self,keylist[ik]+'_help',descriptionList[ik])
        def __getitem__(self, k):
                return self.vals[self.indexdic[k]]
        def __repr__(self):
                return sumlst(['%s = %s\n'%(k, getattr(self,k)) for k in self.atrlist])
        def __str__(self):
                return self.__repr__()



def rem3sig(arr):
        l = 0
        while len(arr)!=l:
                l = len(arr)
                m = arr.mean()
                s = arr.std()
                arr = array([x for x in arr if m-3*s<x<m+3*s])
        return arr


def printTbl(tbl,fn=None,header=None,footer=None,yeshur=True, tsv=False,hlineindices=[]):
        if fn!=None: f = file(fn,'w')
        if header!=None:
                if fn!=None: f.write('%s'%header)
                else: print header	
        colwidths = [max([len(l[i]) for l in tbl]) for i in rl(tbl[0])]
        for il in rl(tbl):
                l = tbl[il]
                if yeshur: l2 = [('%' +'%d'%colwidths[i] + 's')%(l[i]) for i in rl(l)]
                else: l2 = ['%s'%(l[i]) for i in rl(l)]
                if tsv: s = string.join(l2,'\t')
                else: s = string.join(l2,r'  &  ')+r'  \\'
                if fn!=None: 
                        f.write('%s\n'%s)
                        if il in hlineindices: f.write('\\hline\n')
                else: 
                        print s
                        if il in hlineindices: print '\\hline\n'
        if footer!=None:
                if fn!=None: f.write('%s'%footer)
                else: print header	

def bool2int(boollist):
        l = len(boollist)
        bases = 2**arange(len(boollist))
        return (boollist * bases).sum()

class Enum:
        def __init__(self, vals):
                assert(len(unique(vals))==len(vals))
                self.vals = vals
                for v in vals:
                        setattr(self, v, v)
        def __iter__(self):
                return self.vals.__iter__()
        def isValid(self,val):
                return val in self
        def index(self,v):
                return self.vals.index(v)

lim2cups = lambda mn,mx,ngroups = 4: array([linspace(mn,mx,ngroups+1)[i:i+2] for i in range(ngroups)])
def stdN(arr):
        return std(arr)/len(arr)**0.5

def nSignificantDigits(N,sigdig,retstr=False):
        if N==0.: 
                if retstr: return '0'
                return 0.
        sn = abs(N)/N
        N = abs(N)
        maxpower = int(log(N))
        if log(N)<0 and maxpower!=log(N): maxpower-=1
        afterpoint = maxpower+1-sigdig
        val = sn * iround(N, 10**afterpoint)
        if retstr: 
                if afterpoint>=0: return '%d'%val
                else: 
                        s = ('%.'+'%d'%(-afterpoint)+'f')%val
                        if s[-2:] == '.0': s = s[:-2]  #patch for nSigDig(0.997,1,True)
                        return s
        return val

def nSignificantDigitsFormatter(sigdig=1):
        return matplotlib.ticker.FuncFormatter(lambda x,pos,sigdig=sigdig: nSignificantDigits(x,sigdig,True))


class hashdict(dict):
        def __init__(self,func,default,*args,**kwargs):
                self.hashfunc = func
                self.default = default
                tmpdic = dict(*args,**kwargs)
                for k in tmpdic:
                        self[k] = tmpdic[k]
        def __getitem__(self,k):
                if self.hashfunc(k) not in self:
                        return self.default
                return dict.__getitem__(self,self.hashfunc(k))
        def __setitem__(self,k,val):
                return dict.__setitem__(self,self.hashfunc(k),val)
class SubstringDic(dict):
        def __getitem__(self,substr):
                return dict.__getitem__(self,self.getFullKey(substr))
        def get(self,substr,default=None):
                try:
                        return self.getFullKey(substr)
                except KeyError:
                        if default!=None: return default
                        raise KeyError, substr	
        def has_key(self,substr):
                for k in self:
                        if substr in k:
                                return True
                return False		
        def getFullKey(self,substr):
                for k in self:
                        if substr in k:
                                return k
                raise KeyError, substr


def processinfo(title):
        print title
        print 'module name:', __name__
        print 'parent process:', os.getppid()
        print 'process id:', os.getpid()	

def matchGroups(grpA,grpB,matchFuncsA,maxdmatchs,matchFuncsB=None,idfuncA=hash,idfuncB=hash):	
        if matchFuncsB==None: matchFuncsB=matchFuncsA
        mfdics = []
        for imf in rl(matchFuncsA):
                mfdics.append(     dict([(idfuncA(a), matchFuncsA[imf](a)) for a in grpA])) 
                mfdics[imf].update(dict([(idfuncB(b), matchFuncsB[imf](b)) for b in grpB]))
        couples = {}; invcouples = {}
        for a in Progress(grpA):
                for b in grpB:
                        isPotentialMatch=True
                        if idfuncB(b) in couples: continue
                        for imf in rl(matchFuncsA):
                                if abs(mfdics[imf][idfuncA(a)]-mfdics[imf][idfuncB(b)])>maxdmatchs[imf]:
                                        isPotentialMatch=False
                                        break
                        if isPotentialMatch:				
                                couples[idfuncB(b)]    = idfuncA(a)
                                invcouples[idfuncA(a)] = idfuncB(b)
                                break
        return couples, invcouples
def floatCanFail(s):
        try:
                return float(s)
        except:
                return s
class Progress:
        def __init__(self, iterable,name='progress',frac=None,fmt='%d',dummy=False):
                self.iterable = iterable		
                self.fmt=fmt
                self.dummy=dummy
                if frac!=None: self.frac = frac
                else: 
                        if len(self.iterable)>1: self.frac = max(0.01, 1./(len(self.iterable)-1.))
                        else: self.frac = 1.
                self.name=name
        def __iter__(self):
                self.origIter = self.iterable.__iter__()
                self.startTime = time.time()
                self.i = 0.
                self.looplen = len(self.iterable)
                self.printEvery = iround(self.looplen*self.frac)
                return self
        def next(self):
                if not self.dummy and self.printEvery>0:
                        if self.i%self.printEvery==0.: 
                                if self.looplen>0:
                                        clear_output()					
                                        print '\r','%s: '%self.name, self.fmt%(self.i*100./self.looplen) + '%', int(time.time()-self.startTime), 'seconds passed',
                                sys.stdout.flush()
                self.i+=1
                try:
                        return self.origIter.next()
                except StopIteration:
                        if not self.dummy and self.printEvery>0: 
                                print
                                sys.stdout.flush()
                        raise StopIteration


def searchsortedclosest(arr, val):
        assert(arr[0]!=arr[1])
        if arr[0]<arr[1]:
                ind = searchsorted(arr,val)
                ind = minarray(ind, len(arr)-1)
                return maxarray(ind - (val - arr[maxarray(ind-1,0)] < arr[ind] - val),0)        
        else:
                ind = searchsorted(-arr,-val)
                ind = minarray(ind, len(arr)-1)
                return maxarray(ind - (-val + arr[maxarray(ind-1,0)] < -arr[ind] + val),0)        



def maxarray(arr, v):
        return arr + (arr<v)*(v-arr)
def minarray(arr, v):
        return arr + (arr>v)*(v-arr)

def searchAndInterpolate(arr_xs,val,arr_ys): # beyond edges takes edge values
        ind = searchsorted(arr_xs,val)
        if ind==0: return arr_ys[0]
        if ind==len(arr_xs): return arr_ys[-1]
        if arr_xs[ind]==val: return arr_ys[ind]
        return lineFunc(arr_xs[ind-1], arr_ys[ind-1], arr_xs[ind], arr_ys[ind])(val)
def searchAndInterpolateArr(arr_xs,vals,arr_ys,weights=None): # beyond edges extrapolates from last two points
        inds = searchsorted(arr_xs,vals)
        inds = minarray( maxarray(inds,1),len(arr_xs)-1)
        x1s = arr_xs.take(inds-1)
        y1s = arr_ys.take(inds-1)

        x2s = arr_xs.take(inds)
        y2s = arr_ys.take(inds)

        if weights!=None:
                w1s = weights.take(inds-1)
                w2s = weights.take(inds)
        else:
                w1s = w2s = 1.

        return ( y2s*w2s*(vals-x1s) + y1s*w1s*(x2s-vals) ) / (w1s*(vals-x1s) + w2s*(x2s-vals))

def searchAndInterpolate1D(arr_xs,valx,arr_ys): # beyond edges extrapolates from last two points
        i = min( max(searchsorted(arr_xs, valx), 1), len(arr_xs)-1)

        dx = arr_xs[i] - arr_xs[i-1]
        xn = 1.* (valx - arr_xs[i-1]) / dx

        return arr_ys[i-1]*(1-xn) + arr_ys[i]*xn

def searchAndInterpolate2D(arr_xs,valx,arr_ys,valy,matrix_zs):
        i = min( max(searchsorted(arr_xs, valx), 1), len(arr_xs)-1)
        j = min( max(searchsorted(arr_ys, valy), 1), len(arr_ys)-1)

        dx = arr_xs[i] - arr_xs[i-1]
        xn = 1.* (valx - arr_xs[i-1]) / dx
        dy = arr_ys[j] - arr_ys[j-1]
        yn = 1.* (valy - arr_ys[j-1]) / dy

        return (matrix_zs[i-1,j-1]*(1-xn)*(1-yn) + matrix_zs[i-1,j]*(1-xn)*yn + 
                matrix_zs[i,j-1]*xn*(1-yn) + matrix_zs[i,j]*xn*yn)

def searchAndInterpolate2DArr(arr_xs,valx,arr_ys,valy,matrix_zs):
        i = minarray( maxarray(searchsorted(arr_xs, valx), 1), len(arr_xs)-1)
        j = minarray( maxarray(searchsorted(arr_ys, valy), 1), len(arr_ys)-1)

        dx = arr_xs[i] - arr_xs[i-1]
        xn = 1.* (valx - arr_xs[i-1]) / dx
        dy = arr_ys[j] - arr_ys[j-1]
        yn = 1.* (valy - arr_ys[j-1]) / dy

        return (matrix_zs[i-1,j-1]*(1-xn)*(1-yn) + matrix_zs[i-1,j]*(1-xn)*yn + 
                matrix_zs[i,j-1]*xn*(1-yn) + matrix_zs[i,j]*xn*yn)



def searchAndInterpolate3D(arr_xs,valx,arr_ys,valy,arr_zs, valz, matrix_ws):
        i = min( max(searchsorted(arr_xs, valx), 1), len(arr_xs)-1)
        j = min( max(searchsorted(arr_ys, valy), 1), len(arr_ys)-1)
        k = min( max(searchsorted(arr_zs, valz), 1), len(arr_zs)-1)

        dx = arr_xs[i] - arr_xs[i-1]
        xn = 1.* (valx - arr_xs[i-1]) / dx
        dy = arr_ys[j] - arr_ys[j-1]
        yn = 1.* (valy - arr_ys[j-1]) / dy
        dz = arr_zs[k] - arr_zs[k-1]
        zn = 1.* (valz - arr_zs[k-1]) / dz

        return ( (matrix_ws[i-1,j-1,k-1]*(1-xn)*(1-yn) + matrix_ws[i-1,j,k-1]*(1-xn)*yn + matrix_ws[i,j-1,k-1]*xn*(1-yn) + matrix_ws[i,j,k-1]*xn*yn)*(1-zn) +
                 (matrix_ws[i-1,j-1,k]  *(1-xn)*(1-yn) + matrix_ws[i-1,j,k]  *(1-xn)*yn + matrix_ws[i,j-1,k]  *xn*(1-yn) + matrix_ws[i,j,k]  *xn*yn)*zn )

def searchAndInterpolate4D(arr_xs,valx,arr_ys,valy,arr_zs, valz, arr_ws,valw, matrix_vs):
        i = min( max(searchsorted(arr_xs, valx), 1), len(arr_xs)-1)
        j = min( max(searchsorted(arr_ys, valy), 1), len(arr_ys)-1)
        k = min( max(searchsorted(arr_zs, valz), 1), len(arr_zs)-1)
        l = min( max(searchsorted(arr_ws, valw), 1), len(arr_ws)-1)


        dx = arr_xs[i] - arr_xs[i-1]
        xn = 1.* (valx - arr_xs[i-1]) / dx
        dy = arr_ys[j] - arr_ys[j-1]
        yn = 1.* (valy - arr_ys[j-1]) / dy
        dz = arr_zs[k] - arr_zs[k-1]
        zn = 1.* (valz - arr_zs[k-1]) / dz
        dw = arr_ws[l] - arr_ws[l-1]
        wn = 1.* (valw - arr_ws[l-1]) / dw

        return ( ( (matrix_vs[i-1,j-1,k-1,l-1]*(1-xn)*(1-yn) + matrix_vs[i-1,j,k-1,l-1]*(1-xn)*yn + matrix_vs[i,j-1,k-1,l-1]*xn*(1-yn) + matrix_vs[i,j,k-1,l-1]*xn*yn)*(1-zn) +
                   (matrix_vs[i-1,j-1,k,l-1]  *(1-xn)*(1-yn) + matrix_vs[i-1,j,k,l-1]  *(1-xn)*yn + matrix_vs[i,j-1,k,l-1]  *xn*(1-yn) + matrix_vs[i,j,k,l-1]  *xn*yn)*zn )*(1-wn) + 
                 ( (matrix_vs[i-1,j-1,k-1,l]*(1-xn)*(1-yn) + matrix_vs[i-1,j,k-1,l]*(1-xn)*yn + matrix_vs[i,j-1,k-1,l]*xn*(1-yn) + matrix_vs[i,j,k-1,l]*xn*yn)*(1-zn) +
                   (matrix_vs[i-1,j-1,k,l]  *(1-xn)*(1-yn) + matrix_vs[i-1,j,k,l]  *(1-xn)*yn + matrix_vs[i,j-1,k,l]  *xn*(1-yn) + matrix_vs[i,j,k,l]  *xn*yn)*zn )*wn )

def searchAndInterpolate5D(arr_xs,valx,arr_ys,valy,arr_zs, valz, arr_ws,valw, arr_vs, valv, matrix_qs):
        i = min( max(searchsorted(arr_xs, valx), 1), len(arr_xs)-1)
        j = min( max(searchsorted(arr_ys, valy), 1), len(arr_ys)-1)
        k = min( max(searchsorted(arr_zs, valz), 1), len(arr_zs)-1)
        l = min( max(searchsorted(arr_ws, valw), 1), len(arr_ws)-1)
        m = min( max(searchsorted(arr_vs, valv), 1), len(arr_vs)-1)


        dx = arr_xs[i] - arr_xs[i-1]
        xn = 1.* (valx - arr_xs[i-1]) / dx
        dy = arr_ys[j] - arr_ys[j-1]
        yn = 1.* (valy - arr_ys[j-1]) / dy
        dz = arr_zs[k] - arr_zs[k-1]
        zn = 1.* (valz - arr_zs[k-1]) / dz
        dw = arr_ws[l] - arr_ws[l-1]
        wn = 1.* (valw - arr_ws[l-1]) / dw
        dv = arr_vs[m] - arr_vs[m-1]
        vn = 1.* (valv - arr_vs[m-1]) / dv


        return ( ( ( (matrix_qs[i-1,j-1,k-1,l-1,m-1]*(1-xn)*(1-yn) + matrix_qs[i-1,j,k-1,l-1,m-1]*(1-xn)*yn + matrix_qs[i,j-1,k-1,l-1,m-1]*xn*(1-yn) + matrix_qs[i,j,k-1,l-1,m-1]*xn*yn)*(1-zn) +
                     (matrix_qs[i-1,j-1,k,l-1,m-1]  *(1-xn)*(1-yn) + matrix_qs[i-1,j,k,l-1,m-1]  *(1-xn)*yn + matrix_qs[i,j-1,k,l-1,m-1]  *xn*(1-yn) + matrix_qs[i,j,k,l-1,m-1]  *xn*yn)*zn )*(1-wn) + 
                   ( (matrix_qs[i-1,j-1,k-1,l,m-1]*(1-xn)*(1-yn) + matrix_qs[i-1,j,k-1,l,m-1]*(1-xn)*yn + matrix_qs[i,j-1,k-1,l,m-1]*xn*(1-yn) + matrix_qs[i,j,k-1,l,m-1]*xn*yn)*(1-zn) +
                     (matrix_qs[i-1,j-1,k,l,m-1]  *(1-xn)*(1-yn) + matrix_qs[i-1,j,k,l,m-1]  *(1-xn)*yn + matrix_qs[i,j-1,k,l,m-1]  *xn*(1-yn) + matrix_qs[i,j,k,l,m-1]  *xn*yn)*zn )*wn )*(1-vn) + 
                 ( ( (matrix_qs[i-1,j-1,k-1,l-1,m]*(1-xn)*(1-yn) + matrix_qs[i-1,j,k-1,l-1,m]*(1-xn)*yn + matrix_qs[i,j-1,k-1,l-1,m]*xn*(1-yn) + matrix_qs[i,j,k-1,l-1,m]*xn*yn)*(1-zn) +
                     (matrix_qs[i-1,j-1,k,l-1,m]  *(1-xn)*(1-yn) + matrix_qs[i-1,j,k,l-1,m]  *(1-xn)*yn + matrix_qs[i,j-1,k,l-1,m]  *xn*(1-yn) + matrix_qs[i,j,k,l-1,m]  *xn*yn)*zn )*(1-wn) + 
                   ( (matrix_qs[i-1,j-1,k-1,l,m]*(1-xn)*(1-yn) + matrix_qs[i-1,j,k-1,l,m]*(1-xn)*yn + matrix_qs[i,j-1,k-1,l,m]*xn*(1-yn) + matrix_qs[i,j,k-1,l,m]*xn*yn)*(1-zn) +
                     (matrix_qs[i-1,j-1,k,l,m]  *(1-xn)*(1-yn) + matrix_qs[i-1,j,k,l,m]  *(1-xn)*yn + matrix_qs[i,j-1,k,l,m]  *xn*(1-yn) + matrix_qs[i,j,k,l,m]  *xn*yn)*zn )*wn )*vn )





def searchAndInterpolate3DArr(arr_xs,valx,arr_ys,valy,arr_zs, valz, matrix_ws):
        i = minarray( maxarray(searchsorted(arr_xs, valx), 1), len(arr_xs)-1)
        j = minarray( maxarray(searchsorted(arr_ys, valy), 1), len(arr_ys)-1)
        k = minarray( maxarray(searchsorted(arr_zs, valz), 1), len(arr_zs)-1)

        dx = arr_xs[i] - arr_xs[i-1]
        xn = 1.* (valx - arr_xs[i-1]) / dx
        dy = arr_ys[j] - arr_ys[j-1]
        yn = 1.* (valy - arr_ys[j-1]) / dy
        dz = arr_zs[k] - arr_zs[k-1]
        zn = 1.* (valz - arr_zs[k-1]) / dz

        return ( (matrix_ws[i-1,j-1,k-1]*(1-xn)*(1-yn) + matrix_ws[i-1,j,k-1]*(1-xn)*yn + matrix_ws[i,j-1,k-1]*xn*(1-yn) + matrix_ws[i,j,k-1]*xn*yn)*(1-zn) +
                 (matrix_ws[i-1,j-1,k]  *(1-xn)*(1-yn) + matrix_ws[i-1,j,k]  *(1-xn)*yn + matrix_ws[i,j-1,k]  *xn*(1-yn) + matrix_ws[i,j,k]  *xn*yn)*zn )

def searchAndInterpolate4DArr(arr_xs,valx,arr_ys,valy,arr_zs, valz, arr_ws,valw, matrix_vs):
        i = minarray( maxarray(searchsorted(arr_xs, valx), 1), len(arr_xs)-1)
        j = minarray( maxarray(searchsorted(arr_ys, valy), 1), len(arr_ys)-1)
        k = minarray( maxarray(searchsorted(arr_zs, valz), 1), len(arr_zs)-1)
        l = minarray( maxarray(searchsorted(arr_ws, valw), 1), len(arr_ws)-1)


        dx = arr_xs[i] - arr_xs[i-1]
        xn = 1.* (valx - arr_xs[i-1]) / dx
        dy = arr_ys[j] - arr_ys[j-1]
        yn = 1.* (valy - arr_ys[j-1]) / dy
        dz = arr_zs[k] - arr_zs[k-1]
        zn = 1.* (valz - arr_zs[k-1]) / dz
        dw = arr_ws[l] - arr_ws[l-1]
        wn = 1.* (valw - arr_ws[l-1]) / dw

        return ( ( (matrix_vs[i-1,j-1,k-1,l-1]*(1-xn)*(1-yn) + matrix_vs[i-1,j,k-1,l-1]*(1-xn)*yn + matrix_vs[i,j-1,k-1,l-1]*xn*(1-yn) + matrix_vs[i,j,k-1,l-1]*xn*yn)*(1-zn) +
                   (matrix_vs[i-1,j-1,k,l-1]  *(1-xn)*(1-yn) + matrix_vs[i-1,j,k,l-1]  *(1-xn)*yn + matrix_vs[i,j-1,k,l-1]  *xn*(1-yn) + matrix_vs[i,j,k,l-1]  *xn*yn)*zn )*(1-wn) + 
                 ( (matrix_vs[i-1,j-1,k-1,l]*(1-xn)*(1-yn) + matrix_vs[i-1,j,k-1,l]*(1-xn)*yn + matrix_vs[i,j-1,k-1,l]*xn*(1-yn) + matrix_vs[i,j,k-1,l]*xn*yn)*(1-zn) +
                   (matrix_vs[i-1,j-1,k,l]  *(1-xn)*(1-yn) + matrix_vs[i-1,j,k,l]  *(1-xn)*yn + matrix_vs[i,j-1,k,l]  *xn*(1-yn) + matrix_vs[i,j,k,l]  *xn*yn)*zn )*wn )

def searchAndInterpolate5DArr(arr_xs,valx,arr_ys,valy,arr_zs, valz, arr_ws,valw, arr_vs, valv, matrix_qs):
        i = minarray( maxarray(searchsorted(arr_xs, valx), 1), len(arr_xs)-1)
        j = minarray( maxarray(searchsorted(arr_ys, valy), 1), len(arr_ys)-1)
        k = minarray( maxarray(searchsorted(arr_zs, valz), 1), len(arr_zs)-1)
        l = minarray( maxarray(searchsorted(arr_ws, valw), 1), len(arr_ws)-1)
        m = minarray( maxarray(searchsorted(arr_vs, valv), 1), len(arr_vs)-1)


        dx = arr_xs[i] - arr_xs[i-1]
        xn = 1.* (valx - arr_xs[i-1]) / dx
        dy = arr_ys[j] - arr_ys[j-1]
        yn = 1.* (valy - arr_ys[j-1]) / dy
        dz = arr_zs[k] - arr_zs[k-1]
        zn = 1.* (valz - arr_zs[k-1]) / dz
        dw = arr_ws[l] - arr_ws[l-1]
        wn = 1.* (valw - arr_ws[l-1]) / dw
        dv = arr_vs[m] - arr_vs[m-1]
        vn = 1.* (valv - arr_vs[m-1]) / dv


        return ( ( ( (matrix_qs[i-1,j-1,k-1,l-1,m-1]*(1-xn)*(1-yn) + matrix_qs[i-1,j,k-1,l-1,m-1]*(1-xn)*yn + matrix_qs[i,j-1,k-1,l-1,m-1]*xn*(1-yn) + matrix_qs[i,j,k-1,l-1,m-1]*xn*yn)*(1-zn) +
                     (matrix_qs[i-1,j-1,k,l-1,m-1]  *(1-xn)*(1-yn) + matrix_qs[i-1,j,k,l-1,m-1]  *(1-xn)*yn + matrix_qs[i,j-1,k,l-1,m-1]  *xn*(1-yn) + matrix_qs[i,j,k,l-1,m-1]  *xn*yn)*zn )*(1-wn) + 
                   ( (matrix_qs[i-1,j-1,k-1,l,m-1]*(1-xn)*(1-yn) + matrix_qs[i-1,j,k-1,l,m-1]*(1-xn)*yn + matrix_qs[i,j-1,k-1,l,m-1]*xn*(1-yn) + matrix_qs[i,j,k-1,l,m-1]*xn*yn)*(1-zn) +
                     (matrix_qs[i-1,j-1,k,l,m-1]  *(1-xn)*(1-yn) + matrix_qs[i-1,j,k,l,m-1]  *(1-xn)*yn + matrix_qs[i,j-1,k,l,m-1]  *xn*(1-yn) + matrix_qs[i,j,k,l,m-1]  *xn*yn)*zn )*wn )*(1-vn) + 
                 ( ( (matrix_qs[i-1,j-1,k-1,l-1,m]*(1-xn)*(1-yn) + matrix_qs[i-1,j,k-1,l-1,m]*(1-xn)*yn + matrix_qs[i,j-1,k-1,l-1,m]*xn*(1-yn) + matrix_qs[i,j,k-1,l-1,m]*xn*yn)*(1-zn) +
                     (matrix_qs[i-1,j-1,k,l-1,m]  *(1-xn)*(1-yn) + matrix_qs[i-1,j,k,l-1,m]  *(1-xn)*yn + matrix_qs[i,j-1,k,l-1,m]  *xn*(1-yn) + matrix_qs[i,j,k,l-1,m]  *xn*yn)*zn )*(1-wn) + 
                   ( (matrix_qs[i-1,j-1,k-1,l,m]*(1-xn)*(1-yn) + matrix_qs[i-1,j,k-1,l,m]*(1-xn)*yn + matrix_qs[i,j-1,k-1,l,m]*xn*(1-yn) + matrix_qs[i,j,k-1,l,m]*xn*yn)*(1-zn) +
                     (matrix_qs[i-1,j-1,k,l,m]  *(1-xn)*(1-yn) + matrix_qs[i-1,j,k,l,m]  *(1-xn)*yn + matrix_qs[i,j-1,k,l,m]  *xn*(1-yn) + matrix_qs[i,j,k,l,m]  *xn*yn)*zn )*wn )*vn )





def searchAndInterpolateMultiD(arrs_xs,vals,matrix_ys):	
        lenarray = lens(arrs_xs)
        inds = array([searchsortedclosest(arrs_xs[i],vals[i]) for i in rl(vals)])
        y0 = matrix_ys[tuple(inds)]
        res = float(y0)
        for i in rl(vals):
                offset = vals[i] - arrs_xs[i][inds[i]]
                tmp_inds = array(inds)
                tmp_inds[i] = min(max(inds[i] + sign(offset),0),lenarray[i]-1)
                if tmp_inds[i]!=inds[i]:
                        derivative = (matrix_ys[tuple(tmp_inds)] - y0) / (arrs_xs[i][tmp_inds[i]] - arrs_xs[i][inds[i]])
                else:
                        derivative = 0.
                res += derivative * offset
        return res

def cartesianMultiplication(lsts, init=None):
        if len(lsts)==1 and init==None: return [[l] for l in lsts[0]]
        if len(lsts)==0: return init
        prevlsts = cartesianMultiplication(lsts[:-1],init=init)
        newlsts = sumlst([[lst + [x] for x in lsts[-1]] for lst in prevlsts])
        return newlsts
def labelsize(increase,large=16,small=10):
        for param in 'font.size', 'xtick.labelsize','ytick.labelsize','legend.fontsize':
                rcParams[param]=(small,large)[increase]
        rcParams['axes.labelsize'] = (small,large)[increase] + 4
slantlineprops = slantlinepropsgray = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'0.5'}
slantlinepropsblue  = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'b'}
slantlinepropsgreen  = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'g'}
slantlinepropsmagenta  = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'m'}
slantlinepropsred   = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'r'}
slantlinepropsblack = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'k'}
slantlinepropswhite = {'width':0.3,'headwidth':1.5,'headlength':2,'color':'w'}
def piecewisemax(arr, val):
        return array([max(a,val) for a in arr])
def mergearrs(arrlist):
        c = zeros(len(arrlist)*len(arrlist[0]))
        for i in rl(arrlist[0]):
                for j in rl(arrlist):
                        c[len(arrlist)*i + j] = arrlist[j][i]
        return c
class dumbClass():
        def __getattr__(self,k):
                return None
def filelines(fn):
        f = file(fn)
        ls = f.readlines()
        f.close()
        return ls

def common(arr):
        return sorted(mifkad(arr).items(),key=lambda x: x[1])[-1][0]
class LogOnLinearLocator(matplotlib.ticker.FixedLocator):
        def __init__(self,logrng=50.):
                bases = arange(-logrng,logrng)
                subs = arange(1.,10.)
                self.locs = sumlst([[base + log(s) for s in subs] for base in bases])
                matplotlib.ticker.FixedLocator.__init__(self, self.locs)

def updateTerminalTitle(title,pyprefix=True):
        sys.stdout.write("\x1b]2;%s%s\x07"%(("","python - ")[pyprefix],title))
#def luminFactor(z):
        #return 4.*pi*( calcDists(z)[-2]*3e24 )**2
#luminFactor_zs = arange(0,1,1e-4)
#luminFactors = array([luminFactor(z) for z in luminFactor_zs])
def luminFactorArr(zs):
        return searchAndInterpolateArr(luminFactor_zs, zs, luminFactors)
def range_brace(xmin, xmax, xstep,ymid, height, beta=100.):
        def half_brace(x, beta):		
                x0, x1 = x[0], x[-1]
                b2 = beta / (x1-x0)
                y = 1/(1.+e**(-b2*(x-x0))) - 1/(1.+exp(-b2*(x1-x)))		          
                return y
        x0 = arange(xmin, (xmax+xmin)/2., xstep)
        y0 = half_brace(x0, beta)* height + ymid	
        y = concat((y0, y0[::-1]))
        x = concat((x0,arange((xmax+xmin)/2., xmax,xstep)))
        return x, y
def canonicalellipse(a,b,ndots=1000):
        xs = linspace(-a,a,ndots)
        ys1 = b*(1-(xs/a)**2)**0.5
        ys2 = -b*(1-(xs/a)**2)**0.5
        return concat([xs,xs[::-1]]), concat([ys1,ys2[::-1]])
def ellipse(a,b,phi,ndots=1000):
        ts = linspace(0.,2*pi,ndots)
        xs = a * cos(ts) * cos(phi) - b * sin(ts) * sin(phi)
        ys = a * cos(ts) * sin(phi) + b * sin(ts) * cos(phi)
        return xs,ys
def sortinds(arr):
        return sorted(rl(arr),key=lambda i: arr[i])
class Both:
        def __init__(self, a, b):
                self.a = a
                self.b = b
        def __getattr__(self,name):
                return concat([getattr(self.a,name)(), getattr(self.b,name)()])
        def __getitem__(self, k):
                return concat([self.a.__getitem__(k), self.b.__getitem__(k)])
        def __call__(self,method,*args, **kwargs):
                return concat([getattr(self.a,method)(*args, **kwargs), getattr(self.b,method)(*args, **kwargs)])
        def sources(self):
                return [0]*len(self.a) + [1]*len(self.b)



presentation=0
figureDir=''
figureDir2=''
figureDirPresentation=''
def mysavefig(nm,save=True,figureExt='.pdf',figureExt2='.png',dir2save=None,**kwargs):
        if save:
                gcf().canvas.draw()
                if dir2save!=None: savefig(dir2save + nm + figureExt2,**kwargs)
                elif not presentation: 			
                        savefig(figureDir  + nm + figureExt,**kwargs)
                        savefig(figureDir2 + nm + figureExt2,**kwargs)
                else:
                        savefig(figureDirPresentation + nm + figureExt,**kwargs)

def mylegend(frame=False,removeErrorBars=False,color=None,**kwargs):
        l = legend(**kwargs)
        if not frame: l.draw_frame(False)
        if removeErrorBars:
                handles, labels = gca().get_legend_handles_labels()   
                handles = [h[0] for h in handles]
                legend(handles, labels, **kwargs)
        if color!=None:
                for text in l.get_texts():
                        setp(text, color = color)

#dustFitsNorth = pyfits.open('data/SFD_dust_4096_ngp.fits')
#dustFitsSouth = pyfits.open('data/SFD_dust_4096_sgp.fits')
#dustMaps = (dustFitsNorth['PRIMARY'].data,  dustFitsSouth['PRIMARY'].data)
#def MW_EBV(ra,dec):
        #l , b = celgal.celgal().gal((ra,dec))
        #n = [-1,1][b>0]
        #fx = lambda l, b:  n*2048*sqrt(1-n*sin(b/180.*pi))*cos(l/180.*pi)+2047.5
        #fy = lambda l, b: -n*2048*sqrt(1-n*sin(b/180.*pi))*sin(l/180.*pi)+2047.5
        #return dustMaps[b<0][int(fy(l,b)),int(fx(l,b))]


def FWHM(xs,ys):
        halfmax = max(ys)*0.5
        for i in rl(xs):
                if ys[i]>halfmax:
                        s = i
                        break
        for i in range(len(xs)-1,-1,-1):
                if ys[i]>halfmax:
                        e = i
                        break
        return xs[e] - xs[s]

def getlinemax(l):
        return l.get_xdata()[l.get_ydata().argmax()]
def figBPT(rows=1,cols=3):
        fig = rtfig(figsize=(1+cols*2,1.4*(rows+1)))
        subplots_adjust(wspace=0,left=(0.1,0.12,0.1)[cols-1],bottom=0.13,right=0.99,top=0.99,hspace=1e-4)
        axlist = [subplot(rows,cols,i+1) for i in range(cols*rows)]
        rcParams['font.size']=7
        return fig,axlist
def plotBPTbg(axlist,plotLines=True,zoom=False,plotOnlyLiner=False,plotWa09=False,axlistinds=[0,1,2],plotHo97=False,Ke06ls='--'):
        for i in axlistinds:
                axslimit = (BGBPT.xr_axes,BGBPT.xr_axes_zoom)[zoom][i]
                axes(axlist[axlistinds.index(i)])
                gca().xaxis.set_major_locator( matplotlib.ticker.MultipleLocator((0.5,0.3)[zoom]))
                gca().xaxis.set_minor_locator( matplotlib.ticker.MultipleLocator(0.1))
                gca().yaxis.set_major_locator( matplotlib.ticker.MultipleLocator((0.5,0.3)[zoom]))
                gca().yaxis.set_minor_locator( matplotlib.ticker.MultipleLocator(0.1))			
                if plotLines:
                        xs = arange(axslimit[0],axslimit[1],0.01)
                        ys = BGBPT.Ke01[i](xs)
                        mx = (ys<axslimit[2]).tolist().index(True) + 1
                        if not plotOnlyLiner: 
                                plot(xs[:mx], ys[:mx],'k',lw=0.5)
                                if i==0:
                                        ys2=BGBPT.Ka03[i](xs)
                                        mx2= (ys2<axslimit[2]).tolist().index(True) + 1
                                        plot(xs[:mx2],ys2[:mx2],'k--',lw=2)
                        if i!=0:		
                                xs2 = arange([0,-0.3,-1.1][i],[0,0.3,-0.2][i],.01)
                                ys2=BGBPT.Ke06[i](xs2)
                                plot(xs2,ys2,'k'+Ke06ls,lw=1)
                                if plotWa09:
                                        xs3 = BGBPT.Wa09[i].plotxrng
                                        ys3 = BGBPT.Wa09[i](xs3)
                                        plot(xs3,ys3,'k:',lw=2)
                        if i==2:
                                if plotHo97:
                                        xs3 = BGBPT.Ho97[i].plotxrng
                                        ys3 = BGBPT.Ho97[i].plotyrng
                                        plot(xs3,ys3,c='k',ls='-.',lw=1)

                        if False and i in (0,1):
                                ys3 = BGBPT.SL12[i](xs)
                                plot(xs,ys3,'k:',lw=2)
                axis(axslimit)
                xlabel(r'$\log\ {\rm [%s] / H}\alpha$'%(('N II','S II', 'O I')[i]))
                if axlistinds.index(i)==0: ylabel(r'$\log\ {\rm [O III] / H}\beta$')
                else: gca().yaxis.set_major_formatter(matplotlib.ticker.NullFormatter())


def figBPT_overKewley(axlistinds=[0,1,2]):
        fig,axlist = figBPT(cols=len(axlistinds))
        plotBPTbg(axlist,True,plotOnlyLiner=True,plotHo97=True,axlistinds=axlistinds)
        Kewley06.plotOnAx(axlist,applyExtent=False,axlistinds=axlistinds)
        return axlist


Verdicts =  Enum(['Good', 'UpperLimit', 'LowerLimit', 'NoLimit', 'LargeError', 'NotObserved'])
Slopes = Enum(['Positive','Negative','Vertical'])
PointVsLine = Enum(['Above', 'Below', 'Ambiguous'])

class FuncWrapper:
        def __init__(self,func,args=[],kwargs={}):
                self.func = func
                self.args = args
                self.kwargs = kwargs
        def __call__(self,p0):
                return self.func(p0,*self.args,**self.kwargs)
def GaussianProfile(lambdas,lambda0,FWHM,luminosity,dLoglambda):
        sigma = FWHM/2.35
        vs = (lambdas / lambda0 - 1.) * ld.c_kms
        dv = (10.**dLoglambda - 1.) * ld.c_kms
        return scipy.stats.distributions.norm(loc=0.,scale=sigma).pdf(vs) * dv * luminosity
class myDict(dict):
        def valuesSortedByKey(self):
                return [x[1] for x in sorted(self.items())]
class roundedDict(dict):
        def __init__(self,dic,nDigits=3):
                self.nDigits = nDigits
                for k in dic.keys():
                        self.__setitem__(k,dic[k])		
        def roundkey(self,k):
                if iterable(k):
                        return tuple([iround(kk,10**-self.nDigits) for kk in k])
                else:
                        return iround(k,10**-self.nDigits)
        def __setitem__(self,k,val):
                dict.__setitem__(self,self.roundkey(k),val)
        def __getitem__(self,k):
                return dict.__getitem__(self,self.roundkey(k))

def signedFloatStr(x,fmt):
        if x<0: return fmt%x
        if x>=0: return '+%s'%(fmt%x)

def logErrorbar(xs,ys,logyerrs,logxerrs=0.,*args,**kwargs):
        linYerrs = array([ys*(1 - 10**-logyerrs), ys*(10**logyerrs - 1)])
        linXerrs = array([xs*(1 - 10**-logxerrs), xs*(10**logxerrs - 1)])
        return errorbar(xs,ys,linYerrs, linXerrs,*args,**kwargs)

def minmaxErrorbar(xs,ys,rangeys,rangexs=None,*args,**kwargs):	
        minys,maxys = rangeys
        if rangexs!=None: minxs,maxxs = rangexs
        else: minxs,maxxs = xs,xs
        yerrs = ys - minys, maxys - ys
        xerrs = xs - minxs, maxxs - xs
        return errorbar(xs,ys,yerrs, xerrs,*args,**kwargs)



def xi2U(alpha,e=1000.):
        if alpha!=-1: meanhnu = alpha/(alpha+1.) * (e**(alpha+1) - 1.) / (e**alpha-1.)
        else:         meanhnu = (ln(e) - 1. ) / (1. - 1./e)
        return 4*pi*ld.c*ld.RydbergEV*ld.eV * meanhnu
def EW_Linear_COG(f,N,wl):
        #N in cm^-2, W in A
        W = N*f*wl**2 / 1.13e20
        return W
def legendWithoutErrorbars(*args,**kwargs):
        legend(*args,**kwargs)
        # get handles
        handles, labels = gca().get_legend_handles_labels()
        # remove the errorbars
        handles = [h[0] for h in handles]
        # use them in the legend
        gca().legend(handles, labels,*args, **kwargs)
def choiceWithReplacement(lst, nChoices=None,use_np=True):  #this func exists in new versions of numpy.random
        if nChoices==None: nChoices=len(lst)
        inds = numpy.random.randint(0,len(lst),nChoices)
        if use_np:
                return array(lst).take(inds).tolist()
        else:
                return [lst[i] for i in inds]
def geometricMean(arr):
        return 10**mean(log(arr))
def shrink(data, rows, cols):
        return data.reshape(rows, data.shape[0]/rows, cols, data.shape[1]/cols).sum(axis=1).sum(axis=2)
def plotScale(x,y,size,s,textargs,relativeHeight=0.2,offsettext=0, *args,**kwargs):
        plot([x-size/2.,x+size/2.],[y,y],*args,**kwargs)
        plot([x-size/2.,x-size/2.],[y-size*relativeHeight/2,y+size*relativeHeight/2],*args,**kwargs)
        plot([x+size/2.,x+size/2.],[y-size*relativeHeight/2,y+size*relativeHeight/2],*args,**kwargs)
        text(x,y+size*relativeHeight/2+offsettext,s,ha='center',**textargs)
def logNone(x):
        if x==None: return None
        return log(x)
def n_over_k(n,k):
        #if k==0 or k==n: return 1 #for efficiency
        return scipy.misc.factorial(n) / (scipy.misc.factorial(k) * scipy.misc.factorial(n-k))
def invertDic(dic):
        return dict((v, k) for k, v in dic.iteritems())
def ismonotonic(arr,maxDiffForEqual=1e-10):
        return ( (-1 not in sign(arr[1:]-arr[:-1]+maxDiffForEqual)) or
                 (1 not in sign(arr[1:]-arr[:-1]-maxDiffForEqual)) )

def VoigtProfile(v, b,nu0=3e18/1215.7,gamma_Draine=7616e8/1215.7):
        """
        Return the Voigt line shape at dnu=nu-nu0 with Lorentzian component HWHM gamma and Gaussian component b

        """
        dnu  = v/ld.c_kms * nu0
        b_nu = b/ld.c_kms * nu0
        gamma = gamma_Draine / (4*pi)

        integrand_for_ingtegrating_by_nu = real(wofz((dnu + 1j*gamma)/b_nu)) / (pi**0.5*b_nu)
        return integrand_for_ingtegrating_by_nu * nu0 / ld.c_kms  # for integration dv in km/s



#def ringSurfaceBrightness(L,Rmin,Rmax,z):
        ## Rmin, Rmax should be in kpc
        #area_in_arcsec = pi * (Rmax**2 - Rmin**2) / calcDists(z)[-3]**2	
        #return L / area_in_arcsec / luminFactor(z)

bT_func = lambda A,T,b_res=0.: (0.129**2 * (T/A) + b_res**2.)**0.5
def xor(a,b):
        return (a and not b) or (not a and b)
def log2linearErrors(vs,logerrs):
        return vs * (1.-10.**-logerrs), vs * (10.**logerrs - 1.)
def Planck(nu,T):
        return 2 * ld.h * nu**3 / ld.c**2 * (e**(ld.h*nu/(ld.kB*T)) - 1)**-1
def dv(z_glx, z_abs):
        ratio = (1+z_abs)**2 / (1+z_glx)**2
        return ld.c_kms * (ratio - 1) / (ratio + 1)
def z_abs(z_glx, dv):
        return (1+z_glx) * (1+dv/ld.c_kms)**0.5 / (1-dv / ld.c_kms)**0.5 - 1
def mybarplot(x,y,dx=None,*args,**kwargs):
        if dx!=None:
                newx = array([x-dx/2.,x+dx/2.]).transpose().flatten()
        else:
                dx = array((x[1:]-x[:-1])/2.).transpose().flatten()
                dx = array([dx,-dx]).transpose().flatten()
                dx = pad(dx,(1,1),mode='edge')
                dx[0] = -dx[0]
                dx[-1] = -dx[-1]
                newx = array([x,x]).transpose().flatten()
                newx = newx+dx	
        newy = array([y,y]).transpose().flatten()
        plot(newx,newy,*args,**kwargs)
def myconcat(arr):
        if len(arr)==0: return arr
        return concat(arr)

def dPhi(dv,u,b):
        return 0.5 * ( scipy.special.erf((u+dv/2.)/b) - scipy.special.erf((u-dv/2.)/b) )
def cubicSpline(r,h,d=3): #Monaghan+92
        coeff = (2/3.,10/(7*pi),1/pi)[d] / h**d
        q = r/h
        return coeff * ( ( (0<=q) * (q<=1) * (1-1.5*q**2+0.75*q**3) ) *
                         ( (1<q)  * (q<=2) * 0.25*(2-q)**3 ) )





def Delta_c(z): #Bryan & Norman 98
        x = cosmo.OmegaMz(z) - 1
        return 18*pi**2 + 82*x - 39*x**2

class NFW:	
        mu=0.6
        X=0.75
        def __init__(self,Mvir,z,cvir,_fdr = 100.):
                self._fdr = _fdr
                self.Mvir = Mvir
                self.z = z
                self.cvir = cvir
                self.dr = self.r_scale()/self._fdr
                rs = YTArray(arange(self.dr,self.rvir(),self.dr),'kpc')
                self.rho_scale = (self.Mvir / (4*np.pi * rs**2 * self.dr * 
                                               self.rho2rho_scale(rs) ).sum() ).to('g/cm**3') 
        def Delta_c(self): #Bryan & Norman 98
                x = cosmo.OmegaMz(self.z) - 1
                return 18*pi**2 + 82*x - 39*x**2
        def rvir(self):
                return ((self.Mvir / (4/3.*pi*self.Delta_c()*cosmo.rho_crit(self.z)))**(1/3.)).to('kpc')
        def r_ta(self): return 2*self.rvir()
        def r_scale(self):
                return self.rvir() / self.cvir
        def rho2rho_scale(self,r): 
                return 4. / ( (r/self.r_scale()) * (1+r/self.r_scale())**2 ) #eq. 1 in Dutton&Maccio14
        def rho(self,r):
                return self.rho_scale * self.rho2rho_scale(r)
        def enclosedMass(self,rs):
                return (16*np.pi*self.rho_scale * self.r_scale()**3 * 
                        (ln(1+rs/self.r_scale()) - (self.r_scale()/rs + 1.)**-1.)).to('Msun')
                #rs = YTArray(arange(self.dr,r,self.dr),'kpc')
                #dMs = 4*np.pi*rs**2*self.rho(rs)*self.dr
                #if not cum:
                        #return dMs.sum().to('Msun')
                #else:
                        #return rs, dMs.cumsum().to('Msun')
        def v_vir(self):
                return ((un.G*self.Mvir / self.rvir())**0.5).to('km/s')
        def v_ff(self,rs,dr=None):
                if dr==None: dr = self.dr
                Ms = self.enclosedMass(rs)
                vs = (2*(un.G*Ms / rs**2 * dr)[::-1].cumsum()[::-1])**0.5
                return vs.to('km/s')
        def v_ff_HubbleExpansion(self,rmax=None):
                if rmax == None: rmax = self.r_ta()
                rs, Ms = self.enclosedMass(rmax,True)
                gs = un.G * Ms / rs**2
                vs =  YTArray(zeros(len(rs)),'km/s')
                vs[-1]=10.*un.km/un.s
                for i in u.Progress(range(len(rs)-2,-1,-1)):
                        dv = (vs[i+1]**-1 * gs[i+1]  - cosmo.H(self.z))* self.dr
                        vs[i] = vs[i+1] + dv
                return rs, vs.to('km/s')

        def V_c(self):
                return ((un.G*self.Mvir/self.rvir())**0.5).to('km/s')
        def v_circ(self,rs):
                Ms = self.enclosedMass(rs)
                return ((un.G*Ms / rs)**0.5).to('km/s')
        def T_vir(self):
                return (self.mu * un.mp * self.V_c()**2 / (2*un.kb)).to('K')
        def mean_enclosed_rho2rhocrit(self,rs):
                Ms = self.enclosedMass(rs)
                return Ms / (4/3.*pi*rs**3) / cosmo.rho_crit(self.z)
        def r200(self,delta=200.):
                rs = YTArray(arange(self.dr,2*self.rvir(),self.dr),'kpc')
                mean_rho2rhocrit = self.mean_enclosed_rho2rhocrit(rs)
                return rs[searchsorted(-mean_rho2rhocrit,-delta)]
        def r200m(self,delta=200.):
                rs = YTArray(arange(self.dr,2*self.rvir(),self.dr),'kpc')
                mean_rho2rhocrit = self.mean_enclosed_rho2rhocrit(rs)
                return rs[searchsorted(-mean_rho2rhocrit,-delta*cosmo.OmegaMz(self.z))]		
        def M200(self,delta=200.):
                return self.enclosedMass(self.r200(delta))
        def M200m(self,delta=200.):
                return self.enclosedMass(self.r200m(delta))
        def r_ta(self,rlaunch,vout2vesc):
                rs = arange(rlaunch,10*self.rvir(),self.dr)*un.kpc
                return rs[u.searchsorted(-self.v_ff(rs), -self.v_ff(rs)[0]*(1-vout2vesc**2)**0.5)]
        def v_bal(self,rlaunch,vout2vesc):
                rs = arange(rlaunch,10*self.rvir(),self.dr)*un.kpc
                v_esc = self.v_ff(rs)
                return rs, ((vout2vesc**2-1)*v_esc[0]**2 + v_esc**2)**0.5
def NFWFromM200c(M200c,z,cvir):
        enclosedMassFunc = lambda r,rho_s, r_s: (16*np.pi*rho_s * r_s**3 * 
                                                 (ln(1+r/r_s) - (r_s/r + 1.)**-1.))
        dr = un.kpc
        _Mvirs = 10.**arange(11.,13.5,.005)*un.Msun
        _rs = YTArray(arange(dr,1000.*un.kpc,dr),'kpc')
        Mvirs, rs = meshgrid(_Mvirs,_rs)
        r_virs = ((Mvirs / (4/3.*pi*Delta_c(z)*cosmo.rho_crit(z)))**(1/3.)).to('kpc') 
        r_scales = r_virs / cvir
        rho2rho_scales = 4. / ( (rs/r_scales) * (1+rs/r_scales)**2 )

        rho_scales = (Mvirs / enclosedMassFunc(r_virs,1.,r_scales)).to('g/cm**3') 	
        enclosedMasses = enclosedMassFunc(rs,rho_scales,r_scales).to('Msun')
        mean_enclosed_rho2rhocrit = enclosedMasses / (4/3.*pi*rs**3) / cosmo.rho_crit(z)
        r200cs = zeros(len(_Mvirs))
        for i in rl(_Mvirs):
                r200cs[i] =  _rs[searchsorted(-mean_enclosed_rho2rhocrit[:,i],-200.)]
        r200cs = r200cs * un.kpc
        M200cs = (16*np.pi*rho_scales[0,:] * r_scales[0,:]**3 * 
                  (ln(1+r200cs/r_scales[0,:]) - (r_scales[0,:]/r200cs + 1.)**-1.)).to('Msun')
        Mvir = 10**searchAndInterpolate(log(M200cs), log(M200c),log(_Mvirs))*un.Msun
        return NFW(Mvir,z,cvir)


def fitpolynomial(pars,xs,ys,isULs,n=3):
        vals = reduce(lambda x,y: x+y,[pars[i]*xs**(i-n) for i in rl(pars)])
        deltas = ys - vals
        deltas = deltas.take( ((1-isULs) + isULs * (deltas < 0)).nonzero()[0] )
        return (deltas**2).sum()

def fitBrokenPowerLaw(pars,xs,ys,isULs):
        vals = (pars[0]*xs**pars[1])*(xs<pars[4]) + (pars[2]*xs**pars[3])*(xs>pars[4])
        deltas = ys - vals
        deltas = deltas.take( ((1-isULs) + isULs * (deltas < 0)).nonzero()[0] )
        return (deltas**2).sum()
def plotWithArrows(xs,ys,incidence,direction,l=1.,firstArrowInd=1,
                   arrowprops=slantlinepropsblack,plotLine=True,**kwargs):
        if plotLine: plot(xs,ys,**kwargs)
        for i in range(firstArrowInd,len(xs),incidence):
                if direction=='left': datatup = (xs[i]-l,ys[i])
                if direction=='right': datatup = (xs[i]+l,ys[i])
                if direction=='down': datatup = (xs[i],ys[i]-l)
                if direction=='up': datatup = (xs[i],ys[i]+l)
                annotate('',datatup,(xs[i],ys[i]),arrowprops=arrowprops)
def weighted_avg_and_std(values, weights):
        """
        Return the weighted average and standard deviation.

        values, weights -- Numpy ndarrays with the same shape.
        """
        average = np.average(values, weights=weights)
        variance = np.average((values-average)**2, weights=weights)  # Fast and numerically precise
        return (average, (variance)**0.5)		
