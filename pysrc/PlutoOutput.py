from pyPLUTO import *
import my_utils as u
from my_utils import array, rl, unique, zeros, lens, iround, log, pi, arange, e
import string
import pylab as pl
import Table
import ListsAndDics as ld

import astropy.units as un
import astropy.constants as cons

import sys
rpc_hydro_dir = '/home/jonathan/Dropbox/other_repositories/rpc_hydro/'
sys.path.append(rpc_hydro_dir+'pysrc_new/' )
from parameters import *

projectDir = './'
parameters   = {'r_cloud_pc': 1.0, 'rho_in_cgs':1e-20, 'rho_ex_cgs': 1e-30, 'T_in_cgs':100.0} #file 'Belt/run-folder/pluto.ini'


class Property():
    def __init__(self,snapshot,symbol,description,unitsStr,varNameCodeUnits=None,codeUnitsToCGS=None,otherProperty=None,function=None,dimension=None):
        self.snapshot = snapshot
        # hide original variable
        self.symbol = symbol
        self.description = description
        self.unitsStr = unitsStr
        if function == None:       
            if hasattr(self.snapshot, varNameCodeUnits): #verify this property was indeed read from outputs
                newVarName = '_' + varNameCodeUnits
                setattr(self.snapshot, newVarName, getattr(self.snapshot,varNameCodeUnits) )
                delattr(self.snapshot, varNameCodeUnits)
                self.isFunction = False
                self.varNameCodeUnits = newVarName
                self.codeUnitsToCGS = codeUnitsToCGS
                self.read = True
            else:
                self.read = False
        else:
            self.read = True
            self.isFunction = True
            self.functionOfOtherProperty = function 
            self.otherProperty = otherProperty
        if dimension!=None: self.dimension = dimension
        elif varNameCodeUnits!=None and varNameCodeUnits[0]=='x': self.dimension = int(varNameCodeUnits[1])
        else: self.dimension = range(1,(2,3)[len(self.snapshot.dx3)>1]+1)
    def nDims(self):
        return len(self.shape())
    def codeUnits(self):
        if not self.read: raise Exception, str(self) + 'not read'        
        assert(not self.isFunction)
        return getattr(self.snapshot, self.varNameCodeUnits)
    def cgs(self,sliceObj=None):
        if not self.read: raise Exception, str(self) + 'not read'
        if self.isFunction:
            if type(self.otherProperty)==type(()):
                val = self.functionOfOtherProperty(*self.otherProperty)
            else:
                val = self.functionOfOtherProperty(self.otherProperty)
        else:
            val = self.codeUnits() * self.codeUnitsToCGS
        return array(val)
    def tiled_cgs(self,tiledByProperty,tile_d=False):
        if not tile_d: prop = self.cgs()
        else: prop = self.d()
        if tiledByProperty.nDims()==2:
            if self.dimension==1:
                return np.tile(prop,tiledByProperty.shape()[1]).reshape(tiledByProperty.shape(),order='F')
            if self.dimension==2:
                return np.tile(prop,tiledByProperty.shape()[0]).reshape(tiledByProperty.shape(),order='C')
        
        
    def shape(self):
        return self.cgs().shape
    def d(self,dimension=None):
        if self.nDims()==1:
            padded_array = u.pad(self.cgs(),[(1,1)],mode='reflect',reflect_type='odd')
            return (padded_array[2:] - padded_array[:-2])/2        
        if self.nDims()==2:
            if dimension==1:
                padded_array = u.pad(self.cgs(),[(1,1), (0,0)],mode='reflect',reflect_type='odd')
                return (padded_array[2:,:] - padded_array[:-2,:])/2
            if dimension==2:
                padded_array = u.pad(self.cgs(),[(0,0),(1,1)],mode='reflect',reflect_type='odd')
                return (padded_array[:,2:] - padded_array[:,:-2])/2
        if self.nDims()==3:
            if dimension==1:
                padded_array = u.pad(self.cgs(),[(1,1), (0,0),(0,0)],mode='reflect',reflect_type='odd')
                return (padded_array[2:,:,:] - padded_array[:-2,:,:])/2
            if dimension==2:
                padded_array = u.pad(self.cgs(),[(0,0),(1,1),(0,0)],mode='reflect',reflect_type='odd')
                return (padded_array[:,2:,:] - padded_array[:,:-2,:])/2
            if dimension==3:
                padded_array = u.pad(self.cgs(),[(0,0),(0,0),(1,1)],mode='reflect',reflect_type='odd')
                return (padded_array[:,:,2:] - padded_array[:,:,:-2])/2        
            
    def __str__(self):
        if self.unitsStr!='':
            return r'$%s~[%s]$'%(self.symbol,self.unitsStr)
        else:
            return r'$%s$'%(self.symbol)
    def __repr__(self):
        return self.__str__()
    def formatStr(self,x):
        return '%s'%u.myScientificFormat(x,halfs=True,afterDotDigits=0)
    def fullStr(self,withvar=True):
        if withvar:
            return r'$%s=$ %s $\,%s$'%(self.symbol,self.formatStr(self.cgs()),self.unitsStr)
        else:
            return r'%s$\,%s$'%(self.formatStr(self.cgs()),self.unitsStr)
#class TableProperty(Property):
    #def cgs(self,slicesTuple=None):
        #if type(self.otherProperty)==type(()):
            #return self.functionOfOtherProperty(*tuple([x.cgs()[slicesTuple] for x in self.otherProperty]))
        #else:
            #return self.functionOfOtherProperty(self.otherProperty.cgs()[slicesTuple])

#class LambdaProperty(TableProperty):
    #def cgs(self,slicesTuple=None):
        #normedLambda = self.functionOfOtherProperty(*tuple([x.cgs()[slicesTuple] for x in self.otherProperty[1:]]))
        #return normedLambda * self.otherProperty[0].cgs()[slicesTuple]**2
    
    
class Snapshot(pload):
    def varsToRead(self):
        if self._varsToRead==None: return self.vars
        else: return self._varsToRead
    def __init__(self,run,*args,**kwargs):  
        self._varsToRead = run.varsToRead
        pload.__init__(self,*args,**kwargs)
        self.rho   = Property(self,r'\rho',       'mass volume density',  r'{\rm gr}~{\rm cm}^{-3}',varNameCodeUnits='rho',codeUnitsToCGS= run.rho0)
        self.nH    = Property(self,r'n_{\rm H}','Hydrogen volume density',r'{\rm cm}^{-3}',otherProperty=self.rho,
                                       function=lambda rho: rho.cgs() * X / cons.m_p.to('g').value)
        
        
        self.P     = Property(self,r'P_{\rm gas}','gas thermal pressure', r'{\rm dyn~cm}^{-2}',       varNameCodeUnits='prs',codeUnitsToCGS= run.rho0 * run.v0**2)
        self.v_r     = Property(self,r'v_{\rm r}','radial velocity', r'{\rm km} {\rm s}^{-1}',        varNameCodeUnits='vx1',codeUnitsToCGS= run.v0/1e5)
        self.v_theta = Property(self,r'v_{\theta}','perpendicular velocity', r'{\rm km} {\rm s}^{-1}',varNameCodeUnits='vx2',codeUnitsToCGS= run.v0/1e5)
        self.Fion  = Property(self,r'F_{\rm ion}','ionizing flux', r'{\rm erg} {\rm s}^{-1} {\rm cm}^{-2}',            varNameCodeUnits='FluxIon',codeUnitsToCGS= 1e13)
        self.Fopt  = Property(self,r'F_{\rm ion}','optical flux',  r'{\rm erg} {\rm s}^{-1} {\rm cm}^{-2}',            varNameCodeUnits='FluxOpt',codeUnitsToCGS= 1e13)
        self.t     = Property(self,r't',          'time',                 r'{\rm yr}',              varNameCodeUnits='SimTime', codeUnitsToCGS=1.,dimension=-1)
        self.r     = Property(self,r'r',          'distance',             r'{\rm pc}',              varNameCodeUnits='x1', codeUnitsToCGS=1.)
        self.theta = Property(self,r'\theta',     'polar coordinate',     r'',                      varNameCodeUnits='x2', codeUnitsToCGS=1.)
        self.fHII  = Property(self,r'f_{\rm HII}',r'HII fraction'        ,r'',                      varNameCodeUnits='ionx',codeUnitsToCGS=1.) 
        self.fHI   = Property(self,r'f_{\rm HI}', r'HI fraction'         ,r'',otherProperty=self.fHII,
                              function = lambda fHII:1-fHII.cgs())
        self.mass_flux = Property(self,r'rho v_r', r'radial mass flux'   ,r'g {\rm cm}^{-2} {\rm s}^{-1}',otherProperty=(self.rho,self.v_r),
                              function = lambda rho,v_r:rho.cgs() * v_r.cgs() * 1e5)
        self.mu    = Property(self,r'\mu','mean molecular weight','',otherProperty=self.fHI,
                              function = lambda fHI,X=X,Y=Y,Z=Z,AH=AH,AHe=AHe,AZ=AZ: 1./((1.-fHI.cgs())*(X+Y/2.+Z/2.) + X/AH + Y/AHe + Z/AZ))
        self.T     = Property(self,r'T','gas temperature',r'{\rm K}',otherProperty=(self.P,self.rho,self.mu),
                              function = lambda P,rho,mu: self.mu.cgs() * cons.m_p.to('g').value * P.cgs()/rho.cgs() / cons.k_B.to('erg/K').value)
        self.Toutput = Property(self,r'T','gas temperature',r'{\rm K}',varNameCodeUnits='Tgas',codeUnitsToCGS= 10.**10.5)
        
        self.A     = Property(self,r'A','cell area',r'{\rm cm}^2',otherProperty=(self.r,self.theta,self.rho),
                                  function=lambda r,theta,rho: (r.tiled_cgs(rho) * theta.tiled_cgs(rho,tile_d=True) *
                                                                    2. * pi * theta.tiled_cgs(rho) * r.tiled_cgs(rho) *
                                                                   un.pc.to('cm')**2))
        self.V     = Property(self,r'V','cell volume',r'{\rm cm}^3',otherProperty=(self.r,self.A,self.rho),
                             function=lambda r,A,rho: r.tiled_cgs(rho,tile_d=True) * un.pc.to('cm') * A.cgs())
        self.Omega = Property(self,r'\Omega','cell covering factor',r'',otherProperty=self.A,
                             function=lambda A,r=run.r0: A.cgs() / (4*pi*r**2) )
        self.m     = Property(self,r'm','cell mass',r'g',otherProperty=(self.V,self.rho),
                              function=lambda V,rho: V.cgs() * rho.cgs() )
                             
        
        self.NH     = Property(self,r'N_{\rm H}',  'H column',             r'{\rm cm}^{-2}',         varNameCodeUnits='NH', codeUnitsToCGS=1.)
        self.NHI    = Property(self,r'N_{\rm HI}', 'HI column',            r'{\rm cm}^{-2}',         varNameCodeUnits='NHI',codeUnitsToCGS=1.)
        self.tau_HI = Property(self,r'\tau_{\rm HI} (1ryd)', 'HI optical depth at 1 rydberg',  r'',otherProperty=self.NHI, function=lambda NHI,sigma=10**-17.2: NHI.cgs()*sigma)
        self.tau_dust = Property(self,r'\tau_{\rm dust} (1ryd)', 'dust optical depth at 1 rydberg',  r'',otherProperty=self.NH, function=lambda NH,sigma=2e-21: NH.cgs()*sigma)
        self.F0     = Property(self,r'F_0','Flux without absorption',r'{\rm erg}~{\rm s}^{-1}~{\rm cm}^{-2}',otherProperty=self.r,          
                              function=lambda r,L=run.LAGN: L / (4*u.pi*(r.cgs()*ld.pc)**2),dimension=self.r.dimension)
        self.Ftotal = Property(self,r'F_{\rm total}','total Flux',r'{\rm erg}~{\rm s}^{-1}~{\rm cm}^{-2}',otherProperty=(self.Fion,self.Fopt),
                              function=lambda Fion,Fopt: Fion.cgs()+Fopt.cgs())
        self.F2F0   = Property(self,r'F/F_0','flux fraction',r'',otherProperty=(self.F0,self.Ftotal),
                                   function=lambda F0,Ftotal: Ftotal.cgs()/F0.tiled_cgs(tiledByProperty=Ftotal) )
        self.Fion2Fion0 = Property(self,r'F^{\rm ion}/F^{\rm ion}_0','ionizing flux fraction',r'',otherProperty=(self.F0,self.Fion),
                                   function=lambda F0,Fion,optFrac=run.optFrac: Fion.cgs()/((1-optFrac)*F0.tiled_cgs(tiledByProperty=Ftotal)) )
        self.Prad1  = Property(self,r'P_{\rm rad}','absorbed radiation pressure',r'{\rm dyn}',otherProperty=(self.F0,self.Ftotal),
                              function=lambda F0,Ftotal: (F0.tiled_cgs(tiledByProperty=Ftotal)-Ftotal.cgs())/cons.c.to('cm/s').value)
        self.Prad12k= Property(self,r'P_{\rm rad}/k','absorbed radiation pressure (nT)', r'{\rm cm}^{-3}{\rm K}',otherProperty=self.Prad1,            
                                          function=lambda Prad1: Prad1.cgs()/cons.k_B.to('erg/K').value)
        self.Xi    = Property(self,r'\Xi','rad pressure to gas pressure', r'',otherProperty=(self.F0,self.P),
                                  function=lambda F0,P: F0.tiled_cgs(tiledByProperty=P)/array(cons.c.to('cm/s').value)/P.cgs() )
        self.U0     = Property(self,r'U_0','ionization parameter without absorption',r'',otherProperty=(self.F0,self.nH),
                              function=lambda F0,nH,ionfrac=ion_fraction,hnu=meanhnu.to('erg').value: (
                                  F0.tiled_cgs(tiledByProperty=nH) *ionfrac / hnu / 
                                  array(cons.c.to('cm/s').value)/nH.cgs()))
   
        self.P2k           = Property(self,r'P_{\rm gas}/k','gas thermal pressure (nT)', r'{\rm cm}^{-3}{\rm K}',otherProperty=self.P,            
                                      function=lambda P: P.cgs()/cons.k_B.to('erg/K').value)

        self.x             = Property(self,r'x', 'perpendicular cartesian coordinate',r'{\rm pc}',otherProperty=(self.theta,self.r),
                                      function=lambda theta,r:r.cgs().mean()*theta.cgs(),dimension=2)
        
        self.z             = Property(self,r'z', 'parallel cartesian coordinate',r'{\rm pc}',otherProperty=self.r,             
                                      function=lambda r:r.cgs()-r.cgs().mean(),dimension=1)
        
        self.propertyDict = {}
        properties = ['rho','P','t','r','theta','nH','P2k','T','x','z','V','A','Omega','Xi','Fion','mass_flux',
                      'Fopt','Ftotal','F2F0','Fion2Fion0','Prad1','Prad12k','mu','fHI','v_r','v_theta']
        properties += ['NH','NHI','F0','U0','tau_HI']
        for k in properties:
            self.propertyDict[k] = getattr(self,k)
                
        ## table based properties
        self.Lambda = Property(self,r'\Lambda',r'cooling function',r'{\rm erg}~{\rm s}^{-1}~{\rm cm}^{3}',
                                    otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.absCoolingTable.properties]),
                                    function=lambda *props: Table.Table.__call__(run.absCoolingTable,*tuple([x.cgs() for x in props])))
        
        self.coolingPerUnitVolume = Property(self,r'n_{\rm H}^2\Lambda',r'cooling per unit Volume',r'{\rm erg}~{\rm s}^{-1}~{\rm cm}^{-3}',
                                     otherProperty=(self.nH,self.Lambda),
                                     function=lambda nH,Lambda: nH.cgs()**2*Lambda.cgs())
        self.Lgas        = Property(self,r'L_{\rm gas}','cell gas luminosity',r'{\rm erg~s}^{-1}',otherProperty=(self.coolingPerUnitVolume,self.V),
                              function=lambda coolingPerUnitVolume,V: coolingPerUnitVolume.cgs()*V.cgs())
        self.Lgas2Omega  = Property(self,r'L_{\rm gas}/\Omega','cell gas luminosity per unit covering factor',r'{\rm erg~s}^{-1}',otherProperty=(self.Lgas,self.Omega),
                                  function=lambda L,Omega: L.cgs()/Omega.cgs())
        
        for k in 'coolingPerUnitVolume','Lambda','Lgas','Lgas2Omega':
            self.propertyDict[k] = getattr(self,k)
        
        self.gasIonOpacity  = Property(self,r'\sigma_{\rm gas,ion}',r'ion gas opacity',r'{\rm cm^2~per~H-atom}',
                                           otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.gasIonOpacityTable.properties]),
                                           function=lambda *props: Table.Table.__call__(run.gasIonOpacityTable,*tuple([x.cgs() for x in props]))*ld.mP/X)
        self.dustIonOpacity = Property(self,r'\sigma_{\rm dust,ion}',r'ion dust opacity',r'{\rm cm^2~per~H-atom}',
                                            otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.dustIonOpacityTable.properties]),
                                            function=lambda *props: Table.Table.__call__(run.dustIonOpacityTable,*tuple([x.cgs() for x in props]))*ld.mP/X*d2g)
        self.dustOptOpacity = Property(self,r'\sigma_{\rm dust,opt}',r'opt dust opacity',r'{\rm cm^2~per~H-atom}',
                                            otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.dustOptOpacityTable.properties]),
                                            function=lambda *props: Table.Table.__call__(run.dustOptOpacityTable,*tuple([x.cgs() for x in props]))*ld.mP/X*d2g)           
        self.sigma = Property(self,r'\sigma','spectrum averaged opacity',r'{\rm cm^2~per~H-atom}',
                              otherProperty=(self.gasIonOpacity,self.dustIonOpacity,self.dustOptOpacity),
                              function=lambda gasIon,dustIon,dustOpt: ((
                                  (1-ion_fraction)*dustOpt*dust2gasRatio
                                  + ion_fraction*(dustIon*dust2gasRatio + gasIon)) / X * cons.m_p.to('g').value))
        self.tau = Property(self,r'\tau','spectrum averaged optical depth',r'',
                            otherProperty=(self.sigma,self.nH,self.r),
                            function=lambda sigma,nH,r: (sigma*nH*r.d()).cumsum())
        self.fHI_Table      = Property(self,r'f_{\rm HI}',r'HI fraction',r'',
                                           otherProperty=tuple([self.propertyDict[propertyName] for propertyName in run.fHITable.properties]),
                                                function=lambda *props: Table.Table.__call__(run.fHITable,*tuple([x.cgs() for x in props])))           
    
        
        self.Ldust          = Property(self,r'L_{\rm dust}','cell dust luminosity',r'{\rm erg~s}^{-1}',otherProperty=(self.Fion,self.Fopt,self.A,self.dustIonOpacity,self.dustOptOpacity,self.NH),
                                        function=lambda Fion,Fopt,A,sigma_ion,sigma_opt,NH: (Fion.cgs()*sigma_ion.cgs() + Fopt.cgs()*sigma_opt.cgs()) * A.cgs()*NH.d(dimension=1))
        self.Ldust2Omega = Property(self,r'L_{\rm dust}/\Omega','cell dust per unit covering factor',r'{\rm erg~s}^{-1}',otherProperty=(self.Ldust,self.Omega),
                                        function=lambda L,Omega: L.cgs()/Omega.cgs())
        self.Liondust       = Property(self,r'L_{\rm dust}^{\rm ion}','cell dust luminosity due to ionizing photons',r'{\rm erg~s}^{-1}',otherProperty=(self.Fion,self.A,self.dustIonOpacity,self.NH),
                                           function=lambda Fion,A,sigma_ion,NH: Fion.cgs()*sigma_ion.cgs() * A.cgs()*NH.d(dimension=1))
        self.Liondust2Omega = Property(self,r'L_{\rm dust}^{\rm ion}/\Omega','cell dust per unit covering factor',r'{\rm erg~s}^{-1}',otherProperty=(self.Liondust,self.Omega),
                                        function=lambda L,Omega: L.cgs()/Omega.cgs())
        
        #self.Lattenuated     = Property(self,r'L e^{-\tau_{\rm d}}','attentuated cell luminosity',r'{\rm erg~s}^{-1}',
                                        #otherProperty=(self.Lgas,self.tau_d),
                              #function=lambda L,tau_d,norm=3.: L.cgs()*e**(-tau_d.cgs()/norm))
        
        self.particlesPerH  = Property(self,r'n/n_{\rm H}',r'particles per hydrogen',r'',otherProperty=self.mu,function=lambda mu,X=X: (1./X/mu))
        
        self.nHI            = Property(self,r'n_{\rm HI}','neutral hydrogen density',r'{\rm cm}^{-3}',otherProperty=(self.nH,self.fHI),
                                       function=lambda nH,fHI: nH.cgs()*fHI.cgs() )   
        self.nHII           = Property(self,r'n_{\rm HII}','ionized hydrogen density',r'{\rm cm}^{-3}',otherProperty=(self.nH,self.fHI),
                                           function=lambda nH,fHI: nH.cgs()*(1-fHI.cgs()) )   
        for k in ('gasIonOpacity','dustIonOpacity','dustOptOpacity','nHI','nHII','Ldust','Ldust2Omega',
                  'Liondust','Liondust2Omega','fHI_Table','sigma','tau'):   
            self.propertyDict[k] = getattr(self,k)
                    
            
    def __getitem__(self,k):
        if type(k)==type(''):
            return self.propertyDict[k]
        elif type(k)==type((None,)):
            if type(k[0])==type(lambda x: None):
                return self.funcProperty(k[0],k[1],*k[2:])
            elif len(k)==2:
                return self.derivative(k[0], k[1])
    def derivative(self,property1, property2):
        if type(property1) in (type(''),type((None,))): property1 = self[property1]
        if type(property2) in (type(''),type((None,))): property2 = self[property2]                
        return Property(self, r'{\rm d}%s / {\rm d}%s'%(property1.symbol,property2.symbol),'d(%s)/d(%s)'%(property1.description,property2.description), 
                        unitsStr = '%s / %s'%(property1.unitsStr, property2.unitsStr),
                        otherProperty=(property1,property2),
                        function = lambda property1, property2: _derivativeFunc(property1, property2))        
        
    def integrate(self, integrand, integrateOver,factor=1.,name=None,desc=None,unitsStr=None):
        
        if type(integrand)     in (type(''),type((None,))): integrand     = self[integrand]
        if type(integrateOver) in (type(''),type((None,))): integrateOver = self[integrateOver]       
        if name==None: name = r'int %s {\rm d}%s'%(integrand.symbol,integrateOver.symbol)
        if desc==None: desc = r'int (%s) d(%s)'%(integrand.description,integrateOver.description), 
        if unitsStr==None: unitsStr = '%s %s'%(property1.unitsStr, property2.unitsStr),
        return Property(self, name,desc,unitsStr,
                        otherProperty=(integrand,integrateOver),
                        function = lambda integrand, integrateOver,factor=factor: _integrateFunc(integrand, integrateOver)*factor)        
        
    def funcProperty(self,func,s,*otherProperties):
        props = [None]*len(otherProperties)
        for i in rl(otherProperties):
            if type(otherProperties[i])  in (type(''),type((None,))): props[i] = self[otherProperties[i]]
            else: props[i] = otherProperties[i]
        return Property(self, s,'','',otherProperty=tuple(props),function=func)

        


def _derivativeFunc(property1, property2):
    dnumerator = property1.d(property2.dimension)
    if property1.nDims()==2:
        if property2.dimension==1:
            ddenominator = np.tile(property2.d(property2.dimension),property1.shape()[1]).reshape(property1.shape())
        if property2.dimension==2:
            ddenominator = np.tile(property2.d(property2.dimension),property1.shape()[0]).reshape(property1.shape())
    return dnumerator / ddenominator
def _integrateFunc(integrand,integrateOver):
    assert(integrand.nDims()==2)
    assert(integrateOver.dimension==1)
    ds = np.tile(integrateOver.d(integrateOver.dimension),integrand.shape()[1]).reshape(integrand.shape())          
    return (integrand.cgs() * ds).cumsum(axis=0)

       
class Run:
    #runsDir = '/u/joseo/scratch/'
    #runsDir = '/home/jose/Dropbox/work_shared_folders/belt/'
    runsDir = '/home/jonathan/Dropbox/belt/'
    def __init__(self,name,nTimeSteps,r,v0,L0,rho0,L,optFrac,varsToRead=None):
        self.name = name
        self.dirName = self.runsDir + self.name + '/'
        self.dataDirName = self.dirName 
        self.dataDirName += 'data/'
        #self.tableFilename = self.dirName + 'output_table.txt'
        self.tableFilename = self.runsDir + 'output_table.txt'
        self.netCoolingTable = Table.Table(('U0','T','NH','NHI'),self.tableFilename,firstLine=1,calculatedValueColumn=6)
        self.absCoolingTable = Table.Table(('U0','T','NH','NHI'),self.tableFilename,firstLine=1,calculatedValueColumn=5)
        self.gasIonOpacityTable     = Table.Table(('U0','T','NH','NHI'),self.tableFilename,firstLine=1,calculatedValueColumn=2)
        self.dustIonOpacityTable    = Table.Table(('U0','T','NH','NHI'),self.tableFilename,firstLine=1,calculatedValueColumn=1)
        self.dustOptOpacityTable    = Table.Table(('U0','T','NH','NHI'),self.tableFilename,firstLine=1,calculatedValueColumn=0)
        self.fHITable               = Table.Table(('U0','T','NH','NHI'),self.tableFilename,firstLine=1,calculatedValueColumn=3)
        
        self.r0 = r*un.pc.to('cm')
        self.v0,self.L0, self.rho0 = v0, L0, rho0        
        self.LAGN = L
        self.optFrac = optFrac
        self.varsToRead = varsToRead
        
        self._data = [None for iT in range(nTimeSteps)]
    def __getitem__(self,k):
        if self._data[k]==None: 
            print 'loading snapshot #%d'%k
            self._data[k] = Snapshot(self,k,self.dataDirName) 
        return self._data.__getitem__(k)
    def delSnapshot(self,k):
        if self._data[k]!=None: 
            del self._data[k] 
    def __len__(self):
        return self._data.__len__()
    def plotter(self):
        return RunPlotter(self)
class RunPlotter:
    figsDir = 'pyFigures/'
    def __init__(self,run):
        #assert(isinstance(run,Run))
        self.run = run
    def plot2D(self,iT,var,logScale=False,minZrange=1.01,ax=None,**kwargs):
        vals = self.run[iT][var].cgs()        
        
        if not logScale:
            zmin = kwargs.get('vmin',np.min(vals))
            zmax = max(minZrange*zmin,kwargs.get('vmax',np.max(vals)))
            pcolormesh(self.run[iT].x.cgs(),self.run[iT].r.cgs(),vals,vmin=zmin,vmax=zmax)
        else:
            zmin = kwargs.get('vmin',np.min(vals.flatten().take(vals.flatten().nonzero()[0])))
            zmax = max(minZrange*zmin,kwargs.get('vmax',np.max(vals)))
            #print var, zmin, zmax
            pcolormesh(self.run[iT].x.cgs(),self.run[iT].r.cgs(),vals,
                       norm=matplotlib.colors.LogNorm(vmin=zmin, vmax=zmax))
        ylabel(self.run[iT].r)
        xlabel(self.run[iT].x)
        if kwargs.has_key('title'): tit = kwargs['title']
        else: tit = str(self.run[iT][var])
        title(tit)        
        ylim(np.min(self.run[iT].r.cgs()),np.max(self.run[iT].r.cgs()))
        xlim(np.min(self.run[iT].x.cgs()),np.max(self.run[iT].x.cgs()))
    def multiplot2D(self,var,iTs=None,save=False,sqrtnPanels=3,**kwargs):        
        figure(figsize=(10,8))
        subplots_adjust(hspace=0.3,wspace=0.3)
        for iiT,iT in enumerate(iTs):
            subplot(sqrtnPanels,sqrtnPanels,iiT+1)
            fulltit = r'%s,  %s'%(self.run[iT][var],self.run[iT].t.fullStr())
            self.plot2D(iT, var,title=fulltit,**kwargs)
        if save:
            fn = self.run.dirName + self.figsDir + var + '.png'
            pl.savefig(fn)
        
    def plot2Dmovie(self,var,iTs,vmin,vmax,save=False):        
        #pl.title(r'%s,  %s'%(run[iT][var],run[iT].t.fullStr()))
        fig1 = figure()
        ims = [(pcolormesh(self.run[iT].x.cgs(),self.run[iT].r.cgs(),self.run[iT][var].cgs(),
                         norm=matplotlib.colors.LogNorm(vmin=vmin, vmax=vmax)),) for iT in iTs]
        im_ani = matplotlib.animation.ArtistAnimation(fig1, ims, interval=10, repeat_delay=3000,blit=True)
        
        
        #ylabel(self.run[iT].r)
        #xlabel(self.run[iT].x)
        #pl.title(r'%s,  %s'%(self.run[iT][var],self.run[iT].t.fullStr()))
        ##colorbar(orientation='vertical')
        #ylim(np.min(self.run[iT].r.cgs()),np.max(self.run[iT].r.cgs()))
        #xlim(np.min(self.run[iT].x.cgs()),np.max(self.run[iT].x.cgs()))
    
        if save:
            fn = self.run.dirName + self.figsDir + var + '.mp4'
            im_ani.save(fn, metadata={'artist':'Guido'})
        return im_ani
    def multiplot2D_multiVars(self,variables,iT,save,sqrtnPanels=3,**kwargs):
        figure(figsize=(10,8))
        subplots_adjust(hspace=0.3,wspace=0.3)        
        for ivar,var in enumerate(sorted(variables.keys())):
            subplot(sqrtnPanels,sqrtnPanels,ivar+1)            
            fulltit = r'%s,  %s'%(self.run[iT][var],self.run[iT].t.fullStr())
            if type(variables)==type({}):
                self.plot2D(iT, var,title=fulltit,vmin=variables[var][0],vmax=variables[var][1],**kwargs)
            else:
                self.plot2D(iT, var,title=fulltit,**kwargs)
        if save:
            fn = self.run.dirName + self.figsDir + 't%d'%iT + '.png'
            pl.savefig(fn)
        
    def midPlane(self,iTs,var1,var1range=None,var2range=None,logScale=False,granularity=False,showlabel=True):
        for iT in iTs:
            if False:#isinstance(self.run[iT][var1], TableProperty): 
                vals1 = self.run[iT][var1].cgs((slice(None),0))
            else:
                vals1 = self.run[iT][var1].cgs()[:,0]
            plot(self.run[iT].r.cgs(), vals1,('-','.-')[granularity],label=self.run[iT].t.fullStr(withvar=False))
        xlabel(self.run[iTs[0]].r)
        ylabel(self.run[iTs[0]][var1])
        if var1range!=None: 
            ylim(*var1range)
        if logScale: 
            semilogy()
            gca().yaxis.set_major_formatter(u.arilogformatter)
        if showlabel:
            u.mylegend(loc='best')
    def multiplotMidPlane(self,var1,var2,iTs, save=False,**kwargs):
        figure(figsize=(20,12))
        subplots_adjust(hspace=0.3,wspace=0.3)
        for iiT,iT in enumerate(iTs):
            subplot(3,3,iiT+1)
            self.midPlane(iT, var1,var2,**kwargs)
            title(self.run[iT].t.fullStr())
        if save:
            fn = self.run.dirName + self.figsDir + '_midPlane.png'
            pl.savefig(fn)
    def midPlane_multiVars(self,variables,iTs, save=False,xls=(None,None),sqrtnPanels=3,**kwargs):
        figure(figsize=(10,8))
        subplots_adjust(hspace=0.3,wspace=0.3)
        for ivar,var in enumerate(u.Progress(variables)): 
            if type(variables)==type({}):
                if len(variables[var])==2: iPanel,rng = ivar, variables[var]
                if len(variables[var])==3: iPanel,rng = variables[var][0], variables[var][1:]
                subplot(sqrtnPanels,sqrtnPanels,iPanel+1)
                self.midPlane(iTs, var,var1range=rng,showlabel=(ivar==0),**kwargs)                                
                text(0.04,0.92,r'\textbf{(%c)}'%string.letters[26+iPanel],transform=gca().transAxes,fontsize=16)
            else:
                subplot(sqrtnPanels,sqrtnPanels,ivar+1)
                self.midPlane(iTs, var,**kwargs)                                
            
            xlim(*xls)
        if save:
            fn = self.run.dirName + self.figsDir + 'midPlane.png'
            pl.savefig(fn)
        
    def dMdlogP(self,iTs,dlogn=0.1):
        bins=10.**arange(-5,15,dlogn)
        u.figure()
        for iT in iTs:
            pl.hist(self.run[iT].P2k.cgs().flatten(),bins=bins,
                    weights=self.run[iT].m.cgs().flatten() / un.Msun / dlogn,
                    label=self.run[iT].t.fullStr(),histtype='step')
        xlabel(self.run[iTs[0]].P2k)
        ylabel(r'${\rm d}M / {\rm d~log} P$ [M$_\odot$]')
        semilogx()
        gca().xaxis.set_major_formatter(u.arilogformatter)
        u.mylegend(loc='best')
        u.xlim(1e5,1e15)
        pl.axvline((self.run.LAGN*un.erg/un.s/(4*pi*(self.run[1].r.cgs()[0]*un.pc)**2*cons.c*cons.kb)).to('cm**-3*K'),c='k',ls=':')
    def dLdlogn(self,iTs,dlogn=0.1,):
        bins=10.**arange(-10,10,dlogn)
        u.figure()
        for iiT,iT in enumerate(iTs):
            c = 'kbcry'[iiT]
            #pl.hist(self.run[iT].nH.cgs().flatten(),bins=bins,
                    #weights= self.run[iT].L.cgs().flatten()/ dlogn,histtype='step',ls=':')
            pl.hist(self.run[iT].nH.cgs().flatten(),bins=bins,
                    weights= self.run[iT].Lattenuated.cgs().flatten()/ dlogn,
                    label=self.run[iT].t.fullStr(),histtype='step',ls='-',color=c)
        xlabel(self.run[iTs[0]].nH)
        ylabel(r'${\rm d}L / {\rm d~log} n$ [$L_{\odot}$]')
        semilogx()
        gca().xaxis.set_major_formatter(u.arilogformatter)
        u.mylegend(loc='best')
        u.xlim(1,1e7)
        pl.axvline((0.5*self.run.LAGN*un.erg/un.s/
                    (4*pi*(self.run[1].r.cgs()[0]*un.pc)**2*cons.c*2.3*cons.k_B*1e4*un.K)).to('cm**-3'),c='k',ls=':')

    def dLdlogT(self,iTs,dlogT=0.02):
        bins=10.**arange(1,7,dlogT)
        u.figure()
        for iT in iTs:
            pl.hist(self.run[iT].T.cgs().flatten(),bins=bins,
                    weights= self.run[iT].Lattenuated.cgs().flatten()/ dlogT,
                    label=self.run[iT].t.fullStr(),histtype='step',ls='-')
            print iT, 10**((log(self.run[iT].T.cgs().flatten())*self.run[iT].Lattenuated.cgs().flatten()).sum() / self.run[iT].Lattenuated.cgs().flatten().sum())
        xlabel(self.run[iTs[0]].T)
        ylabel(r'${\rm d}L / {\rm d~log} T$ [$L_{\odot}$]')
        semilogx()
        gca().xaxis.set_major_formatter(u.arilogformatter)
        u.mylegend(loc='best')
        u.xlim(3000,3e5)
        pl.axvline(1e4,c='k',ls=':')
        
    

    def forApplications2015(self, iT,save=True,showInset=True,showLegend=True, showSkewers=True):
        figure(figsize=(rc.fig_width / (1.5,1.)[showSkewers],3))
        
        
        axes([0.1,0.15,(0.32,0.16)[showSkewers],0.75])
        self.plot2D(0, 'nH',vmin=1, vmax=1e6, logScale=True,cbar=False)
        title(r'$t=0~{\rm yr}$')
        arrow(0.1,100.57,     0, 1.86,head_width=0.04,fc='k',ec='k',lw=0.7)
        arrow(0.1,100.57+1.86,0,-1.86,head_width=0.04,fc='k',ec='k',lw=0.7)
        text(0.13,101.8,r'$\tau_{\rm UV}=25,$',fontsize=9)
        text(0.13,101.6,r'$\tau_{\rm IR}=0.25$',fontsize=9)
        gca().xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(0.3))
                
        axes([(0.47,0.29)[showSkewers],0.15,(0.45,0.21)[showSkewers],0.75])
        self.plot2D(iT, 'nH',vmin=1, vmax=1e6, logScale=True,cbar=False)
        title(r'$t=10^4~{\rm yr}$')
        
        gca().xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(0.3))
        pl.colorbar(format=u.arilogformatter,fraction=0.2)
        ylabel('')
        gca().yaxis.set_major_formatter(matplotlib.ticker.NullFormatter())
        text(0.535,0.5,r'$n_{\rm H}~[{\rm cm}^{-3}]$',rotation=90,transform=gcf().transFigure,va='center',ha='center')


        #inset
        if showInset:
            xrng = 0.37,0.40
            yrng = 101.1,101.26
            sx,ex = u.searchsorted(self.run[iT].x.cgs(), xrng)
            sr,er = u.searchsorted(self.run[iT].r.cgs(), yrng)
            gca().add_patch(matplotlib.patches.Rectangle((xrng[0],yrng[0]), (xrng[1]-xrng[0]), (yrng[1]-yrng[0]),fc='none',ec='k',lw=1))
            annotate('',(1,100.8),(xrng[1]+0.02,u.mean(yrng)), arrowprops={'width':0.3,'headwidth':1.5,'frac':0.15,'color':'.3'},fontsize=8,ha='center')
            axes([0.385,0.2,0.05,0.15])
            pcolormesh(self.run[iT].x.cgs()[sx:ex],self.run[iT].r.cgs()[sr:er],self.run[iT].nH.cgs()[sr:er,sx:ex],norm=matplotlib.colors.LogNorm(vmin=1, vmax=1e6))
            xlim(*xrng)
            ylim(*yrng)
            gca().xaxis.set_major_locator(matplotlib.ticker.NullLocator())
            gca().yaxis.set_major_locator(matplotlib.ticker.NullLocator())
        
        if showSkewers:
            axes([0.64,0.15,0.3,0.75])
            F0 = self.run[iT].F.cgs().flatten().max()
            Ps = self.run[iT].P2k.cgs()
            taus = self.run[iT].tau.cgs()
            hydrostatictaus = 10**u.arange(-1,1.2,0.1)
            hydrostaticPs = F0 / ld.c /ld.kB * (1-u.e**-hydrostatictaus)
            plot(hydrostatictaus, hydrostaticPs,c='k',lw=2,zorder=10,label=r'hydrostatic approximation')
            
            maxind = taus.shape[1]#u.searchsorted(self.run[iT].x.cgs(),1.)
            for i in range(0,maxind,3):
                plot(taus[:,i], Ps[:,i],'-',c='.8',lw=(.3,1)[i==0],label=('_nolegend_','radial skewers')[i==0])
            loglog()
            gca().xaxis.set_major_formatter(u.arilogformatter)
            gca().yaxis.set_major_formatter(u.arilogformatter)
            title(r'$t=10^4~{\rm yr}$')
            ylabel(r'thermal pressure $P_{\rm gas}/k~[{\rm cm}^{-3}{\rm K}]$')
            xlabel(r'UV optical depth $\tau_{\rm UV}$')
            if showLegend:
                u.mylegend(loc='lower left')
        
            xlim(0.1,10)
            ylim(3e6,5e9)
        
        #axes([0.55,0.1,0.34,0.3])
        #maxtau = 1; dlogU = 0.5; dlogT = 0.5
        #tableUs = 10**u.array(self.run.netCoolingTable.propertyValues[0])
        #tableTs = 10**u.array(self.run.netCoolingTable.propertyValues[1])
        
        #Us = self.run[iT].U.cgs().flatten()
        #taus = self.run[iT].tau.cgs().flatten()
        #Ts = self.run[iT].T.cgs().flatten()
        #nHsqr = self.run[iT].nH.cgs().flatten()**2
        #rim = self.run[iT].x.cgs().repeat(1024).reshape(512,1024).transpose().flatten() * 3e18 * 2*u.pi * self.run.dr**2
        #totalLambda = zeros(len(tableUs))
        #for iU in rl(tableUs):
            #goodUs = (Us > tableUs[iU]*10**(-0.5*dlogU)) * (Us < tableUs[iU]*10**(0.5*dlogU)) * (taus < maxtau)
            #for iT in rl(tableTs):
                #goodUsAndTs = goodUs * (Ts > tableTs[iT]*10**(-0.5*dlogT)) * (Ts < tableTs[iT]*10**(0.5*dlogT)) * nHsqr * rim
                #totalLambda[iU] += self.run.netCoolingTable(tableUs[iU],tableTs[iT]) * goodUsAndTs.sum()
        #u.plot(tableUs,totalLambda / totalLambda.max(),'-',c='k')
        #loglog()
        ##plot([0.05,100],[1e39,1e39*(100/0.05)**-1.],c='k',lw=2)
        #gca().xaxis.set_major_formatter(u.arilogformatter)
        #gca().yaxis.set_major_formatter(u.arilogformatter)
        #title(r'$t=10^4~{\rm yr}$')
        #text(0.005,2e-3,'emission measure distribution')
        
        #xlabel(r'$U$')
        #ylabel(r'${\rm d}L / {\rm d log}~U~[{\rm erg~s}^{-1}]$')
        #ylim(1e-3,3)
        #xlim(3e-3,100)
        
        if save:
            pl.savefig('doc/Applications/2015/ResearchProposal/Hydro.png')
            
        
        
        
    





