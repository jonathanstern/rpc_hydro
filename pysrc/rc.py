#!/usr/bin/python
# -*- coding: utf-8 -*-
from pylab import *
log=log10
rc('font', family = 'serif')
NiceGraphics = 1

if NiceGraphics:
    fig_width_pt = 504.0  # Get this from LaTeX using \showthe\columnwidth
    # 240 -- one column
    # 504 -- full page
    inches_per_pt = 1.0/72.27               # Convert pt to inch
    golden_mean = (sqrt(5)-1.0)/2.0         # Aesthetic ratio
    fig_width = fig_width_pt*inches_per_pt  # width in inches
    fig_height = fig_width*golden_mean      # height in inches
    fig_size =  [fig_width,fig_height]
    mpparams = {#'backend': 'ps',
            #'ps.usedistiller':'xpdf',
            'axes.labelsize': 10,
            'font.size': 10,
            'font.size':10,
            'legend.fontsize': 10,
            'legend.labelspacing' : 0.3,
            'legend.numpoints' : 1,
            'xtick.labelsize': 10,
            'ytick.labelsize': 10,
            'text.usetex': True,
            'figure.figsize': fig_size,
            'axes.linewidth': 0.5,
            'axes.titlesize': 9,
            'patch.linewidth':0.5,
            'figure.subplot.bottom': 0.12,
            'figure.subplot.left': 0.1,
            'figure.subplot.right': 0.98,
            'figure.subplot.top': 0.95,
            'figure.subplot.wspace': 0.07,
            'figure.subplot.hspace': 0.07}

    rcParams.update(mpparams)
    rcParams['text.latex.preamble']='\usepackage{times}'
    rcParams['text.latex.preamble'].append('\usepackage{mathptmx}')
    rcParams['text.latex.preamble'].append('\usepackage{amsmath}')
    rcParams['text.latex.preamble'].append('\usepackage{amssymb}')
    rcParams['text.latex.preamble'].append('\usepackage{nicefrac}')

presentation = 0
d = 0    
if presentation: d = 6
if presentation==2: d=12
for k in ('axes.labelsize', 'font.size', 'legend.fontsize', 'xtick.labelsize', 'ytick.labelsize','font.size'):
    mpparams[k] = mpparams[k] + d

rcParams.update(mpparams)

        
        
    
    

    

bySizeParams={ (13,9):    {'bottom':0.06,'left':0.06,'top':0.97,'right':0.985, 'wspace':0.12},
               (6.5,4.7): {'left':.12, 'bottom':.09, 'top':0.995,'right':0.985},
               (9,5):     {'left':.08, 'bottom':.12, 'top':0.995,'right':0.985, 'wspace':0.1},
               (9,9):     {'left':.08, 'bottom':.06, 'top':0.97,'right':0.985, 'wspace':0.,'hspace':1e-5},
               (9,13):    {'left':.08, 'bottom':.08, 'top':0.985,'right':0.985, 'hspace':1e-5},
               (8,6):     {'left':.09, 'bottom':.1, 'top':0.97,'right':0.91, 'wspace':0.09},
               (10,10):    {'left':.09, 'bottom':.09, 'top':0.985, 'hspace':0.34},
               (10,12):    {'left':.07, 'bottom':.06, 'top':0.985, 'right':0.985},
               }