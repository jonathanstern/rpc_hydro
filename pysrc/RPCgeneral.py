import sys, string
from my_utils import log
import my_utils as u
#import ListsAndDics as ld
from numpy import arange

Lbol2Lbha = 130.

##eq. 6  in RPCI:
def n_f_Func(Li,rpc): 
    return 7.4e4 * (Li/1e45) * (r/50.)**-2. #cm^-3

##eq. 24 in RPCI
def dOmega_dlogr(r, eta, OmegaBLR, OmegaNLR, rin, rout, rsub, logrstep=0.1):
    ##calculate coefficient from total Omega
    
    _rs = 10.**arange(log(rin),log(rsub)+logrstep/2.,logrstep)
    coeffBLR = OmegaBLR / ( (_rs/rin)**eta * logrstep ).sum()

    _rs = 10.**arange(log(rsub),log(rout)+logrstep/2.,logrstep)
    coeffNLR = OmegaNLR / ( (_rs/rin)**eta * logrstep ).sum()
    
    ##calculate dOmega(r)
    coeff = ( (r<rsub)*coeffBLR + (r>rsub)*coeffNLR )
    val = coeff * (r/rin)**eta
    return val * ( (rin<r) * (r<rout) )

##eq. 26 in RPCI:
def rinf(MBH):
    return 2.3 * (MBH/1e8)**0.5 #pc

##eq. 27 in RPCI:
def n_f_at_rinf_Func(L2Ledd): 
    return 1.6e8 * L2Ledd #cm^-3

##v vs. r
def v_func(r2rinf, sigma_star): 
    return sigma_star * u.maxarray( r2rinf**-0.5, 1. ) #km/s

## Eddington Ratio
def L2Ledd(L,MBH):
    return L / (MBH * ld.SunEddingtonLuminosity)


## MBH-sigma relation (Guletkin+2009)
def sigmaFromMBH(MBH):
    return 200. * (MBH/10.**8.12)**(1./4.24) #km/s

## eq. 1 in spectroastrometry
def rRM(L1450):
    return 0.5 * (L1450 / 1e47)**0.5

## eq. 2 in AGNII
def MBH(fwhmha, L):
    return 10**7.4 * (fwhmha/1000.)**2.06 * (L/(Lbol2Lbha*1e44))**0.545
    

## Richards+06 Bolometric Correction
sed = [map(float,string.split(l)) for l in file('data/Richards_SED.txt').readlines()[30:]]
for i in range(140,149):
    sed[i][1] = 45.76+(i-140)*.01
def R06LBolFac(wl,FUVwl = 1538.,R06LBolFacForFUV = 4.):
    logL_wl, logL_FUV = [u.searchAndInterpolate([s[0] for s in sed], u.log(3e18/_wl), [s[1] for s in sed]) for _wl in wl, FUVwl]    
    return R06LBolFacForFUV*10.**( logL_FUV - logL_wl )

## eq. 19 in RPCII
def r_sublimation(L):
    return 0.2 * (L/10.**46.)**0.5