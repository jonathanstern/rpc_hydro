import time, subprocess, multiprocessing, os, shutil, string, glob
import ListsAndDics as ld
import my_utils as u
from pylab import *
from my_utils import log, array, rl, arange, concat, searchsorted, searchsortedclosest, unzip, out

c = 3e10; a0 = 6e-18
alphaB = 3.125e-13; #currently normalized so base model gives frac(HII)=0.5 at N/NS = 1
rydberg2Angstrom = lambda ryd: ld.rydberg*1e8 / ryd
dataDir = None

class Element:
    solarAbundances = 10**array([0.,-1.0000, -3.6108,-4.0701,-3.3098,-4.0000,-5.6696,-4.4597,-5.5302,-4.4597,-4.7352,-6.7190,-5.6003,-5.6402,-4.5498 ,-5.7496])
    MetalNames =           'h','he',    'c',    'n',    'o',    'ne',   'na',   'mg',   'al',   'si',   's',    'cl',   'ar',   'ca',   'fe',    'ni'	
    solarAbundances = dict([(MetalNames[i],solarAbundances[i]) for i in rl(MetalNames)]) 
    Asplund09Abundances = {'O':8.69,'Si':7.51,'N':7.83}
    depletions = {'Na':0.15,'Ca':10**-3.75}
    def __init__(self, name):
        self.name = name
    def shortname(self):
        elementLetter = self.name[0].upper() + ('',self.name[1])[self.name in ('argon','neon','silicon','aluminium','helium','nickel')]
        if self.name=='iron': elementLetter='Fe'
        if self.name=='magnesium': elementLetter='Mg'
        if self.name=='sodium': elementLetter='Na'
        if self.name=='zinc': elementLetter='Zn'
        if self.name=='calcium': elementLetter='Ca'
        return elementLetter
    def nElectrons(self):
        return ld.IonizationEnergiesDic[self.shortname()][0]
    def __str__(self):
        return self.shortname()
    def __repr__(self):
        return self.__str__()
    def abundance(self,abundanceSet='solar'):
        if abundanceSet=='solar': return self.solarAbundances[self.shortname().lower()]
        if abundanceSet=='depleted': return self.solarAbundances[self.shortname().lower()] * self.depletions.get(self.shortname(),1.)


elementList = [Element(name) for name in ('hydrogen','helium','sulphur', 'nitrogen', 'neon','argon','sodium','iron','oxygen','magnesium','carbon','silicon','aluminium','nickel','zinc','calcium')]
elementListDic = dict([(el.shortname(),el) for el in elementList])
class Ion:
    def __init__(self, s,ionLevel=None):
        if isinstance(s,Ion):
            self.el = s.el
            self.ionLevel = s.ionLevel
        else:
            if s[-1]=='*':
                self.excited = True
                s = s[:-1]
            else:
                self.excited = False
            for el in sorted(elementList,key=lambda el: -len(str(el))):
                if str(el)==s[:len(str(el))]:
                    self.el = el
                    if ionLevel!=None: self.ionLevel = ionLevel
                    else: self.ionLevel = u.roman.index(s[len(str(el)):])-1
                    break
    def ionizationEnergy(self,eV=False):
        if eV: fact = 1312./13.6
        else: fact = 1312. #rydberg
        if self.ionLevel==0: return 0.
        return ld.IonizationEnergiesDic[str(self.el)][2][self.ionLevel - 1] / fact
    def __hash__(self):
        return hash(self.__str__())
    def __str__(self):
        s = '%s%s'%(self.el, u.roman[self.ionLevel+1])
        if not self.excited:
            return s
        else:
            return '%s*'%s
    def __repr__(self):
        return self.__str__()
    def __eq__(self,other):
        if type(other)==type(''):
            return self.__str__()==other
        if isinstance(other, Ion):
            return self.__str__()==other.__str__()
        return False
    def __ne__(self,other):
        return not self.__eq__(other)

class IonMeasurement:
    def __init__(self,ion,logN,limit=None,elogN=None,flag=None):
        self.ion = Ion(ion)
        self.N = 10.**logN
        self.isUpperLimit = limit in ('ul','<','{<=}','{~<}')
        self.isLowerLimit = limit in ('ll','>','{>=}')
        if self.notLimit(): self.elogN = float(elogN)
        else: self.elogN=None
        self.flag = flag
    def marker(self):
        if self.isRange(): return '*'
        if self.isLowerLimit: return '^'
        if self.isUpperLimit: return 'v'
        return '.'
    def notLimit(self):
        return not(self.isLowerLimit or self.isUpperLimit)
    def isRange(self):
        return self.isLowerLimit and self.isUpperLimit
    def Nstr(self):
        if self.isUpperLimit: limitstr='<'
        elif self.isLowerLimit: limitstr='>'
        else: limitstr=''
        return '%s%.2f'%(limitstr,log(self.N))
    def __str__(self):
        return '%s %s'%(self.ion,self.Nstr())
    def __repr__(self):
        return self.__str__()
def parseMeasurement(meas,logNabsUnit=None,baseErr=0.3):
    meas = meas.strip()
    meas = meas.replace(ld.minussign,'-')
    if 'NA' in meas or 'BL' in meas: return None,None,None
    if logNabsUnit!=None:
        if '+' in meas and '-' in meas: #val+err-err
            strval, strerr = string.split(meas,'+')
            val = float(strval)
            logN = log(val)+logNabsUnit
            errp, errneg = map(float,string.split(strerr,'-'))
            elogN = log( (val + errp) / (val-errneg) ) / 2.
            limit = None
        elif '-' in meas:  #range: mn - mx
            mn,mx=map(float,string.split(meas,'-'))
            logN = mean([log(mn),log(mx)])+logNabsUnit
            elogN = log(mx/mn)/2.
            limit = None
        elif '<' in meas or '>' in meas:
            logN = log(float(meas[1:])) + logNabsUnit
            limit = meas[0]
            elogN = None	    
    else:
        if ld.pmsign in meas and '>' in meas:
            logN,elogN = map(float,string.split(meas[1:],ld.pmsign))
            limit = meas[0]
            logN -= elogN	
        elif '<' in meas or '>' in meas:
            logN = float(meas[1:])
            limit = meas[0]
            elogN = None
        elif ld.approxsign in meas: 
            logN = float(meas[3:])
            limit = None
            elogN = baseErr		
        elif ld.pmsign in meas: #\pm sign
            logN,elogN = map(float,string.split(meas,ld.pmsign))
            limit = None    
    return logN,limit,elogN
def ratioMarker(marker1,marker2):
    """return marker of marker1/marker2 ratio"""
    if marker1 == 'v':
        if marker2 in '.^':
            return 'v'
        if marker2 == 'v': 
            return None
    if marker1 == '^':
        if marker2 in '.v':
            return '^'
        if marker2 == '^': 
            return None
    if marker1 == '.':
        if marker2 == 'v':
            return '^'
        if marker2 == '^': 
            return 'v'
        if marker2 == '.':
            return '.'

class IonMeasurementSet:
    O2Hsolar = 8.69
    def __init__(self,reference,objectname,z,cols,Mstar=None,SFR=None,SFRisUL=False,L=None,v=None,Rperp=None, O2H=-1,U=(None,None),ra=None,dec=None,mags=None,includeFlag=False,
                 az_angle=None, axis_ratio=None, angle_comments=None):
        """cols format should be 
        ion,logN,''/'ul'/'dl',elogN,flag 
        or 
        ion,logN,''/'ul'/'dl',elogN 
        according to the value of 'includeFlag'
        for each ion, where eN is a string of float if not a limit and anything if is a limit"""
        self.reference = reference
        self.objectname = objectname
        self.z = z
        if O2H==-1: self.Z = -1
        else: self.Z = 10**(O2H-self.O2Hsolar)
        self.Mstar = Mstar  #in Msun
        self.Lstar = L #in units of Lstar
        self.SFR = SFR # in Msun/yr
        self.SFRisUpperLimit = SFRisUL
        self.v=v
        self.Rperp = Rperp # in kpc
        self.Urng = U
        if ra!=None: self.SDSSname = 'J'+string.join(string.split(ra+dec,':'),'')
        else: self.SDSSname=None
        self.mags = mags
        self.az_angle, self.axis_ratio, self.angle_comments = az_angle, axis_ratio, angle_comments
        if includeFlag:
            self._measurements = sorted([IonMeasurement(cols[i*5],cols[i*5+1],cols[i*5+2],cols[i*5+3],cols[i*5+4]) for i in range(len(cols)/5) if str(cols[i*5])[:2]!='Ti'], 
                                        key=lambda m: m.ion.ionizationEnergy())
        else:
            self._measurements = sorted([IonMeasurement(cols[i*4],cols[i*4+1],cols[i*4+2],cols[i*4+3]) for i in range(len(cols)/4) if str(cols[i*4])[:2]!='Ti'], 
                                        key=lambda m: m.ion.ionizationEnergy())	    
    def NHI(self,dummy=None):
        if self.hasIon('HI'):
            return log(self['HI'].N)
        else: return dummy
    def measurements(self,includeHI=False):
        return self._measurements[not includeHI:]
    def sortedAllMeasurements(self):
        return sorted(self._measurements,key=lambda me: me.ion.ionizationEnergy())
    def hasIon(self,k):
        ms = [m for m in self._measurements if str(m.ion)==k]
        return len(ms)>0
    def iMeasurement(self,meas):
        return self.measurements().index(meas)
    def radec(self):
        assert(self.SDSSname!=None)
        return u.SDSSname2radec(self.SDSSname)
    def deredmags(self):
        return [m - ld.SDSSfilterA2EBV[im]*u.MW_EBV(*self.radec()) for im,m in enumerate(self.mags)]
    def SED(self,rkpc):
        rydbergs = 3e18 / array(ld.SDSSfilters) / ld.Rydbergnu * (1.+self.z)
        nuFnus = array([u.ABmag2flux(self.deredmags()[im],ld.SDSSfilters[im] ,True) * u.luminFactor(self.z) / (4*pi*(rkpc*3e21)**2) for im,m in enumerate(self.mags)])
        return rydbergs, nuFnus
    def LinLstar(self,Rmaglstar = -21.12):  #Rband and r band are not exactly the same thing
        return 10**(0.4*(rmaglstar - (self.deredmags()[2] - 5 * log(u.calcDists(self.z)[-2] * 1e5)) ))
    def sSFR(self):
        return self.SFR / self.Mstar
    def isSF(self):
        return self.sSFR() > 0.9e-11
    def hasDetections(self,includeHI=False):
        for m in self.measurements(includeHI):
            if (not m.isUpperLimit) or m.isRange(): return True
        return False
    def get(self,k,default=None):
        try: return self[k]
        except AssertionError:
            return default
    def __getitem__(self,k):
        ms = [m for m in self._measurements if str(m.ion)==k]
        assert(len(ms)==1)
        return ms[0]
    def __iter__(self):
        return self.measurements().__iter__()
    def __len__(self):
        return self.measurements().__len__()
    def __str__(self):
        if self.reference == 'Werk+13':
            return string.join(['%s z=%.2f R_p=%d M_*=%.1f SFR=%s%.1f Z=%.1f'%(self.objectname,self.z,self.Rperp,log(self.Mstar),
                                                                               ('','<')[self.SFRisUpperLimit],self.SFR,self.Z)] + [str(m) for m in self._measurements],'\n')	
        return string.join(['%s z=%.2f'%(self.objectname,self.z,)] + [str(m) for m in self._measurements],'\n')
    def shortstring(self):
        ss = string.split(self.objectname,'_')
        return '%s'%(ss[0])


    def __repr__(self):
        return self.__str__()


    def Mhalo(self,Delta_vir = 200.):
        R200 = self.Rvir * ld.kpc
        rhomatter = ld.rhoCritz0 * (1+self.z)**3 * ld.OmegaDM
        return 4.*pi/3 * R200**3 * Delta_vir * rhomatter / ld.Msun
class ObjectSet:
    def __init__(self, objs):
        self.objs = objs
    def __len__(self):
        return len(self.objs)
    def __iter__(self):
        return self.objs.__iter__()
    def __getitem__(self,k):
        return self.objs.__getitem__(k)
    def __add__(self,other):
        return ObjectSet(self.objs + other.objs)
    def allMeasurements(self,includeHI=False):
        return u.sumlst([o.measurements(includeHI) for o in self])
    def allIons(self,includeHI=False):
        ions = unique([str(meas.ion) for meas in self.allMeasurements() ])
        return sorted(map(Ion,ions), key=lambda x: -x.ionizationEnergy()) + [[],[Ion('HI')]][includeHI]
    def plotByIon(self,xlims=(10**11.5,10**20.5),baseErr=0.2):
        rtf = u.rtfig(figsize=(23,12.5))
        subplots_adjust(left=0.04,bottom=0.04,right=0.96,top=0.98,hspace=0.,wspace=0.15)
        nRows = len(self.allIons())/3
        if len(self.allIons())%3: nRows+=1
        for iIon, ion in enumerate(self.allIons()):
            subplot(nRows,3,iIon+1)	
            allIonMeas = [(obj['HI'].N,obj[ion].N, obj[ion].marker()+obj['HI'].marker()) 
                          for iObj,obj in enumerate(self) if obj.hasIon(ion)]
            for imarker,marker in enumerate(('..','.^','.v','^.','^^','^v','v.','v^','vv')):
                tups = [m[:2] for m in allIonMeas if m[2]==marker]
                if len(tups):
                    if marker[1]=='.':
                        if marker[0]=='.': c='b'
                        else: c = 'rg'[marker[0]=='v']
                    else: c = '.3'
                    if marker[0]=='.':
                        errorbar([x[0] for x in tups],[x[1] for x in tups],
                                 [[x[1]*(1-10**-baseErr) for x in tups],
                                  [x[1]*(10**baseErr-1)  for x in tups]],
                                 fmt=None,mec=c,mfc=c,ecolor=c)
                    else:
                        u.tscatter(tups,verts=u.buildMarker(([0],[180])[marker[0]=='v']),marker=None,c=c,edgecolors=c,s=50)
                    if marker[1]!='.':
                        u.tscatter(tups,verts=u.buildMarker(([90],[270])[marker[1]=='v']),marker=None,c=c,edgecolors=c,s=50)

            loglog()
            ylim(min([x[1] for x in allIonMeas])/3,max([x[1] for x in allIonMeas])*3)
            xlim(*xlims)
            text(0.85,0.05,ion,transform=gca().transAxes,fontsize=16)
            rtf.ylabel(r'$N_{\rm ion}$',fontsize=16)
            rtf.xlabel(r'$N_{\rm HI}$',fontsize=16)
    def subset(self,func):
        return ObjectSet([o for o in self if func(o)])
    def downgrade(self, vfunc, name, z):
        cols = []
        for ion in self.allIons(includeHI=True):
            N = 0.; limit = True; upperLimit = False
            for obj in self:
                if not vfunc(obj.v) or not obj.hasIon(ion): continue
                N += obj[ion].N
                if obj[ion].notLimit(): limit = False
                if obj[ion].isUpperLimit: upperLimit = True
            limitMarker = ('.','><'[upperLimit])[limit]
            cols += [ion,log(N),limitMarker,0.]
        return IonMeasurementSet(self.objs[0].reference, name, z, cols)





    def __str__(self):	
        return 'Object set: %s'%u.mifkad([o.reference for o in self.objs])
    def __repr__(self):
        return self.__str__()
    def scatterplot(self,ion,ion0='HI',baseErr=None,c='k'):
        allIonMeas = [(obj[ion0].N,obj[ion].N,obj[ion0].elogN,obj[ion].elogN,obj[ion0].marker()+obj[ion].marker()) 
                      for iObj,obj in enumerate(self) if obj.hasIon(ion) and obj.hasIon(ion0)]
        for imarker,marker in enumerate(('..','.^','.v','^.','^^','^v','v.','v^','vv')):
            tups = [m[:4] for m in allIonMeas if m[4]==marker]
            if len(tups):
                if marker[1]=='.': # N_ion not limit
                    errorbar([x[0] for x in tups],[x[1] for x in tups],
                             [[x[1]*(1-10**-x[3]) for x in tups],
                              [x[1]*(10**x[3]-1)  for x in tups]],
                             fmt=None,mec=c,mfc=c,ecolor=c)
                else:  # N_ion limit
                    u.tscatter([x[:2] for x in tups],verts=u.buildMarker(([0],[180])[marker[1]=='v']),marker=None,c=c,edgecolors=c,s=50)
                if marker[0]=='.': # N_HI not limit
                    errorbar([x[0] for x in tups],[x[1] for x in tups],[0. for x in tups],
                             [[x[0]*(1-10**-x[2]) for x in tups],
                              [x[0]*(10**x[2]-1)  for x in tups]],
                             fmt=None,mec=c,mfc=c,ecolor=c)
                else: #N_HI limit		    
                    u.tscatter([x[:2] for x in tups],verts=u.buildMarker(([90],[270])[marker[0]=='v']),marker=None,c=c,edgecolors=c,s=50)

class CloudyLineOutput:
    def __init__(self,energy,cloudyname,intrinsicI,emergentI,linetype):
        self.wl = rydberg2Angstrom(energy)
        self.cloudyname = cloudyname
        self.intrinsicI = intrinsicI
        self.emergentI = emergentI
        self.linetype = linetype
    def __str__(self):
        return "%d: %s %.1f"%(self.wl,self.cloudyname,log(self.emergentI))
    def __repr__(self):
        return self.__str__()
class EmissionLine:
    def __init__(self,cloudyName, element=None, ionizationLevel=None, label=None, ncrit=None,
                 transitionProb=None,colStrength=None,statWeights=None,isMolecule=False,isCO=False,iCO=None,
                 isForbidden=None,isRecombination=None):
        self.cloudyName = cloudyName	
        ss = string.split(cloudyName)

        if ss[-1][-1]=='m': self.wl = float(ss[-1][:-1])*1e4
        else: self.wl = float(ss[-1])

        self._label = label
        self._isForbidden = isForbidden
        self._isRecombination = isRecombination
        self.transitionProb = transitionProb
        self.colStrength = colStrength
        self.isMolecule=isMolecule
        self.isCO=isCO
        if self.isCO: 
            self.iCO=iCO
            self.isMolecule = True
        if statWeights!=None: self.statWeights = {'top':statWeights[0]*1., 'bottom':statWeights[1]*1.}
        if element==None: 
            self.element = ss[0][0].upper()
            if len(ss[0])>=2: self.element+=ss[0][1]
        else: self.element = element
        if ionizationLevel==None:
            if len(ss)>2: sionlevel = ss[1]
            elif len(ss[0])>2:
                if ss[0][2]=='F': sionlevel = '2' #for CaII 7291
                else: sionlevel = ss[0][2:4]		
            else: 
                sionlevel=None		
            if sionlevel==None: self.ionizationLevel=sionlevel
            elif sionlevel[-1].isdigit(): self.ionizationLevel = int(sionlevel)
            elif sionlevel[-1] in u.roman: self.ionizationLevel = u.roman.index(sionlevel)	    
        else: self.ionizationLevel = ionizationLevel
        self.ncrit = ncrit
    def upperCaseName(self):
        s = self.cloudyName.upper()
        if not self.isMolecule and (s[2]==' ' or s[:4]=='CAF1'): s = s[0] + s[1].lower() + s[2:]
        if s[-1]!='M': s+='A'
        else: s=s[:-1]+'m'
        return string.join(string.split(s),' ')
    def linename(self):
        return self.element + u.roman[self.ionizationLevel] + '%d'%self.wl
    def isForbidden(self):
        if self._isForbidden!=None: return self._isForbidden
        return self.element not in ('H','He')
    def isRecombination(self):
        if self._isRecombination!=None: return self._isRecombination
        return self.element in ('H','He')    
    def label(self,onlyelement=False,onlywl=False,python=False):
        if self._label!=None: return self._label
        if not self.isMolecule: elementLabel = '%s%s %s%s'%( ('','[')[self.isForbidden()], self.element, u.roman[self.ionizationLevel], ('',']')[self.isForbidden()])
        elif self.isCO: elementLabel = 'CO J(%d-%d)'%(self.iCO+1,self.iCO)
        wlLabel = '%s'%string.split(self.cloudyName)[-1]
        if wlLabel[-1]=='m': wlLabel = wlLabel[:-1] + (r'$\mu$m','m')[python]
        else: wlLabel = wlLabel + (r'\AA','A')[python]
        if onlyelement: return elementLabel
        if onlywl: return wlLabel
        return elementLabel + ' ' + wlLabel
    def key(self):
        return self.wl
    def ionizationPotential(self,eV=True):
        assert(self.ionizationLevel!=False)
        if eV: fact = 1312./13.6
        else: fact = 1312.
        if not self.isRecombination() and self.ionizationLevel==1: return 0.
        return IonizationEnergiesDic[self.element][2][self.ionizationLevel + self.isRecombination() - 2] / fact
    def hnu(self):
        return ld.h * 3e18 / self.wl
    def qUp(self,T=1e4):
        return self.statWeights['top']/self.statWeights['bottom']*self.qDown(T)*exp(-self.hnu()/(kB*T))
    def qDown(self,T=1e4):
        return 8.629e-6 * self.colStrength * T**-0.5 / self.statWeights['top']
    def coolingRate(self,ne,T=1e4):
        return self.qUp()*self.hnu() / (1.+ne*self.qDown()/self.transitionProb)
    def __str__(self):
        return self.label(python=True)
    def __repr__(self):
        return self.label(python=True)
class SED:
    waveunits = 'Hz','rydberg','ev','kev','A','mic'
    intensityunits = 'Jnu','Fnu','nuFnu','Lnu','nuLnu','Llambda','Flambda'      #always in [ergs s^-1] and if applicable: [cm^-2], [sr^-1], [Hz^-1]
    def copyConstructor(self,other):
        assert(isinstance(other, SED))
        self.dlognu = other.dlognu
        self._nus = array(other._nus)
        self._intensities = array(other._intensities)
        self.unit = other.unit	
        self.cloudynuRange = other.cloudynuRange
        self.key = other.key
    def __init__(self, wavevalues, intensityvalues, waveunit='Hz', intensityunit='Fnu',
                 resolution=0.01,cloudynuRange=(None,None),k=None,other=None): 
        assert(waveunit in self.waveunits)
        assert(intensityunit in self.intensityunits)
        if other!=None: self.copyConstructor(other)
        else:
            if waveunit in ('A','mic'): 
                wavevalues = wavevalues[::-1]
                intensityvalues = intensityvalues[::-1]

            if waveunit=='Hz':      nus = wavevalues
            if waveunit=='rydberg': nus = wavevalues * ld.Rydbergnu
            if waveunit=='ev':      nus = wavevalues * ld.eV / ld.h
            if waveunit=='kev':     nus = wavevalues * 1000. * ld.eV / ld.h
            if waveunit=='A':       nus = 3e18 / wavevalues 
            if waveunit=='mic':     nus = 3e14 / wavevalues 

            self.dlognu = resolution
            minlnu = u.iround(log(nus[0]),self.dlognu)
            if minlnu < log(nus[0]): minlnu += self.dlognu
            maxlnu = u.iround(log(nus[-1]),self.dlognu)
            if maxlnu < log(nus[-1]): maxlnu += self.dlognu
            self._nus = 10**arange(minlnu,maxlnu,self.dlognu)
            if intensityunit[1:]=='lambda': 
                assert(waveunit=='A')
                self.unit = intensityunit[0]
                intensities = wavevalues * intensityvalues
            else: 
                self.unit = intensityunit[-3]
                intensities = intensityvalues
            self._intensities = 10.**u.searchAndInterpolateArr(log(nus), log(self._nus), log(intensities))
            if intensityunit[:2]=='nu' or intensityunit[1:]=='lambda': self._intensities /= self._nus
            self.cloudynuRange = cloudynuRange
            self.key = k
    def nus(self): return self._nus  
    def minnu(self): return self.nus()[0]
    def maxnu(self): return self.nus()[-1]
    def wls(self): return 3e18/self.nus()
    def rydbergs(self): return self.nus() / ld.Rydbergnu 
    def energies(self): return self.rydbergs() * ld.RydbergEV
    def dnus(self):
        mids = (self.nus()[2:] - self.nus()[:-2])/2.
        return concat([[mids[0]],mids,[mids[-1]]])

    def Jnus(self):
        assert(self.unit=='J')
        return self._intensities
    def nuJnus(self):
        return self.Jnus()*self.nus()
    def Fnus(self,r=None):
        if self.unit=='F': return self._intensities
        if self.unit=='J': return self._intensities * 4.*pi / 3. #correct for radiation pressure calculation only
        if self.unit=='L': return self._intensities / (4.*pi*r**2.)
    def toFnus(self,r=None):
        newSED = self.__class__(other=self)
        newSED._intensities = array(newSED.Fnus(r))
        newSED.unit='F'
        return newSED      
    def nuFnus(self,r=None):
        return self.nus()*self.Fnus(r)
    def nuFnus_cast(self,nus,r=None):
        res = self.nus()*self.Fnus(r)
        return u.searchAndInterpolateArr(self.nus(), nus, res)

    def Lnus(self):
        assert(self.unit=='L')
        return self._intensities
    def nuLnus(self):
        return self.nus() * self.Lnus()
    def Qnus(self,r=None):
        return self.Fnus(r) / (ld.h * self.nus())
    def nuQnus(self,r=None):
        return self.Qnus(r)*self.nus()
    def integrate(self,intensities,nurng=None):
        if nurng!=None: s,e = self.nuInd(nurng)
        else: s,e=None,None
        return (intensities*self.dnus())[s:e].sum()
    def __add__(self,other):
        assert(isinstance(other,SED))
        assert(self.unit==other.unit)
        assert(self.dlognu==other.dlognu)
        newSED = self.__class__(other=self)
        minlnu = log(min(self.minnu(), other.minnu()))
        maxlnu = log(max(self.maxnu(), other.maxnu()))
        newSED._nus = 10.**arange(minlnu,maxlnu+newSED.dlognu,newSED.dlognu)
        newSED._intensities = zeros(len(newSED._nus))
        for sed in (self,other):
            ind = searchsortedclosest(newSED._nus, sed.minnu())
            l = len(sed.nus())
            newSED._intensities[ind:ind+l] += sed._intensities
        return newSED
    def addrange(self,other,minnu,maxnu):
        newSED = other.__class__(other=other)
        s,e=newSED.nuInd(array([minnu,maxnu]))
        for i in rl(newSED._nus):
            if (i<s) or (i>=e): 
                newSED._intensities[i] = 0.
        return self + newSED	
    def __mul__(self,other):
        assert(isinstance(other,float) or isinstance(other,int))
        newSED = self.__class__(other=self)
        newSED._intensities *= other
        return newSED
    def __div__(self,other):
        return self.__mul__(1./other)

    def nuInd(self,nu):
        return searchsorted(self.nus(),nu)
    def ionizationInds(self,maxRydberg=1000.):
        return self.nuInd(ld.Rydbergnu), self.nuInd(ld.Rydbergnu*maxRydberg) 
    def adjustSpec(self,nurange,factor):
        s,e = self.nuInd(nurange)
        self._intensities[s:e]*=factor
    def phi(self,isOpticallyThin,byF=False,maxRydberg=1000.):
        if not byF:
            if isOpticallyThin:	    
                return self.integrate(self.Jnus() / (ld.h * self.nus()) * 4 * pi, (ld.Rydbergnu, ld.Rydbergnu*maxRydberg))
            else:
                return self.integrate(self.Jnus() / (ld.h * self.nus()) * 2 * pi, (ld.Rydbergnu, ld.Rydbergnu*maxRydberg))
        else:
            return self.integrate(self.Fnus() / (ld.h * self.nus()), (ld.Rydbergnu, ld.Rydbergnu*maxRydberg))
    def ng(self,isOpticallyThin,byF=False):
        return self.phi(isOpticallyThin,byF) / ld.c
    def meanhnu(self,maxRydberg=1000.):
        arbitraryr=1 #in case self.unit=='L'
        num     = self.integrate(self.Fnus(arbitraryr), (ld.Rydbergnu, ld.Rydbergnu*maxRydberg))
        denom   = self.integrate(self.Qnus(arbitraryr), (ld.Rydbergnu, ld.Rydbergnu*maxRydberg))
        return num/denom
    def __str__(self): 
        return str(self.key)
    def __repr__(self):
        return self.__str__()
    def addSlopes(self,slopesBegin,slopesEnd):
        nus = self._nus
        Fnus = self.Fnus()
        for minwl,slope in slopesBegin[::-1]:
            nus = u.concat([array([3e18/minwl]),nus])
            newFnu = Fnus[0] / 10**(log(nus[1]/nus[0]) * slope)
            Fnus = u.concat([array([newFnu]),Fnus])
        for maxwl,slope in slopesEnd:
            nus = u.concat([nus,array([3e18/maxwl])])
            newFnu = Fnus[-1] * 10**(log(nus[-1]/nus[-2]) * slope)
            Fnus = u.concat([Fnus,array([newFnu])])
        return SED((nus,Fnus), waveunit='Hz', intensityunit='Fnu',k=self.key,resolution=self.dlognu)




    def forCloudy(self,r=1.,dMinVal=-15,nusPerLine=50):
        if self.cloudynuRange[0]!=None: s = searchsorted(self.nus(),self.cloudynuRange[0])
        else: s = 0
        if self.cloudynuRange[1]!=None: e = searchsorted(self.nus(),self.cloudynuRange[1])
        else: e = len(self.nus())

        xs = log(self.nus())[s:e]
        ys = log(self.Fnus(r))[s:e]

        for i in rl(ys): 
            if not isinf(ys[i]): break
        s=i
        for i in range(len(ys)-1,-1,-1): 
            if not isinf(ys[i]): break
        e=i+1
        xs = xs[s:e]
        ys = ys[s:e]	
        minVal = ys.max() + dMinVal
        s = ''
        for i in range(0,len(xs),nusPerLine):
            prenom = ('continue   ','interpolate')[i==0]	    
            s += '%s %s\n'%(prenom, string.join(['(%.4f %.4f)'%(xs[i],max(ys[i],minVal)) for i in range(i,min(i+nusPerLine,len(xs)))]))
        return s[:-1]

class PowerLawSED(SED):
    def copyConstructor(self,other):
        SED.copyConstructor(self,other=other)    
        self.index = other.index
        self.coeff = other.coeff
        self.nucutoff = other.nucutoff
        self.nu0 = other.nu0
    def __init__(self,index=None,coeff=None,nu0=None,nucutoff=None,minnu=1e9,maxnu=1e22,
                 intensityUnit='F',other=None):
        if other!=None: self.copyConstructor(other)
        else:
            self.index = index
            self.coeff = coeff
            self.nucutoff = nucutoff
            self.nu0 = nu0
            nus = 10.**arange(log(minnu),log(maxnu),0.01)
            intensities =  coeff * (nus/nu0)**index 
            if nucutoff!=None: intensities * e ** (-nus/nucutoff)	    
            SED.__init__(self, constructorInput=(nus,intensities), waveunit='Hz', intensityunit=intensityUnit+'nu')
class opacitySED(SED):
    intensityunits = ('cm^2 H^-1',)
    def __init__(self, constructorInput=None, waveunit='Hz',isLog=(False,False),
                 resolution=0.01,cloudynuRange=(None,None),k=None,other=None): 
        SED.__init__(self, constructorInput=constructorInput, waveunit=waveunit,intensityunit='cm^2 H^-1',
                     isLog=isLog,resolution=resolution,cloudynuRange=cloudynuRange,k=k,other=other)

    def sigmas(self):
        return self._intensities
    def taus(self,nus,NH):
        return NH*u.searchAndInterpolateArr(log(self.nus()),log(nus),self.sigmas())
    def extinction(self,nus,NH):
        return u.e**-self.taus(nus, NH)

class Model:
    basedir = None
    cloudyexe = '/home/jonathan/research/separate/c17.00/source/cloudy.exe'
    #cloudyexe = '/home/jonathan/research/separate/programs/c13.03/source/cloudy.exe'
    #cloudyexe = '/home/jonathan/research/separate/c13.05/source/cloudy.exe'
    #cloudyexe = '/home/jonathan/research/separate/programs/c13.03_fix/c13.03/source/cloudy.exe'
    cloudy10exe = '/home/jonathan/research/separate/programs/c10.01/source/cloudy.exe'
    outputprefix = '.coutput'
    inputprefix = '.cinput' 
    nus0dic =     {('H',0):  3e18 / rydberg2Angstrom(1.),
                   ('He',0): 3e18 / rydberg2Angstrom(2372.3/1312.),
                   ('He',1): 3e18 / rydberg2Angstrom(4.) }
    nus0Opacdic = {('H',0):  6e-18,
                   ('He',0): 8e-18,
                   ('He',1): 1.5e-18 }
    cloudyMetalNames = 'he','li','be','b','c','n','o','f','ne','na','mg','al','si','p','s','cl','ar','k','ca','sc','ti','v','cr','mn','fe','co','ni','cu','zn'
    logXi2logUdic = {3:4.5,2:3.,1.5: 2.3,1.2: 1.9,1:1.6,0.5:0.6,0:-0.4,-0.2: -0.7,-0.5:-1.1,-1:-1.8,-1.5:-2.4,-1.8: -2.7,-2:-2.9,-3:-4.,
                     2.7:4,1.7:2.5,0.7:1,0.2:0.,-0.3:-0.8,-0.8:-1.5,-1.3:-2,-2.3:-3.3,-3.3:-4.}
    meanhnuForUtoNconversion = 36*ld.eV


    def __init__(self, modelname, n=None, U=None, Z = 2, aEUV=None,initialPressure = True,EddingtonRatio=None,CFdust=0.5, Uisxi=False,HighEnergyPhotons=True,ISRF=None,
                 dust=True,useIR=True,columndensity=23,neutralColumn=False,inputSEDdir=None,givenSED=None,BB=None, GrovesParams=False,setPresIoniz=None,turbulence=None,stopT=None,stop_pfrac=None,
                 loadAll=False,modeldir=None,emittedRadiationPressure=True,xspecRef=None,iterate=True,d2m=None,emissionLines=[],ispdr=False,HMz=None,zCMB=None,
                 starburst99fn=None,starburst99logage=None,elementlist=elementList,opticalFactor=None,ionizationSpectrumLoadEnergies=(1.,1000),isGlobule=False,max_nDecs=7,
                 radius=None,luminosity=None,logXi=None,savelevel=2,maxIterations=10,runC10=False,logBreakFreq=None,dlogBreakFlux=None,saveTmap=False,electronTemp=None,
                 densityPowerLawIndex=None,densityPowerLawScale=None,densityPowerLawGivenAsAMD=True,drmax=None,CRB=False,dustyNotDepleted=False,fixNa=True,dereddenedAGN=False,firstLoad=True,nuFnu=None,
                 nmaps=17,AV=None,dustType=None,nuFnu_freq=None,profileFileName=None):
        self.name = modelname
        #if initial pressure is an integer, U*n effectively sets n_gamma. 
        #Final n_0 will be so that n_0*T_0 = P_0, which can be different than given n
        self.n = n
        self.U = U 
        self.Uisxi = Uisxi
        if nuFnu==None and nuFnu_freq==None: self.nuFnu=None
        else: self.nuFnu = nuFnu,nuFnu_freq
        self.Z = Z
        self.inputT = electronTemp
        self.aEUV = aEUV
        self.hasdust = dust
        self._modeldir = modeldir
        self.useIR = useIR
        self.CFdust = CFdust
        self.colDensity = columndensity
        self.AV = AV
        self.neutralColumn = neutralColumn
        self.ionizationSpectrumLoadEnergies = ionizationSpectrumLoadEnergies
        self.inputSEDdir = inputSEDdir
        self.givenSED = givenSED  #SED from SED class
        self.BB = BB
        self.EddingtonRatio = EddingtonRatio
        self.GrovesParams = GrovesParams
        self.fixNa = fixNa
        self.xspecRef = xspecRef
        if d2m!=None: self.d2m = 10.**d2m
        else: self.d2m = None
        self.emittedRadiationPressure = emittedRadiationPressure
        self.loaded = False
        self.CloudyStat = "not loaded / files not written"
        self.setPresIoniz = setPresIoniz
        self.iterate = iterate
        self.highEnergyPhotons = HighEnergyPhotons
        self.turbulence = turbulence	
        self.ISRF = ISRF
        self.HMz = HMz
        self.emissionLines = emissionLines
        self.elements = elementlist
        self.starburst99fn,self.starburst99logage = starburst99fn,starburst99logage
        self.radius = radius
        self.luminosity = luminosity #this is ionizing luminosity
        assert(stopT==None or not ispdr)
        self.stopT = stopT
        self.CRB = CRB
        self.stop_pfrac = stop_pfrac
        self.maxIterations = maxIterations
        self.savelevel = savelevel
        if ispdr: self.stopT=10
        if not os.path.exists(self.modeldir()[:-1]): os.mkdir(self.modeldir()[:-1])
        if self.inputSED() and firstLoad: #SED from previous cloudy run
            shutil.copy(self.basedir + self.inputSEDdir + '/transmittedSpectrum.coutput', self.inputSEDfile())
        self.opticalFactor = opticalFactor
        if self.opticalFactor!=None:
            shutil.copy(self.basedir + 'alteredContinuum%d.cinput'%self.opticalFactor, self.modeldir() + 'alteredContinuum%d.cinput'%self.opticalFactor)	
        assert(initialPressure==True or logXi==None)
        self.initialPressure = initialPressure
        if logXi!=None:
            logXi = u.iround(logXi,0.1)
            Prad = 10.**self.luminosity / (4.*pi*(10.**self.radius)**2*ld.c) 
            self.initialPressure = log( Prad/10**logXi / ld.kB )
            ng = Prad / self.meanhnuForUtoNconversion
            self.n = u.iround(log(ng) - self.logXi2logUdic[logXi],0.1)
            self.logXi = logXi
        self.Cloudy10 = runC10
        if self.Cloudy10: assert(True not in [el.isMolecule for el in self.emissionLines])
        self.logBreakFreq = logBreakFreq
        self.dlogBreakFlux = dlogBreakFlux  	
        self.saveTmap = saveTmap
        if loadAll: self.loadAll()
        self.isGlobule = isGlobule
        if (not densityPowerLawGivenAsAMD) or (densityPowerLawIndex==None):
            self.densityPowerLawIndex = densityPowerLawIndex
            self.densityPowerLawScale = densityPowerLawScale
        else:
            self.AMDindex = densityPowerLawIndex
            self.AMDscale = densityPowerLawScale
            if not self.isGlobule:
                self.densityPowerLawIndex  = 1./densityPowerLawIndex
                self.densityPowerLawScale = self.AMDscale - log( self.AMDindex * u.ln(10.) )
            else:
                self.max_nDecs = max_nDecs
                self.densityPowerLawIndex = 1. / (1-self.AMDindex)
                self.densityPowerLawScale = self.AMDscale - (self.n + log(u.ln(10)*(1-self.AMDindex)))
        self.drmax = drmax
        self.dustyNotDepleted=dustyNotDepleted
        self.dereddenedAGN = dereddenedAGN
        self.zCMB = zCMB
        self.nmaps = nmaps
        self.dustType = dustType
        if profileFileName!=None:
            f = np.load(dataDir+profileFileName+'.npz')
            self.profile = f['log_r'],f['log_n'],f['log_T']
        else:
            self.profile = None
        
    def __call__(self, fname, *args,**kwargs):	
        if type(fname)==type(''):
            f=lambda x: x
            if fname[-2:]=='_l': 
                f = lambda x: u.log(x)
                fname=fname[:-2]
            if fname[-2:]=='_n':
                f = lambda x: not x
                fname=fname[:-2]
            if not hasattr(self, fname): raise AttributeError("cloudy.Model has no attribute '%s'"%fname)
            member = getattr(self,fname)
            if type(member)==type(self.loadAll): 	    
                return f(getattr(self,fname)(*args,**kwargs))
            else: 
                return f(getattr(self,fname))

        else:
            return fname(self)
    def loadAll(self,requireNoWarnings=True):	
        if requireNoWarnings and not self.CloudyOk(): return
        self.loadDepths()
        if self.savelevel>0: 
            self.loadInOutSpectrum()
        else:
            self.loadSpectrumEnergies()
        self.loadOverview()
        self.loadIonization()
        self.loadEnergetics()
        self.loadElements()
        self.loadLines()
        self.loadOpacities(onlyIonizing=False)        
        if self.savelevel>1: 
            self.loadOpacities(onlyIonizing=True)            
        self.loadOpticalDepths()
        if self.savelevel>1: 	    
            self.loadSpectra()  
        self.loadPressure()
        self.loadHydrogen()
        self.loadAbundances()
        self.loadTotalHeatingCooling()
        self.loaded = True
        self.CloudyStat = "loaded"
    def loadTmap(self,filename = 'Tmap',returnEquilibriumTemperature=False,includeIonization=False):
        assert(self.saveTmap)
        ls = u.filelines(self.modeldir() + filename + self.outputprefix)
        Hdic = {}
        Cdic = {}
        if includeIonization:
            n_e = {}
            n_HI = {}
            n_HeI = {}
            n_HeII = {}
        for l in ls:
            ss = string.split(l,'\t')
            try: T = float(ss[0])
            except: continue
            Hdic[T] = float(ss[1])
            Cdic[T] = float(ss[4])
            if includeIonization:
                n_e[T] = float(ss[-6])
                n_HI[T] = float(ss[-5]) * 10**float(ss[-4])
                n_HeI[T] = float(ss[-5]) * self.Hescale() * 10**float(ss[-3])
                n_HeII[T] = float(ss[-5]) * self.Hescale() * 10**float(ss[-2])
        if returnEquilibriumTemperature:
            Teq = float(string.split(ls[1])[3])
            return Hdic, Cdic, Teq
        if includeIonization:
            return Hdic, Cdic, n_e, n_HI, n_HeI, n_HeII
        return Hdic, Cdic
    def CloudyOk(self):
        """
        to check warnings run in model dir:
        grep "W-" */<model name>*coutput
        """
        try: 
            f = file(self.iofilename(isinput=False))
        except IOError:
            return False
        try:
            l = f.readlines()[-1]
        except IndexError:
            return False
        f.close()
        return 'Cloudy exited OK' in l
    def exectime(self):
        """
        to check warnings run in model dir:
        grep "W-" */<model name>*coutput
        """
        try: 
            f = file(self.iofilename(isinput=False))
        except IOError:
            return -1
        try:
            l = f.readlines()[-2]
        except IndexError:
            return -1
        f.close()
        if 'ExecTime' in l: return float(string.split(l)[-1])
        return -1
    def nIterations(self):
        ls = u.filelines(self.iofilename(isinput=False))
        ss = string.split(ls[-2])
        ind = [i for i in rl(ss) if 'iterations' in ss[i]][0]
        return float(ss[ind-1])	
    def stopReason(self,maxIters=100):
        ls = u.filelines(self.iofilename(isinput=False))
        reasonStrs = [None]*maxIters
        for l in ls: 
            if 'Calculation stopped because' not in l: continue
            ind = l.index(' Iteration')
            reasonStr = l[31:ind]
            iterNumber= int(l[ind+11:ind+13])
            reasonStrs[iterNumber-1] = reasonStr
        return reasonStrs[:reasonStrs.index(None)]
    def lastStopReason(self,maxIters=100):
        return self.stopReason(maxIters)[-1]


    def modeldir(self):
        if self._modeldir==None: return self.basedir + self.name + '/'
        return self.basedir + self._modeldir + '/'
    def griddir(self):
        return string.split(self._modeldir,'/')[0]
    def copyModel(self,dest):
        shutil.copytree(self.modeldir(), dest+self.name)

    def nFiles(self):
        return len(glob.glob(self.modeldir()+'*'))
    def inputSED(self):
        return self.inputSEDdir!=None	
    def inputSEDfile(self):
        assert(self.inputSED())
        return self.modeldir() + 'inputSED.cinput'
    def iofilename(self,isinput):
        return self.modeldir() + self.name + (self.outputprefix,self.inputprefix)[isinput]
    def slope3to1mic(self):
        return -1.5 + u.log(self.CFdust/0.5)/u.log(1./3)
    def SED(self):
        cloudyMinFreq = rydberg2Angstrom(1e-8)
        cloudyMaxFreq = rydberg2Angstrom(7.5e6)	
        slopes = [(cloudyMinFreq, 60e4, 2.5), (60e4,20e4, -0.5), (20e4,3e4,-1.),(3e4,1e4,self.slope3to1mic()),(1e4,1100,-0.5),  #LD93
                  (1100,6.2,self.aEUV), 
                  (6.2,12.4/200,-1.), (12.4/200, cloudyMaxFreq,-2.) ]  # a_x from BAT (Tueller+08), a_gamma from Hazy
        if self.dereddenedAGN:
            slopes[4] = (1e4,1100,-0.1)
            dlogL_UV = (-0.1 - -0.5) * log(1e4/1100.)
            slopes[5] = (1100,6.2,self.aEUV - dlogL_UV / log(1100/6.2))

        if not self.highEnergyPhotons: 
            slopes[-2] = (6.2,1.24,-1.)
            slopes[-1] = (1.24, cloudyMaxFreq,-2.) 
        if self.GrovesParams: slopes = [(cloudyMinFreq, 2480, 2.5), (2480.,12.4,self.aEUV), (12.4,cloudyMaxFreq,-2.5)]
        elif not self.useIR: slopes = [(cloudyMinFreq, 3e4, 2)] + [(3e4, 1e4, 1/3.)] + slopes[4:]
        freqslist = [u.log(3e18/slope[0]) for slope in slopes] + [u.log(3e18/slopes[-1][1])]
        fluxslist = [19.07] + [-u.log(slope[1]/slope[0])*slope[2] for slope in slopes] #first number is for typical PG quasars luminosity	
        fluxslist = [u.sumlst(fluxslist[:i+1]) for i in rl(fluxslist)]
        if self.dlogBreakFlux!=None:
            ind = searchsorted(freqslist, self.logBreakFreq)
            oldFlux = u.searchAndInterpolate(freqslist, self.logBreakFreq, fluxslist)
            freqslist = freqslist[:ind] + [self.logBreakFreq]            + freqslist[ind:]	    
            fluxslist = fluxslist[:ind] + [oldFlux + self.dlogBreakFlux] + fluxslist[ind:]	
        return array(freqslist), array(fluxslist)
    def ionizingFluxFraction(self,s=1.,e=1000.):
        lnus,lfnus = self.SED()
        nuFnus = 10**(lnus+lfnus)
        totalFlux = ( (lnus[1:] - lnus[:-1]) * (nuFnus[1:]+nuFnus[:-1])/2. ).sum()

        ion_lnus_range = log(array([s,e])*ld.Rydbergnu)
        sind,eind=searchsorted(lnus,ion_lnus_range)
        s_nuFnu,e_nuFnu = [u.searchAndInterpolate(lnus,x,nuFnus) for x in ion_lnus_range]
        ion_lnus = concat([[ion_lnus_range[0]],lnus[sind:eind-1],[ion_lnus_range[1]]])
        ion_nuFnus = concat([[s_nuFnu],nuFnus[sind:eind-1],[e_nuFnu]])
        ionFlux = ( (ion_lnus[1:] - ion_lnus[:-1]) * (ion_nuFnus[1:]+ion_nuFnus[:-1])/2. ).sum()

        return ionFlux / totalFlux
    def meanhnu(self,alpha=None,e=1000.):
        if alpha==None: alpha = self.aEUV
        return ( alpha/(alpha+1.) * (e**(alpha+1) - 1.) / (e**alpha-1.) ) * ld.h*ld.Rydbergnu

    def GrovesAbundances(self):
        g04_depl = array([-0.987,  -3.91,  -4.42,  -3.53,  -3.92,  -6.28,  -5.12,  -7.21,  -5.49,  -4.8,   -7.02,  -5.6,   -8.17,  -6.54,   -7.15])
        ism      = array([-1.0088, -3.6003,-4.1002,-3.4962,-3.9101,-6.5003,-4.8996,-7.1002,-5.5003,-4.4895,-7.0000,-5.5498,-9.3872,-6.2000 ,-7.7399])
        solar    = array([-1.0000, -3.6108,-4.0701,-3.3098,-4.0000,-5.6696,-4.4597,-5.5302,-4.4597,-4.7352,-6.7190,-5.6003,-5.6402,-4.5498 ,-5.7496])
        names =           'he',    'c',    'n',    'o',    'ne',   'na',   'mg',   'al',   'si',   's',    'cl',   'ar',   'ca',   'fe',    'ni'	
        ratio2SolarISM = dict([(names[i], self.Z*10**(g04_depl[i] - ism[i])) for i in rl(names) if names[i] not in 'he'])
        ratio2SolarISM['he'] = self.Hescale() * 10**(g04_depl[0] - ism[0])
        ratio2SolarISM['n'] = self.Nscale() * ratio2SolarISM['n']	
        return [(self.cloudyMetalNames[i], ratio2SolarISM.get(self.cloudyMetalNames[i],self.Z)) for i in rl(self.cloudyMetalNames)]
    def xspecAbundances(self,whichref,zeroabundance=-12,filename = 'data/RPC_WA/xspecAbundances.txt'):	
        f = file(filename)
        ls = [string.split(l[:-1]) for l in f.readlines()[11:-1]]
        f.close()

        abundic = {}
        for iref, ref in enumerate(ls[0][1:]):
            abundic[ref] = dict([(l[0].lower(), u.log(float(l[iref+1])*self.Z)) for l in ls[2:]])
            abundic[ref]['he'] = abundic[ref]['he']/self.Z + self.GivenHeChangeWithZ() ##change
            abundic[ref]['n'] *= self.Nscale()
        return [(self.cloudyMetalNames[i], abundic[whichref].get(self.cloudyMetalNames[i],zeroabundance)) for i in rl(self.cloudyMetalNames)]


    def Nscale(self):
        return 0.19347 + 0.80653*self.Z #Groves+04 change to Groves+06
    def Hescale(self):
        return 0.71553 + 0.28447*self.Z #Groves+04
    def Nascale(self):
        if self.fixNa and self.hasdust and not self.dustyNotDepleted:
            return 6.4555 #undeplete Na following Weintgartner&Draine 2001 (ApJ..563..842)
        return 1.
    def GivenHeChangeWithZ(self):
        return 0.0293 * (self.Z-1.) #Groves+04

    def writeInputFile(self):
        ###stopping criteria
        stopstring=""
        if self.colDensity!=None:
            if not self.neutralColumn:
                stopstring += "stop column density %f\n"%self.colDensity
            else:
                stopstring += "stop neutral column density %f\n"%self.colDensity
        if self.stopT!=None:
            stopstring += "stop temperature %dK linear\n"%self.stopT
        if self.stop_pfrac!=None:
            stopstring += "stop pfrac %.2f\n"%self.stop_pfrac
        if self.isGlobule:
            logMaxDepth = self.densityPowerLawScale + log(1-10**-(self.max_nDecs/self.densityPowerLawIndex))
            stopstring += "stop depth %.8f\n"%logMaxDepth
        if self.AV!=None:
            stopstring += "stop AV %.4f extended\n"%self.AV
        if self.profile!=None:
            stopstring += "stop depth %.8f\n"%self.profile[0][-2]
        assert(stopstring!="")


        ###SED
        SEDstrs = []
        if self.inputSED():
            SEDstrs.append( 'table read "%s"'%self.inputSEDfile() )
        if self.BB!=None:
            SEDstrs.append( 'blackbody t=%f'%self.BB )
        if self.ISRF!=None:
            SEDstrs.append( """table ism %d  
CMB
Extinguish column = 22"""%self.ISRF )
        if self.CRB:
            SEDstrs.append( """COSMIC RAYS BACKGROUND""" )
        if self.HMz!=None:
            SEDstrs.append( """table HM05 redshift %.1f"""%(self.HMz) )
        if self.opticalFactor!=None:
            SEDstrs.append( string.join(u.filelines(self.modeldir() + 'alteredContinuum%d.cinput'%self.opticalFactor),'') )
        if self.starburst99fn!=None:
            SEDstrs.append( 'table star "%s" %f'%(self.starburst99fn,self.starburst99logage) )
        if self.aEUV!=None:
            SEDstrs.append( 'interpolate ' + string.join(['(%f %f)'%tuple(tup) for tup in unzip(self.SED())]) )
        if self.givenSED!=None:
            SEDstrs.append( self.givenSED.forCloudy() )
        if self.zCMB!=None:
            SEDstrs.append( 'CMB %.2f'%self.zCMB )
        SEDstr = string.join(SEDstrs,'\n')

        ###intensity
        if self.luminosity!=None: 
            #intensityStr = "luminosity %.1f\n"%self.luminosity
            intensityStr = "phi(H) %.1f\n"%log(10.**self.luminosity / (4.*pi*(10.**self.radius)**2 * self.meanhnu(self.aEUV)) )
        elif self.nuFnu!=None:
            lnuFnu, freq = self.nuFnu
            intensityStr = "nuF(nu) %.3f at %.3d\n"%(lnuFnu,freq)
        else:
            if type(self.initialPressure)==type(True): 
                if self.ISRF!=None or self.HMz!=None: intensityStr = ""
                elif self.Uisxi: intensityStr = "xi %.3f\n"%self.U
                else: intensityStr = "ionization parameter %.3f\n"%self.U
            else: 
                assert(not self.Uisxi)
                intensityStr = "phi(H) %.1f\n"%(self.n + self.U + u.log(c))

        ###radius
        if self.radius!=None: 
            #radiusStr = "radius %.1f\n"%self.radius
            radiusStr = ''

        else: radiusStr = ''

        ###dust and metals
        if self.hasdust and (self.d2m==None or self.d2m>0.):
            if self.GrovesParams: 
                s = "abundances " + string.join(['%.2f'%x[1] for x in self.GrovesAbundances()],' ')
                metals="""abundances ism no grains 
grains ism %f 
%s 
save grains opacity "grains.coutput" last"""%(self.Z,s)
            elif self.d2m!=None:
                metalnames = ['he', 'li', 'be', 'b', 'c', 'n', 'o', 'f', 'ne',
                              'na', 'mg', 'al', 'si', 'p', 's', 'cl', 'ar', 'k',
                               'ca', 'sc', 'ti', 'v', 'cr', 'mn', 'fe', 'co', 'ni',
                                'cu', 'zn']
                baseDepletions = array([1.,0.16,0.6,0.13,0.4,1.,0.6,0.3,
                                        1.,0.2,0.2,0.01,0.03,0.25,1.,0.4,
                                        1.0,0.3,0.0001,0.005,0.008,0.006,
                                        0.006,0.05,0.01,0.01,0.01,0.1,0.25])
                depletions = 1. - (1. - baseDepletions) * self.d2m
                depletionstr = string.join(['%s %s\n'%( ('abundances','continue  ')[j!=0],
                                                        string.join(['%2s=%.4f'%(metalnames[j+i], depletions[j+i]) for i in range(min(5,len(depletions)-j))]) )
                                            for j in range(0,len(depletions),5)],'')[:-1]
                metals="""%s
grains ism %f
metals and grains %f
save grains opacity "grains.coutput" last
save grains charge "grainsCharge.coutput" last
save grains drift velocity "grainsDrift.coutput" last
save grains heating "grainsHeating.coutput" last
%ssave grains temperature "grainsTemperature.coutput" last"""%(depletionstr, self.d2m, self.Z, ('','save grains qs "grainsQs.coutput" last\n')[self.savelevel>0])
            elif self.dustyNotDepleted:
                metals="""metals and grains %f
save grains opacity "grains.coutput" last
save grains charge "grainsCharge.coutput" last
save grains drift velocity "grainsDrift.coutput" last
save grains heating "grainsHeating.coutput" last
%ssave grains temperature "grainsTemperature.coutput" last"""%(self.Z,('','save grains qs "grainsQs.coutput" last\n')[self.savelevel>0])
            else:
                if self.dustType==None: 
                    dustStr = ''
                else:
                    dustStr = 'no grains\n'
                    for ss in string.split(self.dustType,'_'):
                        dustStr += 'grains %s\n'%ss
                metals="""abundances ism %s
metals and grains %f
save grains opacity "grains.coutput" last
save grains charge "grainsCharge.coutput" last
save grains drift velocity "grainsDrift.coutput" last
save grains heating "grainsHeating.coutput" last
%ssave grains temperature "grainsTemperature.coutput" last"""%(dustStr[:-1],self.Z,('','save grains qs "grainsQs.coutput" last\n')[self.savelevel>0])
        elif self.xspecRef!=None:
            metals = "abundances " + string.join(['%.2f'%x[1] for x in self.xspecAbundances(self.xspecRef)],' ')
        else:
            metals = """metals %f"""%self.Z

        ### density, pressure, temperature, turbulence and gravity
        if type(self.initialPressure)!=type(True):
            pressure = """constant pressure set %.1f reset
"""%self.initialPressure
        elif self.initialPressure:
            pressure = """constant pressure // 'no abort' not needed if Pline << Pgas 
"""
        else:
            pressure = ""
        if not self.emittedRadiationPressure:
            pressure += """No radiation pressure
"""
        # Groves04: We therefore have chosen to set the hydrogen density in the models to be the density in the [S ii] emission
        #           zone, physically near to the ionization front, but where there are still sufficient ionizing photons to ensure
        #           that n_Hii / n_Hi ~ 1
        # Hazy:     By default the gas pressure in the first zone is derived from hden and the resulting kinetic temperature. 
        #           In consecutive iterations only T changes	
        #           If the keyword 'reset' is used, nT changes in consective iterations so the total P is constant.
        if self.inputT == None and self.profile==None: 
            tempstr = ""    
        elif self.profile!=None:
            tempstr = 'tlaw table depth\n'
            for i in range(len(self.profile[0])):
                tempstr += 'continue %.6f %.6f\n'%(self.profile[0][i],self.profile[2][i])
            tempstr += 'end of tlaw\n'            
        else: 
            tempstr = 'Constant temperature %.2f\n'%self.inputT

        if self.turbulence in (None,0): turbstr=""
        else: turbstr = "turbulence %d no pressure\n"%self.turbulence

        if self.EddingtonRatio==None: gravity = ""
        else:
            gravity = """gravity plane-parallel	u.log %.1f
"""%self.calcGravity()
        if self.densityPowerLawIndex==None and self.profile==None:
            densityStr = 'hden %.3f'%self.n	
        elif self.profile!=None:
            densityStr = 'dlaw table depth\n'
            for i in range(len(self.profile[0])):
                densityStr += 'continue %.6f %.6f\n'%(self.profile[0][i],self.profile[1][i])
            densityStr += 'end of dlaw'
        elif not self.isGlobule:
            densityStr = 'hden %.3f, power = %.2f, scale column density = %.2f'%(self.n,self.densityPowerLawIndex,self.densityPowerLawScale)
        else:
            densityStr = 'Globule density=%.3f, depth=%.8f, power=%.2f'%(self.n,self.densityPowerLawScale,self.densityPowerLawIndex)

        ### calculation	    
        if self.setPresIoniz!=None: setpresioniz = """SET PRES IONIZ %d
"""%self.setPresIoniz
        else: setpresioniz=""
        if self.saveTmap: saveTmapstr = 'set nmaps %d\nsave map zone 0 range 1 to 9.5 "Tmap.coutput" \n'%self.nmaps
        else: saveTmapstr = ""
        if self.drmax!=None: drmaxStr = 'set drmax %.2f\n'%self.drmax
        else: drmaxStr = ""


        ### elements
        elements=string.join(['save element %s "%s.coutput" last'%(element.name,element.name) for element in self.elements],'\n')
        linenames = string.join([x.cloudyName for x in self.emissionLines],'\n')

        ### input file creation
        variable="""Set WeakHeatCool 0.01
set save prefix "%s"
%s%s%s%s%s
%s
%s%s%s%s%s%s%s%s%s%s
"""%(self.modeldir(), stopstring, intensityStr, radiusStr,drmaxStr,densityStr, metals, pressure, gravity, 
     ("element nitrogen scale %f\n"%self.Nscale(),"")[self.GrovesParams], 
     ("element helium scale %f\n"%self.Hescale(),"")[self.GrovesParams],
     ("element sodium scale %f\n"%self.Nascale(),"")[self.GrovesParams],
     setpresioniz,
     ("","iterate to convergence max=%d\n"%self.maxIterations)[self.iterate],
     turbstr,tempstr,     
     SEDstr)	
        general = """print citation
print ages
save abundances "abundances.coutput" last
save ages "timescales.coutput" last
save overview "overview.coutput" last 
save radius "radius.coutput" last
save hydrogen ionization "recombination.coutput" last 
save hydrogen populations "hydrogenPopulation.coutput" last
save temperature "temperature.coutput"
save optical depths "opticaldepths.coutput" last
save pressure "pressure.coutput" last 
%ssave line labels "linelabels.coutput" last
save lines, emissivity, "lines.coutput" last
%s
end of lines
save lines, emissivity, emergent "linesEmergent.coutput" last 
%s
end of lines
save heating "heating.coutput" last
save cooling "cooling.coutput" last
%s%ssave ionizing continuum every "ionizingContinuum.coutput" last  
save transmitted continuum "transmittedSpectrum.coutput" last  
%s%s"""%((r'// ','')[self.savelevel==2],linenames, linenames,
         ('','save continuum "cloudEmission.coutput" last\nsave lines, array "lineTotals.coutput" last\n')[self.savelevel>0],
       ('save total opacity "opacity.coutput" last\n',
        'save continuum every "ionizingSpectrum.coutput" last\nsave total opacity every "opacity.coutput" last\n')[self.savelevel>1],
       saveTmapstr,elements)
        f = file(self.iofilename(isinput=True),'w')
        f.write(variable + general)
        if self.Cloudy10: f.write('\n\n')
        f.close()
        self.CloudyStat="files written"
    def run(self,showprogress=False):
        runModel(self,showprogress=showprogress)	
    def readOutputFile(self, filename, lineprefix='',fields=False,fieldstart=64, fieldsize=32,vallen=9, retBreaks=False,turnToFloat=True,tillfirstbreak=False):
        f = file(self.modeldir() + filename + self.outputprefix)
        ls = []; breaks = []; i=0
        while True:
            l = f.readline()
            if l=='': break
            if l[0]!='#' and l[0]!='\n':
                if ( (type(lineprefix)==type('') and l[:len(lineprefix)]==lineprefix) or
                     (type(lineprefix)==type(()) and lineprefix[0]<float(l.split(None,1)[0])<lineprefix[1]) ) :
                    ls.append(l)
                    i+=1
            else:
                if not tillfirstbreak: breaks.append(i)                
                elif i>0: break
        f.close()
        if not fields:
            vals = [[(str,u.floatCanFail)[turnToFloat](x) for x in string.split(l)] for l in ls]
        else:
            vals = []
            for l in ls:
                fields = [l.expandtabs()[i:i+fieldsize].rstrip() for i in range(fieldstart,len(l.expandtabs())-1,fieldsize)]
                vals.append( dict([(field[:-vallen].rstrip(),float(field[-vallen:])) for field in fields]) )
        if not retBreaks: return vals
        else: return vals, breaks
    def loadSpectrumEnergies(self, energyrange=(1e-8,1e6),filename = 'transmittedSpectrum'):
        vals = self.readOutputFile(filename)[6:]
        s,e = searchsorted([x[0] for x in vals], energyrange)
        self.spectrumEnergies = array([x[0] for x in vals[s:e]])
        self.transmittedSpectrum = array([x[1] for x in vals[s:e]])
        assert(len(self.spectrumEnergies))
    def loadInOutSpectrum(self, energyrange=(1e-8,1e6),filename = 'cloudEmission'):
        """
        header: energy nFn
        """
        vals = self.readOutputFile(filename)
        s,e = searchsorted([x[0] for x in vals], energyrange)
        self.spectrumEnergies = array([x[0] for x in vals[s:e]])
        self.incidentSpectrumnuFnus   = array([x[1] for x in vals[s:e]])
        self.observedSpectrumnuFnus   = array([x[5] for x in vals[s:e]])
        self.emittedOutwardSpectrumnuFnus   = array([x[3] for x in vals[s:e]])
        self.attenuatedAndDiffuse = array([x[4] for x in vals[s:e]])
        self.attenuated = array([x[2] for x in vals[s:e]])
        self.emittedReflectedAndemittedOutwardandAttentuatedSpectrumnuFnus   = array([x[6] for x in vals[s:e]])
        self.lineSpectrumnuFnus   = array([x[7] for x in vals[s:e]])  #only reflected emission
        self.lineOutwardSpectrumnuFnus   = array([x[8] for x in vals[s:e]])
    def loadDepths(self, filename = 'radius'):
        vals = self.readOutputFile(filename)
        self.depths    = array([sum([vals[i][3] for i in range(j)])+vals[j][3]/2. for j in rl(vals)])
        self.olddepths = array([vals[j][2] for j in rl(vals)]) 
        self.ddepths   = array([vals[j][3] for j in rl(vals)]) 
    def loadOverview(self,filename = 'overview'):
        """
        header: depth Te Htot hden eden 2H_2/H HI HII HeI HeII HeIII CO/C C1 C2 C3 C4 O1 O2 O3 O4 O5 O6 AV(point) AV(extend)
        """
        vals = self.readOutputFile(filename)
        self.Ts, self.heating, self.nH, self.ne = [array([x[i] for x in vals]) for i in (1,2,3,4)]
        self.Hstate, self.Hestate, self.Ostate = [[array([x[i] for x in vals]) for i in iStates] for iStates in [(6,7),(8,9,10),(16,17,18,19,20,21)]]
        self.AVs = [[x[i] for x in vals] for i in (22,23)]
    def loadElements(self):
        self.elementStates = {}
        for iElement,element in enumerate(self.elements):
            elementLetter = element.shortname()
            try:
                vals = self.readOutputFile(element.name)
            except IOError:
                continue
            self.elementStates[elementLetter] = [array([log(x[i]) for x in vals]) for i in range(1,element.nElectrons()+2)]
        #self.elementStates['H'], self.elementStates['He'] = self.Hstate, self.Hestate

    def loadIonization(self, filename = 'recombination'):
        vals = self.readOutputFile(filename,lineprefix='hion')
        self.ionizationRates, self.recombinationCoeffsTotal, self.recombinationCoeffsB =  [[x[i] for x in vals] for i in (2,4,5)]        
    def loadLines(self, filenames = ('lines','lineTotals')):
        vals = self.readOutputFile(filenames[0])
        if len(vals):
            self.linesdicF = dict([(self.emissionLines[i].key(), array([x[i+1] for x in vals])) for i in rl(vals[0],-1)])
        else:
            self.linesdicF = {}
        vals = self.readOutputFile(filenames[0] + 'Emergent')
        if len(vals):
            self.linesdicE = dict([(self.emissionLines[i].key(), array([x[i+1] for x in vals])) for i in rl(vals[0],-1)])
        else:
            self.linesdicE = {}

        if self.savelevel>0:    
            vals = self.readOutputFile(filenames[1],turnToFloat=False)
            linesdic = dict([(string.join(val[1:-3]), float(val[-2])) for val in vals])
            self.linesdicT = dict([(self.emissionLines[i].key(), linesdic.get(self.emissionLines[i].upperCaseName(),None)) for i in rl(self.emissionLines)])
    def loadAdditionalLines(self,ks,filename='lineTotals'):
        vals = self.readOutputFile(filename,turnToFloat=False)
        linesdic = dict([(string.join(val[1:-3]), float(val[-2])) for val in vals])
        self.linesdicT.update( dict([(k, linesdic.get(k,None)) for k in ks]) )
    def loadAllLines(self,filename='lineTotals'):
        vals = self.readOutputFile(filename,turnToFloat=False)
        linelist = [CloudyLineOutput(float(v[0]), string.join(v[1:-3]), 10.**float(v[-3]), 10**float(v[-2]), v[-1]) for v in vals]
        self.allLinesDic = dict([(l.cloudyname,l) for l in linelist])

    def lineEW(self,wl,lineFlux,CF_line,useR06,basewl = 1450.,CF_continuum=1.):
        """ this function is correct only for slab geometry"""
        if useR06:
            ind =  u.searchsorted(self.nus(False),3e18/basewl)
            nuFnu_unit_CF = self.incidentSpectrumnuFnus[ind] * 10**(tf.R06LBolFac(basewl) - tf.R06LBolFac(wl)) 
        else:
            ind =  u.searchsorted(self.nus(False),3e18/wl)
            nuFnu_unit_CF = self.incidentSpectrumnuFnus[ind] 
        Llambda = nuFnu_unit_CF / wl * CF_continuum 
        return lineFlux * CF_line / Llambda
    def AllStrongLines(self,minwl,maxwl,minEW,CF_line,useR06,CF_continuum):
        lines = self.loadAllLines()
        return [l for l in lines if minwl < l.wl < maxwl and self.lineEW(l.wl, l.emergentI, CF_line,useR06,CF_continuum=CF_continuum)>minEW]

    def loadEnergetics(self, filenames = ('heating','cooling')):
        self.totalHeating = array([x[2] for x in self.readOutputFile(filenames[1],fields=False)])
        self.totalCooling = array([x[3] for x in self.readOutputFile(filenames[1],fields=False)])
        self.heaters  = self.readOutputFile(filenames[0],fields=True,fieldsize=24)	    ##commented out due to bug in output
        self.coolants = self.readOutputFile(filenames[1],fields=True,fieldsize=32)         ##commented out due to bug in output

    def loadAbundances(self):
        ls = u.filelines(self.iofilename(False))
        ind = [il for il in rl(ls) if ls[il].strip()=='Gas Phase Chemical Composition'][0]
        abundls = [l.strip() for l in ls[ind+1:ind+5]]
        abundstrs = u.sumlst([[l[13*i:13*i+11] for i in range(len(l)/13+1)] for l in abundls])
        abundstrsplit = [string.split(a,':') for a in abundstrs]
        self.abundances = dict([(a[0].strip(),10**float(a[1])) for a in abundstrsplit if len(a)==2])
    def abundance2Solar(self,el):
        return self.abundances[el] / elementListDic[el].abundance()
    def loadOpacities(self, filenames = ('opacity','grains'),onlyIonizing=True):
        if self.savelevel>1: 
            vals, breaks = self.readOutputFile(filenames[0],lineprefix=((1e-8,1e6),self.ionizationSpectrumLoadEnergies)[onlyIonizing],retBreaks=True)

            tmp = [array([x[1] for x in vals[breaks[i]:breaks[i+1]]]) for i in rl(breaks,-1)][:-1]  #there is one too many total opacities
            if onlyIonizing:    self.totalKappas = tmp
            else:               self.allTotalKappas = tmp

            vals = self.readOutputFile(filenames[0],lineprefix=(1e-8,1e6),tillfirstbreak=True)
            self.faceTotalOpacities = array([x[1] for x in vals]) / self.nH[0]
        else:	    
            vals = self.readOutputFile(filenames[0],lineprefix=((1e-8,1e6),self.ionizationSpectrumLoadEnergies)[onlyIonizing])
            tmp = array([x[1] for x in vals]) #radiation pressure cross section	    
            if onlyIonizing:    self.totalKappas = tmp
            else:               self.allTotalKappas = tmp	    
        if self.hasdust:
            vals = self.readOutputFile(filenames[1],lineprefix=(1e-8,1e6))
            self.grainOpacities = array([x[1] for x in vals]) 
    def loadOpticalDepths(self, filename = 'opticaldepths'):
        try:
            vals = self.readOutputFile(filename,lineprefix=(1e-8,1e6))
        except IOError, e:
            print e
            return
        self.total_taus = array([x[1] for x in vals])
        self.absorption_taus = array([x[2] for x in vals])
        self.scattering_taus = array([x[3] for x in vals])

    def loadGrainTemperatures(self, filename = 'grainsTemperature'):
        assert(self.hasdust)
        vals = self.readOutputFile(filename)
        self.grainTemperatures = array(vals)[:,1:]

    def totalOpacities(self,onlyIonizing=True):
        if onlyIonizing: kappas = self.totalKappas
        else: kappas = self.allTotalKappas
        return [kappas[i] / self.nH[i] for i in range(self.nZones())]
    def totalOpacitiesSingleZone(self,iZone,onlyIonizing=True):
        if onlyIonizing: kappas = self.totalKappas
        else: kappas = self.allTotalKappas
        if self.savelevel<2: #only opacities of last zone are saved
            return kappas / self.nH[-1]
        else:
            return kappas[iZone] / self.nH[iZone]

    def gasOpacities(self,onlyIonizing=True):
        if onlyIonizing:
            s,e = self.ionizationEnergyInds()
            return [self.totalOpacities(True)[i]-self.grainOpacities[s:e] for i in range(self.nZones())]
        else:
            return [self.totalOpacities(False)[i]-self.grainOpacities for i in range(self.nZones())]
    def gasOpacitiesSingleZone(self,iZone,onlyIonizing=True):
        if onlyIonizing:
            s,e = self.ionizationEnergyInds()
            return self.totalOpacitiesSingleZone(iZone,True)-self.grainOpacities[s:e]
        else:
            return self.totalOpacitiesSingleZone(iZone,False)-self.grainOpacities

    def Qnus(self,i):
        return self.spectra[i] / (ld.h*self.nus()**2)
    def Kappas(self,elementname='H',ionlevel=0):
        nus0    = self.nus0dic[(elementname, ionlevel)]
        nus0ind = searchsorted(self.nus(),nus0)	

        nIonPernH    = 10**getattr(self,elementname + 'state')[ionlevel] / self.nH
        nu0Opac = self.nus0Opacdic[(elementname, ionlevel)]
        opacGas = nu0Opac * (self.nus()/nus0)**(-3.)

        s,e = self.ionizationEnergyInds()
        opacDust = self.grainOpacities[s:e]

        return array([nIonPernH[i] * (self.Qnus(i)*opacGas* (nIonPernH[i]*opacGas / (nIonPernH[i]*opacGas + opacDust)) *self.dnus())[nus0ind:].sum() /
                      (self.Qnus(i)*self.dnus())[nus0ind:].sum() for i in rl(self.spectra)])
    def meanSigmaDust(self,ionizing=False,photonmean=False,power=1.,grainOpacities=None): #left for backward compatibility
        return self.meanSigma(iZone=0,component='dust', ionizing=ionizing,photonmean=photonmean,power=power,inputOpacities=grainOpacities)
    def meanSigma(self,iZone=0,component=None,ionizing=False,photonmean=False,power=1.,inputOpacities=None,onlyOptical=False,spectrum=None):
        if ionizing:
            s,e = self.ionizationEnergyInds()
        elif onlyOptical:
            s,e = 1,self.ionizationEnergyInds()[0]	    
        else:
            s,e = 1,-1
        if inputOpacities!=None:
            opacities = inputOpacities
        elif component=='dust':
            opacities = self.grainOpacities[s:e]
        elif component=='gas':
            opacities = self.gasOpacitiesSingleZone(iZone,False)[s:e]
        normpower = (1,2)[photonmean]
        if type(spectrum)!=type(None):
            spec = spectrum[s:e]
        elif self.savelevel==1:
            spec = self.incidentSpectrumnuFnus[s:e]
        elif self.savelevel>=2:
            if ionizing:
                spec = self.spectra[iZone]
            else:
                spec = self.allSpectra[iZone][s:e]
        totalOpac = ((spec / self.nus(False)[s:e]**normpower * opacities**power) * self.dnus(False)[s-1:e-1]).sum()	    
        denom     = ((spec / self.nus(False)[s:e]**normpower)                    * self.dnus(False)[s-1:e-1]).sum()	    
        return totalOpac / denom


    def ionizationEnergyInds(self):
        return [searchsortedclosest(self.spectrumEnergies, energy) for energy in self.ionizationSpectrumLoadEnergies]
    def ionizationEnergies(self):
        s,e = self.ionizationEnergyInds()
        return self.spectrumEnergies[s:e]
    def nus(self,ionizing=True):
        if ionizing: es = self.ionizationEnergies()
        else: es = self.spectrumEnergies
        return 3e18/rydberg2Angstrom(es)
    def dnus(self,ionizing=True,ionizingShort=False):
        if ionizing: 
            if ionizingShort:
                s,e = self.ionizationEnergyInds()	    
            else:
                s,e = self.ionizationEnergyInds()[0]-1, self.ionizationEnergyInds()[1]+1	
        else: 
            s,e = None, None
        nus = 3e18/rydberg2Angstrom(self.spectrumEnergies[s:e])
        return (nus[2:] - nus[:-2])/2.	
    def loadSpectra(self, filename = 'ionizingSpectrum',onlyIonizing=False):
        vals, breaks = self.readOutputFile(filename,lineprefix=((1e-8,1e6),self.ionizationSpectrumLoadEnergies)[onlyIonizing],retBreaks=True)
        breaks += [len(vals)]
        tmp = array([array([x[2] for x in vals[breaks[i]:breaks[i+1]]]) for i in rl(breaks,-1)])
        if onlyIonizing: self.spectra = tmp
        else:            self.allSpectra = tmp
        self.diffuseSpectra = array([array([x[3] for x in vals[breaks[i]:breaks[i+1]]]) for i in rl(breaks,-1)])
        self.transmittedSpectra = array([array([x[4] for x in vals[breaks[i]:breaks[i+1]]]) for i in rl(breaks,-1)])
        self.emittedspectra = array([array([x[5] for x in vals[breaks[i]:breaks[i+1]]]) for i in rl(breaks,-1)])

    def loadPressure(self, filename = 'pressure'):
        vals = self.readOutputFile(filename)
        self.Pcorrect, self.Pcurrent, self.Ptotal = [array([x[i] for x in vals]) for i in (1,2,3)] #all these should be the same
        self.Pgas, self.Prad = [array([x[i] for x in vals]) for i in (5,8)]
    def loadHydrogen(self, filename = 'hydrogenPopulation'):
        vals = self.readOutputFile(filename)
        self.nHI, self.nHII, self.nH1s, self.nH2s, self.nH2p, self.nH3 = [array([x[i] for x in vals]) for i in range(1,7)]

    def getPlotter(self):
        return Plot(model=self)

    def U0(self):
        return self.incidentPhotonFlux() / self.nH[0] / ld.c
    def ng0(self): 
        return self.incidentPhotonFlux() / ld.c
    def ng(self):
        return array([(self.Qnus(i) * self.dnus()).sum() / c for i in rl(self.spectra)])
    def localU(self):	
        return self.ng() / self.nH
    def localxi(self):
        Fion = array([(self.Qnus(i) * ld.h * self.nus() * self.dnus()).sum()  for i in rl(self.spectra)])
        return Fion * 4. * pi / self.nH
    def localxiOther(self,mn=0.54,mx=10.):
        s,e = searchsorted(self.nus(), (mn*1000*eV/ld.h, mx*1000*eV/ld.h))
        Fion = array([(self.Qnus(i) * ld.h * self.nus() * self.dnus())[s:e].sum()  for i in rl(self.spectra)])
        return Fion * 4. * pi / nH	
    def localUeffective(self):
        return self.nHII / self.nHI * 3.12e-13 / c / 1e-17
    def localZeta(self):
        return self.Prad / self.Pgas
    def localColumnDensity(self, attr='nH',islog=False, index=None,k=None,fractional=False):	
        vals = getattr(self, attr) 
        if k!=None: vals = vals[k]
        if index!=None: vals = vals[index]
        if islog: vals = 10.**vals
        if fractional!=False: vals = vals*self.nH*fractional
        dcolumn = vals * self.ddepths
        return array([dcolumn[:i+1].sum() for i in rl(dcolumn)])
    def Nion(self,ion):
        if ion.__class__!= Ion: ion = Ion(ion)
        return self.localColumnDensity('elementStates',index=ion.ionLevel,k=str(ion.el),fractional=self.abundances[str(ion.el)],islog=True)
    def Nelement(self,el):
        return (self.Nion('HI')+self.Nion('HII')) * self.abundances[str(el)]
    def s2sT(self):
        return u.log(10**self.linesdicT[6716.]+ 10**self.linesdicT[6731.])
    def ratios(self):
        return (None,#10**self.linesdicT[6563.]/(ld.h*3e18/6562.) / (self.localU()[0]*self.nH[0]*c/2.2), 
                None,#10**self.linesdicT[4861.]/(ld.h*3e18/4861.) / (self.localU()[0]*self.nH[0]*c/2.2/2.3), 
                10**self.linesdicT[5007.] / 10**self.linesdicT[4861.], 
                10**self.linesdicT[6584.] / 10**self.linesdicT[6563.], 
                10**self.s2sT() / 10**self.linesdicT[6563.], 
                10**self.linesdicT[6300.] / 10**self.linesdicT[6563.],
                10**self.linesdicT[6563.] / 10**self.linesdicT[4861.],
                10**self.linesdicT[6584.] / 10**self.s2sT(),
                )
    def getAllEnergyProcesses(self, iscoolant=True):
        lst = (self.heaters, self.coolants)[iscoolant]
        return sorted(unique(concat([d.keys() for d in lst])).tolist())
    def getEnergyProcess(self, processname,iscoolant=True):
        lst = (self.heaters, self.coolants)[iscoolant]
        return array([d.get(processname,0) for d in lst])
    def printEnergySummary(self,iscoolant=True):
        out(['%s\t %.3f'%y for y in 
             sorted([(k,self.getEnergyProcess(k,iscoolant).max()) 
                     for k in self.getAllEnergyProcesses(iscoolant)],key=lambda x: -x[1])])
    def getMainCoolants(self, zone,n=1):
        items = self.coolants[zone].items()
        sitems = sorted(items,key=lambda x: -x[1])
        return [x[0] for x in sitems[:n]]
    def estimatedN_Sfraction(self):
        assert(not self.Uisxi)
        return min(0.7e-2 / self.Z / 10**self.U,1.)
    def ionizationFront(self):
        q=[i for i in rl(self.Hstate[1]) if 10**self.Hstate[1][i]<0.5]
        if len(q): return q[0]
        print "no ionization in:", self.n, self.U, self.Z, self.aEUV
        return len(self.Hstate[1])-1
    def nZones(self):
        return len(self.nH)
    def zoneByNHI(self,lNHI):
        return searchsorted(self.localColumnDensity('Hstate',index=0,fractional=1.),10**lNHI)
    def nIonizationFront(self):
        return self.nH[self.ionizationFront()]
    def GrovesU(self):
        assert(not self.Uisxi)
        kT = kB * 20000.
        GrovesN = self.Pgas[0] / kT
        return u.log(10**self.U * (self.nH[0] / GrovesN))
    def baseU(self): #incident radiation / n at ionization front
        assert(not self.Uisxi)
        return u.log(10**self.U * 10**self.n / self.nIonizationFront())
    def nMolecular(self,T=10):
        return u.log(self.Pgas[-1] / (kB * T))
    def incidentFlux(self):
        return ((self.incidentSpectrumnuFnus / self.nus(False) )[1:-1] * self.dnus(False)).sum()
    def incidentIonizingFlux(self):
        s,e = self.ionizationEnergyInds()
        return ((self.incidentSpectrumnuFnus[s:e] / self.nus() ) * self.dnus()).sum()
    def incidentPhotonFlux(self):
        s,e = self.ionizationEnergyInds()
        return ((self.incidentSpectrumnuFnus[s:e] / (ld.h * self.nus()**2) ) * self.dnus()).sum()

    def AbsorbedSpectra(self,onlyIonizing=True,integrated=False):
        if onlyIonizing: 
            s,e = self.ionizationEnergyInds()
            specs = self.spectra
        else: 
            s,e = None, None
            specs = self.allSpectra
        if integrated: base = self.incidentSpectrumnuFnus[s:e]
        else: base = concat([[self.incidentSpectrumnuFnus[s:e]], specs[:-1]])
        return  base - specs
    def AbsorbedEnergy(self,onlyIonizing=True,integrated=False):
        absorbedSpectra = self.AbsorbedSpectra(onlyIonizing,integrated)
        if onlyIonizing: s,e = None, None
        else: s,e = 1,-1
        return array([(absorbedSpectra[i][s:e] / self.nus(onlyIonizing)[s:e] * self.dnus(onlyIonizing)).sum() for i in rl(absorbedSpectra)])
    def getBandLuminosity(self,maxwl,minwl,zone=None,withLines=True,includeIncident=True):
        if zone==None:
            s,e = [searchsortedclosest(self.spectrumEnergies, ld.rydberg*1e8 / wl) for wl in maxwl,minwl]
            if includeIncident: spec = self.observedSpectrumnuFnus
            else: spec = self.emittedSpectrumnuFnus
            total = ((spec / self.nus(False) )[s+1:e+1] * self.dnus(False)[s:e]).sum()
            #if self.radius!=None and self.luminosity!=None: total *= 4.*pi*(10.**self.radius)**2.  #in this case a default CF=1 is assumed
            if not withLines:
                totalLines = ((self.lineSpectrumnuFnus / self.nus(False) )[s+1:e+1] * self.dnus(False)[s:e]).sum()
                total -=totalLines
            return total	
        else:
            s,e = [searchsortedclosest(self.nus(), 3e18 / wl) for wl in maxwl,minwl]
            dL = (((self.emittedspectra[zone] - (self.emittedspectra[zone-1],0.)[zone==0]) / self.nus() )[s:e] * self.dnus()[s:e]).sum()
            return dL / self.ddepths[zone]
    def sigmaAnalytical(self):
        ngeg = self.incidentFlux()/ c 
        Ts = 10**((self.Ts[:-1] + self.Ts[1:])/2.)
        dlnns = ln(self.nH[1:] - self.nH[:-1])
        return dlnns * (2.3 * kB * Ts) / (ngeg * self.ddepths )
    def sigmaActual(self,i):
        num   = ((self.allSpectra[i] / self.nus(False) * self.allTotalKappas[i] / self.nH[i])[1:-1] * self.dnus(False)).sum()
        denom = ((self.allSpectra[i] / self.nus(False))[1:-1] * self.dnus(False)).sum()
        return num/denom
    def sigmaActualIonizing(self,i):
        num   = (self.spectra[i] / self.nus() * self.totalKappas[i] / self.nH[i] * self.dnus()).sum()
        denom = (self.spectra[i] / self.nus() * self.dnus()).sum()
        return num/denom
    def sigmaXray(self,i,kevrange=(0.5,10.)):
        nusrange = 3e18 / (ld.rydberg*1e8 / (array(kevrange) * 1000. / RydbergEV))
        s,e = searchsorted(self.nus(), nusrange)
        num   = (self.spectra[i] / self.nus() * self.totalKappas[i] / self.nH[i] * self.dnus())[s:e].sum()
        denom = (self.spectra[i] / self.nus() * self.dnus())[s:e].sum()
        return num/denom
    def sigmaFromdP(self): #only correct where tau<<1
        return [(self.Pgas[i+1] - self.Pgas[i])/(self.depths[i+1]-self.depths[i]) / (self.incidentFlux()/c) / mean(self.nH[i:i+2])
                for i in rl(self.depths,-1)]
    def sigmaIncident(self):
        num   = ((self.incidentSpectrumnu / self.nus(False) * self.allTotalKappas[0] / self.nH[0])[1:-1] * self.dnus(False)).sum()
        denom = ((self.incidentSpectrumnu / self.nus(False))[1:-1] * self.dnus(False)).sum()
    def xi0(self,e=1000.):
        if not self.Uisxi:
            alpha = self.aEUV
            if alpha!=-1: meanhnu = alpha/(alpha+1.) * (e**(alpha+1) - 1.) / (e**alpha-1.)
            else:         meanhnu = (ln(e) - 1. ) / (1. - 1./e)
            return self.U + u.log(4*pi*c*RydbergEV*eV * meanhnu)
        return self.U
    def Xi(self):
        return self.incidentIonizingFlux()/ld.c/self.Pgas[0]
    def emissionAveragedLogDensity(self,wl):
        return (log(self.nH)*10**self.linesdicE[wl]*self.ddepths).sum() / (10**self.linesdicE[wl]*self.ddepths).sum()
    def tau(self,j):
        if not hasattr(self, '_dtaus'):
            self._dtaus = self.nH*self.ddepths*array([self.sigmaActualIonizing(i) for i in rl(self.ddepths)])
        return self._dtaus[:j+1].sum()
    def taus(self):
        return [self.tau(i) for i in rl(self.depths)]
    def tauHI(self,sigmaHI = 10.**-17.2):
        ind = searchsorted(self.nus(False), ld.Rydbergnu)
        sigmaDust = self.grainOpacities[ind]
        return self.localColumnDensity() * sigmaDust + self.Nion('HI')*sigmaHI
    def AMD(self,dlogxi=0.25,xirange=(-5,8),getUs = False, phiCalculatedAtSurface=False):
        if not getUs:
            xis = u.log(self.localxi())
        else:
            if phiCalculatedAtSurface:
                xis = u.log(self.incidentPhotonFlux() / ld.c / 10**self.nH)
            else:
                xis = u.log(self.localU())
        dNs = self.ddepths * self.nH
        inds = rl(self.nH)
        xs = u.arange(xirange[0]+dlogxi/2., xirange[1], dlogxi)
        ys = zeros(len(xs))
        for ind in inds:
            xsind = searchsortedclosest(xs, xis[ind])
            ys[xsind] += dNs[ind]
        ys = array([(u.log(ys[i] / dlogxi), 0.)[ys[i]==0.] for i in rl(ys)]) 
        xs2 = mergearrs([xs-dlogxi/2., xs+dlogxi/2.])
        ys2 = mergearrs([ys,ys])
        s,e = ys2.nonzero()[0][0],ys2.nonzero()[0][-1]
        init = ys2[e]
        return xs2[s:e+1],ys2[s:e+1]
    def alterContinuum(self,nonIonizingFactor,fn='alteredContinuum%d.cinput',perline=100):
        col1 = log(self.nus(False))
        ind = searchsorted(col1,log(3e18/912.))
        col2 = log(self.incidentSpectrumnuFnus / self.nus(False))
        col2[:ind] += log(nonIonizingFactor)
        tups = unzip((col1,col2))
        txt = string.join([('continue','interpolate')[i==0] + ' ' + 
                           string.join(['(%.4f %.4f)'%(tup[0],tup[1]) for tup in tups[i*perline:(i+1)*perline]]) for i in range(len(tups)/perline+1)],'\n')
        f = file(self.basedir + fn%nonIonizingFactor,'w')
        f.writelines(txt)
        f.close()



    def HI2H(self,nHI):

        ind = searchsorted(self.Nion('HI'),nHI)
        if ind<len(self.nH):
            return self.localColumnDensity()[ind]
        else:
            return self.localColumnDensity()[-1] + nHI - self.Nion('HI')[-1] #assumes after cloudy calculation all H is neutral
    def atNion(self,ion,N,func,*args,**kwargs):
        vals = func(self,*args,**kwargs)
        return u.searchAndInterpolate(self.Nion(ion), N, vals)#if val is None?
    def totalDustEmission(self,maxnu=1000*ld.eVnu):
        reflected = self.getBandLuminosity(1e7,4e3,withLines=False)
        return reflected + self.transmittedEmission(maxnu)
    def transmittedEmission(self,maxnu=1000*ld.eVnu):
        e = searchsorted(self.nus(False),maxnu)
        transmitted = (self.transmittedSpectrum[1:e] / self.nus(False)[1:e]  * self.dnus(False)[:e-1]).sum()	
        return transmitted



    def loadTotalHeatingCooling(self):
        ls = u.filelines(self.iofilename(isinput=False))
        for l in ls[::-1]:
            if l[:14]==' ENERGY BUDGET':
                self.totalLogCoolingPerUnitArea = float(string.split(l)[5])
                self.totalLogHeatingPerUnitArea = float(string.split(l)[3])
                break
def runModel(model,showprogress=False):  #not an instance method because instance method can't be pickled
    #processinfo('runModel()')
    stdin  = file(model.iofilename(isinput=True ), 'r')
    stdout = file(model.iofilename(isinput=False), 'w')
    print 'running (%s): %s'%(time.ctime().split()[3],model.name), 'process id:', os.getpid()	
    if not model.Cloudy10:
        proc = subprocess.call(model.cloudyexe, stdout=stdout, stdin=stdin)
    else:
        proc = subprocess.call(model.cloudy10exe, stdout=stdout, stdin=stdin)
    stdin.close()
    stdout.close()
    print 'ending (%s): %s'%(time.ctime().split()[3],model.name), 'process id:', os.getpid()
    return True    
def loadModel(model,requireNoWarnings):
    print 'running: %s'%model.name, 'process id:', os.getpid()	
    #print model    
    model.loadAll(requireNoWarnings)
    print 'ending: %s'%model.name, 'process id:', os.getpid()	



class Grid(dict):
    def tostr(self, char, val,nDigitsDic={}):
        ndigits = nDigitsDic.get(char,1)
        if type(val)==type(''): 
            if self.strfunc==None: val = float(val[-4:])
            else: return self.strfunc(val)
        if isinstance(val, SED): return '_%c%s'%(char,self.SEDstrfunc(val))
        if val!=None: return ('_%c%.'+str(ndigits)+'f')%(char,val)
        else: return ''
    def tokey(self, k):
        if isinstance(k,SED): return self.SEDkeyfunc(k)
        return k
    def __init__(self, gridname, ns=[None], Us=[None], aEUVs=[None], Zs=[None],ps=[True],SEDs=[None],colDenss=[None],neutralColumn=False,setPresIoniz=None,iterate=True,Uisxi=False,strfunc=None,
                 logXis=[None],stopT=None,electronTemps=[None],stop_pfrac=None,useIR=True,
                 nuFnus=[None],nuFnu_freq=None,AVs=[None],
                 notCartesian=[],forCompatibility=[],GrovesParams=False,hasdust=True,dusties=[None],emittedRadiationPressure=True,xspecRef=None,HighEnergyPhotons=True,HMzs=[None],                 
                 Ts=[None],d2ms=[None],turbulences=[None],ISRFs=[None],emissionLines=None,ispdr=False,opticalFactors=[None],SEDstrfunc=None,SEDkeyfunc=None,nDigitsDic={},
                 starburst99fns=[None],starburst99logages=[None],ionizationSpectrumLoadEnergies=(1.,1000.),luminosities=[None],radii=[None],savelevel=2,maxIterations=10,runC10=False,CRB=False,
                 dlogBreakFluxes=[None],logBreakFreq=u.log(ld.Rydbergnu*4.),oldkey=False,saveTmap=False,densityPowerLawScales=[None],densityPowerLawIndices=[None],densityPowerLawGivenAsAMD=True,drmax=None,
                 dustyNotDepleted=False,dereddenedAGN=False,firstLoad=True,isGlobule=False,max_nDecs=7,zCMB=None,
                 nmaps=17,dustType=[None],profile_fns=[None]):
        """
        to add a new variable which changes between the models in the grid:
        1. add it to Model as a new input to __init__ with default None
        2. add functionality in Cloudy Model
        3. add it to Grid.__init__ as a new input with default [None]
        4. save it as a member in Grid
        5. add it to variables list below: (variable, "<cloudyModel" variable, first letter)
        """
        dict.__init__(self)
        self.strfunc = strfunc
        self.SEDkeyfunc = SEDkeyfunc
        self.SEDstrfunc = SEDstrfunc
        self.ns = ns
        self.Us = Us
        self.aEUVs = aEUVs
        self.Zs = Zs
        self.electronTemps = electronTemps
        self.name = gridname
        assert (len(ps)==1 and ps[0]==True or len(logXis)==1 and logXis[0]==None)
        self.ps = ps	
        self.logXis = logXis
        self.SEDs=SEDs	
        self.radii = radii
        self.luminosities = luminosities
        self.nuFnus = nuFnus
        if dusties==[None]: dusties = [hasdust]  ##hasdust kept for backward compatibility
        self.dusties = dusties
        if isinstance(SEDs[0], SED): sedtuple = (SEDs, 'givenSED', 'S')
        else: sedtuple = (SEDs, 'inputSEDdir', 'S')
        self.colDenss = colDenss
        self.AVs = AVs
        self.Ts = Ts
        self.d2ms = d2ms
        self.GrovesParams=GrovesParams
        self.turbulences = turbulences	
        self.ISRFs = ISRFs
        self.HMzs = HMzs
        self.ispdr = ispdr
        self.opticalFactors = opticalFactors
        self.starburst99fns,self.starburst99logages = starburst99fns,starburst99logages
        self.dlogBreakFluxes = dlogBreakFluxes
        self.dustType = dustType
        self.densityPowerLawIndices = densityPowerLawIndices   #note these could be in AMD format or density power law format, depending on densityPowerLawGivenAsAMD
        self.densityPowerLawScales = densityPowerLawScales     #note these could be in AMD format or density power law format, depending on densityPowerLawGivenAsAMD
        if not os.path.exists(Model.basedir + self.name): os.mkdir(Model.basedir + self.name)
        self.models = {}
        variables = [(ns,'n','n'), 
                     (Us,'U','U'), 
                     (aEUVs,'aEUV', 'a'), 
                     (Zs, 'Z', 'Z'), 
                     (ps, 'initialPressure', 'p'),
                     sedtuple,
                     (Ts, 'BB', 'T'),
                     (electronTemps, 'electronTemp','t'),
                     (dusties,'dust','D'),
                     (d2ms, 'd2m', 'd'),
                     (colDenss, 'columndensity', 'N'),
                     (turbulences,'turbulence','b'),
                     (ISRFs,'ISRF','i'),
                     (HMzs,'HMz','H'),
                     (opticalFactors,'opticalFactor','o'),
                     (starburst99fns,'starburst99fn','g'),
                     (starburst99logages, 'starburst99logage', 'h'),
                     (luminosities, 'luminosity', 'L'),
                     (radii, 'radius','r'),
                     (logXis,'logXi','X'),
                     (dlogBreakFluxes,'dlogBreakFlux','y'),
                     (densityPowerLawIndices,'densityPowerLawIndex','j'),
                     (densityPowerLawScales,'densityPowerLawScale','k'),
                     (AVs,'AV','A'),
                     (dustType,'dustType','O'),
                     (nuFnus,'nuFnu','v'),
                     (profile_fns,'profileFileName','f')
                     ]
        variables = sorted(variables, key=lambda x: x[2] not in notCartesian)

        if len(notCartesian)>1: 
            vals = [x[0] for x in variables if x[2] in notCartesian]
            assert(len(u.mifkad(u.lens(vals)))==1)
            keys = unzip(vals)
        else: keys = None
        keys = u.cartesianMultiplication([x[0] for x in variables if x[2] not in notCartesian], init=keys)
        inputnames = [x[1] for x in variables]	
        modelnames = [x[2] for x in variables]
        for k in keys:
            modelname = gridname+string.join([self.tostr(modelnames[i],k[i],nDigitsDic) for i in rl(k) if len(variables[i][0])>1 or (modelnames[i] in forCompatibility)],'')
            kwargs = dict([(inputnames[i], k[i]) for i in rl(k)])
            #patch for backward compatibility
            if not oldkey: dicKey = tuple([self.tokey(k[i]) for i in rl(k) if len(variables[i][0])>1 or variables[i][2] in forCompatibility])  
            else: dicKey = tuple([self.tokey(k[i]) for i in rl(k) if len(variables[i][0])>1])  
            self[dicKey] = Model(modelname,modeldir=self.name+'/'+modelname,Uisxi=Uisxi,useIR=useIR,nuFnu_freq=nuFnu_freq,	                         
                                 GrovesParams=self.GrovesParams,setPresIoniz=setPresIoniz,emissionLines=emissionLines,
                                 emittedRadiationPressure=emittedRadiationPressure,xspecRef=xspecRef,stopT=stopT,stop_pfrac=stop_pfrac,isGlobule=isGlobule,max_nDecs=max_nDecs,
                                 HighEnergyPhotons=HighEnergyPhotons,iterate=iterate,ispdr=ispdr,CRB=CRB,dustyNotDepleted=dustyNotDepleted,zCMB=zCMB,
                                 savelevel=savelevel,maxIterations=maxIterations,runC10=runC10,neutralColumn=neutralColumn,dereddenedAGN=dereddenedAGN,firstLoad=firstLoad,
                                 ionizationSpectrumLoadEnergies=ionizationSpectrumLoadEnergies,logBreakFreq=logBreakFreq,saveTmap=saveTmap,densityPowerLawGivenAsAMD=densityPowerLawGivenAsAMD,drmax=drmax,       
                                 nmaps=nmaps,**kwargs)
        self.keynames = [x[1] for x in variables if len(x[0])>1]

    def writeinputfiles(self):
        [self[k].writeInputFile() for k in self]
    def loadAll(self, requireNoWarnings=True, multipleProcs=7,cumulative=False,allowFail=False):
        for k in u.Progress(self):
            if not self[k].loaded or not cumulative:
                if not allowFail: 
                    self[k].loadAll(requireNoWarnings=requireNoWarnings)
                else:
                    try:
                        self[k].loadAll(requireNoWarnings=requireNoWarnings)
                    except AssertionError:
                        continue
    def run(self, multipleProcs=7,sortFunc = None,filterFunc=u.noFilter):	    
        u.processinfo('Grid.run()')
        pool = multiprocessing.Pool(processes=multipleProcs,maxtasksperchild=1)
        skeys = filter(filterFunc, self.keys())
        print '# of models to run:', len(skeys)
        if sortFunc!=None: skeys = sorted(skeys, key=sortFunc)
        for k in skeys:
            self[k].CloudyStat = "currently running"
            pool.apply_async(runModel, (self[k],))
        return pool
    def getPlotter(self):
        return Plot(grid=self)
    def __call__(self,fname,requireNoWarnings=False,*args,**kwargs):
        vals = u.myDict([(k,self[k](fname,*args,**kwargs)) for k in u.Progress(self.keys()) if ((not requireNoWarnings) or self[k].CloudyOk())])
        return vals    
    def subdiccall(self,subdicfunc,fname,*args,**kwargs):
        return u.myDict([(k,self[k](fname,*args,**kwargs)) for k in self.keys() if subdicfunc(k)])
    def keys(self):
        ks = sorted(dict.keys(self))
        if len(ks[0])==1: return [x[0] for x in ks]
        return ks
    def keydict(self,k):
        d = dict([(self.keynames[i], k[i]) for i in rl(k)])
        for singlevals,name in (self.ns,'n'), (self.Us,'U'), (self.aEUVs,'aEUV'), (self.Zs,'Z'), (self.ps,'p'), (self.SEDs,'inputSEDdir'):
            if len(singlevals)==1: 
                d[name] = singlevals[0]
        return d
    #def __getitem__(self,k):
        #if type(k)==type(()): return dict.__getitem__(self,k)
        #if type(k)==type({}):
    def CloudyStatus(self):
        out([(k,self[k].CloudyStat) for k in self])
    def __getitem__(self,k):
        if type(k)==type((None,)):
            return dict.__getitem__(self,k)
        else:
            return dict.__getitem__(self,(k,))
    def dirFiles(self):
        return u.mifkad(self('nFiles').values())



    def addGrid(self, other,joinOnAttr):
        """assumes single value in joinOnAttr"""
        selfAttr = getattr(self,joinOnAttr)[0]
        otherAttr = getattr(other,joinOnAttr)[0]
        setattr(self,joinOnAttr,[selfAttr,otherAttr])
        for k in self.keys():
            newk = tuple([selfAttr]+list(k))
            self[newk] = self[k]
            self.pop(k)
        for k in other.keys():
            newk = tuple([otherAttr]+list(k))
            self[newk] = other[k]

    def griddir(self):
        return self.values()[0].griddir()
    def copyGrid(self,dest):
        os.mkdir(dest + self.griddir())
        self('copyModel',False,dest + self.griddir() + '/')
class Plot:
    def __init__(self, model=None,grid=None):
        self.m = model
        self.g = grid
    def plotIncidentSpectrum(self,newfig=True,showDust=True,showSED=False,showwl=False, clr='k',xls=(12,18.5)):
        if newfig: figure()	
        l = plot(log(self.m.nus(False)), u.log(self.m.incidentSpectrumnuFnus),c=clr)
        u.annotateLine(l[0], r'Incident', 13.5,rotate=False,xytext=(0,5))
        if hasattr(self.m, 'transmittedSpectrum'):
            l = plot(u.log(specnus), u.log(self.m.transmittedSpectrum),c='k')
            u.annotateLine(l[0], r'Transmitted', 14.5,rotate=False,xytext=(0,5))
        if showSED and not self.m.inputSED():
            sed = self.m.SED()	
            normind = searchsorted(specnus, 10**sed[0][1])
            norm = u.log(self.m.incidentSpectrumnuFnus[normind]) - (sed[1][1] + sed[0][1])	
            plot(sed[0], sed[0]+sed[1]+norm,c='k')    
        xlabel(r'$\nu$ (Hz)')
        ylabel(r'$\nu F_{\nu}\ (%s)$'%ld.sflux)
        axvline(u.log(3e18/912.),c='.5',ls=':')
        ylim(-1.2,3.3)
        if showwl:
            xlim(*xls)
            twiny()
            xlim(u.log(3e14/10**array(xls)))	    
        elif self.m.hasdust and showDust:
            twinx()
            l = plot(log(self.m.nus(False)), u.log(self.m.grainOpacities))
            u.annotateLine(l[0], r'$\sigma_{\rm dust}$', 15.6,rotate=False,xytext=(0,5))
            ylabel(r'opacity cm$^2$')
            xlim(*xls); ylim(-24.2,-19.7)


    def plotEmissionSpectrum(self,showLines=True,showIncident=True,newfig=True,norm=1.,showTotal=True,xunits='A'):
        if newfig: rtf=u.rtfig()
        mics = rydberg2Angstrom(array(self.m.spectrumEnergies))/1e4
        if xunits=='A': 
            f=1e4
            xlabel(r'$\lambda$ (\AA)')
        if xunits=='mic': 
            f=1.
            xlabel(r'$\lambda\ (\mu m)$')
        if showTotal: plot(mics*f, self.m.observedSpectrumnuFnus/norm)
        if showLines: plot(mics*f, self.m.lineSpectrumnuFnus/norm)
        if showIncident: plot(mics*f, self.m.incidentSpectrumnuFnus/norm)

        ylabel(r'$\nu F_\nu$')
        semilogx()
        rtf.xformat(arilogformatter)
        xlim(1e2*f,1e-4*f)

    def plotOverview(self,fromIonFront=True):
        maxind = len(self.m.depths)#self.m.ionizationFront()
        if fromIonFront: xs = self.m.localColumnDensity(islog=True)[self.m.ionizationFront()] - self.m.localColumnDensity(islog=True)
        else: xs = self.m.localColumnDensity(islog=True)
        figure()
        subplot(211)
        l = plot(xs, 10**self.m.Ts[:maxind],c='b')
        semilogy()
        u.annotateLine(l[0], 'T', 0.2,rotate=False,xytext=(0,5),xycoords='axes fraction')
        axhline(1e4,c='.5',lw=0.5)
        ylabel(r'K or cm$^{-3}$')	
        l = plot(xs, self.m.nH[:maxind],c='k',ls='--')
        u.annotateLine(l[0], r'$n_{\rm H}$', 0.4,rotate=False,xytext=(0,5),xycoords='axes fraction')
        l = plot(xs, self.m.ne[:maxind],c='k',ls='--')
        u.annotateLine(l[0], r'$n_{\rm e}$', 0.5,rotate=False,xytext=(0,5),xycoords='axes fraction')
        ylim(1,1e6)
        twinx()
        l = plot(xs, self.m.localU()[:maxind],c='k',ls='-.')
        u.annotateLine(l[0], r'$U$', 0.7,rotate=False,xytext=(0,5),xycoords='axes fraction')
        if hasattr(self.m,'nHII'):
            l = plot(xs, self.m.localUeffective()[:maxind]/3.12e-13*self.m.recombinationCoeffsB[:maxind],c='k',ls=':')
            u.annotateLine(l[0], r'$U_{\rm gas}$', 0.7,rotate=False,xytext=(0,-15),xycoords='axes fraction')
        semilogy()
        ylim(10**self.m.U*1e-5, 10**self.m.U*10)
        subplot(212)
        l = [plot(xs, p,c='.5') for p in (self.m.Pcorrect[:maxind], self.m.Pcurrent[:maxind], self.m.Ptotal[:maxind])][0]
        u.annotateLine(l[0], r'$P_{\rm total}$', 0.1,rotate=False,xytext=(0,5),xycoords='axes fraction')
        l = plot(xs, self.m.Pgas[:maxind],c='r',ls='--')
        u.annotateLine(l[0], r'$P_{\rm gas}$', 0.2,rotate=False,xytext=(0,5),xycoords='axes fraction')
        axhline(self.m.Pgas[0],c='k',ls=':')
        l = plot(xs, self.m.Prad[:maxind],c='r',ls=':')
        u.annotateLine(l[0], r'$P_{\rm rad}$', 0.3,rotate=False,xytext=(0,5),xycoords='axes fraction')	
        semilogy()
        ylabel(r'dynes cm$^{-2}$')
        ylim(min(self.m.Prad),max(self.m.Pgas * 10))
        xlabel(r'$N_{\rm H}$')

    def plotRecombination(self):
        rtf = rtfig()
        xs = self.m.localColumnDensity(islog=True)[self.m.ionizationFront()] - self.m.localColumnDensity(islog=True)
        l = plot(xs, self.m.ionizationRates,c='b')
        u.annotateLine(l[0], r'ground state lifetime $^{-1}$',0.5,rotate=False,xytext=(0,5),xycoords='axes fraction')
        ylabel(r's$^{-1}$')
        semilogy()
        ax2=twinx()
        l = plot(xs, self.m.recombinationCoeffsTotal,'--',c='k')
        u.annotateLine(l[0], r"$\alpha$",0.3,rotate=False,xytext=(0,5),xycoords='axes fraction')
        l = plot(xs, self.m.recombinationCoeffsB,'-',c='k')
        u.annotateLine(l[0], r"$\alpha_{\rm B}$",0.3,rotate=False,xytext=(0,5),xycoords='axes fraction')
        ylabel(r'cm$^3$ s$^{-1}$')
        semilogy()
        xlabel(r'$N_{\rm H}$')


    def plotIonizationStates(self,iModel,elements = ('H','He','O','Ne','Fe'),isRight=(0,0,1,0,1),isTop=(0,0,0,1,1),fig=None,isdusty=True):
        if fig==None: fig=figure()

        cs = itertools.cycle('kbcrgy')	
        #xs = self.m.IntegratedAbsorbedEnergy() / self.m.totalIonizingEnergy()
        totalheating = array([(self.m.ddepths*10**self.m.heating)[:i+1].sum() for i in rl(self.m.depths)])
        xs = totalheating / totalheating[-1]
        #xs = self.m.IntegratedAbsorbedEnergy() / self.m.totalIonizingEnergy()
        xls = 0.0,1.0
        #xs = self.m.localColumnDensity(islog=True)*self.m.meanSigmaDust()	    
        for iElement, element in enumerate(elements):
            col = isRight[iElement]
            row = iModel + 3*isTop[iElement]
            axes([0.05+0.5*col,0.1+0.125*(5-row)+0.1*(row<3),0.4,0.125])

            showionizations = {'Fe': range(10)+[11,16,19,24]}.get(element,range(IonizationEnergiesDic[element][0]+1))
            lss = itertools.cycle(['-','--','-.',':'])
            attr = getattr(self.m, element + 'state')

            #subplot(6,2,isps[iElement] + 2*iModel)
            s,e = searchsorted(xs, xls)
            for i in rl(attr):
                if i in showionizations:
                    l = plot(xs, 10**attr[i],c=cs.next(),lw=0.5,ls=lss.next())
                    if max(10**attr[i][s:e])>0.1:
                        u.annotateLine(l[0], u.roman[i+1], min(max(u.getlinemax(l[0]),xls[0]+0.05),xls[1]-0.15),rotate=False,xytext=(0,1),fontsize=7)	     
            ylim(0,1.2)
            gca().yaxis.set_major_locator(matplotlib.ticker.MultipleLocator(0.5))
            xlim(*xls)
            if col==1: 
                gca().yaxis.set_label_position('right')
                gca().yaxis.set_ticks_position('right')		
            if iElement==1: ylabel('H,He')
            else: ylabel(element)
            if iModel!=1: ylabel('')
            if iModel==2: xlabel(r'$f_{\rm E}$')
            else: gca().xaxis.set_major_locator(matplotlib.ticker.NullLocator())

            #semilogx()
    def plotIonizationStates2(self,element,xls,showlabels,rightlabels=[],nolabels=[],usetau=False,showT=False,ls='-'):	
        cs = itertools.cycle('kbcrgy')	
        if usetau:
            maxind =len(self.m.ddepths)
            dtaus = self.m.nH*self.m.ddepths*array([self.m.sigmaActualIonizing(i) for i in rl(self.m.ddepths)])
            xs = array([sum(dtaus[:i]) for i in range(1,maxind+1)])	
        else:
            maxind = self.m.ionizationFront()
            xs = self.m.depths[maxind] - self.m.depths[:maxind]
        attr = getattr(self.m, element + 'state')
        nolabellinedata = []
        for i in range(IonizationEnergiesDic[element][0]+1):
            c=cs.next()
            l = plot(xs, 10**attr[i][:maxind],c=c,lw=(0.75,2)[presentation],ls=ls)
            if showlabels and not i in nolabels and max(10**attr[i][:maxind])>0.1:
                if i>2: lab = r'%s$^{%d+}$'%(element,i)
                if i==2: lab = r'%s$^{++}$'%(element)
                if i==1: lab = r'%s$^{+}$'%(element)
                if i==0: lab = r'%s$^{0}$'%(element)
                u.annotateLine(l[0], lab, 
                               min(max(u.getlinemax(l[0]),xls[1-usetau]*1.5),xls[usetau]/1.5)/(1.,1.1)[i in rightlabels],rotate=False,xytext=(0,1),
                             fontsize=(10,16)[presentation],ha=('left','right')[i in rightlabels])
            else:
                nolabellinedata.append((c,min(max(u.getlinemax(l[0]),xls[1]*1.5),xls[0]/1.5)))
        semilogx()
        ylim(0,1.2)
        ylabel('ion fraction')	
        gca().yaxis.set_major_locator(matplotlib.ticker.MultipleLocator(0.5))
        if showT:
            ca = gca()
            twinx(ca)
            plot(xs, 10**self.m.Ts[:maxind],c='k',lw=0.5,ls='--')
            ylim(7000,1e6)
            ylabel(r'$T\ ({\rm K})$')
            loglog()
            sca(ca)
        if presentation: gca().xaxis.set_major_formatter(mylogformatter)
        else: gca().xaxis.set_major_formatter(arilogformatter)
        xlim(*xls)
        if usetau: xlabel(r'$\tau$')
        else: xlabel(r'$\log (x_{\rm f}-x)$    [cm]')

        if presentation: labelsize(False)
        return nolabellinedata



    def plotAbsorption(self,inds=(0,-1)):
        s,e = self.m.ionizationEnergyInds()
        initSpectrum = self.m.incidentSpectrumnuFnus[s:e]

        figure()
        plot(self.m.nus(), initSpectrum)
        for i in inds:
            plot(self.m.nus(), self.m.spectra[i])
        semilogx()

        totalIonizingEnergy = (initSpectrum / self.m.nus() * self.m.dnus()).sum()
        print self.m.IntegratedAbsorbedEnergy()[0]  / totalIonizingEnergy
        print self.m.IntegratedAbsorbedEnergy()[-1]  / totalIonizingEnergy

    def plotLines(self,fromface=False,showEmergent=True,ax=None,wls=[],xl=(1e15,1e20),yl=(0,1e16),cs='cbyrm',showtwinx=False):  #divide by total ionization energy
        if ax==None: rtfig()
        if fromface: 
            xs = self.m.localColumnDensity(islog=True)
            fact1 = self.m.nH
            maxind = len(self.m.depths)
        else: 
            maxind = self.m.ionizationFront()
            xs = (self.m.depths[maxind] - self.m.depths)[:maxind]
            fact1 = 1./xs	
        #xshift = [0.1,1.5,1.,0.8,1]
        if ax==None: subplot((1,2)[showEmergent],1,1)
        else: sca(ax)
        semilogx()
        for i in rl(wls):
            if type(wls[i]) in (type(1),type(1.)):
                fact2 = 1.#,50.)[wls[i]==6087 and not fromface] #!!!!!!!!
                l = plot(xs, (10**self.m.linesdicE[wls[i]]*fact2 )[:maxind] / fact1,c=cs[i%len(cs)])
                lbl = emissionLinesDic[wls[i]].label(True)
            else:
                l = plot(xs, (10**self.m.linesdicF[wls[i][0]]+10**self.m.linesdicF[wls[i][1]])[:maxind]/ fact1,c=cs[i%len(cs)])
                lbl = emissionLinesDic[wls[i][0]].label(True)
            #u.annotateLine(l[0], lbl, u.getlinemax(l[0]),rotate=False,xytext=(0,5),ha='center',fontsize=10)
        xrays = array([self.m.getBandLuminosity(24.8,6.2,x) for x in range(maxind)])/fact1
        l = tplot([(xs[i],xrays[i]) for i in rl(xs) if not isnan(xrays[i]) and not isinf(xrays[i])],'-',c='k')
        axis('tight')
        ylim(yl)

        xlim(xl)
        if fromface: ylabel(r'$j/n_{\rm H}$ (erg s$^{-1}$)')
        else: ylabel(r'$(x_{\rm f}-x) \cdot j$ (erg s$^{-1}$ cm$^{-2}$)')
        if fromface: xlabel(r'$N_{\rm H}$')
        else: xlabel(r'$x_{\rm f} - x$ (cm)')


        ax=twinx()
        semilogy()
        if showtwinx:
            l = plot(xs, 10**self.m.Ts[:maxind],c='.5',ls='--')
            u.annotateLine(l[0], 'T', xl[0]/(1.5,3)[xl[0]==3e19],rotate=False,xytext=(0,5))
            ylabel(r'$T$(K)')
        else:
            gca().yaxis.set_major_formatter(matplotlib.ticker.NullFormatter())
        ylim(3e3,1e6)
        xlim(xl)


        if showEmergent:
            subplot(212); semilogx()
            for i in rl(wls):
                if type(wls[i])==type(1):
                    l = plot(xs, (self.m.linesdicE[wls[i]] - self.m.linesdicF[wls[i]])[:maxind],c=cs[i])
                    lbl = emissionLinesDic[wls[i]].label(True)
                else:
                    l = plot(xs, (u.log(10**self.m.linesdicE[wls[i][0]]+10**self.m.linesdicE[wls[i][1]]) -
                                  u.log(10**self.m.linesdicF[wls[i][0]]+10**self.m.linesdicE[wls[i][1]]))[:maxind],c=cs[i])
                    lbl = emissionLinesDic[wls[i][0]].label(True)
                u.annotateLine(l[0], lbl, (0.2+0.05*i),rotate=False,xytext=(0,5),xycoords='axes fraction')
            ylabel(r'Emergent to Intrinsic')
            xlim(xl)
            if fromface: xlabel(r'$N_{\rm H}$')
            else: xlabel(r'$x_{\rm f} - x$ (cm)')


    def plotAveragesPerLine(self):
        rtfig()
        cs = 'kbrcky'
        linesE = [self.m.HbsE, self.m.o3sE, self.m.o1sE, self.m.n2sE, self.m.hasE, u.log(10**self.m.s26716sE+10**self.m.s26731sE)]
        linesT = [self.m.HbsT, self.m.o3sT, self.m.o1sT, self.m.n2sT, self.m.hasT, u.log(10**self.m.s26716sT+10**self.m.s26731sT)]
        linenames = r'H$\beta$', '[O III]', '[O I]', '[N II]', r'H$\alpha$', '[S II]'
        for j in range(2):
            subplot(2,1,j+1)
            semilogx()
            for i in rl(linesE):
                weights = 10**linesE[i] 
                absorbedPhotons = lambda iDepth, spec: array(smooth(spec[iDepth] - spec[iDepth+1], 25))
                weightedAbsorbedPhotons = u.sumlst([absorbedPhotons(iDepth, self.m.spectra)*weights[iDepth] for iDepth in rl(self.m.depths,-1)])
                if j==0: 
                    l = plot(self.m.ionizationEnergies(), weightedAbsorbedPhotons / 10**linesT[i])
                    u.annotateLine(l[0], linenames[i], self.m.ionizationEnergies()[weightedAbsorbedPhotons.argmax()],rotate=False)
                else:
                    weightedAbsorbedPhotonsHb = u.sumlst([absorbedPhotons(iDepth, self.m.spectra)*10.**linesE[0][iDepth] for iDepth in rl(self.m.depths,-1)]) / 10**linesT[0]
                    l = plot(self.m.ionizationEnergies(), weightedAbsorbedPhotons / 10**linesT[i] / weightedAbsorbedPhotonsHb)
            #ylim(0,1.1)
            xlabel(r'$h\nu$ (Rydberg)')
            if j==0: ylabel(r'$-\frac{{\rm d}\nu F_\nu(x)}{{\rm d} x}$ averaged over line emission ($%s\ {\rm cm}^{-1}$)'%sergs)
            else:    ylabel(r'as above, normalized to $H\beta$')





    def plotEnergetics(self,ncoolants=1,xls=(1e15,1e20)):
        cs = 'kbcrgykb'
        maxind = len(self.m.depths)#searchsorted(self.m.depths, self.m.depths[self.m.ionizationFront()])
        xs = self.m.depths
        figure()
        subplot(211)
        #semilogx()
        prcnames = ('H  1', 'He 1', 'He 2', 'O  1', 'GrnP')	
        ls = [plot(xs, self.m.getEnergyProcess(prcnames[i],False)[:maxind],c=cs[i]) for i in rl(prcnames)]
        [u.annotateLine(ls[i][0], prcnames[i], max(min(u.getlinemax(ls[i][0]),xs[-2]),xs[1]),rotate=False,xytext=(0,5)) for i in rl(prcnames)]
        text(0.05,0.9,'heating',transform=gca().transAxes)
        ylim(0,1.1)
        ylabel('Fraction of Total')
        xlim(*xls)

        subplot(212)
        #semilogx()
        prcnames = unique(u.sumlst([self.m.getMainCoolants(zone,ncoolants) for zone in rl(self.m.depths)])+['GrnP']) 
        print "coolants:", prcnames
        ls = [plot(xs, self.m.getEnergyProcess(prcnames[i])[:maxind],'--',c=cs[i%len(cs)]) for i in rl(prcnames)]
        [u.annotateLine(ls[i][0], prcnames[i], max(min(u.getlinemax(ls[i][0]),xs[-2]),xs[1]),rotate=False,xytext=(0,5),ha='center') for i in rl(prcnames)]	
        xl = xlim()
        axis('tight')
        ylim(0, 1.1)
        xlim(xl)
        text(0.05,0.9,'cooling',transform=gca().transAxes)
        xlabel(r'$l$')
        ylabel('Fraction of Total')
        xlim(*xls)

    def plotSpecs(self,s=0,d=26,xl=(1,100)):
        figure()
        iDepths = u.arange(-2,-len(self.m.totalKappas)+1,-d)
        subplot(411)
        [plot(self.m.ionizationEnergies(), self.m.totalOpacities()[iDepth]*self.m.N_Stromgren(),label=r'$l/l_{\rm S}=%.2f$'%self.m.normedDepth()[iDepth]) for iDepth in iDepths]
        plot(self.m.spectrumEnergies, self.m.grainOpacities*self.m.N_Stromgren(),c='k',ls='--')
        loglog()
        l = legend(loc = 'upper right')
        l.draw_frame(False)
        axhline(1,c='k',ls=':')
        ylabel(r'$\kappa l_{\rm S}$')
        xlim(*xl)

        subplot(412)
        for iDepth in iDepths:
            plot(self.m.ionizationEnergies(), smooth( self.m.spectra[iDepth],10))
        loglog()
        ylim(1e-3,2)
        ylabel(r'Incident Spectrum $F_{\nu}$')
        xlim(*xl)

        subplot(413)
        for iDepth in iDepths:
        #[plot(self.m.specEnergies, (self.m.spectra[iDepth] - self.spectra[iDepth+1]) / (self.m.normedDepth()[iDepth+1]-self.m.normedDepth()[iDepth]) ) for iDepth in iDepths[1:]]
            absorbedPhotons = array(smooth ( (self.m.spectra[iDepth] - self.m.spectra[iDepth+1]) / self.m.ionizationEnergies(), 10))
            normAbsorbedPhotons = absorbedPhotons / absorbedPhotons.max()
            plot(self.m.ionizationEnergies(), normAbsorbedPhotons)
        #loglog()
        semilogx()
        ylim(0,1)    
        ylabel(r'frac absorbed $\nu Q_{\nu}$')
        xlim(*xl)

        subplot(414)
        for iDepth in iDepths:
            absorbedPhotons = array(smooth ( (self.m.spectra[iDepth] - self.m.spectra[iDepth+1]) , 10))
            normAbsorbedPhotons = absorbedPhotons / absorbedPhotons.max()
            plot(self.m.ionizationEnergies(), normAbsorbedPhotons)
        semilogx()
        ylim(0,1)    
        ylabel(r'frac absorbed $\nu F_{\nu}$')    
        xlabel(r'$h\nu$ (Rydberg)')
        xlim(*xl)

    def plotSpecs2(self,highT=4.5):
        figure()
        iHighT = 0#len(self.m.Ts)-1-searchsorted(self.m.Ts[::-1],4.5)
        iDepths = rl(self.m.depths)[iHighT+1::50]
        print iHighT
        ignoreHighT = lambda Ns,iDepth,iHighT=iHighT: Ns[iDepth] - Ns[iHighT]
        for i in rl(iDepths):
            iDepth = iDepths[i]
            l = plot(self.m.ionizationEnergies(), smooth(self.m.spectra[iDepth],20),c='kbcry'[i%5])
            nus0 = Model.nus0dic[('H',0)]
            taus = ( ignoreHighT(self.m.localColumnDensity('nHI',False),iDepth)                       *Model.nus0Opacdic[('H', 0)]*(self.m.nus()/    nus0 )**(-3.),
                     ignoreHighT(self.m.localColumnDensity('Hestate',True,1,0.098*self.m.Hescale()),iDepth)*Model.nus0Opacdic[('He',1)]*(self.m.nus()/(4.*nus0))**(-3.)
                     *(self.m.nus()>4*nus0),
                    self.m.localColumnDensity()[iDepth]*m.grainOpacities[self.m.ionizationEnergyInds()[0]:self.m.ionizationEnergyInds()[1]] )
            iHe = [j for j in rl(taus[1]) if taus[1][j]>0.][0]
            print '%.1f %.1f %.1f'%(taus[0][0]/2.5, taus[1][iHe]/2.5, taus[2][0]/2.5)
            plot(self.m.ionizationEnergies(), self.m.spectra[0]*exp(-(taus[0]+taus[1]+taus[2])),c='kbcry'[i%5],ls='--')
            #i3 = searchsorted(self.m.ionizationEnergies(),3.3)
            #i17 = searchsorted(self.m.ionizationEnergies(),17.)
            #print u.log((self.m.spectra[0] * exp(-(taus[0]+taus[1]+taus[2])) / smooth(self.m.spectra[iDepth],10))[i3])  / (m.localColumnDensity('nHI',False)[iDepth] * Model.nus0Opacdic[('H', 0)]*(self.m.nus()/    nus0 )[i3]**(-3.))
            #print u.log((self.m.spectra[0] * exp(-(taus[0]+taus[1]+taus[2])) / smooth(self.m.spectra[iDepth],10))[i17]) / (m.localColumnDensity('Hestate',True,1,0.098*m.Hescale())[iDepth]*Model.nus0Opacdic[('He',1)]*(self.m.nus()/(4.*nus0))[i17]**(-3.))
            u.annotateLine(l[0],'%.2g'%self.m.localColumnDensity()[iDepth],3.9,xytext=(-5,15),rotate=False)
        loglog()
        ylim(1e-3,2)
        ylabel(r'Incident Spectrum $F_{\nu}$')
        xlim(1,100)
        ylim(1e-4,30.)

    def plotAll(self):
        self.plotIncidentSpectrum()
        self.plotOverview()
        self.plotRecombination()
        self.plotIonizationStates()
        self.plotLines()
        self.plotEnergetics()
        self.plotSpecs()
    def BPT(self,cfslopes=[],axlist=None,c='k',connectaEUV=False,aEUVinds=None,Zinds=None,Uinds=None,ngammas=None,SEDinds=None,labelset=None,
            maxngammas=[5.5],minngamma=0.,plotngamma=False,ploteta=False,showGroves=True,keyset=1,connectEtas=[]):
        #rcParams['axes.labelsize'] = 'medium'


        if self.g!=None:
            g = self.g
            if aEUVinds == None: aEUVinds = rl(g.aEUVs)
            if Zinds    == None: Zinds = rl(g.Zs)
            if Uinds    == None: Uinds = rl(unique(g.Us))
            if SEDinds  == None: SEDinds = rl(unique(g.SEDs))
            if len(maxngammas)==1: 
                maxngamma = maxngammas[0]
                if ngammas  == None: ngammas = [ng for ng in sorted(unique([(g.keydict(k)['n']+g.keydict(k).get('U',g.Us[0])) 
                                                                            for k in g.keys()]).tolist()) if minngamma<=ng<=maxngamma]
            if axlist==None: 
                #rcParams['xtick.labelsize'] = 14
                #rcParams['ytick.labelsize'] = 14
                axlist = figBPT_overKewley(None,False)
            #rcParams['font.size']=14
            if showGroves: Groves04.showGrid([-1.99,-1.7,-1.4,-1.21],axlist,[0,1,2],c='k',zorder=10,showLabels=False)
            subplots_adjust(top=0.9)
            ratiosDic={}
            for SEDind in SEDinds:
                SED = g.SEDs[SEDind]	
                for Uind in Uinds:
                    U = unique(g.Us)[Uind]	    
                    for Zind in Zinds:
                        Z = sorted(g.Zs)[Zind]
                        if connectaEUV:
                            ratios = [self.g[k].ratios() for k in self.g.keys() if g.keydict(k)['Z']==Z and g.keydict(k).get('U',g.Us[0])==U
                                      and (g.keydict(k)['n'] + g.keydict(k).get('U',g.Us[0]) in ngammas) and g.keydict(k).get('inputSEDdir',None)==SED]			    
                            [axlist[i].plot([u.log(ratio[i+3]) for ratio in ratios], [u.log(ratio[2]) for ratio in ratios],'.-',
                                            c='k',lw=2,ls=('-','--',':')[Uind%3]) for i in rl(axlist)]
                            #if Zind==0: [axlist[0].text(u.log(ratios[r][3])-(0.,0.1)[r==0], u.log(ratios[r][2])+0.05,('',r'S13, $\alpha_{\rm ox}=$')[r==0]+r'$%.1f$'%g.aEUVs[r]) for r in rl(ratios)]
                        else:
                            for aEUVind in aEUVinds:
                                a = sorted(g.aEUVs)[aEUVind]
                                if len(cfslopes)==0:
                                    ratios = [self.g[k].ratios() for k in self.g.keys() if 
                                              g.keydict(k)['aEUV']==a and g.keydict(k)['Z']==Z and g.keydict(k).get('U',g.Us[0])==U
                                              and g.keydict(k)['n'] + g.keydict(k).get('U',g.Us[0]) in ngammas and g.keydict(k)['inputSEDdir']==SED]
                                    indRatio = 2
                                    if Zind==1 and aEUVind==0 or plotngamma:
                                        s = lambda r,b=plotngamma: [('',r'$n_\gamma =$')[r==0] + r'$10^{%.1f}$'%ngammas[r], 
                                                                    ('',r'$\u.log\ n_\gamma=$')[r==len(ngammas)-1] + r'$%.1f$'%ngammas[r]][b]
                                        [[axlist[i].text(u.log(ratios[r][i+1+indRatio]), u.log(ratios[r][indRatio])+0.03,s(r),
                                                         color=colors[aEUVind]) for r in rl(ratios) 
                                          if axlist[i].get_xlim()[0]<u.log(ratios[r][i+1+indRatio])<axlist[i].get_xlim()[1]] for i in rl(axlist)]
                                    [axlist[i].plot([u.log(ratio[i+1+indRatio]) for ratio in ratios], [u.log(ratio[indRatio]) for ratio in ratios],'.-',
                                                    c=colors[Zind],lw=lws[aEUVind],ls=('-','--',':')[Uind%3]) for i in rl(axlist)]				
                                else:
                                    for maxngamma in maxngammas:
                                        gms = [GasModel(g, a, Z, cfslope,logngamma0=maxngamma,logngmin=minngamma,keyset=keyset,SED=SED) for cfslope in cfslopes]
                                        ratios = [gm.ratios() for gm in gms]					
                                        if len(aEUVinds)>1: ratiosDic[a] = ratios
                                        if len(Zinds)>1: ratiosDic[Z] = ratios
                                        indRatio = 0
                                        if Zind==1 and aEUVind==0 or ploteta:
                                            [[axlist[i].text(u.log(ratios[r][i+1+indRatio]), u.log(ratios[r][indRatio])+0.03,
                                                             ('',r'$\eta =$')[r==0] + r'$%.1f$'%cfslopes[r],
                                                             color=colors[aEUVind]) for r in rl(ratios)  
                                              if u.iround(cfslopes[r],0.1) == u.iround(cfslopes[r]) and axlist[i].get_xlim()[0]<u.log(ratios[r][i+1+indRatio])<axlist[i].get_xlim()[1]] for i in rl(axlist)]
                                        [[axlist[i].plot([u.log(ratio[i+1+indRatio]) for ratio in ratios], 
                                                         [u.log(ratio[indRatio])     for ratio in ratios],'-s'[m],mfc='w',
                                                        c='k',lw=0.6,ls='-',ms=2) for i in rl(axlist)]
                                         for m in range(2)]
            for eta in connectEtas:
                ind = cfslopes.index(eta)
                vals = [x[1][ind] for x in sorted(ratiosDic.items())]
                [[axlist[i].plot([u.log(val[i+1+indRatio]) for val in vals], 
                                 [u.log(val[indRatio])     for val in vals],'-s'[m],mfc='w',
                                 c='.3',lw=0.5,ls='-',ms=2) for i in rl(axlist)] for m in range(2)]	    


        else:
            if axlist==None: axlist = figBPT_overKewley(None,False)
            Groves04.showGrid([-1.99,-1.7,-1.4,-1.21],axlist,[0,1,2],c='k',zorder=10)
            subplots_adjust(top=0.9)	    
            ratios = self.m.ratios()
            [axlist[i].plot([u.log(ratios[i+3])], [u.log(ratios[2])],'*',ms=16,c=c) for i in rl(axlist)] 
        if labelset==1:
            m = self.g[(-1.2,2.)]
            axlist[0].text(0.05,0.92,r'$n_\gamma = 10^{%.1f}\ {\rm cm}^{-3}$'%(m.n+m.U),transform=axlist[0].transAxes,fontsize=8)
            axlist[0].text(0.05,0.87,r'$n_{\rm f} = 10^{%.1f}$'%(log(m.nH)[m.ionizationFront()]),transform=axlist[0].transAxes,fontsize=8)
            axlist[0].text(-1.8,0.4,r'$Z = 0.5 ~ Z_\odot$',fontsize=8)
            axlist[0].text(0.1,1.15,r'$Z = 2 ~ Z_\odot$',fontsize=8)
            #axlist[0].text(-1.85,0.88,r'G04, $\alpha_{\rm ox} = -2$')
            [axlist[0].text((-1.8,-1.6,-1.4,-1.2)[ia],(0.95,0.75,0.6,0.45)[ia],('',r'$\alpha_{\rm ox}$')[ia==0]+r'$%.1f$'%((-2,-1.7,-1.4,-1.2)[ia]),fontsize=8) for ia in range(4)]
        if labelset==2:
            axlist[0].text(-1.8,0.35,r'$Z = 0.5$')
            axlist[0].text(0.1,1.3,r'$Z = 2$')	
            axlist[0].text(-0.5,1.2,r'$\alpha_{\rm ox} = -2$',color='k')
            [axlist[0].text((-0.1,0.3)[ia],(1.2,1.15)[ia],r'$%.1f$'%((-1.6,-1.2)[ia]),color='br'[ia]) for ia in range(2)]
            axlist[0].text(-0.05,1.1,r'$U_0 = 10^{0}$',color='r')
            [axlist[0].text((0.2,0.25)[iU],(1.05,1.02)[iU],r'$10^{%d}$'%((2,4)[iU]),color='r') for iU in range(2)]
            axlist[0].text(-1,0.5,r'$n_\gamma = 10^{5}$',color='r')
            axlist[0].text(0.,0.9,r'$n_\gamma = 10^3,\ 10^1$',color='r')
        if labelset in (3,4,6):
            axlist[0].text(0.1,0.95,r'$Z = %.1f Z_\odot$'%self.g.Zs[Zinds[0]],transform=axlist[0].transAxes)
            axlist[0].text(0.1,0.92,r'$\alpha_{\rm ox} = %.2f $'%self.g.aEUVs[aEUVinds[0]],transform=axlist[0].transAxes)
        if labelset in (4,5,6):
            axlist[0].text(0.03,0.12,r'$n_{\rm \gamma;\ min} = 10^{%.1f}\ {\rm cm}^{-3}\ \ (r_{\rm max} = %s\ L^{0.5}_{45}\ {\rm kpc})$'%(minngamma,(2,1.3)[minngamma==0.5]),transform=axlist[0].transAxes,fontsize=8)
            axlist[0].text(0.03,0.05,r'$n_{\rm \gamma;\ max} = 10^{%.1f}\ {\rm cm}^{-3}\ \ (r_{\rm min} = 4\ L^{0.5}_{45}\ {\rm pc})$'%maxngamma, transform=axlist[0].transAxes,fontsize=8)
        if labelset==5:
            [axlist[0].text((0.35,-0.6,0.,0.15)[aEUVind],(1.25,1.2,1.1,1.)[aEUVind],
                            ('',r'$\alpha_{\rm ox}=$')[aEUVind==1]+r'$%.1f$'%sorted(g.aEUVs)[aEUVind],color=colors[aEUVind],fontsize=8) for aEUVind in aEUVinds]
        if labelset==6:
            [axlist[0].text((0.3,0.45,0.48,0.55,0.55)[SEDind],(1.1,1.05,0.98,0.85,0.55)[SEDind],
                            ('',r'$N_{\rm dust}=$')[SEDind==0]+r'$10^{%s}$'%sorted(g.SEDs)[SEDind][-4:],color='k') for SEDind in SEDinds]    
        if labelset==7:
            [axlist[0].text((-1.75,-0.05)[Zind],(0.6,1.2)[Zind],r'$Z = %.1f ~ Z_\odot$'%self.g.Zs[Zind]) for Zind in Zinds]


        gcf().canvas.draw()
        #rcParams['axes.labelsize'] = 'x-large'
        return axlist
    def Allen(self,observed,aEUVinds=None,Zinds=None,cfslopes=[],maxngammas=[5.5],minngammas=[0.],deredden=False,sortby='lambda'):
        g = self.g	
        m = g.values()[0]
        if aEUVinds == None: aEUVinds = rl(g.aEUVs)
        if Zinds    == None: Zinds = rl(g.Zs)	
        figure(); rcParams['font.size']=12
        goodLinesInds = [iLine for iLine in rl(m.AllenLines) if m.AllenLines[iLine]!=None and type(m.AllenLines[iLine])!=type([])] #High Balmer series and unidentified Fe V line
        if sortby=='lambda':    sortedLineInds = goodLinesInds
        if sortby=='ncrit':     sortedLineInds = sorted(goodLinesInds, key=lambda i: observed.lines[i].ncrit)
        if sortby=='ionenergy': sortedLineInds = sorted(goodLinesInds, key=lambda i: observed.lines[i].ionenergy)
        if sortby=='strength':  sortedLineInds = sorted(goodLinesInds, key=lambda i: (observed.lines[i].observed,observed.lines[i].dereddened)[deredden][observed.inddic['a2']])
        for Zind in Zinds:
            Z = sorted(g.Zs)[Zind]
            for aEUVind in aEUVinds:
                a = sorted(g.aEUVs)[aEUVind]
                gms = [GasModel(g, a, Z, cfslope,logngamma0=maxngamma,logngmin=minngamma) for cfslope in cfslopes for maxngamma in maxngammas for minngamma in minngammas]
                for iCol in rl(sortedLineInds):
                    iLine = sortedLineInds[iCol]
                    lums = [gm.lineEmission(iLine,isAllen=True) / gm.lineEmission('Hb' ) for gm in gms]
                    plot([iCol+0.25+0.5*iSlope/len(lums) for iSlope in rl(lums)], lums,'.-',c='kbr'[aEUVind])
                    if not deredden: val = lambda l: l.observed
                    else: val = lambda l: l.dereddened
                    [text(iCol+0.5,val(observed.lines[iLine])[observed.inddic[n]],n[0],fontsize=16) for n in ['a2','cyg'] if val(observed.lines[iLine])[observed.inddic[n]]!=None]
        for iCol in rl(sortedLineInds):
            if iCol%2==0: continue
            pbxs, pbys = poly_between([iCol,iCol+1], 1e-3,1e2)
            fill(pbxs, pbys, facecolor='.9',edgecolor='w')			    
        xticks(array(rl(sortedLineInds))+0.5, [observed.lines[iLine].xtick() for iLine in sortedLineInds],fontsize=12)
        semilogy()
        xlim(0,len(sortedLineInds))
        ylim(1e-3,40)
        ylabel(r'$L({\rm line}) / L({\rm H}\beta)$')
        text(0.1,0.95,r'$Z = %.1f Z_\odot$'%g.Zs[Zinds[0]],transform=gca().transAxes,fontsize=16)
        if len(minngammas)==1: text(0.1,0.92,r'$n_{\rm \gamma;\ min} = 10^{%.1f}\ {\rm cm}^{-3}\ \ (r_{\rm max} = %s\ L^{0.5}_{45}\ {\rm kpc})$'%(minngammas[0],(2,1.3)[minngamma==0.5]),transform=gca().transAxes,fontsize=16)
        if len(maxngammas)==1: text(0.1,0.89,r'$n_{\rm \gamma;\ max} = 10^{%.1f}\ {\rm cm}^{-3}\ \ (r_{\rm min} = 4\ L^{0.5}_{45}\ {\rm pc})$'%maxngammas[0], transform=gca().transAxes,fontsize=16)
        if len(cfslopes)==1:   text(0.1,0.86,r'$\frac{\rm d\Omega}{\rm d \ln r} = r^{%.1f}$'%cfslopes[0], transform=gca().transAxes,fontsize=16)



    def BPTvsNgAoxZ(self,ratiosdic):
        g = self.g
        #ratiosdic = dict([(k, g[k].ratios()) for k in lrgrid])

        ncrits = [None,None,5.8,4.9,3.4,6.3]
        labels = [r'$Q_{{\rm H}\alpha} / Q_{\rm ion}$',
                  r'H$\alpha$/H$\beta$',
                  r'[OIII]/H$\beta$',r'[NII]/H$\alpha$',r'[SII]/H$\alpha$',r'[OI]/H$\alpha$']
        ylims  = [None,None,0.1,0.01,0.01,0.01]
        dylim = 300.
        colors = 'kbr'	
        lws = 0.5,2.	
        figure(); subplots_adjust(hspace=0,wspace=0.2)
        for i in range(6):
            subplot(3,2,i+1)
            for Zind in rl(g.Zs):
                Z = g.Zs[Zind]
                for aEUVind in rl(g.aEUVs):
                    a = g.aEUVs[aEUVind]
                    if i>=2: loglog()
                    else: semilogx()
                    vals = []
                    for nind in rl(g.ns):
                        n = g.ns[nind]
                        U = g.Us[-1]
                        ng = n+U
                        k = (n,a,Z)
                        if k not in self.g: continue
                        if i==0:   ratio = ratiosdic[k][0]/2.2
                        elif i==1: ratio = ratiosdic[k][0]/6562./(ratiosdic[k][1]/2.3/4861.)
                        else:      ratio = ratiosdic[k][i]
                        vals.append( (10**ng,ratio) )
                    l = tplot(sorted(vals),'-',c=colors[aEUVind],lw=lws[Zind])
                        #u.annotateLine(l[0],labels[i],3.,rotate=False,xytext=(0,5))
            if i>=2: axvline(10**ncrits[i]/100.,ls=':',c='.5',lw=2.)
            if i>=4: xlabel(r'$n_\gamma \ ({\rm cm}^{-3})$')
            if i==0: 
                text(1e6,0.026,r'$Z=2.$',color='.3')
                text(1e6,0.055,r'$Z=0.5$',color='.3')
            if i==2:
                text(0.7e4,0.2,r'$n_{\gamma} = n_{\rm crit}/100$',color='.3')
            if i==5:
                text(10**1.2,1.8e-2,r'$\alpha_{\rm ox}=-2$',color=colors[0])
                text(10**1.2,9e-2,r'$\alpha_{\rm ox}=-1.6$',color=colors[1])
                text(10**1.2,6.5e-1,r'$\alpha_{\rm ox}=-1.2$',color=colors[2])
            ylabel(labels[i])
            xlim(1e1,1e7)
            if ylims[i]!=None: ylim(ylims[i])

    def flux(self):
        figure()
        n=3.
        for ia in rl(self.g.aEUVs):
            for iU in rl(self.g.Us):		
                a = self.g.aEUVs[ia]
                U = self.g.Us[iU]
                if not self.g[(n,U,a)].CloudyOk(): continue
                flux = self.g[(n,U,a)].ratios()[0]
                n0 = log(self.g[(n,U,a)].nIonizationFront())
                U0 = self.g[(n,U,a)].GrovesU()
                text(n0,U0,'%.1f'%u.log(flux))
        axis([2,8,-4,1])
    def fluxVsU(self):
        n=3.
        figure(); subplots_adjust(hspace=0.2)
        subplot(511)
        [tplot([(self.g[(n,U,a)].baseU(), u.log(self.g[(n,U,a)].ratios()[0])) 
                for U in self.g.Us if self.g[(n,U,a)].CloudyOk()],
               '-',c='kbcr'[self.g.aEUVs.index(a)],label='%.1f'%self.g[(n,-1,a)].aEUV)
         for a in self.g.aEUVs]
        l = legend()
        l.draw_frame(False)
        xlabel(r'$U_{\rm fiducial}$'); ylabel(r'$F_{\rm H\beta} / F_{\rm Inci 1215\AA}$')	
        xlim(-4,0)
        subplot(512)
        [tplot([(U,u.log(self.g[(n,U,a)].ratios()[0]) ) 
                for U in self.g.Us if self.g[(n,U,a)].CloudyOk()],
               '-',c='kbcr'[self.g.aEUVs.index(a)],)
         for a in self.g.aEUVs]
        xlabel(r'$U_0$'); ylabel(r'$F_{\rm H\beta} / F_{\rm Inci 1215\AA}$')
        xlim(-4,0)
        subplot(513)
        [tplot([(self.g[(n,U,a)].baseU(), U) 
                for U in self.g.Us if self.g[(n,U,a)].CloudyOk()],
               '-',c='kbcr'[self.g.aEUVs.index(a)],)
         for a in self.g.aEUVs]
        plotLine(c='k',ls=':')
        xlabel(r'$U_{\rm fiducial}$'); ylabel(r'$U_0$')	
        xlim(-4,0)
        subplot(514)
        [tplot([(U, self.g[(n,U,a)].ratios()[5]) 
                for U in self.g.Us if self.g[(n,U,a)].CloudyOk()],
               '-',c='kbcr'[self.g.aEUVs.index(a)],)
         for a in self.g.aEUVs]
        xlabel(r'$U_{0}$'); ylabel(r'Balmer decrement')	
        xlim(-4,0)
        subplot(515)
        [tplot([(U, self.g[(n,U,a)].ratios()[6]) 
                for U in self.g.Us if self.g[(n,U,a)].CloudyOk()],
               '-',c='kbcr'[self.g.aEUVs.index(a)],)
         for a in self.g.aEUVs]
        xlabel(r'$U_{0}$'); ylabel(r'%s/%s'%(slsn2,sls2))	
        xlim(-4,0)






    def Tprofile(self, showonemodel=True, aEUVinds=None, Zinds=None, nginds=None, Uinds=None,SEDinds=None,plotargstype=0,labelset=1,
                 j1s=[],j2s=[],plotSput=False,showtau=False,mu=1.,isposter=False):
        if presentation: labelsize(True,18)
        if 'n' in self.g.keynames: ngammas = sorted(unique([(self.g.keydict(k)['n']+self.g.keydict(k)['U']) for k in self.g.keys()]).tolist())
        else: ngammas = array([self.g.ns[0] + self.g.Us[0]])
        rtf = rtfig(tickright=True,figsize=(fig_width,3*len(j2s)))
        if not presentation: subplots_adjust(right=0.91,bottom=0.07,top=0.985)
        else: subplots_adjust(right=0.89,left=0.11,bottom=0.11,top=0.97)
        if aEUVinds == None: aEUVinds = rl(self.g.aEUVs)
        if Zinds    == None: Zinds = rl(self.g.Zs)
        if nginds   == None: nginds = rl(ngammas)
        if Uinds    == None: Uinds = rl(self.g.Us)
        if SEDinds  == None: SEDinds = rl(self.g.SEDs)
        ncols = len(j1s)
        nrows = len(j2s)	
        for icol, j1 in enumerate(j1s):
            for irow,j2 in enumerate(j2s):
                subplot(nrows,ncols,irow*ncols+icol+1)
                loglog()
                for SEDind in SEDinds:
                    for aind in aEUVinds:
                        for Zind in Zinds:		
                            for ing in nginds:
                                ng = ngammas[ing]
                                for Uind in Uinds:
                                    plotargs = ( {'c':'k','lw':(ing+1.)*0.5},
                                                 {'c':'kbr'[aind], 'lw':(0.5,2)[Zind]},
                                                 {'c':'kbcryk'[SEDind]} )[plotargstype]
                                    for ik in rl(self.g.keys()):		    					
                                        k = self.g.keys()[ik]
                                        m = self.g[k]
                                        kd = self.g.keydict(k)
                                        if 'U' in kd and (('n' in kd and kd['n']+kd['U']!=ng) or self.g.Us[Uind]!=kd['U']): continue
                                        if ('aEUV' in kd and kd['aEUV']!=self.g.aEUVs[aind]) or ('Z' in kd and kd['Z']!=self.g.Zs[Zind]): continue
                                        if 'inputSEDdir' in kd and kd['inputSEDdir']!=self.g.SEDs[SEDind]: continue

                                        ##xaxis
                                        if j1==0: 
                                            maxind = m.ionizationFront()#len(m.depths)
                                            xs = array(m.depths)[:maxind]
                                            xs[0] = 1. #!!!!!!!!!!!!!!!!!
                                        if j1==1: 
                                            maxind = m.ionizationFront()
                                            xs = (m.depths[m.ionizationFront()] - m.depths)[:maxind]
                                        if j1==2: 					    
                                            #maxind = m.ionizationFront()
                                            #xs = (m.localColumnDensity(islog=True)[m.ionizationFront()] - m.localColumnDensity(islog=True))[:maxind]
                                            maxind = m.ionizationFront()#len(m.depths)
                                            xs = (m.localColumnDensity(islog=True))[:maxind] 
                                        if j1 in (3,6):
                                            maxind = m.ionizationFront()
                                            xs = (m.depths[m.ionizationFront()] - m.depths)[:maxind] * 10**(m.n+m.U-3)
                                        if j1==4: 
                                            maxind = m.ionizationFront()#len(m.depths)
                                            if False:#m.hasdust: 
                                                xs = (m.localColumnDensity(islog=True)*m.meanSigmaDust())[:maxind]
                                            else: 
                                                dtaus = m.nH*m.ddepths*array([m.sigmaActual(i) for i in rl(m.ddepths)])
                                                xs = array([sum(dtaus[:i]) for i in range(1,maxind+1)])

                                        ##yaxis
                                        if j2==0: 
                                            ys = 10**m.Ts
                                        if j2==1: 
                                            ys = m.nH
                                            if j1==6: ys = 10**(log(m.nH)-(m.n+m.U))
                                        if j2==3: 
                                            ys = 10**(m.Ts+log(m.nH)-4)
                                        if j2==4: 
                                            ys = 10**(m.n+m.U-log(m.nH))
                                        if j2==5: 
                                            ys = m.Pgas/(mu*m.incidentIonizingFlux()/c)#(10**(m.n+m.U)*77*eV)


                                        ##plot
                                        l = plot(xs, ys[:maxind], ls='-', **plotargs)

                                        ##labels
                                        if labelset in (1,4):
                                            if j1 in (0,1,4): 
                                                if j1==1 and xs[0]<1e20: text(xs[0],ys[0],r'$|$',ha='center',va='center',fontsize=16)
                                                yshift=((0,2)[j1==0 and kd['n']!=5],(-10,-15)[kd['n']==4])[j1==1 and j2==3]
                                                if kd['U']==-2: 
                                                    if j1==0: yshift+=2
                                                    if j1==0 and j2==1 and labelset==4: yshift+=5
                                                    if j1==1 and j2==5: yshift-=9
                                                    if j1==1 and j2==1: yshift-=7						    
                                                if kd['U']==-1:
                                                    if j1==1: yshift-=5
                                                if kd['U']==3:
                                                    if j1==1: yshift-=3
                                                if labelset==4 and j1==1:
                                                    if kd['U']==-1: 
                                                        xshift = -18
                                                        if j2==1: yshift -=3
                                                    if kd['U']==0: xshift = -10
                                                    if kd['U']==1: 
                                                        xshift=-5
                                                        yshift+=9
                                                    if j2==1 and kd['U']==-2: xshift=5
                                                    if j2==5 and kd['U']==-2: xshift=2
                                                else:
                                                    xshift = (0,(3,6,)[j1%2])[kd['n']!=0]
                                                    if j1==0 and j2==1 and labelset==4: xshift=-1

                                                if labelset==4 and 5>kd['n']>2 and j1==1:
                                                    yshift+=10
                                                    if j2==0 and kd['n']==3: xshift+=20
                                                    if j2==0 and kd['n']==4: xshift+=10
                                                #if kd['n']==0: 
                                                    #u.annotateLine(l[0],r'$ n_0 = %s \ {\rm cm^{-3}} $'%(arilogformat(10**kd['n'],dollarsign=False))
                                                                    #+ ('',r' ($U_0=%s$)'%arilogformat(10**kd['U'],dollarsign=False))[j2==3 and j1==0]
                                                                    #,(2e14,xs[1])[j1>0],rotate=False,xytext=(xshift,yshift))

                                                #else: 
                                                    #u.annotateLine(l[0],r'%s'%(arilogformat(10**kd['n']))
                                                                    #+ ('',r' ($U_0=%s$)'%arilogformat(10**kd['U'],dollarsign=False))[j2==3 and j1==0]
                                                                    #,(2e14,xs[1])[j1>0],rotate=False,xytext=(xshift,yshift))
                                                if j2 in (5,1):
                                                    u.annotateLine(l[0],('',r'$U_0=$')[kd['U']==-2] 
                                                                   + r'$%s$'%arilogformat(10**kd['U'],dollarsign=False),
                                                                 ((2e14,1.3e14)[kd['U']==-2],xs[1],None,None,2e-3)[j1],
                                                                 rotate=False,xytext=(xshift,yshift))

                                                if j2==j2s[0]:
                                                    if j1==0:
                                                        axis([1e14,(1e19,1e20)[labelset==4],1e-3,10])
                                                        transData =  gca().transData
                                                        transAxesI = gca().transAxes.inverted()							
                                                        if kd['U']!=-2:
                                                            p1 = transAxesI.transform(transData.transform(array([[xs[-2],4.5]])))[0]
                                                            p2 = transAxesI.transform(transData.transform(array([[xs[-2],3.5]])))[0]
                                                            arrow(p1[0],p1[1],p2[0]-p1[0],p2[1]-p1[1],head_width=0.01,transform=gca().transAxes,fc='k',ec='k')
                                                        else:
                                                            p1 = transAxesI.transform(transData.transform(array([[(1.85e16,  8e16)[labelset==4], 4.5]])))[0]
                                                            p2 = transAxesI.transform(transData.transform(array([[(1.3e16,3e15)[labelset==4],(4.65,3.)[labelset==4]]])))[0]
                                                            fact = (1.,0.2)[labelset==4]
                                                            arrow(p1[0],p1[1],(p2[0]-p1[0])*fact,(p2[1]-p1[1])*fact,head_width=0.01,transform=gca().transAxes,fc='k',ec='k')
                                                    if j1==1:
                                                        axis([(1e19,1e20)[labelset==4],1e13,1e-3,10])
                                                        transData =  gca().transData
                                                        transAxesI = gca().transAxes.inverted()														
                                                        if kd['U']!=-2:
                                                            p1 = transAxesI.transform(transData.transform(array([[xs[0],4.5]])))[0]
                                                            p2 = transAxesI.transform(transData.transform(array([[xs[0],3.5]])))[0]				
                                                            arrow(p1[0],p1[1],p2[0]-p1[0],p2[1]-p1[1],head_width=0.01,transform=gca().transAxes,fc='k',ec='k')
                                                        else:
                                                            p1 = transAxesI.transform(transData.transform(array([[(1e16,  1e16)[labelset==4], 4.5]])))[0]
                                                            p2 = transAxesI.transform(transData.transform(array([[(0.7e16,3e15)[labelset==4],(4.5,3.)[labelset==4]]])))[0]
                                                            fact = (1.,0.2)[labelset==4]
                                                            arrow(p1[0],p1[1],(p2[0]-p1[0])*fact,(p2[1]-p1[1])*fact,head_width=0.01,transform=gca().transAxes,fc='k',ec='k')							    
                                            #elif j1==0:		
                                                #print 'j1:', j1
                                                #if kd['n']!=0:
                                                    #print j2
                                                    #u.annotateLine(l[0],r'%s'%(arilogformat(10**kd['n'])) ,
                                                                    #2e19,rotate=False,xytext=((3,0),(6,0))[j1])
                                                #else:   
                                                    #print 'j2: ',j2
                                                    #u.annotateLine(l[0],r'$ n_0 = %s \ {\rm cm^{-3}}$'%(arilogformat(10**kd['n'],dollarsign=False)),
                                                                    #2e18,rotate=False,xytext=(3,0),ha='right')


                                        if labelset in (1,4):
                                            if j2==5 and (j1==0 or j1==1 and kd['U'] in ()):#3,0)):
                                                maxd = u.log(m.depths[m.ionizationFront()])
                                                #if labelset==1: maxd = (17.,17.5,18.,18.5,15.3,16.5)[kd['U']]
                                                sig = m.sigmaActualIonizing(0)
                                                lp = 2.3*kB*10**m.Ts[0]/(mu*sig*m.incidentIonizingFlux()/c)
                                                ds = 10**u.arange(14.,maxd,0.01)
                                                if j1==0: _xs = ds
                                                if j1==1: _xs = m.depths[m.ionizationFront()] - ds
                                                plot(_xs, m.Pgas[0]/(mu*m.incidentIonizingFlux()/c)*exp(ds/lp),ls='--',lw=0.5,c='k')
                                        if False:
                                            m0 = self.g[(0,3)]
                                            energies = m0.IntegratedAbsorbedEnergy()
                                            AbsorbedEnergyFraction = energies / energies[-1]						    
                                            TausDust = m0.localColumnDensity() * m0.grainOpacities[m0.ionizationEnergyInds()[0]]
                                            xs0 = (m0.depths[m0.ionizationFront()] - m0.depths)[:m0.ionizationFront()]
                                            ind = len(xs0)-1-searchsorted(xs0[::-1],xs[0])
                                            Us = log(m0.localU())
                                            if plotSput and j1==1 and j2==0:						    						    
                                                tauSput = lambda n,T: 1e5*(1+(T/1e6)**-3.)/n #Draine 2011b eq 25.14
                                                plot(xs,[tauSput(m.nH[i],10**m.Ts[i]) for i in rl(xs)],c='.5',ls='--')
                                            if showtau and j1==1 and j2==3 and m!=m0:
                                                text(xs[1]/(8.,1000.)[m==m0],ys[1],
                                                     ((r'$(\tau=%.2f)$',
                                                       r'$%.2f$')[False])%(TausDust[ind]),
                                                     fontsize=10,color='.3')

                                                #for T in 2e4,1e5,1e6:
                                                    #r100 = 2260./10**(0.5*ng)/100.
                                                    #x0 = 3.1e-2*3e18*r100**2*(T/1e4)/(m.meanSigmaDust()/1e-21 * m.incidentFlux()*4*pi*r100**2*3e20**2/1e45)							
                                                    #constTanalytical = lambda x,n0,x0=x0: n0*exp(x/x0)
                                                    #ind = searchsorted(-10**m.Ts, -T)
                                                    #plot(xs[ind-50:ind+50], constTanalytical(m.depths[ind-50:ind+50]-m.depths[ind],ys[ind]),c='r')
                                                    #text(xs[ind+49]/1.5,constTanalytical(m.depths[ind+49]-m.depths[ind],ys[ind])*1.2,
                                                                #r'$T=%d x 10^{%d}$'%((1,2)[T==2e4],u.iround(log(T))),color='r',fontsize=12)
                                        if labelset in (2,5):
                                            if j1==1 and j2==0:
                                                xfactor=(1.,150.)[labelset==5]
                                                xfactor2=(1.,2.)[labelset==5 and ing==1]
                                                yfactor=(1.,(0.75,1.2,0.6)[ing])[labelset==5]
                                                if ing==2: 
                                                    text(6e16*xfactor,4e6*yfactor,r'$n_\gamma = 10^{%d}\ {\rm cm}^{-3}$'%ng)
                                                    text(6e16*xfactor,2e6*yfactor,r'$(r=%s\ L_{i,45}^{0.5}\ {\rm pc})$'%(nSignificantDigits(2260./10**(0.5*ng),1,True)))
                                                else: 
                                                    text((3e19,2.5e18)[ing]*xfactor*xfactor2,
                                                         (2.5e5,1e6)[ing]*yfactor,
                                                         r'$10^{%d}\ (%s)$'%(ng, nSignificantDigits(2260./10**(0.5*ng),1,True)))	    
                                            else:						
                                                if j1==3: text(xs[0],ys[0],r'$|$',ha='center',va='center',fontsize=16)
                                                xloc = (xs[1],sorted(unique(xs))[2])[labelset==5 and j1==1 and j2==1]
                                                u.annotateLine(l[0],r'$10^{%d}$'%(kd['n']+kd['U']),xloc,rotate=False,xytext=(3,0))



                                            if False: #j1==2 and j2==2 and kd['U']==5:
                                                Ueq = (m.incidentFlux() / 10**(m.n+m.U) / (2*kB*1e4) / c)**-1.
                                                U0,T0  = ys[0], 10**m.Ts[0]
                                                aox=-self.g.aEUVs[aind]
                                                s,e = m.ionizationEnergyInds()
                                                delta = aox * (m.grainOpacities[s] / m.meanSigmaDust(True,True)  - 1)
                                                tdnu0ratio = m.grainOpacities[m.ionizationEnergyInds()[0]] / m.meanSigmaDust()
                                                #tautag = lambda tau_d,aox=aox,delta=delta: (1. - m.meanSigmaDust(True,True) / m.meanSigmaDust()*tau_d, 
                                                                                            #delta/(delta+aox-1) * (tdnu0ratio*tau_d)**(-(aox-1)/delta)
                                                                                            #) [tdnu0ratio*tau_d>1]
                                                #constTanalytical = lambda tau_d,T=T,Ueq=Ueq,U0=U0,T0=T0,aox=-1.6: Ueq*tautag(tau_d)/(tau_d*(T/1e4)**-1.)
                                                #ind = searchsorted(-10**m.Ts, -T)							
                                                #plot(xs[ind-50:ind+50], [constTanalytical(x) for x in xs[ind-50:ind+50]],c='r')
                                                analytical1 = lambda tau_d,T,Ueq=Ueq: Ueq*T/1e4/tau_d
                                                analytical2 = lambda tau_d,T,Ueq=Ueq,U0=U0,T0=T0: Ueq/((T/1e4)**-1*tau_d + Ueq/U0*T0/T)
                                                analytical3 = lambda tau_d,T,Ueq=Ueq,tdnu0ratio=tdnu0ratio,aox=aox,delta=delta: (
                                                    Ueq*T/1e4/tau_d * (1-aox/(aox+delta)*tdnu0ratio*tau_d) / 
                                                    (1-aox/(aox+delta)*tdnu0ratio*tau_d/2 - tau_d/2* m.meanSigmaDust(power=2)/ m.meanSigmaDust()**2))
                                                #print 'a_ox = %.1f, delta=%.1f, tau(nu_0)/tau = %.1f'%(aox, delta, tdnu0ratio)
                                                #print 'A=%.1f, B=%.1f'%(aox/(aox+delta)*tdnu0ratio, m.meanSigmaDust(power=2)/ m.meanSigmaDust()**2)
                                                for iAnal in ([0,2],[0,1])[len(aEUVinds)>1]:							
                                                    anal = (analytical1, analytical2, analytical3)[iAnal]						    
                                                    ys = anal(xs*m.meanSigmaDust(),10**m.Ts)
                                                    ys = [ys[i] for i in rl(ys) if iAnal==0 or xs[i]*m.meanSigmaDust()<1/2.6]
                                                    plot(xs[:len(ys)],ys,c=('kbr'[aind],'r')[len(aEUVinds)==1],ls='--')

                                        if labelset==3:
                                            pass

        if labelset==1 and not presentation:
            j1=j1s[1]; j2=j2s[0]; subplot(nrows,ncols,j2s.index(j2)*ncols+j1s.index(j1)+1)
            xpos = (0.3,0.6)[isposter]
            if not isposter: text(xpos,0.16,r'$n_\gamma = %d\ {\rm cm}^{-3}\ (r=70\ L_{{\rm i},45}^{1/2}\ {\rm pc})$'%(10**ng),transform=gca().transAxes)
            else: text(xpos,0.16,r'$r=70\ L_{{\rm i},45}^{1/2}\ {\rm pc}$',transform=gca().transAxes)
            #text(xpos,0.14,r'$(r = %s\ L_{i,45}^{0.5}\ {\rm pc})$'%nSignificantDigits(2260./10**(0.5*ng),1,True),transform=gca().transAxes)
            text(xpos,0.1,r'$Z = %s~Z_\odot$'%(intandfloatFunc(self.g.Zs[Zind])), transform=gca().transAxes)
            text(xpos,0.04,r'$\alpha_{\rm ion} = %.1f$'%(self.g.aEUVs[aind]), transform=gca().transAxes)					
            subplots_adjust(bottom=0.1,right=0.93)
            j1=j1s[0]; j2=j2s[0]; subplot(nrows,ncols,j2s.index(j2)*ncols+j1s.index(j1)+1)
            slantlineprops = {'width':0.3,'headwidth':2,'frac':0.3,'color':'0.5'}
            if not isposter: annotate(r'$P_{\rm gas} = \beta P_{\rm rad}$', (4e15, 1), (3e15,1.3),ha='center',arrowprops=slantlineprops)
            else: annotate(r'$P_{\rm gas} = P_{\rm rad}$', (4e15, 1), (3e15,1.3),ha='center',arrowprops=slantlineprops)
            annotate(r'$\frac{P_{\rm gas}(x)}{P_{\rm gas;0}}=e^{x/l_{\rm P}}$', (3e18, 0.012), (0.5e18,1.5e-3),ha='left',arrowprops=slantlineprops)
            annotate(r'${\rm Cloudy}$', (2.e18, 0.015), (2e17,0.01),va='center',arrowprops=slantlineprops)
            j1=j1s[0]; j2=j2s[1]; subplot(nrows,ncols,j2s.index(j2)*ncols+j1s.index(j1)+1)
            if not isposter: annotate(r'$\frac{\beta P_{\rm rad}}{2.3 K_{\rm B} \cdot 10^4}$', (4e15, 3.5e4), (4e15,7e4),ha='left',arrowprops=slantlineprops)
            else: annotate(r'$\frac{P_{\rm rad}}{2.3 K_{\rm B} \cdot 10^4}$', (4e15, 3.5e4), (4e15,7e4),ha='left',arrowprops=slantlineprops)
        if labelset==4:
            subplots_adjust(bottom=0.1,right=0.93)
            j1=j1s[0]; j2=j2s[0]; subplot(nrows,ncols,j2s.index(j2)*ncols+j1s.index(j1)+1)
            slantlineprops = {'width':0.3,'headwidth':2,'frac':0.3,'color':'0.5'}
            annotate(r'$\frac{P_{\rm gas}(x)}{P_{\rm gas;0}}=e^{x/l_{\rm P}}$', (1.7e18, 0.28), (0.3e18,0.1),ha='left',arrowprops=slantlineprops)
            annotate(r'${\rm Cloudy}$', (1.5e18, 0.4), (1e17,0.5),va='center',arrowprops=slantlineprops)	    
        if plotSput and labelset in (1,2):
            for isp in ((3,4),(1,2))[labelset-1]:
                subplot(2,2,isp)
                axhline(1e6,c='.5',ls=':')


        if labelset in (2,5):
            subplots_adjust(wspace=0.1,bottom=0.1)
            #j1=1; j2=0; subplot(nrows,ncols,j2s.index(j2)*ncols+j1s.index(j1)+1)
            #xpos = 0.05
            #text(xpos,0.14,r'$n_0 = 1\ {\rm cm}^{-3}$',transform=gca().transAxes)
            #text(xpos,0.08,r'$Z = %.1f Z_\odot$'%(self.g.Zs[Zind]), transform=gca().transAxes)
            #text(xpos,0.02,r'$\alpha_{\rm ox} = %.1f$'%(self.g.aEUVs[aind]), transform=gca().transAxes)					



        if labelset==3:
            j1=0; j2=0; subplot(nrows,ncols,j2s.index(j2)*ncols+j1s.index(j1)+1)
            text(0.6e22/10**ng, 3e5, r'$Z=0.5$')
            text(0.6e21/10**ng, 1e6, r'$Z=2$')
            j1=0; j2=1; subplot(nrows,ncols,j2s.index(j2)*ncols+j1s.index(j1)+1)
            text(0.6e20,1e-4,r'$\alpha_{\rm ox}=-2.$' ,color='k')
            text(0.4e21,6e-5,r'$-1.6$',color='b')
            text(1.2e21,2e-5,r'$-1.2$',color='r')
        for j1 in j1s:
            for j2 in j2s:
                logstr = ('', r'\log\ ')[presentation]
                subplot(nrows,ncols,j2s.index(j2)*ncols+j1s.index(j1)+1)
                if presentation: 
                    [ax.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x,pos:'%d'%log(x))) for ax in (gca().xaxis, gca().yaxis)]
                if labelset==5: gca().xaxis.set_major_locator(matplotlib.ticker.LogLocator(base=100.))
                if j2==j2s[0]:
                    if labelset in (1,4):
                        if j1==0:
                            #text(0.025,0.88,'illuminated\nsurface',transform=gca().transAxes,ha='left')
                            mid = (4e17,1e18)[labelset==4]
                            text(mid/1.5,(5.5,12)[labelset==4],r'ionization front $(x_{\rm f})$',ha='center')
                            if labelset==1: arrow(1.9e16,(4.5,2.75e5)[j2==1],4e18-1.9e16,0.,head_width=0.000001,fc='k',ec='k')
                            if labelset==4: arrow(8e16,(10.5,2.75e5)[j2==1],3e19-8e16,0.,head_width=0.000001,fc='k',ec='k')
                            arrow(mid,((4.5,10.5)[labelset==4],2.75e5)[j2==1],0.,0.7,head_width=0.000001,fc='k',ec='k')
                        if j1==1:
                            mid = (1e17,3e17)[labelset==4]
                            text(mid*1.5,(5.5,12)[labelset==4],'illuminated surface',ha='center')
                            if labelset==1: arrow(7e15,(4.5,2.75e5)[j2==1],4e18-7e15,0.,head_width=0.000001,fc='k',ec='k')
                            if labelset==4: arrow(1e16,(10.5,2.75e5)[j2==1],3.5e19-1e16,0.,head_width=0.000001,fc='k',ec='k')
                            arrow(mid,((4.5,10.5)[labelset==4],2.75e5)[j2==1],0.,0.7,head_width=0.000001,fc='k',ec='k')


                    #text(0.55,0.95,'back side',transform=gca().transAxes,ha='left')
                    #arrow(0.58,0.92,0.1,0.,transform=gca().transAxes,head_width=0.015,fc='k',ec='k')

                if j1==0: 
                    if not presentation: rtf.xlabel(r'$x$   (cm)')
                    else: rtf.xlabel(r'$\log\ (x)$   [cm]')
                    if labelset==1: xlim(1e14,1e19)
                    if labelset==4: xlim(1e14,1e20)
                if j1==1: 
                    if not presentation: rtf.xlabel(r'$x_{\rm f} - x$   (cm)')
                    else: rtf.xlabel(r'$\log\ (x_{\rm f} - x)$   [cm]')
                    if labelset==1: xlim(1e19,1e13)
                    if labelset==2: xlim(1e20,4e11)
                    if labelset==4: 
                        xlim(1e20,1e13)
                        #axvline(50*3e18, c='.5',ls=':')
                    if labelset==5: xlim(1e22,3e10)

                if j1==2: 
                    rtf.xlabel(r'$N_{\rm H}$ from illuminated face (cm$^{-2}$)') #HI front (cm$^{-2}$)')
                    if labelset!=4: xlim(0,2.5e21)
                    else: xlim(1e20,1e23)
                if j1 in (3,6): 
                    rtf.xlabel(r'$(x_{\rm f} - x)/(n_{\gamma,\ 3})$ (cm)')
                    if labelset==2: xlim(1e20,5e12)
                    if labelset==5: xlim(2e21,4e10)		
                if j1==4: 
                    rtf.xlabel(r'${\bar \tau}$')
                    xlim(1e-3,2)
                    rtf.xformat(arilogformatter)

                if j2==0: 
                    rtf.ylabel(r'$%sT$ (K)'%logstr)
                    ylim(3e3,(3e6,1e7)[labelset!=1])		    
                if j2==1: 
                    if j1!=6:
                        rtf.ylabel(r'$%sn\ ({\rm cm}^{-3})$'%logstr)
                        if labelset==1: ylim(0.5,7e5)
                        if labelset==2: ylim(0.3,1e7)
                        if labelset==4: ylim(0.5,7e5)
                        nf = mu*m.incidentIonizingFlux()/c/(2.3*kB*1e4)
                        axhline(nf, c='.5',ls=':')
                    else:
                        rtf.ylabel(r'$U^{-1} \equiv n/n_\gamma$')
                        if labelset==2: ylim(1e-5,1e2)
                        if labelset==5: ylim(0.3e-5,3e5)
                    gca().yaxis.set_major_formatter(arilogformatter)
                if j2==3: 
                    rtf.ylabel(r'$P_{\rm gas} \propto n T_4\ ({\rm cm}^{-3})$')
                    rtf.yformat(arilogformatter)
                    ylim((3e1,200)[labelset==4],1e6)
                if j2==4: 
                    rtf.ylabel(r'$U \equiv n_\gamma / n$')
                    rtf.yformat(arilogformatter)
                    ylim(1e-3,10**3.5)
                    axhline(1.5e-2, c='.5',ls=':')		    
                if j2==5: 
                    if not isposter: rtf.ylabel(r'$P_{\rm gas} / \beta P_{\rm rad} (\equiv \Xi^{-1})$')
                    else: rtf.ylabel(r'$P_{\rm gas} / P_{\rm rad}$')
                    rtf.yformat(arilogformatter)
                    if labelset==1: ylim(1e-3,10)
                    if labelset==4: ylim(1e-2,20)
                    axhline(1., c='.5',ls=':')
                    if j1==4: plot([1e-3,1],[1e-3,1],ls='--',lw=0.5)
        if presentation: labelsize(False)



    def f_ion(self,Zs,aEUVs,eta=0.):
        g = self.g
        rtf=rtfig()	
        total={}
        for Zind in rl(g.Zs):
            Z = g.Zs[Zind]
            if Z not in Zs: continue
            for aind in rl(g.aEUVs):
                a = g.aEUVs[aind]
                if a not in aEUVs: continue
                for i in range(3):
                    subplot(3,1,i+1)
                    semilogx()		    
                    yfunc1 = lambda m,i=i: ((array(m.ionizationRates)*m.nHI*m.ddepths).sum() ,
                                            10**m.hasT / (ld.h*3e18/6562.) ,
                                       (array(m.ionizationRates)*m.nHI*m.ddepths).sum())[i]
                    yfunc2 = lambda m,i=i: ( (10.**(m.n+m.U)*c),10**m.hasT/ (ld.h*3e18/6562.) )[i==2]
                    tups = sorted([(10**(m.n+m.U), yfunc1(m)/yfunc2(m)) for m in g.values() if m.Z==Z and m.aEUV==a and m.CloudyOk()])
                    l = tplot(tups,'-',lw=(0.5,1,2)[Zind],c='kbry'[aind])
                    if i==0:
                        lblx = 10**((3.,2.5,2.,1.5)[aind])
                        u.annotateLine(l[0],((r'$%.1f Z_\odot,\ %.1f$',r'$Z = %.1f Z_\odot,\ \alpha_{\rm ox} = %.1f$')[aind==0 and Zind==0])%(Z, a),
                                       lblx,rotate=False,xytext=(0,5))	
                        rtf.ylabel(r'$f_{\rm ion}$')
                    if i==1:
                        rtf.ylabel(r'$Q_{\rm H_\alpha}/Q_{\rm ion}$')
                    if i==2:
                        rtf.ylabel(r'$Q_{\rm H_\alpha}/f_{\rm ion}Q_{\rm ion}$')
                        axhline(4.4,ls=':',c='k')
                    gm = GasModel(g,a,Z,eta,7.5,-1.5)
                    total[(i,Z,a)] = gm.integrate(lambda m,y1=yfunc1,y2=yfunc2: y2(m)/y1(m)) / gm.integrate(lambda m: 1)
        rtf.xlabel(r'$n_\gamma\ ({\rm cm}^{-3})$')
        mysavefig('f_ion')
        return total

    def plotDiagnostics(self,aEUVinds=None,Zinds=None,maxngammas=[5.5],minngammas=[0.],etas=u.arange(-1,1,0.1),keyset=1,logngstep=0.5):
        g = self.g
        if aEUVinds == None: aEUVinds = rl(g.aEUVs)
        if Zinds    == None: Zinds    = rl(g.Zs)
        ylbls = [ r'[NeV]/[NeIII]', r'[NeV] (14$\mu$m/24$\mu$m)', '[OIII] $(5007\AA/4363\AA)$', 
                  '[SII] $(6716\AA/6731\AA)$', r'HeII 4686$\AA$ / H$\beta$', r'H$\alpha$/H$\beta$',
                 '[FeVII]/[NeV]', '[OIII] 5007$\AA$/[OI] 6300$\AA$', #'[OIII] 5007$\AA$/[OIV] 28.6$\mu$m',
                 '[NII]/[SII]','[SIII] (19$\mu$m/33$\mu$m']
        observed = [(1.2,7/3.,8/1.6),
                    (0.68,0.95,1.26),
                    10**array([0.7,1.3,1.8]), #Baskin & Laor (2005)
                    1.1,
                    1/3.,
                    4.2,
                    10**array([-0.4,-0.45,-0.25,-0.1]),
                    10**1.65,#BPT by eye
                    #10**array([-0.7,0.,0.8]), Melendez?
                    (1.25,1.5,2.15),
                    (0.4,1.2)]
        logpanels = [2,6,7]
        colors = 'kbr'	
        lws = [(0.5,2.),(0.5,1.,2.)][len(Zinds)==3]

        figure(); subplots_adjust(left=0.05,wspace=0.1)
        for Zind in Zinds:
            Z = sorted(g.Zs)[Zind]	
            for aEUVind in aEUVinds:
                a = sorted(g.aEUVs)[aEUVind]
                for maxngamma in maxngammas:
                    for minngamma in minngammas:			
                        ratios = [GasModel(g, a, Z, eta,logngamma0=maxngamma,logngmin=minngamma,keyset=keyset,logngstep=logngstep).moreratios() for eta in etas]
                        for i in rl(ratios[0]):
                            subplot(5,2,i+1)
                            xs = etas
                            ys = [r[i] for r in ratios]
                            plot(xs,ys,c=colors[aEUVind],lw=lws[Zind])
        for i in rl(ratios[0]):
            subplot(5,2,i+1)
            ylabel(ylbls[i])
            if observed[i]!=None:
                if type(observed[i])==type(1.): axhline(observed[i],c='.5',ls='-',lw=1)
                else: [axhline(observed[i][j],c='.5',lw=(1,0.5)[j in (0,len(observed[i])-1)]) for j in rl(observed[i])]
            if i in logpanels: semilogy()
            if i in (8,9): xlabel(r'$\eta$')
    def sputtering(self):
        rtf=rtfig()
        tauSput = lambda n,T: 1e5*(1+(T/1e6)**-3.)/n #Draine 2011b eq 25.14
        tauDyn = lambda ng: 3000*70*10**-((ng-3)/2.)
        for m in self.g.values():
            maxind = m.ionizationFront()
            xs = (m.depths[m.ionizationFront()] - m.depths)[:maxind] * 10**(m.n+m.U-3)
            ys = [tauSput(log(m.nH)[i],10**m.Ts[i])/tauDyn(m.n+m.U) for i in rl(xs)]
            l = plot(xs[:len(ys)],ys,c='k',ls='--')
            u.annotateLine(l[0],r'$n_\gamma = 10^{%d}$'%(m.n+m.U),1e15,rotate=False)

        loglog()
        xlabel(r'Distance from HI front ($n^{-1}_{\gamma;\ 3}$ pc)')
        ylabel(r'$\tau_{\rm sputtering} / \tau_{\rm dyn}$')
        axhline(0.,ls=':',c='.5')
    def high2lowIonization(self,valnames, aEUVinds=None,Zinds=None,ng = None,Uinds=None,ax=None,ls='-',showlabels=True,c='k',dolog=True,lw=1.):
        g = self.g
        if aEUVinds == None: aEUVinds = rl(g.aEUVs)
        if Zinds    == None: Zinds    = rl(g.Zs)
        if Uinds    == None: Uinds    = rl(g.Us)
        lws = [(0.5,2.),(0.5,1.,2.)][len(Zinds)==3]

        if ax==None: rtfig()
        if dolog: semilogy()
        for Zind in Zinds:
            Z = sorted(g.Zs)[Zind]	
            for aEUVind in aEUVinds:
                a = sorted(g.aEUVs)[aEUVind]
                for i in rl(valnames):
                    vn = valnames[i]
                    ys = []
                    xs = []
                    for Uind in Uinds:
                        #ng = g.ns[ngind]+U
                        #m = self.g[(g.ns[ngind],a,Z)]			
                        m = self.g[(g.ns[Uind], g.Us[Uind])]
                        if ng!=None and m.n+m.U!=ng: continue
                        if vn[0]=='xray':
                            if m.linesdicT[vn[1]]!=None: ys.append ( m.getBandLuminosity(24.8,6.2)/10**m.linesdicT[5007] )
                            else: ys.append(-inf)			    
                        else:
                            if m.linesdicT[vn[0]]==None or m.linesdicT[vn[1]]==None: ys.append(-inf)
                            else: ys.append(10**m.linesdicT[vn[0]] / 10**m.linesdicT[vn[1]])
                        xs.append(m.U)
                    norm = [1.]#[y for y in ys if not isinf(y)]
                    if len(norm)>0: 
                        l = plot(xs,array(ys)/norm[-1],ls=ls,c=c,lw=lw)
                        if vn[0]=='xray': ylbl = r'$L(0.5 - 2 {\rm keV})$', emissionLinesDic[vn[1]].label(onlywl=True)
                        else: ylbl = emissionLinesDic[vn[0]].label(onlywl=True), emissionLinesDic[vn[1]].label(onlywl=True)
                        if showlabels and ls=='-': u.annotateLine(l[0], '%s/%s'%ylbl,10.**vn[3],rotate=False)

    def ModelRange(self,aEUVinds=None,Zinds=None,nginds = None):
        g = self.g
        if aEUVinds == None: aEUVinds = rl(g.aEUVs)
        if Zinds    == None: Zinds    = rl(g.Zs)
        if nginds   == None: nginds    = rl(g.ns)
        U = g.Us[0]
        valnames = [#(1032, 5007,   0.02,0.2), 
                    (3426, 5007,   0.02,0.2), 
                    (258800,5007,  0.4,4,True),
                    (76520,155500, 1,9), 
                    (143200,155500,0.4,4,True),
                    (3426,3869,    1,9),
                    ('xray',5007,  1,90.)]

        colors='kbr'
        lws = [(0.5,2.),(0.5,1.,2.)][len(Zinds)==3]
        figure()
        subplots_adjust(left=0.05,wspace=0.1,right=0.95,hspace=1e-4)
        for i in rl(valnames):
            subplot(3,2,i+1)
            vn = valnames[i]
            loglog()
            for Zind in Zinds:
                Z = sorted(g.Zs)[Zind]	
                for aEUVind in aEUVinds:
                    a = sorted(g.aEUVs)[aEUVind]
                    print Z, a		    
                    ys = []
                    for ngind in nginds:			
                        ng = g.ns[ngind]+U
                        m = self.g[(g.ns[ngind],a,Z)]			
                        if vn[0]=='xray':
                            if m.linesdicT[vn[1]]!=None: ys.append ( m.getBandLuminosity(24.8,6.2)/10**m.linesdicT[5007] )
                            else: ys.append(-inf)			    
                        else:
                            if m.linesdicT[vn[0]]==None or m.linesdicT[vn[1]]==None: ys.append(-inf)
                            else: 
                                print vn
                                ys.append(10**m.linesdicT[vn[0]] / 10**m.linesdicT[vn[1]])
                        if ngind==0 and Zind==0 and aEUVind==0: minval = ys[-1]
                    l = plot(10.**(array(g.ns)+U),array(ys),lw=lws[Zind],c=colors[aEUVind])
                    if vn[0]=='xray': ylbl = r'$L(0.5 - 2 {\rm keV})$', emissionLinesDic[vn[1]].label()
                    else: ylbl = emissionLinesDic[vn[0]].label(), emissionLinesDic[vn[1]].label()
            text(0.03,(0.8,0.1)[len(vn)>4], '%s/%s'%ylbl,transform=gca().transAxes)
            if i>3: xlabel(r'$n_\gamma\ ({\rm cm}^{-3})$')
            else: gca().xaxis.set_major_formatter(matplotlib.ticker.NullFormatter())
            gca().yaxis.set_ticks_position('both')
            ylim(vn[2],vn[3])
            gca().yaxis.set_major_locator(matplotlib.ticker.LogLocator(10,[1.,2.,3.,5.,7.])),
            gca().yaxis.set_major_formatter(arilogformatter2)
            #gca().yaxis.set_minor_locator(matplotlib.ticker.LogLocator(10,[1.,2.,3.,5.,7.])),	    
            #gca().yaxis.set_minor_formatter(arilogformatter2)
            #gca().yaxis.set_major_locator(matplotlib.ticker.FixedLocator(concat([u.arange(10)/i for i in 100.,10.,1.])))
            xlim(3e-3,5e5)
    def possibleRatios(self,wl1,wl2,Zs=None,aEUVs=None):

        g=self.g
        if Zs==None: Zs=g.Zs
        if aEUVs==None: aEUVs=g.aEUVs
        figure()
        for iZ,Z in enumerate(Zs):
            for ia,a in enumerate(aEUVs):	
                if wl2!='x-ray': f=lambda m,wl1=wl1,wl2=wl2: 10**(m.linesdicT[wl1]-m.linesdicT[wl2])
                else: f=lambda m,wl1=wl1: 10**m.linesdicT[wl1] / m.getBandLuminosity(24.8,6.2)
                tplot([(g[k].n+g[k].U,log(v)) for k,v in sorted(g(f).items()) 
                       if g[k].aEUV==a and g[k].Z==Z and g[k].linesdicT[wl1]!=None and g[k].linesdicT[wl2]!=None],'-',lw=(0.5,1,2)[iZ],c='kbry'[ia]) 






class GasModel:
    """
    all radii are in units of the radius at which ngamma=ngamma0
    """

    def __init__(self, grid, rKeyIndex, dlogr=1.,filterFunc=u.noFilter, cfslope=0.,Omega=1.):
        #assert(isinstance(grid,Grid))
        self.grid = grid
        self.keysDic = dict([(k[rKeyIndex],k) for k in grid if filterFunc(k)])

        self.cfslope = cfslope # <0
        self.Omega = Omega
        self.dlogr = dlogr	
        assert(False not in [self.logRadii()[i+1]-self.logRadii()[i] == dlogr for i in rl(self.logRadii(),-1)])
    def logRadii(self):
        return array(sorted(self.keysDic.keys()))
    def radii(self):
        return 10**self.logRadii()
    def __getitem__(self,logr):
        return self.grid[self.keysDic[logr]]
    def coeff(self):
        return self.Omega / (self.radii()**self.cfslope * self.dlogr).sum()
    def dcfdlogr(self,r):
        return self.coeff() * r**self.cfslope
    def integrate(self, func,canfail=False):
        total = 0.
        for logr in self.logRadii():
            r = 10**logr
            dcf = self.dcfdlogr(r) * self.dlogr
            try: val = func(self[logr])
            except Exception,e: 
                print e
                print 'log r=%.1f'%(logr)
                if canfail: val = (func(self[logr-dlogr]) + func(self[logr+dlogr]))/2.
                else: raise e
            total += val * 4.*pi*r**2 *dcf  #assumes intensity case
        return total
    def lineEmission(self,line):
        return self.integrate(lambda m,line=line: 10**m.linesdicT[line])
    def lineRatio(self,line1,line2):
        return self.lineEmission(line1)/self.lineEmission(line2)


LineNames  = [
    (('H  1 4861.33',), {'label':r'$H\beta$'}),
    (('O  3 5006.84',), {'ncrit': 5.8}),
    (('O  1 6300.30',), {'ncrit': 6.3}),
    (('N  2 6583.45',), {'ncrit': 4.9}),
    (('H  1 6562.81',), {'label': r'$H\alpha$'}),
    (('S  2 6716.44',), {'ncrit': 3.2,'transitionProb':2.6e-4,'colStrength':6.9*3.,'statWeights':(3,2)}),
    (('S  2 6730.82',), {'ncrit': 3.6,'transitionProb':8.8e-4,'colStrength':6.9*2.,'statWeights':(2,2)}),
    (('ne 5 3426',), {'ncrit': 7.3}),
    (('ne 5 3346',), {'ncrit': 7.3}),
    (('totl 3727', 'O', 2), {'ncrit': 3.15}), 
    (('ca a 3750', 'H', 1), {}),
    (('ca b 3750', 'H', 1), {}),
    (('ca a 3771', 'H', 1), {}),
    (('ca b 3771', 'H', 1), {}),
    (('h  1 3798',), {}),
    (('h  1 3835',), {}),
    (('ne 3 3869',), {'ncrit': 7.0}),
    (('h  1 3889',), {}),
    (('h  1 3970',), {}),
    (('s II 4070',), {'ncrit': 6.4}),
    (('h  1 4102',), {}),
    (('h  1 4340',), {}),
    (('totl 4363', 'O', 3), {'ncrit': 7.5}),
    (('he 1 4471',), {}),
    (('he 2 4686',), {}),
    (('ar 4 4711',), {'ncrit': 4.4}),
    (('totl 5199', 'Na', 1), {'ncrit': 3.3}),
    (('n  2 5755',), {'ncrit': 7.5}),
    (('he 1 5876',), {}),
    (('na 1 5892',), {}),
    (('fe 7 6087',), {'ncrit': 7.6}),
    (('he 1 6678',), {}),
    (('ar 3 7135',), {'ncrit': 6.7}),
    (('o II 7323',), {'ncrit': 6.8}),
    (('o II 7332',), {'ncrit': 6.8}),
    (('ar 3 7751',), {}),
    (('ne 5 14.32m',), {'ncrit': 4.7}),
    (('ne 5 24.31m',), {'ncrit': 4.4}),
    (('O  6 1032',), {'isForbidden':False}),
    (('Ne 6 7.652m',),{'ncrit':5.5}),#forbidden?
    (('Ne 8 770.4',),{'isForbidden':False}),
    (('Ne 8 780.3',),{'isForbidden':False}),
    (('TOTL 774.0',),{'isForbidden':False}),
    (('O  4 789.0',),{'isForbidden':False}),
    (('O  4 779.9',),{'isForbidden':False}),        
    (('C  3 1907',),{'ncrit':9.5}),
    (('C  3 1910',),{'ncrit':9.5}),
    (('TOTL 1909',),{'ncrit':9.5}),
    (('C  4 1548',),{'isForbidden':False}),    
    (('C  4 1551',),{'isForbidden':False}),
    (('TOTL 1549',),{'isForbidden':False}),
    (('He 2 1640',),{'isForbidden':False}),
    (('S  3 9532',),{'ncrit':5.8}),
    (('O  4 25.88m',),{'ncrit': 3.7}),
    (('Al 9 2.040m',),{'ncrit':8.3}),
    (('Si 6 1.963m',),{'ncrit':8.8}),
    (('Fe 2 1.257m',),{'ncrit':5.0}),
    (('Si 7 2.481m',),{}),#forbidden?
    (('S  3 18.67m',), {'ncrit': 4.2}),
    (('S  3 33.47m',), {'ncrit': 3.6}),
    (('Ne 2 12.81m',),{}), #forbidden?
    (('Ne 3 15.55m',), {'ncrit': 5.5}),
    (('Ne 3 36.01m',), {'ncrit': 4.7}),
    (('h  1 1.282m',), {'label': r'$Pa\beta$'}),
    (('h  1 1.875m',), {'label': r'$Pa\alpha$'}),
    (('O  1 63.17m',), {'ncrit': 4.7}),
    (('O  7 22.10',),{}),
    (('CaF1 7291',), {}),
    (('TOTL 2798',), {}),
] 

COnames = ["CO   2600m", "CO   1300m", "CO   866.7m","CO   650.1m","CO   520.1m","CO   433.4m","CO   371.5m","CO   325.1m","CO   289.0m","CO   260.2m","CO   236.5m","CO   216.9m","CO   200.2m",
           "CO   185.9m","CO   173.6m","CO   162.8m","CO   153.2m","CO   144.7m","CO   137.2m","CO   130.3m","CO   124.2m","CO   118.5m","CO   113.4m","CO   108.7m","CO   104.4m","CO   100.4m",
                     "CO   96.75m","CO   93.32m","CO   90.14m","CO   87.17m","CO   84.39m","CO   81.78m","CO   79.34m","CO   77.04m","CO   74.87m","CO   72.82m","CO   70.89m","CO   69.06m","CO   67.32m",
                     "CO   65.67m","CO   64.10m","CO   62.61m","CO   61.18m","CO   59.83m","CO   58.53m","CO   57.29m","CO   56.11m","CO   54.97m","CO   53.88m","CO   52.84m","CO   51.84m","CO   50.87m",
                     "CO   49.95m","CO   49.06m","CO   48.20m","CO   47.37m","CO   46.57m","CO   45.80m","CO   45.06m","CO   44.35m","CO   43.65m","CO   42.98m","CO   42.33m","CO   41.71m","CO   41.10m",
                     "CO   40.51m","CO   39.94m","CO   39.39m","CO   38.85m"]




emissionLines = [EmissionLine(*x[0],**x[1]) for x in LineNames]
emissionLines += [EmissionLine(name,isCO=True,iCO=iname) for iname,name in enumerate(COnames)]

emissionLinesDic = dict([(l.key(),l) for l in emissionLines])
