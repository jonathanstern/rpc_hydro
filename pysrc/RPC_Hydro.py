import sys, pdb, string, glob
sys.path.append('pysrc')
import Cloudy, Table, PlutoOutput as pp
import my_utils as u
import RPCgeneral as rpc
from pylab import *
from my_utils import log, array, rl, arange, concat, tplot, out, iround, roman, lens, zeros, unique , iround
import rc
import matplotlib.animation
import matplotlib.gridspec as gridspec
import astropy.units as un
import astropy.constants as cons
import multiprocessing
import BPT

#ion()
time_cmap =get_cmap('viridis_r')

#Cloudy.Model.basedir = '/media/jonathan/MyPassport/research/separate/rpc_hydro/CloudyRuns/'
Cloudy.Model.basedir = 'CloudyRuns/'


#u.updateTerminalTitle('RPC Hydro')
projectDir = './'
u.figureDir = u.figureDir2 = projectDir + 'figures/'
dataDir = projectDir + 'data/'
videoDir = pp.Run.runsDir + 'framesForVideo/'
pyobjDir = projectDir + 'pyobjs/'


labelDic = {(17.,0.): r'$A_{\rm V}=0,\ N_{\rm HI}=0$', 
            (17.5,17.5): r'$A_{\rm V}\approx 0,\ N_{\rm HI}=10^{17.5}$', 
            (19.,19.): r'$A_{\rm V}\approx 0,\ N_{\rm HI}=10^{19}$', 
            (21.,0.): r'$A_{\rm V}=0.4,\ N_{\rm HI}=0$',
            (21.,17.): r'$A_{\rm V}=0.4,\ N_{\rm HI}=10^{17}$',
            (22.,0.): r'$A_{\rm V}=4,\ N_{\rm HI}=0$',
            (22.,21.): r'$A_{\rm V}=4,\ N_{\rm HI}=10^{21}$',
            (23.,23.): r'$A_{\rm V}=40,\ N_{\rm HI}=10^{23}$',
            }
cs = ('k','.3','.6','r','y','b','c','g')
def to_key_and_zone(lNH,lNHI,AbsorbedSpectra,maxd=0.1):
    for k in sorted(AbsorbedSpectra.keys()):
        m = AbsorbedSpectra[k]
        NHIs = m.Nion('HI')
        NHs = m.localColumnDensity()
        for i in u.rl(NHs):
            if lNHI!=0:
                if abs(lNH-log(NHs[i]))<maxd and abs(lNHI-log(NHIs[i]))<maxd:
                    return k,i
            else:
                if abs(lNH-log(NHs[i]))<maxd:
                    return k,i
            



def opacities(grid,lng):
    u.figure(figsize=(rc.fig_width,4))
    u.subplots_adjust(top=0.9)
    for k in sorted(grid.keys()):
        m = grid[k]
        inH = list(grid.ns).index(m.n)
        if inH==0:
            plot(m.nus(False),m.grainOpacities,lw=2,c='r',label='dust')    
            ret_nus, ret_grainOpacities = m.nus(False),m.grainOpacities
            text(7e15, 2e-18, r'$U\leq10^{-7}$')        
        plot(m.nus(False),m.gasOpacitiesSingleZone(0,False),c='k',lw=0.5)
        Ulabel=u.arilogformat(10**(lng-m.n),dollarsign=False)
        if 5<inH<10: text(7e15, [7e-19,1e-19,9e-21,4e-22,3.6e-23][inH-5], r'$%s$'%Ulabel)
        if inH==8: text(1.7e16, 0.8e-20, r'$%s$'%Ulabel)
        if inH==9: text(1.7e16, 0.8e-21, r'$%s$'%Ulabel)
        if inH==10: text(4e16, 2.e-23, r'$%s$'%Ulabel)
        if inH==11: text(1.e17, 1e-23, r'$%s$'%Ulabel)
        if inH==12: text(3.e17, 0.25e-23, r'$%s$'%Ulabel)
        if inH==13: text(9.e17, 1e-24, r'$%s$'%Ulabel)
        if inH==14: text(6e17, 0.4e-24, r'$%s,\ 10^{5}$'%Ulabel)
    loglog()
    text(6e14, 6.5e-22, r'total grains (MRN)',color='r',ha='right')
    text(3e16, 3e-19, r'total gas ($U,\ Z=Z_\odot$)',color='k')
    ylim(1e-25,1e-17)
    xlim(0.6e14,1.5e18)
    u.ylabel(r'$\sigma_{\rm r.p.}$ [cm$^{2}$ per H]')
    xlabel(r'$\nu$ [Hz]')
    twiny()
    loglog()
    xlim(3e18/0.6e14,3e18/1.5e18)
    gca().xaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(u.wavelengthformat))
    xlabel('wavelength') 
    return ret_nus, ret_grainOpacities
def opacities_weighted(grid,lng,alpha_ion=-2.6):
    u.figure(figsize=(rc.fig_width,4))
    u.subplots_adjust(top=0.9)
    nu0 = 3e18/912.
    for k in sorted(grid.keys()):
        m = grid[k]
        inH = list(grid.ns).index(m.n)
        if inH==0:
            ind = np.searchsorted(m.nus(False)[1:-1], nu0)
            plot(m.nus(False)[1:-1][ind:],(m.grainOpacities[1:-1]*(m.nus(False)[1:-1]/nu0)**alpha_ion * m.dnus(False))[ind:].cumsum(),lw=2,c='r',label='dust')    
        Ulabel=u.arilogformat(10**(lng-m.n),dollarsign=True)
        if 3.>lng-m.n>-8:
            _ys = m.gasOpacitiesSingleZone(0,True)*(m.nus(True)/nu0)**alpha_ion* m.dnus(True)
            _ys = _ys*(_ys>0)
            ys = _ys.cumsum()
            plot(m.nus(True),ys,lw=0.5,label=Ulabel)
            ind = np.searchsorted(m.nus(True), 1e16)
            text(1e16,ys[ind],Ulabel)
    axvline(4.*nu0,c='k',ls=':')
        #if 5<inH<10: text(7e15, [7e-19,1e-19,9e-21,4e-22,3.6e-23][inH-5]*(7e15/nu0)**alpha_ion, r'$%s$'%Ulabel)
        #if inH==8: text(1.7e16, 0.8e-20*(1.7e16/nu0)**alpha_ion, r'$%s$'%Ulabel)
        #if inH==9: text(1.7e16, 0.8e-21*(1.7e16/nu0)**alpha_ion, r'$%s$'%Ulabel)
        #if inH==10: text(4e16, 2.e-23*(4e16/nu0)**alpha_ion, r'$%s$'%Ulabel)
        #if inH==11: text(1.e17, 1e-23*(1e17/nu0)**alpha_ion, r'$%s$'%Ulabel)
        #if inH==12: text(3.e17, 0.25e-23*(3.e17/nu0)**alpha_ion, r'$%s$'%Ulabel)
        #if inH==13: text(9.e17, 1e-24*(9.e17/nu0)**alpha_ion, r'$%s$'%Ulabel)
        #if inH==14: text(6e17, 0.4e-24*(6e17/nu0)**alpha_ion, r'$%s,\ 10^{5}$'%Ulabel)
        
    loglog()
    #ylim(1e-28,1e-17)
    xlim(nu0,1.5e18)
    u.ylabel(r'$\sigma_{\rm r.p.}(<\nu))$ [cm$^{2}$ per H] weighted by unattentuated spectrum')
    xlabel(r'$\nu$ [Hz]')
    twiny()
    loglog()
    xlim(912.,3e18/1.5e18)
    gca().xaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(u.wavelengthformat))
    xlabel('wavelength') 

def spectra(grid,lng,labelDic=labelDic,cs=cs):    
    figure(figsize=(rc.fig_width,5))
    subplots_adjust(top=0.9)
    ik = 0
    for (lNH,lNHI),label in sorted(labelDic.items()):
        k,iZone = to_key_and_zone(lNH, lNHI, grid)
        lw = (2,0.5)[ik>0]
        c = cs[ik]; ik+=1    
        m = grid[k]
        plot(m.nus(False), m.allSpectra[iZone,:],label=labelDic[(lNH,lNHI)],c=c,lw=lw)
    loglog()
    ylim(1e-2,3e3)
    xlim(0.6e14,1.5e18)
    u.mylegend(loc='lower left',handlelength=1)
    ylabel(r'$\nu F_\nu$ [erg s$^{-1}$ cm$^{-2}$]')
    xlabel(r'$\nu$ [Hz]')
    twiny()
    loglog()
    xlim(3e18/0.6e14,3e18/1.5e18)
    gca().xaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(u.wavelengthformat))
    xlabel('wavelength')
    gca().yaxis.set_major_formatter(u.arilogformatter)    
def weightedOpacity(tables,labelDic=labelDic,cs=cs,X=0.73825,dust2gas_ratio = 0.01):
    dustOptOpacityTable, dustIonOpacityTable, gasIonOpacityTable,fHITable = tables
    U0s  = 10.**arange(-10,5,.1)    
    rtf = figure(figsize=(rc.fig_width,5))
    subplots_adjust(wspace=0.3)
    for iPanel in range(2):
        subplot(1,2,iPanel+1)
        for ik,(k,label) in enumerate(sorted(labelDic.items())):            
            NHs = array([10**k[0]]*len(U0s))
            NHIs = array([10**k[1]]*len(U0s))
            Ts = array([10**4.]*len(U0s)) #change to T_eq
            if iPanel==0:
                opacities = (0.486 * dustOptOpacityTable(U0s,Ts,NHs,NHIs)*dust2gas_ratio +
                             0.514 * dustIonOpacityTable(U0s,Ts,NHs,NHIs)*dust2gas_ratio +
                             0.514 * gasIonOpacityTable(U0s,Ts,NHs,NHIs))
                plot(U0s,opacities*cons.m_p.to('g').value / X,label = labelDic[k],c=cs[ik])
            if iPanel==1:
                fHIs = fHITable(U0s,Ts,NHs,NHIs)
                plot(U0s,fHIs,c=cs[ik])            
        loglog()
        gca().xaxis.set_major_formatter(u.arilogformatter)
        xlabel(r'$U_0$')
        xlim(0.5e-10,2e5)
        
        if iPanel==0:
            u.mylegend(loc='upper right')
            ylabel(r'$\tilde{\sigma}_{\rm r.p.}$ [cm$^2$ per H]')
        if iPanel==1:
            ylabel(r'$f_{\rm HI}$') 
            gca().yaxis.set_major_formatter(u.arilogformatter)
    
def cooling(netCoolingTable,baseU0,labelDic=labelDic,cs=cs):
    Ts = 10.**arange(2,7.5,.01)
    U0s  = 10.**array([-8,-5,-2,0.,2,5])
    
    rtf = figure(figsize=(rc.fig_width,5))
    subplots_adjust(wspace=0.3)
    for iPanel in range(2):
        subplot(1,2,iPanel+1)
        if iPanel==0:
            for iU0,U0 in enumerate(U0s):
                c='kbcmry'[iU0]
                coolings = netCoolingTable(array([U0]*len(Ts)),Ts,
                                             array([0.]*len(Ts)),array([0.]*len(Ts))) 
                plot(Ts,coolings,label=r'$\log\ U_0=%s$'%(u.arilogformat(U0,dollarsign=False)),c=c)
                plot(Ts,-coolings,ls='--',c=c)
            text(0.95,0.05,r'$A_{\rm V}=0,\ N_{\rm HI}=0$',transform=gca().transAxes,ha='right')
        if iPanel==1:
            for ik,k in enumerate(sorted(labelDic.keys())):
                NHs = array([10**k[0]]*len(Ts))
                NHIs = array([10**k[1]]*len(Ts))
                coolings = netCoolingTable(array([baseU0]*len(Ts)),Ts,NHs,NHIs)
                plot(Ts,coolings,label=labelDic[k],c=cs[ik])
                plot(Ts,-coolings,ls='--',c=cs[ik])
            text(0.95,0.05,r'$U_0=%s$'%u.nSignificantDigits(baseU0,1,True),transform=gca().transAxes,ha='right')
        loglog()
        gca().xaxis.set_major_formatter(u.arilogformatter)
        xlabel(r'$T$ [K]')
        ylabel(r'$\Lambda$ [erg s$^{-1}$ cm$^3$]')
        xlim(10,1e9)    
        ylim(1e-28,1e-19)
        u.mylegend(loc='upper left')
        
def Teq(netCoolingTable,U0s  = 10.**array([-10,-8,-6,-4,-2,0.,2,4]),byNH=True):
    rtf  = figure(figsize=(rc.fig_width,5))
    Ts   = 10.**arange(2,7.5,.01)
    NHs = 10.**arange(19,23.,.025)
    NHIs = 10.**arange(15,23.,.025)
    
    
    for iU0,U0 in enumerate(U0s):
        c='kbcmry'[iU0%6]
        if byNH:
            Teqs = zeros(len(NHs))
            for iNH,NH in enumerate(NHs):                    
                coolings = netCoolingTable(array([U0]*len(Ts)),Ts,
                                           array([NH]*len(Ts)),
                                           array([NHIs[0]]*len(Ts)))                
                Teqs[iNH] = Ts[u.searchsorted(coolings,0.)]
            plot(NHs,Teqs,label=r'$\log\ U_0=%s$'%(u.arilogformat(U0,dollarsign=False)),c=c)
        else:
            Teqs = zeros(len(NHIs))
            for iNHI,NHI in enumerate(NHIs):                    
                coolings = netCoolingTable(array([U0]*len(Ts)),Ts,
                                           array([1e21+NHI]*len(Ts)),
                                           array([NHI]*len(Ts)))                
                Teqs[iNHI] = Ts[u.searchsorted(coolings,0.)]
            plot(NHIs,Teqs,label=r'$\log\ U_0=%s$'%(u.arilogformat(U0,dollarsign=False)),c=c)
    loglog()
    gca().xaxis.set_major_formatter(u.arilogformatter)
    ylabel(r'$T_{\rm eq}$ [K]')
    xlabel(r'$N_{\rm H%s}$ [cm$^{-2}$]'%(('I','')[byNH]))
    ylim(10,1e9)    
    xlim(1e15,1e24)
    u.mylegend(loc='upper left')

    
def nTplot2D(runs,iTs,variables,rngs,xls=None,yls=None,**kwargs):
    figure(figsize=(rc.fig_width/2.*len(variables),7))
    gss = (gridspec.GridSpec(len(iTs)+1,len(variables),height_ratios=[4]*len(iTs)+[1.5]),
           gridspec.GridSpec(len(iTs)+1,len(variables),height_ratios=[4]*len(iTs)+[1.5]))    
    gss[0].update(left=0.1,right=0.47,bottom=0.07,top=0.88)
    gss[1].update(left=0.53,right=0.9,bottom=0.07,top=0.88)
        
    for irun,run in enumerate(runs):
        r0 = run.r0/(un.pc/un.cm).to('')        
        for ivar,var in enumerate(variables):            
            logScale = not ( (var[0]=='L') or var in ('absorbedFrac','F2F0','v_r'))
            for iiT,iT in enumerate(iTs):
                ax = subplot(gss[irun][iiT,ivar])
                vmin,vmax = rngs[ivar]         
                
                run.plotter().plot2D(iT, var,logScale=logScale, title='',vmin=vmin,vmax=vmax,**kwargs)
                    
                text(0.95,0.9,run[iT].t.fullStr(withvar=False),ha='right',transform=ax.transAxes)
                if ivar==0:                    
                    ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x,pos,r0=r0: '%d'%(x-r0)))
                    ax.yaxis.set_major_locator(matplotlib.ticker.FixedLocator(r0+array([0.,1.,2.,3.])))
                else:
                    ax.yaxis.set_major_formatter(matplotlib.ticker.NullFormatter())
                if ivar==0 and irun==0:
                    ylabel(r'$\delta r\ [{\rm pc}]$')
                else:
                    ylabel('')
                    
                    
                if iiT==0:
                    ax.xaxis.set_ticks_position('top')
                    ax.xaxis.set_label_position('top')                    
                    if ivar==0:
                        text(0.25+0.5*irun, 0.95, r'$r=%.0f\,{\rm pc}$'%r0, transform=gcf().transFigure,ha='center')
                else:
                    xlabel('')
                    ax.xaxis.set_major_formatter(matplotlib.ticker.NullFormatter())
                if xls!=None: xlim(*xls)
                if yls!=None: ylim(*yls)
            cb = colorbar(ax=subplot(gss[irun][len(iTs),ivar],visible=False),
                          orientation='horizontal',pad=0.,shrink=1.,fraction=0.5)
            cb.set_label(str(run[iT][var])) #fontsize=14
            
def nTplot2D_new(run,iSnapshots,variables,rngs,xls=None,yls=None,**kwargs):
    figure(figsize=(rc.fig_width*1.05,4))
    gss = gridspec.GridSpec(len(variables),len(iSnapshots))
    gss.update(left=0.1,right=0.98,bottom=0.07,top=0.88)
        
    r0 = run.r0/(un.pc/un.cm).to('')        
    for ivar,var in enumerate(variables):            
        logScale = not ( (var[0]=='L') or var in ('absorbedFrac','F2F0','v_r'))
        axs = []
        for iiSnapshot,iSnapshot in enumerate(iSnapshots):
            ax = subplot(gss[ivar,iiSnapshot])
            axs.append(ax)
            vmin,vmax = rngs[ivar]         
            
            run.plotter().plot2D(iSnapshot, var,logScale=logScale, title='',vmin=vmin,vmax=vmax,**kwargs)
                
            
            if ax.is_first_col():  
                ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x,pos,r0=r0: '%d'%(x-r0)))
                ax.yaxis.set_major_locator(matplotlib.ticker.FixedLocator(r0+array([0.,1.,2.,3.])))
                ylabel(r'$\delta r\ [{\rm pc}]$')
            else:
                ax.yaxis.set_major_formatter(matplotlib.ticker.NullFormatter())                    
                ylabel(r'')
                
            #text(0.25+0.5*irun, 0.95, r'$r=%.0f\,{\rm pc}$'%r0, transform=gcf().transFigure,ha='center')
            if ax.is_first_row():
                title(run[iSnapshot].t.fullStr(withvar=False))
            if not ax.is_last_row():                        
                xlabel('')
                ax.xaxis.set_major_formatter(matplotlib.ticker.NullFormatter())
            if xls!=None: xlim(*xls)
            if yls!=None: ylim(*yls)
        cb = colorbar(ax=axs,orientation='vertical',fraction=0.1,pad=0.02)
        cb.set_label(str(run[iSnapshot][var])) #fontsize=14
        #cb.ax.yaxis.set_major_formatter(u.arilogformatter)




def framesForMovieMultiProcessor(Nprocs,runs,min_iT,max_iT,variables,rngs,xls=None,yls=None):
    pool = multiprocessing.Pool(processes=Nprocs,maxtasksperchild=1)
    for iT in range(min_iT,max_iT,1):
        pool.apply_async(framesForMovie, (runs,iT,iT+1,variables,rngs,xls,yls))
    return pool

def framesForMovie(runs,min_iT,max_iT,variables,rngs,xls=None,yls=None,**kwargs):
    u.labelsize(True)
    for iT in range(min_iT,max_iT,1):
        figure(figsize=(rc.fig_width,5.85))
        gss = (gridspec.GridSpec(2,len(variables),height_ratios=[4,1.5]),
               gridspec.GridSpec(2,len(variables),height_ratios=[4,1.5]))    
        gss[0].update(left=0.1,right=0.37,bottom=0.07,top=0.98)
        gss[1].update(left=0.63,right=0.9,bottom=0.07,top=0.98)        
        
        for irun,run in enumerate(runs):
            r0 = run.r0/(un.pc/un.cm).to('')        
            for ivar,var in enumerate(variables):                        
                ax = subplot(gss[irun][0,ivar])
                vmin,vmax = rngs[ivar]         
                logScale = not ( (var[0]=='L') or var in ('absorbedFrac','F2F0'))
                run.plotter().plot2D(iT, var,logScale=logScale, title='',vmin=vmin,vmax=vmax,**kwargs)                  
                
                if ivar==0:                    
                    ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x,pos,r0=r0: '%d'%(x-r0)))
                    ax.yaxis.set_major_locator(matplotlib.ticker.FixedLocator(r0+array([0.,1.,2.,3.])))
                else:
                    ax.yaxis.set_major_formatter(matplotlib.ticker.NullFormatter())
                if ivar==0:
                    ylabel(r'$\delta r\ [{\rm pc}]$')
                else:
                    ylabel('')
                    
                if xls!=None: xlim(*xls)
                if yls!=None: ylim(*yls)
            cb = colorbar(ax=subplot(gss[irun][1,ivar],visible=False),orientation='horizontal',pad=0.,shrink=1.,fraction=0.5)
            cb.set_label(str(run[iT][var])) #fontsize=14
        t = u.iround(runs[0][iT].t.cgs(),100)
        if t<1000: timestr = '%d'%t
        else: timestr='%d,%03d'%(t/1000,t%1000)
        ax.text(0.5,0.025,'t = %s yr'%timestr,ha='center',transform=gcf().transFigure,clip_on=False)
        savefig(videoDir+string.join([r.name for r in runs],'_')+'__'+string.join(variables,'_')+'_%03d.png'%iT)
        close('all')
        for irun,run in enumerate(runs):
            run.delSnapshot(iT)
    #mencoder "mf://B2D_N2048_R100_100_B2D_N2048_R10000_000__nH_???.png" -mf fps=40 -o comparison.avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=800
    
        


def framesForRPCMovieMultiProcessor(Nprocs,run,min_iT,max_iT,var,rng,ix,xls=None,yls=None,suffix='png'):
    pool = multiprocessing.Pool(processes=Nprocs,maxtasksperchild=1)
    for iT in range(min_iT,max_iT):
        pool.apply_async(RPC_movie, (run,iT,var,rng,ix,xls,yls,suffix))
    return pool


def RPC_movie(run,iT,var,rng,ix=205,dx=25,xls=None,yls=None,suffix='png',**kwargs):
    u.labelsize(True)
    figure(figsize=(rc.fig_width,5.85))
    gss = (gridspec.GridSpec(2,1,height_ratios=[4,1.5]),
           gridspec.GridSpec(2,1,height_ratios=[4,1.5]))
    gss[0].update(left=0.1,right=0.39,bottom=0.07,top=0.97)
    gss[1].update(left=0.58,right=0.87,bottom=0.07,top=0.97)        
    
    r0 = run.r0/(un.pc/un.cm).to('')        
    ax = subplot(gss[0][0,0])
    logScale = not ( (var[0]=='L') or var in ('absorbedFrac','F2F0'))
    run.plotter().plot2D(iT, var,logScale=logScale, title='',vmin=rng[0],vmax=rng[1],**kwargs)                  
    
    ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x,pos,r0=r0: '%d'%(x-r0)))
    ax.yaxis.set_major_locator(matplotlib.ticker.FixedLocator(r0+array([0.,1.,2.,3.])))
    ylabel(r'$\delta r\ [{\rm pc}]$')
        
    if xls!=None: xlim(*xls)
    if yls!=None: ylim(*yls)
    cb = colorbar(ax=subplot(gss[0][1,0],visible=False),orientation='horizontal',pad=0.,shrink=1.,fraction=0.5)
    cb.set_label(str(run[iT][var])) #fontsize=14

    sx,mx,ex = np.array([ix-dx,ix,ix+dx])
    xrng = run[iT].x.cgs()[np.array([sx,ex])]
    tau_rng = np.array([0.01,10])
    
    taus = (run[iT].tau_HI.cgs()[:,mx] + 2e-21*(un.pc/un.cm).to('')*(run[iT].r.d()*run[iT].nH.cgs()[:,mx]).cumsum())
    sr,er = u.searchsorted(taus, tau_rng)
    rrng = run[iT].r.cgs()[np.array([sr,er])]
    
    ax.add_patch(matplotlib.patches.Rectangle((xrng[0],rrng[0]), (xrng[1]-xrng[0]), (rrng[1]-rrng[0]),fc='none',ec='w',lw=1))
    annotate('',(1,100.8),(xrng[1]+0.02,u.mean(rrng)), arrowprops={'width':0.3,'headwidth':1.5,'frac':0.15,'color':'.3'},fontsize=8,ha='center')

    ax = subplot(gss[1][0])
    sca(ax)
    ys = run[iT].nH.cgs()[sr:er,mx]*un.cm**-3
    plot(taus[sr:er],ys,c='k')

    static_solution = np.load(pyobjDir+'static_solution.npz')
    plot(static_solution['taus'],static_solution['ns'],c='.75',lw=6,zorder=-10,alpha=0.5)
        
    loglog()
    xls = np.array([0.01,10.])
    yls = np.array([6,1.5e5])
    xlim(*xls)
    ylabel('hydrogen density [cm$^{-3}$]')
    xlabel(r'optical depth (1 ryd)')
    ylim(*yls)
    ax.xaxis.set_major_formatter(u.arilogformatter)
    gca().xaxis.set_major_locator(matplotlib.ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
    gca().xaxis.set_minor_locator(matplotlib.ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
    ax.yaxis.set_major_formatter(u.arilogformatter)
    annotate('static\nsolution',(0.15,200),(1,35),arrowprops=u.slantlinepropsblack,ha='center')
    ax2=twinx()
    ax2.set_yscale('log')
    ngamma = (run.LAGN* pp.ionFluxFraction * un.erg/un.s / (4*u.pi*(run[iT].r.cgs()[sr:er].mean()*un.pc)**2) / pp.meanhnu /cons.c).to('cm**-3')
    ax2.set_ylim(*((ngamma/(1.3*yls*un.cm**-3)).to('').value)) #1.3 is ratio of electrons to hydrogen atoms    
    ax2.yaxis.set_major_formatter(u.arilogformatter)
    ylabel('ionization parameter')

    t = u.iround(run[iT].t.cgs(),100)
    if t<1000: timestr = '%d'%t
    else: timestr='%d,%03d'%(t/1000,t%1000)
    ax.text(0.5,0.025,'t = %s yr'%timestr,ha='center',transform=gcf().transFigure,clip_on=False)
    savefig(videoDir+run.name+'__'+var+'_%03d.%s'%(iT,suffix))
    #mencoder "mf://B2D_N2048_R100_100__nH_???.png" -mf fps=20 -o RPC.avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=800
    
def comparison_to_static_solution_by_time(run,iTs,var,rng,ix,xls=None,yls=None,suffix='png',**kwargs):
    figure(figsize=(rc.fig_width/2.1,3))    
    r0 = run.r0/(un.pc/un.cm).to('')        
    tau_rng = 0.01,100
    static_solution = np.load(pyobjDir+'static_solution.npz')
    
    ax = subplot(111)
    for iiT,iT in enumerate(iTs):
        taus = (run[iT].tau_HI.cgs()[:,ix] + 2e-21*(un.pc/un.cm).to('')*(run[iT].r.d()*run[iT].nH.cgs()[:,ix]).cumsum())
        sr,er = u.searchsorted(taus, tau_rng)
        if er==run[0].r.shape()[0]: er-=1             
        rrng = run[iT].r.cgs()[np.array([sr,er])]    
        ys = run[iT].nH.cgs()[sr:er,ix]*un.cm**-3
        
        label='$t = %d$ kyr'%u.iround(run[iT].t.cgs()/1000)
        c = time_cmap(iiT*1./(len(iTs)-1))
        plot(taus[sr:er],ys,c=c,label=label)

    plot(static_solution['taus'],static_solution['ns'],c='.75',lw=6,zorder=-10,alpha=0.5)        
    loglog()
    xlim(*tau_rng)
    xlabel(r'optical depth (1 ryd)')
    yls = np.array([3,3e5])
    ylim(*yls)          
    ax.xaxis.set_major_formatter(u.arilogformatter)
    ax.xaxis.set_major_locator(matplotlib.ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
    ax.xaxis.set_minor_locator(matplotlib.ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
    legend(ncol=2,fontsize=8,frameon=False)
    ylabel('hydrogen density [cm$^{-3}$]')        
    ax.yaxis.set_major_formatter(u.arilogformatter)        
    annotate('static\nsolution',(0.15,200),(1,35),arrowprops=u.slantlinepropsblack,ha='center')            
    text(0.0125,2e4,r'$x = %.1f$ pc'%run[iT].x.cgs()[ix],fontsize=8)
    ax2=twinx()
    ax2.set_yscale('log')
    ngamma = (run.LAGN* pp.ionFluxFraction * un.erg/un.s / (4*u.pi*(run[iTs[0]].r.cgs()[sr:er].mean()*un.pc)**2) / pp.meanhnu /cons.c).to('cm**-3')
    ax2.set_ylim(*((ngamma/(1.3*yls*un.cm**-3)).to('').value)) #1.3 is ratio of electrons to hydrogen atoms    
    ax2.yaxis.set_major_formatter(u.arilogformatter)
    ylabel('ionization parameter')
    
def comparison_to_static_solution_by_x(run,iT,var,rng,ixs,xls=None,yls=None,suffix='png',dx=25,**kwargs):
    figure(figsize=(rc.fig_width,3))    
    r0 = run.r0/(un.pc/un.cm).to('')        
    tau_rng = 0.01,100
    static_solution = np.load(pyobjDir+'static_solution.npz')
    
    ax = subplot(121)
    for iixs,ix in enumerate(ixs):              
        taus = (run[iT].tau_HI.cgs()[:,ix] + 2e-21*(un.pc/un.cm).to('')*(run[iT].r.d()*run[iT].nH.cgs()[:,ix]).cumsum())
        sr,er = u.searchsorted(taus, tau_rng)
        if er==run[0].r.shape()[0]: er-=1             
        rrng = run[iT].r.cgs()[np.array([sr,er])]    
        ys = run[iT].nH.cgs()[sr:er,ix]*un.cm**-3
        
        label='$x = %.1f$ pc'%run[iT].x.cgs()[ix]
        c = time_cmap(1 - ix*1./run[iT].x.shape()[0])
        plot(taus[sr:er],ys,c=c,label=label)

    plot(static_solution['taus'],static_solution['ns'],c='.75',lw=6,zorder=-10,alpha=0.5)        
    loglog()
    xlim(*tau_rng)
    xlabel(r'optical depth (1 ryd)')
    yls = np.array([3,3e5])
    ylim(*yls)          
    ax.xaxis.set_major_formatter(u.arilogformatter)
    ax.xaxis.set_major_locator(matplotlib.ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
    ax.xaxis.set_minor_locator(matplotlib.ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
    legend(ncol=2,fontsize=8,frameon=False)
    ylabel('hydrogen density [cm$^{-3}$]')        
    ax.yaxis.set_major_formatter(u.arilogformatter)        
    annotate('static\nsolution',(0.15,200),(1,35),arrowprops=u.slantlinepropsblack,ha='center')            
    text(0.0125,2e4,'$t = %d$ kyr'%u.iround(run[iT].t.cgs()/1000),fontsize=8)
    
    ax = subplot(122)
    run.plotter().plot2D(iT, 'nH',logScale=True, title='',vmin=10,vmax=1e5)
    
    qd = 64
    quiver(run[iT].x.cgs()[::qd],run[iT].r.cgs()[::qd*2],run[iT].v_theta.cgs()[::qd*2,::qd],run[iT].v_r.cgs()[::qd*2,::qd],color='w')
    
    for iixs,ix in enumerate(ixs):              
        sx,mx,ex = np.array([ix-dx,ix,ix+dx])
        xrng = run[iT].x.cgs()[np.array([sx,ex])]        
        taus = (run[iT].tau_HI.cgs()[:,ix] + 2e-21*(un.pc/un.cm).to('')*(run[iT].r.d()*run[iT].nH.cgs()[:,ix]).cumsum())
        sr,er = u.searchsorted(taus, tau_rng)
        if er==run[0].r.shape()[0]: er-=1             
        rrng = run[iT].r.cgs()[np.array([sr,er])]
        c = time_cmap(1 - ix*1./run[iT].x.shape()[0])
        ax.add_patch(matplotlib.patches.Rectangle((xrng[0],rrng[0]), (xrng[1]-xrng[0]), (rrng[1]-rrng[0]),fc='none',ec=c,lw=1))
    ax.yaxis.set_label_position('right')
    ax.yaxis.set_ticks_position('right')
    ax.yaxis.set_ticks_position('both')


    #ax = subplot(133)
    #run.plotter().plot2D(iT, 'v_r',logScale=False, title='',vmin=-200,vmax=200)
def forOmer(run,iT,var,rng,ix=205,dx=25,xls=None,yls=None,**kwargs):
    u.labelsize(True)
    figure(figsize=(rc.fig_width/2.7,5.85))
    gss = gridspec.GridSpec(2,1,height_ratios=[4,1.5])
    gss.update(left=0.25,right=0.9,bottom=0.07,top=0.97)
    
    r0 = run.r0/(un.pc/un.cm).to('')        
    ax = subplot(gss[0,0])
    logScale = not ( (var[0]=='L') or var in ('absorbedFrac','F2F0'))
    run.plotter().plot2D(iT, var,logScale=logScale, title='',vmin=rng[0],vmax=rng[1],**kwargs)                  
    
    ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x,pos,r0=r0: '%d'%(x-r0)))
    ax.yaxis.set_major_locator(matplotlib.ticker.FixedLocator(r0+array([0.,1.,2.,3.])))
    ax.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(0.5))
    ylabel(r'$\delta r\ [{\rm pc}]$')
        
    xlim(0,1.5001)
    ylim(100,103.)
    cb = colorbar(ax=subplot(gss[1,0],visible=False),orientation='horizontal',pad=0.,shrink=1.,fraction=0.5)
    cb.set_label(str(run[iT][var])) #fontsize=14

    savefig(videoDir+'forOmer.png',dpi=1200)
    #mencoder "mf://B2D_N2048_R100_100__nH_???.png" -mf fps=20 -o RPC.avi -ovc lavc


def velocity_profile(run,iT,var,rng,ix=205,xls=None,yls=None,**kwargs):
    u.labelsize(True)
    figure(figsize=(rc.fig_width*0.96,5.85))
    gss = (gridspec.GridSpec(2,1,height_ratios=[4,1.5]),
           gridspec.GridSpec(2,1,height_ratios=[4,1.5]))
    gss[0].update(left=0.1,right=0.39,bottom=0.07,top=0.98)
    gss[1].update(left=0.61,right=0.9,bottom=0.07,top=0.98)        
    
    r0 = run.r0/(un.pc/un.cm).to('')        
    ax = subplot(gss[0][0,0])
    logScale = not ( (var[0]=='L') or var in ('absorbedFrac','F2F0'))
    run.plotter().plot2D(iT, var,logScale=logScale, title='',vmin=rng[0],vmax=rng[1],**kwargs)                  
    
    ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x,pos,r0=r0: '%d'%(x-r0)))
    ax.yaxis.set_major_locator(matplotlib.ticker.FixedLocator(r0+array([0.,1.,2.,3.])))
    ylabel(r'$\delta r\ [{\rm pc}]$')
        
    if xls!=None: xlim(*xls)
    if yls!=None: ylim(*yls)
    cb = colorbar(ax=subplot(gss[0][1,0],visible=False),orientation='horizontal',pad=0.,shrink=1.,fraction=0.5)
    cb.set_label(str(run[iT][var])) #fontsize=14

    sx,mx,ex = np.array([ix-25,ix,ix+25])
    xrng = run[iT].x.cgs()[np.array([sx,ex])]
    tau_rng = np.array([0.01,10])
    
    taus = (run[iT].tau_HI.cgs()[:,mx] + 2e-21*(un.pc/un.cm).to('')*(run[iT].r.d()*run[iT].nH.cgs()[:,mx]).cumsum())
    sr,er = u.searchsorted(taus, tau_rng)
    rrng = run[iT].r.cgs()[np.array([sr,er])]
    
    ax.add_patch(matplotlib.patches.Rectangle((xrng[0],rrng[0]), (xrng[1]-xrng[0]), (rrng[1]-rrng[0]),fc='none',ec='k',lw=1))
    annotate('',(1,100.8),(xrng[1]+0.02,u.mean(rrng)), arrowprops={'width':0.3,'headwidth':1.5,'frac':0.15,'color':'.3'},fontsize=8,ha='center')

    ax = subplot(gss[1][0])
    sca(ax)
    ys = run[iT].nH.cgs()[sr:er,mx]*un.cm**-3
    ys = run[iT].v_r.cgs()[sr:er,mx]
    plot(taus[sr:er],ys,c='k')

    #static_solution = np.load(pyobjDir+'static_solution.npz')
    #plot(static_solution['taus'],static_solution['ns'],c='.75',lw=6,zorder=-10,alpha=0.5)
        
    #loglog()
    semilogx()
    xlim(0.01,10)
    #ylabel('gas density [cm$^{-3}$]')
    xlabel(r'optical depth (1 ryd)')
    #ylim(100,1e5)
    ax.xaxis.set_major_formatter(u.arilogformatter)
    gca().xaxis.set_major_locator(matplotlib.ticker.LogLocator(base=10.,subs=(1.,),numticks=12))
    gca().xaxis.set_minor_locator(matplotlib.ticker.LogLocator(base=10.,subs=range(2,10),numticks=12))
    #ax.yaxis.set_major_formatter(u.arilogformatter)
    annotate('static\nsolution',(0.235,500),(2,0.2e3),arrowprops=u.slantlinepropsblack,ha='center')

    t = u.iround(run[iT].t.cgs(),100)
    if t<1000: timestr = '%d'%t
    else: timestr='%d,%03d'%(t/1000,t%1000)
    ax.text(0.5,0.025,'t = %s yr'%timestr,ha='center',transform=gcf().transFigure,clip_on=False)
    #savefig(videoDir+run.name+'__'+var+'_%03d.png'%iT)


def skewers(runs,xaxisvar, variables,rngs, iTs, ix=0,logx=False,logy=True,isRPC=True,**kwargs):
    figure(figsize=(rc.fig_width/2,2))
    subplots_adjust(hspace=0.1,wspace=0.45)
    for ivar,var in enumerate(variables): 
        for irun,run in enumerate(runs):
            ax = subplot(len(variables),len(runs),len(runs)*ivar+irun+1)
            for iiT,iT in enumerate(iTs):                
                xs = run[iT][xaxisvar].cgs()
                if run[iT][xaxisvar].dimension==[1,2]:
                    xs = xs[:,ix]
                c = time_cmap(iiT*1./(len(iTs)-1))
                plot(xs, run[iT][var].cgs()[:,ix],label=run[iT].t.fullStr(withvar=False),c=c)
                front = (run[iT].fHII.cgs()[:,ix]<0.1).nonzero()[0]
                if iT>0 and len(front):
                    x = xs[front[0]]
                    axvline(x,c=c,ls=':')
                    if isRPC and iiT==len(iTs)-1: y=300 
                    elif not isRPC and iiT==1: y=1e5
                    else: continue
                    annotate("ion' front",(x+0.03,y),(x+0.15,y),arrowprops=u.slantlinepropsblack,va='center')
                         
            ylabel(run[iTs[0]][var])
            ylim(rngs[ivar])
            if not logx:
                if logy:
                    semilogy()
            else:
                if logy:
                    loglog()
                else:
                    semilogx()
                ax.xaxis.set_major_formatter(u.arilogformatter)
            if logy:
                ax.yaxis.set_major_formatter(u.arilogformatter)
            if ax.is_last_row():
                xlabel(run[iTs[0]][xaxisvar])            
            else:
                ax.xaxis.set_major_formatter(matplotlib.ticker.NullFormatter())
            if ivar==0 and irun==0:
                u.mylegend(loc='upper right',handlelength=0.5,columnspacing=0.7,handletextpad=0.35)
            if xaxisvar=='F2F0':
                xlim(1.1,-0.1)
                if var=='P2k' and irun==0:
                    hydrostaticxs = arange(0,1.005,0.01)
                    hydrostaticPs = run[iT].F0.cgs()[ix] / cons.c.to('cm/s').value / cons.k_B.to('erg/K').value * (1-hydrostaticxs)
                    plot(hydrostaticxs, hydrostaticPs,c='.75',lw=6,zorder=-10,alpha=0.5)
                    Pdata = hydrostaticPs[u.searchsortedclosest(hydrostaticxs,0.05)]
                    annotate('hydrostatic solution',(0.05,Pdata),(0.1,2*Pdata),arrowprops=u.slantlinepropsblack,ha='right')
            



def r_dependence_calc(runs,iTs=[100,200,300,400,500],dlogn=0.1):
    res = [None]*len(runs)
    bins=10.**arange(-10,15,dlogn)
    for irun,run in enumerate(runs):
        vals = array([])
        weights = array([])
        for iT in iTs:
            vals = concat([vals,run[iT].nH.cgs().flatten()])
            snapshot_weights = run[iT].nHII.cgs().flatten()*run[iT].nH.cgs().flatten()
            weights = concat([weights, snapshot_weights])
        res[irun] = histogram(vals,bins=bins,weights=weights/len(iTs))
    return res
    
def r_dependence_figs(res,runs,T=1e4*un.K,meanhnu=36*un.eV,dlogn=0.1,iT0=100,save=True):
    bins=10.**arange(-10,15,dlogn)
    u.figure(figsize=(rc.fig_width,4))
    subplots_adjust(hspace=0.4)
    for iPanel in range(2):
        subplot(2,1,iPanel+1)
        for irun,run in enumerate(runs):
            c = 'kbcry'[irun]            
            ng = (run.LAGN*un.erg/un.s * (1-run.optFrac) / (4*pi*(run.r0*un.cm)**2*cons.c.to('cm/s')*meanhnu)).to('cm**-3')
            if iPanel==0:
                u.bar(u.edge2Mean(bins),res[irun][0],width = bins[1:]-bins[:-1],color='none',edgecolor=c,
                      label='%s pc'%u.nSignificantDigits(run.r0/3e18,1,True))
                axvline((ng*meanhnu / (2*cons.k_B.to('erg/K')*T)).to('cm**-3').value,c=c,ls=':')
            if iPanel==1:                                
                u.bar(ng.value/u.edge2Mean(bins),res[irun][0],width = ng.value/bins[1:]-ng.value/bins[:-1],color='none',edgecolor=c)  
        semilogx()
        gca().xaxis.set_major_formatter(u.arilogformatter)                
        if iPanel==0:
            xlabel(runs[0][iT0].nH)
            ylabel(r'$L_{\rm gas}/\Omega$ [${\rm erg}~{\rm s}^{-1}$]')
            u.xlim(1,1e12)
            #u.ylim(0,0.3e19)
            u.mylegend(loc='upper right')
        if iPanel==1:
            xlabel(r'$U$')  
            ylabel(r'$L_{\rm gas}/\Omega$ [${\rm erg}~{\rm s}^{-1}$]')
            u.xlim(1e-7,1)
            #u.ylim(0,0.3e19)
            axvline((2*cons.k_B.to('erg/K')*T/meanhnu).to(''),c='.5',ls=':')
    if save:
        u.mysavefig('density_distribution')
        
    rs = array([run.r0 for run in runs])/3e18
    mean_nHs = [(res[irun][0] * u.edge2Mean(res[irun][1])).sum() / res[irun][0].sum() for irun in u.rl(runs)]
    expected_nHs = [(run.LAGN*un.erg/un.s * (1-run.optFrac) / (4*pi*(run.r0*un.cm)**2*cons.c.to('cm/s') *2*cons.k_B*T)).to('cm**-3').value
                    for irun,run in enumerate(runs)]
    mean_Us      = [(run.LAGN*un.erg/un.s * (1-run.optFrac) / (4*pi*(run.r0*un.cm)**2*cons.c.to('cm/s') *meanhnu) / (mean_nHs[irun]*un.cm**-3)).to('').value 
                    for irun,run in enumerate(runs)]
    u.figure()
    u.subplots_adjust(hspace=0.3)
    for iPanel in range(2):
        u.subplot(2,1,iPanel+1)
        if iPanel==0:
            u.plot(rs,mean_nHs,'x',c='k')            
            u.plot(rs,expected_nHs,'--',c='k')            
            u.ylabel(r'$n_{\rm H}$ [cm$^{-3}$]')
            #u.axhline(runs[0].rho0* pp.X / (un.mp/un.g),c='k',ls=':')            
            u.ylim(3e2,3e5)
        if iPanel==1:
            u.plot(rs,mean_Us,'x',c='k')            
            u.ylabel(r'$U$')
            u.axhline((2*cons.k_B*T/meanhnu).to('').value,c='k',ls='--')
        u.gca().xaxis.set_major_formatter(u.arilogformatter)
        u.loglog()
        u.xlabel(r'$r$ [pc]')
    if save:
        u.mysavefig('density_vs_r')


def r_dependence_figs_new(res,runs,T=1e4*un.K,meanhnu=36*un.eV,dlogn=0.1,iT0=100,d=1,save=True,norm_by_Lbol=False):
    bins=arange(-10,15,dlogn)
    u.figure(figsize=(rc.fig_width/2,4))
    subplots_adjust(hspace=0.4)
    ax = subplot(111)    
    for irun,run in enumerate(runs):
        c = 'kbcry'[irun]            
        lng = log((run.LAGN*un.erg/un.s * (1-run.optFrac) / (4*pi*(run.r0*un.cm)**2*cons.c.to('cm/s')*meanhnu)).to('cm**-3').value)
        xs = lng - log(1.3)-u.edge2Mean(bins)
        if norm_by_Lbol: norm = run.LAGN
        else:            norm = res[irun][0].sum()
        ys = res[irun][0] / norm
        u.step(xs,ys,color=c,label='$L=%.0f$'%log(run.LAGN))
    xlabel(r'$\log\ U$')  
    ylabel(r'$L(U)/L({\rm total})$')
    u.xlim(-4.,0.)
    if not norm_by_Lbol: u.ylim(0,0.4)
    U_RPC = (2.3*cons.k_B.to('erg/K')*T/meanhnu).to('')
    axvline(log(U_RPC),c='.7',ls=':')
    u.mylegend(loc='upper left')
    annotate(r'$2.3kT/\langle h\nu\rangle$', (log(U_RPC)+0.05,0.27),(log(U_RPC)+0.25,0.27),arrowprops=u.slantlinepropsblack,va='center')
    text(0.95,0.95,r'$t=%.0f\ {\rm kyr}$'%iT0,ha='right',va='top',transform=ax.transAxes)        
def CalculateCooling(grid):
    # load fullGrid from RPC vs L/c
    rtf = u.rtfig(figsize=(8,6))
    subplots_adjust(left=0.15,right=0.95,bottom=0.15)
    u.labelsize(True)
    vals = grid(lambda self: (self.U0(),self.totalCooling[0]/(10**self.nH[0])**2)).values()
    tplot(vals)
    loglog()
    rtf.xformat(u.arilogformatter)
    xlabel(r'$U \equiv \frac{\phi}{n_{\rm H} c}$')
    ylabel(r'$\frac{\Lambda}{{\rm n_H}^2}\ \ ({\rm erg}\,{\rm cm}^3\,{\rm s}^{-1})$')
    a,b,c =polyfit2(vals, 3)
    text(3e-5,3e-21,r'$\log \frac{\Lambda}{n_{\rm H}^2} = %.3f(\log U)^2 %s\log U %s$'%(a,u.signedFloatStr(b,'%.3f'),u.signedFloatStr(c,'%.3f')))
    u.mysavefig('cooling_vs_U')
    

    
def FixCooling(outfile='HeatingCoolingTable.txt'):
    ls = u.filelines(dataDir + 'old' + outfile)[1:]
    f = file(dataDir + outfile,'w')
    f.write('%20s %20s %20s\n'%('U','T (K)','Cool [erg cm^3 s^-1]'))
    for l in ls:
        U,T,C,H = map(float,string.split(l))
        C2 = C-H
        f.write('%20.4g %20.4g %20.4g\n'%(U,T,C2))
    f.close()



def EnergyDensityToIonizingPhotonDensity(aion,model):
    meanIonEnergy = aion / (aion+1) * ld.RydbergEV  #in eV
    Lion2Ltotal = model.ionizingFluxFraction()
    return meanIonEnergy / Lion2Ltotal
def MeanOpacity(model):
    return model.meanSigmaDust()
def BPTplot(grid):
    tups = (((5006.84,),(4861.33,),'OIII/Hb'),
                     ((6583.45,),(6562.81,),'NII/Ha'),
                     ((6730.82, 6716.44,),(6562.81,),'SII/Ha'),
                     ((6300.3,),(6562.81,),'OI/Ha'))
    log_rs = []
    BPTdic = {}
    for k,m in sorted(grid.items()):
        if not m.CloudyOk():
            continue
        log_L,log_r,simname = k        
        log_rs.append(log_r)        
        print 'r=%.1f'%k[1],
        for tup in tups:
            num = np.sum([10.**m.linesdicT[l] for l in tup[0]])
            denom = np.sum([10.**m.linesdicT[l] for l in tup[1]])            
            print '%s: %.2f'%(tup[2],log(num/denom)),
            BPTdic[('%.1f'%log_r,tup[2])] = log(num/denom)
        print
    axlist = BPT.figBPT_overKewley()
    ys = [BPTdic[('%.1f'%log_r,tups[0][2])] for log_r in log_rs]
    for iax,ax in enumerate(axlist):
        sca(ax)
        xs = [BPTdic[('%.1f'%log_r,tups[iax+1][2])] for log_r in log_rs]
        plot(xs,ys,c='k',marker='s',mec='k',mfc='w')
def BPTplot_byL(grid):
    tups = (((5006.84,),(4861.33,),'OIII/Hb'),
                     ((6583.45,),(6562.81,),'NII/Ha'),
                     ((6730.82, 6716.44,),(6562.81,),'SII/Ha'),
                     ((6300.3,),(6562.81,),'OI/Ha'))
    log_Ls = []
    BPTdic = {}
    for k,m in sorted(grid.items()):
        if not m.CloudyOk():
            continue
        log_L,log_r,simname = k        
        log_Ls.append(log_L)        
        print 'L=%.0f'%k[0],
        for tup in tups:
            num = np.sum([10.**m.linesdicT[l] for l in tup[0]])
            denom = np.sum([10.**m.linesdicT[l] for l in tup[1]])            
            print '%s: %.2f'%(tup[2],log(num/denom)),
            BPTdic[('%.0f'%log_L,tup[2])] = log(num/denom)
        print
    axlist = BPT.figBPT_overKewley()
    ys = [BPTdic[('%.0f'%log_L,tups[0][2])] for log_L in log_Ls]
    cmap = get_cmap('viridis')
    for iax,ax in enumerate(axlist):
        sca(ax)
        xs = [BPTdic[('%.0f'%log_L,tups[iax+1][2])] for log_L in log_Ls]        
        scatter(xs,ys,marker='s',s=30,facecolors = cmap((np.array(log_Ls)-43)/3))
        
    
def BPTplot_byL_HSE(grid):
    tups = (((5006.84,),(4861.33,),'OIII/Hb'),
                     ((6583.45,),(6562.81,),'NII/Ha'),
                     ((6730.82, 6716.44,),(6562.81,),'SII/Ha'),
                     ((6300.3,),(6562.81,),'OI/Ha'))
    log_Ls = []
    BPTdic = {}
    k = lambda log_L, aEUV, log_r: '%.0f %.1f %.0f'%(log_L,aEUV, 10**log_r/3e18)
    for (aEUV,log_L,log_r),m in sorted(grid.items()):
        if not m.CloudyOk(): continue
        log_Ls.append(log_L)        
        print 'L=%.0f'%log_L,
        for tup in tups:
            num = np.sum([10.**m.linesdicT[l] for l in tup[0]])
            denom = np.sum([10.**m.linesdicT[l] for l in tup[1]])            
            print '%s: %.2f'%(tup[2],log(num/denom)),
            BPTdic[(k(log_L,aEUV,log_r),tup[2])] = log(num/denom)
        print
    axlist = BPT.figBPT_overKewley()
    cmap = get_cmap('viridis')
    log_Ls = np.unique(log_Ls)
    for aEUV in grid.aEUVs:
        for log_r in grid.radii:            
            ys = [BPTdic[(k(log_L,aEUV,log_r),tups[0][2])] for log_L in log_Ls]    
            for iax,ax in enumerate(axlist):
                sca(ax)
                xs = [BPTdic[(k(log_L,aEUV,log_r),tups[iax+1][2])] for log_L in log_Ls]
                scatter(xs,ys,marker='s',s=30, facecolors = cmap((np.array(log_Ls)-42)/3))
                plot(xs,ys)
    
        
