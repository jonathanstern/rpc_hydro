from pyPLUTO import *
import ListsAndDics as ld
import my_utils as u
from my_utils import array, rl, unique, zeros, lens, iround, log, arange
import string
import pylab as pl
import astropy.units as un
import astropy.constants as cons
#from mpl_toolkits.mplot3d import Axes3D
#import rc
projectDir = ''
dataDir = projectDir + 'data/'

   

def searchAndInterpolateMultiD(arrs_xs,vals,matrix_ys):	
    lenarray = lens(arrs_xs)
    inds = array([u.searchsortedclosest(arrs_xs[i],vals[i]) for i in rl(vals)])
    y0 = matrix_ys[tuple(inds)]
    res = float(y0)
    for i in rl(vals):
        offset = vals[i] - arrs_xs[i][inds[i]]
        if   arrs_xs[i][0]<arrs_xs[i][1]: adjacent =  u.sign(offset)
        elif arrs_xs[i][0]>arrs_xs[i][1]: adjacent = -u.sign(offset) ## decreasing array
        tmp_inds = array(inds)
        tmp_inds[i] = inds[i] + adjacent        
        if (tmp_inds[i]<0) or (tmp_inds[i]>lenarray[i]-1) or (tmp_inds[i]==inds[i]): ## take extreme value if beyond table range
            derivative = 0.
        else:
            derivative = (matrix_ys[tuple(tmp_inds)] - y0) / (arrs_xs[i][tmp_inds[i]] - arrs_xs[i][inds[i]])
        res += derivative * offset
    return res


class Table:
    def __init__(self, properties, filename, firstLine,calculatedValueColumn=0):
        self.properties = properties
        self.filename = filename
        
        ls = u.filelines(self.filename)[firstLine:]
        data = array([[float(x) for x in string.split(l)] for l in ls])

        self.propertyValues = []; self.minVals = []; self.maxVals = []
        for iProp in rl(self.properties):
            self.propertyValues.append( array(sorted(unique([log(d[iProp]) for d in data])) ) )
            self.minVals.append(10.**self.propertyValues[-1].min())
            self.maxVals.append(10.**self.propertyValues[-1].max())
        
        self.calculatedValueMatrix = zeros(lens(self.propertyValues))
        for d in data:
            inds = tuple([u.searchsortedclosest(self.propertyValues[iProp], log(d[iProp])) for iProp in rl(self.properties)])
            self.calculatedValueMatrix[inds] = d[len(self.properties)+calculatedValueColumn]
    def __call__(self,*args):
        if type(args[0])==u.numpy.ndarray:
            flattenFunc = lambda x: x.flatten()
            deflattenFunc = lambda x,shape=args[0].shape: x.reshape(shape)
        else:
            flattenFunc = lambda x: array([x])
            deflattenFunc = lambda x: x[0]
        
        newVals = self.interpolateFunc([log(
            u.maxarray(
                u.minarray(flattenFunc(arg),self.maxVals[iarg]),
                self.minVals[iarg])) for iarg,arg in enumerate(args)])                                       
        
        return deflattenFunc(newVals)
    def interpolateFunc(self,vals):
        if len(self.properties)==1: 
            return u.searchAndInterpolateArr(self.propertyValues[0], vals[0],self.calculatedValueMatrix)
        if len(self.properties)==2: 
            return u.searchAndInterpolate2DArr(self.propertyValues[0], vals[0],
                                               self.propertyValues[1], vals[1],
                                               self.calculatedValueMatrix)
        if len(self.properties)==3: 
            return u.searchAndInterpolate3DArr(self.propertyValues[0], vals[0],
                                             self.propertyValues[1], vals[1],
                                             self.propertyValues[2], vals[2],
                                             self.calculatedValueMatrix)
        if len(self.properties)==4: 
            return u.searchAndInterpolate4DArr(self.propertyValues[0], vals[0],
                                             self.propertyValues[1], vals[1],
                                             self.propertyValues[2], vals[2],
                                             self.propertyValues[3], vals[3],
                                             self.calculatedValueMatrix)
        if len(self.properties)==5: 
            return u.searchAndInterpolate5DArr(self.propertyValues[0], vals[0],
                                             self.propertyValues[1], vals[1],
                                             self.propertyValues[2], vals[2],
                                             self.propertyValues[3], vals[3],
                                             self.propertyValues[4], vals[4], 
                                             self.calculatedValueMatrix)
def plotCoolingTime(table,lng):
    # 2.3 x 1.5 x kT / nH Lambda
    # assumes properties order are U0, T, NH, NHI
    u.labelsize(True)
    U0s,Ts,NHs,NHIs = table.propertyValues
    pl.figure(figsize=(10,10))
    for iNHI,NHI in enumerate((NHIs[0],NHIs[-1])):        
        for iNH,NH in enumerate((NHs[0],NHs[-1])):
            ls = ('-','--','-.',':')[iNHI*2+iNH]
            print NHI, NH, ls
            if ls=='-.': continue
            for iU,U0 in enumerate(U0s[::-3][:-1]):
                c = 'kbmry'[iU]
                nH = lng - U0
                Lambdas = abs(table(10**U0,10**Ts,10**NH,10**NHI))
                tcools = (2.3*1.5*cons.k_B * 10**Ts*un.K / (10**nH * Lambdas*un.erg/un.s)).to('yr')
                if iNHI==0 and iNH==0:
                    label=r'$\log n_{\rm H}=%.1f$'%(lng-U0)
                else:
                    label='_nolegend_'
                pl.plot(10**Ts,tcools,label=label,c=c,ls=ls)
    pl.loglog()
    pl.xlabel(r'T [K]')
    pl.ylabel(r'$t_{\rm cool}$ or $t_{\rm heat}$ [yr]')
    pl.legend(loc='upper left',ncol=2)
    gca().yaxis.set_major_formatter(u.arilogformatter)
    gca().yaxis.set_major_locator(pl.matplotlib.ticker.LogLocator(numticks=20))
    pl.ylim(1e-7,1e7)
    
        
def simplisticOpacityTable(dustOpacity = 44816.0, dust2gasRatio = 0.01, Uequal = 0.007, logUs=arange(-5,5.1,.5),logTaus=arange(-12,4,0.5),tableName = 'simplisticGasOpacity'):
    f = file(dataDir + tableName + 'Table.txt','w')
    f.write('%20s %20s %20s\n'%('U','tau','Gas Opacity [cm^2 g^-1]'))
    for iU,logU in enumerate(logUs):
        for itau,logTau in enumerate(logTaus):
            gasOpacity = dustOpacity * dust2gasRatio * (10**logU / Uequal)**-1.
            f.write('%20.4g %20.4g %20.4g\n'%(10**logU ,10**logTau, gasOpacity ))
    f.close()
    
def OpacityTable(grid, dustOpacity = 44816.0, dust2gasRatio = 0.01, logUs=arange(-5,5.1,.5),logTaus=arange(-12,4,0.5),tableName = 'gasOpacity'):
    f = file(dataDir + tableName + 'Table.txt','w')
    f.write('%20s %20s %20s\n'%('U','tau','Gas Opacity [cm^2 g^-1]'))
    #sigma_gas / sigma_dust calculation: not clear whether should be photonmean
    #also, wl-integration range is currently different in sigma_dust and sigma_gas. 
    #Think about this
    gas2DustOpacities = dict( grid(lambda m: (m.U, m.meanSigmaGas(0,photonmean=False)/ m.meanSigmaDust(photonmean=False))).values() )
    for iU,logU in enumerate(logUs):
        for itau,logTau in enumerate(logTaus):
            gasOpacity = dustOpacity * dust2gasRatio * gas2DustOpacities[logU]
            f.write('%20.4g %20.4g %20.4g\n'%(10**logU ,10**logTau, gasOpacity ))
    f.close()


def testApproximations(grid):
    for i in range(3):
        figure()
        if i==0:
            grid(lambda m: u.tplot([(m.localColumnDensity()[i],m.meanSigma(i,component='dust',onlyOptical=True)) for i in range(m.nZones())],label=r'$U=10^{%d}$'%m.U))
            ylabel(r'$\sigma_{\rm dust}({\rm optical})$')
        if i==1:
            grid(lambda m: u.tplot([(m.Nion('HI')[i],m.meanSigma(i,component='dust',ionizing=True)) for i in range(m.nZones())],label=r'$U=10^{%d}$'%m.U))
            ylabel(r'$\sigma_{\rm dust}({\rm ionizing})$')
        if i==2:
            grid(lambda m: u.tplot([(m.Nion('HI')[i],m.meanSigma(i,component='gas',ionizing=True)) for i in range(m.nZones())],label=r'$U=10^{%d}$'%m.U))
            ylabel(r'$\sigma_{\rm dust}({\rm ionizing})$')        
        loglog()
        if i==0:
            xlabel(r'$N_{\rm H}$')            
        else:
            xlabel(r'$N_{\rm HI}$')
        u.mylegend()
def OutputTables(grid,lng,X=0.7,dust2gasRatio=0.01): 
    nVals = 7 
    dlogN = 0.5
    newlNHs = arange(18.,23.1,dlogN) 
    newlNHIs = arange(15.,23.1,dlogN) 
    f = file(dataDir + 'output_table.txt','w')
    f.write('%20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s\n'%(
        'U0', 'T [K]','NH [cm^-2]','NHI [cm^-2]',
        'Dust Optical Opacity [cm^2 g^-1]','Dust Ion Opacity [cm^2 g^-1]','Gas Ion Opacity [cm^2 g^-1]',
        'f_HI',
        'Heating [erg cm^3 s^-1]','Cooling [erg cm^3 s^-1]','netCooling [erg cm^3 s^-1]\n'))
    
    sigmaTypes = ('dust','opt'),('dust','ion'),('gas','ion')
    ks = grid.keys()
    res = zeros((len(ks),nVals))
    notloaded = []
    for ik,k in enumerate(u.Progress(ks)):
        m = grid[k]
        if not m.loaded:
            notloaded.append(k)
            continue
        res[ik,:] = array([log(m.meanSigma(component=component,
                                       ionizing=wlRange=='ion',onlyOptical=wlRange=='opt',
                                       spectrum=m.transmittedSpectrum))
                           for component,wlRange in sigmaTypes] +
                          [log((m.Nion('HI') / (m.Nion('HI')+m.Nion('HII')))[-1])] +
                          (array([10.**m.totalLogHeatingPerUnitArea,
                                  10.**m.totalLogCoolingPerUnitArea,
                                  10.**m.totalLogCoolingPerUnitArea-10.**m.totalLogHeatingPerUnitArea]) / m.ddepths.sum()/10.**(2.*m.n)).tolist()
                          )
    
    CloudyOks = array([k not in notloaded for k in ks])
    lUs = lng-array([k[0] for k in ks])
    Ts = array([k[2] for k in ks])
    lNHs  = array([k[1][0] for k in ks])
    lNHIs  = array([k[1][1] for k in ks])
    for inH,lnH in enumerate(grid.ns):
        lU = lng - lnH
        for iT,T in enumerate(grid.electronTemps):        
            good_inds = ((lUs==lU)*(Ts==T)*CloudyOks).nonzero()[0]             
            tri = matplotlib.tri.Triangulation(lNHs.take(good_inds), lNHIs.take(good_inds))  #Delanuy triangulation                        
            val_triangles = [None]*nVals
            for ivalType in range(nVals):
                val_triangles[ivalType] = tri.calculate_plane_coefficients(res[:,ivalType].take(good_inds))                        
            for i_newlNH,newlNH in enumerate(newlNHs):
                if newlNH==23.: newlNH2 = 22.95 #patch since maximum column is 10**22.998
                else: newlNH2 = newlNH
                for i_newlNHI,newlNHI in enumerate(newlNHIs):
                    f.write('%20.4g %20.4g %20.4g %20.4g '%(10**lU,10**T,10**newlNH,10**newlNHI))            
                    for ivalType in range(nVals):                        
                        ind = tri.get_trifinder()(newlNH2,newlNHI)
                        newlNHI2 = newlNHI                        
                        while ind==-1 and 14.5<newlNHI2<=newlNH+dlogN: #fix for case where N_HI>~N_H
                            newlNHI2 -= dlogN
                            ind = tri.get_trifinder()(newlNH2,newlNHI2)
                        while ind==-1 and 14<newlNHI2<=newlNH+dlogN:
                            newlNHI2 += dlogN
                            ind = tri.get_trifinder()(newlNH2,newlNHI2)
                        while ind==-1 and newlNH==23.: # fix for case where N_H=23 and models failed to run
                            newlNH2 -=0.05
                            ind = tri.get_trifinder()(newlNH2,newlNHI)
                            if ind!=-1: print lU,T,newlNH,newlNHI,newlNHI2,": instead of log NH=23 took log NH=%.1f"%newlNH2
                            elif newlNH2==22.: break
                        if ind!=-1:
                            new_val = val_triangles[ivalType][ind,0]*newlNH2 + val_triangles[ivalType][ind,1]*newlNHI2 + val_triangles[ivalType][ind,2] 
                            if ivalType<=3: new_val = 10.**new_val
                            if ivalType<3: new_val /= (cons.m_p.to('g').value/X) #opacities are per gram, not per H
                            if ivalType<2: new_val /= dust2gasRatio #dust opacities are per gram dust
                        else:
                            if newlNHI < newlNH:
                                print 'failed:', lU,T,newlNH,newlNHI
                            new_val = -1
                        f.write('%20.4g%s'%(new_val,(' ','\n')[ivalType==nVals-1]))
    f.close()
    return notloaded
def precedingColumns(k,AbsorbedSpectra_NH_NHI,lng):
    U,NH = [float(x[1:]) for x in string.split(k[1],'_')[-2:]]
    return AbsorbedSpectra_NH_NHI[lng-U,U,NH]

def ThreeD_opacityTable(grid,AbsorbedSpectra_NH_NHI,lng):
    """
    params of grid should be U, NH, NHI
    """
    ks = grid.keys()
    uniqlNHs  = unique(array([iround(log(precedingColumns(k,AbsorbedSpectra_NH_NHI,lng)[0]),0.1) for k in grid.keys()]))
    
    for component in ('dust',):
        for wlRange in ('ion','opt'):
            sigmaDic = grid(lambda m: m.meanSigma(component=component,ionizing=wlRange=='ion',onlyOptical=wlRange=='opt',spectrum=m.transmittedSpectrum))
            ks, lsigmas = sigmaDic.keys(), log(array(sigmaDic.values()))
            Us = array([lng-k[0] for k in ks])
            lNHs  = array([log(precedingColumns(k,AbsorbedSpectra_NH_NHI,lng)[0]) for k in ks])
            lNHIs = array([log(precedingColumns(k,AbsorbedSpectra_NH_NHI,lng)[1]) for k in ks])
            
            fig = figure()
            if component=='dust' and wlRange=='ion': #fold over U
                ax = fig.add_subplot(111, projection='3d')
                ax.plot_trisurf(lNHs,lNHIs,lsigmas,cmap=matplotlib.cm.jet)
                xlabel(r'$N_{\rm H}$')
                ylabel(r'$N_{\rm HI}$')
                ax.zaxis.set_label_text(r'$\sigma_{\rm %s}({\rm %s})$'%(component,wlRange))
                
            if component=='dust' and wlRange=='opt':  #fold over NHI, U
                plot(lNHs, lsigmas,'.')
                xlabel(r'$N_{\rm H}$')
                ylabel(r'$\sigma_{\rm %s}({\rm %s})$'%(component,wlRange))
            
def Tmaps(grid):
    Tmaps = grid(lambda m: m.loadTmap())
    Ts = sorted(Tmaps.values()[0][0].keys(),key=lambda x: x)
    for iFig in range(2):
        rtf= u.rtfig(figsize=(8,6))
        subplots_adjust(left=0.12,bottom=0.1)
        nIndex = 2
        for iT,T in enumerate(Ts):
            ls = ('-','--',':','-.')[iT/7]
            tplot([(10**U,Tmaps[(n,U)][iFig][T] / (10**n)**nIndex) for n,U in sorted(Tmaps.keys())],[ls],label=r'$T=10^{%.1f}{\rm K}$'%log(T))
        ylabel(r'$\Lambda / n_{\rm H}^{%d}\ [{\rm erg~%ss^{-1}}]$'%(nIndex,('','cm^3~')[nIndex==2]),fontsize=16)
        xlabel(r'$U$',fontsize=16)
        loglog()
        legend()
        text(0.9,0.05,('Heating','Cooling')[iFig], transform=gca().transAxes,ha='right',fontsize=16)
        xlim(1e-6,1e9)
        rtf.xformat(u.arilogformatter)
        u.mysavefig(('Heating','Cooling')[iFig])
    for iTab,tableName in enumerate(('Heating','Cooling','netCooling')):
        f = file(dataDir +tableName + 'Table.txt','w')
        f.write('%20s %20s %20s\n'%('U','T (K)','%s [erg cm^3 s^-1]'%tableName))
        for n,U in sorted(Tmaps.keys(),key=lambda x: x[1]):
            for iT,T in enumerate(Ts[::-1]):
                if iTab==0: val = Tmaps[(n,U)][0][T]
                if iTab==1: val = Tmaps[(n,U)][1][T]
                if iTab==2: val = Tmaps[(n,U)][1][T] - Tmaps[(n,U)][0][T]
                f.write('%20.4g %20.4g %20.4g\n'%(10**U,T,val/ (10**n)**2 ))
        f.close()
    
def FourD_Tmaps(grid,AbsorbedSpectra_NH_NHI,lng,showFigs=False):
    Tmaps = grid(lambda m: m.loadTmap())
    uniqTs = sorted(Tmaps.values()[0][0].keys())
    
    if showFigs:
        for iFig in range(2):
            rtf= u.rtfig(figsize=(8,6))
            subplots_adjust(left=0.12,bottom=0.1)
            nIndex = 2
            for iT,T in enumerate(uniqTs):
                ls = ('-','--',':','-.')[iT/7]
                u.tplot([(10**(lng-lnH),Tmaps[(lnH,sed)][iFig][T] / (10**lnH)**nIndex) for lnH,sed in sorted(Tmaps.keys())],[ls],label=r'$T=10^{%.1f}{\rm K}$'%log(T))
            ylabel(r'$\Lambda / n_{\rm H}^{%d}\ [{\rm erg~%ss^{-1}}]$'%(nIndex,('','cm^3~')[nIndex==2]),fontsize=16)
            xlabel(r'$U$',fontsize=16)
            loglog()
            legend()
            text(0.9,0.05,('Heating','Cooling')[iFig], transform=gca().transAxes,ha='right',fontsize=16)
            xlim(1e-6,1e9)
            rtf.xformat(u.arilogformatter)
            u.mysavefig(('Heating','Cooling')[iFig])
    
    dlogN = 0.5
    newlNHs = arange(17.,23.1,dlogN)
    newlNHIs = arange(15.,23.1,dlogN)

    
    netCooldic = dict([((lng-lnH,log(precedingColumns((lnH,sed),AbsorbedSpectra_NH_NHI,lng)[0]),log(precedingColumns((lnH,sed),AbsorbedSpectra_NH_NHI,lng)[1]),log(T)),
                  (Tmaps[(lnH,sed)][1][T] - Tmaps[(lnH,sed)][0][T])/ (10**lnH)**2) for (lnH,sed) in Tmaps.keys() for T in uniqTs])    
    cooldic   = dict([((lng-lnH,log(precedingColumns((lnH,sed),AbsorbedSpectra_NH_NHI,lng)[0]),log(precedingColumns((lnH,sed),AbsorbedSpectra_NH_NHI,lng)[1]),log(T)),
                  (Tmaps[(lnH,sed)][1][T])/ (10**lnH)**2) for (lnH,sed) in Tmaps.keys() for T in uniqTs])    
    heatdic   = dict([((lng-lnH,log(precedingColumns((lnH,sed),AbsorbedSpectra_NH_NHI,lng)[0]),log(precedingColumns((lnH,sed),AbsorbedSpectra_NH_NHI,lng)[1]),log(T)),
                  (Tmaps[(lnH,sed)][0][T])/ (10**lnH)**2) for (lnH,sed) in Tmaps.keys() for T in uniqTs])    
    
    
    ks, netCoolings, coolings, heatings = netCooldic.keys(), array(netCooldic.values()), array(cooldic.values()), array(heatdic.values())
    lUs,lNHs,lNHIs,lTs = [array([k[i] for k in ks]) for i in range(4)]
    
    for ifn, fn in enumerate(('netCooling_Table.txt', 'cooling_Table.txt', 'heating_Table.txt')):
        title = ('net cooling','cooling','heating')[ifn]
        vals = (netCoolings,coolings,heatings)[ifn]
        f = file(dataDir + fn,'w')
        f.write('%20s %20s %20s %20s %20s\n'%('T [K]','U0', 'NH [cm^-2]','NHI [cm^-2]','%s [erg cm^3 s^-1]'%title))
        for iT,T in enumerate(uniqTs):
            for iU,lnH in enumerate(grid.ns):
                lU = lng - lnH    
                good_inds = [ik for ik in u.rl(lTs) if lUs[ik]==lU and lTs[ik]==log(T)]
                tri = matplotlib.tri.Triangulation(lNHs.take(good_inds), lNHIs.take(good_inds))                        
                vals_triangles = tri.calculate_plane_coefficients(vals.take(good_inds))                        
                for i_newlNH,newlNH in enumerate(newlNHs):
                    for i_newlNHI,newlNHI in enumerate(newlNHIs):
                        ind = tri.get_trifinder()(newlNH,newlNHI)
                        newlNHI2 = newlNHI
                        while ind==-1 and 14.5<newlNHI2<=newlNH+dlogN:
                            newlNHI2 -= dlogN
                            ind = tri.get_trifinder()(newlNH,newlNHI2)
                        while ind==-1 and 14<newlNHI2<=newlNH+dlogN:
                            newlNHI2 += dlogN
                            ind = tri.get_trifinder()(newlNH,newlNHI2)
                        if ind!=-1:
                            new_vals = vals_triangles[ind,0]*newlNH + vals_triangles[ind,1]*newlNHI2 + vals_triangles[ind,2] 
                        else:
                            new_vals = -1        
                        f.write('%20.4g %20.4g %20.4g %20.4g %20.4g\n'%(T,10**lU,10**newlNH,10**newlNHI,new_vals))
        f.close()
   
    
def testTables(q):
    Ts = 10**arange(2.,8.,0.01)
    Us = 10**arange(-1.,2.)
    Ntups = (1e20,1e15),(1e20,1e18),(1e20,1e20),(1e21,1e21),(3e22,3e22)
    figure(figsize=(8,11))
    subplots_adjust(hspace=0.3)
    for iPanel in range(2):
        subplot(2,1,iPanel+1)        
        if iPanel==0:
            U = Us[1]
            for i in range(2):
                [plot(Ts, (1.,-1)[i]*q.netCoolingTable(Ts,array([U]*len(Ts)),array([NH]*len(Ts)),array([NHI]*len(Ts))),
                      label=(r'$N_{\rm H}=10^{%.1f},\ \ N_{\rm HI}=10^{%.1f}$'%(log(NH),log(NHI)),'_nolegend')[i],
                      ls=('-','--')[i],c='%.1f'%(0.5-iNtup/10.),lw='%.1f'%(1+(iNtup+1)/1.5)) 
                 for iNtup, (NH,NHI) in enumerate(Ntups)]
            text(0.5,0.93,r'$U=%.1f$'%U,transform=gca().transAxes,ha='center',fontsize=16)  
        if iPanel==1:
            NH, NHI = Ntups[0]  
            for i in range(2):            
                [plot(Ts, (1.,-1)[i]*q.netCoolingTable(Ts,array([U]*len(Ts)),array([NH]*len(Ts)),array([NHI]*len(Ts))),
                      label=(r'$U=%.1f$'%U,'_nolegend')[i],
                      ls=('-','--')[i],c='%.1f'%(0.5-iU/10.),lw='%.1f'%(1+(iU+1)/1.5)) 
                 for iU, U in enumerate(Us)]                
            text(0.5,0.93,r'$N_{\rm H}=10^{%.1f},\ \ N_{\rm HI}=10^{%.1f}$'%(log(NH),log(NHI)),transform=gca().transAxes,ha='center',fontsize=16)
        [axvline(10**logT,ls=':',c='.5',lw=0.5) for logT in q.netCoolingTable.propertyValues[0]]
        loglog()
        xlabel(r'$T~[{\rm K}]$')
        ylabel(r'$\Lambda / n_{\rm H}^2 ~[{\rm erg s^{-1} cm^3}]$')
        xlim(min(Ts),max(Ts))  
        ylim(1e-26,3e-21)
        legend(loc='best')

def testTables2(grid,AbsorbedSpectra_NH_NHI,lng):
    Tmaps = grid(lambda m: m.loadTmap(returnEquilibriumTemperature=True))
    d = dict([((k[0],AbsorbedSpectra_NH_NHI[tuple([float(x[1:]) for x in  string.split(k[1],'_')[1:]])]),v[2]) for k,v in Tmaps.items()])
    lnHs = arange(-2.5,8)
    figure()
    for iU,lnH in enumerate(lnHs):
        subplot(4,3,iU+1)
        text(17.5,23,r'log U = %.1f'%(lng-lnH))
        for k,v in d.items():
            if k[0]==lnH:
                print k[1][0],k[1][1],v
                text(log(k[1][0]),log(k[1][1]),'%.1f'%log(v),clip_on=True) 
        xlim(17,24)
        xlabel(r'log $N_{\rm H}$')
        ylim(15,24)
        ylabel(r'log $N_{\rm HI}$')
def lineEmissionTable(lines,grid,AbsorbedSpectra_NH_NHI,spectrum_lng,lngDic): 
    dlogN = 0.5
    newlNHs = arange(17.,23.1,dlogN)
    newlNHIs = arange(15.,23.1,dlogN)
    for line in lines:
        f = file(dataDir + '%s_table.txt'%line.linename(),'w')
        lineDic = {}
        for k in grid:
            if hasattr(grid[k],'linesdicF'):
                lineDic[k] = grid[k].linesdicF[line.wl][0]
        ks, llines = lineDic.keys(), array(lineDic.values())
        lNHs  = array([log(precedingColumns(k,AbsorbedSpectra_NH_NHI,spectrum_lng)[0]) for k in ks])
        lNHIs = array([log(precedingColumns(k,AbsorbedSpectra_NH_NHI,spectrum_lng)[1]) for k in ks])
        f.write('%20s %20s %20s %20s %20s\n'%('nH', 'U0', 'NH [cm^-2]','NHI [cm^-2]','L_%s [erg/s/cm^3]'%line.linename()))        
        for inH,lnH in enumerate(grid.ns):
            for lng in sorted(lngDic.values()): 
                lU = lng - lnH
                good_inds = [ik for ik,k in enumerate(ks) if lngDic[k[2]]-k[0]==lU and k[0]==lnH]
                if len(good_inds)<=3: continue
                tri = matplotlib.tri.Triangulation(lNHs.take(good_inds), lNHIs.take(good_inds))                        
                lline_triangles = tri.calculate_plane_coefficients(llines.take(good_inds))                        
                for i_newlNH,newlNH in enumerate(newlNHs):
                    for i_newlNHI,newlNHI in enumerate(newlNHIs):
                        ind = tri.get_trifinder()(newlNH,newlNHI)
                        newlNHI2 = newlNHI
                        while ind==-1 and 14.5<newlNHI2<=newlNH+dlogN:
                            newlNHI2 -= dlogN
                            ind = tri.get_trifinder()(newlNH,newlNHI2)
                        while ind==-1 and 14<newlNHI2<=newlNH+dlogN:
                            newlNHI2 += dlogN
                            ind = tri.get_trifinder()(newlNH,newlNHI2)
                        if ind!=-1:
                            new_lline = 10**( lline_triangles[ind,0]*newlNH + lline_triangles[ind,1]*newlNHI2 + lline_triangles[ind,2] )
                        else:
                            new_lline = -1
                        f.write('%20.4g %20.4g %20.4g %20.4g %20.4g\n'%(10**lnH, 10**lU,10**newlNH,10**newlNHI,new_lline))            
        f.close()

        
def NamekataEnergetics(U0,T,NH,NHI,grainOpacityTuple,Lnu_spectral_slope,min_fHI=1e-9,return_individual_processes=False):
    # Collisional ionization of HI
    eV = (un.eV / cons.k_B).to('K').value
    if T>0.8*eV:
        collisional_ionization_rate = np.exp(-32.71396786375
                     +13.53655609057*np.log(T/eV)
                     -5.739328757388*np.log(T/eV)**2
                     +1.563154982022*np.log(T/eV)**3
                     -2.877056004391e-1*np.log(T/eV)**4
                     +3.482559773736999e-2*np.log(T/eV)**5
                     -2.631976517559e-3*np.log(T/eV)**6
                     +1.119543953861e-4*np.log(T/eV)**7
                     -2.039149852002e-6*np.log(T/eV)**8)
    else:
        collisional_ionization_rate  = 0.       

    grainOpacitynus, grainOpacities = grainOpacityTuple 
    rydberg = 13.6 * un.eV.to('erg')

    dlnu = 0.01
    optical_nus  = 10.**np.arange(-1,0.,dlnu)
    ionizing_nus = 10.**np.arange(0.,3.,dlnu)

    #optical dust opacity
    dnus = dlnu * optical_nus
    sigma_dust  = np.interp(optical_nus,grainOpacitynus / (3e18/912.),grainOpacities)
    taus = NH * sigma_dust
    incident_spectrum   = optical_nus**Lnu_spectral_slope
    attenuated_spectrum = incident_spectrum * np.e**-taus
    
    dust_absorbed_spectrum  =  attenuated_spectrum * sigma_dust
    dust_opt_opacity = (dust_absorbed_spectrum * dnus).sum() / (attenuated_spectrum * dnus).sum()
    
    #ionizing dust and gas opacity
    dnus = dlnu * ionizing_nus
    sigma_dust  = np.interp(ionizing_nus,grainOpacitynus / (3e18/912.),grainOpacities)
    #hnu1 = 13.6*eV; epsilon = (nu/nu1 - 1.)**0.5
    #sigma_HI_PI = 6.3e-18 * (nu / nu1)**-4 * np.exp**(4-4*np.arctan(epsilon)**-1/epsilon) / (1-np.exp(-2*np.pi/epsilon))    
    sigma_HI_PI = 6.3e-18 * ionizing_nus**-3    
    
    Qnu_spectral_slope = Lnu_spectral_slope-1.
    taus = NHI * sigma_HI_PI + NH * sigma_dust
    incident_photons_spectrum_per_H   = U0 * cons.c.to('cm/s').value * ionizing_nus**Qnu_spectral_slope * -Lnu_spectral_slope #should be divided by nu_0 where here we assume nu_0=1
    attenuated_photons_spectrum_per_H = incident_photons_spectrum_per_H * np.e**-taus
    
    dust_absorbed_photons_spectrum_per_H  =  attenuated_photons_spectrum_per_H * sigma_dust
    dust_ion_opacity_per_H = (dust_absorbed_photons_spectrum_per_H *dnus*ionizing_nus).sum() / (attenuated_photons_spectrum_per_H*dnus*ionizing_nus).sum()
   
    gas_absorbed_photons_spectrum_per_H   =  attenuated_photons_spectrum_per_H * sigma_HI_PI

    #photoionization rate
    photoionization_rate_per_H_per_HI = (gas_absorbed_photons_spectrum_per_H*dnus).sum() 
    
    # recombination rate 
    lambdaHI = 2*(157807./T)
    recombination_rate_per_p_per_e = 2.753e-14*lambdaHI**1.5 / (1. + (lambdaHI/2.740)**0.407)**2.24    

    #derive f_HI from recombination_rate = photoionization_rate + collisional ionization rate
    a = recombination_rate_per_p_per_e + collisional_ionization_rate 
    b = -(2*recombination_rate_per_p_per_e+collisional_ionization_rate +photoionization_rate_per_H_per_HI)
    c = recombination_rate_per_p_per_e
    fHI = (-b - (b**2-4*a*c)**0.5) / (2*a)
    if b**2-4*a*c < 0 or fHI<min_fHI: fHI = min_fHI
    
    
    gas_ion_opacity_per_H = (gas_absorbed_photons_spectrum_per_H *fHI*ionizing_nus*dnus).sum() / (attenuated_photons_spectrum_per_H*dnus*ionizing_nus).sum()
    

    ## thermal processes -- coefficients are in [erg cm^3 s^-1] 
    ## and assumed to be multiplied by n_H^2 to get heating rate per unit volume
    
    # photoionization heating
    photoionization_heating_rate = (gas_absorbed_photons_spectrum_per_H * fHI * (ionizing_nus-1.) * rydberg  * dnus).sum()
    

    # HII case B recombination cooling rate, Hui & Gnedin 1997
    recombination_cooling = 3.435e-30 * T * lambdaHI**1.5 / (1.+(lambdaHI/2.25)**0.376)**3.72 * (1.-fHI)**2
    
    # free-free \S3.4 in AGN3 
    gaunt_factor = 1.3
    ff_cooling = 1.42e-27 * T**0.5 * gaunt_factor * (1-fHI)**2
    
    # collisional ionization cooling of HI, Cen (1992), eq. 12a
    CiC_HI = 1.27e-21 * T**0.5 * (1.+(T/1e5)**0.5)**-1 * np.e**(-lambdaHI/2.) * (1-fHI)*fHI
    
    # collisional excitation cooling of HI, Cen (1992), eq. 15a
    CeC_HI = 7.5e-19           * (1.+(T/1e5)**0.5)**-1 * np.e**(-118348./T)   * (1-fHI)*fHI
    
    # gas grain heating-cooling
    T_dust = 100. #instead of calculating T_dust using eq. 26 in Namekata+14
    sigma_grain = np.pi * 0.05e-4**2
    gas_grain_cooling = 9.5e-12 * sigma_grain * (8*cons.k_B.to('erg/K').value**3 * T / (np.pi*cons.m_p.to('g').value))**0.5 * 0.4 * 2 * (T - T_dust) #eq. 25 in Namekata
    
    cooling = recombination_cooling + ff_cooling + CiC_HI + CeC_HI + gas_grain_cooling
    heating = photoionization_heating_rate
    net_cooling = cooling - heating
    
    if return_individual_processes:
        return fHI, -photoionization_heating_rate, recombination_cooling, ff_cooling, CiC_HI, CeC_HI, gas_grain_cooling
    return (dust_opt_opacity, dust_ion_opacity_per_H, gas_ion_opacity_per_H, 
            fHI, heating, cooling, net_cooling)

def createNamekataTable(grid, lng, X=0.7, dust2gasRatio=0.01,
                        ionizing_spectral_slope=-1.6, optical_spectral_slope=-0.5): 
    nVals = 7 
    dlogN = 0.5
    newlNHs  = arange(18.,23.1,dlogN) 
    newlNHIs = arange(15.,23.1,dlogN) 
    f = file(dataDir + 'output_table_Namekata14.txt','w')
    f.write('%20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s\n'%(
        'U0', 'T [K]','NH [cm^-2]','NHI [cm^-2]',
        'Dust Optical Opacity [cm^2 g^-1]','Dust Ion Opacity [cm^2 g^-1]','Gas Ion Opacity [cm^2 g^-1]',
        'f_HI',
        'Heating [erg cm^3 s^-1]','Cooling [erg cm^3 s^-1]','netCooling [erg cm^3 s^-1]'))
    
    ks = grid.keys()
    m = grid[ks[0]]
    m.loadAll()            
    grainOpacityTuple = m.nus(False), m.grainOpacities
    lUs = lng-array([k[0] for k in ks])
    Ts = array([k[2] for k in ks])
    lNHs  = array([k[1][0] for k in ks])
    lNHIs  = array([k[1][1] for k in ks])
    for inH,lnH in enumerate(u.Progress(grid.ns)):
        lU = lng - lnH
        for iT,T in enumerate(grid.electronTemps):        
            good_inds = ((lUs==lU)*(Ts==T)).nonzero()[0]             
            tri = matplotlib.tri.Triangulation(lNHs.take(good_inds), lNHIs.take(good_inds))  #Delanuy triangulation                        
            for i_newlNH,newlNH in enumerate(newlNHs):
                if newlNH==23.: newlNH2 = 22.95 #patch since maximum column is 10**22.998
                else: newlNH2 = newlNH
                for i_newlNHI,newlNHI in enumerate(newlNHIs):
                    f.write('%20.4g %20.4g %20.4g %20.4g '%(10**lU,10**T,10**newlNH,10**newlNHI))            
                    energetics = NamekataEnergetics(10**lU,10**T,10**newlNH,10**newlNHI,grainOpacityTuple,
                                                       Lnu_spectral_slope=ionizing_spectral_slope)
                    for ivalType in range(nVals):
                        new_val = energetics[ivalType]
                        if ivalType<3: new_val /= (cons.m_p.to('g').value/X) #opacities are per gram, not per H
                        if ivalType<2: new_val /= dust2gasRatio #dust opacities are per gram dust                        
                        f.write('%20.4g%s'%(new_val,(' ','\n')[ivalType==nVals-1]))
    f.close()
    
def testNamekataEnergetics(lNH, lNHI,m,ionizing_spectral_slope=-1.6):
    grainOpacityTuple = m.nus(False), m.grainOpacities
    
    
    lTs = np.arange(1.,9.,.1)
    lUs = np.arange(-10.,5.1,)
    u.labelsize(True)
    pl.figure(figsize=(20,20))
    pl.subplots_adjust(hspace=0.3,wspace=0.5)
    for iPanel,lU in enumerate(lUs):
        pl.subplot(4,4,iPanel+1)
        vals = []; fHIs = []
        for lT in lTs:       
            res = NamekataEnergetics(10.**lU,10.**lT,10.**lNH,10.**lNHI,grainOpacityTuple,Lnu_spectral_slope=ionizing_spectral_slope,return_individual_processes=True)
            fHIs.append(res[0])
            vals.append(list(res[1:]))
        vals = np.array(vals)
        labels = 'PI heating', 'rec cooling', 'ff cooling', 'CiC HI', 'CeC HI','dust cooling'        
        cs = 'b','c','m','r','y','.5'
        [pl.plot(lTs, vals[:,i],label=labels[i],c=cs[i]) for i in range(vals.shape[1])]
        total = vals.sum(axis=1)
        zerocrossings = ((total[1:] * total[:-1])<0).nonzero()[0]
        [pl.axvline(lTs[x],c='k',ls=':') for x in zerocrossings]
            
        pl.plot(lTs,total,label='total',c='k',lw=2)
        if lU==lUs[0]: 
            pl.legend(handlelength=1,ncol=2)
        pl.text(0.1,0.9,r'$\log\ U=%d$'%lU,transform=pl.gca().transAxes)
        pl.xlabel(r'$\log T$ [K]')
        pl.ylabel(r'cooling rate [erg cm$^{3}$ s$^{-1}$]')
        pl.yscale('symlog',linthreshy=1e-28)
        #yls = pl.ylim()
        #pl.ylim(1e-10*yls[1],yls[1])
        pl.ylim(-1e-18,1e-18)
        
        pl.twinx()
        pl.plot(lTs,fHIs,c='.3',lw=3)
        pl.semilogy()
        pl.ylim(0.3e-9,2)
        
        
        
            