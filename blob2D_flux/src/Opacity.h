#ifndef OPACITY_H_
#define OPACITY_H_

extern int DustOpacityFlag;
extern int OpacityExponent;
extern double ConstantDustOpacity;

extern int GasOpacityFlag;
extern double ConstantGasOpacity;

//
// Frequency dependent Opacities:
//
// TODO: should be not required; currently used in Irradiation.c
extern int freq_nr;
extern double *freq_nu;


// ***********************
// ** External Routines **
// ***********************
//

int InitializeOpacity(void);
int FinalizeOpacity(void);

double FrequencyDependentOpacity(double);
double PlanckMeanDustOpacity(double, double, double, double, double);
double RosselandMeanDustOpacity(double, double, double, double, double);
double PlanckMeanGasOpacity(double, double, double, double);
double RosselandMeanGasOpacity(double, double, double, double);

void Make_GasOpacityTable(); //ONORBE
void Make_DustOpacityOptTable(); //ONORBE
void Make_DustOpacityIonTable(); //ONORBE
double Get_GasOpacityIon(double, double, double, double); //ONORBE
double Get_DustOpacityOpt(double, double, double, double); //ONORBE
double Get_DustOpacityIon(double, double, double, double); //ONORBE

#endif /*OPACITY_H_*/
