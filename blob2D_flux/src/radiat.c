#include "pluto.h"
#include "PhysicalConstantsCGS.h"
#include "Molecules.h"
#include "interface.h"

#ifndef TV_COOLING_TABLE_NZ   
    #define TV_COOLING_TABLE_NZ    16 // U0
#endif
#ifndef TV_COOLING_TABLE_NY   
    #define TV_COOLING_TABLE_NY    12 //T
#endif
#ifndef TV_COOLING_TABLE_NX   
    #define TV_COOLING_TABLE_NX    11 //NH
#endif
#ifndef TV_COOLING_TABLE_NW   
    #define TV_COOLING_TABLE_NW    17 //NHI
#endif
static Table4D cool_tab; /*A 4D table containing pre-computed values of 
                          cooling stored at equally spaced node 
                          values of Log(U0) Log(T) Log(NH) and Log(NHI) .*/

static double E_cost;

/* *************************************************************** * */
void Make_CoolingTable()
/*! 
*********************************************************************** */
{
  int h,i,j,k,l;
  int ntab,ntot,status;
  double scrh;
  double tmin,tmax,umin,umax,NHmin,NHmax,NHImin,NHImax;
  double *U0_arr, *T_arr, *NH_arr, *NHI_arr, *L_arr;
  FILE *fcool;
    print1 (" > Computing cooling 4D lookup table from ascii table...\n");
    fcool = fopen("netCooling_table.txt","r");
    if (fcool == NULL){
      print1 ("! Radiat: netCooling_table.txt could not be found.\n");
      QUIT_PLUTO(1);
    }
    ntot=TV_COOLING_TABLE_NZ*TV_COOLING_TABLE_NY*TV_COOLING_TABLE_NX*TV_COOLING_TABLE_NW;
    L_arr = ARRAY_1D(ntot, double); //cooling-heating (erg cm^3 s^-1)
    U0_arr = ARRAY_1D(ntot, double); // U0
    T_arr = ARRAY_1D(ntot, double); // temperature (K)
    NH_arr = ARRAY_1D(ntot, double); // NH
    NHI_arr = ARRAY_1D(ntot, double); // NHI

    fscanf(fcool, "%*[^\n]\n", NULL); //ignore first line
    ntab = 0;
    while (fscanf(fcool, "%lf  %lf  %lf   %lf  %lf\n", U0_arr + ntab,
                                       T_arr + ntab, NH_arr + ntab, 
                                       NHI_arr+ntab, L_arr + ntab)!=EOF) {
      ntab++;
    }
    fclose(fcool);
    i=0;
    
    // Assuming table goes from min to max
    NHImin=NHI_arr[0];NHImax=NHI_arr[ntot-1];//NHI (W)
    NHmin=NH_arr[0];NHmax=NH_arr[ntot-1];//NH (X)
    tmin=T_arr[0];tmax=T_arr[ntot-1];//T (Y) 
    umin=U0_arr[0];umax=U0_arr[ntot-1];//U0 (Z) 
    print1 ("Minimum and Max values for NHI in cooling table: %e %e.\n",NHImin,NHImax);
    print1 ("Minimum and Max values for NH in cooling table: %e %e.\n",NHmin,NHmax);
    print1 ("Minimum and Max values for T in cooling table: %e %e.\n",tmin,tmax);
    print1 ("Minimum and Max values for U0 in cooling table: %e %e.\n",umin,umax);

    InitializeTable4D(&cool_tab, NHImin,NHImax, TV_COOLING_TABLE_NW,
                                        NHmin,NHmax, TV_COOLING_TABLE_NX,
                                        tmin, tmax, TV_COOLING_TABLE_NY,
                                        umin, umax, TV_COOLING_TABLE_NZ);
    //fill table with array
    for (h = 0; h < cool_tab.nz; h++){ 
        for (i = 0; i < cool_tab.ny; i++){ 
            for (j = 0; j < cool_tab.nx; j++){ 
                for (k = 0; k < cool_tab.nw; k++){ 
                    l=h*TV_COOLING_TABLE_NY*TV_COOLING_TABLE_NX*TV_COOLING_TABLE_NW +
                        i*TV_COOLING_TABLE_NX*TV_COOLING_TABLE_NW + j*TV_COOLING_TABLE_NW + k;
                    cool_tab.f[h][i][j][k] = L_arr[l];
    }}}}
    if (prank==0) WriteBinaryTable4D("netCooling_table.bin",&cool_tab);//store information
    status = Table4DInterpolate(&cool_tab, 1E16, 1E18, 2400.0, 0.18, &scrh);
    print1 ("Test Cooling table NHI=1E16 NH=1E18 T=2400.0 U0=0.18 --> C=%e\n",scrh);
  // factor to transform cooling erg cm^3/s to code units (from old version of the code)
  E_cost = UNIT_LENGTH/UNIT_DENSITY/pow(UNIT_VELOCITY, 3.0); //3.300718e+05.


}

/* *************************************************************** * */
double Get_Cooling(double NHI, double NH, double T, double U0)
/*! 
*********************************************************************** */
{
 int status;
 double scrh;

  //print1 ("COOL %e %e %e %e\n",NHI, NH, U0, T);

  status = Table4DInterpolate(&cool_tab, NHI, NH, T, U0, &scrh);
  if (status != 0){
    print ("! Cooling: table interpolation failure (bound exceeded)\n");
    print ("!  U0=%e T=%e NH=%e NHI=%e \n",U0,T,NH,NHI);
    QUIT_PLUTO(1);
  }
  //print1 ("COOL-- OUT -- %e %e %e %e %e\n",NHI, NH, U0, T, scrh);
  return scrh;

}



/* ***************************************************************** */
void Radiat (double *v, double NHI, double NH, double T, double U0, double *rhs)
/*!
 *   Provide r.h.s. for tabulated cooling.
 * 
 ******************************************************************* */
{
  double  scrh;

/* ---------------------------------------------
            Get values from cell: temperature and U
   --------------------------------------------- */

  // Why not use v[PRS]? ONORBE
  //prs = v[RHOE]*(g_gamma-1.0);
  //if (prs < 0.0) {
  //  prs     = g_smallPressure;
  //  v[RHOE] = prs/(g_gamma - 1.0);
  //}
  //mu  = MolarMass(1); //this should be ionx
  //T   = ReferenceTemperature*mu*prs/v[RHO]/R_UniversalGasConstant; //Kelvin

  if ((NHI <= cool_tab.wmin) && (NH <= cool_tab.xmin) && (U0 <= cool_tab.zmin) && (T< 110.0) ){
      rhs[RHOE]=0.0;
      return; 
  }
  if ((NHI <= cool_tab.wmin) && (NH <= cool_tab.xmin) && (U0 > 5.E6 ) && (T< 110.0) ){
      rhs[RHOE]=0.0;
      return; 
  }

  // Setting values at exactly the limits breaks things badly

  /* Now check limits NH*/
  if (NH <= cool_tab.xmin) { 
    NH = cool_tab.xmin + cool_tab.xmin*0.001;
  }
  if (NH >= cool_tab.xmax) { 
    NH = (1.0-0.00001)*cool_tab.xmax;
  }

  /* Now check limits NHI*/
  if (NHI <= cool_tab.wmin) { 
    NHI = cool_tab.wmin + cool_tab.wmin*0.001;
  }
  if (NHI >= cool_tab.wmax) { 
    NHI = (1.0-0.00001)*cool_tab.wmax;
  }
  if (NHI > NH) { 
    NHI = NH;
   }

  if (T <= cool_tab.ymin) { 
    T = cool_tab.ymin + cool_tab.ymin*0.001;
  }
  if (T >= cool_tab.ymax) { 
      T=(1.0-0.00001)*cool_tab.ymax;
  }

  /* Check dimensionless ionization parameter*/
  if (U0 <= cool_tab.zmin) { 
    U0 = cool_tab.zmin + cool_tab.zmin*0.001;
   }
  if (U0 >= cool_tab.zmax) { 
      U0=(1.0-0.00001)*cool_tab.zmax;
  }




 /* ----------------------------------------------
        4D Table lookup 
   ---------------------------------------------- */
  scrh=Get_Cooling(NHI,NH,T,U0); //scrh is in erg cm^3 s^-1
  // This is from old version of the code
  rhs[RHOE] = -scrh*v[RHO]*v[RHO]; //
  rhs[RHOE] *= E_cost*UNIT_DENSITY*UNIT_DENSITY/(CONST_mp*CONST_mp); //to code units
}



