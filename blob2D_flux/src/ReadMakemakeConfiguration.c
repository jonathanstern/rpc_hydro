#include "pluto.h"
#include "PhysicalConstantsCGS.h"
#include "ReadMakemakeConfiguration.h"
#include "DomainDecomposition.h"
#include "Radiation.h"
#include "Opacity.h"
#include "DustEvolution.h"
#if FLUXLIMITEDDIFFUSION== YES
#include "FLD.h"
#endif
#if IRRADIATION == YES
#include "Irradiation.h"
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>



// *****************************
// ** ReadConfigurationFile() **
// *****************************
//
int ReadMakemakeConfigurationFile(){

	char ConfigurationFile[255];
	char line[120];
	char *name;
	char *number;

	FILE *FilePointer;
	strcpy(ConfigurationFile, "./Makemake.conf");

	if((FilePointer = fopen(ConfigurationFile, "r")) != NULL) {

	    memset(line, 0, sizeof(line));
	    while(fgets(line, sizeof(line), FilePointer)) {

	    	char *lp;
	    	size_t m,n;

	    	//
	    	// init
	    	//
	    	name = number = 0;

	    	//
	    	// exclude comments, empty lines and lines without '=':
	    	//
	    	if(line[0] != '#' && line [0] != '*' && line[0] != ' ' && line[0] != '\n' && strchr(line, '=')){

	    		lp = &line[0];

	    		//
	    		// determine name:
	    		//
	    		m = strspn(lp,"\t =");
	    		n = strcspn(lp+m,"\t =");
	    		if(n > 0){
	    			name = &lp[m];
	    			name[n] = '\0';
	    			lp = lp+m+n+1;
	    		}

	    		//
	    		// determine number:
	    		//
	    		m = strspn(lp,"\t =");
	    		n = strcspn(lp+m,"\t =#\n");
	    		if(n > 0){
	    			number = &lp[m];
	    			number[n] = '\0';
	    		}


	    		//
	    		// exclude non used parameter:
	    		//
	    		if(number && name){

	    			// *************************************
	    			// * Read data from configuration file *
	    			// *************************************
	    			if(!strcmp(name, "f_DegreesOfFreedom")                      ) f_DegreesOfFreedom                 = strtod(number,0);
					else if(!strcmp(name, "RadiationFlag")                      ) RadiationFlag                      = strtol(number,NULL,10);
					else if(!strcmp(name, "PreconditionerRadiation")            ) PreconditionerRadiation            = strtol(number,NULL,10);
                    else if(!strcmp(name, "RadiativeFluxLimiterFlag")           ) RadiativeFluxLimiterFlag           = strtol(number,NULL,10);
					else if(!strcmp(name, "MinimumOpticalDepth")                ) MinimumOpticalDepth                = strtod(number,0);
	    			else if(!strcmp(name, "MaximumOpticalDepth")                ) MaximumOpticalDepth                = strtod(number,0);
	    			else if(!strcmp(name, "ConvergenceSpecification")           ) ConvergenceSpecification           = strtol(number,NULL,10);
	    			else if(!strcmp(name, "ConvergenceRTOL")                    ){
	    				ConvergenceRTOL = strtod(number,0);
	    				if(ConvergenceRTOL == -1) ConvergenceRTOL = PETSC_DEFAULT;
	    			}
	    			else if(!strcmp(name, "ConvergenceABSTOL")                  ){
	    				ConvergenceABSTOL = strtod(number,0);
	    				if(ConvergenceABSTOL == -1) ConvergenceABSTOL = PETSC_DEFAULT;
	    			}
	    			else if(!strcmp(name, "ConvergenceDTOL")                    ){
	    				ConvergenceDTOL = strtod(number,0);
	    				if(ConvergenceDTOL == -1) ConvergenceDTOL = PETSC_DEFAULT;
	    			}
	    			else if(!strcmp(name, "ConvergenceMAXITS")                  ){
	    				ConvergenceMAXITS = strtol(number,NULL,10);
	    				if(ConvergenceMAXITS == -1) ConvergenceMAXITS = PETSC_DEFAULT;
	    			}
	    			else if(!strcmp(name, "BoundaryRadiationEnergyMinX")        ) BoundaryRadiationEnergyMinX        = strtol(number,NULL,10);
	    			else if(!strcmp(name, "BoundaryRadiationEnergyMaxX")        ) BoundaryRadiationEnergyMaxX        = strtol(number,NULL,10);
	    			else if(!strcmp(name, "BoundaryRadiationEnergyMinY")        ) BoundaryRadiationEnergyMinY        = strtol(number,NULL,10);
	    			else if(!strcmp(name, "BoundaryRadiationEnergyMaxY")        ) BoundaryRadiationEnergyMaxY        = strtol(number,NULL,10);
	    			else if(!strcmp(name, "BoundaryRadiationEnergyMinZ")        ) BoundaryRadiationEnergyMinZ        = strtol(number,NULL,10);
	    			else if(!strcmp(name, "BoundaryRadiationEnergyMaxZ")        ) BoundaryRadiationEnergyMaxZ        = strtol(number,NULL,10);
	    			else if(!strcmp(name, "BoundaryTemperatureDirichlet")       ) BoundaryTemperatureDirichlet       = strtod(number,0);
#if FLUXLIMITEDDIFFUSION == YES
                    else if(!strcmp(name, "RadiationKSPType")                   ) RadiationKSPType                   = strtol(number,NULL,10);
                    else if(!strcmp(name, "ConvergenceMINITS")                  ){
	    				ConvergenceMINITS = strtol(number,NULL,10);
	    				if(ConvergenceMINITS == -1) ConvergenceMINITS = 0;
	    			}
                    else if(!strcmp(name, "KSPSetDiagonalScaleFlag")                  ){
	    				KSPSetDiagonalScaleFlag = strtol(number,NULL,10);
	    				if(KSPSetDiagonalScaleFlag == 0) KSPSetDiagonalScaleFlag = PETSC_FALSE;
	    				if(KSPSetDiagonalScaleFlag == 1) KSPSetDiagonalScaleFlag = PETSC_TRUE;
	    			}                    
                    else if(!strcmp(name, "KSPSetLagNormFlag")                  ){
	    				KSPSetLagNormFlag = strtol(number,NULL,10);
	    				if(KSPSetLagNormFlag == 0) KSPSetLagNormFlag = PETSC_FALSE;
	    				if(KSPSetLagNormFlag == 1) KSPSetLagNormFlag = PETSC_TRUE;
	    			}
#endif
#if IRRADIATION == YES
	    			else if(!strcmp(name, "IrradiationFlag")                    ) IrradiationFlag                    = strtol(number,NULL,10);
	    			else if(!strcmp(name, "NumberOfFrequencies")                ) NumberOfFrequencies                = strtol(number,NULL,10);
#endif
//	    			else if(!strcmp(name, "TemperatureUpdateConvergenceRTOL")   ) TemperatureUpdateConvergenceRTOL   = strtod(number,0);
//	    			else if(!strcmp(name, "TemperatureUpdateConvergenceABSTOL") ) TemperatureUpdateConvergenceABSTOL = strtod(number,0);
//	    			else if(!strcmp(name, "TemperatureUpdateConvergenceMAXITS") ) TemperatureUpdateConvergenceMAXITS = strtol(number,NULL,10);
	    			else if(!strcmp(name, "StellarRadiativeForceFlag")          ) StellarRadiativeForceFlag          = strtol(number,NULL,10);
                    else if(!strcmp(name, "ThermalRadiativeForceFlag")          ) ThermalRadiativeForceFlag          = strtol(number,NULL,10);
										
	    			//
	    			// Evaporation parameter:
	    			//
	    			else if(!strcmp(name, "DustEvaporationFlag")                ) DustEvaporationFlag                = strtol(number,NULL,10);
	    			else if(!strcmp(name, "Dust2GasMassRatio")                  ) Dust2GasMassRatio                  = strtod(number,0);
					
	    		} // exclude non used parameter
	    	} // exlude comments, empty lines
	    } // Loop over lines in the configuration file

	    fclose(FilePointer);

	    return 0;
	}
	else{
		printf("ERROR # Unable to open specified Configuration File '%s'.\n", ConfigurationFile);
		exit(1);
	}
}
