#include "pluto.h"
#include "Irradiation.h"
#include "DomainDecomposition.h"
#include "Radiation.h"
#include "Opacity.h"
#include "DustEvolution.h"
#include "PhysicalConstantsCGS.h"
#include "interface.h"
//#include "definitions.h"
#include "MakemakeTools.h"
#include "FLD.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>  // for exit()



//
// Irradiation parameter:
//
int    IrradiationFlag;
int    NumberOfFrequencies;
double IrradiationTemperature, IrradiationLuminosity, IrradiationRadius;

double *Frequency;
double *IrradiationLuminosityPerFrequencyBin;



// ***********************
// ** Internal Routines **
// ***********************
//
double PlanckFunction(double, double);
double IntegratePlanckFunction(double, double, double);
double IntegrateRayleighJeans(double, double, double);
double IntegrateWien(double, double, double);
double IntegrateTaylor(double, double, double);
void   SubdividePlanckFunction(void);
void   IrradiationParallelCommunication(Data *, Grid *);
void   IonizationFractionParallelCommunication(Data *, Grid *);
void   FluxOptParallelCommunication(Data *, Grid *);
void   FluxIonParallelCommunication(Data *, Grid *);
void   NHParallelCommunication(Data *, Grid *);
void   NHIParallelCommunication(Data *, Grid *);
double Get_fHI(double , double , double, double);



// ***************************
// ** InitializeRadiation() **
// ***************************
//
int InitializeIrradiation(){
	
	int i;

#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###       ... Initialize Irradiation ...                                     ###\n");
#endif

	switch(IrradiationFlag) {
		case 0:
			break;
		case 1:
			NumberOfFrequencies = 1;
			break;
		case 2:
			if(NumberOfFrequencies == -1) NumberOfFrequencies = freq_nr;
			Frequency                            = malloc(NumberOfFrequencies * sizeof(double));
			IrradiationLuminosityPerFrequencyBin = malloc(NumberOfFrequencies * sizeof(double));
			//
			// In case of frequency-dependent Irradiation and usage of all available frequencies,
			// determine these frequencies and corresponding opacities now during Initialization:
			//
			if(NumberOfFrequencies == freq_nr){				
				if(freq_nu[0] > freq_nu[1]){
					for(i = 0; i < NumberOfFrequencies; i++){
						Frequency[i] = freq_nu[freq_nr-1-i] / ReferenceFrequency;
					}
				}
				else{
					for(i = 0; i < NumberOfFrequencies; i++){
						Frequency[i] = freq_nu[i] / ReferenceFrequency;
					}
				}
			}
			break;
		default:
			PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
			PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
			PetscPrintf(PETSC_COMM_WORLD, "ERROR IrradiationFlag = %d not available, see 'Makemake.conf'.\n", IrradiationFlag);
			PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
			PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
			exit(1);
	}
	
	return 0;
}



// *************************
// ** FinalizeRadiation() **
// *************************
//
// Frees the memory and destroys the objects allocated during InitializeRadiation().
//
int FinalizeIrradiation(){
		
	if(IrradiationFlag == 2){
		free(Frequency);
		free(IrradiationLuminosityPerFrequencyBin);
	}
	
	return 0;
}



// *******************
// ** Irradiation() **
// *******************
//
// Calculates cell-centered IrradiationEnergy (=> Source term for FLD equation) and
// IrradiationAcceleration (=> to calculate RadiationEnergy and Temperature-Update)
// from face-centered IrradiationFlux:
//
// IrradiationFlux_i = IrradiationFlux_i-1 * Attenuation * Extinction
//  with
//  Attenuation = (R_i-1 / R_i)^2
//  Extinction  = exp(-Tau_i-1)
//         Tau  = rho_i-1 * kappa_i-1 * delta r_i-1
//
int Irradiation(Data *data, Grid *grid){
	
	int FrequencyBin;
	int kl, jl, il;
    int mpierr;
	double Flux0, Flux, Attenuation;
	double FluxOpt, FluxIon, ExtinctionOpt, ExtinctionIon, DustOpacityOpt, DustOpacityIon;
    double GasOpacityOpt, GasOpacityIon, dtauOpt, dtauIon, tau; 
    double kapparhoOpt, kapparhoIon;
    double U0,T,NH,NHI,nHatom;
    double fH,fHI;
	double dx, dAxl, dAxr, dV;
	double rhogas, rhodust;
    //double Tgas, Tdust, Trad;
    double F_f,n_f,c_f,rho_f,U_factor;
    double f_ion,f_opt,FluxOpt0, FluxIon0;
    f_ion=0.486;
    f_opt=1.0-f_ion;
    fH=0.74; // fraction of hydrogen, hardcoded
    // Flux from code units to erg/s/cm^2
    // ReferencePower/ReferenceArea
    // ReferencePower = ReferenceEnergy / ReferenceTime;
    // ReferenceEnergy = UNIT_DENSITY  * pow(UNIT_VELOCITY, 2) * pow(UNIT_LENGTH,3)
    // ReferenceTime = UNIT_LENGTH/UNIT_VELOCITY
    F_f= UNIT_DENSITY  * pow(UNIT_VELOCITY, 3);
    //Normalizaztion factor: mean ionizing photon energy in erg (36 eV to erg) 
    n_f = mipe_in_eV*CONST_eV; //1.196185e-10
    //speed of light in cm/s
    c_f = c_SpeedOfLight*ReferenceVelocity;
    //factor to transform density from code units to hydrogen atoms/cm^3
    rho_f = UNIT_DENSITY/CONST_mp; //5.978638e+03
    // factor to apply to U to get the ionization parameter in same units as table
    U_factor = F_f/n_f/c_f/rho_f; //
	MPI_Status status;
	MPI_Request request;

#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###       ... Irradiation ...                                                ###\n");
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... IrradiationLuminosity =                  %4.1e Lsol      ###\n", IrradiationLuminosity / SolarLuminosity);
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... IrradiationTemperature =                 %4.1e K         ###\n", IrradiationTemperature * ReferenceTemperature);
#endif
	
	//
	// In case of frequency-dependent Irradiation split the stellar source function accordingly:
	//
	if(IrradiationFlag == 2) SubdividePlanckFunction();
	
	// ********************************
	// * Radiation transport equation *
	// ********************************
	//
	// Even in parallel mode this is only done in serial mode
	// due to the radial dependence of above formula (IR_flux_i-1).
	//
	KDOM_LOOP(kl){
		JDOM_LOOP(jl){
			IDOM_LOOP(il){
				data->IrradiationPowerDensity[kl][jl][il] = 0;
			}
		}
	}
	
	for(FrequencyBin = 0; FrequencyBin < NumberOfFrequencies; FrequencyBin++){
#if IONIZATION == YES
		// If ionization is considered, Irradiation of frequencies above 13.6 eV is handled in IonizationIrradiation() routine.
		if(Frequency[FrequencyBin] < 13.6 * eV / h_PlanckConstant)
#endif
		KDOM_LOOP(kl){
			JDOM_LOOP(jl){
				
        		if(grid[IDIR].beg == IBEG){
					tau = 0;
					//
					// Determine (frequency dependent) Flux from Central Object at StartingFace:
					//
					if(IrradiationFlag == 1)
						Flux = IrradiationLuminosity;
					else
						Flux = IrradiationLuminosityPerFrequencyBin[FrequencyBin];
                    
					Flux /= 4.0 * M_PI * pow(grid[IDIR].xl[IBEG], 2);
                    Flux0 = Flux;
                    FluxOpt0 = f_opt * Flux0;
                    FluxIon0 = f_ion * Flux0;
                    FluxOpt=f_opt*Flux0; //fraction of total flux
                    FluxIon=f_ion*Flux0; // fraction of total flux
                    NH=0; //initial column densities
                    NHI=0; //initial column densities
				}
				else{
					//
					// Receive variables at inner radial boundary from previous processor:
					// Remark: In PETSC_COMM_WORLD/MPI_COMM_WORLD the previous cpu in radial/x - direction is always 'prank-1'.
					//
					mpierr = MPI_Recv(&Flux0, 1, MPI_DOUBLE, prank-1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &status);
					mpierr = MPI_Recv(&Flux, 1, MPI_DOUBLE, prank-1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &status);
					mpierr = MPI_Recv(&FluxOpt, 1, MPI_DOUBLE, prank-1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &status);
					mpierr = MPI_Recv(&FluxIon, 1, MPI_DOUBLE, prank-1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &status);
					mpierr = MPI_Recv(&NH , 1, MPI_DOUBLE, prank-1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &status);
					mpierr = MPI_Recv(&NHI , 1, MPI_DOUBLE, prank-1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &status);
				}
				
				//
				// Calculate (frequency-dependent) Flux through Grid along Rays in X-Direction:
				//
				if(Flux != 0){

					IDOM_LOOP(il){
						
						dx   = GridCellLength(grid, IDIR, kl, jl, il  ); //cell length along the radiation axis IDIR
						dAxl = GridCellArea(  grid, IDIR, kl, jl, il  ); //cell area in the other two directions 
						dAxr = GridCellArea(  grid, IDIR, kl, jl, il+1); //NEXT cell area in the other two directions
						dV   = GridCellVolume(grid,       kl, jl, il  ); //Volume of the cell
						
						//Tgas    = data->GasTemperature[kl][jl][il];
						//Tdust   = data->DustTemperature[kl][jl][il];
						//Trad    = IrradiationTemperature;
						rhogas  = data->Vc[RHO][kl][jl][il];
						rhodust = data->DustDensity[kl][jl][il] * rhogas;
                        nHatom  = fH*rho_f*rhogas; // in hydrogen atoms/cm^3
                        // now we compute dimensionless ionization parameter
                        U0      = U_factor*FluxIon0/rhogas; //This is dimensionless ionization parameter, same units as table in gas opacity
                        //fprintf (stdout,"zone [x1(%d) = %f, x2(%d) = %f] U0=%e NH=%e NHI=%e \n",il,grid[IDIR].x[il],jl,grid[JDIR].x[jl],U0,NH,NHI);
                        T   = ReferenceTemperature*MolarMass(data->IonizationFraction[kl][jl][il])*data->Vc[PRS][kl][jl][il]/rhogas/R_UniversalGasConstant; //Kelvin
                        //fflush(stdout);
						

                        //print ("Coordinates j=%d i=%d: rhogas=%e nH=%e U0=%e NH=%e NHI=%e\n",jl,il,rhogas*UNIT_DENSITY,nHatom, U0, NH, NHI);
						//
						// Determine opacities:
						//
						switch(IrradiationFlag){
                            // Consider it zero for rhodust*rho_f<1E-25:
							case 1:
								DustOpacityOpt = PlanckMeanDustOpacity(NHI,NH,T,U0,1.0);
								DustOpacityIon = PlanckMeanDustOpacity(NHI,NH,T,U0,-1.0);
								break;
							default:
								PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: IrradiationFlag = %d is not in allowed range ###\n", IrradiationFlag);
								exit(1);
						}
					    //Store NH and NHI	
						data->FluxOpt[kl][jl][il] = FluxOpt;
						data->FluxIon[kl][jl][il] = FluxIon;
						data->NH[kl][jl][il]   = NH;
						data->NHI[kl][jl][il]  = NHI;
                        //fprintf (stdout,"DustOpt: %e, DustIon: %e \n",DustOpacityOpt,DustOpacityIon);
                        //fflush(stdout);

						switch(GasOpacityFlag){
							case 3:
                                GasOpacityIon = PlanckMeanGasOpacity(NHI,NH,T,U0);
                                break;
                            default:
								PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: IrradiationFlag = %d is not in allowed range ###\n", IrradiationFlag);
								exit(1);
                        }
                        kapparhoOpt = DustOpacityOpt * rhodust; 
                        kapparhoIon = DustOpacityIon * rhodust + GasOpacityIon  * rhogas;

                        kapparhoOpt = LimitOpticalDepth(grid, kl, jl, il, kapparhoOpt);
                        kapparhoIon = LimitOpticalDepth(grid, kl, jl, il, kapparhoIon);

                        dtauOpt= kapparhoOpt*dx; 
                        dtauIon= kapparhoIon*dx; 
						//tau += dtau; This goes out and calculated at the end, if needed
						
						//
						// Lost energy due to extinction (absorption + scattering) along cell length:
						//
						ExtinctionOpt = exp(-dtauOpt);
						ExtinctionIon = exp(-dtauIon);
						
						//
						// Lost energy density due to larger surface of new interface (geometrical decrease):
						//
						Attenuation = dAxl / dAxr; //this is ratio of current cell area over the next one
#if DEBUGGING > 0
						if(Attenuation < 0.66){
							printf("### WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
							printf("### WARNING \n");
							printf("### WARNING  During Irradiation() step:\n");
							printf("### WARNING  Attenuation = %f < 0.66\n", Attenuation);
							printf("### WARNING  You should check your setup!\n");
							printf("### WARNING  Most probably, higher resolution / less stretching in the x-direction is required.\n");
							printf("### WARNING \n");
							printf("### WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
							exit(1);
						}
#endif
                        FluxOpt *= ExtinctionOpt;
                        FluxIon *= ExtinctionIon;
						//
						// Store for coupling to thermal radiation transport and hydrodynamics:
						//
						// For highly optically thin grid cells, the exp function below becomes numerically unstable (1-Extinction = 0).
						// See IrradiationPowerDensity.nb
						//                        
						if(dtauOpt+dtauIon < 1e-5) {
							data->IrradiationPowerDensity[kl][jl][il] += kapparhoOpt*FluxOpt + kapparhoIon*FluxIon; // ~ Instead of 1-e^-tau --> tau
                        } else {
						    data->IrradiationPowerDensity[kl][jl][il] += (Flux - (FluxOpt+FluxIon)) * dAxl/dV; // dF = (F1-F2)/dx
                        }

						
						//
						// Calculate Quantities at the back face of cell:
						//
                        FluxOpt *= Attenuation;
                        FluxIon *= Attenuation;
                        Flux = FluxOpt+FluxIon;
                        //add Attenuation effect to initial Flux0,
                        //optical and ion.
                        Flux0 *= Attenuation;
                        FluxOpt0 *= Attenuation;
                        FluxIon0 *= Attenuation;
                        // Now NH and NHI in cgs units
                        NH += nHatom*dx*UNIT_LENGTH; // dx from parsec to cm
                        fHI = Get_fHI(NHI,NH,T,U0); // Calculate HI fraction
                        NHI += fHI*nHatom*dx*UNIT_LENGTH; //dx from parsec to cm
						data->IonizationFraction[kl][jl][il] = 1.0 - fHI;
                        //print ("END Coordinates j=%d i=%d: nH=%e U0=%e NHend=%e NHIend=%e\n",jl,il,nHatom, U0, NH, NHI);
                    //if (jl==2) {
                    //fprintf(stdout,"il=%d, GasOpacityFlag=%d, rhogas=%e gasopacity=%e rhodust=%e dustopacity=%e kapparho=%e dx=%e dtau=%e Extinction=%e Att=%e dAxl/dV=%e,Irr=%e,Flux=%e\n",il,GasOpacityFlag,rhogas,GasOpacity,rhodust,DustOpacity,kapparho,dx,dtau,Extinction,Attenuation,dAxl/dV,data->IrradiationPowerDensity[kl][jl][il],Flux);
                    //fflush(stdout);
                    //}
												
					} // for(i)
				} // if(Flux != 0){
				//
				// Send Flux at outer radial boundary to next processor:
				// Remark: In PETSC_COMM_WORLD the next cpu in radial/x - direction is always 'prank+1'.
				//
				if(grid[IDIR].end != grid[IDIR].gend){
					mpierr = MPI_Isend(&Flux0, 1, MPI_DOUBLE, prank+1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &request);
					mpierr = MPI_Isend(&Flux, 1, MPI_DOUBLE, prank+1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &request);
					mpierr = MPI_Isend(&NH, 1, MPI_DOUBLE, prank+1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &request);
					mpierr = MPI_Isend(&NHI, 1, MPI_DOUBLE, prank+1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &request);
					mpierr = MPI_Wait(&request, &status);
				}
                
			} // for(j)
            //}
		} // for(k)
		//	} // if(prank)
		//} // for(proc)
	} // for (FrequencyBin)
    
    
    //
    // Zero-Gradient Boundary conditions for the x- and y-direction (only used for computing radiative force in outer boundaries):
    //
	KTOT_LOOP(kl){
		JTOT_LOOP(jl){
			IBEG_LOOP(il){
				data->IrradiationPowerDensity[kl][jl][il] = data->IrradiationPowerDensity[kl][jl][IBEG];
			}
		}
	}
	KTOT_LOOP(kl){
		JTOT_LOOP(jl){
			IEND_LOOP(il){
				data->IrradiationPowerDensity[kl][jl][il] = data->IrradiationPowerDensity[kl][jl][IEND];
			}
		}
	}
#if DIMENSIONS > 1
	KTOT_LOOP(kl){
		JBEG_LOOP(jl){
			ITOT_LOOP(il){
				data->IrradiationPowerDensity[kl][jl][il] = data->IrradiationPowerDensity[kl][JBEG][il];
			}
		}
	}
	KTOT_LOOP(kl){
		JEND_LOOP(jl){
			ITOT_LOOP(il){
				data->IrradiationPowerDensity[kl][jl][il] = data->IrradiationPowerDensity[kl][JEND][il];
			}
		}
	}
#endif
    
    //
    // Parallel communication:
    //
#ifdef PARALLEL
    IrradiationParallelCommunication(data, grid);
    IonizationFractionParallelCommunication(data, grid);
    FluxOptParallelCommunication(data, grid);
    FluxIonParallelCommunication(data, grid);
    NHParallelCommunication(data, grid);
    NHIParallelCommunication(data, grid);
#endif

	

	return 0;
}



// *******************************
// ** SubdividePlanckFunction() **
// *******************************
//
// Determines in dependence of number of chosen frequencies a subdivision of the Planck spectrum of the central object.
//
void SubdividePlanckFunction(){
	
	int i;
	double xconst, BlackBodyPeakFrequency, EnergyDensityPerFrequencyBin, FrequencyMin, FrequencyMax, SurfaceEnergy, fl, fr;
	
	//
	// Build Frequency-grid
	// (if number of frequency bins != number of available frequencies in the used table,
	// otherwise this was already done during InitializeOpacity())
	//
	if(NumberOfFrequencies != freq_nr){

		//
		// Determine Frequency at peak of Planck Function (Wien Law):
		//
		xconst = 2.8214393721;
		BlackBodyPeakFrequency = xconst * k_BoltzmannConstant * IrradiationTemperature /	h_PlanckConstant;
		//printf("BlackBodyPeakFrequency = %e Hz\n", BlackBodyPeakFrequency*ReferenceFrequency);
		
		//
		// Split the Frequency domain in Frequency bins:
		//
		EnergyDensityPerFrequencyBin = a_RadiationConstant * pow(IrradiationTemperature, 4) / NumberOfFrequencies;
		
		// only used, if not all frequency bins are in use
		FrequencyMin = 0.1 * BlackBodyPeakFrequency;
		FrequencyMax = 3.0 * BlackBodyPeakFrequency;
		
		Frequency[0] = FrequencyMin;
		for(i = 1; i < NumberOfFrequencies; i++){
			double guess =
				0.5
				*
				(
				 0.5*(PlanckFunction(IrradiationTemperature, BlackBodyPeakFrequency) + PlanckFunction(IrradiationTemperature, Frequency[i-1]))
				 +
				 PlanckFunction(IrradiationTemperature, FrequencyMin + (FrequencyMax-FrequencyMin) / NumberOfFrequencies)
			 	);
			
			Frequency[i] =
				Frequency[i-1]
				+
				2.0 * EnergyDensityPerFrequencyBin
				/
				(
				 PlanckFunction(IrradiationTemperature, Frequency[i-1])
				 +
				 guess
			 	);
		}
		
	}
	
	
	//
	// Calculate corresponding Surface Luminosities of the central object in the bins:
	//
	for(i = 0; i < NumberOfFrequencies; i++){
		if(i == 0){
			fl = 1e-2*Frequency[0];
			fr = 0.5 * (Frequency[0] + Frequency[1]);
		}
		else if(i == NumberOfFrequencies - 1){
			fl = 0.5 * (Frequency[NumberOfFrequencies - 1] + Frequency[NumberOfFrequencies - 2]);
			fr = 1e+1 * Frequency[NumberOfFrequencies - 1];
		}
		else{
			fl = 0.5 * (Frequency[i] + Frequency[i-1]);
			fr = 0.5 * (Frequency[i] + Frequency[i+1]);
		}
		
		SurfaceEnergy = IntegratePlanckFunction(IrradiationTemperature, fl, fr);
		IrradiationLuminosityPerFrequencyBin[i] = M_PI * c_SpeedOfLight * SurfaceEnergy * pow(IrradiationRadius, 2);
	}
	
	//
	// Write summary table of Frequency dependence:
	//
#if LOG_OUTPUT != 0
		if(g_stepNumber == 0){
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###                                                                          ###\n");
			
//			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           Mean Energy per Frequency Bin : %e                   ###\n", EnergyDensityPerFrequencyBin);
//			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           Equipartition of Total Energy : %14.4f                 ###\n", Sigma);
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###                                                                          ###\n");
			
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           Frequency [Hz]    Luminosity [Lsol]    Opacity [cm^2 g^-1]     ###\n");
			for(i = 0; i < NumberOfFrequencies; i++){
				PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###             %9.2e         %9.2e             %4.2e             ###\n", Frequency[i] / ReferenceTime, IrradiationLuminosityPerFrequencyBin[i] / SolarLuminosity, FrequencyDependentOpacity(Frequency[i]) * ReferenceOpacity);
			}
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###                                                                          ###\n");
		}
#endif
	
	
	
#if DEBUGGING > 0
	//
	// Check nan:
	//
	for(i = 0; i < NumberOfFrequencies; i++){
		if(isnan(IrradiationLuminosityPerFrequencyBin[i])){
			printf("IrradiationLuminosityPerFrequencyBin[%d] = nan\n", i);
			printf(" IrradiationRadius = %e Rsol\n", IrradiationRadius / SolarRadius);
			printf(" ReferenceLength = %e cm\n", ReferenceLength);
			printf(" SolarRadius = %e cm\n", SolarRadius * ReferenceLength);
            exit(1);
		}
	}
	
	//
	// Check IrradiationLuminosityPerFrequencyBin <= 0:
	//
	for(i = 0; i < NumberOfFrequencies; i++){
		if(IrradiationLuminosityPerFrequencyBin[i] < 0){
			printf("IrradiationLuminosityPerFrequencyBin[%d] = %e < 0\n", i, IrradiationLuminosityPerFrequencyBin[i]);
			exit(1);
		}
	}
	
	//
	// Check integration method and coverage of frequency spectrum (for deviations greater than 0.01%):
	//
	double sum, deviation;
	sum = 0;
	for(i = 0; i < NumberOfFrequencies; i++){
		sum += IrradiationLuminosityPerFrequencyBin[i];
	}
	//deviation = fabs(a_RadiationConstant * pow(IrradiationTemperature, 4) - sum) / (a_RadiationConstant * pow(IrradiationTemperature, 4));
	deviation = fabs(IrradiationLuminosity - sum) / IrradiationLuminosity;
	if(deviation > 1e-4){
		PetscPrintf(PETSC_COMM_WORLD, "\n");
		PetscPrintf(PETSC_COMM_WORLD, "WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		PetscPrintf(PETSC_COMM_WORLD, "WARNING ###############################################\n");
		PetscPrintf(PETSC_COMM_WORLD, "WARNING ###\n"                                            );
		PetscPrintf(PETSC_COMM_WORLD, "WARNING ###  Deviation of sum of frequency dependent   \n");
		PetscPrintf(PETSC_COMM_WORLD, "WARNING ###  luminosity bins and total luminosity.     \n");
		PetscPrintf(PETSC_COMM_WORLD, "WARNING ###  Deviation = %.2e\n", deviation);
		//PetscPrintf(PETSC_COMM_WORLD, "WARNING ###  Iteration = %d\n", Iteration                 );
		PetscPrintf(PETSC_COMM_WORLD, "WARNING ###\n"                                            );
		PetscPrintf(PETSC_COMM_WORLD, "WARNING ###############################################\n");
		PetscPrintf(PETSC_COMM_WORLD, "WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		PetscPrintf(PETSC_COMM_WORLD, "\n");
		exit(1);
	}
#endif
	
}



// **********************
// ** PlanckFunction() **
// **********************
//
// Returns Energy in code units.
//
double PlanckFunction(double Temperature, double Frequency){
	
	return
	8.0 * M_PI * h_PlanckConstant / pow(c_SpeedOfLight, 3)
	*
	pow(Frequency, 3)
	/
	(
	 exp(h_PlanckConstant * Frequency / (k_BoltzmannConstant * Temperature))
	 -
	 1
	 );
	
}



// *******************************
// ** IntegratePlanckFunction() **
// *******************************
//
// Calculates an approximation of the integral of the Planckfunction (to specified temperature)
// from given minimum to maximum frequency.
// Uses Rayleigh-Jeans and Wien approximation (see IntegrateRayleighJeans() and IntegrateWien()).
//
double IntegratePlanckFunction(double Temperature, double MinimumFrequency, double MaximumFrequency){
	
	int    i, steps;
	double Integral, Frequency_limit, TaylorWienBorder, TaylorStart, TaylorEnd, WienStart, WienEnd;
	Integral = 0;
	
	//
	// Determine Rayleigh-Jeans and Wien Limits:
	//
	//                    k*T
	// Frequency_limit = -----
	//                     h
	//
	Frequency_limit = k_BoltzmannConstant * Temperature / h_PlanckConstant;
	TaylorWienBorder = 6.0*Frequency_limit;
	
	//
	// Divide the frequency intervall in Taylor and Wien regime:
	//
	TaylorStart = MIN(MinimumFrequency, TaylorWienBorder);
	TaylorEnd   = MIN(MaximumFrequency, TaylorWienBorder);
	WienStart   = MAX(MinimumFrequency, TaylorWienBorder);
	WienEnd     = MAX(MaximumFrequency, TaylorWienBorder);
	
	//Integral += IntegrateRayleighJeans(Temperature, MinimumFrequency, 0.1*Frequency_limit);
	
	//
	// Integrate the Taylor regime:
	//
	if(TaylorStart != TaylorEnd){
		steps = 20;
		
		for(i = 0; i < steps; i++){
			Integral += IntegrateTaylor(Temperature, TaylorStart + i * (TaylorEnd-TaylorStart) / steps, TaylorStart + (i+1) * (TaylorEnd-TaylorStart) / steps);
		}
	}
	
	//
	// Integrate the Wien regime:
	//
	if(WienStart != WienEnd)
		Integral += IntegrateWien(Temperature, WienStart, WienEnd);
	
	return Integral;
}



// ******************************
// ** IntegrateRayleighJeans() **
// ******************************
//
// Approximate analytical solution of the Rayleigh-Jean regime (h*nu << k*T) of the Planck function.
//
double IntegrateRayleighJeans(double Temperature, double MinimumFrequency, double MaximumFrequency){
	return
	8.0 * M_PI * k_BoltzmannConstant * Temperature
	/
	(3.0 * pow(c_SpeedOfLight, 3))
	*
	(pow(MaximumFrequency, 3) - pow(MinimumFrequency, 3));
}



// *********************
// ** IntegrateWien() **
// *********************
//
// Approximate analytical solution of the Wien regime (h*nu >> k*T) of the Planck function.
//
double IntegrateWien(double Temperature, double MinimumFrequency, double MaximumFrequency){
	return
	8.0 * M_PI * k_BoltzmannConstant * Temperature
	/
	pow(c_SpeedOfLight, 3)
	*
	(
	 exp(- h_PlanckConstant * MinimumFrequency / (k_BoltzmannConstant * Temperature))
	 *
	 (
	  pow(MinimumFrequency, 3)
	  +
	  3 * k_BoltzmannConstant * Temperature / h_PlanckConstant * pow(MinimumFrequency, 2)
	  +
	  6 * pow(k_BoltzmannConstant * Temperature / h_PlanckConstant, 2) * MinimumFrequency
	  +
	  6 * pow(k_BoltzmannConstant * Temperature / h_PlanckConstant, 3)
	  )
	 -
	 exp(- h_PlanckConstant * MaximumFrequency / (k_BoltzmannConstant * Temperature))
	 *
	 (
	  pow(MaximumFrequency, 3)
	  +
	  3 * k_BoltzmannConstant * Temperature / h_PlanckConstant * pow(MaximumFrequency, 2)
	  +
	  6 * pow(k_BoltzmannConstant * Temperature / h_PlanckConstant, 2) * MaximumFrequency
	  +
	  6 * pow(k_BoltzmannConstant * Temperature / h_PlanckConstant, 3)
	  )
	 );
}



// ***********************
// ** IntegrateTaylor() **
// ***********************
//
// 2nd oder Taylor expansion of the Planck function
//
double IntegrateTaylor(double Temperature, double MinimumFrequency, double MaximumFrequency){
	
	double Integral, TaylorSamplingPoint, Normalization, t, a, b;
	
	TaylorSamplingPoint = 0.5 * (MinimumFrequency + MaximumFrequency);
	
	//
	// Define some abbreviations:
	//
	Normalization = 8.0 * M_PI * h_PlanckConstant / pow(c_SpeedOfLight, 3);
	t = h_PlanckConstant / (k_BoltzmannConstant * Temperature);
	a = exp(t * TaylorSamplingPoint);
	b = a - 1;
	
	Integral = 0;

	//
	// 0. order:
	//
	Integral +=
	pow(TaylorSamplingPoint, 3) / b
	*
	(
	 MaximumFrequency
	 -
	 MinimumFrequency
	 );
	
	//
	// 1. order:
	//
	Integral +=
	(
	 3 * pow(TaylorSamplingPoint, 2) / b
	 -
	 pow(TaylorSamplingPoint, 3) * t * a / pow(b, 2)
	 )
	*
	0.5
	*
	(
	 pow(MaximumFrequency - TaylorSamplingPoint, 2)
	 -
	 pow(MinimumFrequency - TaylorSamplingPoint, 2)
	 );
	
	//
	// 2. order:
	//
	Integral +=
	0.5
	*
	(
	 6 * TaylorSamplingPoint / b
	 -
	 6 * pow(TaylorSamplingPoint, 2) * t * a / pow(b, 2)
	 -
	 pow(TaylorSamplingPoint, 3) * pow(t, 2) * a * (b-2*a) / pow(b, 3)
	 )
	*
	1.0 / 3.0
	*
	(
	 pow(MaximumFrequency - TaylorSamplingPoint, 3)
	 -
	 pow(MinimumFrequency - TaylorSamplingPoint, 3)
	 );
	
	//
	// Normalization:
	//
	Integral *= Normalization;
	
	return Integral;
}



void IrradiationParallelCommunication(Data *data, Grid *grid){
    
    int par_dim[3] = {0, 0, 0};
    
    //
    // Check the number of processors in each direction:
    //
    D_EXPAND(par_dim[0] = grid[IDIR].nproc > 1;  ,
             par_dim[1] = grid[JDIR].nproc > 1;  ,
             par_dim[2] = grid[KDIR].nproc > 1;)

#if PERIODICX == 1
        if(par_dim[0] == 0) par_dim[0] = 1;
#endif
#if PERIODICY == 1
        if(par_dim[1] == 0) par_dim[1] = 1;
#endif
#if PERIODICZ == 1
        if(par_dim[2] == 0) par_dim[2] = 1;
#endif 
    //
    // Exchange data between parallel processors:
    //
    MPI_Barrier(MPI_COMM_WORLD);
    
    AL_Exchange_dim((char *) data->IrradiationPowerDensity[0][0], par_dim, SZ);
    
    MPI_Barrier(MPI_COMM_WORLD);
    
}



void FluxOptParallelCommunication(Data *data, Grid *grid){
    
    int par_dim[3] = {0, 0, 0};
    
    //
    // Check the number of processors in each direction:
    //
    D_EXPAND(par_dim[0] = grid[IDIR].nproc > 1;  ,
             par_dim[1] = grid[JDIR].nproc > 1;  ,
             par_dim[2] = grid[KDIR].nproc > 1;)

    
    //
    // Exchange data between parallel processors:
    //
    MPI_Barrier(MPI_COMM_WORLD);
    
    AL_Exchange_dim((char *) data->FluxOpt[0][0], par_dim, SZ);
    
    MPI_Barrier(MPI_COMM_WORLD);
    
}


void FluxIonParallelCommunication(Data *data, Grid *grid){
    
    int par_dim[3] = {0, 0, 0};
    
    //
    // Check the number of processors in each direction:
    //
    D_EXPAND(par_dim[0] = grid[IDIR].nproc > 1;  ,
             par_dim[1] = grid[JDIR].nproc > 1;  ,
             par_dim[2] = grid[KDIR].nproc > 1;)

    
    //
    // Exchange data between parallel processors:
    //
    MPI_Barrier(MPI_COMM_WORLD);
    
    AL_Exchange_dim((char *) data->FluxIon[0][0], par_dim, SZ);
    
    MPI_Barrier(MPI_COMM_WORLD);

}

void IonizationFractionParallelCommunication(Data *data, Grid *grid){
    
    int par_dim[3] = {0, 0, 0};
    
    //
    // Check the number of processors in each direction:
    //
    D_EXPAND(par_dim[0] = grid[IDIR].nproc > 1;  ,
             par_dim[1] = grid[JDIR].nproc > 1;  ,
             par_dim[2] = grid[KDIR].nproc > 1;)

    
    //
    // Exchange data between parallel processors:
    //
    MPI_Barrier(MPI_COMM_WORLD);
    
    AL_Exchange_dim((char *) data->IonizationFraction[0][0], par_dim, SZ);
    
    MPI_Barrier(MPI_COMM_WORLD);
}



void NHParallelCommunication(Data *data, Grid *grid){
    
    int par_dim[3] = {0, 0, 0};
    
    //
    // Check the number of processors in each direction:
    //
    D_EXPAND(par_dim[0] = grid[IDIR].nproc > 1;  ,
             par_dim[1] = grid[JDIR].nproc > 1;  ,
             par_dim[2] = grid[KDIR].nproc > 1;)

    
    //
    // Exchange data between parallel processors:
    //
    MPI_Barrier(MPI_COMM_WORLD);
    
    AL_Exchange_dim((char *) data->NH[0][0], par_dim, SZ);
    
    MPI_Barrier(MPI_COMM_WORLD);
    
}


void NHIParallelCommunication(Data *data, Grid *grid){
    
    int par_dim[3] = {0, 0, 0};
    
    //
    // Check the number of processors in each direction:
    //
    D_EXPAND(par_dim[0] = grid[IDIR].nproc > 1;  ,
             par_dim[1] = grid[JDIR].nproc > 1;  ,
             par_dim[2] = grid[KDIR].nproc > 1;)

    
    //
    // Exchange data between parallel processors:
    //
    MPI_Barrier(MPI_COMM_WORLD);
    
    AL_Exchange_dim((char *) data->NHI[0][0], par_dim, SZ);
    
    MPI_Barrier(MPI_COMM_WORLD);
    
}


/* *************************************************************** * */
#ifndef TV_FHI_TABLE_NZ 
    #define TV_FHI_TABLE_NZ    16 //U0
#endif
#ifndef TV_FHI_TABLE_NY 
    #define TV_FHI_TABLE_NY    12 //U0
#endif
#ifndef TV_FHI_TABLE_NX   
    #define TV_FHI_TABLE_NX    11 //NH
#endif
#ifndef TV_FHI_TABLE_NW
    #define TV_FHI_TABLE_NW    17 //NHI
#endif
static Table4D fHI_tab; /*A 4D table containing pre-computed values of 
                          fHI stored at equally spaced node 
                          values of Log(U0), Log(T), Log(NH) and Log(NHI).*/


void Make_fHITable()
/*! 
*********************************************************************** */
{
  int h,i,j,k,l;
  int ntab,ntot,status;
  double scrh=0.0;
  double *U_arr, *T_arr, *NH_arr, *NHI_arr, *L_arr;
  double umin,umax,tmin,tmax,NHmin,NHmax,NHImin,NHImax;
  FILE *fHIt;
    print1 (" > Computing fHI 4D lookup table from ascii table...\n");
    fHIt = fopen("f_HI_table.txt","r");
    if (fHIt == NULL){
      print1 ("! Make_fHITable: fHI_table.dat could not be found.\n");
      QUIT_PLUTO(1);
    }
    ntot=TV_FHI_TABLE_NW*TV_FHI_TABLE_NX*TV_FHI_TABLE_NY*TV_FHI_TABLE_NZ;
    L_arr = ARRAY_1D(ntot, double); //fHI
    U_arr = ARRAY_1D(ntot, double); // U
    T_arr = ARRAY_1D(ntot, double); // T
    NH_arr = ARRAY_1D(ntot, double); //NH 
    NHI_arr = ARRAY_1D(ntot, double); // NHI

    fscanf(fHIt, "%*[^\n]\n", NULL); //ignore first line
    ntab = 0;
    while (fscanf(fHIt, "%lf  %lf  %lf %lf  %lf\n", U_arr + ntab,
                                       T_arr +ntab, NH_arr + ntab, 
                                       NHI_arr + ntab, 
                                       L_arr + ntab)!=EOF) {
      ntab++;
    }
    fclose(fHIt);
    
    // Assuming table goes from min to max
    NHImin=NHI_arr[0];NHImax=NHI_arr[ntot-1];//NHI (W)
    NHmin=NH_arr[0];NHmax=NH_arr[ntot-1];//NH (X)
    tmin=T_arr[0];tmax=T_arr[ntot-1];//T (Y)
    umin=U_arr[0];umax=U_arr[ntot-1];//U (Z)
    print1 ("Minimum and Max values for NHI in fHI table: %e %e.\n",NHImin,NHImax);
    print1 ("Minimum and Max values for NH in fHI table: %e %e.\n",NHmin,NHmax);
    print1 ("Minimum and Max values for T in dust ion opacity table: %e %e.\n",tmin,tmax);
    print1 ("Minimum and Max values for U0 in fHI table: %e %e.\n",umin,umax);

    InitializeTable4D(&fHI_tab, NHImin, NHImax, TV_FHI_TABLE_NW,
                                NHmin, NHmax, TV_FHI_TABLE_NX,
                                tmin, tmax, TV_FHI_TABLE_NY,
                                umin, umax, TV_FHI_TABLE_NZ);
    //fill table with array
    for (h = 0; h < fHI_tab.nz; h++){ //U0
        for (i = 0; i < fHI_tab.ny; i++){ //T
            for (j = 0; j < fHI_tab.nx; j++){ //NH
                for (k = 0; k < fHI_tab.nw; k++){ //NHI
                    l=h*TV_FHI_TABLE_NY*TV_FHI_TABLE_NX*TV_FHI_TABLE_NW +
                       i*TV_FHI_TABLE_NX*TV_FHI_TABLE_NW + j*TV_FHI_TABLE_NW + k;
                    fHI_tab.f[h][i][j][k] = L_arr[l];
    }}}}

    if (prank==0) WriteBinaryTable4D("fHI_table.bin",&fHI_tab);//store information
    status = Table4DInterpolate(&fHI_tab, 3.3E21, 6.3E21, 2400.0, 0.36, &scrh);
    print1 ("Test fHI table: NHI=3E21 NH=6E21 T=2400. U0=0.36 fHI=%e\n",scrh);


}

/* *************************************************************** * */
double Get_fHI(double NHI, double NH, double T, double U0)
/*! 
*********************************************************************** */
{
 int status;
 double fHI;


  if (T <= fHI_tab.ymin) { 
    T = fHI_tab.ymin + fHI_tab.ymin*0.001;
  }
  if (T > fHI_tab.ymax) { 
    T = (1.0-0.00001)*fHI_tab.ymax;
  }
  
  if (U0 <= fHI_tab.zmin) { 
    U0 = fHI_tab.zmin + fHI_tab.zmin*0.001;
  }
  if (U0 > fHI_tab.zmax) { 
    U0 = (1.0-0.00001)*fHI_tab.zmax;
  }
  if (NH <= fHI_tab.xmin) { 
    NH = fHI_tab.xmin + fHI_tab.xmin*0.001;
  }
  if (NH >= fHI_tab.xmax) { 
    NH = (1.0-0.00001)*fHI_tab.xmax;
  }
  if (NHI <= fHI_tab.wmin) { 
    NHI = fHI_tab.wmin + fHI_tab.wmin*0.001;
  }
  if (NHI >= fHI_tab.wmax) { 
    NHI = (1.0-0.00001)*fHI_tab.wmax;
  }
  if (NHI>NH){
    NHI=NH;
  }

  status = Table4DInterpolate(&fHI_tab, NHI, NH, T, U0, &fHI);
  if (status != 0){
    print ("! fHI: table interpolation failure (bound exceeded)\n");
    print ("! U0=%e T=%e NH=%e NHI=%e \n",U0,T,NH,NHI);
    QUIT_PLUTO(1);
  }
  return fHI;

}


