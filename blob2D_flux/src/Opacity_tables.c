#include "pluto.h"
#include "Opacity.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


/* *************************************************************** * */
#ifndef TV_GASOPACITYION_TABLE_NZ   
    #define TV_GASOPACITYION_TABLE_NZ    16 //U0
#endif
#ifndef TV_GASOPACITYION_TABLE_NY   
    #define TV_GASOPACITYION_TABLE_NY    12 //T
#endif
#ifndef TV_GASOPACITYION_TABLE_NX
    #define TV_GASOPACITYION_TABLE_NX    11 //NH
#endif
#ifndef TV_GASOPACITYION_TABLE_NW
    #define TV_GASOPACITYION_TABLE_NW    17 //NHI
#endif
static Table4D gasopion_tab; /*A 4D table containing pre-computed values of 
                          gas opacity stored at equally spaced node 
                          values of Log(U0) Log(T) Log(NH) and Log(NHI) .*/


void Make_GasOpacityIonTable()
/*! 
*********************************************************************** */
{
  int h,i,j,k,l;
  int ntab,ntot,status;
  double scrh;
  double *U_arr, *T_arr, *NH_arr, *NHI_arr, *O_arr;
  double umin,umax,NHmin,NHmax,NHImin,NHImax,tmin,tmax;
  FILE *fgasopion;
    print1 (" > Computing gas ion opacity 4D lookup table from ascii table...\n");
    fgasopion = fopen("GasIonOpacity_table.txt","r");
    if (fgasopion == NULL){
      print1 ("! Make_GasOpacityTable: GasIonOpacity_table.txt could not be found.\n");
      QUIT_PLUTO(1);
    }
    ntot=TV_GASOPACITYION_TABLE_NW*TV_GASOPACITYION_TABLE_NX*TV_GASOPACITYION_TABLE_NY*TV_GASOPACITYION_TABLE_NZ;
    O_arr = ARRAY_1D(ntot, double); //gas opacity (cm^2 g^-1)
    U_arr = ARRAY_1D(ntot, double); // U
    T_arr = ARRAY_1D(ntot, double); // T (K)
    NH_arr = ARRAY_1D(ntot, double); //NH (cm^-2)
    NHI_arr = ARRAY_1D(ntot, double); // NHI (cm^-2)

    fscanf(fgasopion, "%*[^\n]\n", NULL); //ignore first line
    ntab = 0;
    while (fscanf(fgasopion, "%lf  %lf  %lf  %lf %lf\n", U_arr + ntab,
                                       T_arr + ntab, NH_arr + ntab, 
                                       NHI_arr + ntab, O_arr + ntab)!=EOF) {
      ntab++;
    }
    fclose(fgasopion);
    i=0;
    
    // Assuming table goes from min to max
    NHImin=NHI_arr[0];NHImax=NHI_arr[ntot-1];//NHI (W)
    NHmin=NH_arr[0];NHmax=NH_arr[ntot-1];//NH (X)
    tmin=T_arr[0];tmax=T_arr[ntot-1];//T (Y)
    umin=U_arr[0];umax=U_arr[ntot-1];//U (Z)
    print1 ("Minimum and Max values for NHI in gas ion opacity table: %e %e.\n",NHImin,NHImax);
    print1 ("Minimum and Max values for NH in gas ion opacity table: %e %e.\n",NHmin,NHmax);
    print1 ("Minimum and Max values for T in gas ion opacity table: %e %e.\n",tmin,tmax);
    print1 ("Minimum and Max values for U0 in gas ion opacity table: %e %e.\n",umin,umax);

    InitializeTable4D(&gasopion_tab, NHImin, NHImax, TV_GASOPACITYION_TABLE_NW,
                                  NHmin, NHmax, TV_GASOPACITYION_TABLE_NX,
                                  tmin, tmax, TV_GASOPACITYION_TABLE_NY,
                                  umin, umax, TV_GASOPACITYION_TABLE_NZ);
    //fill table with array
    for (h = 0; h < gasopion_tab.nz; h++){ //U0
        for (i = 0; i < gasopion_tab.ny; i++){ //T
            for (j = 0; j < gasopion_tab.nx; j++){ //NH
                for (k = 0; k < gasopion_tab.nw; k++){ //NHI
                    l=h*TV_GASOPACITYION_TABLE_NY*TV_GASOPACITYION_TABLE_NX*TV_GASOPACITYION_TABLE_NW +
                     i*TV_GASOPACITYION_TABLE_NX*TV_GASOPACITYION_TABLE_NW + j*TV_GASOPACITYION_TABLE_NW +k;
                    gasopion_tab.f[h][i][j][k] = O_arr[l];
    }}}}
    if (prank==0) WriteBinaryTable4D("gas_ion_opacity_table.bin",&gasopion_tab);//store information
    status = Table4DInterpolate(&gasopion_tab, 1.E16, 5.E18, 2400.0, 0.18, &scrh);
    print1 ("Test Gas Opacity Ion table: NHI=1E16 NH=5E18 U0=0.18 T=2400 K gasopacity=%e\n",scrh);


}

/* *************************************************************** * */
double Get_GasOpacityIon(double NHI, double NH, double T, double U0)
/*! 
*********************************************************************** */
{
 int status;
 double gasopacityion;


  // Setting values at exactly the limits breaks things badly
  
  if (T <= gasopion_tab.ymin) { 
    T = gasopion_tab.ymin + gasopion_tab.ymin*0.001;
  }
  if (T > gasopion_tab.ymax) { 
    T = (1.0-0.00001)* gasopion_tab.ymax;
  }

  /* Check dimensionless ionization parameter*/
  if (U0 <= gasopion_tab.zmin) { 
    U0 = gasopion_tab.zmin + gasopion_tab.zmin*0.001;
  }
  if (U0 > gasopion_tab.zmax) { 
    U0 = (1.0-0.00001)* gasopion_tab.zmax;
  }

  /* Now check limits NH*/
  if (NH <= gasopion_tab.xmin) { 
    NH = gasopion_tab.xmin + gasopion_tab.xmin*0.001;
  }

  if (NH >= gasopion_tab.xmax) { 
    NH = (1.0-0.00001)*gasopion_tab.xmax;
  }

  /* Now check limits NHI*/
  if (NHI <= gasopion_tab.wmin) { 
    NHI = gasopion_tab.wmin + gasopion_tab.wmin*0.001;
  }
  if (NHI >= gasopion_tab.wmax) { 
    NHI = (1.0-0.00001)*gasopion_tab.wmax;
  }
  if (NHI > NH) { 
      NHI=NH;
  }

  //print1 ("DOGASOPT-- %e %e %e\n",NHI, NH, U0);
  status = Table4DInterpolate(&gasopion_tab, NHI, NH, T, U0, &gasopacityion);
  if (status != 0){
    print ("! Get_GasOpacity: table interpolation failure (bound exceeded)\n");
    print ("!  U0=%e T=%e NH=%e NHI=%e \n",U0,T,NH,NHI);
    QUIT_PLUTO(1);
  }
  //print1 ("DOGASOPT-- OUT -- %e %e %e %e\n",NHI, NH, U0, gasopacity);
  return gasopacityion;

}


/* *************************************************************** * */
#ifndef TV_DUSTOPACITYION_TABLE_NZ   
    #define TV_DUSTOPACITYION_TABLE_NZ    16 //U0
#endif
#ifndef TV_DUSTOPACITYION_TABLE_NY   
    #define TV_DUSTOPACITYION_TABLE_NY    12 //T
#endif
#ifndef TV_DUSTOPACITYION_TABLE_NX
    #define TV_DUSTOPACITYION_TABLE_NX    11 //NH
#endif
#ifndef TV_DUSTOPACITYION_TABLE_NW
    #define TV_DUSTOPACITYION_TABLE_NW    17 //NHI
#endif
static Table4D dustopion_tab; /*A 4D table containing pre-computed values of 
                          dust opacity ion stored at equally spaced node 
                          values of Log(U0) Log(T) Log(NH) and Log(NHI) .*/


void Make_DustOpacityIonTable()
/*! 
*********************************************************************** */
{
  int h,i,j,k,l;
  int ntab,ntot,status;
  double scrh;
  double *U_arr, *T_arr, *NH_arr, *NHI_arr, *O_arr;
  double umin,umax,NHmin,NHmax,NHImin,NHImax,tmin,tmax;
  FILE *fdustopion;
    print1 (" > Computing dust opacity Ion 4D lookup table from ascii table...\n");
    fdustopion = fopen("DustIonOpacity_table.txt","r");
    if (fdustopion == NULL){
      print1 ("! MakeDustOpacityIonTable: DustIonOpacity_table.txt could not be found.\n");
      QUIT_PLUTO(1);
    }
    ntot=TV_DUSTOPACITYION_TABLE_NW*TV_DUSTOPACITYION_TABLE_NX*TV_DUSTOPACITYION_TABLE_NY*TV_DUSTOPACITYION_TABLE_NZ;
    O_arr = ARRAY_1D(ntot, double); //gas opacity (cm^2 g^-1)
    U_arr = ARRAY_1D(ntot, double); // U
    T_arr = ARRAY_1D(ntot, double); // T (K)
    NH_arr = ARRAY_1D(ntot, double); //NH (cm^-2)
    NHI_arr = ARRAY_1D(ntot, double); // NHI (cm^-2)

    fscanf(fdustopion, "%*[^\n]\n", NULL); //ignore first line
    ntab = 0;
    while (fscanf(fdustopion, "%lf  %lf  %lf  %lf  %lf\n", U_arr + ntab,
                                       T_arr + ntab, NH_arr + ntab,
                                       NHI_arr + ntab, O_arr + ntab)!=EOF) {
      ntab++;
    }
    fclose(fdustopion);
    i=0;
    
    // Assuming table goes from min to max
    NHImin=NHI_arr[0];NHImax=NHI_arr[ntot-1];//NHI (W)
    NHmin=NH_arr[0];NHmax=NH_arr[ntot-1];//NH (X)
    tmin=T_arr[0];tmax=T_arr[ntot-1];//T (Y)
    umin=U_arr[0];umax=U_arr[ntot-1];//U (Z)
    print1 ("Minimum and Max values for NHI in dust ion opacity table: %e %e.\n",NHImin,NHImax);
    print1 ("Minimum and Max values for NH in dust ion opacity table: %e %e.\n",NHmin,NHmax);
    print1 ("Minimum and Max values for T in dust ion opacity table: %e %e.\n",tmin,tmax);
    print1 ("Minimum and Max values for U0 in dust ion opacity table: %e %e.\n",umin,umax);

    InitializeTable4D(&dustopion_tab, NHImin, NHImax, TV_DUSTOPACITYION_TABLE_NW,
                                    NHmin, NHmax, TV_DUSTOPACITYION_TABLE_NX,
                                    tmin, tmax, TV_DUSTOPACITYION_TABLE_NY,
                                    umin, umax, TV_DUSTOPACITYION_TABLE_NZ);

    //fill table with array
    for (h = 0; h < dustopion_tab.nz; h++){ //U0
        for (i = 0; i < dustopion_tab.ny; i++){ //T
            for (j = 0; j < dustopion_tab.nx; j++){ //NH
                for (k = 0; k < dustopion_tab.nw; k++){ //NHI
                    l=h*TV_DUSTOPACITYION_TABLE_NY*TV_DUSTOPACITYION_TABLE_NX*TV_DUSTOPACITYION_TABLE_NW +
                       i*TV_DUSTOPACITYION_TABLE_NX*TV_DUSTOPACITYION_TABLE_NW + j*TV_DUSTOPACITYION_TABLE_NW + k;
                     dustopion_tab.f[h][i][j][k] = O_arr[l];
    }}}}
    if (prank==0) WriteBinaryTable4D("dust_ion_opacity_table.bin",&dustopion_tab);//store information
    status = Table4DInterpolate(&dustopion_tab, 1E16, 1E18, 2400.0, 0.18, &scrh);
    print1 ("Test Dust Opacity Ion table: NHI=1E16 NH=1E18 T=2400.0 U0=0.18 dutsionopacity=%e\n",scrh);


}

/* *************************************************************** * */
double Get_DustOpacityIon(double NHI, double NH, double T, double U0)
/*! 
*********************************************************************** */
{
 int status;
 double dustopacityIon;

  // Setting values at exactly the limits breaks things badly
  if (T <= dustopion_tab.ymin) { 
    T = dustopion_tab.ymin + dustopion_tab.ymin*0.001;
  }
  if (T > dustopion_tab.ymax) { 
    T = (1.0-0.00001)* dustopion_tab.ymax;
  }

  /* Check dimensionless ionization parameter*/
  if (U0 <= dustopion_tab.zmin) { 
    U0 = dustopion_tab.zmin + dustopion_tab.zmin*0.001;
  }
  if (U0 > dustopion_tab.zmax) { 
    U0 = (1.0-0.00001)* dustopion_tab.zmax;
  }

  /* Now check limits NH*/
  if (NH <= dustopion_tab.xmin) { 
    NH = dustopion_tab.xmin + dustopion_tab.xmin*0.001;
  }
  if (NH >= dustopion_tab.xmax) { 
    NH = (1.0-0.00001)*dustopion_tab.xmax;
  }

  /* Now check limits NHI*/
  if (NHI <= dustopion_tab.wmin) { 
    NHI = dustopion_tab.wmin + dustopion_tab.wmin*0.001;
  }
  if (NHI >= dustopion_tab.wmax) { 
    NHI = (1.0-0.00001)*dustopion_tab.wmax;
  }
  if (NHI > NH) { 
    NHI=NH;
  }

  status = Table4DInterpolate(&dustopion_tab, NHI, NH, T, U0, &dustopacityIon);
  if (status != 0){
    print ("! dust Opacity Ion: table interpolation failure (bound exceeded)\n");
    print ("! U0=%e T=%e NH=%e NHI=%e \n",U0,T,NH,NHI);
    QUIT_PLUTO(1);
  }
  return dustopacityIon;

}

/* *************************************************************** * */
#ifndef TV_DUSTOPACITYOPT_TABLE_NZ   
    #define TV_DUSTOPACITYOPT_TABLE_NZ    16 //U0
#endif
#ifndef TV_DUSTOPACITYOPT_TABLE_NY   
    #define TV_DUSTOPACITYOPT_TABLE_NY    12 //T
#endif
#ifndef TV_DUSTOPACITYOPT_TABLE_NX
    #define TV_DUSTOPACITYOPT_TABLE_NX    11 //NH
#endif
#ifndef TV_DUSTOPACITYOPT_TABLE_NW
    #define TV_DUSTOPACITYOPT_TABLE_NW    17 //NHI
#endif


static Table4D dustopopt_tab; /*A 4D table containing pre-computed values of 
                          dust opacity stored at equally spaced node 
                          values of Log(U0) Log(T) Log(NH) and Log(NHI) .*/


void Make_DustOpacityOptTable()
/*! 
*********************************************************************** */
{
  int h,i,j,k,l;
  int ntab,ntot,status;
  double scrh;
  double *U_arr, *T_arr, *NH_arr, *NHI_arr, *O_arr;
  double umin,umax,NHmin,NHmax,NHImin,NHImax,tmin,tmax;
  FILE *fdustopopt;
    print1 (" > Computing dust opacity opt 4D lookup table from ascii table...\n");
    fdustopopt = fopen("DustOpticalOpacity_table.txt","r");
    if (fdustopopt == NULL){
      print1 ("! Make_DustOpacityOptTable: dust_opt_opacity_table.dat could not be found.\n");
      QUIT_PLUTO(1);
    }
    ntot=TV_DUSTOPACITYOPT_TABLE_NW*TV_DUSTOPACITYOPT_TABLE_NX*TV_DUSTOPACITYOPT_TABLE_NY*TV_DUSTOPACITYOPT_TABLE_NZ;
    O_arr = ARRAY_1D(ntot, double); //gas opacity (cm^2 g^-1)
    U_arr = ARRAY_1D(ntot, double); // U
    T_arr = ARRAY_1D(ntot, double); // T (K)
    NH_arr = ARRAY_1D(ntot, double); //NH (cm^-2)
    NHI_arr = ARRAY_1D(ntot, double); // NHI (cm^-2)

    fscanf(fdustopopt, "%*[^\n]\n", NULL); //ignore first line
    ntab = 0;
    while (fscanf(fdustopopt, "%lf  %lf  %lf  %lf  %lf\n", U_arr + ntab,
                                       T_arr + ntab, NH_arr + ntab,
                                       NHI_arr + ntab, O_arr + ntab)!=EOF) {
      ntab++;
    }
    fclose(fdustopopt);
    i=0;
    
    // Assuming table goes from min to max
    NHImin=NHI_arr[0];NHImax=NHI_arr[ntot-1];//NHI (W)
    NHmin=NH_arr[0];NHmax=NH_arr[ntot-1];//NH (X)
    tmin=T_arr[0];tmax=T_arr[ntot-1];//T (Y)
    umin=U_arr[0];umax=U_arr[ntot-1];//U (Z)
    print1 ("Minimum and Max values for NHI in dust opt opacity table: %e %e.\n",NHImin,NHImax);
    print1 ("Minimum and Max values for NH in dust opt opacity table: %e %e.\n",NHmin,NHmax);
    print1 ("Minimum and Max values for T in dust opt opacity table: %e %e.\n",tmin,tmax);
    print1 ("Minimum and Max values for U0 in dust opt opacity table: %e %e.\n",umin,umax);

    InitializeTable4D(&dustopopt_tab, NHImin, NHImax, TV_DUSTOPACITYOPT_TABLE_NW,
                                      NHmin,NHmax, TV_DUSTOPACITYOPT_TABLE_NX,
                                      tmin, tmax, TV_DUSTOPACITYOPT_TABLE_NY,
                                      umin, umax, TV_DUSTOPACITYOPT_TABLE_NZ);
    //fill table with array
    for (h = 0; h < dustopopt_tab.nz; h++){ //U
        for (i = 0; i < dustopopt_tab.ny; i++){ //NH
            for (j = 0; j < dustopopt_tab.nx; j++){ //NH
                for (k = 0; k < dustopopt_tab.nw; k++){ //NH
                    l=h*TV_DUSTOPACITYOPT_TABLE_NY*TV_DUSTOPACITYOPT_TABLE_NX*TV_DUSTOPACITYOPT_TABLE_NW +
                         i*TV_DUSTOPACITYOPT_TABLE_NX*TV_DUSTOPACITYOPT_TABLE_NW + j*TV_DUSTOPACITYOPT_TABLE_NW + k;
                    dustopopt_tab.f[h][i][j][k] = O_arr[l];
    }}}}
    if (prank==0) WriteBinaryTable4D("dust_opt_opacity_table.bin",&dustopopt_tab);//store information
    status = Table4DInterpolate(&dustopopt_tab, 1E16, 1E18, 2400.0, 0.18, &scrh);
    print1 ("Test Dust Opacity Opt table: NHI=1E16 NH=1E18 T=2400.0 U0=0.18 dustopacityopt=%e\n",scrh);

}


/* *************************************************************** * */
double Get_DustOpacityOpt(double NHI, double NH, double T, double U0)
/*! 
*********************************************************************** */
{
 int status;
 double dustopacityOpt;

  // Setting values at exactly the limits breaks things badly
  if (T <= dustopopt_tab.ymin) { 
    T = dustopopt_tab.ymin + dustopopt_tab.ymin*0.001;
  }
  if (T > dustopopt_tab.ymax) { 
    T = (1.0-0.00001)* dustopopt_tab.ymax;
  }

  /* Check dimensionless ionization parameter*/
  if (U0 <= dustopopt_tab.zmin) { 
    U0 = dustopopt_tab.zmin + dustopopt_tab.zmin*0.001;
  }
  if (U0 > dustopopt_tab.zmax) { 
    U0 = (1.0-0.00001)* dustopopt_tab.zmax;
  }

  /* Now check limits NH*/
  if (NH <= dustopopt_tab.xmin) { 
    NH = dustopopt_tab.xmin + dustopopt_tab.xmin*0.001;
  }
  if (NH >= dustopopt_tab.xmax) { 
    NH = (1.0-0.00001)*dustopopt_tab.xmax;
  }

  /* Now check limits NHI*/
  if (NHI <= gasopion_tab.wmin) { 
    NHI = dustopopt_tab.wmin + dustopopt_tab.wmin*0.001;
  }
  if (NHI >= dustopopt_tab.wmax) { 
    NHI = (1.0-0.00001)*dustopopt_tab.wmax;
  }
  if (NHI > NH) { 
      NHI=NH;
  }

  status = Table4DInterpolate(&dustopopt_tab,  NHI, NH, T, U0, &dustopacityOpt);

  if (status != 0){
    print ("! Dust Opacity Opt: table interpolation failure (bound exceeded)\n");
    print ("! U0=%e T=%e NH=%e NHI=%e \n",U0,T,NH,NHI);
    QUIT_PLUTO(1);
  }
  return dustopacityOpt;

}



