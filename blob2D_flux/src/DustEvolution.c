#include "pluto.h"
#include "DustEvolution.h"
//#include "Radiation.h"
//#include "FLD.h"
//#include "PhysicalConstantsCGS.h"
#include "DomainDecomposition.h"
#include "interface.h"
//#include "definitions.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <petsc.h>

//
// Default Evaporation parameter:
//
int DustEvaporationFlag;
double Dust2GasMassRatio;


double EvaporationTemperature(double);
double EvaporateDust(double, double);
void DustEvolutionParallelCommunication(Data *, Grid *);
	

//void InitializeDust(){
//	
//	DACreateLocalVector(DACenter, &DustDensityVector);
//	DAVecGetArray(DACenter, DustDensityVector, &DustDensity);
//	
//}
//
//void FinalizeDust(){
//	
//	DAVecRestoreArray(DACenter, DustDensityVector, &DustDensity);
//	VecDestroy(DustDensityVector);
//	
//}


void DustEvolution(Data *data, Grid *grid){

	int kl, jl, il;
	double rhodust, rhogas, Tdust, Tgas, rhodustnew;
	//double frac, T_evap, a, T_nodust, Evaporation_Transition;
    double d2gmassratio;
	double d2gmassrationew, Dust2GasMassRatioMax, Dust2GasMassRatioMin, Tevap;
	
#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###       ... Dust Evolution ...                                             ###\n");
#endif

	KDOM_LOOP(kl){
		JDOM_LOOP(jl){
			IDOM_LOOP(il){
		
				// TODO: implement time-dependent dust evaporation/sublimation
				
				rhogas  = data->Vc[RHO][kl][jl][il];
				rhodust = data->DustDensity[kl][jl][il] * rhogas;
				Tdust   = data->DustTemperature[kl][jl][il];
				Tgas    = data->GasTemperature[kl][jl][il];
				
                d2gmassratio = rhodust/rhogas;
				rhodust *= ReferenceDensity;
				rhogas  *= ReferenceDensity;
				Tdust   *= ReferenceTemperature;
				Tgas    *= ReferenceTemperature;

				switch(DustEvaporationFlag){
					case 0:
						rhodustnew  = rhogas;
						rhodustnew *= Dust2GasMassRatio;
						break;
					case 1:
                        // Dust Sputtering
                        // asumming Tgas in Kelvin
                        // rho_gas in 
                        //dadt = 1e-6 * (1+(Tgas/1e6)**-3) ^-1 x ( n_H / cm^-3) micron per year
                        //rhodustnew = 
					case 2:
					case 3:
					case 4:
					case 5:
						rhodustnew  = rhogas;
						rhodustnew *= Dust2GasMassRatio;
						rhodustnew *= EvaporateDust(rhogas, Tdust);
						break;
                    case 6:
						Tevap = EvaporationTemperature(rhogas);
						Dust2GasMassRatioMax = 0.01;
//						Dust2GasMassRatioMin = 0.0;
//						Dust2GasMassRatioMin = 1e-20;
						
                        // CHANGES:
                        // - if(d2gmassratio >= Dust2GasMassRatioMax) deleted
                        // - 1e-10 safety margin added instead
                        
                        //						if(d2gmassratio >= Dust2GasMassRatioMax){
                        //                            d2gmassrationew = d2gmassratio;
                        //							rhodustnew = rhodust;
                        //						}
                        //						else{
                        if(Tdust == Tevap){
                            d2gmassrationew = d2gmassratio;
                            rhodustnew = rhodust;
                        }
                        else if(Tdust > Tevap){ // Evaporation
                            d2gmassrationew = exp(- g_dt / (100.0 * Tevap/(Tdust-Tevap) * (Dust2GasMassRatioMax-d2gmassratio + 1e-10)/Dust2GasMassRatioMax)) * d2gmassratio;
//                            d2gmassrationew = MAX(d2gmassrationew, Dust2GasMassRatioMin);
                            rhodustnew = d2gmassrationew * rhogas;
                        }
                        else if(Tdust < Tevap){ // Sublimation
                            d2gmassrationew = d2gmassratio - (exp(- g_dt / (10.0 * Tevap/(Tevap-Tdust) * Dust2GasMassRatioMax/(Dust2GasMassRatioMax-d2gmassratio))) - 1.0) * (Dust2GasMassRatioMax-d2gmassratio);
                            rhodustnew = d2gmassrationew * rhogas;
                        }
                        //                            if(fabs((d2gmassratio - Dust2GasMassRatioMax)/Dust2GasMassRatioMax) < 1e-3 && Tdust < Tevap){
                        //                            	d2gmassrationew = Dust2GasMassRatioMax;
                        //                                rhodustnew = d2gmassrationew * rhogas;
                        //                            }
                        //						}
						break;
				}
                
                if(rhodustnew == INFINITY){
					printf("d2gmassrationew = %e\n", d2gmassrationew);
					printf("d2gmassratio = %e\n", d2gmassratio);
					printf("rhodust = %e\n", rhodust);
					printf("rhogas = %e\n", rhogas);
					printf("Tevap = %e\n", Tevap);
					printf("Tdust = %e\n", Tdust);
					if(d2gmassratio == Dust2GasMassRatioMax){
						printf("d2gmassratio == Dust2GasMassRatioMax\n");
					}
					else{
						printf("d2gmassratio != Dust2GasMassRatioMax\n");
						printf("Dust2GasMassRatioMax-d2gmassratio = %e\n", Dust2GasMassRatioMax-d2gmassratio);
					}
                    
					exit(1);
				}
                
				if(DustEvaporationFlag == 6 && d2gmassrationew > 1.01*Dust2GasMassRatioMax){
					printf("ERROR: d2gmassrationew = %e\n", d2gmassrationew);
					printf("ERROR: d2gmassratio = %e\n", d2gmassratio);
					printf("ERROR: rhodust = %e\n", rhodust);
					printf("ERROR: rhogas = %e\n", rhogas);
					printf("ERROR: Tevap = %e\n", Tevap);
					printf("ERROR: Tdust = %e\n", Tdust);
					exit(1);
				}

				//rhodustnew /= ReferenceDensity;
				data->DustDensity[kl][jl][il] = rhodustnew / rhogas;
				
			}
		}
	}

    
    //
    // Zero-Gradient Boundary conditions for the x- and y-direction:
    //
	KTOT_LOOP(kl){
		JTOT_LOOP(jl){
			IBEG_LOOP(il){
				data->DustDensity[kl][jl][il] = data->DustDensity[kl][jl][IBEG];
			}
		}
	}
	KTOT_LOOP(kl){
		JTOT_LOOP(jl){
			IEND_LOOP(il){
				data->DustDensity[kl][jl][il] = data->DustDensity[kl][jl][IEND];
			}
		}
	}
#if DIMENSIONS > 1
	KTOT_LOOP(kl){
		JBEG_LOOP(jl){
			ITOT_LOOP(il){
				data->DustDensity[kl][jl][il] = data->DustDensity[kl][JBEG][il];
			}
		}
	}
	KTOT_LOOP(kl){
		JEND_LOOP(jl){
			ITOT_LOOP(il){
				data->DustDensity[kl][jl][il] = data->DustDensity[kl][JEND][il];
			}
		}
	}
#endif
    
    //
    // Parallel communication:
    //
#ifdef PARALLEL
    DustEvolutionParallelCommunication(data, grid);
#endif
    
}


// *********************
// ** EvaporateDust() **
// *********************
//
// Input: Dust Density     in g cm^-3 (cgs)
//        Dust Temperature in Kelvin  (cgs)
//
// Output: Fraction (between 0 and 1) of non-evaporated dust.
//
// Determines evaporation temperature using the Pollack et al. formulas (see Isella & Natta (2005)).
// Time for evaporation of dust is assumed to be zero.
//
double EvaporateDust(double GasDensity, double Tdust){

	double frac, Tevap, a, Tfullyevaporated;
	double Evaporation_Transition = 200; // in K
    
	//
    // Determine Evaporation Temperature:
    //
    Tevap = EvaporationTemperature(GasDensity);

    //
    // Determine fraction of evaporated dust:
    //
    switch(DustEvaporationFlag){
    case 0:
		//
        // No evaporation:
        //
		frac = 1;
		break;
    case 1:
        //
    	// Step function:
    	//
    	if(Tdust < Tevap)
    		frac = 1.0;
    	else
    		frac = 0.0;
        break;
    case 2:
        //
        // Linear Transition:
        //
        if(Tdust <= Tevap)
		    frac = 1;
	    else if(Tdust > Tevap + Evaporation_Transition)
		    frac = 0;
	    else
		    frac = 1 - (Tdust - Tevap) / Evaporation_Transition;
	    //
	    // This is exactly the same function with 'max/min' instead of 'if' statements:
	    //
	    //frac = 1 - (T_dust - T_evap) / Evaporation_Transition;
	    //frac = max(frac, 0);
	    //frac = min(frac, 1);
        break;
    case 3:
        //
        // Hysteresis:
        //
        a = 0.01;
        frac = - atan(a * (Tdust - Tevap)) / M_PI + 0.5;
        break;
	case 4:
		//
		// Hysteresis + steep decrease for high temperatures:
		//
		a = 0.01;
		Tfullyevaporated = 1.2 * Tevap;
		if(Tdust <= Tevap){
			// Hysteresis (case 3):
			frac = - atan(a * (Tdust - Tevap)) / M_PI + 0.5;
		}
		else if(Tdust >= Tfullyevaporated)
			frac = 0;
		else
			// Linear transition (case 2):
			frac = 0.5 * (1 - (Tdust - Tevap) / (Tfullyevaporated - Tevap));
		break;
	case 5:
		//
		// Hysteresis + steep decrease for high temperatures:
		//
		a = 0.05;
		Tfullyevaporated = 1.05 * Tevap;
		if(Tdust <= Tevap){
			// Hysteresis (case 3):
			frac = - atan(a * (Tdust - Tevap)) / M_PI + 0.5;
		}
		else if(Tdust >= Tfullyevaporated)
			frac = 0;
		else
			// Linear transition (case 2):
			frac = 0.5 * (1 - (Tdust - Tevap) / (Tfullyevaporated - Tevap));
		break;
    default:
    	PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
    	PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
    	PetscPrintf(PETSC_COMM_WORLD, "ERROR DustEvaporationFlag = %d not available, see 'Makemake.conf'.\n", DustEvaporationFlag);
    	PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
    	PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
    	exit(1);
    }
	
#if DEBUGGING > 0
	//
	// check:
	//
	if(frac < 0){
		PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
    	PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
    	PetscPrintf(PETSC_COMM_WORLD, "ERROR frac = %e < 0 in routine EvaporateDust() in file Opacity.c\n", frac);
    	PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
    	PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
    	exit(1);
	}
	if(frac > 1){
		PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
    	PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
    	PetscPrintf(PETSC_COMM_WORLD, "ERROR frac = %e > 1 in routine EvaporateDust() in file Opacity.c\n", frac);
    	PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
    	PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
    	exit(1);
	}
#endif

	return frac;
}


// ******************************
// ** EvaporationTemperature() **
// ******************************
//
// Determines evaporation temperature using the Pollack et al. formulas (see Isella & Natta (2005)).
//
double EvaporationTemperature(double GasDensityCGS){

	double Evaporation_g        = 2000.0; // in K
	double Evaporation_exponent = 0.0195; // dimensionless

    return Evaporation_g * pow(GasDensityCGS, Evaporation_exponent); // in K

}


void DustEvolutionParallelCommunication(Data *data, Grid *grid){
    
    int par_dim[3] = {0, 0, 0};
    
    //
    // Check the number of processors in each direction:
    //
    D_EXPAND(par_dim[0] = grid[IDIR].nproc > 1;  ,
             par_dim[1] = grid[JDIR].nproc > 1;  ,
             par_dim[2] = grid[KDIR].nproc > 1;)
    
#if PERIODICX == 1
        if(par_dim[0] == 0) par_dim[0] = 1;
#endif
#if PERIODICY == 1
        if(par_dim[1] == 0) par_dim[1] = 1;
#endif
#if PERIODICZ == 1
        if(par_dim[2] == 0) par_dim[2] = 1;
#endif 
    
    //
    // Exchange data between parallel processors:
    //
    MPI_Barrier(MPI_COMM_WORLD);
    
    AL_Exchange_dim((char *) data->DustDensity[0][0], par_dim, SZ);
    
    MPI_Barrier(MPI_COMM_WORLD);
    
}


