#include "pluto.h"
#include "Radiation.h"
#include "PhysicalConstantsCGS.h"
#include "Opacity.h"
#include "DomainDecomposition.h"
#include "DustEvolution.h"
#include "Molecules.h"
#include "ReadRestrictionsConfiguration.h"
#include "interface.h"

#if FLUXLIMITEDDIFFUSION == YES
#include "FLD.h"
#endif
#if IRRADIATION == YES
#include "Irradiation.h"
#endif

#if STELLAREVOLUTION == YES
#include "StellarEvolution.h"
#endif

//#if IONIZATION == YES
//#include "Ionization.h"
//#endif

#include <petsc.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>  // for exit()



int  RadiationFlag;
int  StellarRadiativeForceFlag;
int  ThermalRadiativeForceFlag;
int  RadiativeFluxLimiterFlag;
double MinimumOpticalDepth, MaximumOpticalDepth;


void GetPressure(Data *, Grid *, Time_Step *);
void UpdateDustTemperature(Data *, Grid *);
void UpdateDustTemperature_EquilibriumTemperature(Data *, Grid *);
void UpdateGasTemperature(Data *, Grid *);
void UpdatePressure(Data *, Grid *);
void RadiationParallelCommunication(Data *, Grid *);


    // JONORBE:were in FLD
int PreconditionerRadiation;
int    ConvergenceSpecification;
double ConvergenceRTOL;
double ConvergenceABSTOL;
double ConvergenceDTOL;
int    ConvergenceMAXITS;
int BoundaryRadiationEnergyMinX, BoundaryRadiationEnergyMaxX, BoundaryRadiationEnergyMinY, BoundaryRadiationEnergyMaxY, BoundaryRadiationEnergyMinZ, BoundaryRadiationEnergyMaxZ;
double BoundaryTemperatureDirichlet;


///////

void InitializeRadiation(Grid *grid){
	
    //	InitializeDust();
	
#if FLUXLIMITEDDIFFUSION == YES
	InitializeFLD(grid);
#endif
	
#if IRRADIATION == YES
	InitializeIrradiation();
#endif
	
}

void FinalizeRadiation(){
	
    //	FinalizeDust();
	
#if FLUXLIMITEDDIFFUSION == YES
	FinalizeFLD();
#endif
	
#if IRRADIATION == YES
	FinalizeIrradiation();
#endif
	
}



// *****************
// ** Radiation() **
// *****************
//
// 'Timetable' of Radiation subroutine calls.
//
void Radiation(Data *data, Grid *grid, Time_Step *Dts){
    
    int kl, jl, il;
	
    //
	// Get Hydrodynamical changes to Thermodynamics:
	//
	GetPressure(data, grid, Dts);
    
#if IRRADIATION == YES
	if(IrradiationFlag != 0) Irradiation(data, grid);
    // JONORBE: approximation so that 
    // only Irradiation is important
    // now is redundant but should work when FLUXLIMITEDDIFFUSION = NO
    if(RadiationFlag == 0){
        TOT_LOOP(kl,jl,il){
            data->RadiationEnergyDensity[kl][jl][il] = 0;
        }
    }
#endif
    
#if FLUXLIMITEDDIFFUSION == YES
    if(RadiationFlag == 0){
        TOT_LOOP(kl,jl,il){
            data->RadiationEnergyDensity[kl][jl][il] = 0;
        }
    }
	else if(RadiationFlag == 1){
        FluxLimitedDiffusion_EquilibriumTemperature(data, grid);
    }
    else if(RadiationFlag == 2){
        FluxLimitedDiffusion(data, grid);
    }
    else{
        printf("ERROR  In Radiation.c:\n");
        printf("ERROR   RadiationFlag = %d not available yet.\n", RadiationFlag);
        exit(1);
    }
    
	//else VecZeroEntries(RadiationEnergyVector);
#endif
	
	//
	// Update Dust Temperature due to Radiation Transport:
	//
    if(RadiationFlag == 0 || RadiationFlag == 2) UpdateDustTemperature(data, grid);
    else if(RadiationFlag == 1) UpdateDustTemperature_EquilibriumTemperature(data, grid);
    
	//
	// Update Thermodynamics:
	//
// ONORBE: No need to UpdateGasTemperature nor UpdatePressure
// BUT DEFINITELY NOT PRESSURE
#if IONIZATION == NO
	UpdateGasTemperature(data, grid);
// all heating managed with tables
// so we definitely no need to UpdatePressure
//#if IRRADIATION == YES
//    if(IrradiationFlag != 0)
//#endif
//#if FLUXLIMITEDDIFFUSION == YES
//	if(RadiationFlag != 0)
//#endif
       // UpdatePressure(data, grid);
#endif
    
    //
    // Zero-Gradient Boundary conditions for the x- and y-direction:
    //
	KTOT_LOOP(kl){
		JTOT_LOOP(jl){
			IBEG_LOOP(il){
				data->DustTemperature[kl][jl][il] = data->DustTemperature[kl][jl][IBEG];
#if IONIZATION == NO
				data->GasTemperature[kl][jl][il]  = data->GasTemperature[kl][jl][IBEG];
				data->Vc[PRS][kl][jl][il]         = data->Vc[PRS][kl][jl][IBEG];
#endif
			}
		}
	}
	KTOT_LOOP(kl){
		JTOT_LOOP(jl){
			IEND_LOOP(il){
				data->DustTemperature[kl][jl][il] = data->DustTemperature[kl][jl][IEND];
#if IONIZATION == NO
				data->GasTemperature[kl][jl][il]  = data->GasTemperature[kl][jl][IEND];
				data->Vc[PRS][kl][jl][il]         = data->Vc[PRS][kl][jl][IEND];
#endif
			}
		}
	}
#if DIMENSIONS > 1
	KTOT_LOOP(kl){
		JBEG_LOOP(jl){
			ITOT_LOOP(il){
				data->DustTemperature[kl][jl][il] = data->DustTemperature[kl][JBEG][il];
#if IONIZATION == NO
				data->GasTemperature[kl][jl][il]  = data->GasTemperature[kl][JBEG][il];
				data->Vc[PRS][kl][jl][il]         = data->Vc[PRS][kl][JBEG][il];
#endif
			}
		}
	}
	KTOT_LOOP(kl){
		JEND_LOOP(jl){
			ITOT_LOOP(il){
				data->DustTemperature[kl][jl][il] = data->DustTemperature[kl][JEND][il];
#if IONIZATION == NO
                data->GasTemperature[kl][jl][il]  = data->GasTemperature[kl][JEND][il];
				data->Vc[PRS][kl][jl][il]         = data->Vc[PRS][kl][JEND][il];
#endif
			}
		}
	}
#endif
    
    //
    // Parallel communication:
    //
#ifdef PARALLEL
    RadiationParallelCommunication(data, grid);
#endif
    
    double Tdust;
    int exit_flag = 0;
    if(DEBUGGING_T_dust_max >= 0){
        KTOT_LOOP(kl){
            JTOT_LOOP(jl){
                ITOT_LOOP(il){
                    Tdust = data->DustTemperature[kl][jl][il];
                    if(Tdust > DEBUGGING_T_dust_max){
                        printf("ERROR   Tdust[%d][%d][%d] =  %e K > %e K\n", kl, jl, il, Tdust*ReferenceTemperature, DEBUGGING_T_dust_max*ReferenceTemperature);
                        printf("ERROR    rho[%d][%d][%d] =   %e g cm^-3\n", kl, jl, il, data->Vc[RHO][kl][jl][il]*ReferenceDensity);
                        printf("ERROR    d2g[%d][%d][%d] =   %e\n", kl, jl, il, data->DustDensity[kl][jl][il]);
                        printf("ERROR    Erad[%d][%d][%d] =  %e K\n", kl, jl, il, pow(data->RadiationEnergyDensity[kl][jl][il]/a_RadiationConstant, 0.25) * ReferenceTemperature);
                        printf("ERROR    VX1[%d][%d][%d] =   %e km s^-1\n", kl, jl, il, data->Vc[VX1][kl][jl][il]*ReferenceVelocity*1e-5);
                        printf("ERROR    Tdust[%d][%d][%d] = %e K\n", kl, jl, il+1, data->DustTemperature[kl][jl][il+1]*ReferenceTemperature);
                        printf("ERROR    rho[%d][%d][%d] =   %e g cm^-3\n", kl, jl, il+1, data->Vc[RHO][kl][jl][il+1]*ReferenceDensity);
                        printf("ERROR    d2g[%d][%d][%d] =   %e\n", kl, jl, il+1, data->DustDensity[kl][jl][il+1]);
                        printf("ERROR    Erad[%d][%d][%d] =  %e K\n", kl, jl, il+1, pow(data->RadiationEnergyDensity[kl][jl][il+1]/a_RadiationConstant, 0.25) * ReferenceTemperature);
                        printf("ERROR    VX1[%d][%d][%d] =   %e km s^-1\n", kl, jl, il+1, data->Vc[VX1][kl][jl][il+1]*ReferenceVelocity*1e-5);
                        exit_flag = 1;
                    }
                }
            }
        }
        if(exit_flag){
            //        KTOT_LOOP(k){
            //            JTOT_LOOP(j){
            //                ITOT_LOOP(i){
            //                    printf("ERROR    Tdust[%d][%d][%d] = %e K\n", kl, jl, il, data->DustTemperature[kl][jl][il]*ReferenceTemperature);
            //                    printf("ERROR    d2g[%d][%d][%d] =   %e\n", kl, jl, il, data->DustDensity[kl][jl][il]);
            //                }
            //            }
            //        }
            exit(1);
        }
    }
    
}



void GetPressure(Data *data, Grid *grid, Time_Step *Dts){
	
	int kl, jl, il;
	double Pgasnew, Tdustold, Tdustnew, rhonew, x, y;
    double dtau, bla;
    double Erad;
    double NHI,NH,T,U0;
    double DustOpacityOpt,DustOpacityIon,dtr;
    double F_f,n_f,c_f,rho_f,U_factor;
    double FluxIon0,f_ion,rhodust;
	
#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###       ... Get Pressure ...                                               ###\n");
#endif
	
    Dts->dt_rad = 1e+38; // default maximum value of Pluto-MHD code
    // Flux from code units to erg/s/cm^2
    // ReferencePower/ReferenceArea
    // ReferencePower = ReferenceEnergy / ReferenceTime;
    // ReferenceEnergy = UNIT_DENSITY  * pow(UNIT_VELOCITY, 2) * pow(UNIT_LENGTH,3)
    // ReferenceTime = UNIT_LENGTH/UNIT_VELOCITY
    F_f= UNIT_DENSITY  * pow(UNIT_VELOCITY, 3);
    //Normalizaztion factor: mean ionizing photon energy in erg (36 eV to erg) 
    n_f = mipe_in_eV*CONST_eV; //1.196185e-10
    //speed of light in cm/s
    c_f = c_SpeedOfLight*ReferenceVelocity;
    //factor to transform density from code units to hydrogen atoms/cm^3
    rho_f = UNIT_DENSITY/CONST_mp; //5.978638e+03
    // factor to apply to U to get the ionization parameter in same units as table
    U_factor = F_f/n_f/c_f/rho_f; //
    // fraction of flux ionizing part
    f_ion=0.486;
    

	FluxIon0 = IrradiationLuminosity;
	FluxIon0 /= 4.0 * M_PI * pow(grid[IDIR].xl[IBEG], 2);
    FluxIon0 *= f_ion;

	KDOM_LOOP(kl){
		JDOM_LOOP(jl){
			IDOM_LOOP(il){
                
				rhonew   = data->Vc[RHO][kl][jl][il];
				Pgasnew  = data->Vc[PRS][kl][jl][il];
				rhodust = data->DustDensity[kl][jl][il] * rhonew;
                Tdustold = data->DustTemperature[kl][jl][il];
                NH = data->NH[kl][jl][il];// this is NH just before the cell in XXX units 
                NHI = data->NHI[kl][jl][il];// this is NHI just before the cell in XXX units 
                U0 = U_factor*FluxIon0/rhonew; //This is dimensionless ionization parameter, same units as table in gas opacity

#if IONIZATION == YES
                x        = data->IonizationFraction[kl][jl][il];
                y        = data->NeutralFraction[kl][jl][il];
#else
                x        = data->IonizationFraction[kl][jl][il];
                y        = 1.0 - data->IonizationFraction[kl][jl][il];
#endif
                
                // TODO: if PDVinRT is used, this whole routine is negligible!
                //if(x == 0)
                //    Tdustnew = MolarMass(x) / R_UniversalGasConstant * Pgasnew / rhonew;
				//else
                //    Tdustnew = Tdustold;

                // JONORBE, this is Tgas
                Tdustnew = ReferenceTemperature*MolarMass(x)*Pgasnew/rhonew/R_UniversalGasConstant; //Kelvin
                
                //
				// check the linearization approach:
                // TODO: here, the dT due to hydro is handled, maybe also the dT due to radiation has been handled accordingly
                //
                // for dT = 30%, dt decreases about 90%
                // for dT = 20%, dt decreases about 73%
                // for dT = 10%, dt decreases about 27%
                // for dT =  7%, dt stays constant
                // for dT =  5%, dt is allowed to increase about 20%
                // for dT =  1%, dt is allowed to increase about 80%
                //
                //Dts->dt_rad = MIN(Dts->dt_rad, g_dt * 2.0 * exp(- 10.0 * fabs(Tdustnew - Tdustold) / Tdustold));
                // => sim freezes
                //Dts->dt_rad = MIN(Dts->dt_rad, g_dt * 2.0 * exp(- 1.0 * fabs(Tdustnew - Tdustold) / Tdustold));
                                
                //				Tdustnew = MIN(Tdustnew, 1.1 * Tdustold);
                //				Tdustnew = MAX(Tdustnew, 0.9 * Tdustold);
                //				if(fabs(Tdustnew-Tdustold) / Tdustold > 0.1){
                //					printf("ERROR: dT/T (%d,%d,%d)= %e\n", kl,jl,il, fabs(Tdustnew-Tdustold) / Tdustold);
                //					printf("Tdustold = %e K\n", Tdustold * ReferenceTemperature);
                //					printf("Tdustnew = %e K\n", Tdustnew * ReferenceTemperature);
                //					printf("Pgasnew  = %e erg cm^-3\n", Pgasnew * ReferencePressure);
                //					printf("rhonew   = %e g cm^-3\n", rhonew * ReferenceDensity);
                //					printf("VX1      = %e km s^-1\n", data->Vc[VX1][kl][jl][il] * ReferenceVelocity*1e-5);
                //					printf("d2g      = %e\n", data->DustDensity[kl][jl][il]);
                //					exit(1);
                //				}
				
                
				// 'molecular line cooling' of shocks:
                //				if(Tdustnew > Tdustold){
                //					dust2gassmassratio = data->DustDensity[kl][jl][il];
                //
                //					//Tdustnew = Tdustold + (Tdustnew - Tdustold) * 100.0 * dust2gassmassratio;
                //					Tdustnew = Tdustnew - (Tdustnew - Tdustold) * exp(-500.0 * dust2gassmassratio);
                //				}
                
                // handle RHOMIN regions?
                
                
				//#if IONIZATION != 0
				//#endif
				
#if PDVINRT == YES
                Tdustnew = Tdustold;
//                Dts->dt_rad = 1000.0 * g_dt;
#endif
				//
                // Ramsey & Dullemond (2014), eq. 24; from Krumholz et al. (2007)
                //
                DustOpacityOpt = RosselandMeanDustOpacity(NHI,NH,Tdustnew,U0,1.0);
                DustOpacityIon = RosselandMeanDustOpacity(NHI,NH,Tdustnew,U0,-1.0);
                dtau = rhodust * grid[IDIR].dx[il] * (DustOpacityOpt+DustOpacityIon);
                if(dtau > 1e-5) bla = 1.0 - exp(-dtau);
                else bla = dtau;
                //                bla = MAX(dtau, 1e-4);
                //                bla = MIN(dtau, 0.5);
                Erad  = data->RadiationEnergyDensity[kl][jl][il];
#if IRRADIATION == YES
                Erad += data->IrradiationPowerDensity[kl][jl][il] * g_dt;
#endif
                Dts->dt_rad = MIN(Dts->dt_rad, grid[IDIR].dx[il] / sqrt(4.0/9.0 * Erad / data->Vc[RHO][kl][jl][il] * bla));
                dtr = grid[IDIR].dx[il] / sqrt(4.0/9.0 * Erad / data->Vc[RHO][kl][jl][il] * bla);

                if(T_dust_min >= 0) Tdustnew = MAX(Tdustnew, T_dust_min);
				if(T_dust_max >= 0) Tdustnew = MIN(Tdustnew, T_dust_max);
                
				//data->DustTemperature[kl][jl][il] = Tdustnew;
				data->DustTemperature[kl][jl][il] = Tdustnew;
				
			}
		}
	}
	
}



void UpdateDustTemperature_EquilibriumTemperature(Data *data, Grid *grid){
    
	int kl, jl, il, iter;
	double rhodust, rhogas, Tgas, Erad, Irad, kapparho;
    double Tdustold, Tdustnew, mix, DeltaABSTOL, DeltaRTOL;
    double ConvergenceRTOL, ConvergenceABSTOL;
    int    ConvergenceMAXITS, ConvergenceMINITS;
    double NHI=1.0,NH=1.0,T=1.0,U0=1.0;
    print ("! UpdateDustTemperature_EquilibriumTemperature: NOT WORK\n");
    QUIT_PLUTO(1);
    
#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###       ... Update Dust Temperature ...   Equi-T                           ###\n");
#endif
    
    //
    // Set parameters for Newton-Raphson like iterative solver:
    //
    mix               = 0.6;
    ConvergenceRTOL   = 1e-3;
    ConvergenceABSTOL = 0.1/ReferenceTemperature;
    ConvergenceMAXITS = 100;
    ConvergenceMINITS = 4;
    
	KDOM_LOOP(kl){
		JDOM_LOOP(jl){
			IDOM_LOOP(il){
				
				rhogas   = data->Vc[RHO][kl][jl][il];
				rhodust  = data->DustDensity[kl][jl][il] * rhogas;
				Tgas     = data->GasTemperature[kl][jl][il];
				Tdustnew = data->DustTemperature[kl][jl][il];
                Tdustold = Tdustnew;
				Erad     = data->RadiationEnergyDensity[kl][jl][il];
#if IRRADIATION == YES
                Irad     = data->IrradiationPowerDensity[kl][jl][il];
#else
                Irad     = 0.0;
#endif
                //
                // Reset iteration parameter:
                //
                DeltaRTOL   = 10.0 * ConvergenceRTOL;
                DeltaABSTOL = 10.0 * ConvergenceABSTOL;
                iter        = 0;
                
                while(
                      ((
                        DeltaRTOL   > ConvergenceRTOL
                        &&
                        DeltaABSTOL > ConvergenceABSTOL
                        )
                       ||
                       iter < ConvergenceMINITS)
#if IRRADIATION == NO
                      &&
                      iter == 0
#endif
                      ){
                    
                    //
                    // Mix with Temperature from last step to avoid iterative jumping:
                    //
                    Tdustnew = mix * Tdustnew + (1.0 - mix) * Tdustold;
                    Tdustold = Tdustnew;
                    
                    //
                    // Calculate new Temperature:
                    //
                    // JONORBE: THIS IS WRONG
                    kapparho = PlanckMeanDustOpacity(NHI,NH,T,U0,1.0) * rhodust + PlanckMeanGasOpacity(NHI,NH,T,U0) * rhogas;
                    kapparho = LimitOpticalDepth(grid, kl, jl, il, kapparho);
                    
                    Tdustnew =
                    pow(
                        (Erad + Irad / c_SpeedOfLight / kapparho)
                        /
                        a_RadiationConstant
                        , 0.25);
                    
                    //
                    // Calculate convergence criteria:
                    //
                    DeltaABSTOL = fabs(Tdustnew - Tdustold);
                    DeltaRTOL   = DeltaABSTOL / Tdustold;
                    
                    //
                    // Next iteration:
                    //
                    iter ++;
                    
                    //
                    // Quit in case of non-convergence:
                    //
                    if(iter == ConvergenceMAXITS) {
                        printf("ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
                        printf("ERROR\n");
                        printf("ERROR No Convergence reached in iterative Temperature Update in cell (%d, %d, %d)\n", kl,jl,il);
                        printf("ERROR %d internal iterations were done.\n", iter);
                        printf("ERROR\n");
                        printf("ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
                        exit(1);
                    }
                    
                } // while()-loop
                
                if(T_dust_min >= 0) Tdustnew = MAX(Tdustnew, T_dust_min);
				if(T_dust_max >= 0) Tdustnew = MIN(Tdustnew, T_dust_max);

				data->DustTemperature[kl][jl][il] = Tdustnew;
				
			}
		}
	}
    
}



// TODO: Implement check: Linearization of T^4 equation is only valid for small changes in T !!
void UpdateDustTemperature(Data *data, Grid *grid){
    
	int    kl, jl, il;
	double xabbr, rhodust, rhogas, Tdust, Tgas, Tdustnew, Erad, c_V, x, kapparho;
    double Tdustold, DeltaABSTOL, DeltaRTOL, mix;
    int    iter;
    double ConvergenceRTOL, ConvergenceABSTOL;
    int    ConvergenceMAXITS, ConvergenceMINITS;
	
#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###       ... Update Dust Temperature ...                                    ###\n");
#endif
    
    //
    // Set parameters for Newton-Raphson like iterative solver:
    //
    mix               = 0.6;
    ConvergenceRTOL   = 1e-3;
    ConvergenceABSTOL = 0.1/ReferenceTemperature;
    ConvergenceMAXITS = 4000; // JONORBE 400;
    ConvergenceMINITS = 4;
    
	KDOM_LOOP(kl){
		JDOM_LOOP(jl){
			IDOM_LOOP(il){
				
				rhogas    = data->Vc[RHO][kl][jl][il];
				rhodust   = data->DustDensity[kl][jl][il] * rhogas;
				Tgas      = data->GasTemperature[kl][jl][il];
				Tdust     = data->DustTemperature[kl][jl][il];
				Erad      = data->RadiationEnergyDensity[kl][jl][il];
#if IONIZATION == YES
                x         = data->IonizationFraction[kl][jl][il];
#else
                x         = data->IonizationFraction[kl][jl][il];
#endif
                c_V       = R_UniversalGasConstant / (MolarMass(x) * (gamma_AdiabaticIndex - 1.0));
                
                
                // TODO-check: Does PlanckMeanDust(Tdust, Tdust, rhodust) require an iterative Newton-Raphson like update for Tdust?!
                // --> at least stabilizes the parallelization of the freq.-de[. Pascucci test
//                kapparho  = PlanckMeanDustOpacity(Tdust, Tdust, rhodust) * rhodust + PlanckMeanGasOpacity(Tgas, Tgas, rhogas) * rhogas;
//                kapparho  = LimitOpticalDepth(grid, kl, jl, il, kapparho);
//                xabbr     = kapparho * c_SpeedOfLight * g_dt;
//                
//                Tdustnew  = (3.0 * xabbr * a_RadiationConstant * pow(Tdust, 4) + rhogas * c_V * Tdust);
//#if FLUXLIMITEDDIFFUSION == YES
//				Tdustnew += xabbr * Erad;
//#endif
//#if IRRADIATION == YES
//				Tdustnew += g_dt * data->IrradiationPowerDensity[kl][jl][il];
//#endif
//				Tdustnew /= rhogas * c_V + 4.0 * xabbr * a_RadiationConstant * pow(Tdust, 3);
                
                
                Tdustnew  = data->DustTemperature[kl][jl][il];
                Tdustold  = Tdustnew;
                
                //
                // Reset iteration parameter:
                //
                DeltaRTOL   = 10.0 * ConvergenceRTOL;
                DeltaABSTOL = 10.0 * ConvergenceABSTOL;
                iter        = 0;
                
                while(
                      ((
                        DeltaRTOL   > ConvergenceRTOL
                        &&
                        DeltaABSTOL > ConvergenceABSTOL
                        )
                       ||
                       iter < ConvergenceMINITS)
#if IRRADIATION == NO
                      &&
                      iter == 0
#endif
                      ){
                    
                    //
                    // Mix with Temperature from last step to avoid iterative jumping:
                    //
                    Tdustnew = mix * Tdustnew + (1.0 - mix) * Tdustold;
                    Tdustold = Tdustnew;
                    
                    //
                    // Calculate new Temperature:
                    Tdustnew  = rhogas * c_V * Tdustold;
                    //Tdustnew  += 3.0 * xabbr * a_RadiationConstant * pow(Tdustold, 4); 
#if FLUXLIMITEDDIFFUSION == YES
                    // JONORBE: this kappa are wrong but
                    // we do not use them
                    print ("! UpdateDustTemperature: NOT WORK\n");
                    QUIT_PLUTO(1);
                    kapparho  = PlanckMeanDustOpacity(0.0,0.0,0.0,0.0,1.0) * rhodust + PlanckMeanGasOpacity(0.0,0.0,0.0,0.0) * rhogas;
                    kapparho  = LimitOpticalDepth(grid, kl, jl, il, kapparho);
                    xabbr     = kapparho * c_SpeedOfLight * g_dt;
                    Tdustnew += xabbr * Erad;
#endif
#if IRRADIATION == YES
                    Tdustnew += g_dt * data->IrradiationPowerDensity[kl][jl][il];
#endif
                    Tdustnew /= rhogas * c_V; // + 4.0 * xabbr * a_RadiationConstant * pow(Tdustold, 3);
                    
                    //
                    // Calculate convergence criteria:
                    //
                    DeltaABSTOL = fabs(Tdustnew - Tdustold);
                    DeltaRTOL   = DeltaABSTOL / Tdustold;
                    
                    //
                    // Next iteration:
                    //
                    iter ++;
                    
                    //
                    // Quit in case of non-convergence:
                    //
                    if(iter == ConvergenceMAXITS) {
                        printf("ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
                        printf("ERROR\n");
                        printf("ERROR No Convergence reached in iterative Temperature Update in cell (%d, %d, %d)\n", kl,jl,il);
                        printf("ERROR Tdust_orig=%e Tdustold = %e Tdustnew = %e \n", data->DustTemperature[kl][jl][il], Tdustold, Tdustnew);
                        printf("ERROR DeltaABSTOL=%e must be smaller than ConvergenceABSTOL=%e\n", DeltaABSTOL,ConvergenceABSTOL);
                        printf("ERROR DeltaRTOL=%e must be smaller than ConvergenceRTOL=%e\n", DeltaABSTOL,ConvergenceRTOL);
                        printf("ERROR %d internal iterations were done.\n", iter);
                        printf("ERROR See Radiation.c UpdateDustTemperature() for details.\n");
                        printf("ERROR\n");
                        printf("ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
                        exit(1);
                    }
                    
                } // while()-loop
                
                if(RadiationFlag == 2 && Tdustnew > 1.5*Tdust){
                    printf("WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
                    printf("WARNING  dT too high for linearization to be valid.\n");
                    printf("WARNING   cell = (%d, %d, %d)\n", kl, jl, il);
                    printf("WARNING   Tdustold = %e K\n", Tdust*ReferenceTemperature);
                    printf("WARNING   Tdustnew = %e K\n", Tdustnew*ReferenceTemperature);
                    printf("WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
                    //Tdustnew = 1.5*Tdust;
                }
                
                if(T_dust_min >= 0) Tdustnew = MAX(Tdustnew, T_dust_min);
				if(T_dust_max >= 0) Tdustnew = MIN(Tdustnew, T_dust_max);
                
				data->DustTemperature[kl][jl][il] = Tdustnew;
				
			}
		}
	}
    
}



#if IONIZATION == NO
void UpdateGasTemperature(Data *data, Grid *grid){
	
	int kl, jl, il;
	
#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###       ... Update Gas Temperature ...                                     ###\n");
#endif
	
	KDOM_LOOP(kl){
		JDOM_LOOP(jl){
			IDOM_LOOP(il){
                
				data->GasTemperature[kl][jl][il] = data->DustTemperature[kl][jl][il];
				
			}
		}
	}
	
}



void UpdatePressure(Data *data, Grid *grid){
	
	int kl, jl, il;
	double rhogas, Tgas, Pgas, ionx;
	
#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###       ... Update Pressure ...                                            ###\n");
#endif
	
	KDOM_LOOP(kl){
		JDOM_LOOP(jl){
			IDOM_LOOP(il){
                
				Tgas   = data->GasTemperature[kl][jl][il];
				rhogas = data->Vc[RHO][kl][jl][il];
	            ionx = data->IonizationFraction[kl][jl][il]; // ionization fraction of the cell
                
				Pgas   = R_UniversalGasConstant / MolarMass(ionx) * rhogas * Tgas;
                
				data->Vc[PRS][kl][jl][il] = Pgas;
                				
			}
		}
	}
	
}
#endif



void RadiationParallelCommunication(Data *data, Grid *grid){
    
    int par_dim[3] = {0, 0, 0};
    
    //
    // Check the number of processors in each direction:
    //
    D_EXPAND(par_dim[0] = grid[IDIR].nproc > 1;  ,
             par_dim[1] = grid[JDIR].nproc > 1;  ,
             par_dim[2] = grid[KDIR].nproc > 1;)
    
#if PERIODICX == 1
        if(par_dim[0] == 0) par_dim[0] = 1;
#endif
#if PERIODICY == 1
        if(par_dim[1] == 0) par_dim[1] = 1;
#endif
#if PERIODICZ == 1
        if(par_dim[2] == 0) par_dim[2] = 1;
#endif 
    //
    // Call userdef internal boundary (side == 0):
    //
    //#if INTERNAL_BOUNDARY == YES
    //    UserDefBoundary(data, NULL, 0, grid);
    //#endif
    
    //
    // Exchange data between parallel processors:
    //
    MPI_Barrier(MPI_COMM_WORLD);
    
    AL_Exchange_dim((char *) data->DustTemperature[0][0], par_dim, SZ);
    AL_Exchange_dim((char *) data->GasTemperature[0][0] , par_dim, SZ);
    AL_Exchange_dim((char *) data->Vc[PRS][0][0]        , par_dim, SZ);
    AL_Exchange_dim((char *) data->fun[0][0]            , par_dim, SZ);
    
    MPI_Barrier(MPI_COMM_WORLD);
    
}


// ****************
// * Flux-limiter *
// ****************
//
// lambda_fluxlim = ( 1/R) * ((exp(2R) + 1) / (exp(2R) - 1) - (1/R))
// Flux limiter according to Levermore & Pomraning (1981), equation 22.
// Rational approximation of the term above following equation 28:
//
//                      2 + R
// lambda_fluxlim = --------------    (lambda and R at cell interfaces)
//                   6 + 3R + R^2
//
double FluxLimiter(double R){
    
    double lambda;
    
    switch(RadiativeFluxLimiterFlag){
        case 0:
            // Levermore & Pomraning
            lambda = (2.0 + R) / (6.0 + 3.0 * R + R*R);
            break;
            //        case 1:
            //            // TODO: Ensman
            //            lambda = 2.0 / (6.0 + R);
            //            break;
            //        case 2:
            //            // TODO: Minerbo
            //            lambda = ;
            //            break;
        default:
            printf("ERROR: RadiativeFluxLimiterFlag = %d not avialable", RadiativeFluxLimiterFlag);
            exit(1);
    }
    return lambda;
}



double LimitOpticalDepth(Grid *grid, int kl, int jl, int il, double kapparho){
    
    double dlmin, dlmax;
    double kapparholimited, kapparhomin, kapparhomax;
    
    double dx;
    dx    = GridCellLength(grid, IDIR, kl, jl, il);
    dlmin = dx;
    dlmax = dlmin;
#if DIMENSIONS > 1
    double dy;
    dy    = GridCellLength(grid, JDIR, kl, jl, il);
    dlmin = MIN(dlmin, dy);
    dlmax = MAX(dlmax, dy);
#endif
#if DIMENSIONS > 2
    double dz;
    dz    = GridCellLength(grid, KDIR, kl, jl, il);
    dlmin = MIN(dlmin, dz);
    dlmax = MAX(dlmax, dz);
#endif
    kapparhomin     = MinimumOpticalDepth / dlmin;
    kapparhomax     = MaximumOpticalDepth / dlmax;

    kapparholimited = kapparho;
    if(MinimumOpticalDepth > 0) kapparholimited = MAX(kapparholimited, kapparhomin);
    if(MaximumOpticalDepth > 0) kapparholimited = MIN(kapparholimited, kapparhomax);

    return kapparholimited;
}

