#include "pluto.h"
#include "FLD.h"
#include "PhysicalConstantsCGS.h"
#include "Opacity.h"
#include "DomainDecomposition.h"
#include "interface.h"
#include "Molecules.h"
#include "ReadRestrictionsConfiguration.h"

#include <petsc.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>  // for exit()

//
// Diffusion parameter:
//
int PreconditionerRadiation;
double MinimumOpticalDepth, MaximumOpticalDepth;

//KSPConvergedReason FLD_ConvergedReason = 0;
int FLD_ConvergedReason = 0;
int FLD_Iterations = 0;
double FLD_ResidualNorm = 0.0;

Mat RadiationMatrix;
Vec RadiationRightHandSideVector;
PetscScalar ***RadiationRightHandSide;
Vec GlobalSolutionVector;
PetscScalar ***GlobalSolution;
KSP KSPRadiation;
int    RadiationKSPType;
int    ConvergenceSpecification;
double ConvergenceRTOL;
double ConvergenceABSTOL;
double ConvergenceDTOL;
int    ConvergenceMAXITS;
int    ConvergenceMINITS;

int    KSPSetLagNormFlag;
int    KSPSetDiagonalScaleFlag;


int BoundaryRadiationEnergyMinX, BoundaryRadiationEnergyMaxX, BoundaryRadiationEnergyMinY, BoundaryRadiationEnergyMaxY, BoundaryRadiationEnergyMinZ, BoundaryRadiationEnergyMaxZ;
double BoundaryTemperatureDirichlet;



int RadiationBC(Data *, Grid *);
int FluxLimitedDiffusion(Data *, Grid *);

double FluxLimiter(double);
void FLDParallelCommunication(Data *, Grid *);



int KSPConvergedCustomized(KSP KSPx, PetscInt iteration, PetscReal rnorm, KSPConvergedReason *reason, void *ctx){

    // petsc3.1:
    KSPDefaultConverged(KSPx, iteration, rnorm, reason, ctx);
    // petsc3.5:
    //KSPConvergedDefault(KSPx, iteration, rnorm, reason, ctx);
    
	if(iteration < ConvergenceMINITS) *reason = KSP_CONVERGED_ITERATING;
    
    // PETSc User's Manual:
    //  Output Parameter:
    //   positive - if the iteration has converged
    //   negative - if residual norm exceeds divergence threshold
    //   0        - otherwise
    
    return 0;
}



// ***************************
// ** InitializeRadiation() **
// ***************************
//
int InitializeFLD(Grid *grid){
	
	int I;
    PC PCRadiation;
    //KSPType RadiationKSPType;
	
	//
	// Initialize KSP-Solver:
	//
	DAGetMatrix(DACenter, MATMPIAIJ, &RadiationMatrix);
	DACreateGlobalVector(DACenter, &RadiationRightHandSideVector);
	DAVecGetArray(DACenter, RadiationRightHandSideVector, &RadiationRightHandSide);
	VecDuplicate(RadiationRightHandSideVector, &GlobalSolutionVector);
	DAVecGetArray(DACenter, GlobalSolutionVector, &GlobalSolution);
	KSPCreate(PETSC_COMM_WORLD, &KSPRadiation);
	PCCreate(PETSC_COMM_WORLD, &PCRadiation);
    
	//KSPRICHARDSON, KSPCHEBYCHEV, KSPCG, KSPGMRES, KSPTCQMR, KSPBCGS, KSPCGS, KSPTFQMR, KSPCR, KSPLSQR, KSPBICG, or KSPPREONLY
    switch(RadiationKSPType){
		case -1:
			break;
		case 0:
			KSPSetType(KSPRadiation, KSPPREONLY);
			break;
		case 1:
			KSPSetType(KSPRadiation, KSPRICHARDSON);
			break;
		case 2:
			KSPSetType(KSPRadiation, KSPCHEBYCHEV);
			break;
		case 3:
			KSPSetType(KSPRadiation, KSPCG);
			break;
		case 4:
			KSPSetType(KSPRadiation, KSPGMRES);
			break;
		case 5:
			KSPSetType(KSPRadiation, KSPTCQMR);
			break;
		case 6:
			KSPSetType(KSPRadiation, KSPBCGS);
			break;
		case 7:
			KSPSetType(KSPRadiation, KSPCGS);
			break;
		case 8:
			KSPSetType(KSPRadiation, KSPTFQMR);
			break;
		case 9:
			KSPSetType(KSPRadiation, KSPCR);
			break;
		case 10:
			KSPSetType(KSPRadiation, KSPLSQR);
			break;
		case 11:
			KSPSetType(KSPRadiation, KSPBICG);
            break;
        case 12:
			KSPSetType(KSPRadiation, KSPFGMRES);
			break;
        case 13:
			KSPSetType(KSPRadiation, KSPLGMRES);
			break;
        case 14:
			KSPSetType(KSPRadiation, KSPMINRES);
			break;
        case 15:
			KSPSetType(KSPRadiation, KSPGCR);
			break;
        case 16:
			KSPSetType(KSPRadiation, KSPCR);
			break;
        case 17:
			KSPSetType(KSPRadiation, KSPBCGSL);
			break;
        case 18:
			KSPSetType(KSPRadiation, KSPIBCGS);
			break;
		default:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: RadiationKSPType = %d is not in allowed range              ###\n", RadiationKSPType);
			exit(1);
			break;
	}
	
	
	switch(PreconditionerRadiation){
		case -1:
			break;
		case 0:
			PCSetType(PCRadiation, PCNONE);
			break;
		case 1:
			PCSetType(PCRadiation, PCMAT);
			break;
		case 2:
			PCSetType(PCRadiation, PCJACOBI);
			break;
		case 3:
			PCSetType(PCRadiation, PCBJACOBI);
			break;
		case 4:
			PCSetType(PCRadiation, PCSOR);
			break;
		case 5:
			PCSetType(PCRadiation, PCASM);
			break;
		case 6:
			PCSetType(PCRadiation, PCTFS);
			break;
		case 7:
			PCSetType(PCRadiation, PCEISENSTAT);
			break;
		case 8:
			PCSetType(PCRadiation, PCREDUNDANT);
			break;
		case 9:
			PCSetType(PCRadiation, PCREDISTRIBUTE);
			break;
		case 10:
			PCSetType(PCRadiation, PCHYPRE);
			break;
		case 11:
			PCSetType(PCRadiation, PCML);
			break;
		default:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: PreconditionerRadiation = %d is not in allowed range              ###\n", PreconditionerRadiation);
			exit(1);
			break;
	}
	if(PreconditionerRadiation != -1) KSPSetPC(KSPRadiation, PCRadiation);
	
	/*
	 //Not running:
	 PCSetType(PCRadiation, PCLU);
	 PCSetType(PCRadiation, PCILU);
	 PCSetType(PCRadiation, PCKSP);
	 PCSetType(PCRadiation, PCCHOLESKY);
	 PCSetType(PCRadiation, PCPBJACOBI);
	 PCSetType(PCRadiation, PCMG);
	 PCSetType(PCRadiation, PCICC);
	 PCSetType(PCRadiation, PCNN);
	 PCSetType(PCRadiation, PCGASM);
	 PCSetType(PCRadiation, PCSPAI);
	 PCSetType(PCRadiation, PCCOMPOSITE);
	 PCSetType(PCRadiation, PCSHELL);
	 PCPARMS
	 PCFIELDSPLIT
	 PCPROMETHEUS
	 PCGALERKIN
	 PCEXOTIC
	 PCHMPI
	 PCSUPPORTGRAPH
	 PCASA
	 PCCP
	 PCBFBT
	 PCLSC
	 PCPYTHON
	 PCPFMG
	 PCSYSPFMG
	 PCSACUSP
	 PCSACUSPPOLY
	 PCBICGSTABCUSP
	 PCSVD
	 PCAINVCUSP
	 PCGAMG
	 */
	
    
    KSPSetInitialGuessNonzero(KSPRadiation, PETSC_TRUE);
	
    //
	// Determine the convergence criteria for the approximative implicit solver:
	//
	if(ConvergenceSpecification == 1){
		if(ConvergenceRTOL != PETSC_DEFAULT) ConvergenceRTOL = 4.0 * ConvergenceRTOL;
		if(ConvergenceDTOL != PETSC_DEFAULT) ConvergenceDTOL = 4.0 * ConvergenceDTOL;
	}
	
    
    //
	// Set the convergence/abort conditions for Solver as specified in configuration file:
	//
	// Input Parameters for KSPSetTolerances():
	//	KSPRadiation      - the Krylov subspace context
	//	ConvergenceRTOL   - the relative convergence tolerance (relative decrease in the residual norm)
	//	ConvergenceABSTOL - the absolute convergence tolerance (absolute size of the residual norm)
	//	ConvergenceDTOL   - the relative divergence tolerance
	//	ConvergenceMAXITS - maximum number of iterations to use
	//
	KSPSetTolerances(KSPRadiation, ConvergenceRTOL, ConvergenceABSTOL, ConvergenceDTOL, ConvergenceMAXITS);
    
    //
    // Allow for usage of 'ConvergenceMINITS' (minimum number of iterations to use):
    //
    void *ctx;
    KSPDefaultConvergedCreate(&ctx);
    KSPSetConvergenceTest(KSPRadiation, KSPConvergedCustomized, ctx, KSPDefaultConvergedDestroy);
    
    //
    //
    //
    // TODO: is this diagonal scaling the same as I did already manually?! check!
    KSPSetDiagonalScale(KSPRadiation, KSPSetDiagonalScaleFlag);
    KSPSetLagNorm(KSPRadiation, KSPSetLagNormFlag);
    
    // petsc3.5
    //KSPSetReusePreconditioner(KSPRadiation, PETSC_TRUE);
    //PCSetReusePreconditioner(KSPRadiation, PETSC_TRUE);
    
    // TODO (not important): to get further insights into the internal solver convergence:
    //KSPSetResidualHistory(KSP ksp,PetscReal a[],PetscInt na,PetscBool reset);
    
	//
	// Set Diagonal elements to Unity:
	//
	int start,end;
	MatGetOwnershipRange(RadiationMatrix, &start, &end);
	for(I = start; I < end; I++){
		MatSetValue(RadiationMatrix, I, I, (PetscScalar) 1.0, INSERT_VALUES);
	}
	MatAssemblyBegin(RadiationMatrix, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(  RadiationMatrix, MAT_FINAL_ASSEMBLY);
	
	return 0;
}


// *************************
// ** FinalizeRadiation() **
// *************************
//
// Frees the memory and destroys the objects allocated during InitializeRadiation().
//
int FinalizeFLD(){

	MatDestroy(RadiationMatrix);
	DAVecRestoreArray(DACenter, RadiationRightHandSideVector, &RadiationRightHandSide);
	VecDestroy(RadiationRightHandSideVector);
	DAVecRestoreArray(DACenter, GlobalSolutionVector, &GlobalSolution);
	VecDestroy(GlobalSolutionVector);
	KSPDestroy(KSPRadiation);
	
	return 0;
}



// *******************
// ** RadiationBC() **
// *******************
//
// Sets the specified Boundary Conditions for the Diffusion routine in Matrix and Right-Hand-Side Vector.
//
int RadiationBC(Data *data, Grid *grid){
	
	int kl, jl, il, k,j,i;
	MatStencil row, col[6];
	PetscScalar v[6];
	
	int LastLocalCenterIndexX, FirstLocalCenterIndexX, WidthX;
	int LastLocalCenterIndexY, FirstLocalCenterIndexY, WidthY;
	int LastLocalCenterIndexZ, FirstLocalCenterIndexZ, WidthZ;
	// Returns the global (x,y,z) indices of the lower left corner of the local region, excluding ghost points:
	DAGetCorners(DACenter, &FirstLocalCenterIndexX, &FirstLocalCenterIndexY, &FirstLocalCenterIndexZ, &WidthX, &WidthY, &WidthZ);
	LastLocalCenterIndexX = FirstLocalCenterIndexX + WidthX - 1;
	LastLocalCenterIndexY = FirstLocalCenterIndexY + WidthY - 1;
	LastLocalCenterIndexZ = FirstLocalCenterIndexZ + WidthZ - 1;
	
	// ******************************
	// * Boundary Condition at Xmin *
	// ******************************
	//
	KDOM_LOOP(kl){
		PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
		row.k = k;
		col[0].k = k;
		JDOM_LOOP(jl){
			PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
			row.j = j;
			col[0].j = j;
			IBEG_LOOP(il){
				PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
				
				if(i == 0){
					row.i = i;
					col[0].i = i+1;
					
					switch(BoundaryRadiationEnergyMinX){
						case 0:
							//
							// Periodic: periodic boundaries are handled by the PETSc library per default!
							//
							break;
						case 1:
							//
							// Dirichlet = 0.0 Kelvin:
							//
							RadiationRightHandSide[k][j][i] = 0.0;
							data->RadiationEnergyDensity[kl][jl][il] = RadiationRightHandSide[k][j][i];
							break;
							
						case 2:
							//
							// Dirichlet:
							//
							RadiationRightHandSide[k][j][i] = a_RadiationConstant * pow(BoundaryTemperatureDirichlet / ReferenceTemperature, 4);
							data->RadiationEnergyDensity[kl][jl][il] = RadiationRightHandSide[k][j][i];
							break;
							
						case 3:
							//
							// ZeroGradient:
							//
							v[0] = -1;
							MatSetValuesStencil(RadiationMatrix, 1, &row, 1, &col[0], &v[0], INSERT_VALUES);
							RadiationRightHandSide[k][j][i] = 0;
							data->RadiationEnergyDensity[kl][jl][il] = data->RadiationEnergyDensity[kl][jl][IBEG];
							break;
							
						case 5:
							//
							// ZeroGradient * 0.9:
							//
							v[0] = - 0.9;
							MatSetValuesStencil(RadiationMatrix, 1, &row, 1, &col[0], &v[0], INSERT_VALUES);
							RadiationRightHandSide[k][j][i] = 0;
							data->RadiationEnergyDensity[kl][jl][il] = 0.9*data->RadiationEnergyDensity[kl][jl][IBEG];
							break;
							
						case 6:
							//
							// ZeroGradient of Radiative Flux (in spherical coordinates):
							//
							v[0] = - GridCellArea(grid, IDIR, kl, jl, il+1) / GridCellArea(grid, IDIR, kl, jl, il);
							MatSetValuesStencil(RadiationMatrix, 1, &row, 1, &col[0], &v[0], INSERT_VALUES);
							RadiationRightHandSide[k][j][i] = 0;
							data->RadiationEnergyDensity[kl][jl][il] =
								data->RadiationEnergyDensity[kl][jl][IBEG]
								*
								GridCellArea(grid, IDIR, kl, jl, IBEG) / GridCellArea(grid, IDIR, kl, jl, il);
							break;
							
						case 7:
							//
							// Irradiation from central source:
							//
							//RadiationRightHandSide[k][j][i] =
							//	a_RadiationConstant
							//	*
							//	pow(
							//		TotalTemperature
							//		*
							//		pow(StellarRadius / LocationCenterX[k][j][i], 0.5)
							//	, 4);
							//						RadiationRightHandSide[k][j][i] =
							//							TotalLuminosity
							//							/
							//							(M_PI * pow(LocationCenterX[k][j][i], 2) * c_SpeedOfLight);
							//						RadiationEnergy[k][j][i] = RadiationRightHandSide[k][j][i];
							//						break;
						default:
							PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: BoundaryRadiationEnergyMinX = %d is not in allowed range            ###\n", BoundaryRadiationEnergyMinX);
							exit(1);
							break;
					}
				}
			}
		}
	}
	
	// ******************************
	// * Boundary Condition at Xmax *
	// ******************************
	//
	KDOM_LOOP(kl){
		PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
		row.k = k;
		col[1].k = k;
		JDOM_LOOP(jl){
			PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
			row.j = j;
			col[1].j = j;
			IEND_LOOP(il){
				PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
				if(i == LastLocalCenterIndexX){
					row.i = i;
					col[1].i = i-1;
					
					switch(BoundaryRadiationEnergyMaxX){
						case 0:
							//
							// Periodic: periodic boundaries are handled by the PETSc library per default!
							//
							break;
						case 1:
							//
							// Dirichlet = 0.0 Kelvin:
							//
							RadiationRightHandSide[k][j][i] = 0.0;
							data->RadiationEnergyDensity[kl][jl][il] = RadiationRightHandSide[k][j][i];
							break;
							
						case 2:
							//
							// Dirichlet:
							//
							RadiationRightHandSide[k][j][i] =
							a_RadiationConstant * pow(BoundaryTemperatureDirichlet / ReferenceTemperature, 4);
							data->RadiationEnergyDensity[kl][jl][il] = RadiationRightHandSide[k][j][i];
							break;
						case 3:
							//
							// ZeroGradient:
							//
							v[1] = -1;
							MatSetValuesStencil(RadiationMatrix, 1, &row, 1, &col[1], &v[1], INSERT_VALUES);
							RadiationRightHandSide[k][j][i] = 0;
							data->RadiationEnergyDensity[kl][jl][il] = data->RadiationEnergyDensity[kl][jl][IEND];
							break;
						case 5:
							//
							// ZeroGradient * 0.9:
							//
							v[1] = -0.9;
							MatSetValuesStencil(RadiationMatrix, 1, &row, 1, &col[1], &v[1], INSERT_VALUES);
							RadiationRightHandSide[k][j][i] = 0;
							data->RadiationEnergyDensity[kl][jl][il] = 0.9*data->RadiationEnergyDensity[kl][jl][IEND];
							break;
						case 6:
							//
							// ZeroGradient of Radiative Flux (in spherical coordinates):
							//
							v[1] = - GridCellArea(grid, IDIR, kl, jl, il-1) / GridCellArea(grid, IDIR, kl, jl, il);
							MatSetValuesStencil(RadiationMatrix, 1, &row, 1, &col[1], &v[1], INSERT_VALUES);
							RadiationRightHandSide[k][j][i] = 0;
							data->RadiationEnergyDensity[kl][jl][il] =
								data->RadiationEnergyDensity[kl][jl][IEND]
								*
								GridCellArea(grid, IDIR, kl, jl, IEND) / GridCellArea(grid, IDIR, kl, jl, il);;
							break;
						default:
							PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: BoundaryRadiationEnergyMaxX = %d is not in allowed range            ###\n", BoundaryRadiationEnergyMaxX);
							exit(1);
							break;
					}
				}
			}
		}
	}
	
#if DIMENSIONS > 1
	// ******************************
	// * Boundary Condition at Ymin *
	// ******************************
	//
	KDOM_LOOP(kl){
		PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
		row.k = k;
		col[2].k = k;
		JBEG_LOOP(jl){
			PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
			if(j == 0){
				row.j = j;
				col[2].j = j+1;
				IDOM_LOOP(il){
					PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
					row.i = i;
					col[2].i = i;
					
					switch(BoundaryRadiationEnergyMinY){
						case 0:
							//
							// Periodic: periodic boundaries are handled by the PETSc library per default!
							//
							break;
						case 1:
							//
							// Dirichlet = 0.0 Kelvin:
							//
							RadiationRightHandSide[k][j][i] = 0.0;
							data->RadiationEnergyDensity[kl][jl][il] = RadiationRightHandSide[k][j][i];
							break;
							
						case 2:
							//
							// Dirichlet:
							//
							RadiationRightHandSide[k][j][i] = a_RadiationConstant * pow(BoundaryTemperatureDirichlet / ReferenceTemperature, 4);
							data->RadiationEnergyDensity[kl][jl][il] = RadiationRightHandSide[k][j][i];
							break;
						case 3:
							//
							// ZeroGradient:
							//
							v[2] = -1;
							MatSetValuesStencil(RadiationMatrix, 1, &row, 1, &col[2], &v[2], INSERT_VALUES);
							RadiationRightHandSide[k][j][i] = 0;
							data->RadiationEnergyDensity[kl][jl][il] = data->RadiationEnergyDensity[kl][JBEG][il];
							break;
						case 5:
							//
							// ZeroGradient * 0.9:
							//
							v[2] = -0.9;
							MatSetValuesStencil(RadiationMatrix, 1, &row, 1, &col[2], &v[2], INSERT_VALUES);
							RadiationRightHandSide[k][j][i] = 0;
							data->RadiationEnergyDensity[kl][jl][il] = 0.9*data->RadiationEnergyDensity[kl][JBEG][il];
							break;
						default:
							PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: BoundaryRadiationEnergyMinY = %d is not in allowed range            ###\n", BoundaryRadiationEnergyMinY);
							exit(1);
							break;
					}
				}
			}
		}
	}
	
	// ******************************
	// * Boundary Condition at Ymax *
	// ******************************
	//
	KDOM_LOOP(kl){
		PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
		row.k = k;
		col[3].k = k;
		JEND_LOOP(jl){
			PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
			if(j == LastLocalCenterIndexY){
				row.j = j;
				col[3].j = j-1;
				IDOM_LOOP(il){
					PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
					row.i = i;
					col[3].i = i;
					
					switch(BoundaryRadiationEnergyMaxY){
						case 0:
							//
							// Periodic: periodic boundaries are handled by the PETSc library per default!
							//
							break;
						case 1:
							//
							// Dirichlet = 0.0 Kelvin:
							//
							RadiationRightHandSide[k][j][i] = 0.0;
							data->RadiationEnergyDensity[kl][jl][il] = RadiationRightHandSide[k][j][i];
							break;
							
						case 2:
							//
							// Dirichlet:
							//
							RadiationRightHandSide[k][j][i] = a_RadiationConstant * pow(BoundaryTemperatureDirichlet / ReferenceTemperature, 4);
							data->RadiationEnergyDensity[kl][jl][il] = RadiationRightHandSide[k][j][i];
							break;
						case 3:
							//
							// ZeroGradient:
							//
							v[3] = -1;
							MatSetValuesStencil(RadiationMatrix, 1, &row, 1, &col[3], &v[3], INSERT_VALUES);
							RadiationRightHandSide[k][j][i] = 0;
							data->RadiationEnergyDensity[kl][jl][il] =data->RadiationEnergyDensity[kl][JEND][il];
							break;
						case 5:
							//
							// ZeroGradient * 0.9:
							//
							v[3] = -0.9;
							MatSetValuesStencil(RadiationMatrix, 1, &row, 1, &col[3], &v[3], INSERT_VALUES);
							RadiationRightHandSide[k][j][i] = 0;
							data->RadiationEnergyDensity[kl][jl][il] = 0.9*data->RadiationEnergyDensity[kl][JEND][il];
							break;
						default:
							PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: BoundaryRadiationEnergyMaxY = %d is not in allowed range            ###\n", BoundaryRadiationEnergyMaxY);
							exit(1);
							break;
					}
				}
			}
		}
	}
#endif
#if DIMENSIONS > 2
	// ******************************
	// * Boundary Condition at Zmin *
	// ******************************
	//
	KBEG_LOOP(kl){
		PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
		if(k == 0){
			row.k = k;
			col[4].k = k+1;
			JDOM_LOOP(jl){
				PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
				row.j = j;
				col[4].j = j;
				IDOM_LOOP(il){
					PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
					row.i = i;
					col[4].i = i;
					
					switch(BoundaryRadiationEnergyMinZ){
						case 0:
							//
							// Periodic: periodic boundaries are handled by the PETSc library per default!
							//
							break;
							
						case 1:
							//
							// Dirichlet = 0.0 Kelvin:
							//
							RadiationRightHandSide[k][j][i] = 0.0;
							data->RadiationEnergyDensity[kl][jl][il] = RadiationRightHandSide[k][j][i];
							break;
							
						case 2:
							//
							// Dirichlet:
							//
							RadiationRightHandSide[k][j][i] = a_RadiationConstant * pow(BoundaryTemperatureDirichlet / ReferenceTemperature, 4);
							data->RadiationEnergyDensity[kl][jl][il] = RadiationRightHandSide[k][j][i];
							break;
						case 3:
							//
							// ZeroGradient:
							//
							v[4] = -1;
							MatSetValuesStencil(RadiationMatrix, 1, &row, 1, &col[4], &v[4], INSERT_VALUES);
							RadiationRightHandSide[k][j][i] = 0;
							data->RadiationEnergyDensity[kl][jl][il] = data->RadiationEnergyDensity[KBEG][jl][il];
							break;
						default:
							PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: BoundaryRadiationEnergyMinZ = %d is not in allowed range            ###\n", BoundaryRadiationEnergyMinZ);
							exit(1);
							break;
					}
				}
			}
		}
	}
	
	// ******************************
	// * Boundary Condition at Zmax *
	// ******************************
	//
	KEND_LOOP(kl){
		PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
		if(k == LastLocalCenterIndexZ){
			row.k = k;
			col[5].k = k-1;
			JDOM_LOOP(jl){
				PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
				row.j = j;
				col[5].j = j;
				IDOM_LOOP(il){
					PlutoLocal2PETScGlobal(grid, kl, jl, il, &k, &j, &i);
					row.i = i;
					col[5].i = i;
					
					switch(BoundaryRadiationEnergyMaxZ){
						case 0:
							//
							// Periodic: periodic boundaries are handled by the PETSc library per default!
							//
							break;
							
						case 1:
							//
							// Dirichlet = 0.0 Kelvin:
							//
							RadiationRightHandSide[k][j][i] = 0.0;
							data->RadiationEnergyDensity[kl][jl][il] = RadiationRightHandSide[k][j][i];
							break;
							
						case 2:
							//
							// Dirichlet:
							//
							RadiationRightHandSide[k][j][i] = a_RadiationConstant * pow(BoundaryTemperatureDirichlet / ReferenceTemperature, 4);
							data->RadiationEnergyDensity[kl][jl][il] = RadiationRightHandSide[k][j][i];
							break;
						case 3:
							//
							// ZeroGradient:
							//
							v[5] = -1;
							MatSetValuesStencil(RadiationMatrix, 1, &row, 1, &col[5], &v[5], INSERT_VALUES);
							RadiationRightHandSide[k][j][i] = 0;
							data->RadiationEnergyDensity[kl][jl][il] = data->RadiationEnergyDensity[KEND][jl][il];
							break;
						default:
							PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: BoundaryRadiationEnergyMaxZ = %d is not in allowed range            ###\n", BoundaryRadiationEnergyMaxZ);
							exit(1);
							break;
					}
				}
			}
		}
	}
#endif
	
	MatAssemblyBegin(RadiationMatrix, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(  RadiationMatrix, MAT_FINAL_ASSEMBLY);
	VecAssemblyBegin(RadiationRightHandSideVector);
	VecAssemblyEnd(  RadiationRightHandSideVector);
	
	return 0;
}



// ****************************
// ** FluxLimitedDiffusion() **
// ****************************
//
// Calculates the new distribution of diffusive radiation energy implicitly.
//
int FluxLimitedDiffusion_EquilibriumTemperature(Data *data, Grid *grid){
	
    int    kl, jl, il, kg, jg, ig;
	double dV;
	double dAxl, dAxr;
	double dxl,  dxr;
	double fxml, fxmr;
	double fxll, fxrr;
	
	double rhodust, rhodustxl, rhodustxr;
	double rhogas,  rhogasxl,  rhogasxr;
	double Tdust, Tdustxl, Tdustxr;
	double Tgas,  Tgasxl,  Tgasxr;
	double kappadust, kappadustxl, kappadustxr;
	double kappagas,  kappagasxl,  kappagasxr;
	double kapparho,  kapparhoxl,  kapparhoxr;
	double ER,  ERxl,   ERxr;
	double Rxl,  Rxr;
	double Lxl,  Lxr;
	double Dxl,  Dxr;
    
#if DIMENSIONS > 1
	double dAyl, dAyr;
	double dyl,  dyr;
	double fyml, fymr;
	double fyll, fyrr;
	
	double rhodustyl, rhodustyr;
	double rhogasyl,  rhogasyr;
	double Tdustyl, Tdustyr;
	double Tgasyl,  Tgasyr;
	double kappadustyl, kappadustyr;
	double kappagasyl,  kappagasyr;
	double kapparhoyl,  kapparhoyr;
	double ERyl,   ERyr;
	double Ryl,  Ryr;
	double Lyl,  Lyr;
	double Dyl,  Dyr;
#endif
    
#if DIMENSIONS > 2
	double dAzl, dAzr;
	double dzl,  dzr;
	double fzml, fzmr;
	double fzll, fzrr;
	
	double rhodustzl, rhodustzr;
	double rhogaszl,  rhogaszr;
	double Tdustzl, Tdustzr;
	double Tgaszl,  Tgaszr;
	double kappadustzl, kappadustzr;
	double kappagaszl,  kappagaszr;
	double kapparhozl,  kapparhozr;
	double ERzl,   ERzr;
	double Rzl,  Rzr;
	double Lzl,  Lzr;
	double Dzl,  Dzr;
#endif
    
    double fabbr, x, c_V;
#if IRRADIATION == YES
    double Irad;
#endif
    
    
#if PDVINRT == YES
    double Pgasnew, Pgasold;
#endif
    
	double DiagonalValue, RightHandSideValue;
	MatStencil row, col[6];
	PetscScalar v[6];
	
#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###       ... Diffusion ...                                                  ###\n");
#endif
	
	//
	// Apply Boundary Conditions for RadiationEnergy Field, Matrix and RHS:
	//
	RadiationBC(data, grid);
	
	
    //
    // Compute ER:
    //
    KTOT_LOOP(kl){
        JTOT_LOOP(jl){
            ITOT_LOOP(il){
                rhogas      = data->Vc[RHO][kl][jl][il];
                rhodust     = data->DustDensity[kl][jl][il] * rhogas;
                Tdust       = data->DustTemperature[kl][jl][il];
                Tgas        = data->GasTemperature[kl][jl][il];
                kapparho    = PlanckMeanDustOpacity(Tdust, Tdust, rhodust) * rhodust + PlanckMeanGasOpacity(Tgas, Tgas, rhogas) * rhogas;
                //kapparho    = LimitOpticalDepth(grid, kl, jl, il, kapparho);
                // Remark: Including the limiter here can lead to artifical heating, see e.g. problems in the (parallel) runs of the Pascucci test
                
                ER          = a_RadiationConstant * pow(Tdust, 4);
#if IRRADIATION == YES
                Irad        = data->IrradiationPowerDensity[kl][jl][il];
                ER         -= Irad / c_SpeedOfLight / kapparho;

                if(T_dust_min >= 0) ER = MAX(ER, a_RadiationConstant * pow(T_dust_min, 4));
                else                ER = MAX(ER, 0.0);
#endif
                
                data->RadiationEnergyDensity[kl][jl][il] = ER;
            }
        }
    }
    
    
    // ************************************
	// * Matrix and Right-Hand-Side Setup *
	// ************************************
    //
	// Set Values:
	//
	KDOM_LOOP(kl){
		PlutoLocal2PETScGlobal(grid, kl, jl, il, &kg, &jg, &ig);
		row.k    = kg;
		col[0].k = kg;
		col[1].k = kg;
		col[2].k = kg;
		col[3].k = kg;
		col[4].k = kg+1;
		col[5].k = kg-1;
		JDOM_LOOP(jl){
			PlutoLocal2PETScGlobal(grid, kl, jl, il, &kg, &jg, &ig);
			row.j    = jg;
			col[0].j = jg;
			col[1].j = jg;
			col[2].j = jg+1;
			col[3].j = jg-1;
			col[4].j = jg;
			col[5].j = jg;
			IDOM_LOOP(il){
				PlutoLocal2PETScGlobal(grid, kl, jl, il, &kg, &jg, &ig);
				row.i    = ig;
				col[0].i = ig+1;
				col[1].i = ig-1;
				col[2].i = ig;
				col[3].i = ig;
				col[4].i = ig;
				col[5].i = ig;
				
				dV    = GridCellVolume(grid,       kl,   jl,   il  );
				dAxl  = GridCellArea(  grid, IDIR, kl,   jl,   il  );
				dAxr  = GridCellArea(  grid, IDIR, kl,   jl,   il+1);
				dxl   = 0.5 * (GridCellLength(grid, IDIR, kl,   jl,   il-1) + GridCellLength(grid, IDIR, kl,   jl,   il  ));
				dxr   = 0.5 * (GridCellLength(grid, IDIR, kl,   jl,   il  ) + GridCellLength(grid, IDIR, kl,   jl,   il+1));
				fxml  = 1.0 - 0.5 * GridCellLength(grid, IDIR, kl,   jl,   il  ) / dxl;
				fxmr  = 1.0 - 0.5 * GridCellLength(grid, IDIR, kl,   jl,   il  ) / dxr;
				fxll  = 1.0 - 0.5 * GridCellLength(grid, IDIR, kl,   jl,   il-1) / dxl;
				fxrr  = 1.0 - 0.5 * GridCellLength(grid, IDIR, kl,   jl,   il+1) / dxr;
				
				rhogas      = data->Vc[RHO][kl  ][jl  ][il  ];
				rhogasxl    = data->Vc[RHO][kl  ][jl  ][il-1];
				rhogasxr    = data->Vc[RHO][kl  ][jl  ][il+1];
				rhodust     = data->DustDensity[kl  ][jl  ][il  ] * rhogas;
				rhodustxl   = data->DustDensity[kl  ][jl  ][il-1] * rhogasxl;
				rhodustxr   = data->DustDensity[kl  ][jl  ][il+1] * rhogasxr;
				Tdust       = data->DustTemperature[kl  ][jl  ][il  ];
				Tdustxl     = data->DustTemperature[kl  ][jl  ][il-1];
				Tdustxr     = data->DustTemperature[kl  ][jl  ][il+1];
				Tgas        = data->GasTemperature[kl  ][jl  ][il  ];
				Tgasxl      = data->GasTemperature[kl  ][jl  ][il-1];
				Tgasxr      = data->GasTemperature[kl  ][jl  ][il+1];
				kappadust   = RosselandMeanDustOpacity(Tdust  , rhodust  );
				kappadustxl = RosselandMeanDustOpacity(Tdustxl, rhodustxl);
				kappadustxr = RosselandMeanDustOpacity(Tdustxr, rhodustxr);
				kappagas    = RosselandMeanGasOpacity(Tgas    , rhogas   );
				kappagasxl  = RosselandMeanGasOpacity(Tgasxl  , rhogasxl );
				kappagasxr  = RosselandMeanGasOpacity(Tgasxr  , rhogasxr );
				kapparho    = kappadust   * rhodust   + kappagas   * rhogas  ;
				kapparhoxl  = kappadustxl * rhodustxl + kappagasxl * rhogasxl;
				kapparhoxr  = kappadustxr * rhodustxr + kappagasxr * rhogasxr;
				ER          = data->RadiationEnergyDensity[kl  ][jl  ][il  ];
				ERxl        = data->RadiationEnergyDensity[kl  ][jl  ][il-1];
				ERxr        = data->RadiationEnergyDensity[kl  ][jl  ][il+1];
#if IONIZATION == YES
                x         = data->IonizationFraction[kl][jl][il];
#else
                x         = 0;
#endif
                c_V       = R_UniversalGasConstant / (MolarMass(x) * (gamma_AdiabaticIndex - 1.0));

                // TODO check: Tgas / Tdust still correct in case of radiation+ionization? See derivation in Kuiper et al 2010! in HII regions, gas and dust will be thermally decoupled.
                //fabbr       = 1.0 / (1.0 + c_V * rhogas * Tgas / (4.0 * a_RadiationConstant * pow(Tdust, 4)));
                fabbr       = 1.0 / (1.0 + c_V * rhogas / (4.0 * a_RadiationConstant * pow(Tdust, 3)));
				
#if DIMENSIONS > 1
				dAyl = GridCellArea(  grid, JDIR, kl,   jl,   il  );
				dAyr = GridCellArea(  grid, JDIR, kl,   jl+1, il  );
				dyl  = 0.5 * (GridCellLength(grid, JDIR, kl,   jl-1, il  ) + GridCellLength(grid, JDIR, kl,   jl,   il  ));
				dyr  = 0.5 * (GridCellLength(grid, JDIR, kl,   jl,   il  ) + GridCellLength(grid, JDIR, kl,   jl+1, il  ));
				fyml = 1.0 - 0.5 * GridCellLength(grid, JDIR, kl,   jl  , il  )	/ dyl;
				fymr = 1.0 - 0.5 * GridCellLength(grid, JDIR, kl,   jl  , il  )	/ dyr;
				fyll = 1.0 - 0.5 * GridCellLength(grid, JDIR, kl,   jl-1, il  )	/ dyl;
				fyrr = 1.0 - 0.5 * GridCellLength(grid, JDIR, kl,   jl+1, il  )	/ dyr;
				
				rhogasyl    = data->Vc[RHO][kl  ][jl-1][il  ];
				rhogasyr    = data->Vc[RHO][kl  ][jl+1][il  ];
				rhodustyl   = data->DustDensity[kl  ][jl-1][il  ] * rhogasyl;
				rhodustyr   = data->DustDensity[kl  ][jl+1][il  ] * rhogasyr;
				Tdustyl     = data->DustTemperature[kl  ][jl-1][il  ];
				Tdustyr     = data->DustTemperature[kl  ][jl+1][il  ];
				Tgasyl      = data->GasTemperature[kl  ][jl-1][il  ];
				Tgasyr      = data->GasTemperature[kl  ][jl+1][il  ];
				kappadustyl = RosselandMeanDustOpacity(Tdustyl, rhodustyl);
				kappadustyr = RosselandMeanDustOpacity(Tdustyr, rhodustyr);
				kappagasyl  = RosselandMeanGasOpacity(Tgasyl  , rhogasyl );
				kappagasyr  = RosselandMeanGasOpacity(Tgasyr  , rhogasyr );
				kapparhoyl  = kappadustyl * rhodustyl + kappagasyl * rhogasyl;
				kapparhoyr  = kappadustyr * rhodustyr + kappagasyr * rhogasyr;
				ERyl        = data->RadiationEnergyDensity[kl  ][jl-1][il  ];
				ERyr        = data->RadiationEnergyDensity[kl  ][jl+1][il  ];
#endif
#if DIMENSIONS > 2
				dAzl        = GridCellArea(  grid, KDIR, kl,   jl,   il  );
				dAzr        = GridCellArea(  grid, KDIR, kl+1, jl,   il  );
				dzl         = 0.5 * (GridCellLength(grid, KDIR, kl-1, jl,   il  ) + GridCellLength(grid, KDIR, kl,   jl,   il  ));
				dzr         = 0.5 * (GridCellLength(grid, KDIR, kl,   jl,   il  ) + GridCellLength(grid, KDIR, kl+1, jl,   il  ));
				fzml        = 1.0 - 0.5 * GridCellLength(grid, KDIR, kl,   jl,   il  )	/ dzl;
				fzmr        = 1.0 - 0.5 * GridCellLength(grid, KDIR, kl,   jl,   il  )	/ dzr;
				fzll        = 1.0 - 0.5 * GridCellLength(grid, KDIR, kl-1, jl,   il  )	/ dzl;
				fzrr        = 1.0 - 0.5 * GridCellLength(grid, KDIR, kl+1, jl,   il  ) / dzr;
				
				rhogaszl    = data->Vc[RHO][kl-1][jl  ][il  ];
				rhogaszr    = data->Vc[RHO][kl+1][jl  ][il  ];
				rhodustzl   = data->DustDensity[kl-1][jl  ][il  ] * rhogaszl;
				rhodustzr   = data->DustDensity[kl+1][jl  ][il  ] * rhogaszr;
				Tdustzl     = data->DustTemperature[kl-1][jl  ][il  ];
				Tdustzr     = data->DustTemperature[kl+1][jl  ][il  ];
				Tgaszl      = data->GasTemperature[kl-1][jl  ][il  ];
				Tgaszr      = data->GasTemperature[kl+1][jl  ][il  ];
				kappadustzl = RosselandMeanDustOpacity(Tdustzl, rhodustzl);
				kappadustzr = RosselandMeanDustOpacity(Tdustzr, rhodustzr);
				kappagaszl  = RosselandMeanGasOpacity(Tgaszl  , rhogaszl );
				kappagaszr  = RosselandMeanGasOpacity(Tgaszr  , rhogaszr );
				kapparhozl  = kappadustzl * rhodustzl + kappagaszl * rhogaszl;
				kapparhozr  = kappadustzr * rhodustzr + kappagaszr * rhogaszr;
				ERzl        = data->RadiationEnergyDensity[kl-1][jl  ][il  ];
				ERzr        = data->RadiationEnergyDensity[kl+1][jl  ][il  ];
#endif
				
				//
				// Calculate R (at cell interfaces):
				//
				// Scalar R for lambda_fluxlim(R):
				//
				//                        |grad ER|
				//  R = ----------------------------------------------   (at cell interfaces)
				//       sigma_absorption * B + sigma_scattering * ER
				//
				//      B  = integrated Planck-function = a T_DiffusionionField^4 = ER
				//      sigma_absorption = kappa_rosseland * rho
				//      sigma_scattering = 0
				//
				//
				//  R = abs(grad ER) / (kappa_rosseland * rho * ER):
				//
				Rxl = fabs(ER   - ERxl) / dxl;
				Rxr = fabs(ERxr - ER  ) / dxr;
#if DIMENSIONS > 1
				Ryl = fabs(ER   - ERyl) / dyl;
				Ryr = fabs(ERyr - ER  ) / dyr;
#endif
#if DIMENSIONS > 2
				Rzl = fabs(ER   - ERzl) / dzl;
				Rzr = fabs(ERzr - ER  ) / dzr;
#endif
				Rxl /= fxll * kapparhoxl * ERxl + fxml * kapparho * ER;
				Rxr /= fxrr * kapparhoxr * ERxr + fxmr * kapparho * ER;
#if DIMENSIONS > 1
				Ryl /= fyll * kapparhoyl * ERyl + fyml * kapparho * ER;
				Ryr /= fyrr * kapparhoyr * ERyr + fymr * kapparho * ER;
#endif
#if DIMENSIONS > 2
				Rzl /= fzll * kapparhozl * ERzl + fzml * kapparho * ER;
				Rzr /= fzrr * kapparhozr * ERzr + fzmr * kapparho * ER;
#endif
				
				//
				// Calculate Flux-Limiter (at cell interfaces):
				//
				Lxl = FluxLimiter(Rxl);
				Lxr = FluxLimiter(Rxr);
#if DIMENSIONS > 1
				Lyl = FluxLimiter(Ryl);
				Lyr = FluxLimiter(Ryr);
#endif
#if DIMENSIONS > 2
				Lzl = FluxLimiter(Rzl);
				Lzr = FluxLimiter(Rzr);
#endif
				
				
				// **************************
				// * Diffusion coefficients *
				// **************************
				//
				//                         lambda_fluxlim * c_light
				// DiffusionCoefficient = --------------------------    (at cell interfaces)
				//                           kappa_rosseland * rho
				//
                kapparho    = LimitOpticalDepth(grid, kl  , jl  , il  , kapparho  );
				kapparhoxl  = LimitOpticalDepth(grid, kl  , jl  , il-1, kapparhoxl);
				kapparhoxr  = LimitOpticalDepth(grid, kl  , jl  , il+1, kapparhoxr);

				Dxl         = Lxl * c_SpeedOfLight / (fxll * kapparhoxl + fxml * kapparho);
				Dxr         = Lxr * c_SpeedOfLight / (fxrr * kapparhoxr + fxmr * kapparho);
#if DIMENSIONS > 1
                kapparhoyl  = LimitOpticalDepth(grid, kl  , jl-1, il  , kapparhoyl);
				kapparhoyr  = LimitOpticalDepth(grid, kl  , jl+1, il  , kapparhoyr);

				Dyl         = Lyl * c_SpeedOfLight / (fyll * kapparhoyl + fyml * kapparho);
				Dyr         = Lyr * c_SpeedOfLight / (fyrr * kapparhoyr + fymr * kapparho);
#endif
#if DIMENSIONS > 2
                kapparhozl  = LimitOpticalDepth(grid, kl-1, jl  , il  , kapparhozl);
				kapparhozr  = LimitOpticalDepth(grid, kl+1, jl  , il  , kapparhozr);

				Dzl         = Lzl * c_SpeedOfLight / (fzll * kapparhozl + fzml * kapparho);
				Dzr         = Lzr * c_SpeedOfLight / (fzrr * kapparhozr + fzmr * kapparho);
#endif
				
				//
				// Calculate Diagonal matrix entry (the actual entry is always set to unity):
				//				
				DiagonalValue  = 0;
				
				DiagonalValue += Dxl * dAxl / dxl + Dxr * dAxr / dxr;
#if DIMENSIONS > 1
				DiagonalValue += Dyl * dAyl / dyl + Dyr * dAyr / dyr;
#endif
#if DIMENSIONS > 2
				DiagonalValue += Dzl * dAzl / dzl + Dzr * dAzr / dzr;
#endif
				DiagonalValue *= g_dt * fabbr / dV;
				
				DiagonalValue += 1.0;
				
				
				//
				// Set Off-Diagonal Entries:
				//
				v[0] =  - g_dt * fabbr / dV * Dxr * dAxr / dxr / DiagonalValue;
				v[1] =  - g_dt * fabbr / dV * Dxl * dAxl / dxl / DiagonalValue;
#if DIMENSIONS > 1
				v[2] =  - g_dt * fabbr / dV * Dyr * dAyr / dyr / DiagonalValue;
				v[3] =  - g_dt * fabbr / dV * Dyl * dAyl / dyl / DiagonalValue;
#endif
#if DIMENSIONS > 2
				v[4] =  - g_dt * fabbr / dV * Dzr * dAzr / dzr / DiagonalValue;
				v[5] =  - g_dt * fabbr / dV * Dzl * dAzl / dzl / DiagonalValue;
#endif
				
				MatSetValuesStencil(RadiationMatrix, 1, &row, 2*DIMENSIONS, col, v, INSERT_VALUES);
				
				//
				// Set Right-Hand-Side:
				//
				RightHandSideValue  = ER;
#if IRRADIATION == YES
                Irad                = data->IrradiationPowerDensity[kl][jl][il];
				RightHandSideValue += g_dt * fabbr * Irad;
#endif
#if PDVINRT == YES
                // TODO: in ionized regions, the dust is thermally decoupled from the gas and, hence, the pdV term should not heat up the dust, only the gas
                Pgasnew             = data->Vc[PRS][kl][jl][il];
                Pgasold             = data->fun[kl][jl][il];
                // TODO: how to handle the compressional heat (causing strong thermal radiative force in the low-density region)
                // check: only not included in evaporated dust-free regions (here, the dust temperature is updated!)
                //if(RHO_min >= 0 && rhogas > 10.0 * RHO_min)
//                if(RHO_min >= 0 && rhogas > 1.1 * RHO_min)
                    RightHandSideValue += fabbr * (Pgasnew - Pgasold) / (g_gamma - 1.0);
#endif
				RightHandSideValue /= DiagonalValue;
                
                //
				// Write value to Right-Hand-Side Vector:
				//
				RadiationRightHandSide[kg][jg][ig] = RightHandSideValue;
				
				
				//
				// Store flux limiter for radiative force during next hydrodynamics step:
				//
                double R;
                R  = pow(0.5 * (Rxl + Rxr), 2);
#if DIMENSIONS > 1
                R += pow(0.5 * (Ryl + Ryr), 2);
#endif
#if DIMENSIONS > 2
                R += pow(0.5 * (Rzl + Rzr), 2);
#endif
                R = sqrt(R);
                
                
                R = fabs(ERxr - ERxl) / (dxr + dxl) / kapparho / ER;
               
                data->RadiationFLuxLimiter[kl][jl][il] = FluxLimiter(R);
                //data->RadiationFLuxLimiter[kl][jl][il] = 0.5 * (Lxl + Lxr);
                // TODO: alternatively, store 3D-lambda at cell interfaces and use upwind interface for radiative force
			}
		}
	}
	
	MatAssemblyBegin(RadiationMatrix, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(  RadiationMatrix, MAT_FINAL_ASSEMBLY);
	VecAssemblyBegin(RadiationRightHandSideVector);
	VecAssemblyEnd(  RadiationRightHandSideVector);
	//MatView(RadiationMatrix, PETSC_VIEWER_STDOUT_WORLD);
	//MatView(RadiationMatrix, PETSC_VIEWER_DRAW_WORLD);
	//VecView(RadiationRightHandSideVector, PETSC_VIEWER_STDOUT_WORLD);
	
	//
	// Assign calculated Radiation Matrix to Krylov-Subspace Solver:
	//
	KSPSetOperators(KSPRadiation, RadiationMatrix, RadiationMatrix, DIFFERENT_NONZERO_PATTERN);
	
	//
	// Solve the linear Equation-System:
	//
	KSPSolve(KSPRadiation, RadiationRightHandSideVector, GlobalSolutionVector);
	//VecView(GlobalSolutionVector, PETSC_VIEWER_STDOUT_WORLD);
	
	//
	// Get Convergence Reason:
	//
	KSPGetConvergedReason(KSPRadiation, &FLD_ConvergedReason);
	KSPGetIterationNumber(KSPRadiation, &FLD_Iterations);
	KSPGetResidualNorm(KSPRadiation, &FLD_ResidualNorm);
	/*
	 KSPDefaultConverged() reaches convergence when
	 rnorm < MAX (rtol * rnorm_0, abstol)
	 */
	
#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... Converged Reason: ");
	switch(FLD_ConvergedReason){
		case KSP_CONVERGED_RTOL:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "RTOL");
			break;
		case KSP_CONVERGED_ATOL:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "ATOL");
			break;
		case KSP_CONVERGED_ITS:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_CONVERGED_ITS");
			break;
		case KSP_CONVERGED_CG_NEG_CURVE:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_CONVERGED_CG_NEG_CURVE");
			break;
		case KSP_CONVERGED_CG_CONSTRAINED:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_CONVERGED_CG_CONSTRAINED");
			break;
		case KSP_CONVERGED_STEP_LENGTH:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_CONVERGED_STEP_LENGTH");
			break;
		case KSP_CONVERGED_HAPPY_BREAKDOWN:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_CONVERGED_HAPPY_BREAKDOWN");
			break;
		case KSP_CONVERGED_ITERATING:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_CONVERGED_ITERATING");
			break;
		case KSP_DIVERGED_DTOL:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_DTOL");
			break;
		case KSP_DIVERGED_ITS:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_ITS");
			break;
		case KSP_DIVERGED_NULL:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_NULL");
			break;
		case KSP_DIVERGED_BREAKDOWN:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_BREAKDOWN");
			break;
		case KSP_DIVERGED_BREAKDOWN_BICG:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_BREAKDOWN_BICG");
			break;
		case KSP_DIVERGED_NONSYMMETRIC:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_NONSYMMETRIC");
			break;
		case KSP_DIVERGED_INDEFINITE_PC:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_INDEFINITE_PC");
			break;
		case KSP_DIVERGED_NAN:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_NAN");
			break;
		case KSP_DIVERGED_INDEFINITE_MAT:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_INDEFINITE_MAT");
			break;
	}
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "                                     ###\n");
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... Used iterations: %4d                                      ###\n", FLD_Iterations);
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... Residual (radiation energy): %.4e                    ###\n", FLD_ResidualNorm);
#endif
	
	if(FLD_ConvergedReason == -9){
#if LOG_OUTPUT != 0
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "ERROR\n");
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "ERROR 'nan' in Matrix or Right-Hand-Side of Flux-Limited Diffusion routine.\n");
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "ERROR\n");
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
#endif
		exit(1);
	}
	else if(FLD_ConvergedReason < 0){
//#if LOG_OUTPUT != 0
		PetscFPrintf(PETSC_COMM_WORLD, stdout, "WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		PetscFPrintf(PETSC_COMM_WORLD, stdout, "WARNING\n");
		PetscFPrintf(PETSC_COMM_WORLD, stdout, "WARNING Approx. implicit Solver for Diffusion Equation wasn't converged.\n");
//		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "WARNING Automatic PC-Switch in use ...\n");
		PetscFPrintf(PETSC_COMM_WORLD, stdout, "WARNING\n");
		PetscFPrintf(PETSC_COMM_WORLD, stdout, "WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
//#endif
		// TODO: in case of non-convergence, switch KSPtype automatically
        
		// TODO: in development ...
		//  - avoid infinite loop, if all PCs doesn't converge
		//
		// Switch Pre-Conditioner and re-compute the problem:
		//
		//		PC PCRadiation;
		//		PCCreate(PETSC_COMM_WORLD, &PCRadiation);
		//		switch(PreconditionerRadiation){
		//			case -1:
		//			case 3:
		//				PCSetType(PCRadiation, PCHYPRE);
		//				PreconditionerRadiation = 10;
		//				break;
		//			case 10:
		//				PCSetType(PCRadiation, PCBJACOBI);
		//				PreconditionerRadiation = 3;
		//				break;
		//			default:
		//				PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: PreconditionerRadiation = %d is not in allowed range              ###\n", PreconditionerRadiation);
		//				exit(1);
		//				break;
		//		}
		//		KSPSetPC(KSPRadiation, PCRadiation);
		//
		//		FluxLimitedDiffusion(grid);
		exit(1);
	}
	
	// ***************************
	// * Update Radiation Energy *
	// ***************************
	//
	//DAGlobalToLocalBegin(DACenter, GlobalSolutionVector, INSERT_VALUES, RadiationEnergyVector);
	//DAGlobalToLocalEnd  (DACenter, GlobalSolutionVector, INSERT_VALUES, RadiationEnergyVector);
	KDOM_LOOP(kl){
		JDOM_LOOP(jl){
			IDOM_LOOP(il){
                
				PlutoLocal2PETScGlobal(grid, kl, jl, il, &kg, &jg, &ig);
				data->RadiationEnergyDensity[kl][jl][il] = GlobalSolution[kg][jg][ig];

                if(T_dust_min >= 0) data->RadiationEnergyDensity[kl][jl][il] = MAX(data->RadiationEnergyDensity[kl][jl][il], a_RadiationConstant * pow(T_dust_min, 4));
				if(T_dust_max >= 0) data->RadiationEnergyDensity[kl][jl][il] = MIN(data->RadiationEnergyDensity[kl][jl][il], a_RadiationConstant * pow(T_dust_max, 4));
                
			}
		}
	}
	
	
    
    //
    // Zero-Gradient Boundary conditions for the x- and y-direction (only used for computing radiative force in outer boundaries):
    //
	KTOT_LOOP(kl){
		JTOT_LOOP(jl){
			IBEG_LOOP(il){
				data->RadiationFLuxLimiter[kl][jl][il]   = data->RadiationFLuxLimiter[kl][jl][IBEG];
			}
		}
	}
	KTOT_LOOP(kl){
		JTOT_LOOP(jl){
			IEND_LOOP(il){
				data->RadiationFLuxLimiter[kl][jl][il]   = data->RadiationFLuxLimiter[kl][jl][IEND];
			}
		}
	}
#if DIMENSIONS > 1
	KTOT_LOOP(kl){
		JBEG_LOOP(jl){
			ITOT_LOOP(il){
				data->RadiationFLuxLimiter[kl][jl][il]   = data->RadiationFLuxLimiter[kl][JBEG][il];
			}
		}
	}
	KTOT_LOOP(kl){
		JEND_LOOP(jl){
			ITOT_LOOP(il){
				data->RadiationFLuxLimiter[kl][jl][il]   = data->RadiationFLuxLimiter[kl][JEND][il];
			}
		}
	}
#endif
    
    //
    // Parallel communication:
    //
#ifdef PARALLEL
    FLDParallelCommunication(data, grid);
#endif
	
#if DEBUGGING > 0
	KDOM_LOOP(kl){
		JDOM_LOOP(jl){
			IDOM_LOOP(il){
                //				PlutoLocal2PETScGlobal(grid, kl, jl, il, &kg, &jg, &ig);
                //				if(isnan(RadiationEnergy[kg][jg][ig]) != 0){
                //					printf("ERROR in FluxLimitedDiffusion(): nan in RadiationEnergy[%d][%d][%d]\n", kg,jg,ig);
                //					exit(1);
                //				}
                //				if(RadiationEnergy[kg][jg][ig] < 0){
                //					printf("WARNING in FluxLimitedDiffusion(): RadiationEnergy[%d][%d][%d] = %e erg cm^-3 ~ %e K < 0.\n", kg,jg,ig, RadiationEnergy[kg][jg][ig] * ReferenceEnergyDensity, - pow(fabs(RadiationEnergy[kg][jg][ig]) / a_RadiationConstant, 0.25) * ReferenceTemperature);
                //					exit(1);
                //				}
				if(isnan(data->RadiationEnergyDensity[kl][jl][il]) != 0){
					printf("ERROR in FluxLimitedDiffusion(): nan in RadiationEnergyDensity[%d][%d][%d]\n", kl,jl,il);
					exit(1);
				}
				if(data->RadiationEnergyDensity[kl][jl][il] < 0){
					printf("WARNING in FluxLimitedDiffusion(): RadiationEnergyDensity[%d][%d][%d] = %e erg cm^-3 ~ %e K < 0.\n", kl,jl,il, data->RadiationEnergyDensity[kl][jl][il] * ReferenceEnergyDensity, - pow(fabs(data->RadiationEnergyDensity[kl][jl][il]) / a_RadiationConstant, 0.25) * ReferenceTemperature);
					exit(1);
				}
                
			}
		}
	}
#endif
	
	return 0;
}



// ****************************
// ** FluxLimitedDiffusion() **
// ****************************
//
// Calculates the new distribution of diffusive radiation energy implicitly.
//
int FluxLimitedDiffusion(Data *data, Grid *grid){
	
	int    kl, jl, il, kg, jg, ig;
	double dV;
	double dAxl, dAxr;
	double dxl,  dxr;
	double fxml, fxmr;
	double fxll, fxrr;
	
	double rhodust, rhodustxl, rhodustxr;
	double rhogas,  rhogasxl,  rhogasxr;
	double Tdust, Tdustxl, Tdustxr;
	double Tgas,  Tgasxl,  Tgasxr;
	double kappadust, kappadustxl, kappadustxr;
	double kappagas,  kappagasxl,  kappagasxr;
	double kapparho,  kapparhoxl,  kapparhoxr;
    double kapparhoPlanck;
	double ER,  ERxl,   ERxr;
	double Rxl,  Rxr;
	double Lxl,  Lxr;
	double Dxl,  Dxr;

#if DIMENSIONS > 1
	double dAyl, dAyr;
	double dyl,  dyr;
	double fyml, fymr;
	double fyll, fyrr;
	
	double rhodustyl, rhodustyr;
	double rhogasyl,  rhogasyr;
	double Tdustyl, Tdustyr;
	double Tgasyl,  Tgasyr;
	double kappadustyl, kappadustyr;
	double kappagasyl,  kappagasyr;
	double kapparhoyl,  kapparhoyr;
	double ERyl,   ERyr;
	double Ryl,  Ryr;
	double Lyl,  Lyr;
	double Dyl,  Dyr;
#endif
    
#if DIMENSIONS > 2
	double dAzl, dAzr;
	double dzl,  dzr;
	double fzml, fzmr;
	double fzll, fzrr;
	
	double rhodustzl, rhodustzr;
	double rhogaszl,  rhogaszr;
	double Tdustzl, Tdustzr;
	double Tgaszl,  Tgaszr;
	double kappadustzl, kappadustzr;
	double kappagaszl,  kappagaszr;
	double kapparhozl,  kapparhozr;
	double ERzl,   ERzr;
	double Rzl,  Rzr;
	double Lzl,  Lzr;
	double Dzl,  Dzr;
#endif
    double xabbr, yabbr, x, c_V;

    
#if PDVINRT == YES
    double Pgasnew, Pgasold;
#endif
    
	double DiagonalValue, RightHandSideValue;
	MatStencil row, col[6];
	PetscScalar v[6];
	
#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###       ... Diffusion ...                                                  ###\n");
#endif
	
	//
	// Apply Boundary Conditions for RadiationEnergy Field, Matrix and RHS:
	//
	RadiationBC(data, grid);
	
	//
	// Communicate the inner halos for parallel execution:
	//
	//CommunicateHalo(DACenter, RadiationEnergyVector);
//	DALocalToLocalBegin(DACenter, RadiationEnergyVector, INSERT_VALUES, RadiationEnergyVector);
//	DALocalToLocalEnd(  DACenter, RadiationEnergyVector, INSERT_VALUES, RadiationEnergyVector);
	
	
	// ************************************
	// * Matrix and Right-Hand-Side Setup *
	// ************************************
	//
	// Set Values:
	//
	KDOM_LOOP(kl){
		PlutoLocal2PETScGlobal(grid, kl, jl, il, &kg, &jg, &ig);
		row.k    = kg;
		col[0].k = kg;
		col[1].k = kg;
		col[2].k = kg;
		col[3].k = kg;
		col[4].k = kg+1;
		col[5].k = kg-1;
		JDOM_LOOP(jl){
			PlutoLocal2PETScGlobal(grid, kl, jl, il, &kg, &jg, &ig);
			row.j    = jg;
			col[0].j = jg;
			col[1].j = jg;
			col[2].j = jg+1;
			col[3].j = jg-1;
			col[4].j = jg;
			col[5].j = jg;
			IDOM_LOOP(il){
				PlutoLocal2PETScGlobal(grid, kl, jl, il, &kg, &jg, &ig);
				row.i    = ig;
				col[0].i = ig+1;
				col[1].i = ig-1;
				col[2].i = ig;
				col[3].i = ig;
				col[4].i = ig;
				col[5].i = ig;
				
				dV    = GridCellVolume(grid,       kl,   jl,   il  );
				dAxl  = GridCellArea(  grid, IDIR, kl,   jl,   il  );
				dAxr  = GridCellArea(  grid, IDIR, kl,   jl,   il+1);
				dxl   = 0.5 * (GridCellLength(grid, IDIR, kl,   jl,   il-1) + GridCellLength(grid, IDIR, kl,   jl,   il  ));
				dxr   = 0.5 * (GridCellLength(grid, IDIR, kl,   jl,   il  ) + GridCellLength(grid, IDIR, kl,   jl,   il+1));
				fxml  = 1.0 - 0.5 * GridCellLength(grid, IDIR, kl,   jl,   il  ) / dxl;
				fxmr  = 1.0 - 0.5 * GridCellLength(grid, IDIR, kl,   jl,   il  ) / dxr;
				fxll  = 1.0 - 0.5 * GridCellLength(grid, IDIR, kl,   jl,   il-1) / dxl;
				fxrr  = 1.0 - 0.5 * GridCellLength(grid, IDIR, kl,   jl,   il+1) / dxr;
				
				rhogas      = data->Vc[RHO][kl  ][jl  ][il  ];
				rhogasxl    = data->Vc[RHO][kl  ][jl  ][il-1];
				rhogasxr    = data->Vc[RHO][kl  ][jl  ][il+1];
				rhodust     = data->DustDensity[kl  ][jl  ][il  ] * rhogas;
				rhodustxl   = data->DustDensity[kl  ][jl  ][il-1] * rhogasxl;
				rhodustxr   = data->DustDensity[kl  ][jl  ][il+1] * rhogasxr;
				Tdust       = data->DustTemperature[kl  ][jl  ][il  ];
				Tdustxl     = data->DustTemperature[kl  ][jl  ][il-1];
				Tdustxr     = data->DustTemperature[kl  ][jl  ][il+1];
				Tgas        = data->GasTemperature[kl  ][jl  ][il  ];
				Tgasxl      = data->GasTemperature[kl  ][jl  ][il-1];
				Tgasxr      = data->GasTemperature[kl  ][jl  ][il+1];
				kappadust   = RosselandMeanDustOpacity(Tdust  , rhodust  );
				kappadustxl = RosselandMeanDustOpacity(Tdustxl, rhodustxl);
				kappadustxr = RosselandMeanDustOpacity(Tdustxr, rhodustxr);
				kappagas    = RosselandMeanGasOpacity(Tgas    , rhogas   );
				kappagasxl  = RosselandMeanGasOpacity(Tgasxl  , rhogasxl );
				kappagasxr  = RosselandMeanGasOpacity(Tgasxr  , rhogasxr );
				kapparho    = kappadust   * rhodust   + kappagas   * rhogas  ;
				kapparhoxl  = kappadustxl * rhodustxl + kappagasxl * rhogasxl;
				kapparhoxr  = kappadustxr * rhodustxr + kappagasxr * rhogasxr;
				ER          = data->RadiationEnergyDensity[kl  ][jl  ][il  ];
				ERxl        = data->RadiationEnergyDensity[kl  ][jl  ][il-1];
				ERxr        = data->RadiationEnergyDensity[kl  ][jl  ][il+1];
				
#if DIMENSIONS > 1
				dAyl = GridCellArea(  grid, JDIR, kl,   jl,   il  );
				dAyr = GridCellArea(  grid, JDIR, kl,   jl+1, il  );
				dyl  = 0.5 * (GridCellLength(grid, JDIR, kl,   jl-1, il  ) + GridCellLength(grid, JDIR, kl,   jl,   il  ));
				dyr  = 0.5 * (GridCellLength(grid, JDIR, kl,   jl,   il  ) + GridCellLength(grid, JDIR, kl,   jl+1, il  ));
				fyml = 1.0 - 0.5 * GridCellLength(grid, JDIR, kl,   jl  , il  )	/ dyl;
				fymr = 1.0 - 0.5 * GridCellLength(grid, JDIR, kl,   jl  , il  )	/ dyr;
				fyll = 1.0 - 0.5 * GridCellLength(grid, JDIR, kl,   jl-1, il  )	/ dyl;
				fyrr = 1.0 - 0.5 * GridCellLength(grid, JDIR, kl,   jl+1, il  )	/ dyr;
				
				rhogasyl    = data->Vc[RHO][kl  ][jl-1][il  ];
				rhogasyr    = data->Vc[RHO][kl  ][jl+1][il  ];
				rhodustyl   = data->DustDensity[kl  ][jl-1][il  ] * rhogasyl;
				rhodustyr   = data->DustDensity[kl  ][jl+1][il  ] * rhogasyr;
				Tdustyl     = data->DustTemperature[kl  ][jl-1][il  ];
				Tdustyr     = data->DustTemperature[kl  ][jl+1][il  ];
				Tgasyl      = data->GasTemperature[kl  ][jl-1][il  ];
				Tgasyr      = data->GasTemperature[kl  ][jl+1][il  ];
				kappadustyl = RosselandMeanDustOpacity(Tdustyl, rhodustyl);
				kappadustyr = RosselandMeanDustOpacity(Tdustyr, rhodustyr);
				kappagasyl  = RosselandMeanGasOpacity(Tgasyl  , rhogasyl );
				kappagasyr  = RosselandMeanGasOpacity(Tgasyr  , rhogasyr );
				kapparhoyl  = kappadustyl * rhodustyl + kappagasyl * rhogasyl;
				kapparhoyr  = kappadustyr * rhodustyr + kappagasyr * rhogasyr;
				ERyl        = data->RadiationEnergyDensity[kl  ][jl-1][il  ];
				ERyr        = data->RadiationEnergyDensity[kl  ][jl+1][il  ];
#endif
#if DIMENSIONS > 2
				dAzl = GridCellArea(  grid, KDIR, kl,   jl,   il  );
				dAzr = GridCellArea(  grid, KDIR, kl+1, jl,   il  );
				dzl  = 0.5 * (GridCellLength(grid, KDIR, kl-1, jl,   il  ) + GridCellLength(grid, KDIR, kl,   jl,   il  ));
				dzr  = 0.5 * (GridCellLength(grid, KDIR, kl,   jl,   il  ) + GridCellLength(grid, KDIR, kl+1, jl,   il  ));
				fzml = 1.0 - 0.5 * GridCellLength(grid, KDIR, kl,   jl,   il  )	/ dzl;
				fzmr = 1.0 - 0.5 * GridCellLength(grid, KDIR, kl,   jl,   il  )	/ dzr;
				fzll = 1.0 - 0.5 * GridCellLength(grid, KDIR, kl-1, jl,   il  )	/ dzl;
				fzrr = 1.0 - 0.5 * GridCellLength(grid, KDIR, kl+1, jl,   il  ) / dzr;
				
				rhogaszl    = data->Vc[RHO][kl-1][jl  ][il  ];
				rhogaszr    = data->Vc[RHO][kl+1][jl  ][il  ];
				rhodustzl   = data->DustDensity[kl-1][jl  ][il  ] * rhogaszl;
				rhodustzr   = data->DustDensity[kl+1][jl  ][il  ] * rhogaszr;
				Tdustzl     = data->DustTemperature[kl-1][jl  ][il  ];
				Tdustzr     = data->DustTemperature[kl+1][jl  ][il  ];
				Tgaszl      = data->GasTemperature[kl-1][jl  ][il  ];
				Tgaszr      = data->GasTemperature[kl+1][jl  ][il  ];
				kappadustzl = RosselandMeanDustOpacity(Tdustzl, rhodustzl);
				kappadustzr = RosselandMeanDustOpacity(Tdustzr, rhodustzr);
				kappagaszl  = RosselandMeanGasOpacity(Tgaszl  , rhogaszl );
				kappagaszr  = RosselandMeanGasOpacity(Tgaszr  , rhogaszr );
				kapparhozl  = kappadustzl * rhodustzl + kappagaszl * rhogaszl;
				kapparhozr  = kappadustzr * rhodustzr + kappagaszr * rhogaszr;
				ERzl        = data->RadiationEnergyDensity[kl-1][jl  ][il  ];
				ERzr        = data->RadiationEnergyDensity[kl+1][jl  ][il  ];
#endif
				
				//
				// Calculate R (at cell interfaces):
				//
				// Scalar R for lambda_fluxlim(R):
				//
				//                        |grad ER|
				//  R = ----------------------------------------------   (at cell interfaces)
				//       sigma_absorption * B + sigma_scattering * ER
				//
				//      B  = integrated Planck-function = a T_DiffusionionField^4 = ER
				//      sigma_absorption = kappa_rosseland * rho
				//      sigma_scattering = 0
				//
				//
				//  R = abs(grad ER) / (kappa_rosseland * rho * ER):
				//
				Rxl = fabs(ER   - ERxl) / dxl;
				Rxr = fabs(ERxr - ER  ) / dxr;
#if DIMENSIONS > 1
				Ryl = fabs(ER   - ERyl) / dyl;
				Ryr = fabs(ERyr - ER  ) / dyr;
#endif
#if DIMENSIONS > 2
				Rzl = fabs(ER   - ERzl) / dzl;
				Rzr = fabs(ERzr - ER  ) / dzr;
#endif
				Rxl /= fxll * kapparhoxl * ERxl + fxml * kapparho * ER;
				Rxr /= fxrr * kapparhoxr * ERxr + fxmr * kapparho * ER;
#if DIMENSIONS > 1
				Ryl /= fyll * kapparhoyl * ERyl + fyml * kapparho * ER;
				Ryr /= fyrr * kapparhoyr * ERyr + fymr * kapparho * ER;
#endif
#if DIMENSIONS > 2
				Rzl /= fzll * kapparhozl * ERzl + fzml * kapparho * ER;
				Rzr /= fzrr * kapparhozr * ERzr + fzmr * kapparho * ER;
#endif
				
				//
				// Calculate Flux-Limiter (at cell interfaces):
				//
				Lxl = FluxLimiter(Rxl);
				Lxr = FluxLimiter(Rxr);
#if DIMENSIONS > 1
				Lyl = FluxLimiter(Ryl);
				Lyr = FluxLimiter(Ryr);
#endif
#if DIMENSIONS > 2
				Lzl = FluxLimiter(Rzl);
				Lzr = FluxLimiter(Rzr);
#endif
				
				
				// **************************
				// * Diffusion coefficients *
				// **************************
				//
				//                         lambda_fluxlim * c_light
				// DiffusionCoefficient = --------------------------    (at cell interfaces)
				//                           kappa_rosseland * rho
				//
                kapparho    = LimitOpticalDepth(grid, kl  , jl  , il  , kapparho  );
				kapparhoxl  = LimitOpticalDepth(grid, kl  , jl  , il-1, kapparhoxl);
				kapparhoxr  = LimitOpticalDepth(grid, kl  , jl  , il+1, kapparhoxr);

				Dxl         = Lxl * c_SpeedOfLight / (fxll * kapparhoxl + fxml * kapparho);
				Dxr         = Lxr * c_SpeedOfLight / (fxrr * kapparhoxr + fxmr * kapparho);
#if DIMENSIONS > 1
                kapparhoyl  = LimitOpticalDepth(grid, kl  , jl-1, il  , kapparhoyl);
				kapparhoyr  = LimitOpticalDepth(grid, kl  , jl+1, il  , kapparhoyr);

				Dyl         = Lyl * c_SpeedOfLight / (fyll * kapparhoyl + fyml * kapparho);
				Dyr         = Lyr * c_SpeedOfLight / (fyrr * kapparhoyr + fymr * kapparho);
#endif
#if DIMENSIONS > 2
                kapparhozl  = LimitOpticalDepth(grid, kl-1, jl  , il  , kapparhozl);
				kapparhozr  = LimitOpticalDepth(grid, kl+1, jl  , il  , kapparhozr);

				Dzl         = Lzl * c_SpeedOfLight / (fzll * kapparhozl + fzml * kapparho);
				Dzr         = Lzr * c_SpeedOfLight / (fzrr * kapparhozr + fzmr * kapparho);
#endif				

#if IONIZATION == YES
                x         = data->IonizationFraction[kl][jl][il];
#else
                x         = 0;
#endif
                c_V       = R_UniversalGasConstant / (MolarMass(x) * (gamma_AdiabaticIndex - 1.0));

				//
				// Calculate Diagonal matrix entry (the actual entry is always set to unity):
				//
                kapparhoPlanck = PlanckMeanDustOpacity(Tdust, Tdust, rhodust) * rhodust + PlanckMeanGasOpacity(Tgas, Tgas, rhogas) * rhogas;
                kapparhoPlanck = LimitOpticalDepth(grid, kl, jl, il, kapparhoPlanck);
                xabbr          = kapparhoPlanck * c_SpeedOfLight * g_dt;
				yabbr          = rhogas * c_V / pow(Tdust, 3) + 4.0 * a_RadiationConstant * xabbr;
				
				DiagonalValue  = 0;
				
				DiagonalValue += Dxl * dAxl / dxl + Dxr * dAxr / dxr;
#if DIMENSIONS > 1
				DiagonalValue += Dyl * dAyl / dyl + Dyr * dAyr / dyr;
#endif
#if DIMENSIONS > 2
				DiagonalValue += Dzl * dAzl / dzl + Dzr * dAzr / dzr;
#endif				
				DiagonalValue *= g_dt / dV;
				
				DiagonalValue += 1.0 + xabbr - 4.0 * a_RadiationConstant * pow(xabbr, 2) / yabbr;
				
				
				//
				// Set Off-Diagonal Entries:
				//
				v[0] =  - g_dt / dV * Dxr * dAxr / dxr / DiagonalValue;
				v[1] =  - g_dt / dV * Dxl * dAxl / dxl / DiagonalValue;
#if DIMENSIONS > 1
				v[2] =  - g_dt / dV * Dyr * dAyr / dyr / DiagonalValue;
				v[3] =  - g_dt / dV * Dyl * dAyl / dyl / DiagonalValue;
#endif
#if DIMENSIONS > 2
				v[4] =  - g_dt / dV * Dzr * dAzr / dzr / DiagonalValue;
				v[5] =  - g_dt / dV * Dzl * dAzl / dzl / DiagonalValue;
#endif				
				
				MatSetValuesStencil(RadiationMatrix, 1, &row, 2*DIMENSIONS, col, v, INSERT_VALUES);
				
				//
				// Set Right-Hand-Side:
				//
				RightHandSideValue  = 0;
				RightHandSideValue += 3.0 * xabbr * a_RadiationConstant * pow(Tdust, 4);
				RightHandSideValue += rhogas * c_V * Tdust;
#if IRRADIATION == YES
				RightHandSideValue += g_dt * data->IrradiationPowerDensity[kl][jl][il];
#endif
				RightHandSideValue *= 4.0 / yabbr;
				RightHandSideValue -= 3.0 * pow(Tdust, 4);
				RightHandSideValue *= a_RadiationConstant * xabbr;
				RightHandSideValue += ER;
#if PDVINRT == YES
                // TODO: in ionized regions, the dust is thermally decoupled from the gas and, hence, the pdV term should not heat up the dust, only the gas
                Pgasnew = data->Vc[PRS][kl][jl][il];
                Pgasold = data->fun[kl][jl][il];
//                if(RHO_min >= 0 && rhogas > 1.1 * RHO_min)
                    RightHandSideValue += (Pgasnew - Pgasold) / (g_gamma - 1.0);
#endif
				RightHandSideValue /= DiagonalValue;
                

				
				//
				// Write value to Right-Hand-Side Vector:
				//
				RadiationRightHandSide[kg][jg][ig] = RightHandSideValue;
				
				
				//
				// Store flux limiter for radiative force for hydrodynamics:
				//
				double R;
                R  = pow(0.5 * (Rxl + Rxr), 2);
#if DIMENSIONS > 1
                R += pow(0.5 * (Ryl + Ryr), 2);
#endif
#if DIMENSIONS > 2
                R += pow(0.5 * (Rzl + Rzr), 2);
#endif
                R = sqrt(R);
                data->RadiationFLuxLimiter[kl][jl][il] = FluxLimiter(R);
                //data->RadiationFLuxLimiter[kl][jl][il] = 0.5 * (Lxl + Lxr);
                // TODO: alternatively, store 3D-lambda at cell interfaces and use upwind interface for radiative force
			}
		}
	}
	
	MatAssemblyBegin(RadiationMatrix, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(  RadiationMatrix, MAT_FINAL_ASSEMBLY);
	VecAssemblyBegin(RadiationRightHandSideVector);
	VecAssemblyEnd(  RadiationRightHandSideVector);
	//MatView(RadiationMatrix, PETSC_VIEWER_STDOUT_WORLD);
	//MatView(RadiationMatrix, PETSC_VIEWER_DRAW_WORLD);
	//VecView(RadiationRightHandSideVector, PETSC_VIEWER_STDOUT_WORLD);
	
	//
	// Assign calculated Radiation Matrix to Krylov-Subspace Solver:
	//
	KSPSetOperators(KSPRadiation, RadiationMatrix, RadiationMatrix, DIFFERENT_NONZERO_PATTERN);
	
	//
	// Solve the linear Equation-System:
	//
	KSPSolve(KSPRadiation, RadiationRightHandSideVector, GlobalSolutionVector);
	//VecView(GlobalSolutionVector, PETSC_VIEWER_STDOUT_WORLD);
	
	//
	// Get Convergence Reason:
	//
	KSPGetConvergedReason(KSPRadiation, &FLD_ConvergedReason);
	KSPGetIterationNumber(KSPRadiation, &FLD_Iterations);
	KSPGetResidualNorm(KSPRadiation, &FLD_ResidualNorm);
	/*
	 KSPDefaultConverged() reaches convergence when
	 rnorm < MAX (rtol * rnorm_0, abstol)
	 */
	
#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... Converged Reason: ");
	switch(FLD_ConvergedReason){
		case KSP_CONVERGED_RTOL:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "RTOL");
			break;
		case KSP_CONVERGED_ATOL:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "ATOL");
			break;
		case KSP_CONVERGED_ITS:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_CONVERGED_ITS");
			break;
		case KSP_CONVERGED_CG_NEG_CURVE:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_CONVERGED_CG_NEG_CURVE");
			break;
		case KSP_CONVERGED_CG_CONSTRAINED:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_CONVERGED_CG_CONSTRAINED");
			break;
		case KSP_CONVERGED_STEP_LENGTH:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_CONVERGED_STEP_LENGTH");
			break;
		case KSP_CONVERGED_HAPPY_BREAKDOWN:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_CONVERGED_HAPPY_BREAKDOWN");
			break;
		case KSP_CONVERGED_ITERATING:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_CONVERGED_ITERATING");
			break;
		case KSP_DIVERGED_DTOL:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_DTOL");
			break;
		case KSP_DIVERGED_ITS:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_ITS");
			break;
		case KSP_DIVERGED_NULL:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_NULL");
			break;
		case KSP_DIVERGED_BREAKDOWN:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_BREAKDOWN");
			break;
		case KSP_DIVERGED_BREAKDOWN_BICG:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_BREAKDOWN_BICG");
			break;
		case KSP_DIVERGED_NONSYMMETRIC:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_NONSYMMETRIC");
			break;
		case KSP_DIVERGED_INDEFINITE_PC:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_INDEFINITE_PC");
			break;
		case KSP_DIVERGED_NAN:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_NAN");
			break;
		case KSP_DIVERGED_INDEFINITE_MAT:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "KSP_DIVERGED_INDEFINITE_MAT");
			break;
	}
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "                                     ###\n");
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... Used iterations: %4d                                      ###\n", FLD_Iterations);
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... Residual (radiation energy): %.4e                    ###\n", FLD_ResidualNorm);
#endif
	
	if(FLD_ConvergedReason == -9){
#if LOG_OUTPUT != 0
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "ERROR\n");
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "ERROR 'nan' in Matrix or Right-Hand-Side of Flux-Limited Diffusion routine.\n");
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "ERROR\n");
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
#endif
		exit(1);
	}
	else if(FLD_ConvergedReason < 0){
#if LOG_OUTPUT != 0
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "WARNING\n");
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "WARNING Approx. implicit Solver for Diffusion Equation wasn't converged.\n");
//		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "WARNING Automatic PC-Switch in use ...\n");
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "WARNING\n");
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
#endif
		
		// TODO: in development ...
		//  - avoid infinite loop, if all PCs doesn't converge
		//
		// Switch Pre-Conditioner and re-compute the problem:
		//
		//		PC PCRadiation;
		//		PCCreate(PETSC_COMM_WORLD, &PCRadiation);
		//		switch(PreconditionerRadiation){
		//			case -1:
		//			case 3:
		//				PCSetType(PCRadiation, PCHYPRE);
		//				PreconditionerRadiation = 10;
		//				break;
		//			case 10:
		//				PCSetType(PCRadiation, PCBJACOBI);
		//				PreconditionerRadiation = 3;
		//				break;
		//			default:
		//				PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: PreconditionerRadiation = %d is not in allowed range              ###\n", PreconditionerRadiation);
		//				exit(1);
		//				break;
		//		}
		//		KSPSetPC(KSPRadiation, PCRadiation);
		//		
		//		FluxLimitedDiffusion(grid);
		exit(1);
	}
	
	// ***************************
	// * Update Radiation Energy *
	// ***************************
	//
	//DAGlobalToLocalBegin(DACenter, GlobalSolutionVector, INSERT_VALUES, RadiationEnergyVector);
	//DAGlobalToLocalEnd  (DACenter, GlobalSolutionVector, INSERT_VALUES, RadiationEnergyVector);
	KDOM_LOOP(kl){
		JDOM_LOOP(jl){
			IDOM_LOOP(il){
                
				PlutoLocal2PETScGlobal(grid, kl, jl, il, &kg, &jg, &ig);
				data->RadiationEnergyDensity[kl][jl][il] = GlobalSolution[kg][jg][ig];
                
                // TODO: TMINDUST should be negligible here; this is already considered in the Tdust update. In regions with dominating irradiation, the following lines add additional T_dust_min to the total radiation field??
                if(T_dust_min >= 0) data->RadiationEnergyDensity[kl][jl][il] = MAX(data->RadiationEnergyDensity[kl][jl][il], a_RadiationConstant * pow(T_dust_min, 4));
                if(T_dust_max >= 0)	data->RadiationEnergyDensity[kl][jl][il] = MIN(data->RadiationEnergyDensity[kl][jl][il], a_RadiationConstant * pow(T_dust_max, 4));
            }
		}
	}
	
	
    
    //
    // Zero-Gradient Boundary conditions for the x- and y-direction (only used for computing radiative force in outer boundaries):
    //
	KTOT_LOOP(kl){
		JTOT_LOOP(jl){
			IBEG_LOOP(il){
				data->RadiationFLuxLimiter[kl][jl][il]   = data->RadiationFLuxLimiter[kl][jl][IBEG];
			}
		}
	}
	KTOT_LOOP(kl){
		JTOT_LOOP(jl){
			IEND_LOOP(il){
				data->RadiationFLuxLimiter[kl][jl][il]   = data->RadiationFLuxLimiter[kl][jl][IEND];
			}
		}
	}
#if DIMENSIONS > 1
	KTOT_LOOP(kl){
		JBEG_LOOP(jl){
			ITOT_LOOP(il){
				data->RadiationFLuxLimiter[kl][jl][il]   = data->RadiationFLuxLimiter[kl][JBEG][il];
			}
		}
	}
	KTOT_LOOP(kl){
		JEND_LOOP(jl){
			ITOT_LOOP(il){
				data->RadiationFLuxLimiter[kl][jl][il]   = data->RadiationFLuxLimiter[kl][JEND][il];
			}
		}
	}
#endif
    
    //
    // Parallel communication:
    //
#ifdef PARALLEL
    FLDParallelCommunication(data, grid);
#endif
	
#if DEBUGGING > 0
	KDOM_LOOP(kl){
		JDOM_LOOP(jl){
			IDOM_LOOP(il){
//				PlutoLocal2PETScGlobal(grid, kl, jl, il, &kg, &jg, &ig);
//				if(isnan(RadiationEnergy[kg][jg][ig]) != 0){
//					printf("ERROR in FluxLimitedDiffusion(): nan in RadiationEnergy[%d][%d][%d]\n", kg,jg,ig);
//					exit(1);
//				}
//				if(RadiationEnergy[kg][jg][ig] < 0){
//					printf("WARNING in FluxLimitedDiffusion(): RadiationEnergy[%d][%d][%d] = %e erg cm^-3 ~ %e K < 0.\n", kg,jg,ig, RadiationEnergy[kg][jg][ig] * ReferenceEnergyDensity, - pow(fabs(RadiationEnergy[kg][jg][ig]) / a_RadiationConstant, 0.25) * ReferenceTemperature);
//					exit(1);
//				}
				if(isnan(data->RadiationEnergyDensity[kl][jl][il]) != 0){
					printf("ERROR in FluxLimitedDiffusion(): nan in RadiationEnergyDensity[%d][%d][%d]\n", kl,jl,il);
					exit(1);
				}
				if(data->RadiationEnergyDensity[kl][jl][il] < 0){
					printf("WARNING in FluxLimitedDiffusion(): RadiationEnergyDensity[%d][%d][%d] = %e erg cm^-3 ~ %e K < 0.\n", kl,jl,il, data->RadiationEnergyDensity[kl][jl][il] * ReferenceEnergyDensity, - pow(fabs(data->RadiationEnergyDensity[kl][jl][il]) / a_RadiationConstant, 0.25) * ReferenceTemperature);
					exit(1);
				}

			}
		}
	}
#endif
	
	return 0;
}



// ****************
// * Flux-limiter *
// ****************
//
// lambda_fluxlim = ( 1/R) * ((exp(2R) + 1) / (exp(2R) - 1) - (1/R))
// Flux limiter according to Levermore & Pomraning (1981), equation 22.
// Rational approximation of the term above following equation 28:
//
//                      2 + R
// lambda_fluxlim = --------------    (lambda and R at cell interfaces)
//                   6 + 3R + R^2
//
double FluxLimiter(double R){
	return (2.0 + R) / (6.0 + 3.0 * R + R*R);
}



double LimitOpticalDepth(Grid *grid, int kl, int jl, int il, double kapparho){
	
	double dlmin, dlmax;
	double kapparholimited, kapparhomin, kapparhomax;
	
	double dx;
	dx    = GridCellLength(grid, IDIR, kl, jl, il);
	dlmin = dx;
	dlmax = dlmin;
#if DIMENSIONS > 1
    double dy;
	dy    = GridCellLength(grid, JDIR, kl, jl, il);
	dlmin = MIN(dlmin, dy);
	dlmax = MAX(dlmax, dy);
#endif
#if DIMENSIONS > 2
    double dz;
	dz    = GridCellLength(grid, KDIR, kl, jl, il);
	dlmin = MIN(dlmin, dz);
	dlmax = MAX(dlmax, dz);
#endif
	
	kapparhomin     = MinimumOpticalDepth / dlmin;
	kapparhomax     = MaximumOpticalDepth / dlmax;
	
	kapparholimited = kapparho;
	if(MinimumOpticalDepth > 0) kapparholimited = MAX(kapparholimited, kapparhomin);
	if(MaximumOpticalDepth > 0) kapparholimited = MIN(kapparholimited, kapparhomax);
	
//#if DEBUGGING > 0
//    double tau_max;
//    tau_max = kapparho * dlmax;
//    if(tau_max > 1.0){
//        printf("ERROR: tau_local[%d,%d,%d] = %e > 1.0\n", kl,jl,il, tau_max);
//        exit(1);
//    }
//#endif
    
	return kapparholimited;
}



void FLDParallelCommunication(Data *data, Grid *grid){
    
    int par_dim[3] = {0, 0, 0};
    
    //
    // Check the number of processors in each direction:
    //
    D_EXPAND(par_dim[0] = grid[IDIR].nproc > 1;  ,
             par_dim[1] = grid[JDIR].nproc > 1;  ,
             par_dim[2] = grid[KDIR].nproc > 1;)
    
    //
    // Call userdef internal boundary (side == 0):
    //
    //#if INTERNAL_BOUNDARY == YES
    //    UserDefBoundary(data, NULL, 0, grid);
    //#endif
    
    //
    // Exchange data between parallel processors:
    //
    MPI_Barrier(MPI_COMM_WORLD);
    
    AL_Exchange_dim((char *) data->RadiationEnergyDensity[0][0], par_dim, SZ);
    AL_Exchange_dim((char *) data->RadiationFLuxLimiter[0][0], par_dim, SZ);
        
    MPI_Barrier(MPI_COMM_WORLD);
    
}


