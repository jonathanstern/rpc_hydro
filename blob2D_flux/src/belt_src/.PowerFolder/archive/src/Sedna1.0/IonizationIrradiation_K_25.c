#include "pluto.h"
#include "IonizationIrradiation.h"
#include "Irradiation.h"
#include "DirectRecombination.h" // for DirectRecombinationFlag, to check for "on-the-spot" approximation
#include "MaterialProperties.h"
#include "Molecules.h"
#include "Opacity.h"
#include "DustEvolution.h"
#include "PhysicalConstantsCGS.h"
#include "interface.h"
//#include "definitions.h"
//#include "DomainDecomposition.h"
//#include "MakemakeTools.h"

#if STELLAREVOLUTION == YES
#include "StellarEvolution.h"
#endif

#include "petsc.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>  // for exit()

//
// Ionization parameter:
//
int IonizationIrradiationFlag;
double IonizationIrradiationTemperature, IonizationIrradiationRadius;



// ***********************
// ** Internal Routines **
// ***********************
//
// for test reasons currently external
//double IonizationRateEquation(double, double, double, double, double);
double QuadraticEquation(double, double, double);
void IonizationIrradiationParallelCommunication(Data *, Grid *);



// ******************************
// ** Ionization_Irradiation() **
// ******************************
//
int IonizationIrradiation(Data *data, Grid *grid){
	
	int kl, jl, il, i;
//	int proc;
	double Frequency, Flux, x, y, ustar, ustarxl, ustarxr, urec, sigmastar, dtau_ion, tau_ion;
	double Extinction, Attenuation, dtau, tau_rad, dtau_rad;
	double r, dx, dAxl, dAxr, dV;
	double rhogas, Tgas, nH;
    double mpierr;
    
#if IRRADIATION == YES
	//double Luminosity, tau;
	double DustOpacity, GasOpacity, dtau_dust, dtau_gas;
	double rhodust, Tdust, Trad;
#endif
	
	MPI_Status status;
	MPI_Request request;
    
#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... Irradiation ...                                            ###\n");
#endif
	
#if IRRADIATION == YES
	KDOM_LOOP(kl){
		JDOM_LOOP(jl){
			IDOM_LOOP(il){
				data->IrradiationPowerDensity[kl][jl][il] = 0;
			}
		}
	}
#endif
	
	// ********************************
	// * Radiation transport equation *
	// ********************************
	//
	// Even in parallel mode this is only done in serial mode.
	//
    
	// TODO: compute photon mean frequency, especially for a meaningful FrequencyDependentOpacity(Frequency) and NPhotons-to-flux conversion
	Frequency = 13.6 * eV / h_PlanckConstant; // in code units
    
	// TODO: just use two bins: FUV and EUV
    //	for(FrequencyBin = 0; FrequencyBin < NumberOfFrequencies; FrequencyBin++){
    // TODO: check parallelization (with Pascucci Test)
    //for(proc = 0; proc < grid[IDIR].nproc*grid[JDIR].nproc*grid[KDIR].nproc; proc ++){
    //	if(prank == proc){
    
    KDOM_LOOP(kl){
        JDOM_LOOP(jl){
            if(grid[IDIR].beg == IBEG){
                // TODO: the factor 4.0 * M_PI * pow(IonizationIrradiationRadius, 2) and the dependence of NPhotons on IonizationIrradiationRadius can be eliminated by computing directly the NPhotonsPerUnitArea
                
                tau_ion = 0;
                tau_rad = 0;
                
                dAxl = GridCellArea(grid, IDIR, kl, jl, IBEG);
                r    = grid[IDIR].xl[IBEG];
                //					NPhotons  = g_dt * PhotonRateEUV(IonizationIrradiationTemperature, IonizationIrradiationRadius);
                //					NPhotons *= 4.0 * M_PI * pow(IonizationIrradiationRadius, 2) / dAxl;
                
                Flux      = PhotonRateEUV(IonizationIrradiationTemperature, IonizationIrradiationRadius);
                Flux     *= h_PlanckConstant * Frequency;
                Flux     /= 4.0 * M_PI * r*r;
                //printf("PhotonRateEUV = %e s^-1\n", PhotonRateEUV(IonizationIrradiationTemperature, IonizationIrradiationRadius) / ReferenceTime); exit(1);
#ifdef STARBENCH_DTYPE_EARLY
                Flux  = 1e+49; // in s^-1
                Flux *= ReferenceTime; // s^-1 in code units
                Flux *= h_PlanckConstant * Frequency;  // erg s^-1 in code units
                Flux /= 4.0 * M_PI * r*r; // erg s^-1 cm^-2 in code units
#endif
#ifdef STARBENCH_DTYPE_LATE
                Flux  = 1e+49; // in s^-1
                Flux *= ReferenceTime; // s^-1 in code units
                Flux *= h_PlanckConstant * Frequency;  // erg s^-1 in code units
                Flux /= 4.0 * M_PI * r*r; // erg s^-1 cm^-2 in code units
#endif
#ifdef STARBENCH_INSTABILITY
                Flux  = 1e+48; // in s^-1
                Flux *= ReferenceTime; // s^-1 in code units
                Flux *= h_PlanckConstant * Frequency;  // erg s^-1 in code units
                Flux /= 4.0 * M_PI * r*r; // erg s^-1 cm^-2 in code units
#endif
#ifdef STARBENCH_SHADOWS
                if(grid[JDIR].x[jl] < 1.4*Parsec || grid[JDIR].x[jl] > 2.6*Parsec){
                    Flux  = 1e+14; // in s^-1 cm^-2
                    Flux *= ReferenceTime * ReferenceArea; // s^-1 cm^-2 in code units
                    Flux *= h_PlanckConstant * Frequency;  // erg s^-1 cm^-2 in code units
                }
                else{
                    Flux = 0;
                }
#endif
                
            }
            else{
                //
                // Receive NPhotons at inner radial boundary from previous processor:
                // Remark: In PETSC_COMM_WORLD the previous cpu in radial/x - direction is always 'prank-1'.
                //
                mpierr = MPI_Recv(&Flux    , 1, MPI_DOUBLE, prank-1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &status);
                mpierr = MPI_Recv(&tau_ion , 1, MPI_DOUBLE, prank-1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &status);
                mpierr = MPI_Recv(&tau_rad , 1, MPI_DOUBLE, prank-1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &status);
            }
            
            //
            // Calculate (frequency-dependent) (Ionizing) Flux through Grid along Rays in X-Direction:
            //
            IDOM_LOOP(il){
                
                dx         = GridCellLength(grid, IDIR, kl, jl, il  );
                dAxl       = GridCellArea(  grid, IDIR, kl, jl, il  );
                dAxr       = GridCellArea(  grid, IDIR, kl, jl, il+1);
                dV         = GridCellVolume(grid,       kl, jl, il  );
                
                rhogas     = data->Vc[RHO][kl][jl][il];
                nH         = rhogas / AbsoluteMolecularMass(mtot/NH);
                Tgas       = data->GasTemperature[kl][jl][il];
                urec       = data->DirectRecombinationPhotonDensity[kl][jl][il];
                x          = data->IonizationFraction[kl][jl][il];
                y          = data->NeutralFraction[kl][jl][il];
                sigmastar  = IonizationCrossSection(Tgas);
                
                printf("x = %e\n", x);
                
                //
                // Lost energy density due to larger surface of new interface (geometrical decrease):
                //
                Attenuation = dAxl / dAxr;
#if DEBUGGING > 0
                if(Attenuation < 0.66){
                    printf("### WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
                    printf("### WARNING \n");
                    printf("### WARNING  During IonizationIrradiation() step:\n");
                    printf("### WARNING  Attenuation = %f < 0.66\n", Attenuation);
                    printf("### WARNING  You should check your setup!\n");
                    printf("### WARNING  Most probably, higher resolution / less stretching in the x-direction is required.\n");
                    printf("### WARNING \n");
                    printf("### WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
                    exit(1);
                }
#endif
                
                //////////////////////////////////
                // Ionizing radiation transport //
                //////////////////////////////////
                ustarxl = Flux / c_SpeedOfLight / (h_PlanckConstant * Frequency);
                
                
                // TODO: check convergence of Newton-Raphson iterative update (currently set to 100 steps by default)
                // TODO: compute equilibrium timescale and compare to current (hydro/radiative) timestep
                for(i = 0; i < 100; i++){
                    //
                    // Compute flux at radial back face of grid cell:
                    //
                    dtau_ion   = nH * y * sigmastar * dx;
                    
                    Extinction = exp(-dtau_ion);
                    ustarxr    = ustarxl * Attenuation * Extinction;
                    
                    //
                    // Solve rate equations for ionization-recombination balance:
                    //
                    ustar      = 0.5 * (ustarxl + ustarxr); // TODO: improve this approximation by using the proper integral
                    x          = IonizationFractionRateEquation(ustar, urec, Tgas, nH, x);
                    printf("x = %e\n", x);

                    y          = NeutralFractionRateEquation(   ustar, urec, Tgas, nH, y);
                }
                tau_ion += dtau_ion;
                
                //
                // Update stored stellar EUV flux and ionization fraction:
                //
                data->StellarEUVPhotonDensity[kl][jl][il] = ustar;
                data->IonizationFraction[kl][jl][il]      = x;
                data->NeutralFraction[kl][jl][il]         = y;
                
#if DEBUGGING > 0
                double deviation;
                deviation = 1e-7;
#ifdef STARBENCH_DTYPE_EARLY
                deviation = 1e-9;
#endif
#ifdef STARBENCH_DTYPE_LATE
                deviation = 1e-9;
#endif
#ifdef STARBENCH_INSTABILITY
                deviation = 1e-7;
#endif
#ifdef STARBENCH_SHADOWS
                deviation = 1e-8;
#endif
                if(fabs(x+y - 1.0) > deviation){
                    printf("### ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
                    printf("### ERROR \n");
                    printf("### ERROR  During IonizationIrradiation():\n");
                    printf("### ERROR  (k,j,i) = (%d,%d,%d)\n", kl, jl, il);
                    printf("### ERROR  Ionization Fraction + Neutral Fraction != 1\n");
                    printf("### ERROR  x = %e\n", x);
                    if(x == 1.0) printf("### ERROR  x is exactly equal to unity.\n");
                    printf("### ERROR  y = %e\n", y);
                    if(y == 1.0) printf("### ERROR  y is exactly equal to unity.\n");
                    printf("### ERROR  fabs(x+y-1) = %e > %5.0e\n", fabs(x+y - 1.0), deviation);
                    printf("### ERROR   ustar = %e cm^-3\n", ustar * ReferenceNumberDensity);
                    printf("### ERROR   urec  = %e cm^-3\n", urec  * ReferenceNumberDensity);
                    printf("### ERROR   Tgas  = %e K\n",     Tgas  * ReferenceTemperature);
                    printf("### ERROR   nH    = %e cm^-3\n", nH    * ReferenceTemperature);
                    printf("### ERROR    rhogas = %e g cm^-3\n", rhogas * ReferenceDensity);
                    printf("### ERROR    AbsoluteMolecularMass(mtot/NH) = %e\n", AbsoluteMolecularMass(mtot/NH));
                    printf("### ERROR \n");
                    printf("### ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
                    exit(1);
                }
#endif
                
                //////////////////////////////////////
                // Non-ionizing radiation transport //
                //////////////////////////////////////
#if IRRADIATION == YES
                rhodust = data->DustDensity[kl][jl][il];
                Tdust   = data->DustTemperature[kl][jl][il];
                Trad    = IrradiationTemperature;
                
                //
                // Determine opacity:
                //
                switch(IrradiationFlag){
                    case 1:
                        DustOpacity = PlanckMeanDustOpacity(Trad, Tdust, rhodust);
                        break;
                    case 2:
                        DustOpacity = FrequencyDependentOpacity(Frequency);
                        break;
                    default:
                        PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: IrradiationFlag = %d is not in allowed range              ###\n", IrradiationFlag);
                        exit(1);
                }
                GasOpacity = PlanckMeanGasOpacity(Trad, Tgas, rhogas);
                
                //
                // Lost energy due to extinction (absorption + scattering) along cell length:
                //
                dtau_dust  = DustOpacity * rhodust * dx;
                dtau_gas   = GasOpacity  * rhogas  * dx;
                dtau_rad   = dtau_dust + dtau_gas;
                tau_rad   += dtau_rad;
                Extinction = exp(-dtau_rad);
                
                //
                // Store for coupling to thermal radiation transport and hydrodynamics:
                //
                // For highly optically thin grid cells, the exp function below becomes numerically 'unstable' (1-Extinction = 0).
                // See IrradiationPowerDensity.nb
                //
                if(dtau_rad < 1e-5)
                    data->IrradiationPowerDensity[kl][jl][il] += (DustOpacity * rhodust + GasOpacity  * rhogas) * 0.5*Flux*(1.0 + Attenuation); // ~ Flux at cell center (where rho and kappa are defined)
                else
                    data->IrradiationPowerDensity[kl][jl][il] += (1.0 - Extinction) * dAxl/dV * Flux; // absorbed flux along dx
#else
                dtau_rad = 0.0;
#endif
                
                //
                // Calculate Flux at back face of current grid cell:
                //
                dtau        = dtau_ion + dtau_rad;
                Extinction  = exp(-dtau);
                Flux       *= Attenuation * Extinction;
                
            } // for(i)
            //
            // Send Flux at outer radial boundary to next processor:
            // Remark: In PETSC_COMM_WORLD the next cpu in radial/x - direction is always 'prank+1'.
            //
            if(grid[IDIR].end != grid[IDIR].gend){
                mpierr = MPI_Isend(&Flux    , 1, MPI_DOUBLE, prank+1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &request);
                mpierr = MPI_Isend(&tau_ion , 1, MPI_DOUBLE, prank+1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &request);
                mpierr = MPI_Isend(&tau_rad , 1, MPI_DOUBLE, prank+1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &request);
                mpierr = MPI_Wait(&request, &status);
            }
        } // for(j)
    } // for(k)
    
    //	} // if(prank)
    //} // for(proc)
    //	} // for (FrequencyBin)
	
	//printf("tau_ion = %e\n", tau_ion);
    
    
    //
    // Zero-Gradient Boundary conditions for the x- and y-direction:
    //
	KTOT_LOOP(kl){
		JTOT_LOOP(jl){
			IBEG_LOOP(il){
				data->IonizationFraction[kl][jl][il] = data->IonizationFraction[kl][jl][IBEG];
				data->NeutralFraction[kl][jl][il]    = data->NeutralFraction[kl][jl][IBEG];
                // TODO: currently not used anywhere else; no BC update required
                //StellarEUVPhotonDensity
                //StellarFUVPhotonDensity
			}
		}
	}
	KTOT_LOOP(kl){
		JTOT_LOOP(jl){
			IEND_LOOP(il){
				data->IonizationFraction[kl][jl][il] = data->IonizationFraction[kl][jl][IEND];
				data->NeutralFraction[kl][jl][il]    = data->NeutralFraction[kl][jl][IEND];
			}
		}
	}
#if DIMENSIONS > 1
	KTOT_LOOP(kl){
		JBEG_LOOP(jl){
			ITOT_LOOP(il){
				data->IonizationFraction[kl][jl][il] = data->IonizationFraction[kl][JBEG][il];
				data->NeutralFraction[kl][jl][il]    = data->NeutralFraction[kl][JBEG][il];
			}
		}
	}
	KTOT_LOOP(kl){
		JEND_LOOP(jl){
			ITOT_LOOP(il){
				data->IonizationFraction[kl][jl][il] = data->IonizationFraction[kl][JEND][il];
				data->NeutralFraction[kl][jl][il]    = data->NeutralFraction[kl][JEND][il];
			}
		}
	}
#endif
    
    //
    // Parallel communication:
    //
#ifdef PARALLEL
    IonizationIrradiationParallelCommunication(data, grid);
#endif
    
	
	return 0;
}



double IonizationFractionRateEquation(double PhotonDensityStar, double PhotonDensityRecombination, double GasTemperature, double HydrogenNumberDensity, double CurrentIonizationFraction){
	
	double NewIonizationFraction;
	double IonizationRadiative, IonizationCollisions, Recombinations, alpha;
	double A,B,C;
	    
    if(DirectRecombinationFlag == 1)
        alpha = RecombinationRateIntoAnyState(GasTemperature);
    else if(DirectRecombinationFlag == 0){
        // use "on-the-spot" approximation:
        alpha = RecombinationRateIntoAnyButGroundState(GasTemperature);
    }
    else{
        printf("### ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
		printf("### ERROR \n");
		printf("### ERROR  During IonizationFractionRateEquation():\n");
		printf("### ERROR  DirectRecombinationFlag = %d not available\n", DirectRecombinationFlag);
		printf("### ERROR \n");
        printf("### ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
		exit(1);
    }
	
	IonizationRadiative  = c_SpeedOfLight * g_dt * (IonizationCrossSection(GasTemperature) * PhotonDensityStar + DirectRecombinationCrossSection() * PhotonDensityRecombination);
	IonizationCollisions = HydrogenNumberDensity * g_dt * CollisionalIonizationRate(GasTemperature);
	Recombinations       = HydrogenNumberDensity * g_dt * alpha;
	
	A = IonizationCollisions + Recombinations;
	B = 1.0 + IonizationRadiative - IonizationCollisions;
	C = - CurrentIonizationFraction - IonizationRadiative;
    
	NewIonizationFraction = QuadraticEquation(A, B, C);
    
#ifdef IONXMIN
    NewIonizationFraction = MAX(NewIonizationFraction, IONXMIN);
#endif
    
#if DEBUGGING > 0
	if(NewIonizationFraction < 0.0){
		printf("### WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		printf("### WARNING \n");
		printf("### WARNING  During IonizationFractionRateEquation():\n");
		printf("### WARNING  NewIonizationFraction = %f < 0\n", NewIonizationFraction);
		printf("### WARNING \n");
		printf("### WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		exit(1);
	}
    double deviation;
    deviation = 1e-30;
//#ifdef STARBENCH_DTYPE_EARLY
//    deviation = 1e-15;
//#endif
    if(NewIonizationFraction - 1.0 > deviation){
		printf("### WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		printf("### WARNING \n");
		printf("### WARNING  During IonizationFractionRateEquation():\n");
		printf("### WARNING   PhotonDensityStar          = %e cm^-3\n", PhotonDensityStar/ReferenceVolume);
		printf("### WARNING   PhotonDensityRecombination = %e cm^-3\n", PhotonDensityRecombination/ReferenceVolume);
		printf("### WARNING   GasTemperature             = %e K\n", GasTemperature*ReferenceTemperature);
		printf("### WARNING   HydrogenNumberDensity      = %e cm^-3\n", HydrogenNumberDensity*ReferenceNumberDensity);
		printf("### WARNING   CurrentIonizationFraction  = %e\n", CurrentIonizationFraction);
		printf("### WARNING    IonizationRadiative  = %e\n", IonizationRadiative);
		printf("### WARNING    IonizationCollisions = %e\n", IonizationCollisions);
		printf("### WARNING    Recombinations       = %e\n", Recombinations);
		printf("### WARNING     A = %e\n", A);
		printf("### WARNING     B = %e\n", B);
		printf("### WARNING     C = %e\n", C);
		printf("### WARNING  NewIonizationFraction = %e > 1\n", NewIonizationFraction);
		printf("### WARNING  NewIonizationFraction - 1 = %e > %.1e\n", NewIonizationFraction - 1.0, deviation);
		printf("### WARNING \n");
		printf("### WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		exit(1);
	}
#endif
    
    NewIonizationFraction = MIN(NewIonizationFraction, 1.0);
	
	return NewIonizationFraction;
    
}



double NeutralFractionRateEquation(double PhotonDensityStar, double PhotonDensityRecombination, double GasTemperature, double HydrogenNumberDensity, double CurrentNeutralFraction){
	
	double NewNeutralFraction;
	double IonizationRadiative, IonizationCollisions, Recombinations, alpha;
	double A,B,C;
	    
    if(DirectRecombinationFlag == 1)
        alpha = RecombinationRateIntoAnyState(GasTemperature);
    else if(DirectRecombinationFlag == 0){
        // use "on-the-spot" approximation:
        alpha = RecombinationRateIntoAnyButGroundState(GasTemperature);
    }
    else{
        printf("### ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
		printf("### ERROR \n");
		printf("### ERROR  During NeutralFractionRateEquation():\n");
		printf("### ERROR  DirectRecombinationFlag = %d not available\n", DirectRecombinationFlag);
		printf("### ERROR \n");
        printf("### ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
		exit(1);
    }
	
	IonizationRadiative  = c_SpeedOfLight * g_dt * (IonizationCrossSection(GasTemperature) * PhotonDensityStar + DirectRecombinationCrossSection() * PhotonDensityRecombination);
	IonizationCollisions = HydrogenNumberDensity * g_dt * CollisionalIonizationRate(GasTemperature);
	Recombinations       = HydrogenNumberDensity * g_dt * alpha;
	
	A = IonizationCollisions + Recombinations;
	B = 1.0 + IonizationRadiative +  IonizationCollisions + 2.0 * Recombinations;
	C = CurrentNeutralFraction + Recombinations;
    
	NewNeutralFraction = QuadraticEquation(A, -B, C);
    
#ifdef IONXMIN
    // TODO: define NEUYMAX !?
    NewNeutralFraction = MIN(NewNeutralFraction, 1.0-IONXMIN);
#endif
    
#if DEBUGGING > 0
	if(NewNeutralFraction < 0.0){
		printf("### WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		printf("### WARNING \n");
		printf("### WARNING  During NeutralFractionRateEquation():\n");
		printf("### WARNING  NewNeutralFraction = %f < 0\n", NewNeutralFraction);
		printf("### WARNING \n");
		printf("### WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		exit(1);
	}
    
    double deviation;
    deviation = 1e-7;
#ifdef STARBENCH_DTYPE_EARLY
    deviation = 1e-12;
#endif
#ifdef STARBENCH_DTYPE_LATE
    deviation = 1e-13;
#endif
    if(NewNeutralFraction - 1.0 > deviation){
        printf("### WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		printf("### WARNING \n");
		printf("### WARNING  During NeutralFractionRateEquation():\n");
		printf("### WARNING   PhotonDensityStar          = %e cm^-3\n", PhotonDensityStar/ReferenceVolume);
		printf("### WARNING   PhotonDensityRecombination = %e cm^-3\n", PhotonDensityRecombination/ReferenceVolume);
		printf("### WARNING   GasTemperature             = %e K\n", GasTemperature*ReferenceTemperature);
		printf("### WARNING   HydrogenNumberDensity      = %e cm^-3\n", HydrogenNumberDensity*ReferenceNumberDensity);
		printf("### WARNING   CurrentNeutralFraction     = %e\n", CurrentNeutralFraction);
		printf("### WARNING    IonizationRadiative  = %e\n", IonizationRadiative);
		printf("### WARNING    IonizationCollisions = %e\n", IonizationCollisions);
		printf("### WARNING    Recombinations       = %e\n", Recombinations);
		printf("### WARNING     A = %e\n", A);
		printf("### WARNING     B = %e\n", B);
		printf("### WARNING     C = %e\n", C);
		printf("### WARNING  NewNeutralFraction = %e > 1\n", NewNeutralFraction);
		printf("### WARNING  NewNeutralFraction - 1 = %e > %.1e\n", NewNeutralFraction - 1.0, deviation);
		printf("### WARNING \n");
		printf("### WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		exit(1);
	}
#endif
    
    NewNeutralFraction = MIN(NewNeutralFraction, 1.0);
	
	return NewNeutralFraction;
    
}


// *************************
// ** QuadraticEquation() **
// *************************
//
// Solves a quadratic equation of the form A*x^2 + B*x + C = 0 and returns the positive root.
//
double QuadraticEquation(double A, double B, double C){
    
	double p, q, x1, x2;
	
	//
	// Convert equation A*x^2 + B*x + C = 0 into x^2 - p*x + q = 0:
	//
	p = -B/A;
	q = +C/A;
	
	//
	// Solutions: x1,2 = 0.5*p +/- sqrt(0.25 * p^2 - q)
	//
	if(p < 0){
        
		//
		// Negative root:
		//
		x2 = 0.5*p - sqrt(0.25 * p*p - q);
		
		//
		// Positive root:
		//
		//  Remark: Computation method x1 = 0.5*p + sqrt(0.25 * p^2 - q) is numerically unstable for p ~ q (or |x1| ~ |x2|)
        //  Using "Wurzelsatz von Vieta" instead.
		//
		//x1 = 0.5*p + sqrt(0.25 * p*p - q);
		x1 = q / x2;
        
		return x1;
		
	}
	else{
        
		x1 = 0.5*p + sqrt(0.25 * p*p - q);
		x2 = q / x1;
		
		return x2;
		
	}
}



void IonizationIrradiationParallelCommunication(Data *data, Grid *grid){
    
    int par_dim[3] = {0, 0, 0};
    
    //
    // Check the number of processors in each direction:
    //
    D_EXPAND(par_dim[0] = grid[IDIR].nproc > 1;  ,
             par_dim[1] = grid[JDIR].nproc > 1;  ,
             par_dim[2] = grid[KDIR].nproc > 1;)
    
    //
    // Call userdef internal boundary (side == 0):
    //
    //#if INTERNAL_BOUNDARY == YES
    //    UserDefBoundary(data, NULL, 0, grid);
    //#endif
    
    //
    // Exchange data between parallel processors:
    //
    MPI_Barrier(MPI_COMM_WORLD);
    
    AL_Exchange_dim((char *) data->IonizationFraction[0][0], par_dim, SZ);
    AL_Exchange_dim((char *) data->NeutralFraction[0][0], par_dim, SZ);
    AL_Exchange_dim((char *) data->StellarEUVPhotonDensity[0][0], par_dim, SZ);
    AL_Exchange_dim((char *) data->StellarFUVPhotonDensity[0][0], par_dim, SZ);
    
    MPI_Barrier(MPI_COMM_WORLD);
    
}


