/* ///////////////////////////////////////////////////////////////////// */
/*!
 \file
 \brief Convert between primitive and conservative variables.
 
 The PrimToCons() converts an array of primitive quantities to
 an array of conservative variables for the HD equations.
 
 The ConsToPrim() converts an array of conservative quantities to
 an array of primitive quantities.
 During the conversion, pressure is normally recovered from total
 energy unless zone has been tagged with FLAG_ENTROPY.
 In this case we recover pressure from conserved entropy:
 
 if (FLAG_ENTROPY is TRUE)  --> p = p(S)
 else                       --> p = p(E)
 
 \author A. Mignone (mignone@ph.unito.it)
 B. Vaidya
 \date   April 14, 2014
 */
/* ///////////////////////////////////////////////////////////////////// */
#include "pluto.h"
#include "interface.h"
#include "PhysicalConstantsCGS.h"
#include "Molecules.h"



/* ********************************************************************* */
void GetIndices (int i, Grid *grid, int *kl, int *jl, int *il)
/*!
 *  Print the location of a particular zone (i,j,k)
 *  in the computational domain.
 *  \note This function must be initialized before using it
 *        to store grid information. This is done  by calling
 *        Where(i, grid) the very first time.
 *        Subsequent calls can be then done by simply using
 *        Where(i,NULL).
 *
 *********************************************************************** */
{
    int    ii=0;
    double x1;
#if DIMENSIONS > 1
    int    jj=0;
    double x2;
#endif
#if DIMENSIONS > 2
    int    kk=0;
    double x3;
#endif
    
    static Grid *grid1, *grid2, *grid3;
    
    /* --------------------------------------------------
     Keep a local copy of grid for subsequent calls
     -------------------------------------------------- */
    
    if (grid != NULL){
        grid1 = grid + IDIR;
        grid2 = grid + JDIR;
        grid3 = grid + KDIR;
        return;
    }
    
#ifdef CH_SPACEDIM
    if (g_intStage < 0) return; /* HOT FIX used by CHOMBO
                                 (g_intStage = -1) when writing HDF5 file */
#endif
    
    if (g_dir == IDIR){
        D_EXPAND(ii = i;, jj = g_j;, kk = g_k;)
    }else if (g_dir == JDIR){
        D_EXPAND(ii = g_i;, jj = i;, kk = g_k;)
    }else if (g_dir == KDIR){
        D_EXPAND(ii = g_i;, jj = g_j;, kk = i;)
    }
    
    D_EXPAND(
             x1 = grid1->x[ii];  ,
             x2 = grid2->x[jj];  ,
             x3 = grid3->x[kk];
             )
    
    D_SELECT(
             //print ("zone [x1(%d) = %f]",
             //       ii, grid1->x[ii]);
             *il = ii;,
             
             //print ("zone [x1(%d) = %f, x2(%d) = %f]",
             //       ii, grid1->x[ii],
             //       jj, grid2->x[jj]);
             *il = ii;
             *jl = jj;,
             
             //print ("zone [x1(%d) = %f, x2(%d) = %f, x3(%d) = %f]",
             //      ii, grid1->x[ii],
             //      jj, grid2->x[jj],
             //       kk, grid3->x[kk]);
             *il = ii;
             *jl = jj;
             *kl = kk;
             )
    return;
    //#ifdef CH_SPACEDIM
    //    print (", Level = %d, %d\n", LEVEL, grid1->level);
    //    return;
    //#endif
    //#ifdef PARALLEL
    //    print (", proc %d\n", prank);
    //    return;
    //#else
    //    print ("\n");
    //    return;
    //#endif
    
}



/* ********************************************************************* */
void PrimToCons (double **uprim, double **ucons, int ibeg, int iend)
/*!
 * Convert primitive variables to conservative variables.
 *
 * \param [in]  uprim array of primitive variables
 * \param [out] ucons array of conservative variables
 * \param [in]  beg   starting index of computation
 * \param [in]  end   final index of computation
 *
 *********************************************************************** */
{
    int  i, nv, status;
    double *v, *u;
    double rho, rhoe, T, gmm1;
    
#if EOS == IDEAL
    gmm1 = g_gamma - 1.0;
#endif
    for (i = ibeg; i <= iend; i++) {
        
        v = uprim[i];
        u = ucons[i];
        
        u[RHO] = rho = v[RHO];
        EXPAND (u[MX1] = rho*v[VX1];  ,
                u[MX2] = rho*v[VX2];  ,
                u[MX3] = rho*v[VX3];)
        
#if EOS == IDEAL
        u[ENG] = EXPAND(v[VX1]*v[VX1], + v[VX2]*v[VX2], + v[VX3]*v[VX3]);
        u[ENG] = 0.5*rho*u[ENG] + v[PRS]/gmm1;
#elif EOS == PVTE_LAW
        status = GetPV_Temperature(v, &T);
        if (status != 0){
            T      = T_CUT_RHOE;
            v[PRS] = Pressure(v, T);
        }
        rhoe = InternalEnergy(v, T);
        
        u[ENG] = EXPAND(v[VX1]*v[VX1], + v[VX2]*v[VX2], + v[VX3]*v[VX3]);
        u[ENG] = 0.5*rho*u[ENG] + rhoe;
        
        if (u[ENG] != u[ENG]){
            printf("PrimToCons\n");
            printf("KE:%12.6e uRHO : %12.6e, m2 : %12.6e \n",rhoe,v[RHO],u[ENG]);
            QUIT_PLUTO(1);
        }
#endif
        
        for (nv = NFLX; nv < (NFLX + NSCL); nv++) u[nv] = rho*v[nv];
    }
    
}



/* ********************************************************************* */
int ConsToPrim (double **ucons, double **uprim, int ibeg, int iend,
                unsigned char *flag, const Data *data, Grid *grid)
/*!
 * Convert from conservative to primitive variables.
 *
 * \param [in]  ucons  array of conservative variables
 * \param [out] uprim  array of primitive variables
 * \param [in]  beg    starting index of computation
 * \param [in]  end    final index of computation
 * \param [out] flag   array of flags tagging zones where conversion
 *                     went wrong.
 *
 * \return Return (0) if conversion was succesful in every zone
 *         [ibeg,iend].
 *         Otherwise, return a non-zero integer number giving the bit
 *         flag(s) turned on during the conversion process.
 *         In this case, flag contains the failure codes of those
 *         zones where where conversion did not go through.
 *
 *********************************************************************** */
{
    int  i, nv, status=0, use_energy;
    double tau, rho, gmm1, rhoe, T;
    double kin, m2, rhog1;
    double *u, *v;
    double corr_e,prs_floor;
    
    int    kl, jl, il;
    
#if EOS == IDEAL
    gmm1 = g_gamma - 1.0;
#endif
    for (i = ibeg; i <= iend; i++) {
        
        flag[i] = 0;
        u = ucons[i];
        v = uprim[i];
        
        m2  = EXPAND(u[MX1]*u[MX1], + u[MX2]*u[MX2], + u[MX3]*u[MX3]);
        
        /* -------------------------------------------
         Check density positivity
         ------------------------------------------- */
        
        if (u[RHO] < 0.0) {
        //if (u[RHO] < g_smallDensity) {
            //print("! ConsToPrim: rho < 0 (%8.2e), prs_con= %8.2e; rho_pri=%8.2e, prs_pri=%8.2e, ", u[RHO],u[PRS],v[RHO],v[PRS]);
            //Where (i, NULL);
            // constant temperature (p/nV)=kT
            // u[PRS] = MAX( g_smallPressure, g_smallDensity*u[PRS] / u[RHO] ) ;
            // constant internal energy E(intermal) = p / (gamma−1)
            u[PRS] = MAX( g_smallPressure ,u[PRS]);
            // constant total energy : E = p / ( gamma−1)+ 0.5 rho v^2
            //u[PRS] = MAX( g_smallPressure, u[PRS] + 0.5* (g_gamma−1.0)*m2* (1./u[RHO] −1./ g_smallDensity));
            u[RHO] = g_smallDensity;
            //flag[i] |= RHO_FAIL;
            //
            EXPAND(v[VX1] = 0.0;,
               v[VX2] = 0.0;,
               v[VX3] = 0.0;)
            u[ENG] = g_smallPressure /gmm1;
            
            //use maximum of g_smallPressure or mean value of left + right cell (to a void new pressure mimina)
            v[PRS] = MAX( g_smallPressure , 0.5*(uprim[i-1][PRS] + uprim[i+1][PRS]) );
            if ( u[ENG] <= v[PRS]/gmm1) {
                // if due to the new pressure the thermal energy is now larger than the total energy, remove kinetic energy.
                v[PRS] = u[ENG]*gmm1;
                EXPAND(v[VX1] = 0.0;,
                  v[VX2] = 0.0;,
                  v[VX3] = 0.0;)
            }/* else {
                // otherwise reduce kinetic energy by scaling velocities .
                // corr_e = sqrt ( E_kin (new) / E_kin ( old)) = v (new) / v (old)
                corr_e = sqrt( ( u[ENG]-v[PRS]/gmm1) / kin);
                EXPAND(v[VX1] = v[VX1]*corr_e;,
                  v[VX2] = v[VX2]*corr_e;,
                  v[VX3] = v[VX3]*corr_e;)
            }*/
#if ENTROPY_SWITCH == YES
            rhog1 = pow(rho, g_gamma - 1.0);
            u[ENTR]=v[PRS]/rhog1;
#endif
            v[RHO]= rho = u[RHO];
//#if IONIZATION == YES
//            GetIndices(i, NULL, &kl, &jl, &il);
//            if(data->IonizationFraction[kl][jl][il] > 0.5)
//                u[RHO] = 1e-20/ReferenceDensity;
//            else{
//                printf("x = %e\n", data->IonizationFraction[kl][jl][il]);
//                exit(1);
//            }
//#else
          exit(1);
//#endif
        } else { //density > 0
        
          v[RHO] = rho = u[RHO];
          tau = 1.0/u[RHO];
          EXPAND(v[VX1] = u[MX1]*tau;  ,
                 v[VX2] = u[MX2]*tau;  ,
                 v[VX3] = u[MX3]*tau;)
        /* --------------------------------------------
         Recover pressure from energy or entropy:
         1. IDEAL Equation of state
         JONORBE: These checks only make sense in well defined density cells
         -------------------------------------------- */
        
#if EOS == IDEAL
        
        kin = 0.5*m2/u[RHO];
        use_energy = 1;
#if ENTROPY_SWITCH == YES
#ifdef CH_SPACEDIM
        if (g_intStage > 0)   /* -- HOT FIX used with Chombo: avoid calling
                               CheckZone when writing file to disk          -- */
#endif
            if (CheckZone(i,FLAG_ENTROPY)){
                use_energy = 0;
                rhog1 = pow(rho, gmm1);
                v[PRS] = u[ENTR]*rhog1;
                //g_smallPressure = R_UniversalGasConstant / MolarMass(1) * g_smallDensity * 1.E7/ReferenceTemperature;
                if (v[PRS] < 0.0){
                    WARNING(
                            print("! ConsToPrim: p(S) < 0 (%8.2e), u[ENTR]= %8.2e, rho=%8.2e, kin=%8.2e", v[PRS], u[ENTR], rho, kin);
                            Where (i, NULL);
                            )
                    
                    //g_smallPressure = R_UniversalGasConstant / MolarMass(0) * u[RHO] * 1.E7/ReferenceTemperature;
                    //v[PRS]   = g_smallPressure;
                    v[PRS] = MAX( g_smallPressure , 0.5*(uprim[i-1][PRS] + uprim[i+1][PRS]) );
                    flag[i] |= PRS_FAIL;
                    exit(1);
                    
//#if IONIZATION == YES
//                    double x;
//                    GetIndices(i, NULL, &kl, &jl, &il);
//                    x      = data->IonizationFraction[kl][jl][il];
//                    if(x > 0.5)
//                        v[PRS] = R_UniversalGasConstant / MolarMass(x) * u[RHO] * data->GasTemperature[kl][jl][il];
//                    else{
//                        printf("x = %e\n", data->IonizationFraction[kl][jl][il]);
//                        exit(1);
//                    }
//#else
//                    exit(1);
//#endif
                }
                u[ENG] = v[PRS]/gmm1 + kin; /* -- redefine energy -- */
            }
#endif  /* ENTROPY_SWITCH == YES */
        
        if (use_energy){
            if (u[ENG] < 0.0) {
                WARNING(
                        print("! ConsToPrim: E < 0 (%8.2e), ", u[ENG]);
                        Where (i, NULL);
                        )
                u[ENG]   = g_smallPressure/gmm1 + kin;
                flag[i] |= ENG_FAIL;
                
                exit(1);
                
            }else{
                v[PRS] = gmm1*(u[ENG] - kin);
                prs_floor = R_UniversalGasConstant / MolarMass(1) * g_smallDensity * 5.E6/ReferenceTemperature;
                if (v[PRS] < g_smallPressure){
                    //WARNING(
                    //        print("! ConsToPrim: p(E) < 0 (%8.2e) -> %8.2e or %8.2e or %8.2e, v[RHO]=%8.2e u[ENG]=%8.2e kin=%8.2e, ", v[PRS], g_smallPressure, 0.5*(uprim[i-1][PRS] + uprim[i+1][PRS]), prs_floor, v[RHO],u[ENG],kin);
                     //       Where (i, NULL);
                     //       )
                    //v[PRS] = MAX( g_smallPressure , prs_floor);
                    v[PRS]   = g_smallPressure;
                    flag[i] |= PRS_FAIL;
                    u[ENG]   = v[PRS]/gmm1+ kin; /* -- redefine energy -- */
                // otherwise reduce kinetic energy by scaling velocities .
                // corr_e = sqrt ( E_kin (new) / E_kin ( old)) = v (new) / v (old)
                corr_e = sqrt( ( u[ENG]-v[PRS]/gmm1) / kin);
                EXPAND(v[VX1] = v[VX1]*corr_e;,
                  v[VX2] = v[VX2]*corr_e;,
                  v[VX3] = v[VX3]*corr_e;)
                    //exit(1);
                    
                }
            }
        }
        
#endif  /* EOS == IDEAL */
        } //END density>0
        
        /* -- compute scalars  -- */
        
        for (nv = NFLX; nv < NVAR; nv++) v[nv] = u[nv]*tau;
        
        /* --------------------------------------------
         Recover pressure from energy or entropy:
         2. PVTE_LAW Equation of state
         -------------------------------------------- */
        
#if EOS == PVTE_LAW
        if (u[ENG] != u[ENG]){
            print("! ConsToPrim: NaN found\n");
            Show(ucons,i);
            QUIT_PLUTO(1);
        }
        kin  = 0.5*m2/u[RHO];
        rhoe = u[ENG] - kin;
        
        status = GetEV_Temperature (rhoe, v, &T);
        if (status != 0){  /* If something went wrong while retrieving the  */
            /* temperature, we floor \c T to \c T_CUT_RHOE,  */
            /* recompute internal and total energies.        */
            T = T_CUT_RHOE;
            WARNING(  
                    print ("! ConsToPrim: rhoe < 0 or T < T_CUT_RHOE; "); Where(i,NULL);
                    )
            rhoe     = InternalEnergy(v, T);
            u[ENG]   = rhoe + kin; /* -- redefine total energy -- */
            flag[i] |= PRS_FAIL;
            
            exit(1);
            
        }
        
        v[PRS] = Pressure(v, T);
        
#endif  /* EOS == PVTE_LAW */
        
        status |= flag[i];
    }
    return(status);
}
