#if PLUTO_INTERFACE == 0
#include "Timestep.h"
#include "Initialize_global.h"
#elif PLUTO_INTERFACE == 1
#include "pluto.h"
#include "interface.h"
#include "definitions.h"
#include "DomainDecomposition.h"
#if STORE_BOUNDARY_FLUX == YES
#include "boundary_fluxes.h"
#endif
#endif
#include "StellarEvolution.h"
#include "HosokawaOmukai2009.h"
#include "PhysicalConstantsCGS.h"

int StellarEvolutionFlag = 0;
int AccretionLuminosityFlag = 1;
int AccretionRateFlag = 1;

double ConstantAccretionRate;

double StellarMass;
double StellarRadius;
double StellarLuminosity;
double AccretionLuminosity;
double TotalLuminosity;
double StellarTemperature;
double AccretionTemperature;
double TotalTemperature;

double StellarRadiusScalingExponent;
double StellarLuminosityScalingExponent;

double StellarEvolution_dt;
double StellarEvolution_IntegrateTime = 0.0;
double StellarEvolution_IntegrateMass = 0.0;
double StellarEvolution_AccretionRate;


// ***********************
// ** Internal Routines **
// ***********************
//
double ComputeAccretionLuminosity(double, double, double);
double LuminosityAndRadius2Temperature(double, double);


// ************************
// ** StellarEvolution() **
// ************************
//
int StellarEvolution(){
	
#if LOG_OUTPUT != 0
	if(TerminalOutput){
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###       ... Stellar Evolution ...                                          ###\n");
	}
#endif
    
	double AccretionRate;
	double StelcorResults[2];
    
    // *************************************
    // ** Stellar Mass and Accretion rate **
    // *************************************
    //
	switch(AccretionRateFlag){
		case 0:
			//
			// set constant accretion rate:
			//
			AccretionRate = ConstantAccretionRate;
			StellarMass  += AccretionRate * dt_Timestep;
#if STORE_BOUNDARY_FLUX == YES
			//
			// update also M_X1_BEG for gravity routine:
			//
			M_X1_BEG = StellarMass;
#endif
			break;
		case 1:
			//
			// get AccretionRate and StellarMass from Hydrodynamics:
			//
#if STORE_BOUNDARY_FLUX == YES
			AccretionRate  = Flux_X1_BEG_OUTWARD_Global;
#if PROTOSTELLAR_OUTFLOW == YES
			AccretionRate -= Flux_X1_BEG_INWARD_Global;
#endif
			StellarMass   = M_X1_BEG;
#else
			if(AccretionLuminosityFlag == 1){
				printf("ERROR: StellarEvolution() is called with STORE_BOUNDARY_FLUX = NO, AccretionRateFlag = 1 and AccretionLuminosityFlag = 1\n");
				exit(1);
			}
			else
				AccretionRate = 0.0;
			if(StellarEvolutionFlag == 1 || StellarEvolutionFlag == 2){
				printf("ERROR: StellarEvolution() is called with STORE_BOUNDARY_FLUX == NO and StellarEvolutionFlag == %d\n", StellarEvolutionFlag);
				exit(1);
			}
#endif
			break;
		default:
			PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
			PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
			PetscPrintf(PETSC_COMM_WORLD, "ERROR AccretionRateFlag = %d not available, see 'Makemake.conf'.\n", AccretionRateFlag);
			PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
			PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
			exit(1);
			break;
	}
	
//#if PROTOSTELLAR_OUTFLOW == YES
//	AccretionRate = (1.0 - PROTOSTELLAR_OUTFLOW_factor_accretionrate);
//#endif
//	StellarMass  += AccretionRate * dt_Timestep;
//#if STORE_BOUNDARY_FLUX == YES
	//
	// update also M_X1_BEG for gravity routine:
	//
//	M_X1_BEG = StellarMass;
//#endif

    //
   	// Integrate the timestep and the mass accreted:
   	//
   	StellarEvolution_IntegrateTime += dt_Timestep;
   	StellarEvolution_IntegrateMass += AccretionRate * dt_Timestep;
	
#if LOG_OUTPUT != 0
	if(TerminalOutput){
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... StellarMass =                     %6.2f Msol              ###\n", StellarMass* ReferenceMass/SolarMass);
		//PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... StellarEvolution_IntegrateTime = %4.1e yr                ###\n", StellarEvolution_IntegrateTime * ReferenceTime/Year);
		//PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... StellarEvolution_IntegrateMass = %4.1e Msol              ###\n", StellarEvolution_IntegrateMass * ReferenceMass/SolarMass);
	}
#endif
    
   	//
    // Update stellar properties:
    //
    if(StellarEvolution_IntegrateTime > StellarEvolution_dt || NSTEP == 0){
        
    	//
    	// Calculate the accretion rate during StellarEvolution_dt:
    	//
    	StellarEvolution_AccretionRate = StellarEvolution_IntegrateMass / StellarEvolution_IntegrateTime;
#if LOG_OUTPUT != 0
		if(TerminalOutput){
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... StellarEvolution_AccretionRate = %4.1e Msol yr^-1        ###\n", StellarEvolution_AccretionRate * ReferenceMass/ReferenceTime * Year/SolarMass);
		}
#endif
		
    	// ***********************************
    	// ** Stellar Radius and Luminosity **
    	// ***********************************
    	//
    	switch(StellarEvolutionFlag){
            case 0:
                break;
            case 1:
                StellarRadius = StellarRadius_Hosokawa2009(StellarMass, StellarEvolution_AccretionRate);
                StellarLuminosity = StellarLuminosity_Hosokawa2009(StellarMass, StellarEvolution_AccretionRate);
                break;
            case 2:
                StellarRadius = StellarRadius_Hosokawa2009(StellarMass, StellarEvolution_AccretionRate);
                StellarLuminosity = dmax(StellarLuminosity, StellarLuminosity_Hosokawa2009(StellarMass, StellarEvolution_AccretionRate));
                break;
            case 3:
                printf("ERROR: Prost not implemented yet.\n");
                exit(1);
#if PROST == YES
#if LOG_OUTPUT != 0
                if(TerminalOutput) PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... Call 'Prost' subroutine for Stellar Evolution ...          ###\n");
#endif
#endif
                break;
            case 4:
#if STELCOR == YES
				if(LocalRank == 0){
#if LOG_OUTPUT != 0
					if(TerminalOutput) PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... Call 'stelcor' subroutine for Stellar Evolution ...        ###\n");
#endif
					StelcorInterface();
					StelcorResults[0] = StellarLuminosity;
					StelcorResults[1] = StellarRadius;
					MPI_Bcast(StelcorResults, 2, MPI_DOUBLE, 0, PETSC_COMM_WORLD);
				}
				else{
					MPI_Bcast(StelcorResults, 2, MPI_DOUBLE, 0, PETSC_COMM_WORLD);
					StellarLuminosity = StelcorResults[0];
					StellarRadius     = StelcorResults[1];
				}
				//printf("LocalRank = %d: L = %.1e and R = %.2f\n", LocalRank, StellarLuminosity*ReferenceLuminosity/SolarLuminosity, StellarRadius*ReferenceLength/SolarRadius);
#endif
                break;
            case 5:
                StellarRadius =     pow(StellarMass * ReferenceMass / SolarMass, StellarRadiusScalingExponent)     * SolarRadius / ReferenceLength;
                StellarLuminosity = pow(StellarMass * ReferenceMass / SolarMass, StellarLuminosityScalingExponent) * SolarLuminosity / ReferenceLuminosity;
                break;
            default:
                PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
                PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
                PetscPrintf(PETSC_COMM_WORLD, "ERROR StellarEvolutionFlag = %d not available, see 'Makemake.conf'.\n", StellarEvolutionFlag);
                PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
                PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
                exit(1);
                break;
    	}
        
#if LOG_OUTPUT != 0
    	if(TerminalOutput){
   	        PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... StellarRadius =                   %6.2f Rsol              ###\n", StellarRadius * ReferenceLength/SolarRadius);
   	        PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... StellarLuminosity =              %4.1e Lsol              ###\n", StellarLuminosity * ReferenceLuminosity/SolarLuminosity);
    	}
#endif
        
    	//
        // Reset timestep and mass accreted:
        //
        StellarEvolution_IntegrateTime = 0.0;
        StellarEvolution_IntegrateMass = 0.0;
    } // update Stellar properties, if StellarEvolution_IntegrateTime > StellarEvolution_dt
    
    
	// *************************
    // ** Stellar Temperature **
    // *************************
    //
	StellarTemperature = LuminosityAndRadius2Temperature(StellarLuminosity, StellarRadius);
	
    //
    //                G * M_star
    //  L_acc = Mdot ------------
    //                  R_star
    //
    if(AccretionLuminosityFlag != 0){
    	AccretionLuminosity  = ComputeAccretionLuminosity(AccretionRate, StellarMass, StellarRadius);
		AccretionTemperature = LuminosityAndRadius2Temperature(AccretionLuminosity, StellarRadius);
	}
    else{
    	AccretionLuminosity  = 0;
		AccretionTemperature = 0;
	}
    
    
    // ***********************
    // ** TotalLuminosity **
    // ***********************
    //
	// The parameters TotalLuminosity and TotalTemperature are used for the Irradiation subroutine => Feedback to the hydrodynamics.
	//
    TotalLuminosity  = AccretionLuminosity + StellarLuminosity;
	TotalTemperature = LuminosityAndRadius2Temperature(TotalLuminosity, StellarRadius);

    
#if DEBUG == 1
	if(isnan(AccretionRate)){
        printf("ERROR: AccretionRate = 'nan'\n");
        exit(1);
    }
    if(AccretionRate < 0){
    	printf("ERROR: AccretionRate = %e < 0\n", AccretionRate);
    	exit(1);
    }
	if(isnan(StellarEvolution_AccretionRate)){
        printf("ERROR: AccretionRate = 'nan'\n");
        exit(1);
    }
    if(StellarEvolution_AccretionRate < 0){
    	printf("ERROR: AccretionRate = %e < 0\n", StellarEvolution_AccretionRate);
    	exit(1);
    }
	if(isnan(StellarMass)){
        printf("ERROR: StellarMass = 'nan'\n");
        exit(1);
    }
    if(StellarMass < 0){
    	printf("ERROR: StellarMass = %e < 0\n", StellarMass);
    	exit(1);
    }
	if(isnan(StellarRadius)){
        printf("ERROR: StellarRadius = 'nan'\n");
        exit(1);
    }
    if(StellarRadius < 0){
    	printf("ERROR: StellarRadius = %e < 0\n", StellarRadius);
    	exit(1);
    }
    if(isnan(StellarLuminosity)){
    	printf("ERROR: StellarLuminosity = 'nan'\n");
    	exit(1);
    }
    if(StellarLuminosity < 0){
    	printf("ERROR: StellarLuminosity = %e < 0\n", StellarLuminosity);
    	exit(1);
    }
	if(isnan(AccretionLuminosity)){
    	printf("ERROR: AccretionLuminosity = 'nan'\n");
    	exit(1);
    }
    if(AccretionLuminosity < 0){
    	printf("ERROR: AccretionLuminosity = %e < 0\n", AccretionLuminosity);
    	exit(1);
    }
	if(isnan(TotalLuminosity)){
        printf("ERROR: StellarLuminosity = 'nan'\n");
        exit(1);
    }
    if(TotalLuminosity < 0){
        printf("StellarLuminosity = %e\n", StellarLuminosity);
        printf("AccretionLuminosity = %e\n", AccretionLuminosity);
        printf("ERROR: TotalLuminosity = %e < 0\n", TotalLuminosity);
        exit(1);
    }
    if(isnan(StellarTemperature)){
        printf("ERROR: StellarTemperature = 'nan' in StellarEvolution.c\n");
        exit(1);
    }
    if(StellarTemperature < 0){
        printf("ERROR: StellarTemperature = %e < 0 in StellarEvolution.c\n", StellarTemperature);
        exit(1);
    }
	if(isnan(AccretionTemperature)){
        printf("ERROR: AccretionTemperature = 'nan' in StellarEvolution.c\n");
        exit(1);
    }
    if(AccretionTemperature < 0){
        printf("ERROR: AccretionTemperature = %e < 0 in StellarEvolution.c\n", AccretionTemperature);
        exit(1);
    }
	if(isnan(TotalTemperature)){
        printf("ERROR: TotalTemperature = 'nan' in StellarEvolution.c\n");
        exit(1);
    }
    if(TotalTemperature < 0){
        printf("ERROR: TotalTemperature = %e < 0 in StellarEvolution.c\n", TotalTemperature);
        exit(1);
    }
#endif
	
#if LOG_OUTPUT != 0
    if(TerminalOutput){
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... AccretionLuminosity =            %4.1e Lsol              ###\n", AccretionLuminosity * ReferenceLuminosity/SolarLuminosity);
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... TotalLuminosity =                %4.1e Lsol              ###\n", TotalLuminosity * ReferenceLuminosity/SolarLuminosity);
		//PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... StellarTemperature =             %4.1e K                 ###\n", StellarTemperature * ReferenceTemperature);
		//PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... AccretionTemperature =           %4.1e K                 ###\n", AccretionTemperature * ReferenceTemperature);
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... TotalTemperature =               %4.1e K                 ###\n", TotalTemperature * ReferenceTemperature);
    }
#endif
	
    return 0;
}


// **********************************
// ** ComputeAccretionLuminosity() **
// **********************************
//
//               G * M_star
// L_acc = Mdot ------------
//                 R_star
//
double ComputeAccretionLuminosity(double AccretionRate, double StellarMass, double StellarRadius){
    return G_GravityConstant * AccretionRate * StellarMass / StellarRadius;
}


// ***************************************
// ** LuminosityAndRadius2Temperature() **
// ***************************************
//
//             L
// T^4 = ----------------
//        4 pi Sigma R^2
//
double LuminosityAndRadius2Temperature(double Luminosity, double Radius){
    return pow(Luminosity / (4.0 * M_PI * sigma_StefanBoltzmannConstant * pow(Radius, 2)), 0.25);
}

