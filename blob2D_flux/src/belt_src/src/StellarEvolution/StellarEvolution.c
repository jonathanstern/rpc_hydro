#include "pluto.h"
#include "interface.h"
//#include "definitions.h"
#include "DomainDecomposition.h"
#include "boundary_fluxes.h"
#include "PhysicalConstantsCGS.h"
#include "StellarEvolution.h"
//#include "HosokawaOmukai2009.h"
//#include "SuperEddingtonMainSequence.h"
//#include "ProSiT.h"

#if IRRADIATION == YES
#include "Irradiation.h"
#endif
#if IONIZATION == YES
#include "IonizationIrradiation.h"
#endif



//
// External:
//
int    StellarEvolutionFlag;
int    AccretionLuminosityFlag;
int    AccretionRateFlag;
double StellarEvolution_dt;
double ConstantAccretionRate;
double StellarLuminosity = -1;
double StellarRadius;
double StellarRadiusScalingExponent;
double StellarLuminosityScalingExponent;



//
// Internal:
//
double StellarEvolution_IntegrateTime         = 0.0;
double StellarEvolution_IntegrateAccretedMass = 0.0;



// ***********************
// ** Internal Routines **
// ***********************
//
void   UpdateStellarLuminosityAndStellarRadius(double, double);
double ComputeAccretionLuminosity(double, double, double);
double LuminosityAndRadius2Temperature(double, double);



// ************************
// ** StellarEvolution() **
// ************************
//
void StellarEvolution(){

	double StellarMass, StellarAccretionRate, StellarTemperature, MeanStellarAccretionRate, AccretionLuminosity, AccretionTemperature;
#if STELCOR == YES
	double StelcorResults[2];
#endif

#if LOG_OUTPUT != 0
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###       ... Stellar Evolution ...                                          ###\n");
#endif
    
	// *****************************************
	// ** Set Stellar Mass and Accretion rate **
	// *****************************************
	//
	switch(AccretionRateFlag){
		case 0:
			//
			// set constant accretion rate:
			//
			StellarAccretionRate = ConstantAccretionRate;
			// TODO: StellarMass not globally defined anymore
			StellarMass         += StellarAccretionRate * g_dt;
			//
			// update also M_X1_BEG for gravity routine:
			//
			M_X1_BEG            += StellarAccretionRate * g_dt;
			break;
		case 1:
			//
			// get AccretionRate and StellarMass from Hydrodynamics:
			//
#if HYDRODYNAMICS == YES
			StellarMass           = M_X1_BEG;
			StellarAccretionRate  = Flux_X1_BEG_OUTWARD_Global;
#else
			StellarMass           = M_X1_BEG;
			StellarAccretionRate  = 0.0;
#endif
			break;
		default:
			PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
			PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
			PetscPrintf(PETSC_COMM_WORLD, "ERROR AccretionRateFlag = %d not available, see 'Makemake.conf'.\n", AccretionRateFlag);
			PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
			PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
			exit(1);
			break;
	}
    
    //
   	// Integrate the timestep and the mass accreted:
   	//
   	StellarEvolution_IntegrateTime += g_dt;
   	StellarEvolution_IntegrateAccretedMass += StellarAccretionRate * g_dt;
	
#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... StellarMass =                             %6.2f Msol      ###\n", StellarMass / SolarMass);
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... StellarAccretionRate =                   %4.1e Msol/yr   ###\n", StellarAccretionRate * Year/SolarMass);
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... StellarEvolution_IntegrateTime =         %4.1e yr        ###\n", StellarEvolution_IntegrateTime / Year);
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... StellarEvolution_IntegrateAccretedMass = %4.1e Msol      ###\n", StellarEvolution_IntegrateAccretedMass / SolarMass);
#endif
    
   	//
    // Update stellar properties:
	//
	//  StellarLuminosity < 0 => not initialized yet (first main iteration or restart)
    //
	//if(StellarEvolution_IntegrateTime > StellarEvolution_dt || g_stepNumber == 0 || g_stepNumber == cmd_line.nrestart){
	if(StellarEvolution_IntegrateTime > StellarEvolution_dt || StellarLuminosity < 0 || g_stepNumber == 0){
  
    	//
    	// Calculate the accretion rate during StellarEvolution_dt:
    	//
    	MeanStellarAccretionRate = StellarEvolution_IntegrateAccretedMass / StellarEvolution_IntegrateTime;

#if LOG_OUTPUT != 0
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... MeanStellarAccretionRate =               %4.1e Msol/yr   ###\n", MeanStellarAccretionRate * Year/SolarMass);
#endif
		
    	// ***********************************
    	// ** Stellar Radius and Luminosity **
    	// ***********************************
    	//
		UpdateStellarLuminosityAndStellarRadius(StellarMass, MeanStellarAccretionRate);
		                
    	//
        // Reset timestep and mass accreted:
        //
        StellarEvolution_IntegrateTime         = 0.0;
        StellarEvolution_IntegrateAccretedMass = 0.0;
		
    } // update Stellar properties, if StellarEvolution_IntegrateTime > StellarEvolution_dt
    
	
	// *************************
	// ** Stellar Temperature **
	// *************************
	//
	StellarTemperature = LuminosityAndRadius2Temperature(StellarLuminosity, StellarRadius);

#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... StellarRadius =                           %6.2f Rsol      ###\n", StellarRadius / SolarRadius);
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... StellarLuminosity =                      %4.1e Lsol      ###\n", StellarLuminosity / SolarLuminosity);
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... StellarTemperature =                     %4.1e K         ###\n", StellarTemperature * ReferenceTemperature);
#endif
	
    //
    //                G * M_star
    //  L_acc = Mdot ------------
    //                  R_star
    //
    if(AccretionLuminosityFlag != 0){
    	AccretionLuminosity  = ComputeAccretionLuminosity(StellarAccretionRate, StellarMass, StellarRadius);
		AccretionTemperature = LuminosityAndRadius2Temperature(AccretionLuminosity, StellarRadius);
#if LOG_OUTPUT != 0
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... AccretionLuminosity =                    %4.1e Lsol      ###\n", AccretionLuminosity / SolarLuminosity);
		PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... AccretionTemperature =                   %4.1e K         ###\n", AccretionTemperature * ReferenceTemperature);
#endif
	}
    else{
    	AccretionLuminosity  = 0;
		AccretionTemperature = 0;
	}
    
	
    // **************************
    // ** Interface quantities **
    // **************************
    //
#if IRRADIATION == YES
	IrradiationRadius      = StellarRadius;
    IrradiationLuminosity  = AccretionLuminosity + StellarLuminosity;
	IrradiationTemperature = LuminosityAndRadius2Temperature(IrradiationLuminosity, IrradiationRadius);
#endif
	
#if IONIZATION == YES
	IonizationIrradiationTemperature = StellarTemperature;
	IonizationIrradiationRadius      = StellarRadius;
#endif
    
#if DEBUGGING == 1
	//
	// Stellar properties:
	//
    if(isnan(StellarLuminosity)){
    	printf("ERROR: StellarLuminosity = 'nan'\n");
    	exit(1);
    }
    if(StellarLuminosity < 0){
    	printf("ERROR: StellarLuminosity = %e < 0\n", StellarLuminosity);
    	exit(1);
    }
	if(isnan(StellarRadius)){
        printf("ERROR: StellarRadius = 'nan'\n");
        exit(1);
    }
    if(StellarRadius <= 0){
    	printf("ERROR: StellarRadius = %e <= 0\n", StellarRadius);
    	exit(1);
    }
	if(isnan(StellarTemperature)){
        printf("ERROR: StellarTemperature = 'nan' in StellarEvolution.c\n");
        exit(1);
    }
    if(StellarTemperature < 0){
        printf("ERROR: StellarTemperature = %e < 0 in StellarEvolution.c\n", StellarTemperature);
        exit(1);
    }
	if(isnan(StellarAccretionRate)){
        printf("ERROR: StellarAccretionRate = 'nan'\n");
        exit(1);
    }
	
	//
	// Accretion properties:
	//
    if(StellarAccretionRate < 0){
    	printf("ERROR: StellarAccretionRate = %e Msol/yr < 0\n", StellarAccretionRate * Year/SolarMass);
    	exit(1);
    }
	if(isnan(AccretionLuminosity)){
    	printf("ERROR: AccretionLuminosity = 'nan'\n");
    	exit(1);
    }
    if(AccretionLuminosity < 0){
    	printf("ERROR: AccretionLuminosity = %e < 0\n", AccretionLuminosity);
    	exit(1);
    }
	if(isnan(AccretionTemperature)){
        printf("ERROR: AccretionTemperature = 'nan' in StellarEvolution.c\n");
        exit(1);
    }
    if(AccretionTemperature < 0){
        printf("ERROR: AccretionTemperature = %e < 0 in StellarEvolution.c\n", AccretionTemperature);
        exit(1);
    }
	
	//
	// Interface properties:
	//
#if IRRADIATION == YES
	if(isnan(IrradiationLuminosity)){
        printf("ERROR: IrradiationLuminosity = 'nan'\n");
        exit(1);
    }
    if(IrradiationLuminosity < 0){
        printf("StellarLuminosity            = %e\n", StellarLuminosity);
        printf("AccretionLuminosity          = %e\n", AccretionLuminosity);
        printf("ERROR: IrradiationLuminosity = %e < 0\n", IrradiationLuminosity);
        exit(1);
    }
	if(isnan(IrradiationTemperature)){
        printf("ERROR: IrradiationTemperature = 'nan' in StellarEvolution.c\n");
        exit(1);
    }
    if(IrradiationTemperature < 0){
        printf("ERROR: IrradiationTemperature = %e < 0 in StellarEvolution.c\n", IrradiationTemperature);
        exit(1);
    }
#endif
#if IONIZATION == YES
	if(isnan(IonizationIrradiationRadius)){
        printf("ERROR: IonizationIrradiationRadius = 'nan'\n");
        exit(1);
    }
    if(IonizationIrradiationRadius <= 0){
        printf("ERROR: IonizationIrradiationRadius = %e <= 0\n", IonizationIrradiationRadius);
        exit(1);
    }
	if(isnan(IonizationIrradiationTemperature)){
        printf("ERROR: IonizationIrradiationTemperature = 'nan' in StellarEvolution.c\n");
        exit(1);
    }
    if(IonizationIrradiationTemperature < 0){
        printf("ERROR: IonizationIrradiationTemperature = %e < 0 in StellarEvolution.c\n", IonizationIrradiationTemperature);
        exit(1);
    }
	IonizationIrradiationTemperature = StellarTemperature;
	IonizationIrradiationRadius      = StellarRadius;
#endif
#endif
		
}



// ***********************************************
// ** UpdateStellarLuminosityAndStellarRadius() **
// ***********************************************
//
void UpdateStellarLuminosityAndStellarRadius(double StellarMass, double AccretionRate){

#if DEBUGGING == 1
	if(isnan(AccretionRate)){
        printf("ERROR: AccretionRate = 'nan'\n");
        exit(1);
    }
    if(AccretionRate < 0){
    	printf("ERROR: AccretionRate = %e Msol/yr < 0\n", AccretionRate * Year/SolarMass);
    	exit(1);
    }
	if(isnan(StellarMass)){
        printf("ERROR: StellarMass = 'nan'\n");
        exit(1);
    }
    if(StellarMass < 0){
    	printf("ERROR: StellarMass = %e Msol < 0\n", StellarMass / SolarMass);
    	exit(1);
    }
#endif
	
	
	switch(StellarEvolutionFlag){
		case 0:
			break;
//		case 1:
//			StellarRadius     =     StellarRadius_Hosokawa2009(StellarMass, AccretionRate);
//			StellarLuminosity = StellarLuminosity_Hosokawa2009(StellarMass, AccretionRate);
//			break;
//		case 2:
//			StellarRadius     = StellarRadius_Hosokawa2009(StellarMass, AccretionRate);
//			StellarLuminosity = MAX(StellarLuminosity, StellarLuminosity_Hosokawa2009(StellarMass, AccretionRate));
//			break;
//        case 3:
//            //
//            // Convert from code units to ProSiT units:
//            //
//            StellarMass   /= SolarMass;
//            AccretionRate /= SolarMass/Year;
//            
//            //
//            // Call ProSiT:
//            //
//            ProSiT(StellarMass, AccretionRate, &StellarLuminosity, &StellarRadius);
//            
//            //
//            // Print Evolution:
//            //
//            //printf("###           ... Mstar = %.2e Msol\n", StellarMass);
//            //printf("###           ... Mdot  = %.2e Msol yr^-1\n", StellarEvolution_AccretionRate);
//            //printf("###           ... Lstar = %.2e Lsol\n", StellarLuminosity);
//            //printf("###           ... Rstar = %.2e Rsol\n", StellarRadius);
//            
//            //
//            // Convert from ProSiT units to code units:
//            //
//            StellarMass       *= SolarMass;
//            AccretionRate     *= SolarMass/Year;
//            StellarLuminosity *= SolarLuminosity;
//            StellarRadius     *= SolarRadius;
//            break;
		case 4:
#if STELCOR == YES
			if(LocalRank == 0){
#if LOG_OUTPUT != 0
				PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... Call 'stelcor' subroutine for Stellar Evolution ...        ###\n");
#endif
				StelcorInterface();
				StelcorResults[0] = StellarLuminosity;
				StelcorResults[1] = StellarRadius;
				MPI_Bcast(StelcorResults, 2, MPI_DOUBLE, 0, PETSC_COMM_WORLD);
			}
			else{
				MPI_Bcast(StelcorResults, 2, MPI_DOUBLE, 0, PETSC_COMM_WORLD);
				StellarLuminosity = StelcorResults[0];
				StellarRadius     = StelcorResults[1];
			}
#endif
			break;
		case 5:
			StellarRadius =     pow(StellarMass / SolarMass, StellarRadiusScalingExponent)     * SolarRadius;
			StellarLuminosity = pow(StellarMass / SolarMass, StellarLuminosityScalingExponent) * SolarLuminosity;
			break;
//		case 6:
//			StellarLuminosity = StellarLuminosity_SuperEddingtonMainSequence(StellarMass);
//			StellarRadius     =     StellarRadius_SuperEddingtonMainSequence(StellarMass);
//			break;
//		case 7:
//			StellarLuminosity = StellarLuminosity_SuperEddingtonMainSequence(StellarMass);
//			if(StellarMass / SolarMass < 72.0) StellarRadius = StellarRadius_Hosokawa2009(StellarMass, 1.1e-3*SolarMass/Year);
//			else StellarRadius = pow(StellarMass / SolarMass - 30.0, 2.0/3.0) * SolarRadius;
//			break;
		default:
			PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
			PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
			PetscPrintf(PETSC_COMM_WORLD, "ERROR StellarEvolutionFlag = %d not available, see 'StellarEvolution.c'.\n", StellarEvolutionFlag);
			PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
			PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
			exit(1);
			break;
	}

}



// **********************************
// ** ComputeAccretionLuminosity() **
// **********************************
//
//                 G * M_star
// L_acc = Mdot * ------------
//                   R_star
//
double ComputeAccretionLuminosity(double AccretionRate, double StellarMass, double StellarRadius){
    return G_GravityConstant * AccretionRate * StellarMass / StellarRadius;
}



// ***************************************
// ** LuminosityAndRadius2Temperature() **
// ***************************************
//
//             L
// T^4 = ----------------
//        4 pi Sigma R^2
//
double LuminosityAndRadius2Temperature(double Luminosity, double Radius){
    return pow(Luminosity / (4.0 * M_PI * sigma_StefanBoltzmannConstant * pow(Radius, 2)), 0.25);
}


