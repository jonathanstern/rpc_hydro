#ifndef STELLAREVOLUTION_H_
#define STELLAREVOLUTION_H_

extern int StellarEvolutionFlag;
extern int AccretionLuminosityFlag;
extern int AccretionRateFlag;
extern double StellarEvolution_dt;
extern double ConstantAccretionRate;
extern double StellarLuminosity;
extern double StellarRadius;
extern double StellarRadiusScalingExponent;
extern double StellarLuminosityScalingExponent;


// ***********************
// ** External Routines **
// ***********************
//
void StellarEvolution(void);

#if STELCOR == YES
int StelcorInterface(void);
void stelcor_(double*, double*, double*, double*, double*, double*, int*);
#endif

#endif /* STELLAREVOLUTION_H_ */
