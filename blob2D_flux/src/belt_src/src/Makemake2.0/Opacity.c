#include "pluto.h"
#include "Opacity.h"
#include "Radiation.h"
#include "FLD.h"
#include "PhysicalConstantsCGS.h"
#include "DomainDecomposition.h"
#include "interface.h"
//#include "definitions.h"
//#include "Macros.h"
//#include "Grid.h"
#include "MakemakeTools.h" // LinearInterpolation

#if STELLAREVOLUTION == YES
#include "StellarEvolution.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>



int    DustOpacityFlag;
int    GasOpacityFlag;
int    OpacityExponent;
double ConstantDustOpacity;
double ConstantGasOpacity;

int    freq_nr;
double *freq_nu;
double *opac_kabs;

double *meanopac_temp;
int    meanopac_ntemp;
double *meanopac_kapplanck;
double *meanopac_kapross;



// ***********************
// ** Internal Routines **
// ***********************
//
double RosselandMeanOpacity_Routine(double, double, int, int, int);

//
// To read tables in RADMC format and compute the Mean Opacities:
//
int ReadRADMCTablesAndComputeOpacities(FILE *, FILE *);
double bplanck(double, double);
double bplanckdt(double, double);
double integrate(int, double*, double*);

//
// Bell & Lin (1994) fitting routine:
//
double FittedOpacities(double, double, int, int, int);

//
// Gas opacities by Helling et al. (2000):
//
double **RosselandMeanGasOpacityArray;
double **PlanckMeanGasOpacityArray;
double *GasOpacityArrayDensityValues;
double *GasOpacityArrayTemperatureValues;
int nx = 71;
int ny = 71;
//int hunt(double*, int, double);
void InitializeGasOpacities(void);
void FinalizeGasOpacities(void);
//double RosselandMeanGasOpacity(double, double);

//
// macro operators to convert input into string:
//
#define STRINGIZE2(x) #x
#define STRINGIZE(x) STRINGIZE2(x)



// *************************
// ** InitializeOpacity() **
// *************************
//
int InitializeOpacity(){
	
	FILE *FrequencyFilePointer, *DustOpacityFilePointer;
	
#if LOG_OUTPUT != 0
	if(prank == 0) fprintf(LogFile, "###       ... Initialize Opacities ...                                       ###\n");
#endif


	//printf("%s\n", STRINGIZE(BELT_DIR));
	
	char path2file[255];
	
	switch(DustOpacityFlag){
		case 0:
		case 1:
			return 0;
		case 2:
			strcpy (path2file, STRINGIZE(BELT_DIR));
			strcat (path2file,"/src/Makemake2.0/Opacities/");
			strcat (path2file,"OssenkopfHenning1994/frequency.inp");
			FrequencyFilePointer =   fopen(path2file, "r");
			strcpy (path2file, STRINGIZE(BELT_DIR));
			strcat (path2file,"/src/Makemake2.0/Opacities/");
			strcat (path2file,"OssenkopfHenning1994/dustopac_1.inp");
			DustOpacityFilePointer = fopen(path2file, "r");
			ReadRADMCTablesAndComputeOpacities(FrequencyFilePointer, DustOpacityFilePointer);
			break;
		case 3:
			strcpy (path2file, STRINGIZE(BELT_DIR));
			strcat (path2file,"/src/Makemake2.0/Opacities/");
			strcat (path2file,"DraineLee1984/frequency.inp");
			FrequencyFilePointer =   fopen(path2file, "r");
			strcpy (path2file, STRINGIZE(BELT_DIR));
			strcat (path2file,"/src/Makemake2.0/Opacities/");
			strcat (path2file,"DraineLee1984/dustopac_1.inp");
			DustOpacityFilePointer = fopen(path2file, "r");
			ReadRADMCTablesAndComputeOpacities(FrequencyFilePointer, DustOpacityFilePointer);
			break;
		case 4:
			strcpy (path2file, STRINGIZE(BELT_DIR));
			strcat (path2file,"/src/Makemake2.0/Opacities/");
			strcat (path2file,"LaorDraine1993/frequency.inp");
			FrequencyFilePointer =   fopen(path2file, "r");
			strcpy (path2file, STRINGIZE(BELT_DIR));
			strcat (path2file,"/src/Makemake2.0/Opacities/");
			strcat (path2file,"LaorDraine1993/dustopac_1.inp");
			DustOpacityFilePointer = fopen(path2file, "r");
			ReadRADMCTablesAndComputeOpacities(FrequencyFilePointer, DustOpacityFilePointer);
			break;
		case 5:
			strcpy (path2file, STRINGIZE(BELT_DIR));
			strcat (path2file,"/src/Makemake2.0/Opacities/");
			strcat (path2file,"WeingartnerDraine2001/frequency.inp");
			FrequencyFilePointer =   fopen(path2file, "r");
			strcpy (path2file, STRINGIZE(BELT_DIR));
			strcat (path2file,"/src/Makemake2.0/Opacities/");
			strcat (path2file,"WeingartnerDraine2001/dustopac_1.inp");
			DustOpacityFilePointer = fopen(path2file, "r");
			ReadRADMCTablesAndComputeOpacities(FrequencyFilePointer, DustOpacityFilePointer);
			break;
			//	case 6:
			//		if(IrradiationFlag != 0){
			//			if(prank == 0){
			//				printf("ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
			//				printf("ERROR\n");
			//				printf("ERROR Bell & Lin (1994) opacities (DustOpacityFlag = 6) cannot be used\n");
			//				printf("ERROR for Irradiation (use IrradiationFlag = 0).\n");
			//				printf("ERROR\n");
			//				printf("ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
			//			}
			//			exit(1);
			//		}
			//		break;
		case 7:
			strcpy (path2file, STRINGIZE(BELT_DIR));
			strcat (path2file,"/src/Makemake2.0/Opacities/");
			strcat (path2file,"WeingartnerDraine2001_MC3D/frequency.inp");
			FrequencyFilePointer =   fopen(path2file, "r");
			strcpy (path2file, STRINGIZE(BELT_DIR));
			strcat (path2file,"/src/Makemake2.0/Opacities/");
			strcat (path2file,"WeingartnerDraine2001_MC3D/dustopac_1.inp");
			DustOpacityFilePointer = fopen(path2file, "r");
			ReadRADMCTablesAndComputeOpacities(FrequencyFilePointer, DustOpacityFilePointer);
			break;
		default:
			if(prank == 0){
				printf("ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
				printf("ERROR\n");
				printf("ERROR DustOpacityFlag = %d not available, see 'Makemake.conf'.\n", DustOpacityFlag);
				printf("ERROR\n");
				printf("ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
			}
			MPI_Barrier(MPI_COMM_WORLD);
			exit(1);
	}
	
	
	switch(GasOpacityFlag){
		case 0:
		case 1:
			break;
		case 2:
			InitializeGasOpacities();
			break;
		default:
			if(prank == 0){
				printf("ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
				printf("ERROR\n");
				printf("ERROR GasOpacityFlag = %d not available, see 'Makemake.conf'.\n", GasOpacityFlag);
				printf("ERROR\n");
				printf("ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
			}
			MPI_Barrier(MPI_COMM_WORLD);
			exit(1);
	}
	
	return 0;
}




// ***********************
// ** FinalizeOpacity() **
// ***********************
//
// Frees the memory and destroys the objects allocated during InitializeRadiation().
//
int FinalizeOpacity(){
	
	switch(DustOpacityFlag){
		case 2:
		case 3:
		case 4:
		case 5:
		case 7:
			free(freq_nu);
			free(opac_kabs);
			free(meanopac_temp);
			free(meanopac_kapplanck);
			free(meanopac_kapross);
			break;
	}
	
	switch(GasOpacityFlag){
		case 0:
		case 1:
			break;
		case 2:
			FinalizeGasOpacities();
			break;
	}
	
	return 0;
}



// ******************************************
// ** ReadRADMCTablesAndComputeOpacities() **
// ******************************************
//
int ReadRADMCTablesAndComputeOpacities(FILE *FrequencyFilePointer, FILE *DustOpacityFilePointer){
	
	int i, itemp, inu;
	int nfdum;
	//	PetscErrorCode petscerrorcode;
	
	//
	// Read the frequency array from file:
	//
	double MaximumFrequency = 0.0;
	if(!(FrequencyFilePointer == NULL)){
		fscanf(FrequencyFilePointer, "%d", &freq_nr);
		
		//
		// Allocate memory for frequencies:
		//
		//		petscerrorcode = PetscNew(double[freq_nr], &freq_nu);
		freq_nu = malloc(freq_nr * sizeof(double));
		//		if(petscerrorcode){
		//			printf("ERROR: Unable to allocate memory in Opacity.c.\n");
		//			exit(1);	
		//		}
		
		//
		// Read frequencies:
		//
		for (i = 0; i < freq_nr; i++) {
			fscanf(FrequencyFilePointer, "%lE", &freq_nu[i]);
			MaximumFrequency = MAX(MaximumFrequency, freq_nu[i]);
		}
		//
		// Close file:
		//
		fclose (FrequencyFilePointer);
	}
	else{
		printf("ERROR: Unable to open the given frequency-input file.\n");
		exit(1);
	}
	
	//
	// Read the dust opacities from file:
	//
	FILE *OpacityTableOut;
	char buffer[256];
	int nomeaning;
	
	if(!(DustOpacityFilePointer == NULL)) {
		fscanf(DustOpacityFilePointer, "%d %d", &nfdum, &nomeaning);
		if (nfdum != freq_nr){
			printf("ERROR: frequency.inp and dustopac_1.inp have different number of frequencies/opacities.\n");
			exit(1);
		}
		
		//
		// Allocate memory for frequency-dependent opacities:
		//
		//		petscerrorcode = PetscNew(double[freq_nr], &opac_kabs);
		opac_kabs = malloc(freq_nr * sizeof(double));
		//		if(petscerrorcode){
		//			printf("ERROR: Unable to allocate memory in Opacity.c.\n");
		//			exit(1);	
		//		}
		
		//
		// Read opacities:
		//
		sprintf(buffer, "%s%s", "./data/", "FrequencyDependentOpacities.out");
		if(prank == 0) OpacityTableOut = fopen(buffer, "w");
		for(i = 0; i < nfdum; i++) {
			fscanf (DustOpacityFilePointer, "%lE", &opac_kabs[i]);
			if(prank == 0) fprintf(OpacityTableOut, "%e %e\n", freq_nu[i], opac_kabs[i]);
		}
		//
		// Close file:
		//
		fclose (DustOpacityFilePointer);
	}
	else{
		printf("ERROR: Can't open 'dustopac_1.inp'\n");
		exit(1);
	}
	if(prank == 0) fclose(OpacityTableOut);
	
	//
	// With gas-to-dust ratio convert from the dust opacity to an opacity for the gas+dust mixture:
	//
	//	for(i = 0; i < freq_nr; i++) {
	//		opac_kabs[i] = opac_kabs[i] / GasToDustRatio;
	//	}
	
	//
	// Make the temperature grid for the mean opacities:
	//
	//  meanopac_ntemp = size of mean opacity table
	//  tmin, tmax = lower and upper limit of temperature column of the mean opacity table
	// NO! Handled now as pure dust opacity
	//
	double tmin = 0.1; // in [K]
	double tmax = 100000.0; // in [K]
	meanopac_ntemp = 100000;
	
	//
	// Allocate memory:
	//
	//	petscerrorcode = PetscNew(double[meanopac_ntemp], &meanopac_temp);
	meanopac_temp = malloc(meanopac_ntemp * sizeof(double));
	//	if(petscerrorcode){
	//		printf("ERROR: Unable to allocate memory in Opacity.c.\n");
	//		exit(1);	
	//	}
	
	//
	// Calculate temperatures:
	//
	for(itemp = 0; itemp < meanopac_ntemp; itemp++) {
		meanopac_temp[itemp] = tmin * pow((tmax/tmin), itemp/(meanopac_ntemp-1.0));
	}
	
	//
	// Make the Planck opacities:
	//
	double *dumarr1, *dumarr2;
	sprintf(buffer, "%s%s", "./data/", "PlanckMeanOpacities.out");
	if(prank == 0) OpacityTableOut = fopen(buffer, "w");
	
	//
	// Allocate memory:
	//
	//	petscerrorcode = PetscNew(double[freq_nr], &dumarr1);
	dumarr1 = malloc(freq_nr * sizeof(double));
	//	if(petscerrorcode){
	//		printf("ERROR: Unable to allocate memory in Opacity.c.\n");
	//		exit(1);	
	//	}
	//	petscerrorcode = PetscNew(double[freq_nr], &dumarr2);
	dumarr2 = malloc(freq_nr * sizeof(double));
	//	if(petscerrorcode){
	//		printf("ERROR: Unable to allocate memory in Opacity.c.\n");
	//		exit(1);	
	//	}
	//	petscerrorcode = PetscNew(double[meanopac_ntemp], &meanopac_kapplanck);
	meanopac_kapplanck = malloc(meanopac_ntemp * sizeof(double));
	//	if(petscerrorcode){
	//		printf("ERROR: Unable to allocate memory in Opacity.c.\n");
	//		exit(1);	
	//	}	
	
	//
	// 'Integrate' over the frequency-dependent Opacities for each point in the Temperature grid.
	//
	for(itemp = 0; itemp < meanopac_ntemp; itemp++) {
		for(inu = 0; inu < freq_nr; inu++) {
			dumarr1[inu] = bplanck(meanopac_temp[itemp], freq_nu[inu]);
			dumarr2[inu] = dumarr1[inu] * opac_kabs[inu];
		}
		double dum1 = integrate(freq_nr, freq_nu, dumarr1);
		double dum2 = integrate(freq_nr, freq_nu, dumarr2);
		meanopac_kapplanck[itemp] = dum2/dum1;
		if(prank == 0) fprintf(OpacityTableOut, "%e %e\n", meanopac_temp[itemp], meanopac_kapplanck[itemp]);
	}
	
	
	//
	// Make the Rosseland opacities:
	//
	sprintf(buffer, "%s%s", "./data/", "RosselandMeanOpacities.out");
	if(prank == 0) OpacityTableOut = fopen(buffer, "w");
	
	//
	// Allocate memory:
	//
	//	petscerrorcode = PetscNew(double[meanopac_ntemp], &meanopac_kapross);
	meanopac_kapross = malloc(meanopac_ntemp * sizeof(double));
	//	if(petscerrorcode){
	//		printf("ERROR: Unable to allocate memory in Opacity.c.\n");
	//		exit(1);	
	//	}	
	
	//
	// 'Integrate' over the frequency-dependent Opacities for each point in the Temperature grid.
	//
	for(itemp = 0; itemp < meanopac_ntemp; itemp++) {
		for(inu = 0; inu < freq_nr; inu++) {
			dumarr1[inu] = bplanckdt(meanopac_temp[itemp], freq_nu[inu]);
			dumarr2[inu] = dumarr1[inu] / opac_kabs[inu];
		}
		double dum1 = integrate(freq_nr, freq_nu, dumarr1);
		double dum2 = integrate(freq_nr, freq_nu, dumarr2);
		meanopac_kapross[itemp] = dum1/dum2;
		if(prank == 0) fprintf(OpacityTableOut, "%e %e\n", meanopac_temp[itemp], meanopac_kapross[itemp]);
	}
	if(prank == 0) fclose(OpacityTableOut);
	
	//
	// Free memory:
	//
	free(dumarr1);
	free(dumarr2);
	
	return 0;
}



// *****************************************************************
// *            THE BLACKBODY PLANCK FUNCTION B_nu(T)              *
// *                                                               *
// * This function computes the Blackbody function                 *
// *                                                               *
// *                2 h nu^3 / c^2                                 *
// *    B_nu(T)  = -------------------    [ erg / cm^2 s ster Hz ] *
// *               exp(-h nu / kT) - 1                             *
// *                                                               *
// *  ARGUMENTS:                                                   *
// *     nu    [Hz]            = Frequency                         *
// *     temp  [K]             = Electron temperature              *
// *****************************************************************
//
double bplanck (double temp, double nu) {
    if(temp != 0)
    	return 1.47455E-47 * pow(nu, 3) / (exp(4.7989E-11 * nu / temp)-1.) + 1.E-290;
    else
    	return 0;
}



// *************************************************************
// *       THE TEMPERATURE DERIVATIVE OF PLANCK FUNCTION       *
// *                                                           *
// *  This function computes the temperature derivative of the *
// *  Blackbody function                                       *
// *                                                           *
// *     dB_nu(T)     2 h^2 nu^4      exp(h nu / kT)        1  *
// *     --------   = ---------- ------------------------  --- *
// *        dT          k c^2    [ exp(h nu / kT) - 1 ]^2  T^2 *
// *                                                           *
// *   ARGUMENTS:                                              *
// *       nu    [Hz]            = Frequency                   *
// *       temp  [K]             = Electron temperature        *
// *************************************************************
//
double bplanckdt (double temp, double nu) {
    double theexp = exp(4.7989E-11*nu/temp);
    if (theexp < 1E+33)
    	return 7.07661334104E-58 * pow(nu, 4) * theexp / ( pow((theexp-1.), 2) * pow(temp, 2) ) + 1E-290;
    else
    	return 7.07661334104E-58 * pow(nu, 4) /( theexp * pow(temp, 2) ) + 1.E-290;
}



// ***********************
// * INTEGRATION ROUTINE *
// ***********************
//
double integrate(int n, double * x, double * f){
	
	int i;
    double integral = 0;
	
    for(i = 1; i < n; i++) {
    	integral +=
		0.5 * (f[i] + f[i-1])
		*
		(x[i] - x[i-1]);
    }
    if(x[n] > x[1])
    	return integral;
    else
    	return -integral;
}



// *********************************
// ** FrequencyDependentOpacity() **
// *********************************
//
// Returns the (linear interpolated) frequency dependent opacity for given input frequency [in code units].
//
double FrequencyDependentOpacity(double Frequency){
	
	double Opacity;
//  double frac;
	int index;
	
	//
	// Convert input frequency into cgs units [Hz]:
	//
	Frequency *= ReferenceFrequency;
	
	//
	// Determine position in frequency table:
	//
	index = hunt(freq_nu, freq_nr, Frequency);
	
	//
	// Return the 'extreme' opacities for 'extreme' frequencies:
	//
	if(index == -1)             Opacity = opac_kabs[0];
	else if(index == freq_nr-1) Opacity = opac_kabs[freq_nr-1];
	else{
		//
		// Linear interpolation of neighboring opacities:
		//
        if(freq_nu[index] < freq_nu[index+1])
            Opacity =
                LinearInterpolation1D(
                                      freq_nu[index]  , freq_nu[index+1],
                                      opac_kabs[index], opac_kabs[index+1],
                                      Frequency
                                      );
        else
            Opacity =
                LinearInterpolation1D(
                                      freq_nu[index+1]  , freq_nu[index],
                                      opac_kabs[index+1], opac_kabs[index],
                                      Frequency
                                      );
//		frac =
//		(Frequency        - freq_nu[index])
//		/
//		(freq_nu[index+1] - freq_nu[index]);
//
//		Opacity =
//		(1-frac) * opac_kabs[index]
//		+
//		frac     * opac_kabs[index+1];
	}
	
#if DEBUGGING == 1
	if(index == -1 || index == freq_nr-1){
		printf("WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		printf("WARNING\n");
		printf("WARNING The frequency input into 'FrequencyDependentOpacity' is\n");
		printf("WARNING out of the range of the specified frequency/opacity table.\n");
		printf("WARNING  Frequency input = %e Hz\n", Frequency);
		printf("WARNING  Frequency[0] =    %e Hz\n", freq_nu[0]);
		printf("WARNING  Frequency[imax] = %e Hz\n", freq_nu[freq_nr-1]);
		printf("WARNING\n");
		printf("WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		exit(1);
	}
#endif
	
	//
	// Convert Opacity to code units:
	//
	Opacity /= ReferenceOpacity;
	
	return Opacity;
}



// *************************
// ** PlanckMeanOpacity() **
// *************************
//
double PlanckMeanDustOpacity(double RadiationTemperature, double DustTemperature, double DustDensity){
	
	double PlanckMeanOpacity, frac;
	int index;
	
	//
	// Convert input quantities into cgs units:
	//
	RadiationTemperature *= ReferenceTemperature;
	DustDensity *= ReferenceDensity;
	
	switch(DustOpacityFlag){
		case 0:
			PlanckMeanOpacity = 0;
			break;
		case 1:
			PlanckMeanOpacity = ConstantDustOpacity;
			break;
		case 2:
		case 3:
		case 4:
		case 5:
		case 7:
			//
			// Determine position in meanopac_temp table:
			//
			index = hunt(meanopac_temp, meanopac_ntemp, RadiationTemperature);
			
			//
			// Return the 'extreme' opacities for 'extreme' temperatures:
			//
			if(index == -1)	                   PlanckMeanOpacity = meanopac_kapplanck[0];
			else if(index == meanopac_ntemp-1) PlanckMeanOpacity = meanopac_kapplanck[meanopac_ntemp-1];
			else{
				//
				// Linear interpolation of neighboring opacities:
				//
                // TODO: Why does the routine call make a difference (e.g. in the "Pascucci_Tau=100.0_Full" Radiation Transport test)
//                PlanckMeanOpacity =
//                    LinearInterpolation1D(
//                                          meanopac_temp[index]     , meanopac_temp[index+1],
//                                          meanopac_kapplanck[index], meanopac_kapplanck[index+1],
//                                          RadiationTemperature
//                                          );

//                PlanckMeanOpacity = (meanopac_kapplanck[index+1] - meanopac_kapplanck[index]) / (meanopac_temp[index+1] - meanopac_temp[index]) * (RadiationTemperature - meanopac_temp[index]) + meanopac_kapplanck[index];

                
				frac =
	    		(RadiationTemperature   - meanopac_temp[index])
	    		/
	    		(meanopac_temp[index+1] - meanopac_temp[index]);

#if DEBUGGING == 1
				if( (frac < 0) || (frac > 1) ){
					printf("ERROR during calculation of Planck Mean Opacity: frac < 0 or frac > 1\n");
					printf("trad =                %e\n", RadiationTemperature);
					printf("meanopac_temp[%4d] = %e\n", index, meanopac_temp[index]);
					printf("meanopac_temp[%4d] = %e\n", index+1, meanopac_temp[index+1]);
					printf("frac =                  %e\n", frac);
					exit(1);
				}
#endif

				PlanckMeanOpacity =
				(1-frac) * meanopac_kapplanck[index]
				+
				frac     * meanopac_kapplanck[index+1];
			}
			break;
		case 6:
			// Bell & Lin (1994) Planck mean opacities currently not available?! IrradiationFlag = 0 is default.
			break;
	}
	
	//
	// Convert PlanckMeanOpacity into code units:
	//
	PlanckMeanOpacity /= ReferenceOpacity;
	
	return PlanckMeanOpacity;
}



// ****************************
// ** RosselandMeanOpacity() **
// ****************************
//
double RosselandMeanDustOpacity(double DustTemperature, double DustDensity){
	
	double RosselandMeanOpacity;
	int index;
	
	//
	// Convert input quantities from code units to cgs units:
	//
	DustDensity     *= ReferenceDensity;
	DustTemperature *= ReferenceTemperature;
	
	switch(DustOpacityFlag){
		case 0:
			RosselandMeanOpacity = 0;
			break;
		case 1:
			RosselandMeanOpacity = ConstantDustOpacity;
			break;
		case 2:
		case 3:
		case 4:
		case 5:
		case 7:
			//
			// Determine position in meanopac_temp table:
			//
			index = hunt(meanopac_temp, meanopac_ntemp, DustTemperature);
			//
			// Return the 'extreme' opacities for 'extreme' temperatures:
			//
			if(index == -1)                    RosselandMeanOpacity = meanopac_kapross[0];
			else if(index == meanopac_ntemp-1) RosselandMeanOpacity = meanopac_kapross[meanopac_ntemp-1];
			else{
				//
				// Linear interpolation of neighbouring opacities:
				//
				RosselandMeanOpacity =
                    LinearInterpolation1D(
									  meanopac_temp[index]   , meanopac_temp[index+1],
									  meanopac_kapross[index], meanopac_kapross[index+1],
									  DustTemperature
									  );
                //				frac =
                //	    		(DustTemperature        - meanopac_temp[index])
                //	    		/
                //	    		(meanopac_temp[index+1] - meanopac_temp[index]);
                //#if DEBUG > 0
                //				if( (frac < 0) || (frac > 1) ){
                //					printf("ERROR during calculation of Rosseland Mean Opacity: frac < 0 or frac > 1\n");
                //					printf("DustTemperature =      %e\n", DustTemperature);
                //					printf("meanopac_temp[%4d] = %e\n", index,   meanopac_temp[index]);
                //					printf("meanopac_temp[%4d] = %e\n", index+1, meanopac_temp[index+1]);
                //					printf("frac =                %e\n", frac);
                //					exit(1);
                //				}
                //#endif
                //				RosselandMeanOpacity =
                //	        	(1-frac) * meanopac_kapross[index]
                //	        	+
                //	        	frac     * meanopac_kapross[index+1];
			}
			break;
		case 6:
			// TODO: fix implementation, see FittedOpacities routine below
			//RosselandMeanOpacity = FittedOpacities(DustTemperature, DustDensity, k, j, i);
			break;
		default:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: DustOpacityFlag = %d is not in allowed range              ###\n", DustOpacityFlag);
			exit(1);
			break;
	}
	
	//
	// Convert RosselandMeanOpacity output from cgs units to code units:
	//
	RosselandMeanOpacity /= ReferenceOpacity;
	
	return RosselandMeanOpacity;	
}



// *********************
// ** FittedOpacities **
// *********************
//
// Get opacity from fitted values. See Bell & Lin (1994).
//
double FittedOpacities(double DustTemperature, double Density, int k, int j, int i){
	
	double RosselandMeanOpacity;
	double temp, rho;
	double num1 = 1.e22, num2 = 6.5e-5, num3;
	double k1,k2,k3,k4,k5,k6,k7,k8;
	double k11,k22,k33,k44,k66,k77,k88;
	
	// Opacity coefficients ki
	// Cannot handle 2.e81 in c[3] --> 2.e21
	double c[8] = {2.e-4, 2.e16, 0.1, 2.e21, 1.e-8, 1.e-36, 1.5e20, 0.348};
	
	// Density exponent a
	double a[8] = {0.0, 0.0, 0.0, 1.0, 2/3., 1/3., 1.0, 0.0};
	
	// Temperature exponent b
	double b[8] = {2.0, -7.0, 1./2., -24.0, 3.0, 10.0, -5/2., 0.0};
	
	// Different regions..
	double power[3] = {2.8369e-2, 1.1464e-2, 2.2667e-1};
	double coef[3] = {1.46e3, 4.51e3, 2.37e6};
	
	//
	// Convert input quantities into cgs units:
	//
	Density         *= ReferenceDensity;
	DustTemperature *= ReferenceTemperature;
	
	temp = DustTemperature;
	rho = Density;
	
	if(temp < coef[0]*(pow(rho,power[0])) ){
		k1 = c[0]*pow(rho,a[0])*pow(temp,b[0]);
		k2 = c[1]*pow(rho,a[1])*pow(temp,b[1]);
		k3 = c[2]*pow(rho,a[2])*pow(temp,b[2]);
		k11 = pow(k1,2.);
		k22 = pow(k2,2.);
		k = pow( pow((k11*k22/(k11+k22)),2.) + pow(k3/(1.+(num1/pow(temp,10.))),4) ,1/4.);
		RosselandMeanOpacity = k;
	}
	else {
		if (temp < (coef[1]*pow(rho,power[1])) ){
			k3 = c[2]*pow(rho,a[2])*pow(temp,b[2]);
			k4 = c[3]*pow(rho,a[3])*pow(temp,b[3])*(1.e20)*(1.e20)*(1.e20);
			k5 = c[4]*pow(rho,a[4])*pow(temp,b[4]);
			k33 = pow(k3,4);
			k44 = pow(k4,4);
			num3 = pow(1 - 4*temp,8);
			k = pow( (k33*k44/(k33+k44)) + pow(k5/(1.+(num2/(num3*pow(rho,2/3.)*100.))),4) ,1/4.);
			RosselandMeanOpacity = k;
		}
		else {
			if ( temp < (coef[2]*pow(rho,power[2])) ){
				k5 = c[4]*pow(rho,a[4])*pow(temp,b[4]);
				k6 = c[5]*pow(rho,a[5])*pow(temp,b[5]);
				k7 = c[6]*pow(rho,a[6])*pow(temp,b[6]);
				k66 = pow(k6,2);
				k77 = pow(k7,2);
				num3 = (1-4*temp);
				k = pow( pow(k66*k77/(k66+k77),2) + pow(k5/(1.+pow(num3/(1.1*pow(rho,0.04762)),10)),4) ,1/4.);
				RosselandMeanOpacity = k;
			}
			else {
				k7 = c[6]*pow(rho,a[6])*pow(temp,b[6]);
				k8 = c[7]*pow(rho,a[7])*pow(temp,b[7]);
				k77 = pow(k7,2);
				k88 = pow(k8,2);
				k = pow(k77 + k88,1/2.);
				RosselandMeanOpacity = k;
			}
		}
	}
	
	return RosselandMeanOpacity;
}



// **********************************************************************
// ** Gas opacities by Helling et al. (2000) / Semenenov et al. (2003) **
// **********************************************************************
//
void InitializeGasOpacities(){
	
	FILE *GasOpacityFile;
	int i, ix, iy;
	
	//
	// Allocate memory for the gas opacity density and temperature grid values:
	//
	GasOpacityArrayDensityValues =     malloc(nx * sizeof(double));
	GasOpacityArrayTemperatureValues = malloc(ny * sizeof(double));
	
	//
	// Set the values of the density - temperature grid:
	//
	GasOpacityArrayDensityValues[0]  = 0.2364E-06;
	GasOpacityArrayDensityValues[1]  = 0.1646E-06;
	GasOpacityArrayDensityValues[2]  = 0.1147E-06;
	GasOpacityArrayDensityValues[3]  = 0.7985E-07;
	GasOpacityArrayDensityValues[4]  = 0.5560E-07;
	GasOpacityArrayDensityValues[5]  = 0.3872E-07;
	GasOpacityArrayDensityValues[6]  = 0.2697E-07;
	GasOpacityArrayDensityValues[7]  = 0.1878E-07;
	GasOpacityArrayDensityValues[8]  = 0.1308E-07;
	GasOpacityArrayDensityValues[9]  = 0.9107E-08;
	GasOpacityArrayDensityValues[10] = 0.6342E-08;
	GasOpacityArrayDensityValues[11] = 0.4417E-08;
	GasOpacityArrayDensityValues[12] = 0.3076E-08;
	GasOpacityArrayDensityValues[13] = 0.2142E-08;
	GasOpacityArrayDensityValues[14] = 0.1492E-08;
	GasOpacityArrayDensityValues[15] = 0.1039E-08;
	GasOpacityArrayDensityValues[16] = 0.7234E-09;
	GasOpacityArrayDensityValues[17] = 0.5038E-09;
	GasOpacityArrayDensityValues[18] = 0.3508E-09;
	GasOpacityArrayDensityValues[19] = 0.2443E-09;
	GasOpacityArrayDensityValues[20] = 0.1701E-09;
	GasOpacityArrayDensityValues[21] = 0.1185E-09;
	GasOpacityArrayDensityValues[22] = 0.8252E-10;
	GasOpacityArrayDensityValues[23] = 0.5746E-10;
	GasOpacityArrayDensityValues[24] = 0.4002E-10;
	GasOpacityArrayDensityValues[25] = 0.2787E-10;
	GasOpacityArrayDensityValues[26] = 0.1941E-10;
	GasOpacityArrayDensityValues[27] = 0.1352E-10;
	GasOpacityArrayDensityValues[28] = 0.9412E-11;
	GasOpacityArrayDensityValues[29] = 0.6554E-11;
	GasOpacityArrayDensityValues[30] = 0.4565E-11;
	GasOpacityArrayDensityValues[31] = 0.3179E-11;
	GasOpacityArrayDensityValues[32] = 0.2214E-11;
	GasOpacityArrayDensityValues[33] = 0.1542E-11;
	GasOpacityArrayDensityValues[34] = 0.1074E-11;
	GasOpacityArrayDensityValues[35] = 0.7476E-12;
	GasOpacityArrayDensityValues[36] = 0.5206E-12;
	GasOpacityArrayDensityValues[37] = 0.3626E-12;
	GasOpacityArrayDensityValues[38] = 0.2525E-12;
	GasOpacityArrayDensityValues[39] = 0.1758E-12;
	GasOpacityArrayDensityValues[40] = 0.1225E-12;
	GasOpacityArrayDensityValues[41] = 0.8528E-13;
	GasOpacityArrayDensityValues[42] = 0.5939E-13;
	GasOpacityArrayDensityValues[43] = 0.4136E-13;
	GasOpacityArrayDensityValues[44] = 0.2880E-13;
	GasOpacityArrayDensityValues[45] = 0.2006E-13;
	GasOpacityArrayDensityValues[46] = 0.1397E-13;
	GasOpacityArrayDensityValues[47] = 0.9727E-14;
	GasOpacityArrayDensityValues[48] = 0.6774E-14;
	GasOpacityArrayDensityValues[49] = 0.4717E-14;
	GasOpacityArrayDensityValues[50] = 0.3285E-14;
	GasOpacityArrayDensityValues[51] = 0.2288E-14;
	GasOpacityArrayDensityValues[52] = 0.1593E-14;
	GasOpacityArrayDensityValues[53] = 0.1109E-14;
	GasOpacityArrayDensityValues[54] = 0.7726E-15;
	GasOpacityArrayDensityValues[55] = 0.5381E-15;
	GasOpacityArrayDensityValues[56] = 0.3747E-15;
	GasOpacityArrayDensityValues[57] = 0.2609E-15;
	GasOpacityArrayDensityValues[58] = 0.1817E-15;
	GasOpacityArrayDensityValues[59] = 0.1265E-15;
	GasOpacityArrayDensityValues[60] = 0.8813E-16;
	GasOpacityArrayDensityValues[61] = 0.6137E-16;
	GasOpacityArrayDensityValues[62] = 0.4274E-16;
	GasOpacityArrayDensityValues[63] = 0.2976E-16;
	GasOpacityArrayDensityValues[64] = 0.2073E-16;
	GasOpacityArrayDensityValues[65] = 0.1443E-16;
	GasOpacityArrayDensityValues[66] = 0.1005E-16;
	GasOpacityArrayDensityValues[67] = 0.7000E-17;
	GasOpacityArrayDensityValues[68] = 0.4875E-17;
	GasOpacityArrayDensityValues[69] = 0.3395E-17;
	GasOpacityArrayDensityValues[70] = 0.2364E-17;
	
	GasOpacityArrayTemperatureValues[0]  =   500.00;
	GasOpacityArrayTemperatureValues[1]  =   521.86;
	GasOpacityArrayTemperatureValues[2]  =   544.68;
	GasOpacityArrayTemperatureValues[3]  =   568.50;
	GasOpacityArrayTemperatureValues[4]  =   593.35;
	GasOpacityArrayTemperatureValues[5]  =   619.30;
	GasOpacityArrayTemperatureValues[6]  =   646.38;
	GasOpacityArrayTemperatureValues[7]  =   674.64;
	GasOpacityArrayTemperatureValues[8]  =   704.14;
	GasOpacityArrayTemperatureValues[9]  =   734.93;
	GasOpacityArrayTemperatureValues[10] =   767.06;
	GasOpacityArrayTemperatureValues[11] =   800.60;
	GasOpacityArrayTemperatureValues[12] =   835.61;
	GasOpacityArrayTemperatureValues[13] =   872.15;
	GasOpacityArrayTemperatureValues[14] =   910.28;
	GasOpacityArrayTemperatureValues[15] =   950.08;
	GasOpacityArrayTemperatureValues[16] =   991.63;
	GasOpacityArrayTemperatureValues[17] =  1034.99;
	GasOpacityArrayTemperatureValues[18] =  1080.24;
	GasOpacityArrayTemperatureValues[19] =  1127.47;
	GasOpacityArrayTemperatureValues[20] =  1176.77;
	GasOpacityArrayTemperatureValues[21] =  1228.23;
	GasOpacityArrayTemperatureValues[22] =  1281.93;
	GasOpacityArrayTemperatureValues[23] =  1337.99;
	GasOpacityArrayTemperatureValues[24] =  1396.49;
	GasOpacityArrayTemperatureValues[25] =  1457.55;
	GasOpacityArrayTemperatureValues[26] =  1521.28;
	GasOpacityArrayTemperatureValues[27] =  1587.80;
	GasOpacityArrayTemperatureValues[28] =  1657.23;
	GasOpacityArrayTemperatureValues[29] =  1729.69;
	GasOpacityArrayTemperatureValues[30] =  1805.32;
	GasOpacityArrayTemperatureValues[31] =  1884.26;
	GasOpacityArrayTemperatureValues[32] =  1966.65;
	GasOpacityArrayTemperatureValues[33] =  2052.64;
	GasOpacityArrayTemperatureValues[34] =  2142.39;
	GasOpacityArrayTemperatureValues[35] =  2236.07;
	GasOpacityArrayTemperatureValues[36] =  2333.84;
	GasOpacityArrayTemperatureValues[37] =  2435.89;
	GasOpacityArrayTemperatureValues[38] =  2542.40;
	GasOpacityArrayTemperatureValues[39] =  2653.56;
	GasOpacityArrayTemperatureValues[40] =  2769.59;
	GasOpacityArrayTemperatureValues[41] =  2890.69;
	GasOpacityArrayTemperatureValues[42] =  3017.09;
	GasOpacityArrayTemperatureValues[43] =  3149.01;
	GasOpacityArrayTemperatureValues[44] =  3286.70;
	GasOpacityArrayTemperatureValues[45] =  3430.41;
	GasOpacityArrayTemperatureValues[46] =  3580.41;
	GasOpacityArrayTemperatureValues[47] =  3736.96;
	GasOpacityArrayTemperatureValues[48] =  3900.36;
	GasOpacityArrayTemperatureValues[49] =  4070.91;
	GasOpacityArrayTemperatureValues[50] =  4248.91;
	GasOpacityArrayTemperatureValues[51] =  4434.69;
	GasOpacityArrayTemperatureValues[52] =  4628.60;
	GasOpacityArrayTemperatureValues[53] =  4830.98;
	GasOpacityArrayTemperatureValues[54] =  5042.22;
	GasOpacityArrayTemperatureValues[55] =  5262.69;
	GasOpacityArrayTemperatureValues[56] =  5492.80;
	GasOpacityArrayTemperatureValues[57] =  5732.98;
	GasOpacityArrayTemperatureValues[58] =  5983.65;
	GasOpacityArrayTemperatureValues[59] =  6245.29;
	GasOpacityArrayTemperatureValues[60] =  6518.36;
	GasOpacityArrayTemperatureValues[61] =  6803.38;
	GasOpacityArrayTemperatureValues[62] =  7100.86;
	GasOpacityArrayTemperatureValues[63] =  7411.34;
	GasOpacityArrayTemperatureValues[64] =  7735.41;
	GasOpacityArrayTemperatureValues[65] =  8073.64;
	GasOpacityArrayTemperatureValues[66] =  8426.66;
	GasOpacityArrayTemperatureValues[67] =  8795.12;
	GasOpacityArrayTemperatureValues[68] =  9179.68;
	GasOpacityArrayTemperatureValues[69] =  9581.07;
	GasOpacityArrayTemperatureValues[70] = 10000.00;
	
	
	//
	// Allocate memory for the gas opacity array:
	//
	RosselandMeanGasOpacityArray = malloc(ny*sizeof(double));
	for (i = 0; i < ny; i++) {
		RosselandMeanGasOpacityArray[i] = malloc(nx*sizeof(double));
	}
	
	//
	// Read the gas opacity file into the gas opacity array:
	//
	char path2file[255];
	strcpy(path2file, STRINGIZE(BELT_DIR));
	strcat(path2file,"/src/Makemake2.0/Opacities/");
	strcat(path2file,"Hellingetal2000/kR_h2001.dat");
	GasOpacityFile = fopen(path2file, "r");
	
	// Read the bla bla value:
	fscanf(GasOpacityFile, "%le", &RosselandMeanGasOpacityArray[0][0]);
	
	// Read the 2D array:
	for(ix = 0; ix < nx; ix++){
		for(iy = 0; iy < ny; iy++){
			fscanf(GasOpacityFile, "%le", &RosselandMeanGasOpacityArray[iy][ix]);
			//printf("%d %d: %e\n", iy, ix, RosselandMeanGasOpacityArray[iy][ix]);
		}
	}
	fclose(GasOpacityFile);
	
	//
	// Allocate memory for the gas opacity array:
	//
	PlanckMeanGasOpacityArray = malloc(ny*sizeof(double));
	for (i = 0; i < ny; i++) {
		PlanckMeanGasOpacityArray[i] = malloc(nx*sizeof(double));
	}
	
	//
	// Read the gas opacity file into the gas opacity array:
	//
	strcpy(path2file, STRINGIZE(BELT_DIR));
	strcat(path2file,"/src/Makemake2.0/Opacities/");
	strcat(path2file,"Hellingetal2000/kP_h2001.dat");
	GasOpacityFile = fopen(path2file, "r");
	
	// Read the bla bla value:
	fscanf(GasOpacityFile, "%le", &PlanckMeanGasOpacityArray[0][0]);
	
	// Read the 2D array:
	for(ix = 0; ix < nx; ix++){
		for(iy = 0; iy < ny; iy++){
			fscanf(GasOpacityFile, "%le", &PlanckMeanGasOpacityArray[iy][ix]);
			//printf("%d %d: %e\n", iy, ix, PlanckMeanGasOpacityArray[iy][ix]);
		}
	}
	fclose(GasOpacityFile);
}



void FinalizeGasOpacities(){
	free(RosselandMeanGasOpacityArray);
	free(PlanckMeanGasOpacityArray);
	free(GasOpacityArrayDensityValues);
	free(GasOpacityArrayTemperatureValues);
}



double GasOpacity_Hellingetal2000(double *GasDensities,    int NGasDensities,
								  double *GasTemperatures, int NGasTemperatures,
							      double **D, 
							      double GasDensity, double GasTemperature){
	
	int nlx, nly;
	
	//
	// Search the nearest lower grid point 'nly' to the 'GasTemperature':
	//
	nly = hunt(GasTemperatures, NGasTemperatures, GasTemperature);
	
	//
	// Search the nearest lower grid point 'nlx' to the 'GasDensity':
	//
	nlx = hunt(GasDensities, NGasDensities, GasDensity);
	
	//
	// Check, if GasTemperature and GasDensity are inside the given grid range:
	//
	if(GasDensity <= GasDensities[70]){
		// use the opacity regarding the lowest gas density:
		nlx = 69;
		GasDensity = GasDensities[70];
	}
	if(GasDensity > GasDensities[0]){
		printf("ERROR: Gas opacity for densities > 2.364E-07 g cm^-3 not available yet.\n");
		exit(1);
		// use the opacity regarding the highest gas density:
		//nlx = 0;
		//GasDensity = GasDensities[0];
	}
	if(GasTemperature < GasTemperatures[0]){
		// use the opacity regarding the lowest gas temperature:
		nly = 0;
		GasTemperature = GasTemperatures[0];
	}
	if(GasTemperature >= GasTemperatures[70]){
		//printf("ERROR: Gas opacity for temperatures > 10000 K not available yet.\n");
		//exit(1);
		// use the opacity regarding the highest gas temperature:
		nly = 69;
		GasTemperature = GasTemperatures[70];
	}
	
	//
	// Interpolate:
	//
	return LinearInterpolation2D(GasDensities[nlx+1],  GasDensities[nlx], 
								 GasTemperatures[nly], GasTemperatures[nly+1], 
								 D[nly][nlx+1], D[nly][nlx], D[nly+1][nlx+1], D[nly+1][nlx], 
								 GasDensity, GasTemperature);
}



double PlanckMeanGasOpacity(double RadiationTemperature, double GasTemperature, double GasDensity){
	
	double PlanckMeanOpacity;
	
	//
	// Convert to cgs:
	//
	GasDensity     *= ReferenceDensity;
	GasTemperature *= ReferenceTemperature;
	
	switch(GasOpacityFlag){
		case 0:
			break;
		case 1:
			PlanckMeanOpacity = ConstantGasOpacity;
			break;
		case 2:
			PlanckMeanOpacity = GasOpacity_Hellingetal2000(GasOpacityArrayDensityValues, nx, GasOpacityArrayTemperatureValues, ny, PlanckMeanGasOpacityArray, GasDensity, GasTemperature);
			
			//
			// Convert the opacity from the power exponent to the linear scale:
			//
			PlanckMeanOpacity = pow(10.0, PlanckMeanOpacity);
			break;
		default:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: GasOpacityFlag = %d is not in allowed range              ###\n", GasOpacityFlag);
			exit(1);
	}
	
	//
	// Convert to code units:
	//
	PlanckMeanOpacity /= ReferenceOpacity;
	
	return PlanckMeanOpacity;
}



double RosselandMeanGasOpacity(double GasTemperature, double GasDensity){
	
	double RosselandMeanOpacity;
	
	//
	// Convert to cgs:
	//
	GasDensity     *= ReferenceDensity;
	GasTemperature *= ReferenceTemperature;
	
	switch(GasOpacityFlag){
		case 0:
			break;
		case 1:
			RosselandMeanOpacity = ConstantGasOpacity;
			break;
		case 2:
			RosselandMeanOpacity = GasOpacity_Hellingetal2000(GasOpacityArrayDensityValues, nx, GasOpacityArrayTemperatureValues, ny, RosselandMeanGasOpacityArray, GasDensity, GasTemperature);
			
			//
			// Convert the opacity from the power exponent to the linear scale:
			//
			RosselandMeanOpacity = pow(10.0, RosselandMeanOpacity);

			break;
		default:
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: GasOpacityFlag = %d is not in allowed range              ###\n", GasOpacityFlag);
			exit(1);
	}
	
	//
	// Convert to code units:
	//
	RosselandMeanOpacity /= ReferenceOpacity;
	
	return RosselandMeanOpacity;
}


