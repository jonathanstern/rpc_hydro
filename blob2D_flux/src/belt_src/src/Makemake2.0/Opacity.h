#ifndef OPACITY_H_
#define OPACITY_H_

extern int DustOpacityFlag;
extern int OpacityExponent;
extern double ConstantDustOpacity;

extern int GasOpacityFlag;
extern double ConstantGasOpacity;

//
// Frequency dependent Opacities:
//
// TODO: should be not required; currently used in Irradiation.c
extern int freq_nr;
extern double *freq_nu;


// ***********************
// ** External Routines **
// ***********************
//
int InitializeOpacity(void);
int FinalizeOpacity(void);

double FrequencyDependentOpacity(double);
double PlanckMeanDustOpacity(double, double, double);
double RosselandMeanDustOpacity(double, double);
double PlanckMeanGasOpacity(double, double, double);
double RosselandMeanGasOpacity(double, double);

#endif /*OPACITY_H_*/
