#ifndef IRRADIATION_H_
#define IRRADIATION_H_

extern int IrradiationFlag;
extern int NumberOfFrequencies;
extern double IrradiationTemperature, IrradiationLuminosity, IrradiationRadius;

// *******************
// * extern Routines *
// *******************
//
int InitializeIrradiation(void);
int FinalizeIrradiation(void);
int Irradiation(Data *, Grid *);

#endif /*IRRADIATION_H_*/
