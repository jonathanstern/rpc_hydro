#ifndef RADIATION_H_
#define RADIATION_H_
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscksp.h>

extern int    RadiationFlag;
extern int    StellarRadiativeForceFlag;
extern int    ThermalRadiativeForceFlag;
extern int    PreconditionerRadiation;
extern double MinimumOpticalDepth;

extern KSPConvergedReason FLD_ConvergedReason;
extern int FLD_Iterations;
extern PetscReal FLD_ResidualNorm;

extern int IrradiationFlag;
extern int NumberOfFrequencies;
extern int StartingFace;
extern double TemperatureCentralObject;
extern double RadiusCentralObject;

extern double TemperatureUpdateConvergenceRTOL;
extern double TemperatureUpdateConvergenceABSTOL;
extern int TemperatureUpdateConvergenceMAXITS;

extern int RadiationPressureFlag;
extern Vec RadiationPressureVector;
extern PetscScalar ***RadiationPressure;

extern int ConvergenceSpecification;
extern double ConvergenceRTOL;
extern double ConvergenceABSTOL;
extern double ConvergenceDTOL;
extern int ConvergenceMAXITS;

extern int BoundaryRadiationEnergyMinX;
extern int BoundaryRadiationEnergyMaxX;
extern int BoundaryRadiationEnergyMinY;
extern int BoundaryRadiationEnergyMaxY;
extern int BoundaryRadiationEnergyMinZ;
extern int BoundaryRadiationEnergyMaxZ;
extern double BoundaryTemperatureDirichlet;

extern PetscScalar *Frequency;



// *******************
// * extern Routines *
// *******************
//
void InitializeRadiation(Grid *);
void FinalizeRadiation(void);
void Radiation(Data *, Grid *, Time_Step *);

#endif /*RADIATION_H_*/
