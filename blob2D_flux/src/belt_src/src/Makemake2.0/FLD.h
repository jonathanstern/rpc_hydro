#ifndef FLD_H_
#define FLD_H_

extern int    PreconditionerRadiation;

//extern KSPConvergedReason FLD_ConvergedReason;
extern int    FLD_ConvergedReason;
extern int    FLD_Iterations;
extern double FLD_ResidualNorm;


//extern int    RadiationPressureFlag;

extern int    RadiationKSPType;
extern int    ConvergenceSpecification;
extern double ConvergenceRTOL;
extern double ConvergenceABSTOL;
extern double ConvergenceDTOL;
extern int    ConvergenceMAXITS;
extern int    ConvergenceMINITS;

extern int    KSPSetLagNormFlag;
extern int    KSPSetDiagonalScaleFlag;

extern int BoundaryRadiationEnergyMinX;
extern int BoundaryRadiationEnergyMaxX;
extern int BoundaryRadiationEnergyMinY;
extern int BoundaryRadiationEnergyMaxY;
extern int BoundaryRadiationEnergyMinZ;
extern int BoundaryRadiationEnergyMaxZ;
extern double BoundaryTemperatureDirichlet;



// *******************
// * extern Routines *
// *******************
//
int InitializeFLD(Grid *);
int FinalizeFLD(void);
int FluxLimitedDiffusion(Data *, Grid *);
int FluxLimitedDiffusion_EquilibriumTemperature(Data *, Grid *);
// TODO: move this general routine to Radiation.c/h
// DONE: JONORBE
//double LimitOpticalDepth(Grid *, int, int, int, double);
//double FluxLimiter(double);

#endif /*FLD_H_*/
