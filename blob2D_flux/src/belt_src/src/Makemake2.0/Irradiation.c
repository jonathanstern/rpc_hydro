#include "pluto.h"
#include "Irradiation.h"
#include "DomainDecomposition.h"
#include "Radiation.h"
#include "Opacity.h"
#include "DustEvolution.h"
#include "PhysicalConstantsCGS.h"
#include "interface.h"
//#include "definitions.h"
#include "MakemakeTools.h"
#include "FLD.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>  // for exit()



//
// Irradiation parameter:
//
int    IrradiationFlag;
int    NumberOfFrequencies;
double IrradiationTemperature, IrradiationLuminosity, IrradiationRadius;

double *Frequency;
double *IrradiationLuminosityPerFrequencyBin;



// ***********************
// ** Internal Routines **
// ***********************
//
double PlanckFunction(double, double);
double IntegratePlanckFunction(double, double, double);
double IntegrateRayleighJeans(double, double, double);
double IntegrateWien(double, double, double);
double IntegrateTaylor(double, double, double);
void   SubdividePlanckFunction(void);
void   IrradiationParallelCommunication(Data *, Grid *);



// ***************************
// ** InitializeRadiation() **
// ***************************
//
int InitializeIrradiation(){
	
	int i;

#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###       ... Initialize Irradiation ...                                     ###\n");
#endif

	switch(IrradiationFlag) {
		case 0:
			break;
		case 1:
			NumberOfFrequencies = 1;
			break;
		case 2:
			if(NumberOfFrequencies == -1) NumberOfFrequencies = freq_nr;
			Frequency                            = malloc(NumberOfFrequencies * sizeof(double));
			IrradiationLuminosityPerFrequencyBin = malloc(NumberOfFrequencies * sizeof(double));
			//
			// In case of frequency-dependent Irradiation and usage of all available frequencies,
			// determine these frequencies and corresponding opacities now during Initialization:
			//
			if(NumberOfFrequencies == freq_nr){				
				if(freq_nu[0] > freq_nu[1]){
					for(i = 0; i < NumberOfFrequencies; i++){
						Frequency[i] = freq_nu[freq_nr-1-i] / ReferenceFrequency;
					}
				}
				else{
					for(i = 0; i < NumberOfFrequencies; i++){
						Frequency[i] = freq_nu[i] / ReferenceFrequency;
					}
				}
			}
			break;
		default:
			PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
			PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
			PetscPrintf(PETSC_COMM_WORLD, "ERROR IrradiationFlag = %d not available, see 'Makemake.conf'.\n", IrradiationFlag);
			PetscPrintf(PETSC_COMM_WORLD, "ERROR\n");
			PetscPrintf(PETSC_COMM_WORLD, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
			exit(1);
	}
	
	return 0;
}



// *************************
// ** FinalizeRadiation() **
// *************************
//
// Frees the memory and destroys the objects allocated during InitializeRadiation().
//
int FinalizeIrradiation(){
		
	if(IrradiationFlag == 2){
		free(Frequency);
		free(IrradiationLuminosityPerFrequencyBin);
	}
	
	return 0;
}



// *******************
// ** Irradiation() **
// *******************
//
// Calculates cell-centered IrradiationEnergy (=> Source term for FLD equation) and
// IrradiationAcceleration (=> to calculate RadiationEnergy and Temperature-Update)
// from face-centered IrradiationFlux:
//
// IrradiationFlux_i = IrradiationFlux_i-1 * Attenuation * Extinction
//  with
//  Attenuation = (R_i-1 / R_i)^2
//  Extinction  = exp(-Tau_i-1)
//         Tau  = rho_i-1 * kappa_i-1 * delta r_i-1
//
int Irradiation(Data *data, Grid *grid){
	
	int FrequencyBin;
	int kl, jl, il;
    int mpierr;
	double Flux, Extinction, Attenuation, DustOpacity, GasOpacity, dtau, tau, kapparho;
	double dx, dAxl, dAxr, dV;
	double rhogas, rhodust, Tgas, Tdust, Trad;
	MPI_Status status;
	MPI_Request request;

#if LOG_OUTPUT != 0
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###       ... Irradiation ...                                                ###\n");
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... IrradiationLuminosity =                  %4.1e Lsol      ###\n", IrradiationLuminosity / SolarLuminosity);
	PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           ... IrradiationTemperature =                 %4.1e K         ###\n", IrradiationTemperature * ReferenceTemperature);
#endif
	
	//
	// In case of frequency-dependent Irradiation split the stellar source function accordingly:
	//
	if(IrradiationFlag == 2) SubdividePlanckFunction();
	
	// ********************************
	// * Radiation transport equation *
	// ********************************
	//
	// Even in parallel mode this is only done in serial mode
	// due to the radial dependence of above formula (IR_flux_i-1).
	//
	KDOM_LOOP(kl){
		JDOM_LOOP(jl){
			IDOM_LOOP(il){
				data->IrradiationPowerDensity[kl][jl][il] = 0;
			}
		}
	}
	
	for(FrequencyBin = 0; FrequencyBin < NumberOfFrequencies; FrequencyBin++){
#if IONIZATION == YES
		// If ionization is considered, Irradiation of frequencies above 13.6 eV is handled in IonizationIrradiation() routine.
		if(Frequency[FrequencyBin] < 13.6 * eV / h_PlanckConstant)
#endif
		KDOM_LOOP(kl){
			JDOM_LOOP(jl){
				
        		if(grid[IDIR].beg == IBEG){
					tau = 0;
					//
					// Determine (frequency dependent) Flux from Central Object at StartingFace:
					//
					if(IrradiationFlag == 1)
						Flux = IrradiationLuminosity;
					else
						Flux = IrradiationLuminosityPerFrequencyBin[FrequencyBin];
                    
					Flux /= 4.0 * M_PI * pow(grid[IDIR].xl[IBEG], 2);
				}
				else{
					//
					// Receive Flux at inner radial boundary from previous processor:
					// Remark: In PETSC_COMM_WORLD/MPI_COMM_WORLD the previous cpu in radial/x - direction is always 'prank-1'.
					//
					mpierr = MPI_Recv(&Flux, 1, MPI_DOUBLE, prank-1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &status);
					mpierr = MPI_Recv(&tau , 1, MPI_DOUBLE, prank-1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &status);
				}
				
				//
				// Calculate (frequency-dependent) Flux through Grid along Rays in X-Direction:
				//
				if(Flux != 0){
					IDOM_LOOP(il){
						
						dx   = GridCellLength(grid, IDIR, kl, jl, il  );
						dAxl = GridCellArea(  grid, IDIR, kl, jl, il  );
						dAxr = GridCellArea(  grid, IDIR, kl, jl, il+1);
						dV   = GridCellVolume(grid,       kl, jl, il  );
						
						rhogas  = data->Vc[RHO][kl][jl][il];
						rhodust = data->DustDensity[kl][jl][il] * rhogas;
						Tgas    = data->GasTemperature[kl][jl][il];
						Tdust   = data->DustTemperature[kl][jl][il];
						Trad    = IrradiationTemperature;
						
						//
						// Determine opacities:
						//
						switch(IrradiationFlag){
							case 1:
								DustOpacity = PlanckMeanDustOpacity(Trad, Tdust, rhodust);
								break;
							case 2:
								DustOpacity = FrequencyDependentOpacity(Frequency[FrequencyBin]);
								break;
							default:
								PetscFPrintf(PETSC_COMM_WORLD, LogFile, "### ERROR: IrradiationFlag = %d is not in allowed range              ###\n", IrradiationFlag);
								exit(1);
						}
						GasOpacity = PlanckMeanGasOpacity(Trad, Tgas, rhogas);
                        
                        kapparho   = DustOpacity * rhodust + GasOpacity  * rhogas;
                        kapparho   = LimitOpticalDepth(grid, kl, jl, il, kapparho);

						dtau = kapparho * dx;
						tau += dtau;
						
						//
						// Lost energy due to extinction (absorption + scattering) along cell length:
						//
						Extinction = exp(-dtau);
						
						//
						// Lost energy density due to larger surface of new interface (geometrical decrease):
						//
						Attenuation = dAxl / dAxr;
#if DEBUGGING > 0
						if(Attenuation < 0.66){
							printf("### WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
							printf("### WARNING \n");
							printf("### WARNING  During Irradiation() step:\n");
							printf("### WARNING  Attenuation = %f < 0.66\n", Attenuation);
							printf("### WARNING  You should check your setup!\n");
							printf("### WARNING  Most probably, higher resolution / less stretching in the x-direction is required.\n");
							printf("### WARNING \n");
							printf("### WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
							exit(1);
						}
#endif
						
						//
						// Store for coupling to thermal radiation transport and hydrodynamics:
						//
						// For highly optically thin grid cells, the exp function below becomes numerically unstable (1-Extinction = 0).
						// See IrradiationPowerDensity.nb
						//                        
						if(dtau < 1e-5)
							data->IrradiationPowerDensity[kl][jl][il] += kapparho * 0.5*Flux*(1.0 + Attenuation); // ~ Flux at cell center (where rho and kappa are defined)
						else
							data->IrradiationPowerDensity[kl][jl][il] += (1.0 - Extinction) * dAxl/dV * Flux;
						
						//
						// Calculate Flux at back face of cell:
						//
						Flux *= Attenuation * Extinction;
												
					} // for(i)
				} // if(Flux != 0){
				//
				// Send Flux at outer radial boundary to next processor:
				// Remark: In PETSC_COMM_WORLD the next cpu in radial/x - direction is always 'prank+1'.
				//
				if(grid[IDIR].end != grid[IDIR].gend){
					mpierr = MPI_Isend(&Flux, 1, MPI_DOUBLE, prank+1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &request);
					mpierr = MPI_Isend(&tau , 1, MPI_DOUBLE, prank+1, kl*grid[JDIR].np_tot_glob+jl, PETSC_COMM_WORLD, &request);
					mpierr = MPI_Wait(&request, &status);
				}
			} // for(j)
		} // for(k)
		//	} // if(prank)
		//} // for(proc)
	} // for (FrequencyBin)
    
    
    //
    // Zero-Gradient Boundary conditions for the x- and y-direction (only used for computing radiative force in outer boundaries):
    //
	KTOT_LOOP(kl){
		JTOT_LOOP(jl){
			IBEG_LOOP(il){
				data->IrradiationPowerDensity[kl][jl][il] = data->IrradiationPowerDensity[kl][jl][IBEG];
			}
		}
	}
	KTOT_LOOP(kl){
		JTOT_LOOP(jl){
			IEND_LOOP(il){
				data->IrradiationPowerDensity[kl][jl][il] = data->IrradiationPowerDensity[kl][jl][IEND];
			}
		}
	}
#if DIMENSIONS > 1
	KTOT_LOOP(kl){
		JBEG_LOOP(jl){
			ITOT_LOOP(il){
				data->IrradiationPowerDensity[kl][jl][il] = data->IrradiationPowerDensity[kl][JBEG][il];
			}
		}
	}
	KTOT_LOOP(kl){
		JEND_LOOP(jl){
			ITOT_LOOP(il){
				data->IrradiationPowerDensity[kl][jl][il] = data->IrradiationPowerDensity[kl][JEND][il];
			}
		}
	}
#endif
    
    //
    // Parallel communication:
    //
#ifdef PARALLEL
    IrradiationParallelCommunication(data, grid);
#endif
	
	return 0;
}



// *******************************
// ** SubdividePlanckFunction() **
// *******************************
//
// Determines in dependence of number of chosen frequencies a subdivision of the Planck spectrum of the central object.
//
void SubdividePlanckFunction(){
	
	int i;
	double xconst, BlackBodyPeakFrequency, EnergyDensityPerFrequencyBin, FrequencyMin, FrequencyMax, SurfaceEnergy, fl, fr;
	
	//
	// Build Frequency-grid
	// (if number of frequency bins != number of available frequencies in the used table,
	// otherwise this was already done during InitializeOpacity())
	//
	if(NumberOfFrequencies != freq_nr){

		//
		// Determine Frequency at peak of Planck Function (Wien Law):
		//
		xconst = 2.8214393721;
		BlackBodyPeakFrequency = xconst * k_BoltzmannConstant * IrradiationTemperature /	h_PlanckConstant;
		//printf("BlackBodyPeakFrequency = %e Hz\n", BlackBodyPeakFrequency*ReferenceFrequency);
		
		//
		// Split the Frequency domain in Frequency bins:
		//
		EnergyDensityPerFrequencyBin = a_RadiationConstant * pow(IrradiationTemperature, 4) / NumberOfFrequencies;
		
		// only used, if not all frequency bins are in use
		FrequencyMin = 0.1 * BlackBodyPeakFrequency;
		FrequencyMax = 3.0 * BlackBodyPeakFrequency;
		
		Frequency[0] = FrequencyMin;
		for(i = 1; i < NumberOfFrequencies; i++){
			double guess =
				0.5
				*
				(
				 0.5*(PlanckFunction(IrradiationTemperature, BlackBodyPeakFrequency) + PlanckFunction(IrradiationTemperature, Frequency[i-1]))
				 +
				 PlanckFunction(IrradiationTemperature, FrequencyMin + (FrequencyMax-FrequencyMin) / NumberOfFrequencies)
			 	);
			
			Frequency[i] =
				Frequency[i-1]
				+
				2.0 * EnergyDensityPerFrequencyBin
				/
				(
				 PlanckFunction(IrradiationTemperature, Frequency[i-1])
				 +
				 guess
			 	);
		}
		
	}
	
	
	//
	// Calculate corresponding Surface Luminosities of the central object in the bins:
	//
	for(i = 0; i < NumberOfFrequencies; i++){
		if(i == 0){
			fl = 1e-2*Frequency[0];
			fr = 0.5 * (Frequency[0] + Frequency[1]);
		}
		else if(i == NumberOfFrequencies - 1){
			fl = 0.5 * (Frequency[NumberOfFrequencies - 1] + Frequency[NumberOfFrequencies - 2]);
			fr = 1e+1 * Frequency[NumberOfFrequencies - 1];
		}
		else{
			fl = 0.5 * (Frequency[i] + Frequency[i-1]);
			fr = 0.5 * (Frequency[i] + Frequency[i+1]);
		}
		
		SurfaceEnergy = IntegratePlanckFunction(IrradiationTemperature, fl, fr);
		IrradiationLuminosityPerFrequencyBin[i] = M_PI * c_SpeedOfLight * SurfaceEnergy * pow(IrradiationRadius, 2);
	}
	
	//
	// Write summary table of Frequency dependence:
	//
#if LOG_OUTPUT != 0
		if(g_stepNumber == 0){
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###                                                                          ###\n");
			
//			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           Mean Energy per Frequency Bin : %e                   ###\n", EnergyDensityPerFrequencyBin);
//			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           Equipartition of Total Energy : %14.4f                 ###\n", Sigma);
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###                                                                          ###\n");
			
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###           Frequency [Hz]    Luminosity [Lsol]    Opacity [cm^2 g^-1]     ###\n");
			for(i = 0; i < NumberOfFrequencies; i++){
				PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###             %9.2e         %9.2e             %4.2e             ###\n", Frequency[i] / ReferenceTime, IrradiationLuminosityPerFrequencyBin[i] / SolarLuminosity, FrequencyDependentOpacity(Frequency[i]) * ReferenceOpacity);
			}
			PetscFPrintf(PETSC_COMM_WORLD, LogFile, "###                                                                          ###\n");
		}
#endif
	
	
	
#if DEBUGGING > 0
	//
	// Check nan:
	//
	for(i = 0; i < NumberOfFrequencies; i++){
		if(isnan(IrradiationLuminosityPerFrequencyBin[i])){
			printf("IrradiationLuminosityPerFrequencyBin[%d] = nan\n", i);
			printf(" IrradiationRadius = %e Rsol\n", IrradiationRadius / SolarRadius);
			printf(" ReferenceLength = %e cm\n", ReferenceLength);
			printf(" SolarRadius = %e cm\n", SolarRadius * ReferenceLength);
            exit(1);
		}
	}
	
	//
	// Check IrradiationLuminosityPerFrequencyBin <= 0:
	//
	for(i = 0; i < NumberOfFrequencies; i++){
		if(IrradiationLuminosityPerFrequencyBin[i] < 0){
			printf("IrradiationLuminosityPerFrequencyBin[%d] = %e < 0\n", i, IrradiationLuminosityPerFrequencyBin[i]);
			exit(1);
		}
	}
	
	//
	// Check integration method and coverage of frequency spectrum (for deviations greater than 0.01%):
	//
	double sum, deviation;
	sum = 0;
	for(i = 0; i < NumberOfFrequencies; i++){
		sum += IrradiationLuminosityPerFrequencyBin[i];
	}
	//deviation = fabs(a_RadiationConstant * pow(IrradiationTemperature, 4) - sum) / (a_RadiationConstant * pow(IrradiationTemperature, 4));
	deviation = fabs(IrradiationLuminosity - sum) / IrradiationLuminosity;
	if(deviation > 1e-4){
		PetscPrintf(PETSC_COMM_WORLD, "\n");
		PetscPrintf(PETSC_COMM_WORLD, "WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		PetscPrintf(PETSC_COMM_WORLD, "WARNING ###############################################\n");
		PetscPrintf(PETSC_COMM_WORLD, "WARNING ###\n"                                            );
		PetscPrintf(PETSC_COMM_WORLD, "WARNING ###  Deviation of sum of frequency dependent   \n");
		PetscPrintf(PETSC_COMM_WORLD, "WARNING ###  luminosity bins and total luminosity.     \n");
		PetscPrintf(PETSC_COMM_WORLD, "WARNING ###  Deviation = %.2e\n", deviation);
		//PetscPrintf(PETSC_COMM_WORLD, "WARNING ###  Iteration = %d\n", Iteration                 );
		PetscPrintf(PETSC_COMM_WORLD, "WARNING ###\n"                                            );
		PetscPrintf(PETSC_COMM_WORLD, "WARNING ###############################################\n");
		PetscPrintf(PETSC_COMM_WORLD, "WARNING WARNING WARNING WARNING WARNING WARNING WARNING\n");
		PetscPrintf(PETSC_COMM_WORLD, "\n");
		exit(1);
	}
#endif
	
}



// **********************
// ** PlanckFunction() **
// **********************
//
// Returns Energy in code units.
//
double PlanckFunction(double Temperature, double Frequency){
	
	return
	8.0 * M_PI * h_PlanckConstant / pow(c_SpeedOfLight, 3)
	*
	pow(Frequency, 3)
	/
	(
	 exp(h_PlanckConstant * Frequency / (k_BoltzmannConstant * Temperature))
	 -
	 1
	 );
	
}



// *******************************
// ** IntegratePlanckFunction() **
// *******************************
//
// Calculates an approximation of the integral of the Planckfunction (to specified temperature)
// from given minimum to maximum frequency.
// Uses Rayleigh-Jeans and Wien approximation (see IntegrateRayleighJeans() and IntegrateWien()).
//
double IntegratePlanckFunction(double Temperature, double MinimumFrequency, double MaximumFrequency){
	
	int    i, steps;
	double Integral, Frequency_limit, TaylorWienBorder, TaylorStart, TaylorEnd, WienStart, WienEnd;
	Integral = 0;
	
	//
	// Determine Rayleigh-Jeans and Wien Limits:
	//
	//                    k*T
	// Frequency_limit = -----
	//                     h
	//
	Frequency_limit = k_BoltzmannConstant * Temperature / h_PlanckConstant;
	TaylorWienBorder = 6.0*Frequency_limit;
	
	//
	// Divide the frequency intervall in Taylor and Wien regime:
	//
	TaylorStart = MIN(MinimumFrequency, TaylorWienBorder);
	TaylorEnd   = MIN(MaximumFrequency, TaylorWienBorder);
	WienStart   = MAX(MinimumFrequency, TaylorWienBorder);
	WienEnd     = MAX(MaximumFrequency, TaylorWienBorder);
	
	//Integral += IntegrateRayleighJeans(Temperature, MinimumFrequency, 0.1*Frequency_limit);
	
	//
	// Integrate the Taylor regime:
	//
	if(TaylorStart != TaylorEnd){
		steps = 20;
		
		for(i = 0; i < steps; i++){
			Integral += IntegrateTaylor(Temperature, TaylorStart + i * (TaylorEnd-TaylorStart) / steps, TaylorStart + (i+1) * (TaylorEnd-TaylorStart) / steps);
		}
	}
	
	//
	// Integrate the Wien regime:
	//
	if(WienStart != WienEnd)
		Integral += IntegrateWien(Temperature, WienStart, WienEnd);
	
	return Integral;
}



// ******************************
// ** IntegrateRayleighJeans() **
// ******************************
//
// Approximate analytical solution of the Rayleigh-Jean regime (h*nu << k*T) of the Planck function.
//
double IntegrateRayleighJeans(double Temperature, double MinimumFrequency, double MaximumFrequency){
	return
	8.0 * M_PI * k_BoltzmannConstant * Temperature
	/
	(3.0 * pow(c_SpeedOfLight, 3))
	*
	(pow(MaximumFrequency, 3) - pow(MinimumFrequency, 3));
}



// *********************
// ** IntegrateWien() **
// *********************
//
// Approximate analytical solution of the Wien regime (h*nu >> k*T) of the Planck function.
//
double IntegrateWien(double Temperature, double MinimumFrequency, double MaximumFrequency){
	return
	8.0 * M_PI * k_BoltzmannConstant * Temperature
	/
	pow(c_SpeedOfLight, 3)
	*
	(
	 exp(- h_PlanckConstant * MinimumFrequency / (k_BoltzmannConstant * Temperature))
	 *
	 (
	  pow(MinimumFrequency, 3)
	  +
	  3 * k_BoltzmannConstant * Temperature / h_PlanckConstant * pow(MinimumFrequency, 2)
	  +
	  6 * pow(k_BoltzmannConstant * Temperature / h_PlanckConstant, 2) * MinimumFrequency
	  +
	  6 * pow(k_BoltzmannConstant * Temperature / h_PlanckConstant, 3)
	  )
	 -
	 exp(- h_PlanckConstant * MaximumFrequency / (k_BoltzmannConstant * Temperature))
	 *
	 (
	  pow(MaximumFrequency, 3)
	  +
	  3 * k_BoltzmannConstant * Temperature / h_PlanckConstant * pow(MaximumFrequency, 2)
	  +
	  6 * pow(k_BoltzmannConstant * Temperature / h_PlanckConstant, 2) * MaximumFrequency
	  +
	  6 * pow(k_BoltzmannConstant * Temperature / h_PlanckConstant, 3)
	  )
	 );
}



// ***********************
// ** IntegrateTaylor() **
// ***********************
//
// 2nd oder Taylor expansion of the Planck function
//
double IntegrateTaylor(double Temperature, double MinimumFrequency, double MaximumFrequency){
	
	double Integral, TaylorSamplingPoint, Normalization, t, a, b;
	
	TaylorSamplingPoint = 0.5 * (MinimumFrequency + MaximumFrequency);
	
	//
	// Define some abbreviations:
	//
	Normalization = 8.0 * M_PI * h_PlanckConstant / pow(c_SpeedOfLight, 3);
	t = h_PlanckConstant / (k_BoltzmannConstant * Temperature);
	a = exp(t * TaylorSamplingPoint);
	b = a - 1;
	
	Integral = 0;

	//
	// 0. order:
	//
	Integral +=
	pow(TaylorSamplingPoint, 3) / b
	*
	(
	 MaximumFrequency
	 -
	 MinimumFrequency
	 );
	
	//
	// 1. order:
	//
	Integral +=
	(
	 3 * pow(TaylorSamplingPoint, 2) / b
	 -
	 pow(TaylorSamplingPoint, 3) * t * a / pow(b, 2)
	 )
	*
	0.5
	*
	(
	 pow(MaximumFrequency - TaylorSamplingPoint, 2)
	 -
	 pow(MinimumFrequency - TaylorSamplingPoint, 2)
	 );
	
	//
	// 2. order:
	//
	Integral +=
	0.5
	*
	(
	 6 * TaylorSamplingPoint / b
	 -
	 6 * pow(TaylorSamplingPoint, 2) * t * a / pow(b, 2)
	 -
	 pow(TaylorSamplingPoint, 3) * pow(t, 2) * a * (b-2*a) / pow(b, 3)
	 )
	*
	1.0 / 3.0
	*
	(
	 pow(MaximumFrequency - TaylorSamplingPoint, 3)
	 -
	 pow(MinimumFrequency - TaylorSamplingPoint, 3)
	 );
	
	//
	// Normalization:
	//
	Integral *= Normalization;
	
	return Integral;
}



void IrradiationParallelCommunication(Data *data, Grid *grid){
    
    int par_dim[3] = {0, 0, 0};
    
    //
    // Check the number of processors in each direction:
    //
    D_EXPAND(par_dim[0] = grid[IDIR].nproc > 1;  ,
             par_dim[1] = grid[JDIR].nproc > 1;  ,
             par_dim[2] = grid[KDIR].nproc > 1;)

#if PERIODICX == 1
        if(par_dim[0] == 0) par_dim[0] = 1;
#endif
#if PERIODICY == 1
        if(par_dim[1] == 0) par_dim[1] = 1;
#endif
#if PERIODICZ == 1
        if(par_dim[2] == 0) par_dim[2] = 1;
#endif 
    
    //
    // Exchange data between parallel processors:
    //
    MPI_Barrier(MPI_COMM_WORLD);
    
    AL_Exchange_dim((char *) data->IrradiationPowerDensity[0][0], par_dim, SZ);
    
    MPI_Barrier(MPI_COMM_WORLD);
    
}


