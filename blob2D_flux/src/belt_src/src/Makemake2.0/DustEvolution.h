#ifndef DUSTEVOLUTION_H_
#define DUSTEVOLUTION_H_

extern int DustEvaporationFlag;
extern double Dust2GasMassRatio;


void DustEvolution(Data *, Grid *);

//
// To test the routines in init.c:
//
double EvaporationTemperature(double);

#endif /*DUSTEVOLUTION_H_*/
