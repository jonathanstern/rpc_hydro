/* ///////////////////////////////////////////////////////////////////// */
/*!
  \file
  \brief Miscellaneous functions for handling 3D tables.

  \author J. Onorbe (jose.onnorbe@gmail.com)
  \date   Mar 14, 2016
*/
/* ///////////////////////////////////////////////////////////////////// */
#include "pluto.h"

/* ********************************************************************* */
void InitializeTable3D (Table3D *tab, double xmin, double xmax, int nx,
                                      double ymin, double ymax, int ny,
                                      double zmin, double zmax, int nz)
{
  int i,j,k;

  tab->nx = nx;
  tab->ny = ny;
  tab->nz = nz;
    
/* ---------------------------------------------------------------------- 
   If you do not initialize a structure variable, the effect depends on 
   whether it is has static storage (see Storage Class Specifiers) or 
   not. 
   If it is, members with integral types are initialized with 0 and 
   pointer  members are initialized to NULL; otherwise, the value of 
   the structure's members is indeterminate.
  ----------------------------------------------------------------------- */

  tab->f       = ARRAY_3D(tab->nz, tab->ny, tab->nx, double);
  tab->defined = ARRAY_3D(tab->nz, tab->ny, tab->nx, char);
  for (i = 0; i < tab->nx; i++){
    for (j = 0; j < tab->ny; j++){
        for (k = 0; k < tab->nz; k++){
            tab->f[k][j][i] = 0.0;
            tab->defined[k][j][i] = 1;
        }
    }
  }

  tab->lnx  = ARRAY_1D(tab->nx, double);
  tab->lny  = ARRAY_1D(tab->ny, double);
  tab->lnz  = ARRAY_1D(tab->nz, double);

  tab->x    = ARRAY_1D(tab->nx, double);
  tab->y    = ARRAY_1D(tab->ny, double);
  tab->z    = ARRAY_1D(tab->nz, double);

  tab->dx   = ARRAY_1D(tab->nx, double);
  tab->dy   = ARRAY_1D(tab->ny, double);
  tab->dz   = ARRAY_1D(tab->nz, double);


/* ---------------------------------------------------------------
    Compute table bounds 
   --------------------------------------------------------------- */

  tab->xmin = xmin;
  tab->xmax = xmax;
  tab->ymin = ymin;
  tab->ymax = ymax;
  tab->zmin = zmin;
  tab->zmax = zmax;

  tab->lnxmin = log10(xmin);
  tab->lnxmax = log10(xmax);
  tab->lnymin = log10(ymin);
  tab->lnymax = log10(ymax);
  tab->lnzmin = log10(zmin);
  tab->lnzmax = log10(zmax);
  
  tab->dlnx = (tab->lnxmax - tab->lnxmin)/(double)(tab->nx - 1.0);
  tab->dlny = (tab->lnymax - tab->lnymin)/(double)(tab->ny - 1.0);
  tab->dlnz = (tab->lnzmax - tab->lnzmin)/(double)(tab->nz - 1.0);

  tab->dlnx_1 = 1.0/tab->dlnx;
  tab->dlny_1 = 1.0/tab->dlny;
  tab->dlnz_1 = 1.0/tab->dlnz;

  for (i = 0; i < tab->nx; i++) {
    tab->lnx[i] = tab->lnxmin + i*tab->dlnx;
    tab->x[i]   = pow(10.0, tab->lnx[i]);
  }
  for (j = 0; j < tab->ny; j++) {
    tab->lny[j] = tab->lnymin + j*tab->dlny;
    tab->y[j]   = pow(10.0, tab->lny[j]);
  }
  for (k = 0; k < tab->nz; k++) {
    tab->lnz[k] = tab->lnzmin + k*tab->dlnz;
    tab->z[k]   = pow(10.0, tab->lnz[k]);
  }

/* -- table will not be uniform in x y and z: compute spacings -- */

  for (i = 0; i < tab->nx-1; i++) tab->dx[i] = tab->x[i+1] - tab->x[i];
  for (j = 0; j < tab->ny-1; j++) tab->dy[j] = tab->y[j+1] - tab->y[j];
  for (k = 0; k < tab->nz-1; k++) tab->dz[k] = tab->z[k+1] - tab->z[k];

}

#define LOG_INTERPOLATION NO
/* ********************************************************************* */
int Table3DInterpolate(Table3D *tab, double x, double y, double z, double *f)
/*!
 * Use trilinear interpolation to find the function f(x,y,z) from the 
 * 3D table \c *tab.
 *********************************************************************** */
{
  int i,j,k;
  double lnx, lny, lnz, xn, yn, zn;
  double f00,f01,f10,f11,f0,f1;
  
  lnx = log10(x);  /* Take the log to find the indices */
  lny = log10(y);
  lnz = log10(z);

/* -------------------------------------------------------
    Check bounds 
   ------------------------------------------------------- */

  if (lnx < tab->lnxmin){
    WARNING(
      print ("! Table3DInterpolate: lnx outside range %12.6e < %12.6e\n",
              lnx, tab->lnxmin);
    )
    return -1;
  }
   
  if (lnx > tab->lnxmax){
    WARNING(
      print ("! Table3DInterpolate: lnx outside range: %12.6e > %12.6e\n",
              lnx, tab->lnxmax);
    )
    return 1;
  }

  if (lny < tab->lnymin){
    WARNING(
      print ("! Table3DInterpolate: lny outside range: %12.6e < %12.6e\n",
              lny, tab->lnymin);
    )
    return -2;
  }

  if (lny > tab->lnymax){
    WARNING(
      print ("! Table3DInterpolate: lny outside range: %12.6e > %12.6e\n",
              lny, tab->lnymax);
    )
    return 2;
  }

  if (lnz < tab->lnzmin){
    WARNING(
      print ("! Table3DInterpolate: lnz outside range %12.6e < %12.6e\n",
              lnz, tab->lnzmin);
    )
    return -3;
  }

  if (lnz > tab->lnzmax){
    WARNING(
      print ("! Table3DInterpolate: lnz outside range %12.6e > %12.6e\n",
              lnz, tab->lnzmax);
    )
    return 3;
  }

/* ------------------------------------------------
    Find column and row indices i and j 
   ------------------------------------------------ */
   
  i  = INT_FLOOR((lnx - tab->lnxmin)*tab->dlnx_1); 
  j  = INT_FLOOR((lny - tab->lnymin)*tab->dlny_1); 
  k  = INT_FLOOR((lnz - tab->lnzmin)*tab->dlnz_1); 

  #if LOG_INTERPOLATION == YES
   xn = (lnx - tab->lnx[i])/tab->dlnx;  /* Compute normalized log coordinates */ 
   yn = (lny - tab->lny[j])/tab->dlny;  /* on the unit square [0,1]x[0,1]    */
   zn = (lnz - tab->lnz[k])/tab->dlnz;  
  #else
   xn = (x - tab->x[i])/tab->dx[i];  /* Compute normalized linear coordinates */ 
   yn = (y - tab->y[j])/tab->dy[j];  /* on the unit square [0,1]x[0,1]        */
   zn = (z - tab->z[k])/tab->dz[k]; 
  #endif
  /*if (tab->f[k][j][i]<0 || tab->f[k][j][i+1]<0 || tab->f[k][j+1][i]<0 || tab->f[k][j+1][i+1]<0 ||
      tab->f[k+1][j][i]<0 || tab->f[k+1][j][i+1]<0 || tab->f[k+1][j+1][i]<0 || tab->f[k+1][j+1][i+1]<0) {
      print("Error 3D table i=%d, j=%d, k=%d l=%d \n",i,j,k,k*tab->ny*tab->nx+j*tab->nx+i);
      print("l1=%d, l2=%d, l3=%d l4=%d \n",k*tab->ny*tab->nx+j*tab->nx+i,k*tab->ny*tab->nx+j*tab->nx+i+1, 
              k*tab->ny*tab->nx+(j+1)*tab->nx+i, k*tab->ny*tab->nx+(j+1)*tab->nx + i+1);
      print("l5=%d, l6=%d, l7=%d l8=%d \n",(k+1)*tab->ny*tab->nx+j*tab->nx+i,(k+1)*tab->ny*tab->nx+j*tab->nx+i+1,
              (k+1)*tab->ny*tab->nx+(j+1)*tab->nx+i, (k+1)*tab->ny*tab->nx+(j+1)*tab->nx+i+1);
      print("%e %e %e %e\n",tab->f[k][j][i],tab->f[k][j][i+1],tab->f[k][j+1][i],tab->f[k][j+1][i+1]);
      print("%e %e %e %e\n",tab->f[k+1][j][i],tab->f[k+1][j][i+1],tab->f[k+1][j+1][i],tab->f[k+1][j+1][i+1]);
      print("Error 3D table x=%e, y=%e, z=%e -> value -1\n",x,y,z);
      exit(-1);
    }*/
   
   f00=tab->f[k][j][i]*(1-xn) + tab->f[k][j][i+1]*xn;
   f01=tab->f[k][j+1][i]*(1-xn) + tab->f[k][j+1][i+1]*xn;
   f10=tab->f[k+1][j][i]*(1-xn) + tab->f[k+1][j][i+1]*xn;
   f11=tab->f[k+1][j+1][i]*(1-xn) + tab->f[k+1][j+1][i+1]*xn;
   
   f0=f00*(1.-yn)+f01*yn;
   f1=f10*(1.-yn)+f11*yn;

  (*f) = f0*(1-zn)+f1*zn;
  return 0;  /* success */
}

/* ********************************************************************* */
void WriteBinaryTable3D (char *fname, Table3D *tab)
/*! 
 *  The binary table is a compact format used to write a 3D array 
 *  together with simple structured coordinates.
 *  The file consists of the following information:
 *  \verbatim
     nx
     ny
     nz
     <x[0]..x[nx-1]>
     <y[0]..y[ny-1]>
     <z[0]..z[nz-1]>
     ...........
     <q[0][0][0]..q[0][nx-1]
      q[0][1][0]..q[1][nx-1]
        .......
      q[0][ny-1][0]..q[0][ny-1][nx-1]>
      ................
      q[nz-1][ny-1][0]..q[nz-1][ny-1][nx-1]>
    \endverbatim
 *  All fields are written in binary format using double precision
 *  arithmetic.
 *
 *********************************************************************** */
{
  int    j,k;
  double scrh;
  FILE *fp;
  
  fp = fopen(fname,"wb");
 
  scrh = (double)tab->nx;
  fwrite(&scrh, sizeof(double), 1, fp);
  
  scrh = (double)tab->ny;
  fwrite(&scrh, sizeof(double), 1, fp);

  scrh = (double)tab->nz;
  fwrite(&scrh, sizeof(double), 1, fp);
  
  fwrite(tab->lnx, sizeof(double), tab->nx, fp);
  fwrite(tab->lny, sizeof(double), tab->ny, fp);
  fwrite(tab->lnz, sizeof(double), tab->nz, fp);
  
  for (k = 0; j < tab->nz; k++){
   for (j = 0; j < tab->ny; j++){
     fwrite (tab->f[k][j], sizeof(double), tab->nx, fp);
  }}
  
  fprintf (fp,"\n");
  fclose(fp);
}
