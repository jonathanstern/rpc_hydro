/* ///////////////////////////////////////////////////////////////////// */
/*!
  \file
  \brief Miscellaneous functions for handling 4D tables.

  \author J. Onorbe (jose.onnorbe@gmail.com)
  \date   Mar 14, 2016
*/
/* ///////////////////////////////////////////////////////////////////// */
#include "pluto.h"

/* ********************************************************************* */
void InitializeTable4D (Table4D *tab, double wmin, double wmax, int nw,
                                      double xmin, double xmax, int nx,
                                      double ymin, double ymax, int ny,
                                      double zmin, double zmax, int nz)
{
  int i,j,k,l;

  tab->nw = nw;
  tab->nx = nx;
  tab->ny = ny;
  tab->nz = nz;
    
/* ---------------------------------------------------------------------- 
   If you do not initialize a structure variable, the effect depends on 
   whether it is has static storage (see Storage Class Specifiers) or 
   not. 
   If it is, members with integral types are initialized with 0 and 
   pointer  members are initialized to NULL; otherwise, the value of 
   the structure's members is indeterminate.
  ----------------------------------------------------------------------- */

  tab->f       = ARRAY_4D(tab->nz, tab->ny, tab->nx, tab->nw, double);
  //tab->defined = ARRAY_4D(tab->nz, tab->ny, tab->nx, tab->nw, char);

  for (i = 0; i < tab->nw; i++){
   for (j = 0; j < tab->nx; j++){
     for (k = 0; k < tab->ny; k++){
        for (l = 0; l < tab->nz; l++){
            tab->f[l][k][j][i] = 0.0;
            //tab->defined[l][k][j][i] = 1;
  }}}}

  tab->lnw  = ARRAY_1D(tab->nw, double);
  tab->lnx  = ARRAY_1D(tab->nx, double);
  tab->lny  = ARRAY_1D(tab->ny, double);
  tab->lnz  = ARRAY_1D(tab->nz, double);

  tab->w    = ARRAY_1D(tab->nw, double);
  tab->x    = ARRAY_1D(tab->nx, double);
  tab->y    = ARRAY_1D(tab->ny, double);
  tab->z    = ARRAY_1D(tab->nz, double);

  tab->dw   = ARRAY_1D(tab->nw, double);
  tab->dx   = ARRAY_1D(tab->nx, double);
  tab->dy   = ARRAY_1D(tab->ny, double);
  tab->dz   = ARRAY_1D(tab->nz, double);


/* ---------------------------------------------------------------
    Compute table bounds 
   --------------------------------------------------------------- */

  tab->wmin = wmin;
  tab->wmax = wmax;
  tab->xmin = xmin;
  tab->xmax = xmax;
  tab->ymin = ymin;
  tab->ymax = ymax;
  tab->zmin = zmin;
  tab->zmax = zmax;

  tab->lnwmin = log10(wmin);
  tab->lnwmax = log10(wmax);
  tab->lnxmin = log10(xmin);
  tab->lnxmax = log10(xmax);
  tab->lnymin = log10(ymin);
  tab->lnymax = log10(ymax);
  tab->lnzmin = log10(zmin);
  tab->lnzmax = log10(zmax);
  
  tab->dlnw = (tab->lnwmax - tab->lnwmin)/(double)(tab->nw - 1.0);
  tab->dlnx = (tab->lnxmax - tab->lnxmin)/(double)(tab->nx - 1.0);
  tab->dlny = (tab->lnymax - tab->lnymin)/(double)(tab->ny - 1.0);
  tab->dlnz = (tab->lnzmax - tab->lnzmin)/(double)(tab->nz - 1.0);

  tab->dlnw_1 = 1.0/tab->dlnw;
  tab->dlnx_1 = 1.0/tab->dlnx;
  tab->dlny_1 = 1.0/tab->dlny;
  tab->dlnz_1 = 1.0/tab->dlnz;

  for (i = 0; i < tab->nw; i++) {
    tab->lnw[i] = tab->lnwmin + i*tab->dlnw;
    tab->w[i]   = pow(10.0, tab->lnw[i]);
  }

  for (j = 0; j < tab->nx; j++) {
    tab->lnx[j] = tab->lnxmin + j*tab->dlnx;
    tab->x[j]   = pow(10.0, tab->lnx[j]);
  }

  for (k = 0; k < tab->ny; k++) {
    tab->lny[k] = tab->lnymin + k*tab->dlny;
    tab->y[k]   = pow(10.0, tab->lny[k]);
  }
  for (l = 0; l < tab->nz; l++) {
    tab->lnz[l] = tab->lnzmin + l*tab->dlnz;
    tab->z[l]   = pow(10.0, tab->lnz[l]);
  }

/* -- table will not be uniform in w x y and z: compute spacings -- */

  for (i = 0; i < tab->nw-1; i++) tab->dw[i] = tab->w[i+1] - tab->w[i];
  for (j = 0; j < tab->nx-1; j++) tab->dx[j] = tab->x[j+1] - tab->x[j];
  for (k = 0; k < tab->ny-1; k++) tab->dy[k] = tab->y[k+1] - tab->y[k];
  for (l = 0; l < tab->nz-1; l++) tab->dz[l] = tab->z[l+1] - tab->z[l];

}
/* ********************************************************************* */

#define LOG_INTERPOLATION NO
/* ********************************************************************* */
int Table4DInterpolate(Table4D *tab, double w, double x, double y, double z, double *f)
/*!
 * Use trilinear interpolation to find the function f(w,x,y,z) from the 
 * 4D table \c *tab.
 *********************************************************************** */
{
  int h,i,j,k;
  double lnw, lnx, lny, lnz, wn, xn, yn, zn;
  double f000,f001,f010,f011,f100,f101,f110,f111;
  double f00,f01,f10,f11,f0,f1;
  
  lnw = log10(w);  /* Take the log to find the indices */
  lnx = log10(x);  
  lny = log10(y);
  lnz = log10(z);

/* -------------------------------------------------------
    Check bounds 
   ------------------------------------------------------- */

  if (lnw < tab->lnwmin){
    WARNING(
      print ("! Table4DInterpolate: lnw outside range %12.6e < %12.6e\n",
              lnw, tab->lnwmin);
    )
    return -4;
  }

  if (lnw > tab->lnwmax){
    WARNING(
      print ("! Table4DInterpolate: lnw outside range %12.6e > %12.6e\n",
              lnw, tab->lnwmax);
    )
    return 4;
  }

  if (lnx < tab->lnxmin){
    WARNING(
      print ("! Table4DInterpolate: lnx outside range %12.6e < %12.6e\n",
              lnx, tab->lnxmin);
    )
    return -1;
  }
   
  if (lnx > tab->lnxmax){
    WARNING(
      print ("! Table4DInterpolate: lnx outside range: %12.6e > %12.6e\n",
              lnx, tab->lnxmax);
    )
    return 1;
  }

  if (lny < tab->lnymin){
    WARNING(
      print ("! Table4DInterpolate: lny outside range: %12.6e < %12.6e\n",
              lny, tab->lnymin);
    )
    return -2;
  }

  if (lny > tab->lnymax){
    WARNING(
      print ("! Table4DInterpolate: lny outside range: %12.6e > %12.6e\n",
              lny, tab->lnymax);
    )
    return 2;
  }

  if (lnz < tab->lnzmin){
    WARNING(
      print ("! Table4DInterpolate: lnz outside range %12.6e < %12.6e\n",
              lnz, tab->lnzmin);
    )
    return -3;
  }

  if (lnz > tab->lnzmax){
    WARNING(
      print ("! Table4DInterpolate: lnz outside range %12.6e > %12.6e\n",
              lnz, tab->lnzmax);
    )
    return 3;
  }

/* ------------------------------------------------
    Find column and row indices i and j 
   ------------------------------------------------ */
   
  h  = INT_FLOOR((lnw - tab->lnwmin)*tab->dlnw_1); 
  i  = INT_FLOOR((lnx - tab->lnxmin)*tab->dlnx_1); 
  j  = INT_FLOOR((lny - tab->lnymin)*tab->dlny_1); 
  k  = INT_FLOOR((lnz - tab->lnzmin)*tab->dlnz_1); 

  #if LOG_INTERPOLATION == YES
   wn = (lnw - tab->lnw[h])/tab->dlnw;  /* Compute normalized log coordinates */ 
   xn = (lnx - tab->lnx[i])/tab->dlnx;   
   yn = (lny - tab->lny[j])/tab->dlny;  /* on the unit square [0,1]x[0,1]    */
   zn = (lnz - tab->lnz[k])/tab->dlnz;  
  #else
   wn = (w - tab->w[h])/tab->dw[h];  /* Compute normalized linear coordinates */ 
   xn = (x - tab->x[i])/tab->dx[i];  
   yn = (y - tab->y[j])/tab->dy[j];  /* on the unit square [0,1]x[0,1]        */
   zn = (z - tab->z[k])/tab->dz[k]; 
  #endif

  double ccc=-0.99;
  if (tab->f[k][j][i][h]<ccc || tab->f[k][j][i][h+1]<ccc || tab->f[k][j][i+1][h]<ccc || tab->f[k][j][i+1][h+1]<ccc ||
      tab->f[k][j+1][i][h]<ccc || tab->f[k][j+1][i][h+1]<ccc || tab->f[k][j+1][i+1][h]<ccc || tab->f[k][j+1][i+1][h+1]<ccc ||
      tab->f[k+1][j][i][h]<ccc || tab->f[k+1][j][i][h+1]<ccc || tab->f[k+1][j][i+1][h]<ccc || tab->f[k+1][j][i+1][h+1]<ccc ||
      tab->f[k+1][j+1][i][h]<ccc || tab->f[k+1][j+1][i][h+1]<ccc || tab->f[k+1][j+1][i+1][h]<ccc || tab->f[k+1][j+1][i+1][h+1]<ccc) {
      print("%e %e %e %e\n",tab->f[k][j][i][h],tab->f[k][j][i][h+1],tab->f[k][j][i+1][h],tab->f[k][j][i+1][h+1]);
      print("%e %e %e %e\n",tab->f[k][j+1][i][h],tab->f[k][j+1][i][h+1],tab->f[k][j+1][i+1][h],tab->f[k][j+1][i+1][h+1]);
      print("%e %e %e %e\n",tab->f[k+1][j][i][h],tab->f[k+1][j][i][h+1],tab->f[k+1][j][i+1][h],tab->f[k+1][j][i+1][h+1]);
      print("%e %e %e %e\n",tab->f[k+1][j+1][i][h],tab->f[k+1][j+1][i][h+1],tab->f[k+1][j+1][i+1][h],tab->f[k+1][j+1][i+1][h+1]);
      print("Error Table 4D h=%d i=%d, j=%d k=%d\n",h,i,j,k);
      print("Error w=%e x=%e, y=%e z=%e-> value -1\n",w,x,y,z);
      exit(-1);
    }

   f000=tab->f[k][j][i][h]*(1-wn) + tab->f[k][j][i][h+1]*wn;
   f001=tab->f[k][j][i+1][h]*(1-wn) + tab->f[k][j][i+1][h+1]*wn;
   f010=tab->f[k][j+1][i][h]*(1-wn) + tab->f[k][j+1][i][h+1]*wn;
   f011=tab->f[k][j+1][i+1][h]*(1-wn) + tab->f[k][j+1][i+1][h+1]*wn;
   f100=tab->f[k+1][j][i][h]*(1-wn) + tab->f[k+1][j][i][h+1]*wn;
   f101=tab->f[k+1][j][i+1][h]*(1-wn) + tab->f[k+1][j][i+1][h+1]*wn;
   f110=tab->f[k+1][j+1][i][h]*(1-wn) + tab->f[k+1][j+1][i][h+1]*wn;
   f111=tab->f[k+1][j+1][i+1][h]*(1-wn) + tab->f[k+1][j+1][i+1][h+1]*wn;

   f00=f000*(1.-xn) + f001*xn;
   f01=f010*(1.-xn) + f011*xn;
   f10=f100*(1.-xn) + f101*xn;
   f11=f110*(1.-xn) + f110*xn;

   f0=f00*(1.-yn)+f01*yn;
   f1=f10*(1.-yn)+f11*yn;
  
  (*f) = f0*(1-zn)+f1*zn;
  return 0;  /* success */
}

/* ********************************************************************* */
void WriteBinaryTable4D (char *fname, Table4D *tab)
/*! 
 *  The binary table is a compact format used to write a 4D array 
 *  together with simple structured coordinates.
 *  The file consists of the following information:
 *  \verbatim
     nw
     nx
     ny
     nz
     <w[0]..w[nw-1]>
     <x[0]..x[nx-1]>
     <y[0]..y[ny-1]>
     <z[0]..z[nz-1]>
     ...........
     <q[0][0][0][0]..q[0][0][nw-1]
      q[0][0][1][0]..q[1][0][nw-1]
        .......
      q[0][0][nx-1][0]..q[0][0][nx-1][nw-1]>
      ................
      q[nz-1][ny-1][nx-1][0]..q[nz-1][ny-1][nx-1][nw-1]>
    \endverbatim
 *  All fields are written in binary format using double precision
 *  arithmetic.
 *
 *********************************************************************** */
{
  int    i,j,k;
  double scrh;
  FILE *fp;
  
  fp = fopen(fname,"wb");
 
  scrh = (double)tab->nw;
  fwrite(&scrh, sizeof(double), 1, fp);

  scrh = (double)tab->nx;
  fwrite(&scrh, sizeof(double), 1, fp);
  
  scrh = (double)tab->ny;
  fwrite(&scrh, sizeof(double), 1, fp);

  scrh = (double)tab->nz;
  fwrite(&scrh, sizeof(double), 1, fp);
  
  fwrite(tab->lnw, sizeof(double), tab->nw, fp);
  fwrite(tab->lnx, sizeof(double), tab->nx, fp);
  fwrite(tab->lny, sizeof(double), tab->ny, fp);
  fwrite(tab->lnz, sizeof(double), tab->nz, fp);
  
  for (k = 0; k < tab->nz; k++){
   for (j = 0; j < tab->ny; j++){
     for (i = 0; i < tab->nx; i++){
     fwrite (tab->f[k][j][i], sizeof(double), tab->nw, fp);
  }}}
  
  fprintf (fp,"\n");
  fclose(fp);
}
