/* ///////////////////////////////////////////////////////////////////// */
/*!
  \file
  \brief Miscellaneous functions for handling 2D tables.

  \author J. Onorbe (jose.onnorbe@gmail.com)
  \date   Mar 14, 2016
*/
/* ///////////////////////////////////////////////////////////////////// */
#include "pluto.h"

/* ********************************************************************* */
void InitializeTable1D (Table1D *tab, double xmin, double xmax, int nx)
/*!
 * Allocate memory for the arrays contained in the \c *tab structure 
 * and generate a uniformly spaced grid in log10(x) within the 
 * range provided by \c xmin, \c xmax.
 * On output, the function initializes the following structure members:
 *
 * - <tt> nx </tt>
 * - <tt> lnxmin, lnxmax </tt>
 * - <tt> lnx[] </tt>
 * - <tt> dlnx,  </tt>
 * - <tt> dlnx_1  </tt>
 * - <tt> x[]  </tt>
 * - <tt> dx[] </tt>
 *
 * \param [in,out] tab   pointer to a Table2D structure
 * \param [in]     xmin  lower column limit. 
 * \param [in]     xmax  upper column limit. 
 * \param [in]     nx    number of equally-spaced column bins (in log space)
 * 
 *********************************************************************** */
{
  int i;

  tab->nx = nx;
    
/* ---------------------------------------------------------------------- 
   If you do not initialize a structure variable, the effect depends on 
   whether it is has static storage (see Storage Class Specifiers) or 
   not. 
   If it is, members with integral types are initialized with 0 and 
   pointer  members are initialized to NULL; otherwise, the value of 
   the structure's members is indeterminate.
  ----------------------------------------------------------------------- */

  tab->f       = ARRAY_1D(tab->nx, double);
  tab->defined = ARRAY_1D(tab->nx, char);
  for (i = 0; i < tab->nx; i++){
    tab->f[i] = 0.0;
    tab->defined[i] = 1;
  }

  tab->lnx  = ARRAY_1D(tab->nx, double);

  tab->x    = ARRAY_1D(tab->nx, double);

  tab->dx   = ARRAY_1D(tab->nx, double);


/* ---------------------------------------------------------------
    Compute table bounds 
   --------------------------------------------------------------- */

  tab->xmin = xmin;
  tab->xmax = xmax;
  tab->lnxmin = log10(xmin);
  tab->lnxmax = log10(xmax);
  
  tab->dlnx = (tab->lnxmax - tab->lnxmin)/(double)(tab->nx - 1.0);

  tab->dlnx_1 = 1.0/tab->dlnx;

  for (i = 0; i < tab->nx; i++) {
    tab->lnx[i] = tab->lnxmin + i*tab->dlnx;
    tab->x[i]   = pow(10.0, tab->lnx[i]);
  }

/* -- table will not be uniform in x : compute spacings -- */

  for (i = 0; i < tab->nx-1; i++) tab->dx[i] = tab->x[i+1] - tab->x[i];

}
/* ********************************************************************* */

#define LOG_INTERPOLATION NO

/* ********************************************************************* */
int Table1DInterpolate(Table1D *tab, double x, double *f)
/*!
 * Use linear interpolation to find the function f(x) from the 
 * 1D table \c *tab.
 * 
 * \param [in]   *tab   a pointer to a Table1D structure
 * \param [in]   x      the abscissa where interpolation is needed.
 * \param [out] *f      the interpolated value
 *
 * \return  - Return 0 on success, otherwise: 
 *          - -1 if \c x is below column range; 
 *          -  1 if \c x is above column range;
 *
 *********************************************************************** */
{
  int i;
  double lnx, xn;
  
  lnx = log10(x);  /* Take the log to find the indices */

/* -------------------------------------------------------
    Check bounds 
   ------------------------------------------------------- */

  if (lnx < tab->lnxmin){
    WARNING(
      print ("! Table1DInterpolate: lnx outside range %12.6e < %12.6e\n",
              lnx, tab->lnxmin);
    )
    return -1;
  }
   
  if (lnx > tab->lnxmax){
    WARNING(
      print ("! Table1DInterpolate: lnx outside range: %12.6e > %12.6e\n",
              lnx, tab->lnxmax);
    )
    return 1;
  }


/* ------------------------------------------------
    Find indices i  
   ------------------------------------------------ */
   
  i  = INT_FLOOR((lnx - tab->lnxmin)*tab->dlnx_1); 

  #if LOG_INTERPOLATION == YES
   xn = (lnx - tab->lnx[i])/tab->dlnx;  /* Compute normalized log coordinates */ 
  #else
   xn = (x - tab->x[i])/tab->dx[i];  /* Compute normalized linear coordinates */ 
  #endif
  
  (*f) =  tab->f[i]*(1.-xn) + tab->f[i+1]*xn;
  return 0;  /* success */
}

/* ********************************************************************* */
void WriteBinaryTable1D (char *fname, Table1D *tab)
/*! 
 *  The binary table is a compact format used to write a 1D array 
 *  together with simple structured coordinates.
 *  The file consists of the following information:
 *  \verbatim
     nx
     <x[0]..x[nx-1]>
     <q[0]..q[nx-1]
    \endverbatim
 *  All fields are written in binary format using double precision
 *  arithmetic.
 *
 *********************************************************************** */
{
  int    i;
  double scrh;
  FILE *fp;
  
  fp = fopen(fname,"wb");
 
  scrh = (double)tab->nx;
  fwrite(&scrh, sizeof(double), 1, fp);
  
  fwrite(tab->lnx, sizeof(double), tab->nx, fp);
  
  fwrite (tab->f, sizeof(double), tab->nx, fp);
  
  fprintf (fp,"\n");
  fclose(fp);
}
