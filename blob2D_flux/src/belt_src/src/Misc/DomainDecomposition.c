#include "pluto.h"
//#include "definitions.h"
#include "DomainDecomposition.h"

#include <stdlib.h>


DA DACenter;


int InitializePETScDistributedArray(Data *data, Grid *grid){
	
	int NumberOfProcessors;
	int NumberOfProcessorsX, NumberOfProcessorsY, NumberOfProcessorsZ;
	int NumberOfGlobalGridCellsX, NumberOfGlobalGridCellsY, NumberOfGlobalGridCellsZ;
	int PeriodicityX, PeriodicityY, PeriodicityZ;
    int mpierr;
	
	PeriodicityX = PERIODICX;
	PeriodicityY = PERIODICY;
	PeriodicityZ = PERIODICZ;
	
	//
	// Start the parallel solver Library:
	//
	PetscInitialize(0, 0, (char*) 0, "");
	
	//
	// Print Index number in front of any standard vector output to terminal, txt-file etc.:
	//
	PetscViewerSetFormat(PETSC_VIEWER_STDOUT_WORLD, PETSC_VIEWER_ASCII_INDEX);
	PetscViewerSetFormat(PETSC_VIEWER_STDOUT_SELF,  PETSC_VIEWER_ASCII_INDEX);
	
	//
	// Set number of processors per spatial grid direction:
	//
	AL_Get_size(SZ, &NumberOfProcessors);
	NumberOfProcessorsX = grid[IDIR].nproc;
	if(DIMENSIONS > 1)
		NumberOfProcessorsY = grid[JDIR].nproc;
	else
		NumberOfProcessorsY = 1;		
	if(DIMENSIONS > 2)
		NumberOfProcessorsZ = grid[KDIR].nproc;
	else
		NumberOfProcessorsZ = 1;	
	
	//
	// Now re-arrange the ranks of the cpus inside the PETSc communicator to match the MPI default layout:
	//
	MPI_Comm NewComm;
	int NewRank;
	int x,y,z;
	x = prank / (NumberOfProcessorsZ*NumberOfProcessorsY);
	y = (prank % (NumberOfProcessorsZ*NumberOfProcessorsY)) / NumberOfProcessorsZ;
	z = (prank % (NumberOfProcessorsZ*NumberOfProcessorsY)) % NumberOfProcessorsZ;
	NewRank = z*NumberOfProcessorsY*NumberOfProcessorsX + y*NumberOfProcessorsX + x;
	MPI_Comm_split(PETSC_COMM_WORLD, 1, NewRank, &NewComm);
	PETSC_COMM_WORLD = NewComm;
    
    //printf("[%d] NewRank = %d\n", prank, NewRank);
	
	
	//
	// Set number of global and local grid cells per spatial direction:
	//
	NumberOfGlobalGridCellsX = grid[IDIR].np_int_glob;
	if(DIMENSIONS > 1)
		NumberOfGlobalGridCellsY = grid[JDIR].np_int_glob;
	else
		NumberOfGlobalGridCellsY = 1;
	if(DIMENSIONS > 2)
		NumberOfGlobalGridCellsZ = grid[KDIR].np_int_glob;
	else
		NumberOfGlobalGridCellsZ = 1;
	
	//
	// Add Ghost Cells for non-periodic Boundary Conditions:
	//
	if(!PeriodicityX)                   NumberOfGlobalGridCellsX += 2;
	if(!PeriodicityY && DIMENSIONS > 1) NumberOfGlobalGridCellsY += 2;
	if(!PeriodicityZ && DIMENSIONS > 2) NumberOfGlobalGridCellsZ += 2;
	
	//
	// Transfer the PLUTO parallel Domain Decomposition to PETSc:
	//
	PetscInt lx;
	PetscInt ly;
	PetscInt lz;
	PetscInt *NumberOfLocalGridCellsX = malloc(NumberOfProcessors * sizeof(PetscInt));
	PetscInt *NumberOfLocalGridCellsY = malloc(NumberOfProcessors * sizeof(PetscInt));
	PetscInt *NumberOfLocalGridCellsZ = malloc(NumberOfProcessors * sizeof(PetscInt));
	
	lx = grid[IDIR].np_int;
	if(grid[IDIR].beg == grid[IDIR].gbeg && PeriodicityX == 0) lx += 1;
	if(grid[IDIR].end == grid[IDIR].gend && PeriodicityX == 0) lx += 1;
	
	ly = grid[JDIR].np_int;
#if DIMENSIONS > 1
	if(grid[JDIR].beg == grid[JDIR].gbeg && PeriodicityY == 0) ly += 1;
	if(grid[JDIR].end == grid[JDIR].gend && PeriodicityY == 0) ly += 1;
#endif
	
	lz = grid[KDIR].np_int;
#if DIMENSIONS > 2
	if(grid[KDIR].beg == grid[KDIR].gbeg && PeriodicityZ == 0) lz += 1;
	if(grid[KDIR].end == grid[KDIR].gend && PeriodicityZ == 0) lz += 1;
#endif
	
	mpierr = MPI_Allgather(&lx, 1, MPI_INT, NumberOfLocalGridCellsX, 1, MPI_INT, PETSC_COMM_WORLD);
	mpierr = MPI_Allgather(&ly, 1, MPI_INT, NumberOfLocalGridCellsY, 1, MPI_INT, PETSC_COMM_WORLD);
	mpierr = MPI_Allgather(&lz, 1, MPI_INT, NumberOfLocalGridCellsZ, 1, MPI_INT, PETSC_COMM_WORLD);
	
    int i, inode;
    PetscInt *lx_perdirection = malloc(NumberOfProcessorsX * sizeof(PetscInt));
    PetscInt *ly_perdirection = malloc(NumberOfProcessorsY * sizeof(PetscInt));
    PetscInt *lz_perdirection = malloc(NumberOfProcessorsZ * sizeof(PetscInt));
    
    inode = 0;
    for(i = 0; i < NumberOfProcessorsX; i++){
        lx_perdirection[i] = NumberOfLocalGridCellsX[inode];
        inode += 1;
    }
    
    inode = 0;
    for(i = 0; i < NumberOfProcessorsY; i++){
        ly_perdirection[i] = NumberOfLocalGridCellsY[inode];
        inode += NumberOfProcessorsX;
    }
    
    inode = 0;
    for(i = 0; i < NumberOfProcessorsZ; i++){
        lz_perdirection[i] = NumberOfLocalGridCellsZ[inode];
        inode += NumberOfProcessorsX*NumberOfProcessorsY;
    }
	
	//
	// Set Periodicity (to get access to these ghosts/halos):
	//
	DAPeriodicType Periodicity = DA_NONPERIODIC;
	switch(DIMENSIONS){
			//
			// 1D:
			//
		case 1:
			if(PeriodicityX)
				Periodicity = DA_XPERIODIC;
			else
				Periodicity = DA_NONPERIODIC;
			break;
			//
			// 2D:
			//
		case 2:
			if(PeriodicityX)
				if(PeriodicityY)
					Periodicity = DA_XYPERIODIC;
				else
					Periodicity = DA_XPERIODIC;
			else
				if(PeriodicityY)
					Periodicity = DA_YPERIODIC;
				else
					Periodicity = DA_NONPERIODIC;
			break;
			//
			// 3D:
			//
		case 3:
			if(PeriodicityX)
				if(PeriodicityY)
					if(PeriodicityZ)
						Periodicity = DA_XYZPERIODIC;
					else
						Periodicity = DA_XYPERIODIC;
				else
					if(PeriodicityZ)
						Periodicity = DA_XZPERIODIC;
					else
						Periodicity = DA_XPERIODIC;
			else
				if(PeriodicityY)
					if(PeriodicityZ)
						Periodicity = DA_YZPERIODIC;
					else
						Periodicity = DA_YPERIODIC;
				else
					if(PeriodicityZ)
						Periodicity = DA_ZPERIODIC;
					else
						Periodicity = DA_NONPERIODIC;
			break;
		default:
			PetscPrintf(PETSC_COMM_WORLD, "### ERROR: Dimension = %d is not in allowed range              ###\n", DIMENSIONS);
			exit(1);
			break;
	}
	
	
	// ********************************
	// * Create the Distributed Array *
	// ********************************
	//
	DACreate3d(
			   PETSC_COMM_WORLD,
			   Periodicity,
			   DA_STENCIL_BOX, /* DA_STENCIL_STAR */
			   NumberOfGlobalGridCellsX, //NumberOfGridCellsX + 2*StencilWidthX,
			   NumberOfGlobalGridCellsY, //NumberOfGridCellsY + 2*StencilWidthY,
			   NumberOfGlobalGridCellsZ, //NumberOfGridCellsZ + 2*StencilWidthZ,
			   NumberOfProcessorsX,
			   NumberOfProcessorsY,
			   NumberOfProcessorsZ,
			   1,
			   1, /* CommunicationStencilWidth */
               lx_perdirection,
               ly_perdirection,
               lz_perdirection,
//			   NumberOfLocalGridCellsX,
//			   NumberOfLocalGridCellsY,
//			   NumberOfLocalGridCellsZ,
			   &DACenter
			   );
	
    //printf("[%d] done\n", prank);
    
	return 0;
}
