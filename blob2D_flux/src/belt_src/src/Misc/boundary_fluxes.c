#include "pluto.h"
#include "boundary_fluxes.h"


//
// Fluxes over Boundaries:
//
double Flux_X1_BEG_INWARD_Global;
double Flux_X1_END_INWARD_Global;
double Flux_X2_BEG_INWARD_Global;
double Flux_X2_END_INWARD_Global;
double Flux_X3_BEG_INWARD_Global;
double Flux_X3_END_INWARD_Global;
double Flux_X1_BEG_OUTWARD_Global;
double Flux_X1_END_OUTWARD_Global;
double Flux_X2_BEG_OUTWARD_Global;
double Flux_X2_END_OUTWARD_Global;
double Flux_X3_BEG_OUTWARD_Global;
double Flux_X3_END_OUTWARD_Global;

double Flux_X1_BEG_INWARD_Local  = 0.0;
double Flux_X1_END_INWARD_Local  = 0.0;
double Flux_X2_BEG_INWARD_Local  = 0.0;
double Flux_X2_END_INWARD_Local  = 0.0;
double Flux_X3_BEG_INWARD_Local  = 0.0;
double Flux_X3_END_INWARD_Local  = 0.0;
double Flux_X1_BEG_OUTWARD_Local = 0.0;
double Flux_X1_END_OUTWARD_Local = 0.0;
double Flux_X2_BEG_OUTWARD_Local = 0.0;
double Flux_X2_END_OUTWARD_Local = 0.0;
double Flux_X3_BEG_OUTWARD_Local = 0.0;
double Flux_X3_END_OUTWARD_Local = 0.0;

//
// Total mass loss/gain = Integrated fluxes over time:
//
double M_X1_BEG = 0.0;
double M_X1_END = 0.0;
double M_X2_BEG = 0.0;
double M_X2_END = 0.0;
double M_X3_BEG = 0.0;
double M_X3_END = 0.0;




void ComputeBoundaryFluxes(){
    
#if LOG_OUTPUT != 0
    //		PetscFPrintf(MPI_COMM_WORLD, LogFile, "###       ...  ...  Gather fluxes over boundaries      ###\n");
    //printf("###       ...  ...  Gather Fluxes Flux_X1_BEG_INWARD_Local = %e  ###\n", Flux_X1_BEG_INWARD_Local);
    print1("###       ...  ...  Gather fluxes over boundaries INWARD X1 BEGIN   ###\n");
#endif
	//
	// Gather fluxes over boundaries from all processors:
	//
	MPI_Allreduce(&Flux_X1_BEG_INWARD_Local,  &Flux_X1_BEG_INWARD_Global,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#if LOG_OUTPUT != 0
    //printf("###       ...  ...  Gather Fluxes Flux_X1_END_INWARD_Local = %e  ###\n", Flux_X1_END_INWARD_Local);
    print1("###       ...  ...  Gather fluxes over boundaries OUTWARD X1 END    ###\n");
#endif
	MPI_Allreduce(&Flux_X1_END_INWARD_Local,  &Flux_X1_END_INWARD_Global,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#if LOG_OUTPUT != 0
    print1("###       ...  ...  Gather fluxes over boundaries OUTWARD X2    ###\n");
#endif
	MPI_Allreduce(&Flux_X2_BEG_INWARD_Local,  &Flux_X2_BEG_INWARD_Global,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	MPI_Allreduce(&Flux_X2_END_INWARD_Local,  &Flux_X2_END_INWARD_Global,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#if LOG_OUTPUT != 0
    print1("###       ...  ...  Gather fluxes over boundaries INWARD X3    ###\n");
#endif
	MPI_Allreduce(&Flux_X3_BEG_INWARD_Local,  &Flux_X3_BEG_INWARD_Global,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	MPI_Allreduce(&Flux_X3_END_INWARD_Local,  &Flux_X3_END_INWARD_Global,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

#if LOG_OUTPUT != 0
    print1("###       ...  ...  Gather fluxes over boundaries OUTWARD    ###\n");
#endif
    
	MPI_Allreduce(&Flux_X1_BEG_OUTWARD_Local, &Flux_X1_BEG_OUTWARD_Global, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	MPI_Allreduce(&Flux_X1_END_OUTWARD_Local, &Flux_X1_END_OUTWARD_Global, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	MPI_Allreduce(&Flux_X2_BEG_OUTWARD_Local, &Flux_X2_BEG_OUTWARD_Global, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	MPI_Allreduce(&Flux_X2_END_OUTWARD_Local, &Flux_X2_END_OUTWARD_Global, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	MPI_Allreduce(&Flux_X3_BEG_OUTWARD_Local, &Flux_X3_BEG_OUTWARD_Global, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	MPI_Allreduce(&Flux_X3_END_OUTWARD_Local, &Flux_X3_END_OUTWARD_Global, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
#if MIDPLANESYMMETRY == YES
	Flux_X1_BEG_INWARD_Global  *= 2.0;
	Flux_X1_END_INWARD_Global  *= 2.0;
	Flux_X1_BEG_OUTWARD_Global *= 2.0;
	Flux_X1_END_OUTWARD_Global *= 2.0;
    Flux_X3_BEG_INWARD_Global  *= 2.0;
    Flux_X3_END_INWARD_Global  *= 2.0;
    Flux_X3_BEG_OUTWARD_Global *= 2.0;
    Flux_X3_END_OUTWARD_Global *= 2.0;
#endif
    
#if LOG_OUTPUT != 0
		//PetscFPrintf(MPI_COMM_WORLD, LogFile, "###       ...  ...  Compute Total mass             ###\n");
		print1("###       ...  ...  Compute Total mass             ###\n");
#endif
	//
	// Compute total mass loss/gain = Integrate Fluxes over time
	//
    M_X1_BEG += (Flux_X1_BEG_OUTWARD_Global - Flux_X1_BEG_INWARD_Global) * g_dt;
	M_X1_END += (Flux_X1_END_OUTWARD_Global - Flux_X1_END_INWARD_Global) * g_dt;
	M_X2_BEG += (Flux_X2_BEG_OUTWARD_Global - Flux_X2_BEG_INWARD_Global) * g_dt;
	M_X2_END += (Flux_X2_END_OUTWARD_Global - Flux_X2_END_INWARD_Global) * g_dt;
	M_X3_BEG += (Flux_X3_BEG_OUTWARD_Global - Flux_X3_BEG_INWARD_Global) * g_dt;
	M_X3_END += (Flux_X3_END_OUTWARD_Global - Flux_X3_END_INWARD_Global) * g_dt;
    
#if LOG_OUTPUT != 0
		//PetscFPrintf(MPI_COMM_WORLD, LogFile, "###       ...  ...  Start new local measurement    ###\n");
		print1("###       ...  ...  Start new local measurement    ###\n");
#endif
	//
	// Start new local 'measurement' (during next iteration):
	//
	Flux_X1_BEG_INWARD_Local  = 0.0;
	Flux_X1_END_INWARD_Local  = 0.0;
	Flux_X2_BEG_INWARD_Local  = 0.0;
	Flux_X2_END_INWARD_Local  = 0.0;
	Flux_X3_BEG_INWARD_Local  = 0.0;
	Flux_X3_END_INWARD_Local  = 0.0;
	Flux_X1_BEG_OUTWARD_Local = 0.0;
	Flux_X1_END_OUTWARD_Local = 0.0;
	Flux_X2_BEG_OUTWARD_Local = 0.0;
	Flux_X2_END_OUTWARD_Local = 0.0;
	Flux_X3_BEG_OUTWARD_Local = 0.0;
	Flux_X3_END_OUTWARD_Local = 0.0;
    
}



void StoreBoundaryFluxes(const State_1D *state, Grid *grid, int ks, int js, int is, double factor){

    double MassFlux = 0.0;
    double check=-1.0;
    double rhoborder = -1.0;
    double area = -1.0;
    
    //    printf("beg  = %d\n", grid[g_dir].beg);
    //    printf("lbeg = %d\n", grid[g_dir].lbeg);
    //    printf("gbeg = %d\n", grid[g_dir].gbeg);
    //    printf("IBEG = %d\n", IBEG);

    
    //
    // Left boundary of computational domain:
    //
    if(g_dir == IDIR && grid[IDIR].beg == grid[IDIR].gbeg){
        
#if ZeroFlux_X1_BEG_INWARD == YES
        state->flux[IBEG-1][RHO] = MIN(state->flux[IBEG-1][RHO], 0);
#endif
#if ZeroFlux_X1_BEG_OUTWARD == YES
        state->flux[IBEG-1][RHO] = MAX(state->flux[IBEG-1][RHO], 0);
#endif
        check = 1.0*IDIR;
        rhoborder = state->flux[IBEG-1][RHO]; 
        area = GridCellArea(grid, g_dir, ks, js, IBEG); 
        MassFlux =
            factor
            *
            state->flux[IBEG-1][RHO]
            *
            GridCellArea(grid, g_dir, ks, js, IBEG);
        
        if(MassFlux < 0) Flux_X1_BEG_OUTWARD_Local -= MassFlux;
        else             Flux_X1_BEG_INWARD_Local  += MassFlux;
    }
    else if(g_dir == JDIR && grid[JDIR].beg == grid[JDIR].gbeg){
        
#if ZeroFlux_X2_BEG_INWARD == YES
        state->flux[JBEG-1][RHO] = MIN(state->flux[JBEG-1][RHO], 0);
#endif
#if ZeroFlux_X2_BEG_OUTWARD == YES
        state->flux[JBEG-1][RHO] = MAX(state->flux[JBEG-1][RHO], 0);
#endif
        
        check = 1.0*JDIR;
        rhoborder = state->flux[JBEG-1][RHO]; 
        area = GridCellArea(grid, g_dir, ks, JBEG, is); 
        MassFlux =
            factor
            *
            state->flux[JBEG-1][RHO]
            *
            GridCellArea(grid, g_dir, ks, JBEG, is);
        
        if(MassFlux < 0) Flux_X2_BEG_OUTWARD_Local -= MassFlux;
        else             Flux_X2_BEG_INWARD_Local  += MassFlux;
    }
    else if(g_dir == KDIR && grid[KDIR].beg == grid[KDIR].gbeg){

#if ZeroFlux_X3_BEG_INWARD == YES
        state->flux[KBEG-1][RHO] = MIN(state->flux[KBEG-1][RHO], 0);
#endif
#if ZeroFlux_X3_BEG_OUTWARD == YES
        state->flux[KBEG-1][RHO] = MAX(state->flux[KBEG-1][RHO], 0);
#endif
        
        check = 1.0*KDIR;
        rhoborder = state->flux[KBEG-1][RHO]; 
        area = GridCellArea(grid, g_dir, KBEG, js, is); 
        MassFlux =
            factor
            *
            state->flux[KBEG-1][RHO]
            *
            GridCellArea(grid, g_dir, KBEG, js, is);
        
        if(MassFlux < 0) Flux_X3_BEG_OUTWARD_Local -= MassFlux;
        else             Flux_X3_BEG_INWARD_Local  += MassFlux;
    }
    
#if DEBUGGING > 0
    if(isnan(MassFlux)){
        printf("ERROR: MassFlux = 'nan' Left boundary\n");
        printf("ERROR:  I_dir = %d J_dir=%d K_dir=%d\n", IDIR,JDIR,KDIR);
        printf("ERROR:  g_dir            = %d\n", g_dir);
        printf("ERROR:  check = %e; grid[g_dir].beg = %e; grid[g_dir].gbeg = %e\n", check, grid[g_dir].beg, grid[g_dir].gbeg);
        printf("ERROR:  ks               = %d\n", ks);
        printf("ERROR:  js               = %d\n", js);
        printf("ERROR:  is               = %d\n", is);
        printf("ERROR:  IBEG             = %ld\n", IBEG);
        printf("ERROR:  IEND             = %ld\n", IEND);
        printf("ERROR:  grid[g_dir].beg  = %d\n", grid[g_dir].beg);
        printf("ERROR:  grid[g_dir].lbeg = %d\n", grid[g_dir].gbeg);
        printf("ERROR:  grid[g_dir].gbeg = %d\n", grid[g_dir].gbeg);
        printf("ERROR:  factor              = %e\n", factor);
        printf("ERROR:  state->flux[i][RHO] = %e\n", state->flux[grid[g_dir].lbeg-1][RHO]);
        printf("ERROR:  grid[g_dir].xr[i]   = %e\n", grid[g_dir].xr[grid[g_dir].lbeg-1]);
        printf("ERROR:  rho   = %e\n", rhoborder);
        printf("ERROR:  area   = %e\n", area);
        exit(1);
    }
#endif
    
    
    //
    // Right boundary of computational domain:
    //
    if(g_dir == IDIR && grid[IDIR].end == grid[IDIR].gend){
        
#if ZeroFlux_X1_END_INWARD == YES
        state->flux[IEND][RHO] = MAX(state->flux[IEND][RHO], 0);
#endif
#if ZeroFlux_X1_END_OUTWARD == YES
        state->flux[IEND][RHO] = MIN(state->flux[IEND][RHO], 0);
#endif
        
        MassFlux =
            factor
            *
            state->flux[IEND][RHO]
            *
            GridCellArea(grid, g_dir, ks, js, IEND+1);
        
        if(MassFlux < 0) Flux_X1_END_INWARD_Local  -= MassFlux;
        else             Flux_X1_END_OUTWARD_Local += MassFlux;
    }
    else if(g_dir == JDIR && grid[JDIR].end == grid[JDIR].gend){
        
#if ZeroFlux_X2_END_INWARD == YES
        state->flux[JEND][RHO] = MAX(state->flux[JEND][RHO], 0);
#endif
#if ZeroFlux_X2_END_OUTWARD == YES
        state->flux[JEND][RHO] = MIN(state->flux[JEND][RHO], 0);
#endif

        
        MassFlux =
            factor
            *
            state->flux[JEND][RHO]
            *
            GridCellArea(grid, g_dir, ks, JEND+1, is);
        
        if(MassFlux < 0) Flux_X2_END_INWARD_Local  -= MassFlux;
        else             Flux_X2_END_OUTWARD_Local += MassFlux;
    }
    else if(g_dir == KDIR && grid[KDIR].end == grid[KDIR].gend){
        
#if ZeroFlux_X3_END_INWARD == YES
        state->flux[KEND][RHO] = MAX(state->flux[KEND][RHO], 0);
#endif
#if ZeroFlux_X3_END_OUTWARD == YES
        state->flux[KEND][RHO] = MIN(state->flux[KEND][RHO], 0);
#endif
        
        MassFlux =
            factor
            *
            state->flux[KEND][RHO]
            *
            GridCellArea(grid, g_dir, KEND+1, js, is);
        
        if(MassFlux < 0) Flux_X3_END_INWARD_Local  -= MassFlux;
        else             Flux_X3_END_OUTWARD_Local += MassFlux;
    }
    
#if DEBUGGING > 0
    if(isnan(MassFlux)){
        printf("ERROR: MassFlux = 'nan'\n");
        printf("ERROR:  g_dir            = %d\n", g_dir);
        printf("ERROR:  ks               = %d\n", ks);
        printf("ERROR:  js               = %d\n", js);
        printf("ERROR:  is               = %d\n", is);
        printf("ERROR:  IBEG             = %ld\n", IBEG);
        printf("ERROR:  IEND             = %ld\n", IEND);
        printf("ERROR:  grid[g_dir].beg  = %d\n", grid[g_dir].beg);
        printf("ERROR:  grid[g_dir].lbeg = %d\n", grid[g_dir].gbeg);
        printf("ERROR:  grid[g_dir].gbeg = %d\n", grid[g_dir].gbeg);
        printf("ERROR:  factor              = %e\n", factor);
        printf("ERROR:  state->flux[i][RHO] = %e\n", state->flux[grid[g_dir].lend][RHO]);
        printf("ERROR:  grid[g_dir].xr[i]   = %e\n", grid[g_dir].xr[grid[g_dir].lend]);
        exit(1);
    }
#endif
#if LOG_OUTPUT != 0
    //printf("###       ...  ...  Flux_X1_BEG_INWARD_Local = %e  ###\n", Flux_X1_BEG_INWARD_Local);
    //printf("###       ...  ...  Flux_X1_BEG_OUTWARD_Local = %e  ###\n", Flux_X1_BEG_OUTWARD_Local);
    //printf("###       ...  ...  Flux_X2_BEG_INWARD_Local = %e  ###\n", Flux_X2_BEG_INWARD_Local);
    //printf("###       ...  ...  Flux_X2_BEG_OUTWARD_Local = %e  ###\n", Flux_X2_BEG_OUTWARD_Local);
    //printf("###       ...  ...  Flux_X3_BEG_INWARD_Local = %e  ###\n", Flux_X3_BEG_INWARD_Local);
    //printf("###       ...  ...  Flux_X3_BEG_OUTWARD_Local = %e  ###\n", Flux_X3_BEG_OUTWARD_Local);
    //
    //printf("###       ...  ...  Flux_X1_END_INWARD_Local = %e  ###\n", Flux_X1_END_INWARD_Local);
    //printf("###       ...  ...  Flux_X1_END_OUTWARD_Local = %e  ###\n", Flux_X1_END_OUTWARD_Local);
    //printf("###       ...  ...  Flux_X2_END_INWARD_Local = %e  ###\n", Flux_X2_END_INWARD_Local);
    //printf("###       ...  ...  Flux_X2_END_OUTWARD_Local = %e  ###\n", Flux_X2_END_OUTWARD_Local);
    //printf("###       ...  ...  Flux_X3_END_INWARD_Local = %e  ###\n", Flux_X3_END_INWARD_Local);
    //printf("###       ...  ...  Flux_X3_END_OUTWARD_Local = %e  ###\n", Flux_X3_END_OUTWARD_Local);
#endif
    
}

