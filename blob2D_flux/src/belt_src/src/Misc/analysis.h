#ifndef ANALYSIS_H_
#define ANALYSIS_H_

int WriteAnalysis(const Data *, Grid *);
double TotalMass(const Data *, Grid *);

#endif
