#include "pluto.h"
#include "interface.h"
#include "PhysicalConstantsCGS.h"
#include "Molecules.h"
#include "ReadRestrictionsConfiguration.h"

#if STELLAREVOLUTION == YES
#include "StellarEvolution.h"
#endif

#if IONIZATION == YES
#include "Ionization.h"
#endif


// *********
// * Setup *
// *********
//
FILE *LogFile;
double gamma_AdiabaticIndex;


// ********************
// * Reference values *
// ********************
//
double ReferenceLength;
double ReferenceArea;
double ReferenceVolume;
double ReferenceTime;
double ReferenceVelocity;
double ReferenceDensity;
double ReferencePressure;
double ReferenceTemperature;
double ReferenceAcceleration;
double ReferenceEnergyDensity;
double ReferenceEnergy;
double ReferenceFrequency;
double ReferenceMass;
double ReferenceAccretionRate;
double ReferencePower;
double ReferenceLuminosity;
double ReferenceOpacity;
double ReferenceNumberDensity;
double ReferenceForceDensity;
double ReferenceNumber;


//
// Internal Routines:
//
void UserInput2Reference(void);
void InitializeReferenceVariables(void);

    

int InitializeReferenceSystem(){
	
	//
	// Initialize physical constants in cgs units:
	//
	PhysicalConstantsCGS();

    //
    // Initialize reference variables in cgs units:
    //
    InitializeReferenceVariables();
    
	//
	// Convert all physical constants to code units:
	//
	PhysicalConstants2Reference();
    
	//
	// Convert user input variables to code units:
	//
    UserInput2Reference();
	
	return 0;
}



void InitializeReferenceVariables(){

    //
	// belt                = PLUTO
	//
#if PLUTO == 0
	ReferenceLength 	   = g_unitLength;   // in cm
	ReferenceVelocity	   = g_unitVelocity; // in cm s^-1
	ReferenceDensity	   = g_unitDensity;  // in g cm^-3
#elif PLUTO == 1
	ReferenceLength 	   = UNIT_LENGTH;   // in cm
	ReferenceVelocity	   = UNIT_VELOCITY; // in cm s^-1
	ReferenceDensity	   = UNIT_DENSITY;  // in g cm^-3    
#else
    printf("PLUTO version not correctly specified.\n");
    exit(1);
#endif
    
	//
	// Specify further Reference units:
	//
	ReferenceArea          = pow(ReferenceLength, 2);                                                // in cm^2
	ReferenceVolume        = pow(ReferenceLength, 3);                                                // in cm^3
	ReferencePressure  	   = ReferenceDensity  * pow(ReferenceVelocity, 2);                          // in erg cm^-3 = dyn cm^-2
	ReferenceTime     	   = ReferenceLength / ReferenceVelocity;                                    // in s
	ReferenceAcceleration  = ReferenceVelocity / ReferenceTime;                                      // in cm s^-2
	ReferenceForceDensity  = ReferenceDensity * ReferenceAcceleration;                               // in g cm^-2 s^-2
	ReferenceEnergyDensity = ReferencePressure;                                                      // in erg cm^-3
	ReferenceEnergy        = ReferenceEnergyDensity * ReferenceVolume;                               // in erg
	ReferenceMass          = ReferenceDensity       * ReferenceVolume;                               // in g
	ReferenceAccretionRate = ReferenceMass / ReferenceTime;                                          // in g s^-1
	ReferenceOpacity       = 1.0 / (ReferenceDensity * ReferenceLength);                             // in cm^2 g^-1
	ReferenceFrequency     = 1.0 / ReferenceTime;                                                    // in s^-1 = Hz
	ReferencePower         = ReferenceEnergy / ReferenceTime;                                        // in erg s^-1
	ReferenceLuminosity    = ReferencePower;                                                         // in erg s^-1
	ReferenceTemperature   = pow(ReferenceVelocity, 2) * MolarMass(0) / R_UniversalGasConstant;      // in K
    ReferenceNumberDensity = 1.0 / ReferenceVolume;                                                  // in cm^-3
    ReferenceNumber        = NA_AvogadroConstant;                                                    // in mol^-1
    
}



void UserInput2Reference(){

    // TODO: delete variable 'gamma_AdiabaticIndex' and directly use g_gamma from pluto
#if EOS != ISOTHERMAL
	g_gamma = gamma_AdiabaticIndex;
#endif
    
    //
    // From StellarEvolution.conf:
    //
#if STELLAREVOLUTION == YES
	StellarRadius         *= SolarRadius;          // in code units
	StellarLuminosity     *= SolarLuminosity;      // in code units
	ConstantAccretionRate *= SolarMass/Year;       // in code units
#endif
    
    //
    // From Sedna.conf:
    //
#if IONIZATION == YES
    NeutralGasTemperature /= ReferenceTemperature; // in code units
    IonizedGasTemperature /= ReferenceTemperature; // in code units
#endif
    
    //
    // From Restrictions.conf:
    //
    RHO_min               /= ReferenceDensity;     // in code units
    RHO_max               /= ReferenceDensity;     // in code units
    DEBUGGING_RHO_min     /= ReferenceDensity;     // in code units
    DEBUGGING_RHO_max     /= ReferenceDensity;     // in code units

    PRS_min               /= ReferencePressure;    // in code units
    PRS_max               /= ReferencePressure;    // in code units
    DEBUGGING_PRS_min     /= ReferencePressure;    // in code units
    DEBUGGING_PRS_max     /= ReferencePressure;    // in code units

    T_dust_min            /= ReferenceTemperature; // in code units
    T_dust_max            /= ReferenceTemperature; // in code units
    T_gas_min             /= ReferenceTemperature; // in code units
    T_gas_max             /= ReferenceTemperature; // in code units
    T_rad_min             /= ReferenceTemperature; // in code units
    T_rad_max             /= ReferenceTemperature; // in code units
    DEBUGGING_T_dust_min  /= ReferenceTemperature; // in code units
    DEBUGGING_T_dust_max  /= ReferenceTemperature; // in code units
    DEBUGGING_T_gas_min   /= ReferenceTemperature; // in code units
    DEBUGGING_T_gas_max   /= ReferenceTemperature; // in code units
    
    VX1_min               /= ReferenceVelocity;    // in code units
    VX1_max               /= ReferenceVelocity;    // in code units
    VX2_min               /= ReferenceVelocity;    // in code units
    VX2_max               /= ReferenceVelocity;    // in code units
    VX3_min               /= ReferenceVelocity;    // in code units
    VX3_max               /= ReferenceVelocity;    // in code units
    DEBUGGING_VX1_min     /= ReferenceVelocity;    // in code units
    DEBUGGING_VX1_max     /= ReferenceVelocity;    // in code units
    DEBUGGING_VX2_min     /= ReferenceVelocity;    // in code units
    DEBUGGING_VX2_max     /= ReferenceVelocity;    // in code units
    DEBUGGING_VX3_min     /= ReferenceVelocity;    // in code units
    DEBUGGING_VX3_max     /= ReferenceVelocity;    // in code units

}



void StoreHydrodynamics(Data *data){

    int kl, jl, il;
    
    KTOT_LOOP(kl){
        JTOT_LOOP(jl){
            ITOT_LOOP(il){
                
                data->OLDRHO[kl][jl][il] = data->Vc[RHO][kl][jl][il];
#if EOS != ISOTHERMAL
                data->OLDPRS[kl][jl][il] = data->Vc[PRS][kl][jl][il];
#endif
                
            }
        }
    }
    
}
