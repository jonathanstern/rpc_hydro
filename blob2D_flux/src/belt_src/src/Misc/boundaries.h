#ifndef BOUNDARIES_H_
#define BOUNDARIES_H_

int BC_CELL_ZeroGradient( const Data *, int, Grid *, int, int, int, int);
int BC_CELL_Reflective(   const Data *, int, Grid *, int, int, int, int);

int BC_ZeroGradient(      const Data *, int, Grid *, int);
int BC_Reflective(        const Data *, int, Grid *, int);

int BC_NoOutflow_NoInflow(const Data *, int, Grid *);
int BC_Outflow_NoInflow(  const Data *, int, Grid *);

#endif
