#include "pluto.h"
#include "Molecules.h"
#include "PhysicalConstantsCGS.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


double mtot;
double Ntot;
double NH;
double mMH_AbsoluteMolecularMassOfHydrogen;


//double RelativeMolecularMass(double RelativeMolecularMassNeutral, double RelativeMolecularMassHydrogen, double IonizationFraction){
//    return 1.0 / (1.0/RelativeMolecularMassNeutral + IonizationFraction/RelativeMolecularMassHydrogen);
//}

double MolarMass(double IonizationFraction){
    
    double MolarMass, RelativeMolecularMass;
    
#if IONIZATION == YES
    double RelativeMolecularMassNeutral, RelativeMolecularMassHydrogen;
    RelativeMolecularMassNeutral  = mtot / Ntot;
    RelativeMolecularMassHydrogen = mtot / NH;
    
    RelativeMolecularMass         = 1.0 / (1.0/RelativeMolecularMassNeutral + IonizationFraction/RelativeMolecularMassHydrogen);
#else
    RelativeMolecularMass         = mtot / Ntot;
#endif
    
    MolarMass                     = RelativeMolecularMass * NA_AvogadroConstant * mC_CarbonMass / 12.0;

    //fprintf(stdout,"mtot=%e,Ntot=%e,RelativeMolecularMass=%e,Molar=%e\n",mtot,Ntot,RelativeMolecularMass,MolarMass);
    //fflush(stdout);

    return MolarMass;
}

double AbsoluteMolecularMass(double RelativeMolecularMass){
    return RelativeMolecularMass * mC_CarbonMass / 12.0; // = MolarMass / NA_AvogadroConstant
}
