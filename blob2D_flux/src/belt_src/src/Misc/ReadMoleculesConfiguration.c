#include "pluto.h"
#include "ReadMoleculesConfiguration.h"
#include "Molecules.h"
#include "PhysicalConstantsCGS.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>



// *****************************
// ** ReadConfigurationFile() **
// *****************************
//
int ReadMoleculesConfigurationFile(){

	char ConfigurationFile[255];
	char line[120];
	char *name;
	char *number;

	FILE *FilePointer;
	strcpy(ConfigurationFile, "./Molecules.conf");

	if((FilePointer = fopen(ConfigurationFile, "r")) != NULL) {

	    memset(line, 0, sizeof(line));
	    while(fgets(line, sizeof(line), FilePointer)) {

	    	char *lp;
	    	size_t m,n;

	    	//
	    	// init
	    	//
	    	name = number = 0;

	    	//
	    	// exclude comments, empty lines and lines without '=':
	    	//
	    	if(line[0] != '#' && line [0] != '*' && line[0] != ' ' && line[0] != '\n' && strchr(line, '=')){

	    		lp = &line[0];

	    		//
	    		// determine name:
	    		//
	    		m = strspn(lp,"\t =");
	    		n = strcspn(lp+m,"\t =");
	    		if(n > 0){
	    			name = &lp[m];
	    			name[n] = '\0';
	    			lp = lp+m+n+1;
	    		}

	    		//
	    		// determine number:
	    		//
	    		m = strspn(lp,"\t =");
	    		n = strcspn(lp+m,"\t =#\n");
	    		if(n > 0){
	    			number = &lp[m];
	    			number[n] = '\0';
	    		}


	    		//
	    		// exclude non used parameter:
	    		//
	    		if(number && name){

	    			// *************************************
	    			// * Read data from configuration file *
	    			// *************************************
					     if(!strcmp(name, "mtot")                               ) mtot                               = strtod(number,0);
	    			else if(!strcmp(name, "Ntot")                               ) Ntot                               = strtod(number,0);
	    			else if(!strcmp(name, "NHatoms")                            ) NHatoms                            = strtod(number,0);
                    else if(!strcmp(name, "f_DegreesOfFreedom")                 ) f_DegreesOfFreedom                 = strtod(number,0);


	    		} // exclude non used parameter
	    	} // exlude comments, empty lines
	    } // Loop over lines in the configuration file

	    fclose(FilePointer);

	    return 0;
	}
	else{
		printf("ERROR # Unable to open specified Configuration File '%s'.\n", ConfigurationFile);
		exit(1);
	}
    
    // TODO: has to be done in code units after call to Reference System routine
    //mMH_AbsoluteMolecularMassOfHydrogen = AbsoluteMolecularMass(mtot/NH);
    
}
