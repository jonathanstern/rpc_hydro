#ifndef MAKEMAKETOOLS_H_
#define MAKEMAKETOOLS_H_
#include <petscvec.h>
#include <petscda.h>

int hunt(double*, int, double);
double LinearInterpolation1D(double, double, double, double, double);
double LinearInterpolation2D(double, double, double, double, double, double, double, double, double, double);
double LinearInterpolation3D(double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double);
double QuadraticInterpolation1D(double, double, double, double, double, double, double);

#endif /*MAKEMAKETOOLS_H_*/



