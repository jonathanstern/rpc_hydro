#include "pluto.h"
#include "PhysicalConstantsCGS.h"
#include "interface.h"
#include "Molecules.h"
#if RADIATION == YES
#include "Radiation.h"
#endif

#include <math.h>


//
// Distances
//
double AstronomicalUnit;
double Parsec;
double LightYear;
double SolarRadius;
double EarthRadius;

//
// Time
//
double Minute;
double Hour;
double Day;
double Year;

//
// Weights
//
double SolarMass;
double EarthMass;
double LunarMass;
double JupiterMass;

//
// Temperatures
//
double SolarEffectiveTemperature;

//
// Radiation
//
//double mue_MolarMass;
double f_DegreesOfFreedom;

double a_RadiationConstant;
double NA_AvogadroConstant;
double cv_SpecificHeat;
double c_SpeedOfLight;
double sigma_StefanBoltzmannConstant;

double k_BoltzmannConstant;
double h_PlanckConstant;
double hbar_DiracConstant;
double R_UniversalGasConstant;
double eV;

//
// Ionization
//
double u_AtomicMassUnit;
double mH_HydrogenMass;
double me_ElectronMass;
double mC_CarbonMass;

//
// Gravity
//
double G_GravityConstant;
double g_AccelerationOfGravity;

double SolarLuminosity;




int PhysicalConstantsCGS(){

	//
	// Distances
	//
	AstronomicalUnit = 1.49597870691e+13;   // in cm http://neo.jpl.nasa.gov/glossary/au.html
	Parsec = 206265.0 * AstronomicalUnit;   // in cm
    LightYear = 63241.0 * AstronomicalUnit; // in cm
	//SolarRadius = 0.004652 * AstronomicalUnit; // in cm
	SolarRadius = 6.955e+10;                // in cm
	EarthRadius = 637279760.0;              // in cm

    //
    // Time
    //
    Minute = 60.0;        // in s
    Hour = 60.0 * Minute; // in s
    Day = 24.0 * Hour;    // in s
    Year = 365.25 * Day;  // in s

	//
	// Weights
	//
	SolarMass   = 1.9891e+33;                    // in g
	EarthMass   = 1.0 /   332946.0  * SolarMass; // in g
	LunarMass   = 1.0 / 27068510.0  * SolarMass; // in g
	JupiterMass = 1.0 /     1047.56 * SolarMass; // in g

	//
	// Radiation
	//
	a_RadiationConstant = 7.5657e-15; // in erg * cm^-3 * K^-4
	c_SpeedOfLight = 2.99792458e+10; // in cm/s	
	k_BoltzmannConstant = 1.3807e-16; // in erg / K
	h_PlanckConstant = 6.62606896e-27; // in erg s
	hbar_DiracConstant = h_PlanckConstant / (2.0*M_PI);  // in erg s
	R_UniversalGasConstant = 8.314472e+7; // in g cm^2 s^-2 mol^-1 K^-1
	NA_AvogadroConstant = 6.02214129e+23; // in mol^-1 http://de.wikipedia.org/wiki/Avogadro-Konstante
	eV = 1.0 / 624150974451.150; // in erg
	
	//sigma_StefanBoltzmannConstant = 0.25 * a_RadiationConstant * c_SpeedOfLight; // in erg * cm^-2 * s-^1 * K^-4
	sigma_StefanBoltzmannConstant = 2.0 * pow(M_PI, 5) * pow(R_UniversalGasConstant, 4) / (15.0 * pow(h_PlanckConstant, 3) * pow(c_SpeedOfLight, 2) * pow(NA_AvogadroConstant, 4)); // in erg * cm^-2 * s-^1 * K^-4

	// TODO?
	//X_HydrogenFraction =
	//ElectronScatteringOpacity = 0.19 * (1 + X_HydrogenFraction); // in [cm^2 g^-1]
	
	//
	// Ionization
	//
	u_AtomicMassUnit = 1.6605389e-24;                      // in g
	mH_HydrogenMass  = 1.00794         * u_AtomicMassUnit; // in g
	me_ElectronMass  = 5.4857990946e-4 * u_AtomicMassUnit; // in g
    mC_CarbonMass    = 12.0 / NA_AvogadroConstant;         // in g
    
	//
	// Gravity
	//
	G_GravityConstant = 6.67428e-8;    // in cm^3 g^-1 s^-2
	g_AccelerationOfGravity = 980.665; // in cm/s^2

	SolarLuminosity = 3.839e+33;   // in erg s^-1 http://cdn.worldheritage.org/articles/Solar_luminosity
    //SolarLuminosity = 3.939e+33; // in erg s^-1 http://cdn.worldheritage.org/articles/Solar_luminosity (including solar neutrino radiation)
	//SolarLuminosity = 3.846e+33; // in erg s^-1 http://en.wikipedia.org/wiki/Solar_luminosity & http://solarscience.msfc.nasa.gov
	//SolarLuminosity = 3.826e+33; // in erg s^-1 https://www.cfa.harvard.edu/~dfabricant/huchra/ay145/units.html
	
	//
	// Temperatures
	//
	SolarEffectiveTemperature = pow(SolarLuminosity / (4.0 * M_PI * pow(SolarRadius, 2) * sigma_StefanBoltzmannConstant), 0.25); // in K
	
	return 0;
}




int PhysicalConstants2Reference(){

    //
	// Distances
	//
	AstronomicalUnit /= ReferenceLength;
	Parsec           /= ReferenceLength;
    LightYear        /= ReferenceLength;
	SolarRadius      /= ReferenceLength;
	EarthRadius      /= ReferenceLength;
    
    //
    // Time
    //
    Minute           /= ReferenceTime;
    Hour             /= ReferenceTime;
    Day              /= ReferenceTime;
    Year             /= ReferenceTime;
    
	//
	// Weights
	//
	SolarMass        /= ReferenceMass;
	EarthMass        /= ReferenceMass;
	LunarMass        /= ReferenceMass;
	JupiterMass      /= ReferenceMass;

	//
	// Radiation
	//
	a_RadiationConstant /= ReferenceEnergyDensity / pow(ReferenceTemperature, 4);
	c_SpeedOfLight /= ReferenceVelocity;
    sigma_StefanBoltzmannConstant /= pow(ReferenceVelocity, 3) * ReferenceDensity / pow(ReferenceTemperature, 4);
	
	k_BoltzmannConstant /= ReferenceEnergy / ReferenceTemperature;
	h_PlanckConstant    /= ReferenceEnergy * ReferenceTime;
	hbar_DiracConstant  /= ReferenceEnergy * ReferenceTime;
	R_UniversalGasConstant /= ReferenceMass * pow(ReferenceVelocity, 2) / ReferenceTemperature / ReferenceNumber;
	//mue_MolarMass /= ReferenceMass / ReferenceNumber;
    NA_AvogadroConstant *= ReferenceNumber;
    eV /= ReferenceEnergy;
    
	//
	// Ionization
	//
	u_AtomicMassUnit /= ReferenceMass;
	mH_HydrogenMass  /= ReferenceMass;
	me_ElectronMass  /= ReferenceMass;
    mC_CarbonMass    /= ReferenceMass;
    
	//
	//          f + 2         2
	// gamma = ------- = 1 + ---
	//            f           f
	//
	gamma_AdiabaticIndex = 1.0 + 2.0 / f_DegreesOfFreedom;
    
	//
	// Gravity
	//
	G_GravityConstant *= ReferenceDensity * pow(ReferenceTime, 2);
	g_AccelerationOfGravity /= ReferenceVelocity / ReferenceTime;


    SolarLuminosity /= ReferenceLuminosity;

	//
	// Temperatures
	//
	SolarEffectiveTemperature /= ReferenceTemperature;

#if DEBUGGING > 0
	//
	// check:
	//
    if(fabs(R_UniversalGasConstant / MolarMass(0) - 1.0) > 1e-6){
		printf("ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
		printf("ERROR\n");
		printf("ERROR R_UniversalGasConstant                 = %e in interface.c\n", R_UniversalGasConstant);
        printf("ERROR MolarMass(0)                           = %e g mol^-1\n", MolarMass(0) * ReferenceMass * ReferenceNumber);
		printf("ERROR MolarMass(0)                           = %e in interface.c\n", MolarMass(0));
		printf("ERROR R_UniversalGasConstant / MolarMass(0) = %e != 1 in interface.c\n", R_UniversalGasConstant / MolarMass(0));
		printf("ERROR\n");
		printf("ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR\n");
		exit(1);
	}
#endif
    
	return 0;
}



//double RelativeMolecularMass(double RelativeMolecularMassNeutral, double RelativeMolecularMassHydrogen, double IonizationFraction){
//    return 1.0 / (1.0/RelativeMolecularMassNeutral + IonizationFraction/RelativeMolecularMassHydrogen);
//}
//
//double MolarMass(double RelativeMolecularMass){
//    return RelativeMolecularMass * NA_AvogadroConstant * mC_CarbonMass / 12.0;
//}


