#ifndef PHYSICALCONSTANTSCGS_H_
#define PHYSICALCONSTANTSCGS_H_



//
// Distances
//
extern double AstronomicalUnit;
extern double Parsec;
extern double LightYear;
extern double SolarRadius;
extern double EarthRadius;

//
// Time
//
extern double Minute;
extern double Hour;
extern double Day;
extern double Year;

//
// Weights
//
extern double SolarMass;
extern double EarthMass;
extern double LunarMass;
extern double JupiterMass;

//
// Temperatures
//
extern double SolarEffectiveTemperature;

//
// Radiation
//
//extern double mue_MolarMass;
extern double f_DegreesOfFreedom;

extern double NA_AvogadroConstant;
extern double eV;
extern double a_RadiationConstant;
//extern double cv_SpecificHeat;
extern double c_SpeedOfLight;
extern double sigma_StefanBoltzmannConstant;

extern double k_BoltzmannConstant;
extern double h_PlanckConstant;
extern double hbar_DiracConstant;
extern double R_UniversalGasConstant;

//
// Ionization
//
extern double u_AtomicMassUnit;
extern double mH_HydrogenMass;
extern double me_ElectronMass;
extern double mC_CarbonMass;

//
// Gravity
//
extern double G_GravityConstant;
extern double g_AccelerationOfGravity;

extern double SolarLuminosity;




// ***********************
// ** External Routines **
// ***********************
//
int PhysicalConstantsCGS(void);
int PhysicalConstants2Reference(void);


#endif /*PHYSICALCONSTANTSCGS_H_*/
