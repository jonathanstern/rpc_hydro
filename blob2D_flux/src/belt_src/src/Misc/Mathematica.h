//#define Sqrt sqrt
double Sqrt(double x){return sqrt(x);}

//#define Power	pow
double Power(double x, double y){return pow(x,y);}

// Complex(0,1) = i will be neglected, only real solutions allowed for the physics solved 
//#define Complex(re,im) re
double Complex(double re, double im){return re;}

double Log(double x){return log(x);}

double Abs(double x){return fabs(x);}

double Cos(double x){return cos(x);}
double Sin(double x){return sin(x);}
double Tan(double x){return tan(x);}
double Cot(double x){return 1.0/tan(x);}
double ArcTan(double x){return atan(x);}
double ArcCot(double x){return atan(1.0/x);}

// x out of [-1 ... 1]
//double ArcTanh(double x){return 0.5 * log( (1.0 + x) / (1.0 - x) );}
double ArcTanh(double x){return atanh(x);}
