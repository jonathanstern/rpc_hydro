#include "pluto.h"
#include "interface.h"
#include "PhysicalConstantsCGS.h"

//#if RADIATION == YES || STELLAREVOLUTION == YES
//#include "PhysicalConstantsCGS.h"
//#endif

//#if RADIATION == YES
//#include "Radiation.h"
//#include "Opacity.h"
//#endif

//#if STELLAREVOLUTION == YES
//#include "StellarEvolution.h"
//#endif

#if SELFGRAVITY == YES
#include "SelfGravity.h"
#endif

//#if STELLAR_WIND > 0
//#include "StellarWind.h"
//#endif

//#if STORE_BOUNDARY_FLUX == YES
#include "boundary_fluxes.h"
//#endif

char buffer[256];

double TotalMass(const Data *d, Grid *grid){
	
	double TotalMass = 0;
#if GEOMETRY == SPHERICAL
	double dx3,dx2,dx1;
#endif
	
	
	int i,j,k;
	KDOM_LOOP(k){
#if GEOMETRY == SPHERICAL
		dx3 = grid[KDIR].xr[k] - grid[KDIR].xl[k];
		
#endif
		JDOM_LOOP(j){
#if GEOMETRY == SPHERICAL
			dx2 = -1*cos(grid[JDIR].xr[j]) + cos(grid[JDIR].xl[j]);
#endif
			IDOM_LOOP(i){
#if GEOMETRY == SPHERICAL
				dx1 = pow(grid[IDIR].xr[i],3) - pow(grid[IDIR].xl[i],3) ;
				TotalMass += d->Vc[DN][k][j][i] * dx3 * dx2 * dx1 / 3.0;
#else
				TotalMass += d->Vc[DN][k][j][i] * grid->dV[i];
#endif
			}
		}
	}
	
	//
	// convert to g:
	//
	//TotalMass *= g_unitDensity * pow(g_unitLength, 3);
	
	//
	// convert to solar mass:
	//
	//TotalMass /= CONST_Msun;
	
#if MIDPLANESYMMETRY == YES
	TotalMass *= 2;
#endif
	
	return TotalMass;
}


double TotalPlasmaBeta(const Data *d, Grid *grid){
	
	double TotalPlasmaBeta = 0;

#if PHYSICS == MHD
	int i,j,k,count;
	double B,P;

	count = 0;
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				B = sqrt(pow(d->Vc[BX1][k][j][i], 2) + pow(d->Vc[BX2][k][j][i], 2) + pow(d->Vc[BX3][k][j][i], 2));
#if EOS != ISOTHERMAL
				P = d->Vc[PRS][k][j][i];
#else
                P = d->Vc[RHO][k][j][i] * g_isoSoundSpeed*g_isoSoundSpeed;
#endif
				TotalPlasmaBeta += 2 * P / B;
				count++;
			}
		}
	}
	TotalPlasmaBeta /= count;
#endif
	
	return TotalPlasmaBeta;
}



double TotalMagneticFieldEnergy(const Data *d, Grid *grid){
	
	double TotalMagneticFieldEnergy = 0;
#if PHYSICS == MHD
	double B;
#if GEOMETRY == SPHERICAL
	double dx3,dx2,dx1;
#endif
	
	int i,j,k;
	KDOM_LOOP(k){
#if GEOMETRY == SPHERICAL
		dx3 = grid[KDIR].xr[k] - grid[KDIR].xl[k];
#endif
		JDOM_LOOP(j){
#if GEOMETRY == SPHERICAL
			dx2 = -1*cos(grid[JDIR].xr[j]) + cos(grid[JDIR].xl[j]);
#endif
			IDOM_LOOP(i){
				B = sqrt(pow(d->Vc[BX1][k][j][i], 2) + pow(d->Vc[BX2][k][j][i], 2) + pow(d->Vc[BX3][k][j][i], 2));
#if GEOMETRY == SPHERICAL
				dx1 = pow(grid[IDIR].xr[i],3) - pow(grid[IDIR].xl[i],3) ;
				TotalMagneticFieldEnergy += B * dx3 * dx2 * dx1 / 3.0;
#else
				TotalMagneticFieldEnergy += B * grid->dV[i];
#endif
			}
		}
	}
#endif
	
	TotalMagneticFieldEnergy *= ReferenceEnergy;
	
	return TotalMagneticFieldEnergy;
}


/*
 double TotalMass2FluxRatio(const Data *d, Grid *grid){
 
 double G = CONST_G * g_unitDensity * pow(g_unitLength / g_unitVelocity, 2) ; // G in code units
 double TotalPotentialEnergy = 0;
 int i,j,k;
 int i_,j_,k_; // to convert local PLUTO-indices to global PETSc-ones
 double x1;
 #if GEOMETRY == SPHERICAL
 double dx3,dx2,dx1;
 #endif
 
 KDOM_LOOP(k){
 #if GEOMETRY == SPHERICAL
 dx3 = grid[KDIR].xr[k] - grid[KDIR].xl[k];
 #endif
 JDOM_LOOP(j){
 #if GEOMETRY == SPHERICAL
 dx2 = -1*cos(grid[JDIR].xr[j]) + cos(grid[JDIR].xl[j]);
 #endif
 IDOM_LOOP(i){
 //
 // calculate global PETSc indices from local Pluto ones:
 //
 #if SELFGRAVITY == YES
 PlutoLocal2PETScGlobal(grid, k, j, i, &k_, &j_, &i_);
 #endif
 x1 = grid[IDIR].x[i];
 #if GEOMETRY == SPHERICAL
 dx1 = pow(grid[IDIR].xr[i],3) - pow(grid[IDIR].xl[i],3) ;
 #if SELFGRAVITY == YES
 if(SelfGravityFlag != 0)
 TotalPotentialEnergy += - d->Vc[DN][k][j][i] * GravityPotential[k_][j_][i_] * dx3 * dx2 * dx1 / 3.0;
 #endif
 #else
 #if SELFGRAVITY == YES
 if(SelfGravityFlag != 0){
 TotalPotentialEnergy += - d->Vc[DN][k][j][i] * GravityPotential[k][j][i] * grid->dV[i];
 #endif
 #endif
 //#if STORE_BOUNDARY_FLUX == YES
 TotalPotentialEnergy += d->Vc[DN][k][j][i] * G * M_X1_BEG / x1;
 //#elif STORE_BOUNDARY_FLUX == NO && RADIATION == YES
 //				TotalPotentialEnergy += d->Vc[DN][k][j][i] * G * MSTAR / x1;
 //#endif
 }
 }
 }
 
 return TotalPotentialEnergy;
 }
 */


double TotalSoundspeedEnergy(const Data *d, Grid *grid){
	
	double TotalSoundspeedEnergy = 0;
#if EOS != ISOTHERMAL
	
#if GEOMETRY == SPHERICAL
	double dx3,dx2,dx1;
#endif
	
	int i,j,k;
	KDOM_LOOP(k){
#if GEOMETRY == SPHERICAL
		dx3 = grid[KDIR].xr[k] - grid[KDIR].xl[k];
#endif
		JDOM_LOOP(j){
#if GEOMETRY == SPHERICAL
			dx2 = cos(grid[JDIR].xl[j]) - cos(grid[JDIR].xr[j]);
#endif
			IDOM_LOOP(i){
#if GEOMETRY == SPHERICAL
				dx1 = pow(grid[IDIR].xr[i],3) - pow(grid[IDIR].xl[i],3) ;
				TotalSoundspeedEnergy += 0.5 * g_gamma * d->Vc[PR][k][j][i] * dx3 * dx2 * dx1 / 3.0;
#else
				TotalSoundspeedEnergy += 0.5 * g_gamma * d->Vc[PR][k][j][i] * grid->dV[i];
#endif
//                printf("k = %d\n", k);
//                printf("j = %d\n", j);
//                printf("i = %d\n", i);
//                printf("d->Vc[PR][k][j][i] = %e\n", d->Vc[PR][k][j][i]);
//                printf("d->Vc[PRS][k][j][i] = %e\n", d->Vc[PRS][k][j][i]);
//                printf("dx3 = %e\n", dx3);
//                printf("dx2 = %e\n", dx2);
//                printf("dx1 = %e\n", dx1);

			}
		}
	}
#endif
	
	TotalSoundspeedEnergy *= ReferenceEnergy;
    
//    printf("TotalSoundspeedEnergy = %e\n", TotalSoundspeedEnergy);
//    printf("ReferenceEnergy = %e\n", ReferenceEnergy);
//    printf("g_gamma = %e\n", g_gamma);
//    //exit(1);
	
	return TotalSoundspeedEnergy;
}


double TotalKineticEnergyX(const Data *d, Grid *grid){
	
	double TotalKineticEnergyX = 0;
#if GEOMETRY == SPHERICAL
	double dx3,dx2,dx1;
#endif
	
	
	
	int i,j,k;
	KDOM_LOOP(k){
#if GEOMETRY == SPHERICAL
		dx3 = grid[KDIR].xr[k] - grid[KDIR].xl[k];
		
#endif
		JDOM_LOOP(j){
#if GEOMETRY == SPHERICAL
			dx2 = cos(grid[JDIR].xl[j]) - cos(grid[JDIR].xr[j]);
#endif
			IDOM_LOOP(i){
#if GEOMETRY == SPHERICAL
				dx1 = pow(grid[IDIR].xr[i], 3) - pow(grid[IDIR].xl[i], 3) ;
				TotalKineticEnergyX += 0.5 * d->Vc[DN][k][j][i] * pow(d->Vc[VX][k][j][i], 2) * dx3 * dx2 * dx1 / 3.0;
#else
				TotalKineticEnergyX += 0.5 * d->Vc[DN][k][j][i] * pow(d->Vc[VX][k][j][i], 2) * grid->dV[i];
#endif
			}
		}
	}
	
	TotalKineticEnergyX *= ReferenceEnergy;
	
	return TotalKineticEnergyX;
}


double TotalKineticEnergyY(const Data *d, Grid *grid){
	
	double TotalKineticEnergyY = 0;
#if GEOMETRY == SPHERICAL
	double dx3,dx2,dx1;
#endif
	
	
	int i,j,k;
	KDOM_LOOP(k){
#if GEOMETRY == SPHERICAL
		dx3 = grid[KDIR].xr[k] - grid[KDIR].xl[k];
#endif
		JDOM_LOOP(j){
#if GEOMETRY == SPHERICAL
			dx2 = cos(grid[JDIR].xl[j]) - cos(grid[JDIR].xr[j]);
#endif
			IDOM_LOOP(i){
#if GEOMETRY == SPHERICAL
				dx1 = pow(grid[IDIR].xr[i], 3) - pow(grid[IDIR].xl[i], 3) ;
				TotalKineticEnergyY += 0.5 * d->Vc[DN][k][j][i] * pow(d->Vc[VY][k][j][i], 2) * dx3 * dx2 * dx1 / 3.0;
#else
				
				TotalKineticEnergyY += 0.5 * d->Vc[DN][k][j][i] * pow(d->Vc[VY][k][j][i], 2) * grid->dV[i];
#endif
			}
		}
	}
	
	TotalKineticEnergyY *= ReferenceEnergy;
	
	return TotalKineticEnergyY;
}


double TotalKineticEnergyZ(const Data *d, Grid *grid){
	
	double TotalKineticEnergyZ = 0;
	
#if GEOMETRY == SPHERICAL
	double dx3,dx2,dx1;
#endif
	
	
	int i,j,k;
	KDOM_LOOP(k){
#if GEOMETRY == SPHERICAL
		dx3 = grid[KDIR].xr[k] - grid[KDIR].xl[k];
		
#endif
		JDOM_LOOP(j){
#if GEOMETRY == SPHERICAL
			dx2 = cos(grid[JDIR].xl[j]) - cos(grid[JDIR].xr[j]);
#endif
			IDOM_LOOP(i){
#if GEOMETRY == SPHERICAL
				dx1 = pow(grid[IDIR].xr[i], 3) - pow(grid[IDIR].xl[i], 3) ;
				TotalKineticEnergyZ += 0.5 * d->Vc[DN][k][j][i] * pow(d->Vc[VZ][k][j][i], 2) * dx3 * dx2 * dx1 / 3.0;
#else
				TotalKineticEnergyZ += 0.5 * d->Vc[DN][k][j][i] * pow(d->Vc[VZ][k][j][i], 2) * grid->dV[i];
#endif
			}
		}
	}
	
	TotalKineticEnergyZ *= ReferenceEnergy;
	
	return TotalKineticEnergyZ;
}


double TotalDeltaKineticEnergyZ(const Data *d, Grid *grid){
	
	double TotalKineticEnergyZ = 0;
#if GEOMETRY == SPHERICAL
	double dx3,dx2,dx1;
    double x1, x2;
    double KeplerSpeed;
#endif
	
	
	int i,j,k;
	KDOM_LOOP(k){
#if GEOMETRY == SPHERICAL
		dx3 = grid[KDIR].xr[k] - grid[KDIR].xl[k];
#endif
		JDOM_LOOP(j){
#if GEOMETRY == SPHERICAL
			dx2 = cos(grid[JDIR].xl[j]) - cos(grid[JDIR].xr[j]);
            x2 = grid[JDIR].x[j];
#endif
			IDOM_LOOP(i){
#if GEOMETRY == SPHERICAL
				dx1 = pow(grid[IDIR].xr[i], 3) - pow(grid[IDIR].xl[i], 3);
                x1 = grid[IDIR].x[i];
                
                KeplerSpeed  = sqrt(G_GravityConstant * M_X1_BEG / x1);
                KeplerSpeed *= sin(x2);
                //KeplerSpeed *= sqrt(1.0 + (beta_rho+beta_T)/g_gamma * HoR2);

				//TotalKineticEnergyZ += 0.5 * d->Vc[DN][k][j][i] * pow(d->Vc[VZ][k][j][i], 2) * dx3 * dx2 * dx1 / 3.0;
				TotalKineticEnergyZ += 0.5 * d->Vc[DN][k][j][i] * pow(d->Vc[VZ][k][j][i] - KeplerSpeed, 2) * dx3 * dx2 * dx1 / 3.0;
#else
				TotalKineticEnergyZ +=
				0.5 * d->Vc[DN][k][j][i]
				*
				pow(
					d->Vc[VZ][k][j][i]
					-
					pow(grid->x[i], -0.5)
					, 2)
				*
				grid->dV[i];
#endif
			}
		}
	}
	
	TotalKineticEnergyZ *= ReferenceEnergy;
	
	return TotalKineticEnergyZ;
}


double TotalPotentialEnergy(const Data *d, Grid *grid){
	
//#if PLUTO == 0
//	double G = CONST_G * g_unitDensity * pow(g_unitLength / g_unitVelocity, 2) ; // G in code units
//#elif PLUTO == 1
//    double G = CONST_G * UNIT_DENSITY * pow(UNIT_LENGTH / UNIT_VELOCITY, 2) ; // G in code units
//#endif
    double G = CONST_G * ReferenceDensity * pow(ReferenceLength / ReferenceVelocity, 2) ; // G in code units
	double TotalPotentialEnergy = 0;
	int i,j,k;
	double x1;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				x1 = grid[IDIR].x[i];
#if SELFGRAVITY == YES
                if(SelfGravityFlag != 0) TotalPotentialEnergy += - d->Vc[DN][k][j][i] * d->phi[k][j][i] * GridCellVolume(grid, k, j, i);
#endif
				TotalPotentialEnergy += d->Vc[DN][k][j][i] * G * M_X1_BEG / x1;
			}
		}
	}
	
	TotalPotentialEnergy *= ReferenceEnergy;
	
	return TotalPotentialEnergy;
}


double TotalThermalEnergy(const Data *d, Grid *grid){
	
	double TotalThermalEnergy = 0;
	
#if EOS != ISOTHERMAL
#if GEOMETRY == SPHERICAL
	double dx3,dx2,dx1;
#endif
	
	
	int i,j,k;
	KDOM_LOOP(k){
#if GEOMETRY == SPHERICAL
		dx3 = grid[KDIR].xr[k] - grid[KDIR].xl[k];
		
#endif
		JDOM_LOOP(j){
#if GEOMETRY == SPHERICAL
			dx2 = cos(grid[JDIR].xl[j]) - cos(grid[JDIR].xr[j]);
#endif
			IDOM_LOOP(i){
#if GEOMETRY == SPHERICAL
                // TODO: replace the volume dx1*dx2*dx3 with new routines GridCellVolume, which are independent of GEOMETRY
				dx1 = pow(grid[IDIR].xr[i], 3) - pow(grid[IDIR].xl[i], 3) ;
				TotalThermalEnergy += 1.0/(g_gamma-1) * d->Vc[PR][k][j][i] * dx3 * dx2 * dx1 / 3.0;
#else
				
				TotalThermalEnergy += 1.0/(g_gamma-1) * d->Vc[PR][k][j][i] * grid->dV[i];
#endif
				
			}
		}
	}
#endif
	
	TotalThermalEnergy *= ReferenceEnergy;
	
	return TotalThermalEnergy;
}


double DensityMin(const Data *d){
	
	int i,j,k;
	double Minimum = 1e+60;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Minimum = MIN(Minimum, d->Vc[DN][k][j][i]);
			}
		}
	}
	
	//
	// convert to g cm^-3:
	//
	Minimum *= ReferenceDensity;
	
	return Minimum;
}


double DensityMax(const Data *d){
	
	int i,j,k;
	double Maximum = 0.0;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Maximum = MAX(Maximum, d->Vc[DN][k][j][i]);
			}
		}
	}
	
	//
	// convert to g cm^-3:
	//
	Maximum *= ReferenceDensity;
	
	return Maximum;
}


double PressureMin(const Data *d){
	
	double Minimum = 1e+60;
	
#if EOS != ISOTHERMAL
	int i,j,k;
    
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Minimum = MIN(Minimum, d->Vc[PR][k][j][i]);
			}
		}
	}
	
	//
	// convert to erg/cm^3:
	//
	Minimum *= ReferencePressure;
	
#endif
	
	return Minimum;
}


double PressureMax(const Data *d){
	
	double Maximum = 0.0;
	
#if EOS != ISOTHERMAL
	int i,j,k;
    
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Maximum = MAX(Maximum, d->Vc[PR][k][j][i]);
			}
		}
	}
	
	//
	// convert to erg/cm^3:
	//
	Maximum *= ReferencePressure;
	
#endif
	
	return Maximum;
}


double TemperatureMin(const Data *d){
	
	double Minimum = 1e+60;
	
#if EOS != ISOTHERMAL
	int i,j,k;	
	double Temperature;
    
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Temperature = d->Vc[PR][k][j][i] / d->Vc[DN][k][j][i];
				Minimum = MIN(Minimum, Temperature);
			}
		}
	}
	
	//
	// convert to Kelvin:
	//
	Minimum *= ReferenceTemperature;
#endif
	
	return Minimum;
}


double TemperatureMax(const Data *d){
	
	double Maximum = 0.0;
	
#if EOS != ISOTHERMAL
	int i,j,k;
	double Temperature;

	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Temperature = d->Vc[PR][k][j][i] / d->Vc[DN][k][j][i];
				Maximum = MAX(Maximum, Temperature);
			}
		}
	}
	
	//
	// convert to Kelvin:
	//
	Maximum *= ReferenceTemperature;
#endif
	
	return Maximum;
}


double c_s(const Data *d, int k, int j, int i){
#if EOS == ISOTHERMAL
	return g_isoSoundSpeed;
#else
	return sqrt(g_gamma * d->Vc[PR][k][j][i] / d->Vc[DN][k][j][i]);
#endif
}


double VelocityXMax(const Data *d){
	
	int i,j,k;
	double Maximum = 0.0;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Maximum = MAX(Maximum, fabs(d->Vc[VX][k][j][i]));
			}
		}
	}
	
	//
	// convert to cm s^-1:
	//
	Maximum *= ReferenceVelocity;
	
	return Maximum;
}


double VelocityXMin(const Data *d){
	
	int i,j,k;
	double Minimum = 1e+60;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Minimum = MIN(Minimum, fabs(d->Vc[VX][k][j][i]));
			}
		}
	}
	
	//
	// convert to cm s^-1:
	//
	Minimum *= ReferenceVelocity;
	
	return Minimum;
}


double VelocityYMax(const Data *d){
	
	int i,j,k;
	double Maximum = 0.0;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Maximum = MAX(Maximum, fabs(d->Vc[VY][k][j][i]));
			}
		}
	}
	
	//
	// convert to cm s^-1:
	//
	Maximum *= ReferenceVelocity;
	
	return Maximum;
}


double VelocityYMin(const Data *d){
	
	int i,j,k;
	double Minimum = 1e+60;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Minimum = MIN(Minimum, fabs(d->Vc[VY][k][j][i]));
			}
		}
	}
	
	//
	// convert to cm s^-1:
	//
	Minimum *= ReferenceVelocity;
	
	return Minimum;
}


double VelocityZMax(const Data *d){
	
	int i,j,k;
	double Maximum = 0.0;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Maximum = MAX(Maximum, fabs(d->Vc[VZ][k][j][i]));
			}
		}
	}
	
	//
	// convert to cm s^-1:
	//
	Maximum *= ReferenceVelocity;
	
	return Maximum;
}


double VelocityZMin(const Data *d){
	
	int i,j,k;
	double Minimum = 1e+60;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Minimum = MIN(Minimum, fabs(d->Vc[VZ][k][j][i]));
			}
		}
	}
	
	//
	// convert to cm s^-1:
	//
	Minimum *= ReferenceVelocity;
	
	return Minimum;
}


double MachXMax(const Data *d){
	
	int i,j,k;
	double Maximum = 0.0;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Maximum = MAX(Maximum, fabs(d->Vc[VX][k][j][i]) / c_s(d, k,j,i));
			}
		}
	}
	
	return Maximum;
}


double MachXMin(const Data *d){
	
	int i,j,k;
	double Minimum = 1e+60;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Minimum = MIN(Minimum, fabs(d->Vc[VX][k][j][i]) / c_s(d, k,j,i));
			}
		}
	}
	
	return Minimum;
}


double MachYMax(const Data *d){
	
	int i,j,k;
	double Maximum = 0.0;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Maximum = MAX(Maximum, fabs(d->Vc[VY][k][j][i]) / c_s(d, k,j,i));
			}
		}
	}
	
	return Maximum;
}


double MachYMin(const Data *d){
	
	int i,j,k;
	double Minimum = 1e+60;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Minimum = MIN(Minimum, fabs(d->Vc[VY][k][j][i]) / c_s(d, k,j,i));
			}
		}
	}
	
	return Minimum;
}


double MachZMax(const Data *d){
	
	int i,j,k;
	double Maximum = 0.0;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Maximum = MAX(Maximum, fabs(d->Vc[VZ][k][j][i]) / c_s(d, k,j,i));
			}
		}
	}
	
	return Maximum;
}


double MachZMin(const Data *d){
	
	int i,j,k;
	double Minimum = 1e+60;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
				Minimum = MIN(Minimum, fabs(d->Vc[VZ][k][j][i]) / c_s(d, k,j,i));
			}
		}
	}
	
	return Minimum;
}


double MachMax(const Data *d){
	
	double Maximum = MachXMax(d);
#if COMPONENTS > 1
	Maximum = MAX(Maximum, MachYMax(d));
#endif
#if COMPONENTS > 2
	Maximum = MAX(Maximum, MachZMax(d));
#endif
	
	return Maximum;
}


double MachMin(const Data *d){
	
	double Minimum = MachXMin(d);
#if COMPONENTS > 1
	Minimum = MAX(Minimum, MachYMin(d));
#endif
#if COMPONENTS > 2
	Minimum = MAX(Minimum, MachZMin(d));
#endif
	
	return Minimum;
}


double JeansNumberMax(const Data *d, Grid *grid){
	
	int i, j, k;
	double dx, dy, dz, dxMax;
	double JeansLength, JeansNumber;
	double G = CONST_G * ReferenceDensity * pow(ReferenceLength / ReferenceVelocity, 2) ; // G in code units
	double Maximum = 0.0;
	
	KDOM_LOOP(k){
		JDOM_LOOP(j){
			IDOM_LOOP(i){
#if GEOMETRY == SPHERICAL
				dz = (grid[KDIR].xr[k] - grid[KDIR].xl[k]) * sin(grid[JDIR].x[j]) * grid[IDIR].x[i];
				dy = (grid[JDIR].xr[j] - grid[JDIR].xl[j]) * grid[IDIR].x[i];
#elif GEOMETRY == CYLINDRICAL
				dz = (grid[KDIR].xr[k] - grid[KDIR].xl[k]) * grid[IDIR].x[i];
				dy = grid[JDIR].xr[j] - grid[JDIR].xl[j];
#elif GEOMETRY == CARTESIAN
				dz = grid[KDIR].xr[k] - grid[KDIR].xl[k];
				dy = grid[JDIR].xr[j] - grid[JDIR].xl[j];
#endif
				dx = grid[IDIR].dx[i];
				dxMax = dx;
#if DIMENSIONS > 1
				dxMax = MAX(dxMax, dy);
#endif
#if DIMENSIONS > 2
				dxMax = MAX(dxMax, dz);
#endif
				JeansLength = sqrt(M_PI * pow(c_s(d, k, j, i), 2) / (G * d->Vc[DN][k][j][i]));
				JeansNumber = dxMax / JeansLength;
				Maximum = MAX(Maximum, JeansNumber);
			}
		}
	}
	
	return Maximum;
}




// *********************
// ** WriteAnalysis() **
// *********************
//
int WriteAnalysis(const Data *d, Grid *grid){
	
	FILE *AnalysisOut, *AnalysisInfo, *fopen();
	int col;
	
	//
	// local and global ('b...') scalar variables:
	//
	double  TM,  TS,  TKX,  TKY,  TKZ,  TDKZ,  TT,  TP,  DMin,  DMax,  PMin,  PMax,  TMin,  TMax;
	double bTM, bTS, bTKX, bTKY, bTKZ, bTDKZ, bTT, bTP, bDMin, bDMax, bPMin, bPMax, bTMin, bTMax;
	double  MMin,  MMax,  MXMin,  MXMax,  MYMin,  MYMax,  MZMin,  MZMax;
	double bMMin, bMMax, bMXMin, bMXMax, bMYMin, bMYMax, bMZMin, bMZMax;
	double  VXMin,  VXMax,  VYMin,  VYMax,  VZMin,  VZMax;
	double bVXMin, bVXMax, bVYMin, bVYMax, bVZMin, bVZMax;
	double bJeansMax, JeansMax;
	
#if PHYSICS == MHD
	double  beta,  BE;
    double bbeta, bBE;
#endif
    
	//
	// Compute local scalars:
	//
	TM    = TotalMass(d, grid);
	TS    = TotalSoundspeedEnergy(d, grid);
	TKX   = TotalKineticEnergyX(d, grid);
#if COMPONENTS > 1
	TKY   = TotalKineticEnergyY(d, grid);
#endif
#if COMPONENTS > 2
	TKZ   = TotalKineticEnergyZ(d, grid);
	TDKZ  = TotalDeltaKineticEnergyZ(d, grid);
#endif
	
	TT    = TotalThermalEnergy(d, grid);
	
	TP    = TotalPotentialEnergy(d, grid);
#if PHYSICS == MHD
	beta  = TotalPlasmaBeta(d, grid);
	BE    = TotalMagneticFieldEnergy(d, grid);
#endif
	DMin  = DensityMin(d);
	DMax  = DensityMax(d);
	PMin  = PressureMin(d);
	PMax  = PressureMax(d);
	TMin  = TemperatureMin(d);
	TMax  = TemperatureMax(d);
	VXMin = VelocityXMin(d);
	VXMax = VelocityXMax(d);
#if COMPONENTS > 1
	VYMin = VelocityYMin(d);
	VYMax = VelocityYMax(d);
#else
    VYMin = 0;
	VYMax = 0;
#endif
#if COMPONENTS > 2
	VZMin = VelocityZMin(d);
	VZMax = VelocityZMax(d);
#else
    VZMin = 0;
	VZMax = 0;
#endif
    
	MMin  = MachMin(d);
	MMax  = MachMax(d);
	MXMin = MachXMin(d);
	MXMax = MachXMax(d);
#if COMPONENTS > 1
	MYMin = MachYMin(d);
	MYMax = MachYMax(d);
#else
    MYMin = 0;
	MYMax = 0;
#endif
#if COMPONENTS > 2
	MZMin = MachZMin(d);
	MZMax = MachZMax(d);
#else
    MZMin = 0;
	MZMax = 0;
#endif
	JeansMax = JeansNumberMax(d, grid);
	
	//
	// Compute global scalars from local values via Reduction:
	//
	MPI_Reduce(&TM,    &bTM,    1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&TS,    &bTS,    1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&TKX,   &bTKX,   1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&TKY,   &bTKY,   1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&TKZ,   &bTKZ,   1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&TDKZ,  &bTDKZ,  1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&TT,    &bTT,    1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&TP,    &bTP,    1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
#if PHYSICS == MHD
	MPI_Reduce(&beta,  &bbeta,  1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&BE,    &bBE,    1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
#endif
	MPI_Reduce(&DMin,  &bDMin,  1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&DMax,  &bDMax,  1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&PMin,  &bPMin,  1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&PMax,  &bPMax,  1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&TMin,  &bTMin,  1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&TMax,  &bTMax,  1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    
	MPI_Reduce(&VXMin, &bVXMin, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&VXMax, &bVXMax, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&VYMin, &bVYMin, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&VYMax, &bVYMax, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&VZMin, &bVZMin, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&VZMax, &bVZMax, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

	MPI_Reduce(&MMin,  &bMMin,  1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&MMax,  &bMMax,  1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&MXMin, &bMXMin, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&MXMax, &bMXMax, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&MYMin, &bMYMin, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&MYMax, &bMYMax, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&MZMin, &bMZMin, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&MZMax, &bMZMax, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

	MPI_Reduce(&JeansMax, &bJeansMax, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	
	
	
	if(prank == 0){
		if(g_stepNumber == 0){
			sprintf(buffer, "%s%s", "./data/", "analysis.info");
			AnalysisInfo = fopen(buffer, "w");
			fprintf(AnalysisInfo, "################################################################################\n");
			fprintf(AnalysisInfo, "# Analysis output of scalar quantities:\n");
			fprintf(AnalysisInfo, "#\n");
			fprintf(AnalysisInfo, "#\n");
		}

		//
		// open data file for writing:
		//
        // TODO: currently, this yields overwriting data if restarted
		if(g_stepNumber == 0){
			sprintf(buffer, "%s%s", "./data/", "analysis.out");
			AnalysisOut = fopen(buffer, "w");
		}
		else{
			sprintf(buffer, "%s%s", "./data/", "analysis.out");
			AnalysisOut = fopen(buffer, "a");
		}
		
		
		
		//
		// Write data to file:
		//
		col = 1;
		
		if(g_stepNumber == 0){
			fprintf(AnalysisInfo, "# column %2d: g_stepNumber\n", col);
			col ++;
			fprintf(AnalysisInfo, "#\n");
			fprintf(AnalysisInfo, "#\n");
		}
		fprintf(AnalysisOut, "%ld ", g_stepNumber);
		
		
		if(g_stepNumber == 0){
			fprintf(AnalysisInfo, "#########\n");
			fprintf(AnalysisInfo, "# Times #\n");
			fprintf(AnalysisInfo, "#########\n");
			fprintf(AnalysisInfo, "#\n");
		}
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: g_time                   [code units]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", g_time);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: g_dt                     [code units]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", g_dt);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Wall-Clock-Time          [s]\n", col);
		if(g_stepNumber == 0) col ++;
		//		time(&tend);
		//		fprintf(AnalysisOut, "%e ", difftime(tend, tbeg));
		fprintf(AnalysisOut, "0 ");
		
		if(g_stepNumber == 0){
			fprintf(AnalysisInfo, "#\n");
			fprintf(AnalysisInfo, "#\n");
		}
		
		
		
		if(g_stepNumber == 0){
			fprintf(AnalysisInfo, "##############\n");
			fprintf(AnalysisInfo, "# Mass stuff #\n");
			fprintf(AnalysisInfo, "##############\n");
			fprintf(AnalysisInfo, "#\n");
		}
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Total Mass in computational domain [M_sol]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", bTM / SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "#\n");
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Total mass loss/gain at xbeg  [M_sol]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", M_X1_BEG / SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Total mass loss/gain at xend  [M_sol]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", M_X1_END / SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Total mass loss/gain at ybeg  [M_sol]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", M_X2_BEG / SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Total mass loss/gain at yend  [M_sol]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", M_X2_END / SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Total mass loss/gain at zbeg  [M_sol]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", M_X3_BEG / SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Total mass loss/gain at zend  [M_sol]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", M_X3_END / SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "#\n");
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Boundary inward  mass flux at xbeg [M_sol yr^-1]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", Flux_X1_BEG_INWARD_Global * Year/SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Boundary outward mass flux at xbeg [M_sol yr^-1]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", Flux_X1_BEG_OUTWARD_Global * Year/SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Boundary inward  mass flux at xend [M_sol yr^-1]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", Flux_X1_END_INWARD_Global  * Year/SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Boundary outward mass flux at xend [M_sol yr^-1]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", Flux_X1_END_OUTWARD_Global * Year/SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Boundary inward  mass flux at ybeg [M_sol yr^-1]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", Flux_X2_BEG_INWARD_Global  * Year/SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Boundary outward mass flux at ybeg [M_sol yr^-1]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", Flux_X2_BEG_OUTWARD_Global * Year/SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Boundary inward  mass flux at yend [M_sol yr^-1]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", Flux_X2_END_INWARD_Global  * Year/SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Boundary outward mass flux at yend [M_sol yr^-1]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", Flux_X2_END_OUTWARD_Global * Year/SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Boundary inward  mass flux at zbeg [M_sol yr^-1]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", Flux_X3_BEG_INWARD_Global  * Year/SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Boundary outward mass flux at zbeg [M_sol yr^-1]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", Flux_X3_BEG_OUTWARD_Global * Year/SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Boundary inward  mass flux at zend [M_sol yr^-1]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", Flux_X3_END_INWARD_Global  * Year/SolarMass);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Boundary outward mass flux at zend [M_sol yr^-1]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", Flux_X3_END_OUTWARD_Global * Year/SolarMass);
		
		if(g_stepNumber == 0){
			fprintf(AnalysisInfo, "#\n");
			fprintf(AnalysisInfo, "#\n");
		}
		
		
		if(g_stepNumber == 0){
			fprintf(AnalysisInfo, "############\n");
			fprintf(AnalysisInfo, "# Energies #\n");
			fprintf(AnalysisInfo, "############\n");
			fprintf(AnalysisInfo, "#\n");
		}
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: TotalSoundspeedEnergy    [erg]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", bTS);
        
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: TotalKineticEnergyX      [erg]\n", col);
		if(g_stepNumber == 0)col ++;
		fprintf(AnalysisOut, "%e ", bTKX);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: TotalKineticEnergyY      [erg]\n", col);
		if(g_stepNumber == 0) col ++;
#if COMPONENTS > 1
		fprintf(AnalysisOut, "%e ", bTKY);
#else
		fprintf(AnalysisOut, "0 ");
#endif
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: TotalKineticEnergyZ      [erg]\n", col);
		if(g_stepNumber == 0) col ++;
#if COMPONENTS > 2
		fprintf(AnalysisOut, "%e ", bTKZ);
#else
		fprintf(AnalysisOut, "0 ");
#endif
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: TotalDeltaKineticEnergyZ [erg]\n", col);
		if(g_stepNumber == 0) col ++;
#if COMPONENTS > 2
		fprintf(AnalysisOut, "%e ", bTDKZ);
#else
		fprintf(AnalysisOut, "0 ");
#endif
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: TotalThermalEnergy       [erg]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", bTT);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: TotalPotentialEnergy     [erg]\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", bTP);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: TotalMagneticFieldEnergy [erg]\n", col);
		if(g_stepNumber == 0) col ++;
#if PHYSICS == MHD
		fprintf(AnalysisOut, "%e ", bBE);
#else
		fprintf(AnalysisOut, "0 ");
#endif
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: TotalPlasmaBeta          [dimensionless]\n", col);
		if(g_stepNumber == 0) col ++;
#if PHYSICS == MHD
		fprintf(AnalysisOut, "%e ", bbeta);
#else
		fprintf(AnalysisOut, "0 ");
#endif
		
		if(g_stepNumber == 0){
			fprintf(AnalysisInfo, "#\n");
			fprintf(AnalysisInfo, "#\n");
		}
		
		
		
		if(g_stepNumber == 0){
			fprintf(AnalysisInfo, "#####################\n");
			fprintf(AnalysisInfo, "# Implicit solvers: #\n");
			fprintf(AnalysisInfo, "#####################\n");
			fprintf(AnalysisInfo, "#\n");
		}
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Self-Gravity: Convergence Reason\n", col);
		if(g_stepNumber == 0) col ++;
#if SELFGRAVITY == YES
		fprintf(AnalysisOut, "%d ", SelfGravity_ConvergedReason);
#else
		fprintf(AnalysisOut, "0 ");
#endif
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Self-Gravity: Used Iterations\n", col);
		if(g_stepNumber == 0) col ++;
#if SELFGRAVITY == YES
		fprintf(AnalysisOut, "%d ", SelfGravity_Iterations);
#else
		fprintf(AnalysisOut, "0 ");
#endif
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Self-Gravity: Residual Norm\n", col);
		if(g_stepNumber == 0) col ++;
#if SELFGRAVITY == YES
		fprintf(AnalysisOut, "%e ", SelfGravity_ResidualNorm);
#else
		fprintf(AnalysisOut, "0 ");
#endif
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: Inverse(Maximum Jeans Number)\n", col);
		if(g_stepNumber == 0) col ++;
		fprintf(AnalysisOut, "%e ", 1.0 / bJeansMax);
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: FLD: Convergence Reason\n", col);
		if(g_stepNumber == 0) col ++;
		//#if RADIATION == YES
		//        fprintf(AnalysisOut, "%d ", FLD_ConvergedReason);
		//#else
		fprintf(AnalysisOut, "0 ");
		//#endif
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: FLD: Used Iterations\n", col);
		if(g_stepNumber == 0) col ++;
		//#if RADIATION == YES
		//        fprintf(AnalysisOut, "%d ", FLD_Iterations);
		//#else
		fprintf(AnalysisOut, "0 ");
		//#endif
		
		if(g_stepNumber == 0) fprintf(AnalysisInfo, "# column %2d: FLD: Residual Norm\n", col);
		if(g_stepNumber == 0) col ++;
		//#if RADIATION == YES
		//        fprintf(AnalysisOut, "%e ", FLD_ResidualNorm);
		//#else
		fprintf(AnalysisOut, "0 ");
		//#endif
		
		
		if(g_stepNumber == 0){
			fprintf(AnalysisInfo, "#\n");
			fprintf(AnalysisInfo, "#\n");
		}
		
		
		//
		// Stellar Evolution:
		//
		if(g_stepNumber == 0){
			fprintf(AnalysisInfo, "#####################\n");
			fprintf(AnalysisInfo, "# Stellar Evolution #\n");
			fprintf(AnalysisInfo, "#####################\n");
			fprintf(AnalysisInfo, "#\n");
			fprintf(AnalysisInfo, "# column %2d: Stellar   Mass        [M_sol]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Stellar   Radius      [R_sol]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Stellar   Luminosity  [L_sol]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Accretion Luminosity  [L_sol]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Total     Luminosity  [L_sol]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Stellar   Temperature [K]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Accretion Temperature [K]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Total     Temperature [K]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Eddington factor regarding kappa_Planck(TotalTemperature)\n", col);
			col ++;
			fprintf(AnalysisInfo, "#\n");
			fprintf(AnalysisInfo, "#\n");
		}
		
		//#if STELLAREVOLUTION == YES
		//		fprintf(AnalysisOut, "%e ", StellarMass                 / SolarMass);
		//		fprintf(AnalysisOut, "%e ", StellarRadius             / SolarRadius);
		//        fprintf(AnalysisOut, "%e ", StellarLuminosity     / SolarLuminosity);
		//        fprintf(AnalysisOut, "%e ", AccretionLuminosity   / SolarLuminosity);
		//        fprintf(AnalysisOut, "%e ", TotalLuminosity       / SolarLuminosity);
		//        fprintf(AnalysisOut, "%e ", StellarTemperature   * ReferenceTemperature);
		//        fprintf(AnalysisOut, "%e ", AccretionTemperature * ReferenceTemperature);
		//        fprintf(AnalysisOut, "%e ", TotalTemperature     * ReferenceTemperature);
		//#if RADIATION == YES
		//        fprintf(AnalysisOut, "%e ", TotalLuminosity * PlanckMeanOpacity(TotalTemperature, 1e-12 / ReferenceDensity) / (StellarMass * 4.0*M_PI * CONST_G*g_unitDensity*pow(ReferenceTime,2) * CONST_c/g_unitVelocity));
		//#else
		//fprintf(AnalysisOut, "0 ");
		//#endif
		//#else
		fprintf(AnalysisOut, "0 ");
		fprintf(AnalysisOut, "0 ");
		fprintf(AnalysisOut, "0 ");
		fprintf(AnalysisOut, "0 ");
		fprintf(AnalysisOut, "0 ");
		fprintf(AnalysisOut, "0 ");
		fprintf(AnalysisOut, "0 ");
		fprintf(AnalysisOut, "0 ");
		fprintf(AnalysisOut, "0 ");
		//#endif
		
		//
		// Stellar Wind:
		//
		//#if STELLAR_WIND > 0
		//        fprintf(AnalysisOut, "%e ", StellarWind_MassLossRate * g_unitDensity * pow(g_unitLength, 3) / ReferenceTime * Year / CONST_Msun);
		//        fprintf(AnalysisOut, "%e ", StellarWind_Velocity     * g_unitVelocity * 1e-5);
		//        fprintf(AnalysisOut, "%e ", StellarWind_Density      * g_unitDensity);
		//        fprintf(AnalysisOut, "%e ", StellarWind_EddingtonFactor);
		//#else
		//		fprintf(AnalysisOut, "0 ");
		//		fprintf(AnalysisOut, "0 ");
		//		fprintf(AnalysisOut, "0 ");
		//		fprintf(AnalysisOut, "0 ");
		//#endif
		
		
		if(g_stepNumber == 0){
			fprintf(AnalysisInfo, "###########\n");
			fprintf(AnalysisInfo, "# Extrema #\n");
			fprintf(AnalysisInfo, "###########\n");
			fprintf(AnalysisInfo, "#\n");
			fprintf(AnalysisInfo, "# column %2d: Minimum Gas Density     [g cm^-3]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Maximum Gas Density     [g cm^-3]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Minimum Gas Pressure    [erg cm^-3]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Maximum Gas Pressure    [erg cm^-3]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Minimum Gas Temperature [K]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Maximum Gas Temperature [K]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Minimum Velocity X  [cm s^-1]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Maximum Velocity X  [cm s^-1]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Minimum Velocity Y  [cm s^-1]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Maximum Velocity Y  [cm s^-1]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Minimum Velocity Z  [cm s^-1]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Maximum Velocity Z  [cm s^-1]\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Minimum Mach\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Maximum Mach\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Minimum Mach X\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Maximum Mach X\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Minimum Mach Y\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Maximum Mach Y\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Minimum Mach Z\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: Maximum Mach Z\n", col);
			col ++;
			fprintf(AnalysisInfo, "# column %2d: g_maxMach = Maximum Mach Pluto\n", col);
			col ++;
		}
		fprintf(AnalysisOut, "%e ", bDMin);
		fprintf(AnalysisOut, "%e ", bDMax);
		fprintf(AnalysisOut, "%e ", bPMin);
		fprintf(AnalysisOut, "%e ", bPMax);
		fprintf(AnalysisOut, "%e ", bTMin);
		fprintf(AnalysisOut, "%e ", bTMax);
        
		fprintf(AnalysisOut, "%e ", bVXMin);
		fprintf(AnalysisOut, "%e ", bVXMax);
		fprintf(AnalysisOut, "%e ", bVYMin);
		fprintf(AnalysisOut, "%e ", bVYMax);
		fprintf(AnalysisOut, "%e ", bVZMin);
		fprintf(AnalysisOut, "%e ", bVZMax);

		fprintf(AnalysisOut, "%e ", bMMin);
		fprintf(AnalysisOut, "%e ", bMMax);
		fprintf(AnalysisOut, "%e ", bMXMin);
		fprintf(AnalysisOut, "%e ", bMXMax);
		fprintf(AnalysisOut, "%e ", bMYMin);
		fprintf(AnalysisOut, "%e ", bMYMax);
		fprintf(AnalysisOut, "%e ", bMZMin);
		fprintf(AnalysisOut, "%e ", bMZMax);
		
		fprintf(AnalysisOut, "%e ", g_maxMach);
		
		
		if(g_stepNumber == 0){
			fprintf(AnalysisInfo, "################################################################################\n");
			//
			// close info file:
			//
			fclose(AnalysisInfo);
		}
		
		//
		// end line:
		//
		fprintf(AnalysisOut, "\n");
		
		//
		// close file:
		//
		fclose(AnalysisOut);
		
	} // if rank 0
	
	return 0;
}
