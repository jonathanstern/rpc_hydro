#include "pluto.h"
#if SELFGRAVITY == YES || RADIATION == YES || IONIZATION == YES
#include "DomainDecomposition.h"
#endif




/* ********************************************************************* */
double GridCellVolume(Grid *pgrid, int k, int j, int i)
/*
 * Return cell volume at i,j,k
 *
 *********************************************************************** */
{
    
    return pgrid[IDIR].dV[i] * pgrid[JDIR].dV[j] * pgrid[KDIR].dV[k];
    
//    double dV;
// 
//    double dx, dy, dz;
//    
//#if GEOMETRY == CARTESIAN
//    dx = pgrid[IDIR].dx[i];
//    dy = pgrid[JDIR].dx[j];
//    dz = pgrid[KDIR].dx[k];
//#elif GEOMETRY == CYLINDRICAL
//    dx = pgrid[IDIR].dx[i];
//    dy = pgrid[JDIR].dx[j];
//    dz = pgrid[IDIR].x[i] * pgrid[KDIR].dx[k];
//#elif GEOMETRY == POLAR
//    dx = pgrid[IDIR].dx[i];
//    dy = pgrid[IDIR].x[i] * pgrid[JDIR].dx[j];
//    dz = pgrid[KDIR].dx[k];
//#elif GEOMETRY == SPHERICAL
//    dx = pgrid[IDIR].dx[i];
//    dy = pgrid[IDIR].x[i] * (cos(pgrid[JDIR].xl[j]) - cos(pgrid[JDIR].xr[j]));
//    dz = pgrid[IDIR].x[i] * pgrid[KDIR].dx[k];
//#endif
//    
//    dV = dx*dy*dz;
//    
//    return dV;
}




/* ********************************************************************* */
double GridCellArea(Grid *pgrid, int dir, int k, int j, int i)
/*
 *
 *
 *
 *********************************************************************** */
{
    
    double dA = -1.0;
    
#if GEOMETRY == CARTESIAN
    
    double dx, dy, dz;
        
    dx = pgrid[IDIR].dx[i];
    dy = pgrid[JDIR].dx[j];
    dz = pgrid[KDIR].dx[k];
        
    if      (dir == IDIR) dA = dy * dz;
    else if (dir == JDIR) dA = dz * dx;
    else if (dir == KDIR) dA = dx * dy;

#elif GEOMETRY == CYLINDRICAL

    double Rm, R, dR, dp, dz;
        
    R  = pgrid[IDIR].x[i];
    Rm = pgrid[IDIR].xl[i];
    dR = pgrid[IDIR].dx[i];
    dz = pgrid[JDIR].dx[j];
    dp = pgrid[KDIR].dx[k];
        
    if      (dir == IDIR) dA = Rm*dp * dz;
    else if (dir == JDIR) dA = dR * R*dp;
    else if (dir == KDIR) dA = dR * dz;

#elif GEOMETRY == POLAR

    double Rm, R, dR, dp, dz;

    R  = pgrid[IDIR].x[i];
    Rm = pgrid[IDIR].xl[i];
    dR = pgrid[IDIR].dx[i];
    dp = pgrid[JDIR].dx[j];
    dz = pgrid[KDIR].dx[k];
        
    if      (dir == IDIR) dA = Rm*dp * dz;
    else if (dir == JDIR) dA = dR * dz;
    else if (dir == KDIR) dA = dR * R*dp;

#elif GEOMETRY == SPHERICAL

    double r, rm, dr, dmu, dp, sinm, dt;

    rm  = pgrid[IDIR].xl[i];
    r   = pgrid[IDIR].x[i];
    dr  = pgrid[IDIR].dx[i];
    dmu = pgrid[JDIR].dV[j];
    // remark: fabs() is required for positive areas in the ghost cells
    sinm   = fabs(sin(pgrid[JDIR].xl[j]));
    dt = pgrid[JDIR].dx[j];
    dp = pgrid[KDIR].dx[k];
    
    if      (dir == IDIR) dA = rm*dp * rm*dmu;
    else if (dir == JDIR) dA = dr * r*sinm*dp;
    else if (dir == KDIR) dA = dr * r*dt;

#endif
    
    return dA;

}




double GridCellLength(Grid *pgrid, int dir, int k, int j, int i)
{

    double dl = -1.0;
    
#if GEOMETRY == CARTESIAN
    
    if      (dir == IDIR) dl = pgrid[IDIR].dx[i];
    else if (dir == JDIR) dl = pgrid[JDIR].dx[j];
    else if (dir == KDIR) dl = pgrid[KDIR].dx[k];

#elif GEOMETRY == CYLINDRICAL

    double R;
    R = pgrid[IDIR].x[i];

    if      (dir == IDIR) dl = pgrid[IDIR].dx[i];
    else if (dir == JDIR) dl = pgrid[JDIR].dx[j];
    else if (dir == KDIR) dl = pgrid[KDIR].dx[k] * R;

#elif GEOMETRY == POLAR

    double R;
    R = pgrid[IDIR].x[i];
    
    if      (dir == IDIR) dl = pgrid[IDIR].dx[i];
    else if (dir == JDIR) dl = pgrid[JDIR].dx[j] * R;
    else if (dir == KDIR) dl = pgrid[KDIR].dx[k];

#elif GEOMETRY == SPHERICAL

    double r, t;
    r = pgrid[IDIR].x[i];
    t = pgrid[JDIR].x[j];
    
    if      (dir == IDIR) dl = pgrid[IDIR].dx[i];
    else if (dir == JDIR) dl = pgrid[JDIR].dx[j] * r;
    else if (dir == KDIR) dl = pgrid[KDIR].dx[k] * r * sin(t);

#endif

    return dl;

}




void PlutoLocal2PETScGlobal(Grid *grid, int kl, int jl, int il, int *kg, int *jg, int *ig){
    
	*ig = il + grid[IDIR].beg - 2*grid[IDIR].nghost;
	if(!PERIODICX) *ig += 1;

#if DIMENSIONS > 1
	*jg = jl + grid[JDIR].beg - 2*grid[JDIR].nghost;
	if(!PERIODICY) *jg += 1;
#else
	*jg = 0;
#endif

#if DIMENSIONS > 2
	*kg = kl + grid[KDIR].beg - 2*grid[KDIR].nghost;
	if(!PERIODICZ) *kg += 1;
#else
	*kg = 0;
#endif
	
}
