#ifndef READRESTRICTIONSCONFIGURATION_H_
#define READRESTRICTIONSCONFIGURATION_H_



extern double RHO_min             , RHO_max             ;
extern double PRS_min             , PRS_max             ;
extern double VX1_min             , VX1_max             ;
extern double VX2_min             , VX2_max             ;
extern double VX3_min             , VX3_max             ;
extern double T_dust_min          , T_dust_max          ;
extern double T_gas_min           , T_gas_max           ;
extern double T_rad_min           , T_rad_max           ;
extern double DEBUGGING_RHO_min   , DEBUGGING_RHO_max   ;
extern double DEBUGGING_PRS_min   , DEBUGGING_PRS_max   ;
extern double DEBUGGING_VX1_min   , DEBUGGING_VX1_max   ;
extern double DEBUGGING_VX2_min   , DEBUGGING_VX2_max   ;
extern double DEBUGGING_VX3_min   , DEBUGGING_VX3_max   ;
extern double DEBUGGING_T_gas_min, DEBUGGING_T_gas_max;
extern double DEBUGGING_T_dust_min, DEBUGGING_T_dust_max;

extern int    RHO_min_Conserve_Momentum;
extern int    RHO_min_Conserve_Energy;


//
// External routines:
//
int  ReadRestrictionsConfigurationFile(void);
void ApplyRestrictions(const Data *, Grid *);


#endif /*READRESTRICTIONSCONFIGURATION_H_*/
