#ifndef BOUNDARY_FLUXES_H_
#define BOUNDARY_FLUXES_H_


//
// Fluxes over Boundaries:
//
extern double Flux_X1_BEG_INWARD_Global;
extern double Flux_X1_END_INWARD_Global;
extern double Flux_X2_BEG_INWARD_Global;
extern double Flux_X2_END_INWARD_Global;
extern double Flux_X3_BEG_INWARD_Global;
extern double Flux_X3_END_INWARD_Global;
extern double Flux_X1_BEG_OUTWARD_Global;
extern double Flux_X1_END_OUTWARD_Global;
extern double Flux_X2_BEG_OUTWARD_Global;
extern double Flux_X2_END_OUTWARD_Global;
extern double Flux_X3_BEG_OUTWARD_Global;
extern double Flux_X3_END_OUTWARD_Global;

extern double Flux_X1_BEG_INWARD_Local;
extern double Flux_X1_END_INWARD_Local;
extern double Flux_X2_BEG_INWARD_Local;
extern double Flux_X2_END_INWARD_Local;
extern double Flux_X3_BEG_INWARD_Local;
extern double Flux_X3_END_INWARD_Local;
extern double Flux_X1_BEG_OUTWARD_Local;
extern double Flux_X1_END_OUTWARD_Local;
extern double Flux_X2_BEG_OUTWARD_Local;
extern double Flux_X2_END_OUTWARD_Local;
extern double Flux_X3_BEG_OUTWARD_Local;
extern double Flux_X3_END_OUTWARD_Local;

//
// Total mass loss/gain = Integrated fluxes over time:
//
extern double M_X1_BEG;
extern double M_X1_END;
extern double M_X2_BEG;
extern double M_X2_END;
extern double M_X3_BEG;
extern double M_X3_END;


//
// External Routines:
//
void ComputeBoundaryFluxes(void);
void StoreBoundaryFluxes(const State_1D *, Grid *, int, int, int, double);

#endif
