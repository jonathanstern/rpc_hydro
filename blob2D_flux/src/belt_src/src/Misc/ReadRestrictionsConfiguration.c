#include "pluto.h"
#include "ReadRestrictionsConfiguration.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>



double RHO_min             , RHO_max             ;
double PRS_min             , PRS_max             ;
double VX1_min             , VX1_max             ;
double VX2_min             , VX2_max             ;
double VX3_min             , VX3_max             ;
double T_dust_min          , T_dust_max          ;
double T_gas_min           , T_gas_max           ;
double T_rad_min           , T_rad_max           ;
double DEBUGGING_RHO_min   , DEBUGGING_RHO_max   ;
double DEBUGGING_PRS_min   , DEBUGGING_PRS_max   ;
double DEBUGGING_VX1_min   , DEBUGGING_VX1_max   ;
double DEBUGGING_VX2_min   , DEBUGGING_VX2_max   ;
double DEBUGGING_VX3_min   , DEBUGGING_VX3_max   ;
double DEBUGGING_T_dust_min, DEBUGGING_T_dust_max;
double DEBUGGING_T_gas_min , DEBUGGING_T_gas_max ;

int RHO_min_Conserve_Momentum;
int RHO_min_Conserve_Energy;



// *****************************
// ** ReadConfigurationFile() **
// *****************************
//
int ReadRestrictionsConfigurationFile(){

	char ConfigurationFile[255];
	char line[120];
	char *name;
	char *number;

	FILE *FilePointer;
	strcpy(ConfigurationFile, "./Restrictions.conf");

	if((FilePointer = fopen(ConfigurationFile, "r")) != NULL) {

	    memset(line, 0, sizeof(line));
	    while(fgets(line, sizeof(line), FilePointer)) {

	    	char *lp;
	    	size_t m,n;

	    	//
	    	// init
	    	//
	    	name = number = 0;

	    	//
	    	// exclude comments, empty lines and lines without '=':
	    	//
	    	if(line[0] != '#' && line [0] != '*' && line[0] != ' ' && line[0] != '\n' && strchr(line, '=')){

	    		lp = &line[0];

	    		//
	    		// determine name:
	    		//
	    		m = strspn(lp,"\t =");
	    		n = strcspn(lp+m,"\t =");
	    		if(n > 0){
	    			name = &lp[m];
	    			name[n] = '\0';
	    			lp = lp+m+n+1;
	    		}

	    		//
	    		// determine number:
	    		//
	    		m = strspn(lp,"\t =");
	    		n = strcspn(lp+m,"\t =#\n");
	    		if(n > 0){
	    			number = &lp[m];
	    			number[n] = '\0';
	    		}


	    		//
	    		// exclude non used parameter:
	    		//
	    		if(number && name){

	    			// *************************************
	    			// * Read data from configuration file *
	    			// *************************************
					     if(!strcmp(name, "RHO_min")                            ) RHO_min                            = strtod(number,0);
	    			else if(!strcmp(name, "RHO_max")                            ) RHO_max                            = strtod(number,0);
	    			else if(!strcmp(name, "PRS_min")                            ) PRS_min                            = strtod(number,0);
                    else if(!strcmp(name, "PRS_max")                            ) PRS_max                            = strtod(number,0);
	    			else if(!strcmp(name, "VX1_min")                            ) VX1_min                            = strtod(number,0);
                    else if(!strcmp(name, "VX1_max")                            ) VX1_max                            = strtod(number,0);
	    			else if(!strcmp(name, "VX2_min")                            ) VX2_min                            = strtod(number,0);
                    else if(!strcmp(name, "VX2_max")                            ) VX2_max                            = strtod(number,0);
	    			else if(!strcmp(name, "VX3_min")                            ) VX3_min                            = strtod(number,0);
                    else if(!strcmp(name, "VX3_max")                            ) VX3_max                            = strtod(number,0);
	    			else if(!strcmp(name, "T_dust_min")                         ) T_dust_min                         = strtod(number,0);
                    else if(!strcmp(name, "T_dust_max")                         ) T_dust_max                         = strtod(number,0);
	    			else if(!strcmp(name, "T_gas_min")                          ) T_gas_min                          = strtod(number,0);
                    else if(!strcmp(name, "T_gas_max")                          ) T_gas_max                          = strtod(number,0);
                    else if(!strcmp(name, "T_rad_min")                          ) T_rad_min                          = strtod(number,0);
                    else if(!strcmp(name, "T_rad_max")                          ) T_rad_max                          = strtod(number,0);
                    else if(!strcmp(name, "DEBUGGING_RHO_min")                  ) DEBUGGING_RHO_min                  = strtod(number,0);
                    else if(!strcmp(name, "DEBUGGING_RHO_max")                  ) DEBUGGING_RHO_max                  = strtod(number,0);
	    			else if(!strcmp(name, "DEBUGGING_PRS_min")                  ) DEBUGGING_PRS_min                  = strtod(number,0);
                    else if(!strcmp(name, "DEBUGGING_PRS_max")                  ) DEBUGGING_PRS_max                  = strtod(number,0);
	    			else if(!strcmp(name, "DEBUGGING_VX1_min")                  ) DEBUGGING_VX1_min                  = strtod(number,0);
                    else if(!strcmp(name, "DEBUGGING_VX1_max")                  ) DEBUGGING_VX1_max                  = strtod(number,0);
	    			else if(!strcmp(name, "DEBUGGING_VX2_min")                  ) DEBUGGING_VX2_min                  = strtod(number,0);
                    else if(!strcmp(name, "DEBUGGING_VX2_max")                  ) DEBUGGING_VX2_max                  = strtod(number,0);
	    			else if(!strcmp(name, "DEBUGGING_VX3_min")                  ) DEBUGGING_VX3_min                  = strtod(number,0);
                    else if(!strcmp(name, "DEBUGGING_VX3_max")                  ) DEBUGGING_VX3_max                  = strtod(number,0);
	    			else if(!strcmp(name, "DEBUGGING_T_dust_min")               ) DEBUGGING_T_dust_min               = strtod(number,0);
                    else if(!strcmp(name, "DEBUGGING_T_dust_max")               ) DEBUGGING_T_dust_max               = strtod(number,0);

                    else if(!strcmp(name, "RHO_min_Conserve_Momentum")          ) RHO_min_Conserve_Momentum          = strtol(number,NULL,10);
                    else if(!strcmp(name, "RHO_min_Conserve_Energy")            ) RHO_min_Conserve_Energy            = strtol(number,NULL,10);

	    		} // exclude non used parameter
	    	} // exlude comments, empty lines
	    } // Loop over lines in the configuration file

	    fclose(FilePointer);

	    return 0;
	}
	else{
		printf("ERROR # Unable to open specified Configuration File '%s'.\n", ConfigurationFile);
		exit(1);
	}
    
    // TODO: has to be done in code units after call to Reference System routine
    //mMH_AbsoluteMolecularMassOfHydrogen = AbsoluteMolecularMass(mtot/NH);
    
}



//
// Internal boundary conditions / Restrictions:
//
void ApplyRestrictions(const Data *d, Grid *grid){
    
    int    k,j,i;
    double RHO_old, RHO_new;
#if EOS != ISOTHERMAL
    double PRS_old, PRS_new;
#endif
    double VX1_old, VX1_new;
#if COMPONENTS > 1
    double VX2_old, VX2_new;
#endif
#if COMPONENTS > 2
    double VX3_old, VX3_new;
#endif
    //#if PHYSICS == MHD
    //    double BX1_old, BX1_new;
    //#if COMPONENTS > 1
    //    double BX2_old, BX2_new;
    //#endif
    //#if COMPONENTS > 2
    //    double BX3_old, BX3_new;
    //#endif
    //#endif
    
    
    TOT_LOOP(k,j,i){
        
        //
        // Get current variables:
        //
        RHO_old = d->Vc[RHO][k][j][i];
#if EOS != ISOTHERMAL
        PRS_old = d->Vc[PRS][k][j][i];
#endif
        VX1_old = d->Vc[VX1][k][j][i];
#if COMPONENTS > 1
        VX2_old = d->Vc[VX2][k][j][i];
#endif
#if COMPONENTS > 2
        VX3_old = d->Vc[VX3][k][j][i];
#endif
        
        //
        // Initialize new variables:
        //
        RHO_new = RHO_old;
#if EOS != ISOTHERMAL
        PRS_new = PRS_old;
#endif
        VX1_new = VX1_old;
#if COMPONENTS > 1
        VX2_new = VX2_old;
#endif
#if COMPONENTS > 2
        VX3_new = VX3_old;
#endif
        
        //
        // Apply density restrictions:
        //
        if(RHO_min   >= 0) RHO_new = MAX(RHO_new, RHO_min);
        if(RHO_max   >= 0) RHO_new = MIN(RHO_new, RHO_max);
        
        //
        // Conserve momentum in case of density restrictions:
        //
        if(RHO_min_Conserve_Momentum){
            VX1_new = RHO_old/RHO_new * VX1_old;
#if COMPONENTS > 1
            VX2_new = RHO_old/RHO_new * VX2_old;
#endif
#if COMPONENTS > 2
            VX3_new = RHO_old/RHO_new * VX3_old;
#endif
        }
        
        //
        // Conserve kinetic energy in case of density restrictions:
        //
        if(RHO_min_Conserve_Energy){
            VX1_new = sqrt(RHO_old/RHO_new) * VX1_old;
#if COMPONENTS > 1
            VX2_new = sqrt(RHO_old/RHO_new) * VX2_old;
#endif
#if COMPONENTS > 2
            VX3_new = sqrt(RHO_old/RHO_new) * VX2_old;
#endif
        }
        
        //
        // Apply pressure and temperature restrictions:
        //
#if EOS != ISOTHERMAL
        if(PRS_min   >= 0) PRS_new = MAX(PRS_old, PRS_min);
        if(PRS_max   >= 0) PRS_new = MIN(PRS_old, PRS_max);
        
        if(T_gas_min >= 0) PRS_new = MAX(PRS_old, RHO_new * T_gas_min);
        if(T_gas_max >= 0) PRS_new = MIN(PRS_old, RHO_new * T_gas_max);
#endif
        
        //
        // Apply velocity restrictions:
        //
        if(VX1_min   >= 0) VX1_new = MAX(VX1_new, VX1_min);
        if(VX1_max   >= 0) VX1_new = MIN(VX1_new, VX1_max);
#if COMPONENTS > 1
        if(VX2_min   >= 0) VX2_new = MAX(VX2_new, VX2_min);
        if(VX2_max   >= 0) VX2_new = MIN(VX2_new, VX2_max);
#endif
#if COMPONENTS > 2
        if(VX3_min   >= 0) VX3_new = MAX(VX3_new, VX3_min);
        if(VX3_max   >= 0) VX3_new = MIN(VX3_new, VX3_max);
#endif
        
        //
        // Set new variables:
        //
        d->Vc[RHO][k][j][i] = RHO_new;
#if EOS != ISOTHERMAL
        d->Vc[PRS][k][j][i] = PRS_new;
#endif
        d->Vc[VX1][k][j][i] = VX1_new;
#if COMPONENTS > 1
        d->Vc[VX2][k][j][i] = VX2_new;
#endif
#if COMPONENTS > 2
        d->Vc[VX3][k][j][i] = VX3_new;
#endif
        
    } // TOT_LOOP(k,j,i)
    
}


