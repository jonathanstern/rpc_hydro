#ifndef INTERFACE_H_
#define INTERFACE_H_


int InitializeReferenceSystem(void);
void StoreHydrodynamics(Data *);


// *********
// * Setup *
// *********
//
extern FILE *LogFile;
extern double gamma_AdiabaticIndex;


// ********************
// * Reference values *
// ********************
//
extern double ReferenceLength;
extern double ReferenceArea;
extern double ReferenceVolume;
extern double ReferenceTime;
extern double ReferenceVelocity;
extern double ReferenceDensity;
extern double ReferencePressure;
extern double ReferenceTemperature;
extern double ReferenceAcceleration;
extern double ReferenceEnergyDensity;
extern double ReferenceEnergy;
extern double ReferenceFrequency;
extern double ReferenceMass;
extern double ReferenceAccretionRate;
extern double ReferencePower;
extern double ReferenceLuminosity;
extern double ReferenceOpacity;
extern double ReferenceNumberDensity;
extern double ReferenceForceDensity;
extern double ReferenceNumber;

#endif /*INTERFACE_H*/
