#include "pluto.h"
#include "MakemakeTools.h"
#include "interface.h"
#include "Radiation.h"
#include "Opacity.h"
#include "PhysicalConstantsCGS.h"


#include <petsc.h>
#include <petscvec.h>

#include <stdlib.h>  // for exit()



//int PlutoLocal2PETScGlobal(Grid *grid, int kl, int jl, int il, int *kg, int *jg, int *ig){
//
//	*kg = max(FirstLocalCenterIndexZ, StencilWidthZ) + kl-grid[KDIR].nghost;
//	*jg = max(FirstLocalCenterIndexY, StencilWidthY) + jl-grid[JDIR].nghost;
//	*ig = max(FirstLocalCenterIndexX, StencilWidthX) + il-grid[IDIR].nghost;
//
//	return 0;
//}
//
//int CommunicateHalo(DA DistributedArray, Vec InputVector){
//
//	DALocalToLocalBegin(DistributedArray, InputVector, INSERT_VALUES, InputVector);
//	DALocalToLocalEnd(  DistributedArray, InputVector, INSERT_VALUES, InputVector);
//
//	return 0;
//}



// ************
// ** hunt() **
// ************
//
// Input = (array xx, length n of array xx, sought-after value x)
//
// Output index = position of the value x
//                i.e value x is located between position jlo and jlo+1 in the given array xx,
//                beginning with position xx[0].
//                Out of bound cases yields -1 or n-1.
//
//  Adopted and slightly changed (here no global variable 'jlo' and no vector types are used)
//  from 'Numerical Recipes in C++ 2nd edition', 2002
//
int hunt(double * xx, int n, double x){
	
	int jlo = 1;
	
	int jm, jhi, inc;
	int ascnd;
	
	//int n = xx.size();
	ascnd = (xx[n-1] >= xx[0]);
	if (jlo < 0 || jlo > n-1) {
		jlo = -1;
		jhi = n;
	}
	else {
		inc = 1;
		if ((x >= xx[jlo]) == ascnd) {
			if (jlo == n-1) return jlo;
			jhi = jlo+1;
			while ((x >= xx[jhi]) == ascnd) {
				jlo = jhi;
				inc += inc;
				jhi = jlo+inc;
				if (jhi > n-1) {
					jhi = n;
					break;
				}
			}
		}
		else {
			if (jlo == 0) {
				jlo = -1;
				return jlo;
			}
			jhi = jlo--;
			while ((x < xx[jlo]) == ascnd) {
				jhi = jlo;
				inc <<= 1;
				if (inc >= jhi) {
					jlo = -1;
					break;
				}
				else
					jlo = jhi-inc;
			}
		}
	}
	while (jhi-jlo != 1) {
		jm = (jhi+jlo) >> 1;
		if ((x >= xx[jm]) == ascnd)
			jlo = jm;
		else
			jhi = jm;
	}
	if (x == xx[n-1])
		jlo = n-2;
	if (x == xx[0])
		jlo = 0;
	
	
	return jlo;
}



double LinearInterpolation1D(
							 double x0, double x1, 
							 double value_at_x0, double value_at_x1, 
							 double x
							 ){
	
	double gradient, value_at_x;
	
	//
	// Check, if x1 is greater than x0:
	//
	if(x0 >= x1){
		printf("ERROR: x0 >= x1\n");
		printf("x0 = %e\n", x0);
		printf("x1 = %e\n", x1);
		printf("x  = %e\n", x);
		exit(1);
	}
	
	//
	// Check, if x is inside the given grid range:
	//
	if(x < x0){
		printf("ERROR: x < x0\n");
		printf("x0 = %e\n", x0);
		printf("x1 = %e\n", x1);
		printf("x  = %e\n", x);
		exit(1);
	}
	if(x > x1){
		printf("ERROR: x > x1\n");
		printf("x0 = %e\n", x0);
		printf("x1 = %e\n", x1);
		printf("x  = %e\n", x);
		exit(1);
	}
	
	gradient = (value_at_x1 - value_at_x0) / (x1 - x0);
	value_at_x = gradient * (x - x0) + value_at_x0;
	
	return value_at_x;
}



double LinearInterpolation2D(
							 double x0, double x1, 
							 double y0, double y1, 
							 double value_at_x0y0, double value_at_x1y0, 
							 double value_at_x0y1, double value_at_x1y1, 
							 double x, double y
							 ){
	
	double value_at_xy;
	double value_at_xy0, value_at_xy1;
	
	//
	// 1D linear interpolation in x:
	//
	value_at_xy0 = LinearInterpolation1D(x0, x1, value_at_x0y0, value_at_x1y0, x);
	value_at_xy1 = LinearInterpolation1D(x0, x1, value_at_x0y1, value_at_x1y1, x);
	
	//
	// 1D linear interpolation in y:
	//
	value_at_xy = LinearInterpolation1D(y0, y1, value_at_xy0, value_at_xy1, y);
	
	return value_at_xy;
}



double LinearInterpolation3D(
							 double x0, double x1, 
							 double y0, double y1, 
							 double z0, double z1, 
							 double value_at_x0y0z0, double value_at_x1y0z0, 
							 double value_at_x0y1z0, double value_at_x1y1z0, 
							 double value_at_x0y0z1, double value_at_x1y0z1, 
							 double value_at_x0y1z1, double value_at_x1y1z1, 
							 double x, double y, double z
							 ){
	
	double value_at_xyz;
	double value_at_xyz0, value_at_xyz1;
	
	//
	// 2D linear interpolation in x and y:
	//
	value_at_xyz0 = LinearInterpolation2D(x0,  x1,  y0,  y1, value_at_x0y0z0, value_at_x1y0z0, value_at_x0y1z0, value_at_x1y1z0, x,  y);
	value_at_xyz1 = LinearInterpolation2D(x0,  x1,  y0,  y1, value_at_x0y0z1, value_at_x1y0z1, value_at_x0y1z1, value_at_x1y1z1, x,  y);
	
	//
	// 1D linear interpolation in z:
	//
	value_at_xyz = LinearInterpolation1D(z0, z1, value_at_xyz0, value_at_xyz1, z);
	
	return value_at_xyz;
}



// TODO
//double QuadraticInterpolation1D(double x0, double x1, double x2, double value_at_x0, double value_at_x1, double value_at_x2, double x){
//	
//	double value_at_x;
//	
//	return value_at_x;
//}
