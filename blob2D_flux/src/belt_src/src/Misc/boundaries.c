#include "pluto.h"
#include "boundaries.h"

//
// Boundary Condition Routines, called for a single cell 'k,j,i' for quantity 'q':
//
int BC_CELL_ZeroGradient(const Data *d, int side, Grid *grid, int q, int k, int j, int i){
	
	if(q == VX2 && COMPONENTS < 2) return 0;
	if(q == VX3 && COMPONENTS < 3) return 0;
#if PHYSICS == MHD || PHYSICS == RMHD
	if(q == BX2 && COMPONENTS < 2) return 0;
	if(q == BX3 && COMPONENTS < 3) return 0;
#endif
	
	
	if(side == X1_BEG){
		d->Vc[q][k][j][i] = d->Vc[q][k][j][IBEG];
	}
	else if(side == X1_END) {
		d->Vc[q][k][j][i] = d->Vc[q][k][j][IEND];
	}
	else if(side == X2_BEG){
		d->Vc[q][k][j][i] = d->Vc[q][k][JBEG][i];
	}
	else if(side == X2_END) {
		d->Vc[q][k][j][i] = d->Vc[q][k][JEND][i];
	}
	else if(side == X3_BEG){
		d->Vc[q][k][j][i] = d->Vc[q][KBEG][j][i];
	}
	else if(side == X3_END) {
		d->Vc[q][k][j][i] = d->Vc[q][KEND][j][i];
	}
	
	return 0;
}

int BC_CELL_Reflective(const Data *d, int side, Grid *grid, int q, int k, int j, int i){
	
	if(q == VX2 && COMPONENTS < 2) return 0;
	if(q == VX3 && COMPONENTS < 3) return 0;
#if PHYSICS == MHD || PHYSICS == RMHD
	if(q == BX2 && COMPONENTS < 2) return 0;
	if(q == BX3 && COMPONENTS < 3) return 0;
#endif
	
	int sign = +1;
	
	//
	// In case of vector quantities pointing in the direction of the boundary, flip their direction in the boundary cells:
	//
	if((q == VX1 && side == X1_BEG) || (q == VX1 && side == X1_END) || (q == VX2 && side == X2_BEG) || (q == VX2 && side == X2_END) || (q == VX3 && side == X3_BEG) || (q == VX3 && side == X3_END))
		sign = -1;
#if PHYSICS == MHD || PHYSICS == RMHD
	if((q == BX1 && side == X1_BEG) || (q == BX1 && side == X1_END) || (q == BX2 && side == X2_BEG) || (q == BX2 && side == X2_END) || (q == BX3 && side == X3_BEG) || (q == BX3 && side == X3_END))
		sign = -1;
#endif
	
	if(side == X1_BEG){
		d->Vc[q][k][j][i] = sign * d->Vc[q][k][j][2*IBEG-i-1];
	}
	else if(side == X1_END) {
		d->Vc[q][k][j][i] = sign * d->Vc[q][k][j][2*IEND-i+1];
	}
	else if(side == X2_BEG){
		d->Vc[q][k][j][i] = sign * d->Vc[q][k][2*JBEG-j-1][i];
	}
	else if(side == X2_END) {
		d->Vc[q][k][j][i] = sign * d->Vc[q][k][2*JEND-j+1][i];
	}
	else if(side == X3_BEG){
		d->Vc[q][k][j][i] = sign * d->Vc[q][2*KBEG-k-1][j][i];
	}
	else if(side == X3_END) {
		d->Vc[q][k][j][i] = sign * d->Vc[q][2*KEND-k+1][j][i];
	}
	
	return 0;
}

//
// Boundary Condition Routines, called for all cells on the specified side 'side' for quantity 'q':
//
int BC_ZeroGradient(const Data *d, int side, Grid *grid, int q){
	
	int k,j,i;
	
	if(side == X1_BEG){
		KTOT_LOOP(k) {
			JTOT_LOOP(j) {
				IBEG_LOOP(i) {
					BC_CELL_ZeroGradient(d, side, grid, q, k, j, i);
				}}}
	}
	else if(side == X1_END) {
		KTOT_LOOP(k) {
			JTOT_LOOP(j) {
				IEND_LOOP(i) {
					BC_CELL_ZeroGradient(d, side, grid, q, k, j, i);
				}}}
	}
	else if(side == X2_BEG){
		KTOT_LOOP(k) {
			JBEG_LOOP(j) {
				ITOT_LOOP(i) {
					BC_CELL_ZeroGradient(d, side, grid, q, k, j, i);
				}}}
	}
	else if(side == X2_END) {
		KTOT_LOOP(k) {
			JEND_LOOP(j) {
				ITOT_LOOP(i) {
					BC_CELL_ZeroGradient(d, side, grid, q, k, j, i);
				}}}
	}
	else if(side == X3_BEG){
		KBEG_LOOP(k) {
			JTOT_LOOP(j) {
				ITOT_LOOP(i) {
					BC_CELL_ZeroGradient(d, side, grid, q, k, j, i);
				}}}
	}
	else if(side == X3_END) {
		KEND_LOOP(k) {
			JTOT_LOOP(j) {
				ITOT_LOOP(i) {
					BC_CELL_ZeroGradient(d, side, grid, q, k, j, i);
				}}}
	}
	
	return 0;
}

int BC_Reflective(const Data *d, int side, Grid *grid, int q){
	
	int k,j,i;
	
	if(side == X1_BEG){
		KTOT_LOOP(k) {
			JTOT_LOOP(j) {
				IBEG_LOOP(i) {
					BC_CELL_Reflective(d, side, grid, q, k, j, i);
				}}}
	}
	else if(side == X1_END) {
		KTOT_LOOP(k) {
			JTOT_LOOP(j) {
				IEND_LOOP(i) {
					BC_CELL_Reflective(d, side, grid, q, k, j, i);
				}}}
	}
	else if(side == X2_BEG){
		KTOT_LOOP(k) {
			JBEG_LOOP(j) {
				ITOT_LOOP(i) {
					BC_CELL_Reflective(d, side, grid, q, k, j, i);
				}}}
	}
	else if(side == X2_END) {
		KTOT_LOOP(k) {
			JEND_LOOP(j) {
				ITOT_LOOP(i) {
					BC_CELL_Reflective(d, side, grid, q, k, j, i);
				}}}
	}
	else if(side == X3_BEG){
		KBEG_LOOP(k) {
			JTOT_LOOP(j) {
				ITOT_LOOP(i) {
					BC_CELL_Reflective(d, side, grid, q, k, j, i);
				}}}
	}
	else if(side == X3_END) {
		KEND_LOOP(k) {
			JTOT_LOOP(j) {
				ITOT_LOOP(i) {
					BC_CELL_Reflective(d, side, grid, q, k, j, i);
				}}}
	}
	
	return 0;
}

//
// Boundary Condition Routines, called for all cells on the specified side 'side' for all flow relevant quantities: PRS, DN, V[side]:
//
int BC_NoOutflow_NoInflow(const Data *d, int side, Grid *grid){
	
	int k,j,i;
	
	//
	// Set pressure gradient to zero => No force over the boundary:
	//
#if EOS != ISOTHERMAL
	BC_ZeroGradient(d, side, grid, PRS);
#endif
    
	//
	// Prevent inflow & outflow:
	//
	if(side == X1_BEG){
		KTOT_LOOP(k) {
			JTOT_LOOP(j) {
				IBEG_LOOP(i) {
					BC_CELL_Reflective(d, side, grid, RHO, k, j, i);
					BC_CELL_Reflective(d, side, grid, VX1, k, j, i);
				}}}
	}
	else if(side == X1_END) {
		KTOT_LOOP(k) {
			JTOT_LOOP(j) {
				IEND_LOOP(i) {
					BC_CELL_Reflective(d, side, grid, RHO, k, j, i);
					BC_CELL_Reflective(d, side, grid, VX1, k, j, i);
				}}}
	}
	else if(side == X2_BEG){
		KTOT_LOOP(k) {
			JBEG_LOOP(j) {
				ITOT_LOOP(i) {
					BC_CELL_Reflective(d, side, grid, RHO, k, j, i);
					BC_CELL_Reflective(d, side, grid, VX2, k, j, i);
				}}}
	}
	else if(side == X2_END) {
		KTOT_LOOP(k) {
			JEND_LOOP(j) {
				ITOT_LOOP(i) {
					BC_CELL_Reflective(d, side, grid, RHO, k, j, i);
					BC_CELL_Reflective(d, side, grid, VX2, k, j, i);
				}}}
	}
	else if(side == X3_BEG){
		KBEG_LOOP(k) {
			JTOT_LOOP(j) {
				ITOT_LOOP(i) {
					BC_CELL_Reflective(d, side, grid, RHO, k, j, i);
					BC_CELL_Reflective(d, side, grid, VX3, k, j, i);
				}}}
	}
	else if(side == X3_END) {
		KEND_LOOP(k) {
			JTOT_LOOP(j) {
				ITOT_LOOP(i) {
					BC_CELL_Reflective(d, side, grid, RHO, k, j, i);
					BC_CELL_Reflective(d, side, grid, VX3, k, j, i);
				}}}
	}
	
	return 0;
}

int BC_Outflow_NoInflow(const Data *d, int side, Grid *grid){
	
	int k,j,i;
	
	//
	// Set pressure gradient to zero => No force over the boundary:
	//
#if EOS != ISOTHERMAL
	BC_ZeroGradient(d, side, grid, PRS);
#endif
    
	//
	// Prevent inflow:
	//
	if(side == X1_BEG){
		KTOT_LOOP(k) {
			JTOT_LOOP(j) {
				IBEG_LOOP(i) {
					if (d->Vc[VX1][k][j][IBEG] >= 0){
						BC_CELL_Reflective(d, side, grid, RHO, k, j, i);
						BC_CELL_Reflective(d, side, grid, VX1, k, j, i);
					}
					else{
						//BC_CELL_ZeroGradient(d, side, grid, RHO, k, j, i);
						d->Vc[RHO][k][j][i] = d->Vc[RHO][k][j][IBEG] * pow(grid[IDIR].x[IBEG] / grid[IDIR].x[i], 2);
						BC_CELL_ZeroGradient(d, side, grid, VX1, k, j, i);
					}
				}}}
	}
	else if(side == X1_END) {
		KTOT_LOOP(k) {
			JTOT_LOOP(j) {
				IEND_LOOP(i) {
					if (d->Vc[VX1][k][j][IEND] <= 0){
						BC_CELL_Reflective(d, side, grid, RHO, k, j, i);
						BC_CELL_Reflective(d, side, grid, VX1, k, j, i);
					}
					else{
						BC_CELL_ZeroGradient(d, side, grid, RHO, k, j, i);
						BC_CELL_ZeroGradient(d, side, grid, VX1, k, j, i);
					}
				}}}
	}
	else if(side == X2_BEG){
		KTOT_LOOP(k) {
			JBEG_LOOP(j) {
				ITOT_LOOP(i) {
					if (d->Vc[VX2][k][JBEG][i] >= 0){
						BC_CELL_Reflective(d, side, grid, RHO, k, j, i);
						BC_CELL_Reflective(d, side, grid, VX2, k, j, i);
					}
					else{
						BC_CELL_ZeroGradient(d, side, grid, RHO, k, j, i);
						BC_CELL_ZeroGradient(d, side, grid, VX2, k, j, i);
					}
				}}}
	}
	else if(side == X2_END) {
		KTOT_LOOP(k) {
			JEND_LOOP(j) {
				ITOT_LOOP(i) {
					if (d->Vc[VX2][k][JEND][i] <= 0){
						BC_CELL_Reflective(d, side, grid, RHO, k, j, i);
						BC_CELL_Reflective(d, side, grid, VX2, k, j, i);
					}
					else{
						BC_CELL_ZeroGradient(d, side, grid, RHO, k, j, i);
						BC_CELL_ZeroGradient(d, side, grid, VX2, k, j, i);
					}
				}}}
	}
	else if(side == X3_BEG){
		KBEG_LOOP(k) {
			JTOT_LOOP(j) {
				ITOT_LOOP(i) {
					if (d->Vc[VX3][KBEG][j][i] >= 0){
						BC_CELL_Reflective(d, side, grid, RHO, k, j, i);
						BC_CELL_Reflective(d, side, grid, VX3, k, j, i);
					}
					else{
						BC_CELL_ZeroGradient(d, side, grid, RHO, k, j, i);
						BC_CELL_ZeroGradient(d, side, grid, VX3, k, j, i);
					}
				}}}
	}
	else if(side == X3_END) {
		KEND_LOOP(k) {
			JTOT_LOOP(j) {
				ITOT_LOOP(i) {
					if (d->Vc[VX3][KEND][j][i] <= 0){
						BC_CELL_Reflective(d, side, grid, RHO, k, j, i);
						BC_CELL_Reflective(d, side, grid, VX3, k, j, i);
					}
					else{
						BC_CELL_ZeroGradient(d, side, grid, RHO, k, j, i);
						BC_CELL_ZeroGradient(d, side, grid, VX3, k, j, i);
					}
				}}}
	}
	
	return 0;
}
