#include "pluto.h"
#include "boundary_fluxes.h"
#include "PhysicalConstantsCGS.h"

#if SELFGRAVITY == YES
#include "SelfGravity.h"
#endif

#if RADIATION == YES
#include "Radiation.h"
#endif



#if (BODY_FORCE & VECTOR)
/* ************************************************************************ */
void BodyForceVector(double *v, double *g, double x1, double x2, double x3, int il, int jl, int kl, const Data *data, Grid *grid, Time_Step *Dts)
/*
 *
 *
 *
 *************************************************************************** */
{
    
    double agrav;
    
	g[IDIR] = 0.0;
	g[JDIR] = 0.0;
	g[KDIR] = 0.0;
    
    
    //
    // Central point-mass Gravity:
    //
    agrav    = - G_GravityConstant * M_X1_BEG / (x1*x1);
    g[IDIR] += agrav;
    Dts->dt_grav = MIN(Dts->dt_grav, sqrt(- 2.0 * grid[IDIR].dx[il] / agrav));
    
    
#if IRRADIATION == YES
    double aradstar;
    if(IrradiationFlag != 0){
        if(StellarRadiativeForceFlag == 0){
            // No Stellar Radiative Force
            aradstar = 0.0;
        }
        else if(StellarRadiativeForceFlag == 1){
            // arad = - div F_stellar / c / rho
            aradstar = data->IrradiationPowerDensity[kl][jl][il] / c_SpeedOfLight / data->Vc[RHO][kl][jl][il];
        }
        else{
            printf("ERROR: StellarRadiativeForceFlag = %d not available.\n", StellarRadiativeForceFlag);
            exit(1);
        }
        g[IDIR] += aradstar;
    }
#endif
    
#if FLUXLIMITEDDIFFUSION == YES
    double aradtherm;
    double rhogas, lambda, dx, dErad, Erad, Eradr, Eradl;

    if(RadiationFlag != 0){
        if(ThermalRadiativeForceFlag == 0){
            // No Thermal Radiative Force
            aradtherm = 0.0;
        }
        else if(ThermalRadiativeForceFlag == 1 || ThermalRadiativeForceFlag == 3){
            // thermal radiative force is handled as potential, see below.
            aradtherm = 0.0;
        }
        else if(ThermalRadiativeForceFlag == 2){
            // arad = Kappa_Rosseland * F_thermal / c  with  F_thermal = - D grad E_R
            rhogas = data->Vc[RHO][kl][jl][il];
            // TODO: use lambda from interface according to upwind scheme below!?
            lambda = data->RadiationFLuxLimiter[kl][jl][il];
            Erad   = data->RadiationEnergyDensity[kl][jl][il  ];
            Eradl  = data->RadiationEnergyDensity[kl][jl][il-1];
            Eradr  = data->RadiationEnergyDensity[kl][jl][il+1];
            dx     = GridCellLength(grid, IDIR, kl, jl, il);
            // upwind scheme:
            if(Eradr > Eradl) dErad = Erad  - Eradl;
            else              dErad = Eradr - Erad;
            aradtherm = - lambda * dErad / dx / rhogas;
            g[IDIR]  += aradtherm;
#if DIMENSIONS > 1
            Eradl  = data->RadiationEnergyDensity[kl][jl-1][il];
            Eradr  = data->RadiationEnergyDensity[kl][jl+1][il];
            dx     = GridCellLength(grid, JDIR, kl, jl, il);
            if(Eradr > Eradl) dErad = Erad  - Eradl;
            else              dErad = Eradr - Erad;
            aradtherm = - lambda * dErad / dx / rhogas;
            g[JDIR]  += aradtherm;
#endif
#if DIMENSIONS > 2
            Eradl  = data->RadiationEnergyDensity[kl-1][jl][il];
            Eradr  = data->RadiationEnergyDensity[kl+1][jl][il];
            dx     = GridCellLength(grid, KDIR, kl, jl, il);
            if(Eradr > Eradl) dErad = Erad  - Eradl;
            else              dErad = Eradr - Erad;
            aradtherm = - lambda * dErad / dx / rhogas;
            g[KDIR]  += aradtherm;
#endif
        }
        else{
            printf("ERROR: ThermalRadiativeForceFlag = %d not available.\n", ThermalRadiativeForceFlag);
            exit(1);
        }
    }
#endif
}
#endif




#if (BODY_FORCE & POTENTIAL)
/* ************************************************************************ */
double BodyForcePotential(double x1, double x2, double x3, int il, int jl, int kl, const Data *data, Grid *grid, Time_Step *Dts)
/*
 *
 *
 *
 *************************************************************************** */
{
	double Potential;
    
    Potential = 0.0;
	
    //
	// Self-Gravity of gas in the computational domain:
    //
#if SELFGRAVITY == YES
	if(SelfGravityFlag != 0) Potential += data->phi[kl][jl][il];
#endif
    
    //
    // Thermal Radiative Force in Eddington approximation:
    //
#if FLUXLIMITEDDIFFUSION == YES
    if(ThermalRadiativeForceFlag == 1) Potential += data->RadiationEnergyDensity[kl][jl][il] / 3.0 / data->Vc[RHO][kl][jl][il];
    else if(ThermalRadiativeForceFlag == 3) Potential += data->RadiationFLuxLimiter[kl][jl][il] * data->RadiationEnergyDensity[kl][jl][il] / data->Vc[RHO][kl][jl][il];
#endif
	
	return Potential;
}
#endif


