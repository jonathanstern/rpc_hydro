#ifndef OPACITY_TABLES_H_
#define OPACITY_TABLES_H_


// ***********************
// ** External Routines **
// ***********************
//

void Make_GasOpacityIonTable(); //ONORBE
double Get_GasOpacityIon(double, double, double, double); //ONORBE

void Make_DustOpacityOptTable(); //ONORBE
double Get_DustOpacityOpt(double, double, double, double); //ONORBE

void Make_DustOpacityIonTable(); //ONORBE
double Get_DustOpacityIon(double, double, double, double); //ONORBE

#endif /*OPACITY_TABLES_H_*/
