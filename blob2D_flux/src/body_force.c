#include "pluto.h"
#include "boundary_fluxes.h"
#include "PhysicalConstantsCGS.h"
#include "ReadRestrictionsConfiguration.h"
#include "interface.h"

#if SELFGRAVITY == YES
#include "SelfGravity.h"
#endif

#if RADIATION == YES
#include "Radiation.h"
#include "FLD.h"
#include "Opacity.h"
#endif




#if (BODY_FORCE & VECTOR)
/* ************************************************************************ */
void BodyForceVector(double *v, double *g, double x1, double x2, double x3, int il, int jl, int kl, const Data *data, Grid *grid, Time_Step *Dts)
/*
 *
 *
 *
 *************************************************************************** */
{
    
    double agrav;
    
	g[IDIR] = 0.0;
	g[JDIR] = 0.0;
	g[KDIR] = 0.0;
    
    //
    // Central point-mass Gravity:
    //
//    agrav    = - G_GravityConstant * M_X1_BEG / (x1*x1);
//    g[IDIR] += agrav;
//    Dts->dt_grav = MIN(Dts->dt_grav, sqrt(- 2.0 * grid[IDIR].dx[il] / agrav));
    
    
#if IRRADIATION == YES
    double aradstar;
    if(IrradiationFlag != 0){
        if(StellarRadiativeForceFlag == 0){
            // No Stellar Radiative Force
            aradstar = 0.0;
        }
        else if(StellarRadiativeForceFlag == 1){
            // arad = - div F_stellar / c / rho
            // TODO: Jonathan: check here for correct treatment!!
            if(data->Vc[RHO][kl][jl][il] > 1e-28 / ReferenceDensity)
                aradstar = data->IrradiationPowerDensity[kl][jl][il] / c_SpeedOfLight / data->Vc[RHO][kl][jl][il];
            else
                aradstar = 0;
        }
        else{
            printf("ERROR: StellarRadiativeForceFlag = %d not available.\n", StellarRadiativeForceFlag);
            exit(1);
        }
        g[IDIR] += aradstar;
    }
#endif
    
#if FLUXLIMITEDDIFFUSION == YES
    double dxl,  dxr;
    double fxml, fxmr;
    double fxll, fxrr;
    double NH,NHI;
    double rhodust, rhodustxl, rhodustxr;
    double rhogas,  rhogasxl,  rhogasxr;
    double Tdust, Tdustxl, Tdustxr;
    double Tgas,  Tgasxl,  Tgasxr;
    double kappadust, kappadustxl, kappadustxr;
    double kappagas,  kappagasxl,  kappagasxr;
    double kapparho,  kapparhoxl,  kapparhoxr;
    double ER,  ERxl,   ERxr;
    double Rxl,  Rxr;
    double Lxl,  Lxr;
#if DIMENSIONS > 1
    double dyl,  dyr;
    double fyml, fymr;
    double fyll, fyrr;
    double rhodustyl, rhodustyr;
    double rhogasyl,  rhogasyr;
    double Tdustyl, Tdustyr;
    double Tgasyl,  Tgasyr;
    double kappadustyl, kappadustyr;
    double kappagasyl,  kappagasyr;
    double kapparhoyl,  kapparhoyr;
    double ERyl,   ERyr;
    double Ryl,  Ryr;
    double Lyl,  Lyr;
#endif
#if DIMENSIONS > 2
    double dzl,  dzr;
    double fzml, fzmr;
    double fzll, fzrr;
    double rhodustzl, rhodustzr;
    double rhogaszl,  rhogaszr;
    double Tdustzl, Tdustzr;
    double Tgaszl,  Tgaszr;
    double kappadustzl, kappadustzr;
    double kappagaszl,  kappagaszr;
    double kapparhozl,  kapparhozr;
    double ERzl,   ERzr;
    double Rzl,  Rzr;
    double Lzl,  Lzr;
#endif
    
    
    if(RadiationFlag != 0){
        if(ThermalRadiativeForceFlag == 0){
            // No Thermal Radiative Force
//            aradtherm = 0.0;
        }
        else if(ThermalRadiativeForceFlag == 1 || ThermalRadiativeForceFlag == 3){
            // thermal radiative force is handled as potential, see below.
//            aradtherm = 0.0;
        }
        else if(ThermalRadiativeForceFlag == 2){
            // arad = Kappa_Rosseland * F_thermal / c  with  F_thermal = - D grad E_R
            dxl         = 0.5 * (GridCellLength(grid, IDIR, kl,   jl,   il-1) + GridCellLength(grid, IDIR, kl,   jl,   il  ));
            dxr         = 0.5 * (GridCellLength(grid, IDIR, kl,   jl,   il  ) + GridCellLength(grid, IDIR, kl,   jl,   il+1));
            fxml        = 1.0 - 0.5 * GridCellLength(grid, IDIR, kl,   jl,   il  ) / dxl;
            fxmr        = 1.0 - 0.5 * GridCellLength(grid, IDIR, kl,   jl,   il  ) / dxr;
            fxll        = 1.0 - 0.5 * GridCellLength(grid, IDIR, kl,   jl,   il-1) / dxl;
            fxrr        = 1.0 - 0.5 * GridCellLength(grid, IDIR, kl,   jl,   il+1) / dxr;

            // ONORBE. Will need to change opacities here at some point
            // But also will need to take into account two fluxes!!
			NH = data->NH[kl][jl][il];
			NHI =data->NHI[kl][jl][il];
            
            rhogas      = data->Vc[RHO][kl  ][jl  ][il  ];
            rhogasxl    = data->Vc[RHO][kl  ][jl  ][il-1];
            rhogasxr    = data->Vc[RHO][kl  ][jl  ][il+1];
            rhodust     = data->DustDensity[kl  ][jl  ][il  ] * rhogas;
            rhodustxl   = data->DustDensity[kl  ][jl  ][il-1] * rhogasxl;
            rhodustxr   = data->DustDensity[kl  ][jl  ][il+1] * rhogasxr;
            Tdust       = data->DustTemperature[kl  ][jl  ][il  ];
            Tdustxl     = data->DustTemperature[kl  ][jl  ][il-1];
            Tdustxr     = data->DustTemperature[kl  ][jl  ][il+1];
            Tgas        = data->GasTemperature[kl  ][jl  ][il  ];
            Tgasxl      = data->GasTemperature[kl  ][jl  ][il-1];
            Tgasxr      = data->GasTemperature[kl  ][jl  ][il+1];
            kappadust   = RosselandMeanDustOpacity(Tdust  , rhodust ,0.0 );
            kappadustxl = RosselandMeanDustOpacity(Tdustxl, rhodustxl,0.0);
            kappadustxr = RosselandMeanDustOpacity(Tdustxr, rhodustxr,0.0);
            kappagas    = RosselandMeanGasOpacity(Tgas    , rhogas   ,0.0);
            kappagasxl  = RosselandMeanGasOpacity(Tgasxl  , rhogasxl ,0.0);
            kappagasxr  = RosselandMeanGasOpacity(Tgasxr  , rhogasxr ,0.0);
            kapparho    = kappadust   * rhodust   + kappagas   * rhogas  ;
            kapparhoxl  = kappadustxl * rhodustxl + kappagasxl * rhogasxl;
            kapparhoxr  = kappadustxr * rhodustxr + kappagasxr * rhogasxr;
            ER          = data->RadiationEnergyDensity[kl  ][jl  ][il  ];
            ERxl        = data->RadiationEnergyDensity[kl  ][jl  ][il-1];
            ERxr        = data->RadiationEnergyDensity[kl  ][jl  ][il+1];
            
            Rxl         = fabs(ER   - ERxl) / dxl;
            Rxr         = fabs(ERxr - ER  ) / dxr;
            Rxl        /= fxll * kapparhoxl * ERxl + fxml * kapparho * ER;
            Rxr        /= fxrr * kapparhoxr * ERxr + fxmr * kapparho * ER;
            
            Lxl         = FluxLimiter(Rxl);
            Lxr         = FluxLimiter(Rxr);
            
            g[IDIR]    += 0.5 * (- Lxl * (ER   - ERxl) / dxl - Lxr * (ERxr - ER  ) / dxr) / rhogas;
            
#if DIMENSIONS > 1
            dyl         = 0.5 * (GridCellLength(grid, JDIR, kl,   jl-1, il  ) + GridCellLength(grid, JDIR, kl,   jl,   il  ));
            dyr         = 0.5 * (GridCellLength(grid, JDIR, kl,   jl,   il  ) + GridCellLength(grid, JDIR, kl,   jl+1, il  ));
            fyml        = 1.0 - 0.5 * GridCellLength(grid, JDIR, kl,   jl  , il  )	/ dyl;
            fymr        = 1.0 - 0.5 * GridCellLength(grid, JDIR, kl,   jl  , il  )	/ dyr;
            fyll        = 1.0 - 0.5 * GridCellLength(grid, JDIR, kl,   jl-1, il  )	/ dyl;
            fyrr        = 1.0 - 0.5 * GridCellLength(grid, JDIR, kl,   jl+1, il  )	/ dyr;
            
            rhogasyl    = data->Vc[RHO][kl  ][jl-1][il  ];
            rhogasyr    = data->Vc[RHO][kl  ][jl+1][il  ];
            rhodustyl   = data->DustDensity[kl  ][jl-1][il  ] * rhogasyl;
            rhodustyr   = data->DustDensity[kl  ][jl+1][il  ] * rhogasyr;
            Tdustyl     = data->DustTemperature[kl  ][jl-1][il  ];
            Tdustyr     = data->DustTemperature[kl  ][jl+1][il  ];
            Tgasyl      = data->GasTemperature[kl  ][jl-1][il  ];
            Tgasyr      = data->GasTemperature[kl  ][jl+1][il  ];
            kappadustyl = RosselandMeanDustOpacity(Tdustyl, rhodustyl,0.0);
            kappadustyr = RosselandMeanDustOpacity(Tdustyr, rhodustyr,0.0);
            kappagasyl  = RosselandMeanGasOpacity(Tgasyl  , rhogasyl ,0.0);
            kappagasyr  = RosselandMeanGasOpacity(Tgasyr  , rhogasyr ,0.0);
            kapparhoyl  = kappadustyl * rhodustyl + kappagasyl * rhogasyl;
            kapparhoyr  = kappadustyr * rhodustyr + kappagasyr * rhogasyr;
            ERyl        = data->RadiationEnergyDensity[kl  ][jl-1][il  ];
            ERyr        = data->RadiationEnergyDensity[kl  ][jl+1][il  ];
            
            Ryl         = fabs(ER   - ERyl) / dyl;
            Ryr         = fabs(ERyr - ER  ) / dyr;
            Ryl        /= fyll * kapparhoyl * ERyl + fyml * kapparho * ER;
            Ryr        /= fyrr * kapparhoyr * ERyr + fymr * kapparho * ER;
            Lyl         = FluxLimiter(Ryl);
            Lyr         = FluxLimiter(Ryr);
            
            g[JDIR]    += 0.5 * (- Lyl * (ER   - ERyl) / dyl - Lyr * (ERyr - ER  ) / dyr) / rhogas;
#endif
#if DIMENSIONS > 2
            dzl         = 0.5 * (GridCellLength(grid, KDIR, kl-1, jl,   il  ) + GridCellLength(grid, KDIR, kl,   jl,   il  ));
            dzr         = 0.5 * (GridCellLength(grid, KDIR, kl,   jl,   il  ) + GridCellLength(grid, KDIR, kl+1, jl,   il  ));
            fzml        = 1.0 - 0.5 * GridCellLength(grid, KDIR, kl,   jl,   il  )	/ dzl;
            fzmr        = 1.0 - 0.5 * GridCellLength(grid, KDIR, kl,   jl,   il  )	/ dzr;
            fzll        = 1.0 - 0.5 * GridCellLength(grid, KDIR, kl-1, jl,   il  )	/ dzl;
            fzrr        = 1.0 - 0.5 * GridCellLength(grid, KDIR, kl+1, jl,   il  ) / dzr;
            
            rhogaszl    = data->Vc[RHO][kl-1][jl  ][il  ];
            rhogaszr    = data->Vc[RHO][kl+1][jl  ][il  ];
            rhodustzl   = data->DustDensity[kl-1][jl  ][il  ] * rhogaszl;
            rhodustzr   = data->DustDensity[kl+1][jl  ][il  ] * rhogaszr;
            Tdustzl     = data->DustTemperature[kl-1][jl  ][il  ];
            Tdustzr     = data->DustTemperature[kl+1][jl  ][il  ];
            Tgaszl      = data->GasTemperature[kl-1][jl  ][il  ];
            Tgaszr      = data->GasTemperature[kl+1][jl  ][il  ];
            kappadustzl = RosselandMeanDustOpacity(Tdustzl, rhodustzl, 0.0);
            kappadustzr = RosselandMeanDustOpacity(Tdustzr, rhodustzr, 0.0);
            kappagaszl  = RosselandMeanGasOpacity(Tgaszl  , rhogaszl , 0.0);
            kappagaszr  = RosselandMeanGasOpacity(Tgaszr  , rhogaszr , 0.0);
            kapparhozl  = kappadustzl * rhodustzl + kappagaszl * rhogaszl;
            kapparhozr  = kappadustzr * rhodustzr + kappagaszr * rhogaszr;
            ERzl        = data->RadiationEnergyDensity[kl-1][jl  ][il  ];
            ERzr        = data->RadiationEnergyDensity[kl+1][jl  ][il  ];
            
            Rzl         = fabs(ER   - ERzl) / dzl;
            Rzr         = fabs(ERzr - ER  ) / dzr;
            Rzl        /= fzll * kapparhozl * ERzl + fzml * kapparho * ER;
            Rzr        /= fzrr * kapparhozr * ERzr + fzmr * kapparho * ER;
            Lzl         = FluxLimiter(Rzl);
            Lzr         = FluxLimiter(Rzr);
            
            g[KDIR]    += 0.5 * (- Lzl * (ER   - ERzl) / dzl - Lzr * (ERzr - ER  ) / dzr) / rhogas;
#endif
        }
        else{
            printf("ERROR: ThermalRadiativeForceFlag = %d not available.\n", ThermalRadiativeForceFlag);
            exit(1);
        }
    }
#endif
}
#endif




#if (BODY_FORCE & POTENTIAL)
/* ************************************************************************ */
double BodyForcePotential(double x1, double x2, double x3, int il, int jl, int kl, const Data *data, Grid *grid, Time_Step *Dts)
/*
 *
 *
 *
 *************************************************************************** */
{
	double Potential;
    
    Potential = 0.0;
	
	return Potential;
}
#endif


