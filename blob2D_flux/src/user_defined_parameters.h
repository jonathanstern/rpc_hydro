//
// Specify basic code units:
//
#define UNIT_DENSITY                (1e-20)
#define UNIT_LENGTH                 (CONST_pc)
#define UNIT_VELOCITY               (CONST_pc * CONST_c / CONST_ly)
// => unit of time = 1 yr



//
// Specify names of user defined parameters:
//
#define r_cloud_pc                  0
#define rho_in_cgs                  1
#define rho_ex_cgs                  2
#define T_in_cgs                    3

//mean ionizing photon energy in eV
#define mipe_in_eV                  74.66

//
// Specify number of user defined parameters:
//
#define USER_DEF_PARAMETERS         4


// Run-time values of these parameters are specified in the file 'pluto.ini'.
