#ifndef MOLECULES_H_
#define MOLECULES_H_

extern double mtot;
extern double Ntot;
extern double NHatoms;
extern double mMH_AbsoluteMolecularMassOfHydrogen;


double MolarMass(double);
double AbsoluteMolecularMass(double);

#endif /*MOLECULES_H_*/
