#include "pluto.h"
#include "Molecules.h"
#include "PhysicalConstantsCGS.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


double mtot;
double Ntot;
double NHatoms;
double mMH_AbsoluteMolecularMassOfHydrogen;


//double RelativeMolecularMass(double RelativeMolecularMassNeutral, double RelativeMolecularMassHydrogen, double IonizationFraction){
//    return 1.0 / (1.0/RelativeMolecularMassNeutral + IonizationFraction/RelativeMolecularMassHydrogen);
//}

double MolarMass(double IonizationFraction){

    double X,Y,Z,AH,AHe,AZ;
    double nH,nHe,nZ,ntot,meanm,ne;
    double MolarMass, RelativeMolecularMass;
    
#if IONIZATION == YES
    double RelativeMolecularMassNeutral, RelativeMolecularMassHydrogen;
    RelativeMolecularMassNeutral  = mtot / Ntot;
    RelativeMolecularMassHydrogen = mtot / NHatoms;
    
    RelativeMolecularMass         = 1.0 / (1.0/RelativeMolecularMassNeutral + IonizationFraction/RelativeMolecularMassHydrogen);
#else
    // Assuming solar composition
    X=0.7;Y=0.28;Z=0.02;
    //Number of protons and neutrons in each specie
    //Assuming average for metals of 15.5
    AH=1.0;AHe=4.0;AZ=15.5;
    //
    nH=X/AH;
    nHe=Y/AHe;
    nZ=Z/AZ;
    ntot=nH+nHe+nZ;
    meanm=nH*AH+nHe*AHe+nZ*AZ;
    // JONORBE. Warning assuming HII fraction is some type of *global* ionization fraction
    //ne=IonizationFraction*(nH*AH+nHe*AHe/2.+nZ*AZ/2.);
    ne=IonizationFraction*(X+Y/2.+Z/2.);
    RelativeMolecularMass = meanm/(ntot+ne);
#endif
    
    MolarMass                     = RelativeMolecularMass * NA_AvogadroConstant * mC_CarbonMass / 12.0;

    //fprintf(stdout,"mtot=%e,Ntot=%e,RelativeMolecularMass=%e,Molar=%e\n",mtot,Ntot,RelativeMolecularMass,MolarMass);
    //fflush(stdout);

    return MolarMass;
}

double AbsoluteMolecularMass(double RelativeMolecularMass){
    return RelativeMolecularMass * mC_CarbonMass / 12.0; // = MolarMass / NA_AvogadroConstant
}
