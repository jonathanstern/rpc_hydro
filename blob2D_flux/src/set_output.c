/* ///////////////////////////////////////////////////////////////////// */
/*!
 \file
 \brief Set/retrieve output data attributes.
 
 The function SetOutput() sets, for each output data type (DBL, FLT,
 VTK etc..) the default attributes of the corresponding ::Output structures.
 These include the variable name, a pointer to the actual
 3D array, the centering of the variable (center/staggered), a
 conditional inclusion flag (telling if the corresponding variable has
 to be written in the specified format), and so on.
 
 The function SetDumpVar() can be used to include or exclude a given
 variable to be written using a particular output format.
 
 The function GetUserVar() returns the memory address to a
 user-defined 3D array.
 
 \note Starting with PLUTO 4.1 velocity and magnetic field components
 will be saved as scalars when writing VTK output.
 If this is not what you want and prefer to save them as vector
 fields (VTK VECTOR attribute), set VTK_VECTOR_DUMP to YES
 in your definitions.h.
 
 \authors A. Mignone (mignone@ph.unito.it)
 \date    March 26, 2014
 */
/* ///////////////////////////////////////////////////////////////////// */
#include "pluto.h"

#ifndef VTK_VECTOR_DUMP
#define VTK_VECTOR_DUMP NO
#endif

static Output *all_outputs;
/* ********************************************************************* */
void SetOutput (Data *d, Input *input)
/*!
 *  Set default attributes (variable names, pointers to data structures,
 *  filename extensions, etc...) of the output structures.
 *
 * \param [in] d      pointer to Data structure
 * \param [in] input  pointer to input structure
 *
 *********************************************************************** */
{
    int nv, i, k;
    Output *output;
    
    if (input->user_var > 0)
        d->Vuser = ARRAY_4D(input->user_var, NX3_TOT, NX2_TOT, NX1_TOT, double);
    else
        d->Vuser = NULL;
    
    all_outputs = input->output;
    
    /* ---------------------------------------------
     Loop on output types
     --------------------------------------------- */
    
    for (k = 0; k < MAX_OUTPUT_TYPES; k++){
        output = input->output + k;
        output->var_name = ARRAY_2D(64,128,char);
        output->stag_var = ARRAY_1D(64, int);
        output->dump_var = ARRAY_1D(64, int);
        strcpy(output->dir, input->output_dir); /* output directory is the same     */
        /* for all outputs (easy to change) */
        output->nfile    = -1;
        
        /* -- set variables names -- */
        
        SetDefaultVarNames(output);
        
        /* -- Set array pointers -- */
        
        for (nv = 0; nv < NVAR; nv++){
            output->V[nv]        = d->Vc[nv];
            output->stag_var[nv] = -1; /* -- means cell centered -- */
        }
        nv = NVAR;
#ifdef STAGGERED_MHD
        D_EXPAND(
                 output->var_name[nv]   = "bx1s";
                 output->V[nv]          = d->Vs[BX1s];
                 output->stag_var[nv++] = 0;           ,
                 
                 output->var_name[nv]   = "bx2s";
                 output->V[nv]          = d->Vs[BX2s];
                 output->stag_var[nv++] = 1;           ,
                 
                 output->var_name[nv]   = "bx3s";
                 output->V[nv]          = d->Vs[BX3s];
                 output->stag_var[nv++] = 2;
                 )
#endif
        
#if UPDATE_VECTOR_POTENTIAL == YES
#if DIMENSIONS == 3
        output->var_name[nv]   = "Ax1";
        output->V[nv]          = d->Ax1;
        output->stag_var[nv++] = -1;  /* -- vector potential is
                                       computed at cell center -- */
        
        output->var_name[nv]   = "Ax2";
        output->V[nv]          = d->Ax2;
        output->stag_var[nv++] = -1;
#endif
        output->var_name[nv]   = "Ax3";
        output->V[nv]          = d->Ax3;
        output->stag_var[nv++] = -1;
#endif
        //
        // Adding output of belt variables:
        //
        output->var_name[nv]   = (char *) "rhodust";
        output->V[nv]          = d->DustDensity;
        output->stag_var[nv++] = -1;
        
        output->var_name[nv]   = (char *) "Tdust";
        output->V[nv]          = d->DustTemperature;
        output->stag_var[nv++] = -1;
        
        output->var_name[nv]   = (char *) "Tgas";
        output->V[nv]          = d->GasTemperature;
        output->stag_var[nv++] = -1;
        
#if SELFGRAVITY == YES
        output->var_name[nv]   = (char *) "phi";
        output->V[nv]          = d->phi;
        output->stag_var[nv++] = -1;
#endif
#if FLUXLIMITEDDIFFUSION == YES
        output->var_name[nv]   = (char *) "erad";
        output->V[nv]          = d->RadiationEnergyDensity;
        output->stag_var[nv++] = -1;
#endif
        
#if IRRADIATION == YES
        output->var_name[nv]   = (char *) "irad";
        output->V[nv]          = d->IrradiationPowerDensity;
        output->stag_var[nv++] = -1;
        //
        // Adding Flux output: ONORBE
        //
        output->var_name[nv]   = (char *) "FluxOpt";
        output->V[nv]          = d->FluxOpt;
        output->stag_var[nv++] = -1;
        output->var_name[nv]   = (char *) "FluxIon";
        output->V[nv]          = d->FluxIon;
        output->stag_var[nv++] = -1;
        output->var_name[nv]   = (char *) "ionx";
        output->V[nv]          = d->IonizationFraction;
        output->stag_var[nv++] = -1;
        output->var_name[nv]   = (char *) "NH";
        output->V[nv]          = d->NH;
        output->stag_var[nv++] = -1;
        output->var_name[nv]   = (char *) "NHI";
        output->V[nv]          = d->NHI;
        output->stag_var[nv++] = -1;
#endif
        
#if IONIZATION == YES
        output->var_name[nv]   = (char *) "ionx";
        output->V[nv]          = d->IonizationFraction;
        output->stag_var[nv++] = -1;
        
        output->var_name[nv]   = (char *) "iony";
        output->V[nv]          = d->NeutralFraction;
        output->stag_var[nv++] = -1;
        
        output->var_name[nv]   = (char *) "ieuv";
        output->V[nv]          = d->StellarEUVPhotonDensity;
        output->stag_var[nv++] = -1;
        
        output->var_name[nv]   = (char *) "ifuv";
        output->V[nv]          = d->StellarFUVPhotonDensity;
        output->stag_var[nv++] = -1;
        
        output->var_name[nv]   = (char *) "urec";
        output->V[nv]          = d->DirectRecombinationPhotonDensity;
        output->stag_var[nv++] = -1;
#endif
        output->var_name[nv]   = (char *) "fun";
        output->V[nv]          = d->fun;
        output->stag_var[nv++] = -1;
        
        output->nvar = nv;
        
        /* -- repeat for user defined vars -- */
        
        for (i = 0; i < input->user_var; i++){
            sprintf (output->var_name[i + nv], "%s", input->user_var_name[i]);
            output->V[i + nv] = d->Vuser[i];
            output->stag_var[i + nv] = -1; /* -- assume cell-centered -- */
        }
        
        /* -- add user vars to total number of variables -- */
        
        output->nvar += input->user_var;
        
        /* -- select which variables are going to be dumped to disk  -- */
        
        for (nv = output->nvar; nv--; ) output->dump_var[nv] = YES;
#if ENTROPY_SWITCH == YES
        output->dump_var[ENTR] = NO;
#endif
        
        switch (output->type){
            case DBL_OUTPUT:   /* -- dump ALL variables -- */
                sprintf (output->ext,"dbl");
                break;
            case FLT_OUTPUT:   /* -- do not dump staggered fields (below)-- */
                sprintf (output->ext,"flt");
                break;
            case DBL_H5_OUTPUT:   /* -- dump ALL variables -- */
                sprintf (output->ext,"dbl.h5");
                break;
            case FLT_H5_OUTPUT:   /* -- do not dump staggered fields (below)-- */
                sprintf (output->ext,"flt.h5");
                break;
            case VTK_OUTPUT:   /* -- do not dump staggered fields (below) -- */
                sprintf (output->ext,"vtk");
#if VTK_VECTOR_DUMP == YES
                D_EXPAND(output->dump_var[VX1] = VTK_VECTOR;  ,
                         output->dump_var[VX2] = NO;          ,
                         output->dump_var[VX3] = NO;)
#if PHYSICS == MHD || PHYSICS == RMHD
                D_EXPAND(output->dump_var[BX1] = VTK_VECTOR;  ,
                         output->dump_var[BX2] = NO;          ,
                         output->dump_var[BX3] = NO;)
#endif
#endif
                break;
            case TAB_OUTPUT:   /* -- do not dump staggered fields -- */
                sprintf (output->ext,"tab");
                break;
            case PPM_OUTPUT:   /* -- dump density only  -- */
                sprintf (output->ext,"ppm");
                for (nv = output->nvar; nv--; ) output->dump_var[nv] = NO;
                break;
            case PNG_OUTPUT:   /* -- dump density only  -- */
                sprintf (output->ext,"png");
                for (nv = output->nvar; nv--; ) output->dump_var[nv] = NO;
                break;
        }
        
        /* ---------------------------------------------------------------
         for divergence cleaning never dump the scalar psi unless
         the output type can be potentially used for restart
         --------------------------------------------------------------- */
        
#ifdef GLM_MHD
        if (output->type == DBL_OUTPUT || output->type == DBL_H5_OUTPUT)
            output->dump_var[PSI_GLM] = YES;
        else
            output->dump_var[PSI_GLM] = NO;
#endif
    }
    
    /* -- exclude stag components from all output except .dbl -- */
    
#ifdef STAGGERED_MHD
    D_EXPAND( SetDumpVar ("bx1s", VTK_OUTPUT, NO);  ,
             SetDumpVar ("bx2s", VTK_OUTPUT, NO);  ,
             SetDumpVar ("bx3s", VTK_OUTPUT, NO);)
    D_EXPAND( SetDumpVar ("bx1s", FLT_OUTPUT, NO);  ,
             SetDumpVar ("bx2s", FLT_OUTPUT, NO);  ,
             SetDumpVar ("bx3s", FLT_OUTPUT, NO);)
    D_EXPAND( SetDumpVar ("bx1s", FLT_H5_OUTPUT, NO);  ,
             SetDumpVar ("bx2s", FLT_H5_OUTPUT, NO);  ,
             SetDumpVar ("bx3s", FLT_H5_OUTPUT, NO);)
    D_EXPAND( SetDumpVar ("bx1s", TAB_OUTPUT, NO);  ,
             SetDumpVar ("bx2s", TAB_OUTPUT, NO);  ,
             SetDumpVar ("bx3s", TAB_OUTPUT, NO);)
#endif
    
    /* -- defaults: dump density only in ppm and png formats -- */
    
    SetDumpVar ((char *) "rho", PPM_OUTPUT, YES);
    SetDumpVar ((char *) "rho", PNG_OUTPUT, YES);
    /* ONORBE: excluding fields not wanted for vtk */
    D_EXPAND( SetDumpVar ("fun", VTK_OUTPUT, NO);  ,
              SetDumpVar ("irad", VTK_OUTPUT, NO);  ,
              SetDumpVar ("erad", VTK_OUTPUT, NO);)
    D_EXPAND( SetDumpVar ("rhodust", VTK_OUTPUT, NO);  ,
              SetDumpVar ("Tdust", VTK_OUTPUT, NO);  ,
              SetDumpVar ("Tgas", VTK_OUTPUT, NO);)
    /* ONORBE: excluding fields not wanted for vtk */
    D_EXPAND( SetDumpVar ("fun", DBL_OUTPUT, NO);  ,
              SetDumpVar ("irad", DBL_OUTPUT, NO);  ,
              SetDumpVar ("erad", DBL_OUTPUT, NO);)
    D_EXPAND( SetDumpVar ("rhodust", DBL_OUTPUT, NO);  ,
              SetDumpVar ("Tdust", DBL_OUTPUT, NO);  ,
              SetDumpVar ("Tgas", DBL_OUTPUT, NO);)
   
    ChangeDumpVar();
}

/* ********************************************************************* */
int SetDumpVar (char *var_name, int out_type, int flag)
/*!
 *  Include ('flag == YES') or exclude ('flag == NO') the
 *  variable associated to 'var_name' in or from the output
 *  type 'out_type'.
 *  If 'out_type' corresponds to an image (ppm or png), create
 *  a correspdonding Image structure.
 *
 * \param [in] var_name  the name of the variable (e.g. "rho", "vx1",...)
 * \param [in] out_type  select the output type (e.g., DBL_OUTPUT,
 *             VTK_OUTPUT, and so forth)
 * \param [in] flag     an integer values (YES/NO).
 *********************************************************************** */
{
    int k, nv;
    Output *output;
    
    for (k = 0; k < MAX_OUTPUT_TYPES; k++){
        output = all_outputs + k;
        if (output->type == out_type) break;
    }
    
    for (nv = output->nvar; nv--; ) {
        if (strcmp(output->var_name[nv], var_name) == 0) {
            output->dump_var[nv] = flag;
            if (flag == YES){
                if (out_type == PPM_OUTPUT || out_type == PNG_OUTPUT){
                    CreateImage (var_name);
                }
            }
            return(0);
        }
    }
    
    print1 ("! var_name '%s' cannot be set/unset for writing\n",var_name);
    return(1);
    
}

/* ********************************************************************* */
double ***GetUserVar (char *var_name)
/*! 
 *  return a pointer to the 3D array associated with the 
 *  variable named 'var_name'.
 *
 *********************************************************************** */
{
    int indx = -1;
    
    while (strcmp(all_outputs->var_name[++indx], var_name)){
        if (all_outputs->V[indx] == NULL){
            print1 ("! Error: uservar '%s' is not allocated\n"); 
            QUIT_PLUTO(1);
        }
    }
    return (all_outputs->V[indx]);
}
