#include "pluto.h"
#include "interface.h"
#include "analysis.h"
#include "boundaries.h"
#include "boundary_fluxes.h"
#include "DustEvolution.h"
#include "PhysicalConstantsCGS.h"
#include "Molecules.h"
#include "ReadRestrictionsConfiguration.h"

#if RADIATION == YES
#include "Radiation.h"
#endif

#if FLUXLIMITEDDIFFUSION == YES
#include "FLD.h"
#endif

#if STELLAREVOLUTION == YES
#include "StellarEvolution.h"
#endif



/* ********************************************************************* */
void Init (double *us, double x1, double x2, double x3, Data *data, Grid *grid, int kl, int jl, int il)
/*
 *
 *
 *
 *********************************************************************** */
{
	static int first_call = 1;
	
    double r_sph, theta, phi, r_min, r_max, x_center, y_center, z_center, x, y, z, r, r_cloud;
    double rho_in, rho_ex, P_in, P_ex, T_in, T_ex, ion_ini;
	
//    printf("dx =    %e\n", grid[IDIR].dx[IBEG]);
//    printf("dy =    %e\n", grid[JDIR].dx[JBEG]*grid[IDIR].x[IBEG]);
//    printf("dy/dx = %e\n", grid[JDIR].dx[JBEG]*grid[IDIR].x[IBEG] / grid[IDIR].dx[IBEG]);
//    exit(1);
	
	// ********************
	// * Reference System *
	// ********************
	//
	if(first_call) InitializeReferenceSystem();
#if RADIATION != YES
    ReferenceTemperature = g_inputParam[T_in_cgs]; // in [K]
#endif
    
    //
    // Spherical coordinates of computational domain:
    //
    r_sph = x1;
#if DIMENSIONS > 1
    theta = x2;
#else
    theta = 0.0;
#endif
#if DIMENSIONS > 2
    phi = x3;
#else
    phi = 0.0;
#endif
    
    //
	// Inner and outer radius of computational domain:
	//
	r_min = grid[IDIR].xl_glob[grid[IDIR].gbeg]; // in code units
	r_max = grid[IDIR].xr_glob[grid[IDIR].gend]; // in code units
    
    //
    // Compute location of center of core as center of computational domain:
    //
    x_center = 0.0;
    y_center = 0.0;
    z_center = r_min + 0.5 * (r_max - r_min);
    
    //
    // Cartesian coordinates:
    //
    x = r_sph * sin(theta) * cos(phi);
    y = r_sph * sin(theta) * sin(phi);
    z = r_sph * cos(theta);
    
    //
    // Spherical coordinates wrt to cloud center:
    //
    r = sqrt((x-x_center)*(x-x_center) + (y-y_center)*(y-y_center) + (z-z_center)*(z-z_center));
    
    
    //
    // Physical setup:
    ion_ini = 0.0; //initial ionization fraction
    //
    // Isobar model:
    r_cloud = g_inputParam[r_cloud_pc] * CONST_pc / ReferenceLength;
    rho_in  = g_inputParam[rho_in_cgs] / ReferenceDensity;
    rho_ex  = g_inputParam[rho_ex_cgs] / ReferenceDensity;
    T_in    = g_inputParam[T_in_cgs]   / ReferenceTemperature;
    P_in    = R_UniversalGasConstant / MolarMass(ion_ini) * rho_in * T_in;
    P_ex    = P_in;
    T_ex    = MolarMass(ion_ini) / R_UniversalGasConstant * P_ex / rho_ex;
    
    // Isothermal model:
    //r_cloud = g_inputParam[r_cloud_pc] * CONST_pc / ReferenceLength;
    //rho_in  = g_inputParam[rho_in_cgs] / ReferenceDensity;
    //rho_ex  = g_inputParam[rho_ex_cgs] / ReferenceDensity;
    //T_in    = g_inputParam[T_in_cgs]   / ReferenceTemperature;
    //T_ex    = T_in;
    //P_in    = R_UniversalGasConstant / MolarMass(ion_ini) * rho_in * T_in;
    //P_ex    = R_UniversalGasConstant / MolarMass(ion_ini) * rho_ex * T_ex;
    
    
    if(r < r_cloud){
        //
        // interior:
        //
        us[RHO] = rho_in;
#if EOS != ISOTHERMAL
        us[PRS] = P_in;
#if PDVINRT == YES
        data->fun[kl][jl][il] = us[PRS];
#endif
#endif
        data->DustTemperature[kl][jl][il] = T_in;
        data->GasTemperature[kl][jl][il]  = T_in;
#if RADIATION == YES
        data->RadiationEnergyDensity[kl][jl][il]  = a_RadiationConstant * pow(T_in, 4);
#endif
    }
    else{
        //
        // exterior:
        //
        us[RHO] = rho_ex;
#if EOS != ISOTHERMAL
        us[PRS] = P_ex;
#if PDVINRT == YES
        data->fun[kl][jl][il] = us[PRS];
#endif
#endif
        data->DustTemperature[kl][jl][il] = T_ex;
        data->GasTemperature[kl][jl][il]  = T_ex;
#if RADIATION == YES
        data->RadiationEnergyDensity[kl][jl][il]  = a_RadiationConstant * pow(T_ex, 4);
#endif

    }

    
#if RADIATION == YES
    data->DustDensity[kl][jl][il]             = Dust2GasMassRatio;
    data->IrradiationPowerDensity[kl][jl][il] = 0.0;
    data->FluxOpt[kl][jl][il] = 0.0;
    data->FluxIon[kl][jl][il] = 0.0;
    data->IonizationFraction[kl][jl][il] = ion_ini;
    data->NH[kl][jl][il] = 0.0;
    data->NHI[kl][jl][il] = 0.0;
#endif

    
    //
    // Cloud at rest:
    //
    us[VX1] = 0.0;
#if COMPONENTS > 1
    us[VX2] = 0.0;
#endif
#if COMPONENTS > 2
    us[VX3] = 0.0;
#endif
	
	
    //
    // No gravity associated with AGN:
    //
	//M_X1_BEG = 0.0;
	//StellarMass =
    
	first_call = 0;
}



/* ********************************************************************* */
void Analysis (const Data *d, Grid *grid)
/*
 *
 *
 *********************************************************************** */
{
	static int first_call = 1;
    
	WriteAnalysis(d, grid);
	
	first_call = 0;
}



/* ********************************************************************* */
void UserDefBoundary (const Data *d, RBox *box, int side, Grid *grid)
/*!
 *  Assign user-defined boundary conditions.
 *
 * \param [in,out] d  pointer to the PLUTO data structure containing
 *                    cell-centered primitive quantities (d->Vc) and
 *                    staggered magnetic fields (d->Vs, when used) to
 *                    be filled.
 * \param [in] box    pointer to a RBox structure containing the lower
 *                    and upper indices of the ghost zone-centers/nodes
 *                    or edges at which data values should be assigned.
 * \param [in] side   specifies the boundary side where ghost zones need
 *                    to be filled. It can assume the following
 *                    pre-definite values: X1_BEG, X1_END,
 *                                         X2_BEG, X2_END,
 *                                         X3_BEG, X3_END.
 *                    The special value side == 0 is used to control
 *                    a region inside the computational domain.
 * \param [in] grid  pointer to an array of Grid structures.
 *
 *********************************************************************** */
{
    
	if(side == 0){
        ApplyRestrictions(d, grid);
	}
    else if(side == X1_BEG){
        BC_Outflow_NoInflow(d, side, grid);
#if COMPONENTS > 1
        BC_ZeroGradient(d, side, grid, VY);
#endif
#if COMPONENTS > 2
        BC_ZeroGradient(d, side, grid, VZ);
#endif
    }
    else if(side == X1_END){
        BC_Outflow_NoInflow(d, side, grid);
#if COMPONENTS > 1
        BC_ZeroGradient(d, side, grid, VY);
#endif
#if COMPONENTS > 2
        BC_ZeroGradient(d, side, grid, VZ);
#endif
    }
}

